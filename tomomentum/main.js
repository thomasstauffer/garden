import * as q from '../crow/q.js';
import { html, toDOM } from './../crow/html.js';

// TODO move/rename into Crow
// 1m -> 0.001, 1M = 1'000'000, ...
function numberFromSI(value) {
	const prefixes = { 'Y': 1e+24, 'Z': 1e+21, 'E': 1e+18, 'P': 1e+15, 'T': 1e+12, 'G': 1e+9, 'M': 1e+6, 'k': 1e+3, '': 1, 'm': 1e-3, 'u': 1e-6, 'n': 1e-9, 'p': 1e-12, 'f': 1e-15, 'a': 1e-18, 'z': 1e-21, 'y': 1e-24 };
	const chars = Object.keys(prefixes).join('');
	const re = RegExp('(-?[0-9]+(.[0-9]+)?)([' + chars + '])?'); // TODO according to eslint \. is unnecessary? TODO test
	const match = value.replace(/'/g, '').match(re);
	if (match === null) {
		return 0.0;
	} else {
		const base = parseFloat(match[1]);
		const unit = match[3] === undefined ? '' : match[3];
		const result = base * prefixes[unit];
		return result;
	}
}

// TODO move/rename into Crow
function numberToSI(value) {
	const prefixes = { '24': 'Y', '21': 'Z', '18': 'E', '15': 'P', '12': 'T', '9': 'G', '6': 'M', '3': 'k', '0': '', '-3': 'm', '-6': 'μ', '-9': 'n', '-12': 'p', '-15': 'f', '-18': 'a', '-21': 'z', '-24': 'y' };

	const sign = value < 0 ? '-' : '';
	const v = Math.abs(value);

	const expSci = Math.log10(v);
	const expEng = Math.floor(expSci / 3) * 3;

	const normal = sign + v.toPrecision(6);
	const eng = sign + (v / Math.pow(10, expEng)).toFixed(3) + 'e' + expEng;
	const si = sign + (v / Math.pow(10, expEng)).toFixed(3) + prefixes[expEng];

	return [normal, eng, si];
}

function calc() {
	/* eslint-disable no-unused-vars */

	const ρcu = 1.68e-8;
	const cu_temperature_coefficient = 3.93e-3; // at 25 °C
	const cu_tc = cu_temperature_coefficient;
	const u0 = 4 * Math.PI * 1e-7;
	const ucu = 0.999994;
	const e0 = 8.854187817 * 1e-12;
	const pi = Math.PI;
	const c0 = 299792458;
	// ρ=rho θ=theta

	const ln = Math.log;
	const pow = Math.pow;
	const sqrt = Math.sqrt;
	const log10 = x => Math.log(x) / Math.log(10);

	/* eslint-enable no-unused-vars */

	const nodeForms = [...document.getElementsByTagName('form')];
	nodeForms.forEach(nodeForm => {
		const formulas = nodeForm.getElementsByClassName('formulas')[0].innerHTML.split('\n');
		const nodeInputs = [...nodeForm.getElementsByTagName('input')];
		let code = '';

		// inputs

		q.each(nodeInputs, function (input) {
			const name = input.dataset['name'];
			const value = numberFromSI(input.value);
			code += 'const ' + name + '=(' + value + ');';
		});

		// formulas

		q.each(formulas, function (formula) {
			formula = formula.trim();
			if (formula.length > 0) {
				code += 'const ' + formula + ';';
			}
		});

		// output

		const nodeOutput = nodeForm.getElementsByTagName('span')[0];
		code += nodeOutput.dataset['name'] + ';';
		try {
			const result = eval(code);
			nodeOutput.innerHTML = numberToSI(result).join(' | ');
		} catch (e) {
			console.log('code', code);
			console.log(e);
		}
	});
}

function main() {
	// fill navigation

	const nodeNav = document.getElementsByTagName('nav')[0];
	const nodeHeaders = [...document.getElementsByTagName('h1')];
	nodeHeaders.forEach(nodeHeader => {
		const nodeA = nodeHeader.children[0];
		nodeNav.appendChild(toDOM(html('p', html('a', { 'href': '#' + nodeA.id }, nodeA.textContent))));
	});

	// set input handlers

	const nodeInputs = [...document.getElementsByTagName('input')];
	nodeInputs.forEach(nodeInput => {
		nodeInput.addEventListener('input', calc);
	});
	calc();
}

window.addEventListener('load', main);