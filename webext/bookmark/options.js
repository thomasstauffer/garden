
function walk(bookmark, node) {
	if (bookmark.type === 'bookmark') {
		const childNode = document.createElement('li');
		node.appendChild(childNode);

		const selectNode = document.createElement('input');
		selectNode.dataset.bookmarkid = bookmark.id;
		selectNode.setAttribute('type', 'checkbox');
		childNode.append(selectNode);
		const textNode = document.createTextNode(bookmark.title);
		childNode.append(textNode);
		const linkNode = document.createElement('a');
		childNode.append(linkNode);
		linkNode.setAttribute('href', bookmark.url);
		linkNode.textContent = ' (' + bookmark.url.slice(0, 30) + '...)';
	} else if (bookmark.type === 'folder') {
		const childNode = document.createElement('li');
		childNode.dataset.bookmarkid = bookmark.id;
		node.appendChild(childNode);

		const selectNode = document.createElement('input');
		selectNode.dataset.bookmarkid = bookmark.id;
		selectNode.setAttribute('type', 'checkbox');
		childNode.append(selectNode);
		const textNode = document.createTextNode('📁 ' + bookmark.title + ' ');
		childNode.append(textNode);
		const moveNode = document.createElement('button');
		moveNode.dataset.bookmarkid = bookmark.id;
		moveNode.textContent = '⬇';
		moveNode.addEventListener('click', () => move(bookmark.id));
		childNode.append(moveNode);

		const childrenNode = document.createElement('ul');
		node.appendChild(childrenNode);
		for (const childBookmark of bookmark.children) {
			walk(childBookmark, childrenNode);
		}
	} else {
		throw 'Unknown Bookmark Type: ' + bookmark.type;
	}
}

async function build() {
	const bookmarkRoot = await browser.bookmarks.getTree();
	const fragment = document.createDocumentFragment();
	walk(bookmarkRoot[0], fragment);
	const mainNode = document.getElementById('main');
	mainNode.innerHTML = '';
	mainNode.appendChild(fragment);
}

async function move(bookmarkid) {
	const selectedNodes = document.querySelectorAll('input:checked');
	const promises = [];
	for (const child of selectedNodes) {
		promises.push(browser.bookmarks.move(child.dataset.bookmarkid, {parentId: bookmarkid}));
	}
	await Promise.all(promises);
	await build();
}

async function main() {
	await build();
	document.getElementById('debug').textContent = ':)';
}

document.addEventListener('DOMContentLoaded', main);
