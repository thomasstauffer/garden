// TODO popup is not updated if data changes in background-script

function tag(name, ...children) {
	const node = document.createElement(name);
	for (const child of children) {
		node.appendChild(child);
	}
	return node;
}

function text(text) {
	return document.createTextNode(text);
}

function viewButton(caption, selected, onClick) {
	const node = tag('span', text(caption));
	node.className = 'button ' + (selected ? 'selected' : '');
	node.addEventListener('click', onClick);
	return node;
}

async function onClickIdentity(identityClicked) {
	const data = (await browser.runtime.getBackgroundPage()).data;
	for (const identity of data.identities) {
		if (identity.service === identityClicked.service) {
			identity.default = identity.name === identityClicked.name;
		}
	}
	//location.reload();
	await view();
}

async function onClickOnOff() {
	const data = (await browser.runtime.getBackgroundPage()).data;
	data.enabled = !data.enabled;
	await view();
}

async function view() {
	const data = (await browser.runtime.getBackgroundPage()).data;

	const nodeMain = document.getElementById('main');
	nodeMain.innerHTML = '';
	const nodeOnOff = viewButton('Enabled', data.enabled, onClickOnOff);
	nodeMain.appendChild(tag('p', nodeOnOff));
	for (const service of data.services) {
		const nodeService = tag('p');
		nodeService.appendChild(tag('span', text(service.name)));
		const identities = data.identities.filter($ => $.service == service.name);
		for (const identity of identities) {
			const nodeIdentity = viewButton(identity.name, identity.default, () => onClickIdentity(identity));
			nodeService.appendChild(nodeIdentity);
		}
		const noDefault = identities.filter($ => $.default === true).length === 0;
		const nodeRandom = viewButton('Random', noDefault, () => onClickIdentity({service: service.name, name: null}));
		nodeService.appendChild(nodeRandom);
		nodeMain.appendChild(nodeService);
	}

	//const storage = await browser.storage.local.get();
	// storage.test
	document.getElementById('debug').innerText = (new Date()).toISOString();
}

async function loaded() {
	view();
}

document.addEventListener('DOMContentLoaded', loaded);
