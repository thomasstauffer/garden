
TODO

- ask a user which of several identities they like to use when they open an URL e.g. google.com
- bugfix: if two identites for the same service are opened, don't switch them
- custom hosts?
- disable container opener, option to enable/disable it, e.g. useful if problems with other extensions
- disable/enable pw filler
- delete IndexedDB, LocalStorage, Downloads, FormData, Workers, Cache, PluginData for specific domains?
- do not create containers for all identites, only when needed? means it is not visible in FF when opening a tab?
- script/makefile to create signed/unsigned extension
- check for breached names/passwords?
- back button almost unusable now
- create a class/functions to get/update identities/services, in the future will store data in the cloud
- only use TLD top level domain -> 'a.b.c.d.'.split('.').filter($ => $).slice(-2).join('.') -> c.d

Internet pages used by friends

- lwn.net
- mail.google.com
- my Jenkins
- my own server
- WhatsApp
- Telegram
- LinkedIn
- FB
- Reddit
- GitHub
- Jira/Confluence/GitLab
- Discord
- Feedbin
- Moodle
- Pocket
- Trello

Look At

Temporary Containers
https://github.com/stoically/temporary-containers

Facebook Container
https://github.com/mozilla/contain-facebook

Forget Me Not
https://github.com/Lusito/forget-me-not

uBlock Origin
https://github.com/gorhill/uBlock
