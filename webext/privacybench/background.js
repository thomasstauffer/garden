import identitiesDefault from './identities.js';
import servicesDefault from './services.js';

// TODO create context for named identity only when it is used for the first time

async function load(url) {
	const response = await fetch(url);
	return await response.json();
}

// https://www.domain.com/path?s=lmgtfy -> www.domain.com
function urlHost(url) {
	return new URL(url).hostname;
}

// https://subdomain.tld.com/path -> tld.com
function urlTopLevelDomain(url) {
	return urlHost(url).split('.').slice(-2).join('.');
}

function sleep(milliSeconds) {
	return new Promise(resolve => setTimeout(resolve, milliSeconds));
}

// https://www.domain.com/path?s=lmgtfy -> https://www.domain.com/path
/*
function urlOriginPath(url) {
	const object = new URL(url);
	return object.origin + object.pathname;
}
*/

const data = {
	identities: [],
	services: [],
	enabled: true,
};

/*
function getISOTime() {
	//const time = (new Date()).toISOString();
	const tzOffsetMilliseconds = (new Date()).getTimezoneOffset() * 60000;
	const time = (new Date(Date.now() - tzOffsetMilliseconds)).toISOString().slice(0, -1);
	return time;
}
*/

function nameMake(options) {
	if (options.random !== undefined) {
		return 'Privacy Bench Random - ' + options.name;
	} else if (options.name !== undefined) {
		return 'Privacy Bench - ' + options.name;
	} else {
		throw 'Unknown Option';
	}
}

function nameIsRandom(name) {
	return name.startsWith('Privacy Bench Random - ');
}

async function cleanupUnusedContextIdentities() {
	console.log('BS cleanupUnusedContextIdentities');
	// only delete cookie stores which are not used in any tab
	const cookieStoreWithoutTabIds = (await browser.cookies.getAllCookieStores()).map($ => $.id);
	const contextualIdentities = await browser.contextualIdentities.query({});
	for (const contextualIdentity of contextualIdentities) {
		if (nameIsRandom(contextualIdentity.name)) {
			if (cookieStoreWithoutTabIds.indexOf(contextualIdentity.cookieStoreId) === -1) {
				await browser.contextualIdentities.remove(contextualIdentity.cookieStoreId);
			}
		}
	}
}

async function contextualIdentitiesCreate(name) {
	return await browser.contextualIdentities.create({name: name, color: 'turquoise', icon: 'fingerprint'});
}

async function contextualIdentitiesGetOrCreate(name) {
	const contextualIdentities = await browser.contextualIdentities.query({name: name});
	if (contextualIdentities.length === 0) {
		return await contextualIdentitiesCreate(name);
	} else {
		return contextualIdentities[0];
	}
}

async function loaded() {
	await cleanupUnusedContextIdentities();
	try {
		const identities = await load('http://127.0.0.1:3333/identities');
		const services = await load('http://127.0.0.1:3333/services');
		data.identities = identities;
		data.services = services;
	} catch (error) {
		data.identities = identitiesDefault;
		data.services = servicesDefault;
	}
	// TODO verify that one and only one identity has default:true per service
	for (const identity of data.identities) {
		const name = nameMake({name: identity.name});
		const contextualIdentity = await contextualIdentitiesGetOrCreate(name);
		identity.cookieStoreId = contextualIdentity.cookieStoreId;
	}
}

async function onConnectContent(port) {
	//console.log('BS onConnectContent', JSON.stringify(port.sender.tab.cookieStoreId));
	//console.log('BS onConnectContent', JSON.stringify(port.sender.url));

	const cookieStoreId = port.sender.tab.cookieStoreId;
	const identity = data.identities.filter($ => $.cookieStoreId === cookieStoreId)[0];
	if (identity !== undefined) {
		const service = data.services.filter($ => $.name === identity.service)[0];
		if (service != undefined) {
			const fields = [
				{name: service.login.user, value: identity.login.user},
				{name: service.login.password, value: identity.login.password},
			];
			port.postMessage({fields: fields, info: ''});
		}
	}
}

async function onBeforeRequest(details) {
	//console.log('BS onBeforeRequest', JSON.stringify(details));

	if (!data.enabled) {
		return;
	}

	// TODO rename host, is not appropriate name
	const host = urlTopLevelDomain(details.url);

	console.log('BS onBeforeRequest', host, details.originUrl, browser.extension.getURL('/'));

	// check if the URL belongs to any identity
	let cookieStoreIdTarget = null;
	for (const identity of data.identities) {
		if (identity.default) {
			const service = data.services.filter($ => $.name === identity.service)[0];
			if (service !== undefined) {
				if (service.hosts.indexOf(host) !== -1) {
					cookieStoreIdTarget = identity.cookieStoreId;
					break;
				}
			}
		}
	}

	if (cookieStoreIdTarget !== null) {
		// does the current tab already a correct contextIdentitity?
		if (details.cookieStoreId === cookieStoreIdTarget) {
			return;
		}
	} else {
		console.log('BS onBeforeRequest 1');

		// is "moz-extension://aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee/" if called internally
		if (details.originUrl === browser.extension.getURL('/')) {
			return;
		}

		console.log('BS onBeforeRequest 2');

		// originUrl is undefined if a new tab manually opened
		if ((details.originUrl !== undefined) && (host === urlTopLevelDomain(details.originUrl))) {
			return;
		}

		console.log('BS onBeforeRequest 3');

		const name = nameMake({random: true, name: host});
		const contextualIdentity = await contextualIdentitiesCreate(name);
		cookieStoreIdTarget = contextualIdentity.cookieStoreId;
	}

	const tab = await browser.tabs.get(details.tabId);
	await browser.tabs.create({url: details.url, cookieStoreId: cookieStoreIdTarget, active: tab.active, index: tab.index, windowId: tab.windowId});
	await browser.tabs.remove(details.tabId); // careful, tabs.onRemoved is called
	return {cancel: true};
}

async function onTabRemoved() {
	await sleep(30000);
	await cleanupUnusedContextIdentities();
}

console.log('PrivacyHub Password Manager');

// can be accessed via browser.runtime.getBackgroundPage()
window.data = data;

browser.webRequest.onBeforeRequest.addListener(onBeforeRequest, {urls: ['<all_urls>'], types: ['main_frame']}, ['blocking']);

// called when a content-script opens a port
browser.runtime.onConnect.addListener(onConnectContent);

browser.tabs.onRemoved.addListener(onTabRemoved);

(async function () {
	await loaded();
})();
