
= Developing

Download Source Folder
about:debugging#/runtime/this-firefox
Load Temporary Add-on
Select Folder

= URLs

about:debugging#/runtime/this-firefox
about:addons
about:preferences
about:preferences#containers

= Documentation

https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Work_with_contextual_identities
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/contextualIdentities
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/cookies
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/Port

= Server

npm install
npm start

= Create Unsigned Extension

about:config
xpinstall.signatures.required -> false
zip -0 PrivacyBenchUnsigned.xpi manifest.json *.js *.png *.html
about:addons -> Install Add-on From File

= Create Signed Extension

https://addons.mozilla.org/en-US/developers/addon/api/key/
npm install -g web-ext
web-ext sign --api-key=x:x:X --api-secret=x

= Icon

https://game-icons.net/1x1/lorc/skeleton-key.html
