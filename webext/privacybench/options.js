
function viewText(tagName, text) {
	const node = document.createElement(tagName);
	node.textContent = text;
	return node;
}

function viewRow(tagName, values) {
	const nodeRow = document.createElement('tr');
	for (const value of values) {
		const node = document.createElement(tagName);
		node.textContent = value;
		nodeRow.appendChild(node);
	}
	return nodeRow;
}

function viewCookieStore(cookies) {
	async function cookieDelete(cookie) {
		const url = (cookie.secure ? 'https://' : 'http://') + cookie.domain + cookie.path;
		const details = {storeId: cookie.storeId, name: cookie.name, url: url};
		await browser.cookies.remove(details);
		location.reload();
	}

	const node = document.createElement('table');
	node.appendChild(viewRow('th', ['domain', 'name', 'path', 'secure', 'session', 'len(value)']));
	for (const cookie of cookies) {
		const nodeButton = viewText('button', 'Clear Cookie');
		nodeButton.addEventListener('click', () => cookieDelete(cookie));

		const nodeCell = document.createElement('td');
		nodeCell.appendChild(nodeButton);

		const nodeRow = viewRow('td', [cookie.domain, cookie.name, cookie.path, cookie.secure, cookie.session, cookie.value.length]);
		nodeRow.appendChild(nodeCell);

		node.appendChild(nodeRow);
	}
	return node;
}

async function updateTest() {
	const valueFromInput = document.getElementById('testInput').value;
	await browser.storage.local.set({test: valueFromInput});
}

async function main() {
	async function contextIdentityDelete(cookieStoreId) {
		await browser.contextualIdentities.remove(cookieStoreId);
		location.reload();
	}

	const fragment = document.createDocumentFragment();

	const cookieStoresInContextIdentities = [];

	const contextualIdentities = await browser.contextualIdentities.query({});
	fragment.append(viewText('h2', 'Contextual Identities'));
	for (const contextualIdentity of contextualIdentities) {
		fragment.append(viewText('h3', contextualIdentity.name));

		const info = `${contextualIdentity.cookieStoreId} ${contextualIdentity.icon} `;
		const nodeInfo = viewText('span', info);
		nodeInfo.style.color = contextualIdentity.color;
		fragment.append(nodeInfo);

		const nodeButton = viewText('button', 'Clear Identity');
		nodeButton.addEventListener('click', () => contextIdentityDelete(contextualIdentity.cookieStoreId));
		fragment.append(nodeButton);

		const cookies = await browser.cookies.getAll({storeId: contextualIdentity.cookieStoreId});
		fragment.append(viewCookieStore(cookies));

		cookieStoresInContextIdentities.push(contextualIdentity.cookieStoreId);
	}

	const cookieStores = await browser.cookies.getAllCookieStores();
	fragment.append(viewText('h2', 'Cookie Stores'));
	for (const cookieStore of cookieStores) {
		if (!cookieStoresInContextIdentities.includes(cookieStore.id)) {
			fragment.append(viewText('h3', cookieStore.id));

			const cookies = await browser.cookies.getAll({storeId: cookieStore.id});
			fragment.append(viewCookieStore(cookies));
		}
	}

	const mainNode = document.getElementById('main');
	mainNode.innerHTML = '';
	mainNode.appendChild(fragment);

	document.getElementById('testInput').addEventListener('input', updateTest);

	//await browser.storage.local.set({test: 'default'});
	const testFromStorage = await browser.storage.local.get('test');
	document.getElementById('testStorage').innerText = JSON.stringify(testFromStorage);

	document.getElementById('debug').textContent = (new Date()).toISOString();
}

document.addEventListener('DOMContentLoaded', main);
