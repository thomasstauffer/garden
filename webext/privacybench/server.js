/*
Test

curl http://127.0.0.1:3333
*/

import identitiesDefault from './identities.js';
import servicesDefault from './services.js';
import http from 'http';

function timeCurrentISO() {
	return (new Date()).toISOString();
}

function responseJSON(res, data) {
	res.setHeader('Content-Type', 'application/json;charset=utf-8');
	res.end(JSON.stringify(data));
}

function responseError(res, code) {
	res.statusCode = code;
	res.end(`${code}`);
}

function handler(req, res) {
	if (req.method !== 'GET') {
		responseError(405); // Method Not Allowed
	} else {
		console.log('GET', req.url);
		if (req.url === '/status') {
			const status = {
				timeStartup: timeStartup,
				timeCurrent: timeCurrentISO(),
			};
			responseJSON(res, status);
		} else if (req.url === '/identities') {
			responseJSON(res, identitiesDefault);
		} else if (req.url === '/services') {
			responseJSON(res, servicesDefault);
		} else {
			responseError(404); // Not Found
		}
	}
}

const timeStartup = timeCurrentISO();
const port = 3333;
const server = http.createServer(handler);
server.listen(port, () => {
	console.log(`http://127.0.0.1:${port}`);
});
