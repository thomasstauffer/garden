async function message(data) {
	//console.log('CS message', JSON.stringify(data));

	if (data.info.length > 0) {
		const nodeHint = document.createElement('pre');
		nodeHint.style.position = 'absolute';
		nodeHint.style.bottom = '10px';
		nodeHint.style.right = '10px';
		nodeHint.style.width = '25em';
		nodeHint.style.height = '20em';
		nodeHint.style.padding = '10px';
		nodeHint.style.backgroundColor = '#ccfc';
		nodeHint.style.zIndex = 666;
		nodeHint.style.border = '3px solid #99f';
		nodeHint.style.borderRadius = '5px';
		nodeHint.style.overflow = 'scroll';
		nodeHint.innerText = data.info;
		document.body.appendChild(nodeHint);
	}

	for (const field of data.fields) {
		const node = document.getElementsByName(field.name)[0];
		if (node !== undefined) {
			node.value = field.value;
		}
	}
}

const port = browser.runtime.connect();
port.onMessage.addListener(message);
