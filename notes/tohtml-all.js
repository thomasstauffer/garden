import fs from 'fs';
import path from 'path';
import * as tomark from '../tomark/tomark.js';
import * as q from '../crow/q.js';

const OUTPUT_DIRECTORY = 'html';

const INDEX_TEMPLATE = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{0}</title>
<link rel="stylesheet" href="../base.css">
</head>
<body>
<header>Notes</header>
<main>

<h1>{0}</h1>

<p>Collection of more or less personal (opinionated) notes.</p>

<ul>
{1}
</ul>
</main>
<footer>Thomas Stauffer</footer>
</body>
</html>
`;

const DOCUMENT_TEMPLATE = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>{0}</title>
<link rel="stylesheet" href="{1}">
<style>

.tomark-format-keyword {
	color: yellow;
}

.tomark-format-number {
	color: orange;
}

</style>
</head>
<body>
<header>{0}</header>
<main>{2}</main>
</body>
</html>
`;

function mkdir(dirname) {
	if (!fs.existsSync(dirname)) {
		fs.mkdirSync(dirname);
	}
}

function walkdir(dirname, callback) {
	const dirnames = [];
	const filenames = [];
	fs.readdirSync(dirname, { withFileTypes: true }).forEach((dirEntry) => {
		if (dirEntry.isDirectory()) {
			dirnames.push(dirEntry.name);
		} else if (dirEntry.isFile()) {
			filenames.push(dirEntry.name);
		}
	});
	callback(dirname, dirnames, filenames);
	dirnames.forEach(subdirname => {
		walkdir(path.join(dirname, subdirname), callback);
	});
}

mkdir(OUTPUT_DIRECTORY);
let indexHTML = '';
walkdir('.', (dirpath, dirnames, filenames) => {
	if (dirpath.startsWith(OUTPUT_DIRECTORY)) {
		return;
	}
	mkdir(path.join(OUTPUT_DIRECTORY, dirpath));
	indexHTML += `<h2>${dirpath}</h2>`;
	const level = ('/' + dirpath).match(/\//g).length;
	const stylePath = '../'.repeat(level + 2) + 'base.css';
	filenames.forEach(filename => {
		const filenameInput = path.join(dirpath, filename);
		if (filename.endsWith('.tomark')) {
			const title = path.parse(filename).name;
			console.log('Convert', title);
			const filenameOutput = path.join(OUTPUT_DIRECTORY, dirpath, title + '.html');
			const text = fs.readFileSync(filenameInput, 'utf8');
			const body = tomark.toHTML(text);
			const document = q.format(DOCUMENT_TEMPLATE, title, stylePath, body);
			fs.writeFileSync(filenameOutput, document);
			indexHTML += `<li><a href="${filenameOutput}">${title}</a></li>\n`;
		} else {
			const filenameOutput = path.join(OUTPUT_DIRECTORY, dirpath, filename);
			console.log('Copy', filename);
			fs.copyFileSync(filenameInput, filenameOutput);
		}
	});
});
fs.writeFileSync('index.html', q.format(INDEX_TEMPLATE, 'Notes', indexHTML));
