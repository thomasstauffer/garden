// TODO use import

const Discord = require('discord.js');
const auth = require('./auth.json');

/*

Developer
https://discord.com/developers/applications/

Discord.js 11.2
https://discord.js.org/#/docs/main/11.2.0/general/welcome

Administrator:
https://discordapp.com/oauth2/authorize?&client_id=739868042770645074&scope=bot&permissions=8

Only Read & Write
https://discordapp.com/oauth2/authorize?&client_id=739868042770645074&scope=bot&permissions=67584

auth.json contains
{
    "token": "..."
}

*/

const client = new Discord.Client();

async function onReady() {
	console.log('ready', client.user.tag);

	const test = false;
	if (test) {
		client.guilds.forEach(async (guild) => {
			//console.log(guild.name);
			const result = await tlm(guild);
			for (const member of Object.values(result)) {
				console.log(member.username, new Date(member.lastTimestamp));
			}
		});
	}
}

client.on('ready', onReady);

async function fetchMoreMessages(channel, limit) {
	const result = [];
	let lastMessageId;
	const discordLimit = 100; // 2020-08-01 seems to be the default

	while (result.length < limit) {
		const options = { limit: discordLimit, before: lastMessageId };
		const messages = await channel.fetchMessages(options);
		if (messages.size == 0) { // no message at all
			break;
		}
		result.push(...messages.array());
		console.log(options, messages.size);
		lastMessageId = messages.last().id;
		if (messages.size < discordLimit) { // no more messages
			break;
		}
	}
	return result;
}

async function tlm(guild) {
	const tlm = {};

	//console.log(guild.members);
	for (const member of guild.members.values()) {
		console.log('member', member.id, member.user.username);
		if (!(member.id in tlm)) {
			tlm[member.id] = { username: member.user.username, lastTimestamp: 0 };
		}
	}
	//console.log(guild.channels);
	for (const channel of guild.channels.values()) {
		console.log('channel', channel.name, channel.type, channel.id);
		if (channel.type === 'text') {
			//const messages = await channel.fetchMessages({ limit: 100 });
			const messages = await fetchMoreMessages(channel, 1000);
			for (const message of messages) {
				//console.log('message', new Date(message.createdTimestamp), message.author.id, message.content);

				if (!(message.author.id in tlm)) {
					tlm[message.author.id] = { username: message.author.username, lastTimestamp: message.createdTimestamp };
				}
				if (message.createdTimestamp > tlm[message.author.id].lastTimestamp) {
					tlm[message.author.id].lastTimestamp = message.createdTimestamp;
				}
			}
		}
	}

	console.log(tlm);

	return tlm;
}

async function onMessage(msg) {
	if (msg.content === '/tlm ping') {
		msg.reply('pong');
	} else if (msg.content === '/tlm tlm') {
		var s = 'The Last Message\n';
		const result = await tlm(msg.guild);
		for (const member of Object.values(result)) {
			s += '**' + member.username + '** ' + member.lastTimestamp + ' (' + new Date(member.lastTimestamp) + ')\n';
		}
		msg.channel.send(s);
	} else if (msg.content === '/tlm debug') {
		//
	}
}

client.on('message', onMessage);

// Bot - Build-A-Bot - Token - Reveal
client.login(auth.token);
