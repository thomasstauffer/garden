
//import { m, render } from 'https://cdn.skypack.dev/million';
import { m, render } from './node_modules/million/dist/million.mjs';

import * as physics from './physics.js';

//const prefixes = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion'];
//const prefixes = [ '', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
//const prefixes = [ '', 'Kilo', 'Mega', 'Giga', 'Tera', 'Peta', 'Exa', 'Zetta', 'Yotta'];

function toEng(number, unit) {
	if (number === 0) {
		return '0 ' + unit;
	} else if (number < 1e-3) {
		const exponent = Math.floor(Math.log10(Math.abs(number)));
		const exponent3 = Math.floor(exponent / 3);
		const base = number / Math.pow(10, exponent3 * 3);
		return base.toFixed(3) + 'e' + (exponent3 * 3) + ' ' + unit;
	} else if (number < 1e3) {
		return number.toFixed(3) + ' ' + unit;
	} else {
		const exponent = Math.floor(Math.log10(Math.abs(number)));
		const exponent3 = Math.floor(exponent / 3);
		const base = number / Math.pow(10, exponent3 * 3);
		return base.toFixed(3) + 'e' + (exponent3 * 3) + ' ' + unit;
	}
}

function clamp(value, min, max) {
	if (value < min) {
		return min;
	} else if (value > max) {
		return max;
	} else {
		return value;
	}
}

function distribution(array) {
	const total = sum(array);
	return array.map($ => $ / total);
}

function withoutKey(object, key) {
	const { [key]: unused, ...rest } = object;
	return rest;
}

function join(array, callable) {
	return array.slice(1).reduce((accumlator, current) => callable(accumlator, current), array[0]);
}

function sum(array) {
	return join(array, (a, b) => a + b);
}

function isDefined(param) {
	return param !== undefined;
}

const stateMars = {
	t: 276,
	p: 651.8,
};

// https://en.wikipedia.org/wiki/Albedo
// albedo 0 = full absorption 1 = full reflection

const stateEarth = {
	name: 'Earth',
	t: 287, // [K]
	tAtmosphere: 0, // due to greenhouse gases (CO2) [K]
	distanceSun: 150e9, // [m]
	albedoBond: 0.306, // [-]
	r: 6371e3, // radius [m]
	m: 5.972e24, // mass [m]
	h2o: 1300e15, // water [m3]
	co2: 5.24e18 * 417e-6, // co2 [kg]
	o2: 5.24e18 * 209460e-6, // oxygen concentration [kg]
	n: 5.24e18 * 780840e-6, // nitrogen concentration [kg]
	b: 40e-6, // [T]
};

// outside this range it is considered to be too harsh too survive (despite not
// necessarily being deadly)
const comfortableLimitAnimal = {
	t: { perfect: 293, range: 15 },
	p: { min: 0.4e5, max: 1.6e5, perfect: 1e5, range: 0.4e-5 },
	h2oLevel: { perfect: 2500, range: 100 },
	co2rel: { perfect: 400e-6, range: 100e-6 },
	o2rel: { perfect: 0.2, range: 0.05 },
};

const plants = {
	tp0: { name: 'Temperature Positive 0', power: 0, buildTime: 0, t: 0.05 },
	tn0: { name: 'Temperature Negative 0', power: 0, buildTime: 0, t: -0.01 },
	wp0: { name: 'Water Positive 0', power: 0, buildTime: 0, w: 1e16 },
	wn0: { name: 'Water Negative 0', power: 0, buildTime: 0, w: 0.0 },
	co2p0: { name: 'CO2 Positive 0', power: 0, buildTime: 0, co2: 40e12 },
	co2n0: { name: 'CO2 Negative 0', power: 0, buildTime: 0, co2: -1e-6 },
	o2p0: { name: 'O2 Positive 0', power: 0, buildTime: 0, o2: 0.0 },
	o2n0: { name: 'O2 Negative 0', power: 0, buildTime: 0, o2: 0.0 },
	np0: { name: 'N Positive 0', power: 0, buildTime: 0, n: 0.0 },
	nn0: { name: 'N Negative 0', power: 0, buildTime: 0, n: 0.0 },
	cap0: { name: 'Capacitor 0', power: 0, maxEnergy: 0, buildTime: 0 },
	e0: { name: 'Energy 0', power: 0, buildTime: 0 },
};

let state = {
	...stateEarth,
	count: 0,
	time: 0,
	buildSelected: Object.keys(plants)[0],
	plants: ['tp0', 'tp0', 'tn0', 'tn0', 'co2p0', 'wp0'],
};

function calculate(state) {
	const sunTemperature = 5778; // [K]
	const sunRadius = 6.957e8; // [m]

	const solarConstant = physics.solarConstant(sunRadius, sunTemperature, state.distanceSun);

	const sunPlanetPower = solarConstant * physics.circleArea(state.r);

	const absorbedPower = (1.0 - state.albedoBond) * sunPlanetPower;

	const heatingPower = absorbedPower;
	// TODO simplified model which should include CO2, pollutions and other factors and not being linear!
	const greenhouseEffect = 0; // absorbedPower * 0.6; 
	const power = heatingPower + greenhouseEffect;

	const planetBlackBodyTemperature = physics.blackBodyTemperature(state.r, power);

	const g = physics.graviation(state.m, state.r);
	const planetSurface = physics.sphereSurface(state.r);
	const planetVolume = physics.sphereVolume(state.r);

	const atmosphereMass = state.co2 + state.o2 + state.n;
	const atmosphereForce = atmosphereMass * g; // f = m * a
	const atmospherePressure = atmosphereForce / planetSurface;

	// https://en.wikipedia.org/wiki/Triple_point#/media/File:Phase_diagram_of_water.svg
	const meltTemperature = physics.celsius2kelvin(0);
	const boilTemperature = physics.celsius2kelvin(100);

	// trivialized model which assumes that part of the planet are colder/hotter
	const overlap = (boilTemperature - meltTemperature) * 0.15; // factor is not allowed to be bigger than 0.5
	const meltA = -1.0 / overlap;
	const meltC = 1.0 - (meltTemperature * meltA);
	const icePercent = clamp(state.t * meltA + meltC, 0.0, 1.0);
	const boilA = 1.0 / overlap;
	const boilC = 1.0 - (boilTemperature * boilA);
	const vaporPercent = clamp(state.t * boilA + boilC, 0.0, 1.0);

	// v(r + h2oLevel) - v(r)  = vwater
	const h2oLevel = physics.sphereRadius(state.h2o + planetVolume) - state.r;

	const [co2rel, o2rel, nrel] = distribution([state.co2, state.o2, state.n]);

	const td = sum(state.plants.map($ => plants[$].t).filter(isDefined));
	const wd = sum(state.plants.map($ => plants[$].w).filter(isDefined));
	const co2d = sum(state.plants.map($ => plants[$].co2).filter(isDefined));

	return { g, planetSurface, planetVolume, solarConstant, planetBlackBodyTemperature, atmosphereMass, atmospherePressure, h2oLevel, co2rel, o2rel, nrel, td, wd, co2d, icePercent, vaporPercent };
}

function viewButton(text, event) {
	return m('button', { onClick: () => signal(event) }, text);
}

function viewSlider(min, max, value) {
	return m('input', { type: 'range', min: min, max: max, value: value, disabled: true });
}

function viewRowText(...texts) {
	return m('tr', {}, texts.map($ => m('td', {}, $)));
}

function viewP(...items) {
	return m('p', {}, items);
}

function viewSelect(options, xxx) {
	return m('select', { onChange: (e) => signal(options[e.target.selectedIndex][2]) },
		options.map($ => m('option', { selected: $[0] === xxx }, $[1]))
	);
}

function view(state) {
	const result = calculate(state);

	return m('div', {}, [
		m('h1', {}, 'Silent Ocean - Terraform'),
		m('h2', {}, state.name),
		m('p', {}, 'Year: ' + state.time),
		m('table', {}, [
			viewRowText('Temperature', toEng(state.t, '°K') + ' / ' + toEng(physics.kelvin2celsius(state.t), '°C')),
			viewRowText('Atmospheric Pressure', toEng(result.atmospherePressure, 'Pa') + ' / ' + toEng(physics.pascal2bar(result.atmospherePressure), 'Bar')),
			viewRowText('Water Mass', toEng(state.h2o / 1e9, 'km3')),
			viewRowText('Water Level', toEng(result.h2oLevel, 'm')),
			viewRowText('Ice %', toEng(100 * result.icePercent, '%')),
			viewRowText('Vapor %', toEng(100 * result.vaporPercent, '%')),
			viewRowText('Radius', toEng(state.r / 1e3, 'km')),
			viewRowText('Surface Area', toEng(result.planetSurface / 1e6, 'km2')),
			viewRowText('Volume', toEng(result.planetVolume / 1e9, 'km3')),
			viewRowText('Atmosphere Mass', toEng(result.atmosphereMass / 1e3, 'Tons')),
			viewRowText('Gravitation', toEng(result.g, 'm/s2')),
			viewRowText('Solar Constant', toEng(result.solarConstant, 'W/m')),
			viewRowText('Black Body Temperature', toEng(result.planetBlackBodyTemperature, '°K') + ' / ' + toEng(physics.kelvin2celsius(result.planetBlackBodyTemperature), '°C')),
			viewRowText('Carbon Dioxide (CO2) Concentration', toEng(state.co2, 'kg') + ' / ' + toEng(100 * result.co2rel, '%')),
			viewRowText('Oxygen (O2) Concentration', toEng(state.o2, 'kg') + ' / ' + toEng(100 * result.o2rel, '%')),
			viewRowText('Nitrogen (N) Concentration', toEng(state.n, 'kg') + ' / ' + toEng(100 * result.nrel, '%')),
			viewRowText('Magnetic Field (Flux Density)', toEng(state.b, 'T')),
		]),
		m('table', {}, state.plants.map($ => viewRowText(plants[$].name, JSON.stringify(withoutKey(plants[$], 'name'))))),
		m('table', {}, [
			viewRowText('Temperature Delta', toEng(result.td, '°K')),
			viewRowText('Water Delta', toEng(result.wd, 'kg')),
			viewRowText('CO2 Delta', toEng(result.co2d, 'kg')),
		]),
		viewP(
			viewSelect(Object.keys(plants).map($ => [$, plants[$].name, { name: 'select', what: $ }]), state.buildSelected),
			state.buildSelected,
			viewButton('Build', { name: 'build' }),
			viewButton('Change Selection', { name: 'cs' }),
		),
		viewP(
			viewSlider(-100, 100, state.count - 50),
		),
		viewP(
			viewButton('Time +1', { name: 'time', amount: 1 }),
			viewButton('Time +10', { name: 'time', amount: 10 }),
			viewButton('Time +100', { name: 'time', amount: 100 }),
		),
		viewButton('Clicked ' + state.count, { name: 'inc' }),
		m('input', { type: 'text' })
	]);
}

function signal(event) {
	state = update(state, event);
	render(document.body, view(state));
}

function update(state, event) {
	if (event.name === 'init') {
		return state;
	} else if (event.name === 'inc') {
		return { ...state, count: state.count + 1 };
	} else if (event.name === 'select') {
		return { ...state, buildSelected: event.what };
	} else if (event.name === 'cs') {
		return { ...state, buildSelected: 'co2p0' };
	} else if (event.name === 'build') {
		return { ...state, plants: state.plants.concat(state.buildSelected) };
	} else if (event.name === 'time') {
		let td = 0;
		let wd = 0;
		let co2d = 0;
		for (let i = 0; i < event.amount; i += 1) {
			const result = calculate(state);
			td += result.td;
			wd += result.wd;
			co2d += result.co2d;
		}
		return {
			...state,
			time: state.time + event.amount,
			t: state.t + td,
			h2o: state.h2o + wd,
			co2: state.co2 + co2d,
		};
	} else {
		console.error('invalid event ', event);
		return state;
	}
	console.error('');
}

function main() {
	signal({ name: 'init' });
}

function timer() {
	signal({ name: 'time', amount: 1 });
}

document.addEventListener('DOMContentLoaded', main);
//window.setInterval(timer, 1000);
