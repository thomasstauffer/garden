
export function celsius2kelvin(celsius) {
	return celsius + 273.15;
}

export function kelvin2celsius(kelvin) {
	return kelvin - 273.15;
}

export function pascal2bar(pascal) {
	return pascal * 1e-5;
}

export function circleArea(radius) {
	return (radius ** 2.0) * Math.PI;
}

export function sphereVolume(radius) {
	return (4 / 3) * Math.PI * Math.pow(radius, 3);
}

export function sphereRadius(planetVolume) {
	return Math.pow(planetVolume / ((4 / 3) * Math.PI), 1 / 3);
}

export function sphereSurface(radius) {
	return 4 * Math.PI * Math.pow(radius, 2);
}

export function graviation(mass, radius) {
    return (universalGravitationalConstant * mass) / Math.pow(radius, 2);
}

// https://en.wikipedia.org/wiki/Gravitational_constant
export const universalGravitationalConstant = 6.67408e-11; // [m3 * kg-1 * s-2]

// https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_constant
export const stefanBoltzmannConstant = 5.670367e-8; // [W * m-2 * K-4]

// https://en.wikipedia.org/wiki/Black-body_radiation
// https://en.wikipedia.org/wiki/Luminosity
export function blackBodyLuminosity(radius, temperature) {
	const surface = sphereSurface(radius);
	return stefanBoltzmannConstant * surface * (temperature ** 4.0);
}

export function blackBodyTemperature(radius, power) {
	const surface = sphereSurface(radius);
	return (power / (surface * stefanBoltzmannConstant)) ** (1.0 / 4.0);
}

export function solarConstant(sunRadius, sunTemperature, distanceToSun) {
	const sunPower = blackBodyLuminosity(sunRadius, sunTemperature);
	return sunPower / sphereSurface(distanceToSun);
}
