import * as html from '../crow/html.js';
import { Store } from '../crow/store.js';
import * as view from './view.js';
import * as update from './update.js';

const celestialDefault = {
	name: '?',
	type: '?',
	discovered: false,
	locations: [],
};

const locationDefault = {
	name: '?',
	discovered: false,
	scanned: 0.0, // 0 = None, 1 = Full
	biome: '?',
};

const droneDefault = {
	chassis: '?',
};

const skillDefault = {
	name: '?',
	value: 0,
};

const augmentationDefault = {

};

const characterDefault = {
	age: 0, // seconds
	skills: {},
	augmentations: [],
};

const corporationDefault = {
	name: '?',
	shareCount: 0,
	sharePrice: 0,
};

const researchDefault = {
	name: '?',
	progress: 0, // 0 .. inf
};

function init() {
	return {
		ticks: 0,
		value: 66,
		time: 0,
		info: {
			// TODO just selecteObject/Item
			//what: 'scanner',
			what: 'location',
			id: 'dantu',
		},
		debugMap: {
			selected: 1,
			items: [[0, 1, 1, 0], [1, 1, 1, 1], [undefined, 1, 1, 0]],
		},
		scanner: {
			enabled: true,
		},
		// where the player is (not a selection)
		currentCelestialId: 'ceres',
		currentLocationId: 'sintana',
		items: [
		],
		// generated randomly in the future
		celestials: {
			'vesta': celestialDefault,
			'pallas': celestialDefault,
			'hygiea': { ...celestialDefault, name: 'Hygiea', discovered: true, locations: {} },
			'ceres': {
				...celestialDefault, name: 'Ceres', discovered: true, locations: {
					// https://en.wikipedia.org/wiki/Sintana_(crater)
					'sintana': { name: 'Sintana', biome: 'TODO', discovered: true, scannedPPM: 0, x: 0, y: 0 },
					'dantu': { name: 'Dantu', biome: 'TODO', discovered: true, scannedPPM: 0, x: 1, y: 1 },
				}
			},
			'eris': celestialDefault,
			'pluto': celestialDefault,
			'phobos': celestialDefault,
			'deimos': celestialDefault,
			'venus': celestialDefault,
			'earth': celestialDefault,
			'mars': celestialDefault,
		}
	};
}

function main() {
	const store = new Store(init());
	store.addEventListener(update.update);
	const main = document.getElementsByTagName('main')[0];
	let treeCurrent = html.html('p', 'Hello World');
	main.appendChild(html.toDOM(treeCurrent));

	function dispatch(event) {
		console.log(event);
		store.dispatchEvent(event);
		render();
	}

	function tick() {
		dispatch({ type: 'tick' });
	}

	function render() {
		if (true) {
			const t0 = performance.now();
			const treeNew = view.view(store.getState());
			const t1 = performance.now();
			html.updateDOM(main.children[0], treeCurrent, treeNew, dispatch);
			treeCurrent = treeNew;
			const t2 = performance.now();
			//console.log(t1 - t0, t2 - t1);
		} else {
			const dom = html.toDOM(view(store.getState()), dispatch);
			main.innerHTML = '';
			main.appendChild(dom);
		}
	}

	render();
	window.setInterval(tick, 10 * 1000);
}

window.addEventListener('load', main);
