
// TODO use a custom scheduler which allows time modifications
/*
async function sleepMS(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}
*/

function appendAndScroll(parent, child) {
	parent.appendChild(child);
	parent.scrollTop = parent.scrollHeight;
}

function appendCommand(env, text) {
	const nodeLink = document.createElement('span');
	nodeLink.innerText = '[' + env.commandId + ']';
	nodeLink.id = 'command-' + env.commandId;

	const nodeText = document.createTextNode(' ' + text);

	const node = document.createElement('code');
	node.appendChild(nodeLink);
	node.appendChild(nodeText);
	appendAndScroll(env.node, node);
}

function appendCode(env, text) {
	const nodeLink = document.createElement('a');
	nodeLink.innerText = '[' + env.commandId + ']';
	nodeLink.href = '#command-' + env.commandId;

	const nodeText = document.createTextNode(' ' + text);

	const node = document.createElement('code');
	node.appendChild(nodeLink);
	node.appendChild(nodeText);
	appendAndScroll(env.node, node);
}

function appendError(env, error) {
	const node = document.createElement('p');
	node.className = 'console-error';
	node.innerText = '[' + env.commandId + '] ' + error;
	appendAndScroll(env.node, node);
}

function appendText(env, text) {
	const node = document.createElement('p');
	node.innerText = '[' + env.commandId + '] ' + text;
	appendAndScroll(env.node, node);
}

function appendImage(env, url) {
	const nodeLink = document.createElement('a');
	nodeLink.innerText = '[' + env.commandId + ']';
	nodeLink.href = '#command-' + env.commandId;

	const nodeImage = document.createElement('img');
	nodeImage.src = url;
	nodeImage.width = 100;
	nodeImage.height = 100;

	const node = document.createElement('div');
	node.appendChild(nodeLink);
	node.appendChild(nodeImage);

	appendAndScroll(env.node, node);
}

async function ping(env, args) {
	function parseIntDefault(string, def = 0.0) {
		const n = parseInt(string);
		return isNaN(n) ? def : n;
	}

	const addr = args[0] ?? '0.0.0.0';
	const n = parseIntDefault(args[1], 5);
	for (let seq = 0; seq < n; seq += 1) {
		await env.scheduler.sleepMS(1000);
		const text = `(from "${addr}") (bytes 64) (seq ${seq}) (ttl 64) (time-ms 0.1)\n`;
		appendCode(env, text);
	}
}

async function scan(env, _args) {
	appendCode(env, 'Scanning ...');
	await env.scheduler.sleepMS(1000);
	appendCode(env, 'Scanned');
}

async function help(env, _args) {
	appendCode(env, 'help | ping | ...');
}

async function rsh(env, _args) {
	await env.scheduler.sleepMS(10);
}

async function demo(env, _args) {
	appendCode(env, 'code');
	appendText(env, 'text');
	appendImage(env, 'test-item-1.png');
}

async function speed(env, args) {
	const factor = parseFloat(args[0]);
	appendCode(env, factor);
	env.scheduler.speed = factor;
}

function cmd(output, string) {
	const tokens = string.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').split(/ /g).filter(value => value !== '');

	const command = tokens[0] ?? '';
	const args = tokens.slice(1);

	const commands = {
		'help': help,
		'rsh': rsh,
		'demo': demo,
		'ping': ping,
		'scan': scan,
		'speed': speed,
	};

	const network = {
		nodes: {
			'1.1': { routes: ['1.2', '1.3', '1.4', '2.1'] },
			'1.2': { routes: ['1.1'] },
			'1.3': { routes: ['1.1'] },
			'1.4': { routes: ['1.1'] },
			'2.1': { routes: ['2.2', '2.3', '3.1'] },
			'2.2': { routes: ['2.1'] },
			'2.3': { routes: ['2.1'] },
			'3.1': { routes: ['3.2', '2.1'] },
			'3.2': { routes: ['3.1'] },
		}
	};

	const world = {
		network: network
	};

	// TODO use a lispalike interpreter

	appendCommand(output, string);
	const callable = commands[command];
	if (callable !== undefined) {
		callable(output, args);
	} else {
		appendError(output, 'Unknown Command');
	}
}

class Scheduler {
	constructor() {
		const intervalMilliseconds = 100;

		this.t = 0.0;
		this.speed = 1.0;
		this.tasks = [];

		const schedule = () => {
			this.t += this.speed * (intervalMilliseconds / 1000.0);
			//console.log(`scheduler t=${this.t} #tasks=${this.tasks.length}`,);

			for (let n = 0; n < 100; n += 1) {
				if (this.tasks.length === 0) {
					break;
				}
				if (this.t < this.tasks[0][0]) {
					break;
				}
				const task = this.tasks.shift();
				const resolve = task[1];
				resolve();
			}
		};

		window.setInterval(schedule, intervalMilliseconds);
	}

	async sleepMS(ms) {
		function insertAt(array, index, element) {
			return [...array.slice(0, index), element, ...array.slice(index)];
		}

		const addTask = (s, resolve) => {
			const t = this.t + s;
			let index = this.tasks.length;
			for (let i = 0; i < this.tasks.length; i += 1) {
				if (t < this.tasks[i][0]) {
					index = i;
					break;
				}
			}
			this.tasks = insertAt(this.tasks, index, [t, resolve]);
		};

		return new Promise((resolve) => addTask(ms / 1000.0, resolve));
	}
}

async function testScheduler(scheduler) {
	const p1 = scheduler.sleepMS(1000);
	const p3 = scheduler.sleepMS(3000);
	const p2 = scheduler.sleepMS(2000);

	await p1;
	console.log('1');
	await p2;
	console.log('2');
	await p3;
	console.log('3');
}

function main() {
	const nodeInput = document.getElementById('console-input');
	const nodeOutput = document.getElementById('console-output');
	nodeInput.focus();

	const scheduler = new Scheduler();
	scheduler.speed = 1.0;
	setTimeout(() => testScheduler(scheduler), 0);

	let commandId = 0;
	nodeInput.addEventListener('keypress', (event) => {
		if (event.key === 'Enter') {
			event.preventDefault();
			const output = { node: nodeOutput, commandId: commandId, scheduler: scheduler };
			cmd(output, nodeInput.value);
			nodeInput.value = '';
			commandId += 1;
		}
	});
}

window.addEventListener('load', main);
