import * as q from '../crow/q.js';

/*
TODO split update

- split by event or by subpart of model? or event and subtype?
- a sub function quite likely needs access to the full model

*/
export function update(model, event) {
	if (event.type === 'debug') {
		console.log(model);
		return model;
	} else if (event.type === 'select') {
		console.log('select', event.index);
		return { ...model, debugMap: { ...model.debugMap, selected: event.index } };
	} else if (event.type === 'info') {
		return { ...model, info: { what: event.what, id: event.id } };
	} else if (event.type === 'scannerOnOff') {
		return { ...model, scanner: { ...model.scanner, enabled: !model.scanner.enabled } };
	} else if ((event.type === 'inc') || (event.type === 'dec')) {
		const add = event.type === 'inc' ? 1 : -1;
		return { ...model, value: q.clamp(model.value + add, 0, 100) };
	} else if (event.type === 'tick') {
		// just a silly idea
		function part(whole, selection, key, update) {
			let s = selection(whole);
			s[key] = update(s[key]);
			return whole;
		}

		const model2 = part(model,
			(model) => model.celestials[model.currentCelestialId].locations[model.currentLocationId], 'scannedPPM',
			(scannedPPM) => q.clamp(scannedPPM + 10000, 0, 1000000),
		);
		return {
			...model2,
			ticks: model.ticks + 1,
			time: model.time + 1,
		};
		/*
		const currentCelestial = model.celestials[model.currentCelestialId];
		const currentLocation = currentCelestial.locations[model.currentLocationId];
		const scannedPPM = q.clamp(currentLocation.scannedPPM + 10000, 0, 1000000);
		return {
			...model,
			ticks: model.ticks + 1,
			time: model.time + 1,
			celestials: {
				...celestials,
				[model.currentCelestialId]: {
					...currentCelestial,
					locations: {
						...currentCelestial.locations,
						[model.currentLocationId]: {
							...currentLocation,
							scannedPPM: scannedPPM
						}
					}
				}
			},
		};
		*/
	} else {
		console.log('Unknown Event', event);
		return model;
	}
}
