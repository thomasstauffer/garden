import * as q from '../crow/q.js';
import * as html from '../crow/html.js';

function viewWindow(caption, ...views) {
	return html.html('div', { 'class': 'window' },
		html.html('h2', caption),
		...views
	);
}

function viewProgress(percent) {
	return html.html('div', { 'class': 'progress' },
		html.html('div', { 'style': 'width: ' + percent + '%;' }, percent + '%')
	);
}

function viewButton(text, event) {
	return html.html('button', { 'onclick': event }, text);
}

function viewUpDown(textUp, eventUp, textDown, eventDown) {
	return html.html('div',
		html.html('button', { 'onclick': eventUp }, textUp),
		html.html('button', { 'onclick': eventDown }, textDown),
	);
}

function viewRadio(child, selected, event) {
	const checked = selected ? { 'checked': 'checked' } : {};
	return html.html('div',
		html.html('input', { 'type': 'radio', 'onclick': event }, checked),
		child
	);
}

function viewMap(library, map) {
	const nx = map.items[0].length;
	const ny = map.items.length;
	const l = 100;
	const w = nx * l;
	const h = ny * l;

	const event = { type: 'inc' };

	const tiles = q.unzip(q.flatten(q.map(map.items, (row, r) => {
		return q.map(row, (col, c) => {
			const item = map.items[r][c];
			const x = c * l;
			const y = r * l;
			const image = item !== undefined ? html.svg('image', { 'href': library[item].url, 'x': x, 'y': y, 'width': l, 'height': l }) : html.svg('g');
			const rect = html.svg('rect', { 'x': x, 'y': y, 'width': l, 'height': l });
			const circle = html.svg('circle', { 'cx': x + (l / 2), 'cy': y + (l / 2), 'r': l / 4, 'onclick': event });
			return [image, rect, circle];
		});
	})));
	const [images, rects, circles] = tiles;

	//const vLines = q.times(nx - 1, $ => html.svg('line', {x1: ($ + 1) * l, y1: 0, x2: ($ + 1) * l, y2: h, 'stroke-width': 5, stroke: '#909'}));
	//const hLines = q.times(ny - 1, $ => html.svg('line', {x1: 0, y1: ($ + 1) * l, x2: h, y2: ($ + 1) * l, 'stroke-width': 5, stroke: '#909'}));

	return html.html('div',
		html.html('div', { 'class': 'choose' },
			...q.map(library, (item, index) => {
				const event = { 'type': 'select', 'index': index };
				return viewRadio(html.html('img', { 'src': item.url, 'onclick': event }), index === map.selected, event);
			})
		),
		html.svg('svg', { width: w, height: h },
			//... hLines,
			//... vLines,
			...images,
			...rects,
			...circles
		),
	);
}

function viewGadget(name, enabled, event) {
	const checked = enabled === true ? { 'checked': 'checked' } : {};
	return html.html('div', { 'class': 'gadget' },
		html.html('input', { 'type': 'checkbox', 'onclick': event, ...checked }),
		html.html('div', name),
	);
}

function viewProperties(...properties) {
	return html.html('div', { 'class': 'properties' },
		...properties
	);
}

function viewShowInfo(name, event) {
	return html.html('button', { 'onclick': event }, name);
}

function viewScanner(scanner) {
	const event = { type: 'scannerOnOff' };
	const checked = scanner.enabled === true ? { 'checked': 'checked' } : {};
	return html.html('div',
		html.html('h3', 'Scanner'),
		html.html('input', { 'type': 'checkbox', 'onclick': event, ...checked }),
	);
}

function viewCelestial(celestial) {
	return html.html('div',
		html.html('h3', 'Celestial'),
		viewProperties(
			t('Name'), t(celestial.name),
			t('Temperature [K]'), t(0),
			t('Atmospheric Pressure [Pa]'), t(0),
			t('Atmospheric O'), t(0), // 0.15bar < 0.12bar unconsicouness, fire risk big 0 concentration (e.g. aluminium)
			t('Atmospheric CO2 = Carbon Dioxide [ppm]'), t(0),
			t('H2O = Water [m3]'), t(0),
			t('Marine Biomass [kg]'), t(0),
			t('Terrestrial Biomass [kg]'), t(0),
		),
	);
}

function viewLocation(location) {
	return html.html('div',
		html.html('h3', 'Location'),
		viewProperties(
			t('Name'), t(location.name),
			t('Coordinates'), t(location.x + ' / ' + location.y),
			t('Scanned'), t((location.scannedPPM / 10000) + '%'),
		)
	);
}

function viewInfo(model) {
	return {
		'scanner': () => viewScanner(model.scanner),
		'celestial': () => viewCelestial(model.celestials[model.info.id]),
		'location': () => viewLocation(model.celestials[model.currentCelestialId].locations[model.info.id]),
	}[model.info.what]();
}

function t(text) {
	return html.html('span', text);
}

export function view(model) {
	return html.html('div',
		viewWindow('Summary',
			viewProperties(
				t('Time [s]'), t(model.time),
				t('Power Consumption [W]'), t(0),
				t('Energy [Wh] Current/Max'), t(0 + ' / ' + 0),
				t('C = Carbon [kg]'), t(0),
				t('Cu = Copper [kg]'), t(0),
				t('Fe = Iron [kg]'), t(0),
				t('Si = Silicon [kg]'), t(0),
				t('Au = Gold [kg]'), t(0),
				t('Li = Lithium [kg]'), t(0),
				t('U = Uranium [kg]'), t(0),
				t('N = Nitrogen [kg]'), t(0),
				t('O = Oxygen [kg]'), t(0),
				t('H = Hydrogen [kg]'), t(0),
			),
		),
		viewWindow('Character',
			viewProperties(
				t('Current Location'), t(model.celestials[model.currentCelestialId].name + ' / ' + model.celestials[model.currentCelestialId].locations[model.currentLocationId].name),
				t('Blood [l]'), t('5'),
				t('Epinephrine/Adrenaline [ng/l]'), t('10'),
				t('Serotonine [ng/l]'), t('200'),
				/*
				t('Endorphine'), t('???'),
				t('Dopamine'), t('???'),
				t('Noradrenaline'), t('???'),
				t('Melatonin'), t('???'),
				*/
			),
		),
		viewWindow('Items',
			viewShowInfo('Scanner', { type: 'info', what: 'scanner' }),
			viewShowInfo('Assembler', { type: 'todo' }),
			viewShowInfo('Disassembler', { type: 'todo' }),
			viewShowInfo('Storage Solid', { type: 'todo' }),
			viewShowInfo('Storage Liquid', { type: 'todo' }),
			viewShowInfo('Storage Gas', { type: 'todo' }),
		),
		viewWindow('Info',
			viewInfo(model)
		),
		viewWindow('Location',
			viewLocation(model.celestials[model.currentCelestialId].locations[model.currentLocationId])
		),
		viewWindow('Map',
			...Object.values(q.map(q.filter(model.celestials[model.currentCelestialId].locations, $ => $.discovered), (location, id) => {
				return viewShowInfo(location.name, { type: 'info', what: 'location', id: id });
			})),
		),
		viewWindow('Universe',
			...Object.values(q.map(q.filter(model.celestials, $ => $.discovered), (celestial, id) => {
				return viewShowInfo(celestial.name, { type: 'info', what: 'celestial', id: id });
			})),
		),
		viewWindow('Debug',
			html.html('input', { 'type': 'text' }),
			html.html('p', model.ticks),
			viewButton('Debug', { type: 'debug' }),
			viewButton('Tick', { type: 'tick' }),
			viewButton('+1', { type: 'inc' }),
			viewProgress(model.value),
			viewUpDown('+', { type: 'inc' }, '-', { type: 'dec' }),
			viewMap([{ url: 'test-item-0.png', size: 1 }, { url: 'test-item-1.png', size: 1 }, { url: 'test-item-1.png', size: 2 }], model.debugMap),
		),
	);
}
