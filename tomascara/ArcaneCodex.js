import * as q from '../crow/q.js';

export function modifyObject(objParam) {
	function merge() {
		const args = q.map(Array.from(arguments), x => x === undefined ? {} : x);
		return q.merge(args, items => q.map(items, x => x === undefined ? 0 : x));
	}
	
	const obj = objParam;
	const result = {};

	const KLASSEN = {
		Troll: {
			Groessenklasse: 'Gross (G)',
			AngriffVerteidigungModifikator: -1,
			EigenschaftenGP: 40,
			CP: 30,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 5, GehenMarsch: 10, GehenSprint: 40, Schwimmen: 5, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 4, Konstitution: 4, Geschicklichkeit: 2, Wahrnehmung: 2, Intelligenz: 1, Willenskraft: 2, Charisma: 1, },
			EigenschaftenMax: { Staerke: 12, Konstitution: 12, Geschicklichkeit: 10, Wahrnehmung: 10, Intelligenz: 8, Willenskraft: 10, Charisma: 8, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Fee: {
			Groessenklasse: 'Winzig (W)',
			AngriffVerteidigungModifikator: 2,
			EigenschaftenGP: 38,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 1, GehenMarsch: 2, GehenSprint: 8, Schwimmen: 0.5, FliegenKampf: 3, FliegenMarsch: 5, FliegenSprint: 20, },
			EigenschaftenMin: { Staerke: 1, Konstitution: 1, Geschicklichkeit: 4, Wahrnehmung: 3, Intelligenz: 2, Willenskraft: 2, Charisma: 3, },
			EigenschaftenMax: { Staerke: 5, Konstitution: 6, Geschicklichkeit: 12, Wahrnehmung: 11, Intelligenz: 10, Willenskraft: 9, Charisma: 11, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1-2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Arborim: {
			Groessenklasse: 'Mittel (M)',
			AngriffVerteidigungModifikator: 0,
			EigenschaftenGP: 40,
			CP: 30,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 4, GehenMarsch: 8, GehenSprint: 32, Schwimmen: 4, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 2, Konstitution: 2, Geschicklichkeit: 3, Wahrnehmung: 3, Intelligenz: 3, Willenskraft: 2, Charisma: 3, },
			EigenschaftenMax: { Staerke: 9, Konstitution: 10, Geschicklichkeit: 11, Wahrnehmung: 10, Intelligenz: 10, Willenskraft: 9, Charisma: 11, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Ork: {
			Groessenklasse: 'Mittel (M)',
			AngriffVerteidigungModifikator: 0,
			EigenschaftenGP: 40,
			CP: 30,
			SRBonus: 2,
			Geschwindigkeit: { GehenKampf: 4, GehenMarsch: 8, GehenSprint: 32, Schwimmen: 4, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 3, Konstitution: 3, Geschicklichkeit: 2, Wahrnehmung: 3, Intelligenz: 2, Willenskraft: 2, Charisma: 2, },
			EigenschaftenMax: { Staerke: 11, Konstitution: 11, Geschicklichkeit: 10, Wahrnehmung: 10, Intelligenz: 9, Willenskraft: 10, Charisma: 9, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Mensch: {
			Groessenklasse: 'Mittel (M)',
			AngriffVerteidigungModifikator: 0,
			EigenschaftenGP: 40,
			CP: 40,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 4, GehenMarsch: 8, GehenSprint: 32, Schwimmen: 4, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 3, Konstitution: 3, Geschicklichkeit: 3, Wahrnehmung: 3, Intelligenz: 3, Willenskraft: 3, Charisma: 3, },
			EigenschaftenMax: { Staerke: 10, Konstitution: 10, Geschicklichkeit: 10, Wahrnehmung: 10, Intelligenz: 10, Willenskraft: 10, Charisma: 10, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Morai: {
			Groessenklasse: 'Mittel (M)',
			AngriffVerteidigungModifikator: 0,
			EigenschaftenGP: 40,
			CP: 30,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 4, GehenMarsch: 8, GehenSprint: 32, Schwimmen: 4, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 2, Konstitution: 2, Geschicklichkeit: 3, Wahrnehmung: 3, Intelligenz: 3, Willenskraft: 3, Charisma: 2, },
			EigenschaftenMax: { Staerke: 9, Konstitution: 9, Geschicklichkeit: 11, Wahrnehmung: 11, Intelligenz: 10, Willenskraft: 10, Charisma: 10, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
		Sidhe: {
			Groessenklasse: 'Mittel (M)',
			AngriffVerteidigungModifikator: 0,
			EigenschaftenGP: 40,
			CP: 30,
			SRBonus: 0,
			Geschwindigkeit: { GehenKampf: 4, GehenMarsch: 8, GehenSprint: 32, Schwimmen: 4, FliegenKampf: 0, FliegenMarsch: 0, FliegenSprint: 0, },
			EigenschaftenMin: { Staerke: 2, Konstitution: 2, Geschicklichkeit: 3, Wahrnehmung: 3, Intelligenz: 3, Willenskraft: 3, Charisma: 3, },
			EigenschaftenMax: { Staerke: 9, Konstitution: 9, Geschicklichkeit: 11, Wahrnehmung: 11, Intelligenz: 10, Willenskraft: 10, Charisma: 11, },
			NahkampfFaustTritt: { Name: 'Faust/Tritt', Schaden: '1W10/2B', Mindeststaerke: 0, Groessenklasse: 0, Gewicht: 0, Preis: 0, },
		},
	};

	const KLASSE = KLASSEN[obj.Rasse];
	
	result.Groessenklasse = KLASSE.Groessenklasse;

	const belastung = q.find(obj.Ruestungen, ruestung => ruestung.Name == obj.Ruestung).Belastung;

	result.Belastung = belastung;
	result.Rasse = obj.Rasse;
	result.Groesse = obj.Groesse;
	result.Name = obj.Name;
	result.Alter = obj.Alter;
	result.Gott = obj.Gott;
	
	obj.Nahkampfwaffen = obj.Nahkampfwaffen === undefined ? [] : obj.Nahkampfwaffen;
	obj.Fernkampfwaffen = obj.Fernkampfwaffen === undefined ? [] : obj.Fernkampfwaffen;
	obj.Schilde = obj.Schilde === undefined ? [] : obj.Schilde;
	obj.Gegenstaende = obj.Gegenstaende === undefined ? [] : obj.Gegenstaende;
	obj.Zauber = obj.Zauber === undefined ? [] : obj.Zauber;
	result.Zauber = obj.Zauber;
	
	const schulencpep = merge(obj.SchulenCP, obj.SchulenEP);
	result.SchulenListe = q.sort(q.map(schulencpep, function(cpepstufe, schule) {
		const cpstufe = cpepstufe[0];
		const epstufe = cpepstufe[1];
		const stufe = cpstufe + epstufe;
		return {Name: schule, CPStufe: cpstufe, CP: cpstufe * 8, EPStufe: epstufe, EP: epstufe * 8, Stufe: stufe};
	}), schule => schule.Name);
	result.SchulenCPSumme = q.sum(result.SchulenListe, schule => schule.CP);
	result.SchulenEPSumme = q.sum(result.SchulenListe, schule => schule.EP);
	result.SchulenStufeSumme = q.sum(result.SchulenListe, schule => schule.Stufe);
	
	const EIGENSCHAFTEN = ['Staerke', 'Geschicklichkeit', 'Konstitution', 'Wahrnehmung', 'Intelligenz', 'Willenskraft', 'Charisma'];
	
	result.EigenschaftenGP = {};
	result.EigenschaftenCP = {};
	result.EigenschaftenEP = {};
	result.Eigenschaften = {};
	
	result.EigenschaftenGPSumme = 0;
	result.EigenschaftenCPSumme = 0;
	result.EigenschaftenEPSumme = 0;
	
	q.each(EIGENSCHAFTEN, function(eigenschaft) {
		result.EigenschaftenGP[eigenschaft] = obj.EigenschaftenGP && (eigenschaft in obj.EigenschaftenGP) ? obj.EigenschaftenGP[eigenschaft] : 0;
		result.EigenschaftenCP[eigenschaft] = obj.EigenschaftenCP && (eigenschaft in obj.EigenschaftenCP) ? obj.EigenschaftenCP[eigenschaft] : 0;
		result.EigenschaftenEP[eigenschaft] = obj.EigenschaftenEP && (eigenschaft in obj.EigenschaftenEP) ? obj.EigenschaftenEP[eigenschaft] : 0;
		result.Eigenschaften[eigenschaft] = result.EigenschaftenGP[eigenschaft] + result.EigenschaftenCP[eigenschaft] + result.EigenschaftenEP[eigenschaft];
		result[eigenschaft + 'Bonus'] = result.Eigenschaften[eigenschaft] - 5;
		// TODO unfinished and buggy, creation GP also * 2 (p. 161)
		const twice = KLASSE.EigenschaftenMax[eigenschaft] - 2;
		const cp1 = result.EigenschaftenCP[eigenschaft] * 10;
		const cp2 = Math.max(result.EigenschaftenGP[eigenschaft] + result.EigenschaftenCP[eigenschaft] - twice, 0) * 10;
		const ep1 = result.EigenschaftenEP[eigenschaft] * 10;
		const ep2 = Math.max(result.EigenschaftenGP[eigenschaft] + result.EigenschaftenCP[eigenschaft] - twice, 0) * 10;
		result.EigenschaftenGPSumme += result.EigenschaftenGP[eigenschaft];
		result.EigenschaftenCPSumme += cp1 + cp2;
		result.EigenschaftenEPSumme += ep1 + ep2;
	});
	
	result.StaerkeBonusBelastung = result.StaerkeBonus + belastung;
	result.GeschicklichkeitBonusBelastung = result.GeschicklichkeitBonus + belastung;
	result.KonstitutionBonusBelastung = result.KonstitutionBonus + belastung;
	result.WahrnehmungBonusBelastung = result.WahrnehmungBonus + belastung;
	result.IntelligenzBonusBelastung = result.IntelligenzBonus;
	result.WillenskraftBonusBelastung = result.WillenskraftBonus;
	result.CharismaBonusBelastung = result.CharismaBonus;
	result.Initiative = result.WahrnehmungBonusBelastung;
	result.Verteidigungswert = 14 + result.GeschicklichkeitBonus + result.WahrnehmungBonus + KLASSE.AngriffVerteidigungModifikator;
	result.Schockresistenz = 14 + result.StaerkeBonus + result.KonstitutionBonus + KLASSE.SRBonus;
	result.GeistigerWiderstand = 14 + result.IntelligenzBonus + result.WillenskraftBonus;
	result.Kraftpunkte = result.Eigenschaften['Willenskraft'] + result.SchulenStufeSumme;
	result.Potential = Math.floor(result.Eigenschaften['Willenskraft'] / 2);
	result.Vampir = 'Vampir' in obj;
	result.Blutpunkte = result.Eigenschaften['Willenskraft'] + result.SchulenStufeSumme;
	
	result.Geschwindigkeit = KLASSE.Geschwindigkeit;
	
	result.RuestungenListe = q.sort(obj.Ruestungen, x => x['Name']);
	result.RuestungenPreisSumme = q.sum(result.RuestungenListe, x => x['Preis']);
	result.RuestungenGewichtSumme = q.sum(result.RuestungenListe, x => x['Gewicht']);
	
	result.NahkampfwaffenListe = q.sort(q.map(q.concat(obj.Nahkampfwaffen, KLASSE.NahkampfFaustTritt), function(waffe) {
		waffe.Schaden = waffe.Schaden + ' +' + result.StaerkeBonus;
		return waffe;
	}), x => x['Name']);
	result.NahkampfwaffenPreisSumme = q.sum(result.NahkampfwaffenListe, x => x['Preis']);
	result.NahkampfwaffenGewichtSumme = q.sum(result.NahkampfwaffenListe, x => x['Gewicht']);

	result.FernkampfwaffenListe = q.sort(obj.Fernkampfwaffen, x => x['Name']);
	result.FernkampfwaffenPreisSumme = q.sum(result.FernkampfwaffenListe, x => x['Preis']);
	result.FernkampfwaffenGewichtSumme = q.sum(result.FernkampfwaffenListe, x => x['Gewicht']);
	
	result.SchildeListe = q.sort(obj.Schilde, x => x['Name']);
	result.SchildePreisSumme = q.sum(result.SchildeListe, x => x['Preis']);
	result.SchildeGewichtSumme = q.sum(result.SchildeListe, x => x['Gewicht']);
	
	function anzahl1(o) {
		o.Anzahl = 1;
		return o;
	}
	
	result.Gegenstaende = q.sort(q.concat(obj.Gegenstaende, q.map(obj.Nahkampfwaffen, anzahl1), q.map(obj.Fernkampfwaffen, anzahl1), q.map(obj.Schilde, anzahl1), q.map(obj.Ruestungen, anzahl1)), x => x['Name']);
	result.GegenstaendePreisSumme = q.sum(result.Gegenstaende, gegenstand => gegenstand.Preis * gegenstand.Anzahl);
	result.GegenstaendeGewichtSumme = q.sum(result.Gegenstaende, gegenstand => gegenstand.Gewicht * gegenstand.Anzahl);
	
	const VorzuegeAlle = merge(obj.VorzuegeCP, obj.VorzuegeEP);
	result.VorzuegeListe = q.sort(q.map(VorzuegeAlle, (werte, vorzug) => ({Name: vorzug, CP: werte[0], EP: werte[1]})), vorzug => vorzug.Name);
	result.VorzuegeCPSumme = q.sum(result.VorzuegeListe, vorzug => vorzug.CP);
	result.VorzuegeEPSumme = q.sum(result.VorzuegeListe, vorzug => vorzug.EP);
	
	result.SchwaechenListe = q.sort(q.map(obj.SchwaechenCP, (cp, schwaeche) => ({Name: schwaeche, CP: cp, EP: 0})), schwaeche => schwaeche.Name);
	//result.SchwaechenCPSumme = q.sum(_.toPairs(obj.Schwaechen), 1);
	//result.SchwaechenCPSumme = q.sum(_.toPairs(obj.Schwaechen), item => item[1]);
	result.SchwaechenCPSumme = q.sum(result.SchwaechenListe, schwaeche => schwaeche.CP);
	result.SchwaechenEPSumme = q.sum(result.SchwaechenListe, schwaeche => schwaeche.EP);
	
	const kon = obj.EigenschaftenGP['Konstitution'];
	result.Angeschlagen = ((0 * kon) + 1) + '-' + (1 * kon);
	result.Verletzt = ((1 * kon) + 1) + '-' + (2 * kon);
	result.Verwundet = ((2 * kon) + 1) + '-' + (3 * kon);
	result.SchwerVerwundet = ((3 * kon) + 1) + '-' + (4 * kon);
	result.AusserGefecht = ((4 * kon) + 1) + '-' + (5 * kon);
	result.Komma = ((5 * kon) + 1) + '-' + (6 * kon);
	result.Tod = ((6 * kon) + 1) + '+';
	const sw = q.find(result.VorzuegeListe, o => o.Name.startsWith('Schmerzwiderstand'));
	const swwert = obj.Vampir ? 12 : (sw === undefined ? 0 : sw.CP + sw.EP);
	result.MalusVerletzt = {0: -2, 4: 0, 8: 0, 12: 0}[swwert];
	result.MalusVerwundet = {0: -4, 4: -2, 8: 0, 12: 0}[swwert];
	result.MalusSchwerVerwundet = {0: -6, 4: -4, 8: -2, 12: 0}[swwert];

	const GENERISCHE_FERTIGKEITEN_EIGENSCHAFT = { 'Aufmerksamkeit': 'Wa', 'Ausdauer': 'Kon', 'Ausweichen': 'Wa', 'Bauchreden': 'Cha', 'Beeindrucken': 'Cha', 'Beruf Spezialisierung': 'Int', 'Einschüchtern': 'Will', 'Empathie': 'Cha', 'Entfesseln': 'Ge', 'Etikette': 'Int', 'Fallen': 'Ge', 'Fälschen': 'Ge', 'Fliegen': 'Kon', 'Foltern': 'Will', 'Führung': 'Cha', 'Gassenwissen': 'Int', 'Geschichten Erzählen': 'Cha', 'Gesten Verbergen': 'Spezial', 'Handwerk': 'Ge', 'Hypnose': 'Cha', 'Klettern': 'Ge', 'Kraftakt': 'St', 'Lehren': 'Cha', 'Lippen Lesen': 'Wa', 'Lesen/Schreiben': 'Int', 'Malen': 'Wa', 'Meditation': 'Will', 'Mode': 'Int', 'Musizieren Spezialisierung': 'Wa', 'Nachforschen': 'Int', 'Orientierung': 'Wa', 'Reiten': 'Ge', 'Rennen': 'Kon', 'Rhetorik': 'Cha', 'Schätzen': 'Int', 'Schauspielern': 'Cha', 'Schiffsführung': 'Int', 'Schleichen': 'Ge', 'Schlösser Öffnen': 'Ge', 'Schwimmen': 'St', 'Seemannschaft': 'Ge', 'Singen': 'Cha', 'Spiele': 'Int', 'Springen': 'St', 'Spuren Lesen': 'Wa', 'Taktik': 'Int', 'Tanzen': 'Ge', 'Taschendiebstahl': 'Ge', 'Tiere Abrichten & Pflegen': 'Cha', 'Turnen': 'Ge', 'Überleben': 'Kon', 'Verbergen': 'Ge', 'Verhandeln/Feilschen': 'Will', 'Verkleiden': 'Wa', 'Verspotten': 'Cha', 'Wagen Lenken': 'Will', 'Wissen': 'Int', 'Wunden Behandeln': 'Ge', 'Zechen': 'Kon', 'Ortskunde': 'Int' };

	const KAMPF_FERTIGKEITEN_EIGENSCHAFT = { 'Armbrust': 'Wa', 'Äxte': 'St', 'Belagerungsgeräte': 'St', 'Blasrohr': 'Wa', 'Blind Kämpfen': 'Spezial', 'Bogen': 'Ge', 'Dolche': 'Ge', 'Fechtwaffen': 'Ge', 'Geschütze': 'Wa', 'Improvisierte Waffen': 'Ge', 'Kettenwaffen': 'Ge', 'Kriegshandwerk': 'Kon', 'Lanzen': 'St', 'Lasso': 'Ge', 'Peitsche': 'Ge', 'Schilde': 'St', 'Schleuder': 'Ge', 'Schwarzpulverwaffen': 'Wa', 'Schwerter': 'St', 'Speere & Stäbe': 'Ge', 'Stangenwaffen': 'Ge', 'Stumpfe Hiebwaffen': 'St', 'Waffenloser Kampf': 'Ge', 'Zweihandäxte': 'Spezial' };
	
	const FERTIGKEITEN_EIGENSCHAFT = Object.assign(GENERISCHE_FERTIGKEITEN_EIGENSCHAFT, KAMPF_FERTIGKEITEN_EIGENSCHAFT);
	const FERTIGKEITEN_BONUS = {'St': result.StaerkeBonusBelastung, 'Ge': result.GeschicklichkeitBonusBelastung, 'Kon': result.KonstitutionBonusBelastung, 'Wa': result.WahrnehmungBonusBelastung, 'Int': result.IntelligenzBonusBelastung, 'Will': result.WillenskraftBonusBelastung, 'Cha': result.CharismaBonusBelastung, 'Spezial': 0};
	
	function removeHint(value) {
		return value.replace(/:.*/, '').trim(); // remove everything after : 'Ortskunde: Kreijor' -> 'Ortskunde'
	}
	
	const fertigkeitenAlle = merge(obj.FertigkeitenFrei, obj.FertigkeitenGP, obj.FertigkeitenCP, obj.FertigkeitenEP);
	result.FertigkeitenListe = q.sort(q.map(fertigkeitenAlle, function(werte, fertigkeit) {
		const fertigkeitBasis = removeHint(fertigkeit);
		const name = fertigkeit + ' ' +' (' + FERTIGKEITEN_EIGENSCHAFT[fertigkeitBasis] + ')';
		const freiwert = werte[0];
		const angriffsbonus = fertigkeit in KAMPF_FERTIGKEITEN_EIGENSCHAFT ? KLASSE.AngriffVerteidigungModifikator : 0;
		const gpwert = werte[1];
		const gp = gpwert + Math.max(gpwert - 5, 0);
		const cpwert = werte[2];
		const cp = cpwert + gpwert + Math.max(cpwert + gpwert - 5, 0) - gp;
		const epwert = werte[3];
		const ep = cpwert + gpwert + epwert + Math.max(cpwert + gpwert + epwert - 5, 0) - cp - gp;
		const wert = freiwert + angriffsbonus + gpwert + cpwert + epwert + FERTIGKEITEN_BONUS[FERTIGKEITEN_EIGENSCHAFT[fertigkeitBasis]];
		
		return {Name: name, FreiWert: freiwert, Angriffsbonus: angriffsbonus, GP: gp, GPWert: gpwert, CP: cp, CPWert: cpwert, EP: ep, EPWert: epwert, Wert: wert};
	}), fertigkeit => fertigkeit.Name);
	result.FertigkeitenGPSumme = q.sum(result.FertigkeitenListe, x => x['GP']);
	result.FertigkeitenCPSumme = q.sum(result.FertigkeitenListe, x => x['CP']);
	result.FertigkeitenEPSumme = q.sum(result.FertigkeitenListe, x => x['EP']);
	
	result.NotizenListe = obj.Notizen.trim().split('\n');
	
	result.ErfahrungspunkteListe = q.map(obj.Erfahrungspunkte, function(anzahl, datum) {
		return {Datum: datum, Anzahl: anzahl};
	});
	result.ErfahrungspunkteSumme = q.sum(result.ErfahrungspunkteListe, x => x['Anzahl']);

	result.SummeCP = result.VorzuegeCPSumme + result.FertigkeitenCPSumme + result.EigenschaftenCPSumme + result.SchulenCPSumme - result.SchwaechenCPSumme;
	result.SummeEP = result.VorzuegeEPSumme + result.FertigkeitenEPSumme + result.EigenschaftenEPSumme + result.SchulenEPSumme;
	
	result.SummePreis = result.GegenstaendePreisSumme + result.NahkampfwaffenPreisSumme + result.FernkampfwaffenPreisSumme + result.RuestungenPreisSumme;
	
	result.Debug = false;
	
	result.Datum = new Date().toISOString();
	
	return result;
}

