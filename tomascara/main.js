/* global doT */

import * as q from '../crow/q.js';
import * as toml from '../crow/toml.js';
import * as ac from './ArcaneCodex.js';

async function read(url) {
	const response = await fetch(url);
	const text = await response.text();
	q.assertEqual(response.status, 200);
	return text;
}

function makeOutput(template, data) {
	const input = toml.parse(data);
	const output = ac.modifyObject(input);
	const tempplateFunction = doT.template(template);
	const html = tempplateFunction(output);
	return html;
}

function addStylesheet(url) {
	const nodeLink = document.createElement('link');
	nodeLink.setAttribute('rel', 'stylesheet');
	nodeLink.setAttribute('type', 'text/css');
	nodeLink.setAttribute('href', url);
	document.getElementsByTagName('head')[0].appendChild(nodeLink);
}

async function main() {
	const FILES = ['arcane-codex-hero.toml', 'arcane-codex-hero.toml'];

	const templateHTML = await read('TemplateArcaneCodex.html');
	//const templateCSS = await read('TemplateArcaneCodex.css');
	addStylesheet('TemplateArcaneCodex.css');

	const datas = await Promise.all(FILES.map($ => read($)));

	const html = datas.reduce((acc, data) => {
		return acc + makeOutput(templateHTML, data);
	}, '');

	const outputNode = document.getElementById('output');
	outputNode.innerHTML = html;
	//const scriptNode = document.getElementById('script');
}

window.addEventListener('load', main);
