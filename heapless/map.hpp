#pragma once

#include "vector.hpp"

namespace heapless {

template <typename K, typename V, size_t N>
class map {
public:
	using key_type = K;
	using mapped_type = V;

	explicit map() {
	}

	~map() {
		clear();
	}

	constexpr size_t capacity() const {
		return N;
	}

	void clear() {
		_lookup.clear();
		_keys.clear();
		_values.clear();
	}

	bool empty() const {
		return _keys.empty();
	}

	V& operator[](const K& key) {
		const size_t size = _lookup.size();

		// TODO no linear search
		for (size_t i = 0; i < size; i += 1) {
			if (_keys[_lookup[i]] == key) {
				return _values[_lookup[i]];
			}
		}

		// TODO fix, at the moment assumes key are insert in sorted order
		_lookup.push_back(size);
		_keys.push_back(key);
		_values.push_back(V());
		return _values[size];
	}

private:
	heapless::vector<size_t, N> _lookup;
	heapless::vector<K, N> _keys;
	heapless::vector<V, N> _values;
};

} // namespace heapless
