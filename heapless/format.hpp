#pragma once

#include <string>

#include "string.hpp"

/*
Inspiration:

https://www.boost.org/doc/libs/1_70_0/libs/format/doc/format.html
https://docs.python.org/3/library/stdtypes.html#str.format
*/

namespace heapless {

std::string to_string(size_t value) {
	return std::to_string(value);
}

std::string to_string(int32_t value) {
	return std::to_string(value);
}

std::string to_string(zstring value) {
	return std::string(value);
}

std::string string_replace(const std::string& s, const std::string& from, const std::string& to) {
	size_t start = s.find(from);
	if (start == std::string::npos) {
		return s;
	}
	std::string result(s);
	result.replace(start, from.length(), to);
	return result;
}

std::string format_index(const std::string& format, size_t) {
	return format;
}

template <typename T, typename... Us>
std::string format_index(const std::string& format, size_t index, const T& first, Us... rest) {
	std::string from = "{" + heapless::to_string(index) + "}";
	std::string to = heapless::to_string(first);
	return format_index(heapless::string_replace(format, from, to), index + 1, rest...);
}

template <typename... Ts>
std::string format(const std::string& format, Ts... args) {
	return format_index(format, 0, args...);
}

} // namespace heapless
