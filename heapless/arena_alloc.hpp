#pragma once

namespace heapless {

class arena {
public:
	explicit arena() : _allocations(0), _deallocations(0), _arena1{{0}}, _arena1_index(0) {
#ifdef DEBUG
		std::cout << "arena::ctor" << std::endl;
#endif
	}

	~arena() {
#ifdef DEBUG
		std::cout << "arena::dtor allocations:" << _allocations << " deallocations:" << _deallocations << std::endl;
#endif
	}

	// TODO type? alignment?
	template <typename T>
	T* allocate(size_t size) {
		int32_t bytes = size * sizeof(T);

		// TODO check size bad_alloc

#ifdef DEBUG
		std::cout << "arena::allocate " << bytes << std::endl;
#endif
		_allocations += bytes;

		size_t index = _arena1_index;
		_arena1_index += 1;
		return reinterpret_cast<T*>(&_arena1[index]);
	}

	template <typename T>
	void deallocate(T*, size_t size) {
		int32_t bytes = size * sizeof(T);

#ifdef DEBUG
		std::cout << "arena::deallocate " << bytes << std::endl;
#endif
		_deallocations += bytes;
	}

private:
	template <size_t S>
	struct element {
		uint8_t data[S];
		// element* next;
	};

	size_t _allocations;
	size_t _deallocations;

	// element<4> element0[128];
	// size_t index0;
	element<16> _arena1[32];
	size_t _arena1_index;
	// element<64> element2[16];
	// size_t index2;
};

inline arena& global_arena() {
	static arena arena;
	return arena;
}

template <typename T>
class arena_alloc {
public:
	using value_type = T;

	T* allocate(size_t size) {
		return global_arena().allocate<T>(size);
	}

	void deallocate(T* p, size_t size) {
		global_arena().deallocate<T>(p, size);
	}
};

template <typename T, typename U>
inline bool operator==(const arena_alloc<T>&, const arena_alloc<U>&) throw() {
	return false;
}

template <typename T, typename U>
inline bool operator!=(const arena_alloc<T>&, const arena_alloc<U>&) throw() {
	return true;
}

} // namespace heapless
