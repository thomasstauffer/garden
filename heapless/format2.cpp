
#include <iostream>
#include <list>
#include <memory>
#include <vector>

std::string to_string(const char* value) {
	return std::string(value);
}

template <typename T>
std::string to_string(const T& value) {
	return std::to_string(value);
}

struct printer_interface {
public:
	virtual ~printer_interface() = default;
	virtual std::string to_string() = 0;
};

template <typename T>
struct printer : public printer_interface {
public:
	explicit printer(const T& element) : _element(element) {
	}
	virtual ~printer() override = default;
	virtual std::string to_string() override {
		return ::to_string(_element);
	}
	const T& _element;
};

class formatter1 {
public:
	~formatter1() {
	}

	template <typename T>
	void append(const T& rhs) {
		_list.push_back(std::make_unique<printer<T>>(rhs));
	}

	template <typename T>
	formatter1& operator<<(const T& rhs) {
		append(rhs);
		return *this;
	}

	std::string eval() const {
		std::string s;
		for (auto& i : _list) {
			s.append("[");
			s.append(i->to_string());
			s.append("]");
		}
		return s;
	}

private:
	std::list<std::unique_ptr<printer_interface>> _list;
};

class formatter2 {
public:
	~formatter2() {
		std::cout << "FREE" << std::endl;
	}

	template <typename T>
	void append(const T& rhs) {
		_s.append(" ");
		_s.append(::to_string(rhs));
		_s.append(" ");
	}

	std::string eval() const {
		return _s;
	}

	template <typename T>
	formatter2& operator<<(const T& rhs) {
		append(rhs);
		return *this;
	}

private:
	std::string _s;
};

class formatter3 {
public:
	~formatter3() {
	}

	template <typename T>
	void append(const T& rhs) {
		new (&_space[_index * printer_size]) printer<T>(rhs);
		_index += 1;
	}

	template <typename T>
	formatter3& operator<<(const T& rhs) {
		append(rhs);
		return *this;
	}

	std::string eval() const {
		std::string s;
		for (size_t i = 0; i < _index; i += 1) {
			s.append("[");
			printer_pointer p = printer_pointer(&_space[i * printer_size]);
			s.append(p->to_string());
			s.append("]");
		}
		return s;
	}

private:
	static const size_t printer_size = sizeof(printer<bool>);
	using printer_pointer = printer_interface*;

	size_t _index;
	uint8_t _space[10 * printer_size];
};

void print(const formatter1& f) {
	std::cout << f.eval() << std::endl;
}

void print(const formatter2& f) {
	std::cout << f.eval() << std::endl;
}

void print(const formatter3& f) {
	std::cout << f.eval() << std::endl;
}

int main() {
	print(formatter3() << 123 << 345 << 1.23 << "Foo" << 2 << "Hello World");

	// formatter3 x;
	// x << 123 << 345 << 1.23 << "Foo" << 2 << "Hello World";
	// std::cout << x.eval() << std::endl;

	// print(formatter2() << 123);
}
