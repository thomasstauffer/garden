
#include <functional>
#include <iostream>
#include <map>
#include <optional>
#include <queue>
#include <vector>

//#define DEBUG

/*
http://www.cplusplus.com/reference/exception/exception/
*/

#include "arena_alloc.hpp"
#include "format.hpp"
#include "function.hpp"
#include "map.hpp"
#include "optional.hpp"
#include "queue.hpp"
#include "scoped_lock.hpp"
#include "slice.hpp"
#include "stack_alloc.hpp"
#include "string.hpp"
#include "vector.hpp"

void assert(bool condition) {
	if (!condition) {
		throw std::runtime_error("Assert Failed");
	}
}

// helper classes/structs/functions for later tests

struct test_struct {
	int32_t a;
	int32_t b;
};

int32_t test_add(int32_t a, int32_t b) {
	return a + b;
}

class test_adder {
public:
	test_adder() : _number(0) {
	}

	int32_t and_and_get(int32_t value) {
		this->_number += value;
		return this->_number;
	}

	int32_t get() const {
		return _number;
	}

private:
	int32_t _number;
};

// heapless::vector

template <typename V>
V test_vector_return() {
	V v;
	assert(v.empty());
	v.push_back(1);
	v.push_back(-1);
	v.push_back(0);
	assert(!v.empty());
	assert(v.size() == 3);
	return v;
}

template <typename V>
void test_vector_compare() {
	V v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	V v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);

	V v3;
	v3.push_back(0);
	v3.push_back(1);
	v3.push_back(2);

	assert(v1 == v2);
	assert(v2 == v1);

	assert(v1 != v3);
	assert(v2 != v3);
}

template <typename V>
void test_vector_iterate() {
	V v(test_vector_return<V>());
	typename V::value_type sum1 = 0;
	typename V::value_type sum2 = 0;
	for (size_t i = 0; i < v.size(); i += 1) {
		sum1 += v[i];
	}
	for (auto i : v) {
		sum2 += i;
	}
	assert(v[0] == 1);
	assert(v[1] == -1);
	assert(v[2] == 0);
	assert(sum1 == 0);
	assert(sum2 == 0);
}

template <typename V>
void test_vector() {
	test_vector_iterate<V>();
	test_vector_compare<V>();
}

void test_vector() {
	using std_vector = std::vector<int32_t>;
	using stack_alloc_vector = std::vector<int32_t, heapless::stack_alloc<int32_t, 128>>;
	using arena_alloc_vector = std::vector<int32_t, heapless::arena_alloc<int32_t>>;
	using heapless_vector = heapless::vector<int32_t, 8>;
	// using heapless_vector_view = heapless::vector_view<int32_t>;

	test_vector<std_vector>();
	test_vector<stack_alloc_vector>();
	test_vector<arena_alloc_vector>();
	test_vector<heapless_vector>();
}

// heapless::string

template <typename S>
S test_string_return() {
	S s;
	s.append("Hello");
	s.append(" ");
	s.append("HeapLess");
	s.append(" ");
	s.append("abcdefghijklmnopqrstuvwxyz");
	return s;
}

template <typename S>
void test_string_size() {
	S s(test_string_return<S>());
	assert(s.length() == 41);
	assert(s == "Hello HeapLess abcdefghijklmnopqrstuvwxyz");
}

template <typename S>
void test_string() {
	test_string_size<S>();
}

void test_string() {
	using std_string = std::basic_string<char, std::char_traits<char>, std::allocator<char>>;
	using stack_alloc_string = std::basic_string<char, std::char_traits<char>, heapless::stack_alloc<char, 128>>;
	using arena_alloc_string = std::basic_string<char, std::char_traits<char>, heapless::arena_alloc<char>>;

	test_string<std_string>();
	test_string<stack_alloc_string>();
	test_string<arena_alloc_string>();
}

// heapless::format

void test_format() {
	assert(heapless::format("-{0}-{1}-", 111, 222) == "-111-222-");
}

// heapless::map

template <typename M>
void test_map_access() {
	M map;

	assert(map.empty());

	map[1] = 1;
	map[5] = 5;

	assert(!map.empty());

	assert(map[0] == 0);
	assert(map[1] == 1);
	assert(map[5] == 5);
	assert(map[2] == 0);

	assert(!map.empty());
	map.clear();
	assert(map.empty());
}

template <typename M>
void test_map_speed() {
	M map;

	for (size_t i = 0; i < 1024; i += 1) {
		map[i] = i;
	}

	int32_t sum = 0;
	for (size_t n = 0; n < 1000; n += 1) {
		for (size_t i = 0; i < 1024; i += 1) {
			sum += map[i];
		}
	}

	std::cout << sum << std::endl;
}

template <typename M>
void test_map() {
	test_map_access<M>();
	test_map_speed<M>();
}

void test_map() {
	using std_map = std::map<int32_t, int32_t>;
	using heapless_map = heapless::map<int32_t, int32_t, 1024>;

	test_map<std_map>();
	test_map<heapless_map>();
}

// heapless::queue

void test_queue() {
	// using std_queue = std::queue<test>;
}

// heapless::slice

void test_slice() {
	heapless::vector<int32_t, 8> v;
	for (size_t i = 0; i < 8; i += 1) {
		v.push_back(i);
	}
}

// heapless::function

template <typename T>
void test_function_raw() {
	T f = &test_add;
	assert(f(1, 2) == 3);
}

template <typename T>
void test_function_lambda() {
	test_adder adder;
	T f = [&adder](int32_t value) -> int32_t { return adder.and_and_get(value); };
	assert(f(1) == 1);
	assert(f(2) == 3);
}

template <typename T>
void test_function_member() {
	test_adder adder;
	T f = heapless::make_member_ptr(&test_adder::and_and_get, &adder);
	assert(f(1) == 1);
	assert(f(2) == 3);
}

void test_function() {
	using std_function_iii = std::function<int32_t(int32_t, int32_t)>;
	using function_iii = heapless::function<int32_t(int32_t, int32_t), 128>;

	test_function_raw<std_function_iii>();
	test_function_raw<function_iii>();

	using std_function_ii = std::function<int32_t(int32_t)>;
	using heapless_function_ii = heapless::function<int32_t(int32_t), 128>;

	test_function_lambda<std_function_ii>();
	test_function_lambda<heapless_function_ii>();

	test_function_member<std_function_ii>();
	test_function_member<heapless_function_ii>();
}

// heapless::bind

/*
Effectictive Modern C++
Item 34: Prefer lambdas to std::bind
*/

using test_bind_function = std::function<int32_t()>;

void test_bind_call(const test_bind_function& f) {
	assert(f() == 5);
}

void test_bind_queue(const test_bind_function& f, const test_bind_function& g) {
	std::queue<test_bind_function> q;
	q.push(f);
	q.push(g);
	int32_t sum = 0;
	while (!q.empty()) {
		auto h = q.front();
		q.pop();
		sum += h();
	}
	assert(sum == 8);
}

void test_bind() {
	test_bind_call(std::bind(test_add, 2, 3));

	test_bind_queue(std::bind(test_add, 1, 2), std::bind(test_add, 2, 3));
}

// heapless::optional

template <typename T>
void test_optional_has() {

	auto o1 = T(12);
	assert(o1.has_value());
	assert(o1.value() == 12);

	auto o2 = T();
	assert(!o2.has_value());
	typename T::value_type value = 42;
	assert(o2.value_or(value) == 42);
}

void test_optional() {
	using std_optional = std::optional<int32_t>;
	using optional = heapless::optional<int32_t>;

	test_optional_has<std_optional>();
	test_optional_has<optional>();
}

// heapless::scoped_lock

struct test_mutex {
	int32_t count;

	test_mutex() : count(0) {
	}

	void lock() {
		count += 1;
	}

	void unlock() {
		count -= 1;
	}
};

void test_scoped_lock() {
	test_mutex m;

	assert(m.count == 0);
	m.lock();
	assert(m.count == 1);
	{
		heapless::scoped_lock<test_mutex> lock(m);
		assert(m.count == 2);
	}
	assert(m.count == 1);
}

// main

int main() {
	// std::cout << "HeapLess" << std::endl;

	test_vector();
	test_string();
	test_format();
	test_map();
	test_queue();
	test_slice();
	test_function();
	test_bind();
	test_optional();
	test_scoped_lock();

	return 0;
}
