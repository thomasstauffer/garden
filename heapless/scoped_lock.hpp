#pragma once

namespace heapless {

template <class M>
class scoped_lock // ??? noncopyable
{
	M& _mutex;

public:
	scoped_lock(M& mutex) : _mutex(mutex) {
		_mutex.lock();
	}

	~scoped_lock() {
		_mutex.unlock();
	}
};

} // namespace heapless