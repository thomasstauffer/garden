#pragma once

/*
Inspiration:

https://doc.rust-lang.org/std/slice/index.html
https://docs.python.org/3/library/functions.html#slice

Ideas:

https://en.cppreference.com/w/cpp/named_req/ContiguousContainer
https://en.cppreference.com/w/cpp/numeric/valarray/slice
*/

namespace heapless {}