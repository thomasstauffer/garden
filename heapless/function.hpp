#pragma once

#include <memory>

namespace heapless {

/*
std::function + allocator

http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0302r0.html

std::function + const

http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0045r1.pdf
*/

template <typename RArgs, int SIZE>
class function;

/*
template <typename R, typename... Args>
class empty_functor {
public:
	R operator()(Args...) {
		return R();
	}
};
*/

// TODO write test case which counts constructor & destructor

template <typename R, typename... Args, int SIZE>
class function<R(Args...), SIZE> {
public:
	/*
	TODO bad_function_call?
	function() {
		static_assert(sizeof(empty_functor<R, Args...>) < sizeof(_memory), "not enough memory");
		new (_memory) callable<empty_functor<R, Args...>>(empty_functor<R, Args...>()); // TODO remove <empty_functor<R, Args...>> crashed GCC 7.5.0, bad function call?
	}
	*/

	template <typename T>
	function(const T& t) {
		static_assert(sizeof(callable<T>) < sizeof(_memory), "not enough memory");
		new (_memory) callable<T>(t);
	}

	function(const function<R(Args...), SIZE>& other) {
		static_assert(SIZE < sizeof(_memory), "not enough memory");
		other.as_pointer()->copy_to(_memory);
	}

	~function() {
		as_pointer()->~callable_interface();
	}

	function& operator=(const function<R(Args...), SIZE>& other) {
		static_assert(SIZE < sizeof(_memory), "not enough memory");
		as_pointer()->~callable_interface();
		if (this != &other) {
			other.as_pointer()->copy_to(_memory);
		}
		return *this;
	}

	R operator()(Args... args) {
		return as_pointer()->operator()(args...);
	}

private:
	class callable_interface {
	public:
		virtual ~callable_interface() = default;
		virtual R operator()(Args...) = 0;
		virtual void copy_to(uint8_t*) const = 0;
	};

	typedef callable_interface* pointer;
	typedef const callable_interface* const_pointer;

	const_pointer as_pointer() const {
		return reinterpret_cast<const_pointer>(_memory);
	}

	pointer as_pointer() {
		return reinterpret_cast<pointer>(_memory);
	}

	template <typename T>
	class callable : public callable_interface {
	public:
		callable(const T& t) : _callable(t) {
		}

		virtual R operator()(Args... args) override {
			return _callable(args...);
		}

		virtual void copy_to(uint8_t* memory) const override {
			new (memory) callable<T>(_callable);
		}

	private:
		T _callable;
	};

	// TODO alignas(alignof(T))?
	uint8_t _memory[SIZE];
};

/*
Only for backward compatibility if lambda is not supported. If variadic
templates are not supported then the templates needs to manually rewritten for a
fixed amount of arguments.
*/

template <class C, class R, typename... Args>
class member_ptr {
public:
	using class_type = C*;
	using member_type = R (C::*)(Args...);

	member_ptr(member_type member, class_type clazz) : _member(member), _class(clazz) {
	}

	R operator()(Args... args) {
		return (_class->*_member)(args...);
	}

	member_type _member;
	class_type _class;
};

template <typename C, typename R, typename... Args>
member_ptr<C, R, Args...> make_member_ptr(R (C::*m)(Args...), C* c) {
	return member_ptr<C, R, Args...>(m, c);
}

} // namespace heapless
