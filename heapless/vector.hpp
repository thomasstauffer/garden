#pragma once

#include "slice.hpp"

namespace heapless {

/*
Instead of a view, make a better iterator, with the possibility of a slice.

At the moment this is just a const iterator which works quite ok (elements can
be added and deleted during iteration, as long as the vector does not get
smaller) see also https://doc.rust-lang.org/std/iter/trait.Iterator.html
*/
template <typename T>
class iterator {
public:
	using value_type = typename T::value_type;
	// using reference = typename T::value_type&;
	using const_reference = const typename T::value_type&;

	explicit iterator(const T& store, size_t index) : _store(store), _index(index) {
	}

	bool operator!=(const iterator& other) const {
		return _index != other._index;
	}

	iterator& operator++() {
		// TODO throw already here if it is past the end?
		_index += 1;
		return *this;
	}

	const_reference operator*() const {
		return _store[_index];
	}

private:
	const T& _store;
	size_t _index;
};

template <typename T, size_t N>
class vector {
public:
	using value_type = T;

	using pointer = T*;
	using const_pointer = const T*;

	using reference = T&;
	using const_reference = const T&;

	explicit vector() : _size(0) {
	}

	template <size_t O>
	vector(const vector<T, O>& other) : _size(0) {
		push_back(other);
	}

	vector(const vector<T, N>& other) : _size(0) {
		push_back(other);
	}

	template <size_t O>
	vector<T, N>& operator=(const vector<T, O>& other) {
		if (this != reinterpret_cast<const vector<T, N>*>(&other)) {
			clear();
			push_back(other);
		}
		return *this;
	}

	vector<T, N>& operator=(const vector<T, N>& other) {
		if (this != &other) {
			clear();
			push_back(other);
		}
		return *this;
	}

	~vector() {
		clear();
	}

	bool empty() const {
		return _size == 0;
	}

	size_t size() const {
		return _size;
	}

	constexpr size_t capacity() const {
		return N;
	}

	void push_back(const T& value) {
		if (_size >= N) {
			throw std::invalid_argument("not enough space");
		}
		pointer p = &pointer(&_bytes)[_size];
		new (p) T(value);
		_size += 1;
	}

	template <size_t O>
	void push_back(const vector<T, O>& other) {
		size_t other_size = other._size;
		for (size_t i = 0; i < other_size; i += 1) {
			push_back(other[i]);
		}
	}

	void clear() {
		pointer p = pointer(&_bytes);
		for (size_t i = 0; i < _size; i++) {
			p[i].~T();
		}
		_size = 0;
	}

	reference operator[](const size_t index) {
		if (index >= _size) {
			throw std::invalid_argument("invalid index");
		}
		return pointer(&_bytes)[index];
	}

	const_reference operator[](const size_t index) const {
		if (index >= _size) {
			throw std::invalid_argument("invalid index");
		}
		return pointer(&_bytes)[index];
	}

	/*
	void operator[](const slice slice) {
		throw std::runtime_error("not implemented");
	}
	*/

	/*
	TODO
	instead of putting begin/end here add a function which return an object,
	which has begin/end function, e.g. call then for(auto i : v.iter())
	*/

	iterator<vector<T, N>> begin() {
		return iterator<vector<T, N>>(*this, 0);
	}

	iterator<vector<T, N>> end() {
		return iterator<vector<T, N>>(*this, _size);
	}

private:
	size_t _size;
	uint8_t _bytes[sizeof(T) * N];
};

template <typename T, size_t N, size_t O>
inline bool operator==(const vector<T, N>& left, const vector<T, O>& right) {
	if (left.size() != right.size()) {
		return false;
	}
	for (size_t i = 0; i < left.size(); i++) {
		if (left[i] != right[i]) {
			return false;
		}
	}
	return true;
}

template <typename T, size_t N, size_t O>
inline bool operator!=(const vector<T, N>& left, const vector<T, O>& right) {
	return !(left == right);
}

} // namespace heapless
