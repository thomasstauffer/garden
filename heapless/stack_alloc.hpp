#pragma once

namespace heapless {

template <typename T, size_t SIZE>
class stack_alloc {
public:
	using size_type = size_t;
	using difference_type = ptrdiff_t;

	using pointer = T*;
	using const_pointer = const T*;

	using reference = T&;
	using const_reference = const T&;

	using value_type = T;

	template <class U>
	struct rebind {
		using other = stack_alloc<U, SIZE>;
	};

	// throw/noexcept what is up to date C++17???

	explicit stack_alloc() noexcept : _index(0) {
#ifdef DEBUG
		std::cout << "stack_alloc::ctor" << std::endl;
#endif
	}

	// stack_alloc(const stack_alloc&) throw()
	// template <class U> stack_alloc(const stack_alloc<U>&) throw();

	~stack_alloc() noexcept {
#ifdef DEBUG
		std::cout << "stack_alloc::dtor" << std::endl;
#endif
	}

	pointer address(reference x) const {
#ifdef DEBUG
		std::cout << "stack_alloc::address TODO" << std::endl;
#endif
		return &x;
	}

	const_pointer address(const_reference x) const {
#ifdef DEBUG
		std::cout << "stack_alloc::address TODO" << std::endl;
#endif
		return x;
	}

	pointer allocate(size_type size, const_pointer hint = 0) {
		(void)(hint);

#ifdef DEBUG
		std::cout << "stack_alloc::allocate " << size << "*" << sizeof(value_type) << std::endl;
#endif
		// TODO size > SIZE?
		const size_type index = _index;
		_index += size * sizeof(value_type);
		return reinterpret_cast<pointer>(&_buffer[index]);
	}

	void deallocate(pointer p, size_type size) noexcept {
		(void)p;
		(void)size;

#ifdef DEBUG
		std::cout << "stack_alloc::deallocate " << size << "*" << sizeof(value_type) << std::endl;
#endif
		// TODO
	}

	size_type max_size() const noexcept {
		return SIZE;
	}

	void construct(pointer p, const T& val) {
#ifdef DEBUG
		std::cout << "stack_alloc::construct" << std::endl;
#endif
		new (p) value_type(val);
	}

	void destroy(pointer p) {
#ifdef DEBUG
		std::cout << "stack_alloc::destroy" << std::endl;
#endif
		p->~value_type();
	}

private:
	size_type _index;
	uint8_t _buffer[SIZE * sizeof(value_type)];
};

template <typename T1, size_t SIZE1, typename T2, size_t SIZE2>
inline bool operator==(const stack_alloc<T1, SIZE1>&, const stack_alloc<T2, SIZE2>&) throw() {
	return false;
}

template <typename T1, size_t SIZE1, typename T2, size_t SIZE2>
inline bool operator!=(const stack_alloc<T1, SIZE1>&, const stack_alloc<T2, SIZE2>&) throw() {
	return true;
}

} // namespace heapless
