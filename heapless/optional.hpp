#pragma once

namespace heapless {

struct nullopt {};

// TODO this pattern of construcing/destructing repeats itself in vector and also stack_ptr, generalize?

template <typename T>
class optional {
public:
	using value_type = T;
	using pointer = T*;
	using const_reference = const T&;

	constexpr explicit optional(const T& value) noexcept : _has_value(true) {
		pointer p = pointer(&_space);
		new (p) T(value);
	}

	constexpr explicit optional(const nullopt) noexcept : _has_value(false) {
	}

	constexpr explicit optional() noexcept : _has_value(false) {
	}

	~optional() noexcept {
		pointer p = pointer(&_space);
		p->~T();
	}

	constexpr bool has_value() const noexcept {
		return _has_value;
	}

	constexpr const_reference value() const {
		pointer p = pointer(&_space);
		if (!_has_value) {
			throw std::bad_optional_access();
		}
		return *p;
	}

	constexpr const_reference value_or(const_reference value) const {
		pointer p = pointer(&_space);
		return _has_value ? *p : value;
	}

private:
	bool _has_value;
	uint8_t _space[sizeof(T)];
};

} // namespace heapless