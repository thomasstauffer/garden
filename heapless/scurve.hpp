
#include <cmath>
#include <iostream>

class scurve_exp {
public:
	explicit scurve_exp(float gradient) : _gradient(gradient) {
		_offset = 1.0f / (1.0f + std::exp(0.5f * gradient));
		_scale = 1.0f / (1.0f - 2.0f * _offset);
	}

	float operator()(float t) {
		return (1.0f / (1.0f + std::exp(-((t - 0.5f) * _gradient))) - _offset) * _scale;
	}

private:
	float _gradient;
	float _scale;
	float _offset;
};

class scurve_arctan {
public:
	explicit scurve_arctan(float gradient) : _gradient(gradient) {
		_scale = 1.0f / std::atan(gradient * 0.5f);
	}

	float operator()(float t) {
		return (std::atan((t - 0.5f) * _gradient) * _scale + 1.0f) * 0.5f;
	}

private:
	float _gradient;
	float _scale;
};

int testscurve() {
	scurve_exp s1(15.0f);
	scurve_arctan s2(15.0f);

	for (int32_t i = 0; i <= 100; i += 1) {
		float t = i / 100.0f;
		std::cout << t << "," << s1(t) << "," << s2(t) << std::endl;
	}
}
