
#include <iostream>
#include <limits>

#include <stdio.h>
#include <string.h>

/*
Type safe library which internally depends only on vsnprintf. the
vsnprintf-alike functions are available on many systems and the conversion to a
string is only done if something is logged.
*/

typedef const char* czstring;

struct empty_t {};

empty_t empty() {
	empty_t e;
	return e;
}

template <typename T>
czstring log_type(T) {
	return "?";
}

template <>
czstring log_type<empty_t>(empty_t) {
	return "";
}

template <>
czstring log_type<char>(char) {
	return "%c";
}

template <>
czstring log_type<bool>(bool arg) {
	return arg ? "true" : "false";
}

template <>
czstring log_type<int8_t>(int8_t) {
	return "%d";
}

template <>
czstring log_type<uint8_t>(uint8_t) {
	return "%u";
}

template <>
czstring log_type<int16_t>(int16_t) {
	return "%d";
}

template <>
czstring log_type<uint16_t>(uint16_t) {
	return "%u";
}

template <>
czstring log_type<int32_t>(int32_t) {
	return "%d";
}

template <>
czstring log_type<uint32_t>(uint32_t) {
	return "%u";
}

template <>
czstring log_type<int64_t>(int64_t) {
	return "%lld";
}

template <>
czstring log_type<uint64_t>(uint64_t) {
	return "%llu";
}

template <>
czstring log_type<float>(float) {
	return "%f";
}

template <>
czstring log_type<double>(double) {
	return "%lf";
}

template <>
czstring log_type<czstring>(czstring) {
	return "%s";
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9) {
	const size_t FORMAT_SIZE = 64;
	char format[FORMAT_SIZE];
	format[0] = 0;

	// TODO add check if log is active

	::strcat(format, log_type<T1>(arg1));
	::strcat(format, log_type<T2>(arg2));
	::strcat(format, log_type<T3>(arg3));
	::strcat(format, log_type<T4>(arg4));
	::strcat(format, log_type<T5>(arg5));
	::strcat(format, log_type<T6>(arg6));
	::strcat(format, log_type<T7>(arg7));
	::strcat(format, log_type<T8>(arg8));
	::strcat(format, log_type<T9>(arg9));
	//::printf("%s\n", format);
	::strcat(format, "\n");

	::printf("[%s] ", name);
	::printf(format, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8) {
	log(name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, empty());
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7) {
	log(name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, empty(), empty());
}

template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6) {
	log(name, arg1, arg2, arg3, arg4, arg5, arg6, empty(), empty(), empty());
}

template <typename T1, typename T2, typename T3, typename T4, typename T5>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5) {
	log(name, arg1, arg2, arg3, arg4, arg5, empty(), empty(), empty(), empty());
}

template <typename T1, typename T2, typename T3, typename T4>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3, T4 arg4) {
	log(name, arg1, arg2, arg3, arg4, empty(), empty(), empty(), empty());
}

template <typename T1, typename T2, typename T3>
void log(const char* name, T1 arg1, T2 arg2, T3 arg3) {
	log(name, arg1, arg2, arg3, empty(), empty(), empty(), empty(), empty(), empty());
}

template <typename T1, typename T2>
void log(const char* name, T1 arg1, T2 arg2) {
	log(name, arg1, arg2, empty(), empty(), empty(), empty(), empty(), empty(), empty());
}

template <typename T1>
void log(const char* name, T1 arg1) {
	log(name, arg1, empty(), empty(), empty(), empty(), empty(), empty(), empty(), empty());
}

class MyLog {
public:
	MyLog(czstring name) : _name(name) {
	}
	bool isEnabled() {
		return true;
	}
	bool print(zstring message) {
		::printf("%s", message);
	}

private:
	czstring _name;
}

void log_test() {
	int8_t value = 3;
	log("Name", 0);
	log("Name", 'H', 'e', 'l', 'l', 'o');
	log("Name", 0, true, 0);
	log("Name", 1, 2, value, 4, 5);
	log("Name", 1.23f, " ", 4.56);
	log("Name", 1, 2, 3);
	log("Name", -1, " ", "Hello", " ", std::numeric_limits<uint32_t>::max());
	log("Name", "Hello", " ", std::numeric_limits<uint64_t>::max(), " ", 1);
}

int main() {
	std::cout << "Hello Print" << std::endl;

	log_test();
}
