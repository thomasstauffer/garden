
#include <stdio.h>
#include <gmp.h>

int main() {
	mpz_t n;
	mpz_init(n);

	mpz_t base;
	mpz_init(base);
	mpz_set_ui(base, 2);

	mpz_pow_ui(n, base, 1000);

	mpz_out_str(stdout, 10, n);
	printf ("\n");

	return 0;
}
