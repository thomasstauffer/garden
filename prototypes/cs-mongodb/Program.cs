﻿using System;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Bson;

class Program
{
    public static void Main(string[] args)
    {
        var random = new Random();

        var client = new MongoClient("mongodb://localhost:27017");

        var databaseName = "test";

        var database = client.GetDatabase(databaseName);

        Console.WriteLine(string.Join('\n', client.ListDatabases().ToList()));

        var timeSeriesOptions = new CreateCollectionOptions
        {
            TimeSeriesOptions = new TimeSeriesOptions("timestamp")
        };

        var collectionName = "test" + random.NextInt64(1000000);
        
        bool useTimerSeries = true;

        if (useTimerSeries)
        {
            database.CreateCollection(collectionName, timeSeriesOptions);
        }
        else
        {
            database.CreateCollection(collectionName);
        }

        var collection = database.GetCollection<BsonDocument>(collectionName);

        DateTime startTime = DateTime.UtcNow;

        for (int n = 0; n < 100000; n += 1)
        {
            var docs = Enumerable.Range(1, 30).Select(i => new BsonDocument {
                { "timestamp", startTime.AddSeconds(n) },
                { "value", random.NextDouble() },
                { "metadata", new BsonDocument("value_id", "value_id" + i) }
            });
            collection.InsertMany(docs);
        }

        var filter = Builders<BsonDocument>.Filter.Empty;
        Console.WriteLine("Count {0}", collection.CountDocuments(filter));
    }
}
