
#include <algorithm>
#include <array>
#include <iostream>
#include <map>

// check https://github.com/serge-sans-paille/frozen/blob/master/include/frozen/map.h ???

// find_if is constexpr since c++20

template <typename T, std::size_t Size>
class array {
public:
	using value_type = T;
	using pointer = value_type*;
	using iterator = pointer;

	//constexpr explicit array(const T[Size]& data)  {
	//}

	/*
	constexpr explicit array(std::initializer_list<value_type> data) : _data{data} {
	}
	*/

	constexpr iterator begin() {
		return _data;
	}
	constexpr iterator end() {
		return _data + Size;
	}

private:
	T _data[Size] = {};
};

template <typename T, std::size_t Size>
class set {
public:
	using value_type = T;
	using container_type = std::array<value_type, Size>;

	constexpr explicit set(container_type data) : _data{data} {
	}

	/*
	constexpr explicit set(std::initializer_list<value_type> data) : _data{data} {
	}
	*/

	constexpr bool contains(const T& key) const {
		const auto it = std::find_if(begin(_data), end(_data), [&key](const auto& i) { return i == key; });
		return it != end(_data);
		return false;
	}

private:
	container_type _data;
};

template <typename Key, typename Value, std::size_t Size>
class map {
public:
	using value_type = std::pair<Key, Value>;
	using container_type = std::array<value_type, Size>;

	constexpr map(container_type data) : _data{data} {
	}

	constexpr map(std::initializer_list<value_type> data) : map(container_type{data}) {
	}

	constexpr Value at(const Key& key) const {
		const auto it = std::find_if(begin(_data), end(_data), [&key](const auto& i) { return i.first == key; });
		if (it != end(_data)) {
			return it->second;
		} else {
			throw std::out_of_range("out of range");
		}
	}

private:
	container_type _data;
};

void test_hw() {
	std::cout << "Hello World" << std::endl;
}

void test_array() {
	static constexpr std::array<int, 7> a{1, 1, 2, 3, 5, 8, 13};

    std::cout << a[3] << std::endl;

	//constexpr array<int, 3> b{2, 3, 4};
}

void test_set() {
	static constexpr set<int, 10> s{{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};
	
	std::cout << s.contains(2) << std::endl;
	std::cout << s.contains(5) << std::endl;
}

void test_map() {
	static constexpr map<int, int, 4> m{{{{0x11, 0x22}, {0x33, 0x44}, {0x55, 0x66}, {0x77, 0x88}}}};

	constexpr int a = m.at(0x11);

	std::cout << a << std::endl;
}

int main() {
	test_hw();
	test_array();
	test_set();
	test_map();
	return 0;
}

/*
objdump -s -j .rodata main

main:     file format elf64-x86-64

Contents of section .rodata:
 2000 01000200 00000000 00000000 00000000  ................
 2010 00000000 00000000 00000000 00000000  ................
 2020 00000000 48656c6c 6f20576f 726c6400  ....Hello World.
 2030 01000000 01000000 02000000 03000000  ................
 2040 05000000 08000000 0d000000 00000000  ................
 2050 00000000 00000000 00000000 00000000  ................
 2060 01000000 02000000 03000000 04000000  ................
 2070 05000000 06000000 07000000 08000000  ................
 2080 09000000 0a000000 00000000 00000000  ................
 2090 00000000 00000000 00000000 00000000  ................
 20a0 11000000 22000000 33000000 44000000  ...."...3...D...
 20b0 55000000 66000000 77000000 88000000  U...f...w.......
*/
