#!/usr/bin/env python3

import socket
import struct
import time
import threading
import http.server
import os
import re
import urllib.parse

SERVER_IP = [172, 22, 22, 222]

DHCP_SERVER_PORT = 67
DHCP_CLIENT_PORT = 68
DHCP_DISCOVER, DHCP_OFFER, DHCP_REQUEST, DHCP_DECLINE, DHCP_ACK, DHCP_NACK = 1, 2, 3, 4, 5, 6
# currently just one IP is supported, which wrongly would also be delivered to several clients
DHCP_IP_TO_OFFER = [172, 22, 22, 111]
DHCP_SERVER_IP = SERVER_IP
DHCP_DNS_IP = SERVER_IP
DHCP_ROUTER_IP = SERVER_IP
DHCP_SERVER_SUBNET = [255, 255, 255, 0]
DHCP_INTERFACE = 'enp3s0'
    
def dhcp_decode(raw):
    header = struct.unpack('>BBBBIHH4s4s4s4s16s64s128sI', raw[:240])

    options = {}
    index = 240
    for _ in range(64):
        otype = raw[index+0]
        if otype == 255:
            break
        olength = raw[index+1]
        oraw = raw[index+2:index+2+olength]
        options[otype] = oraw
        index += 2 + olength

    req = {
        'op': header[0],
        'htype': header[1],
        'hlen': header[2],
        'hops': header[3],
        'xid': header[4],
        'secs': header[5],
        'flags': header[6],
        'ciaddr': header[7],
        'yiaddr': header[8],
        'siaddr': header[9],
        'giaddr': header[10],
        'chaddr': header[11],
        #'sname': result[12],
        #'file': result[13],
        'magiccookie': header[14],
        'options': options
    }
    return req

def dhcp_handle(req):
    res = {
        'op': 2,
        'htype': 1,
        'hlen': 6,
        'hops': 0,
        'xid': req['xid'],
        'secs': 1,
        'flags': 0,
        'ciaddr': bytes([0, 0, 0, 0]),
        'yiaddr': bytes([0, 0, 0, 0]),
        'siaddr': bytes([0, 0, 0, 0]),
        'giaddr': bytes([0, 0, 0, 0]),
        'chaddr': req['chaddr'],
        #'sname': result[12],
        #'file': result[13],
        'magiccookie': req['magiccookie'],
        'options': {},
    }

    add_ip = False
    if (req['options'][53][0] == DHCP_REQUEST) and (req['options'][50] == bytes(DHCP_IP_TO_OFFER)):
        res['options'][53] = bytes([DHCP_ACK])
        add_ip = True
    elif (req['options'][53][0] == DHCP_REQUEST):
        res['options'][53] = bytes([DHCP_NACK])
    elif (req['options'][53][0] == DHCP_DISCOVER):
        res['options'][53] = bytes([DHCP_OFFER])
        add_ip = True
    else:
        print('Invalid Request')
        return None

    if add_ip:
        res['yiaddr'] = bytes(DHCP_IP_TO_OFFER)
        res['siaddr'] = bytes(DHCP_SERVER_IP)
        res['options'][1] = bytes(DHCP_SERVER_SUBNET) # Subnet
        res['options'][3] = bytes(DHCP_ROUTER_IP) # Router
        res['options'][54] = bytes(DHCP_SERVER_IP) # Server Identifier
        res['options'][6] = bytes(DHCP_DNS_IP) # DNS
        res['options'][51] = bytes([0, 1, 0, 0]) # Lease Time

    return res

def dhcp_encode(res):
    header_raw = struct.pack('>BBBBIHH4s4s4s4s16s64s128sI', res['op'], res['htype'], res['hlen'], res['hops'], res['xid'], res['secs'], res['flags'], res['ciaddr'], res['yiaddr'], res['siaddr'], res['giaddr'], res['chaddr'], bytes([0]*64), bytes([0]*128), res['magiccookie'])

    options_raw = bytes()
    for otype, oraw in res['options'].items():
        options_raw += bytes([otype, len(oraw)]) + oraw
    options_raw += bytes([255])

    res_raw = header_raw + options_raw

    # depending on the broadcast bit set by the client the response address should be an unicast or a broadcast
    res_addr = ('255.255.255.255', DHCP_CLIENT_PORT)

    return res_raw, res_addr

def dhcp_main():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
    # without binding to an interface -> [Errno 101] Network is unreachable
    s.setsockopt(socket.SOL_SOCKET, 25, (DHCP_INTERFACE + '\0').encode())
    s.bind(('0.0.0.0', DHCP_SERVER_PORT))

    while True:
        req_raw, req_addr = s.recvfrom(1024)
        print('DHCP Receive')
        req = dhcp_decode(req_raw)
        print('DHCP Request:', req)
        res = dhcp_handle(req)
        if res is not None:
            print('DHCP Response:', res)
            res_raw, res_addr = dhcp_encode(res)
            print('DHCP Send:', res_addr)
            s.sendto(res_raw, res_addr)

DNS_SERVER_PORT = 53
DNS_TYPE_A, DNS_TYPE_MX, DNS_TYPE_TXT, DNS_TYPE_AAAA = 1, 15, 16, 28
DNS_CLASS_INTERNET = 1
DNS_ANSWER_IP = SERVER_IP

def dns_decode(raw):
    header = struct.unpack('>HHHHHH', raw[:12])

    req = {
        'id': header[0],
        'flags': header[1],
        'qdcount': header[2],
        'ancount': header[3],
        'nscount': header[4],
        'arcount': header[5],
        'questions': [],
        'answers': [],
        'nameservers': [],
        'additionals': [],
    }

    index = 12
    for i in range(req['qdcount']):
        pass
        qname = []
        while raw[index] != 0:
            length = raw[index]
            index += 1
            qname += [raw[index:index+length]]
            index += length
        index += 1
        qtype, qclass = struct.unpack('>HH', raw[index:index+4])
        index += 4
        req['questions'] += [{
            'qname': qname,
            'qtype': qtype,
            'qclass': qclass,
        }]

    return req

def dns_handle(req):
    res = {
        'id': req['id'],
        'flags': 0x8000,
        'qdcount': 1,
        'ancount': 1,
        'nscount': 0,
        'arcount': 0,
        'questions': req['questions'],
        'answers': [],
        'nameservers': [],
        'additionals': [],
    }

    if (req['qdcount'] != 1) or (req['ancount'] != 0) or (req['nscount'] != 0) or (req['arcount'] != 0) or (len(req['questions']) != 1) or (req['questions'][0]['qclass'] != DNS_CLASS_INTERNET):
        print('Invalid Request')
        return None

    qtype = req['questions'][0]['qtype']
    if (qtype == DNS_TYPE_A) or (qtype == DNS_TYPE_AAAA):
        # TODO return a different answer for an AAAA query
        res['answers'] += [{
            'name': 0xc000 + 0x000c, # 2 highest bit 1 = reference
            'type': DNS_TYPE_A,
            'class': DNS_CLASS_INTERNET,
            'ttl': 60,
            'rdlength': 4,
            'rdata': bytes(DNS_ANSWER_IP),
        }]
    else:
        print('Invalid Request')
        return None

    return res

def dns_encode(res):
    res_raw = struct.pack('>HHHHHH', res['id'], res['flags'], res['qdcount'], res['ancount'], res['nscount'], res['arcount'])

    for question in res['questions']:
        for part in question['qname']:
            res_raw += bytes([len(part)]) + part
        res_raw += bytes([0]) + struct.pack('>HH', question['qtype'], question['qclass'])

    for answer in res['answers']:
        res_raw += struct.pack('>HHHIH', answer['name'], answer['type'], answer['class'], answer['ttl'], answer['rdlength']) + answer['rdata']

    return res_raw

def dns_main():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    s.bind(('0.0.0.0', DNS_SERVER_PORT))

    while True:
        req_raw, req_addr = s.recvfrom(1024)
        print('DNS Receive: ', req_raw)
        req = dns_decode(req_raw)
        print('DNS Request:', req)
        res = dns_handle(req)
        if res is not None:
            print('DNS Response:', res)
            res_raw = dns_encode(res)
            print('DNS Send', res_raw)
            s.sendto(res_raw, req_addr)

HTTP_SERVER_PORT = 80

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        url = urllib.parse.urlparse(self.path)
        path = url.path.strip('/')
        print('HTTP GET:', path)
        if not os.path.exists(path):
            self.send_error(404, "File Not Found")
            return
        file_size = os.path.getsize(path)
        start_range, end_range = 0, file_size - 1

        if 'Range' in self.headers:
            print('HTTP Range:', self.headers['Range'])
            m = re.match('bytes=([0-9]+)-([0-9]+)', self.headers['Range'])
            start_range, end_range = int(m.group(1)), int(m.group(2))
            self.send_response(206)
            self.send_header('Content-type', 'application/octet-stream')
            self.send_header('Accept-Ranges', 'bytes')
            self.send_header('Content-Length', end_range - start_range + 1)
            self.send_header('Content-Range', f'bytes {start_range}-{end_range}/{file_size}')
            self.end_headers()
        else:
            self.send_response(200)
            self.send_header('Content-type', 'application/octet-stream')
            self.end_headers()

        with open(path, 'rb') as file:
            file.seek(start_range)
            self.wfile.write(file.read(end_range - start_range + 1))

    def log_message(self, format, *args):
        pass

# curl -H "Range: bytes=1-8" http://127.0.0.1:8080/test
def http_main():
    server = http.server.HTTPServer(('0.0.0.0', HTTP_SERVER_PORT), RequestHandler)
    server.serve_forever()


def main():
    #http_main()
    #dhcp_main()
    #dns_main()
    threads = [threading.Thread(target=dns_main), threading.Thread(target=dhcp_main), threading.Thread(target=http_main)]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

main()
