﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Concurrent;

namespace reader
{
    public delegate void Reader(Reactor reactor);

    public class MyEvent
    {
        public Object Who;
        public byte[] Buffer;

        public MyEvent(Object who, byte[] buffer)
        {
            Who = who;
            Buffer = buffer;
        }
    }

    public class Reactor
    {
        private BlockingCollection<MyEvent> _events = new BlockingCollection<MyEvent>();

        public void Notify(Object who, byte[] buffer, int length)
        {
            byte[] b = new byte[length];
            for (int i = 0; i < length; i += 1)
            {
                b[i] = buffer[i];
            }
            _events.Add(new MyEvent(who, b));
        }

        public void Register(Reader reader)
        {
            ThreadStart del = delegate
            {
                reader(this);
            };

            Thread _thread;
            _thread = new Thread(del);
            _thread.Start();
        }

        public MyEvent Select()
        {
            var myevent = _events.Take();
            return myevent;
        }
    }

    class TcpServer
    {
        NetworkStream _stream;
        public void Start(Reactor reactor)
        {
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            TcpListener server = new TcpListener(localAddr, 5555);
            server.Start();
            Console.WriteLine("Server Started ...");
            byte[] buffer = new byte[1024];
            TcpClient client = server.AcceptTcpClient();
            _stream = client.GetStream();

            while (true)
            {
                int n = _stream.Read(buffer, 0, buffer.Length);
                reactor.Notify(this, buffer, n);
                if (n == 0)
                {
                    break;
                }
            }
            Console.WriteLine("Client Closed");
            client.Close();
            server.Stop();
        }

        public void Write(byte[] buffer, int length)
        {
            _stream.Write(buffer, 0, length);
        }
    }

    class StdIo
    {
        public void Start(Reactor reactor)
        {
            Console.WriteLine("Console Started ...");
            while (true)
            {
                string line = Console.ReadLine();
                byte[] buffer = Encoding.ASCII.GetBytes(line);
                reactor.Notify(this, buffer, buffer.Length);
            }
        }

        public void Write(byte[] buffer, int length)
        {
            for (int i = 0; i < length; i += 1)
            {
                Console.Write((char)buffer[i]);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Relay");

            var reactor = new Reactor();

            var a = new TcpServer();
            var b = new StdIo();

            reactor.Register(a.Start);
            reactor.Register(b.Start);

            while (true)
            {
                MyEvent ev = reactor.Select();
                Console.WriteLine("New Event");

                if (ev.Who == a)
                {
                    b.Write(ev.Buffer, ev.Buffer.Length);
                }
                else
                {
                    a.Write(ev.Buffer, ev.Buffer.Length);
                }
            }
        }
    }
}
