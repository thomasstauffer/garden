using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace socat {
    class Program {
        public interface Startable {
            void Open();
            void Write(byte[] buffer, int length);
            Message Read();
            void Close();
        }

        public class Message {
            public byte[] Buffer;
            public int Length;

            public Message(byte[] buffer, int length) {
                Buffer = buffer;
                Length = length;
            }
        }

        class TcpServer : Startable {
            TcpClient _client = null;
            TcpListener _server;
            byte[] _buffer;

            public void Open() {
                //IPAddress localAddr = IPAddress.Parse(uri.Host);
                int port = 5555;
                _server = new TcpListener(IPAddress.Any, port);
                _server.Start();
                Console.WriteLine("TcpServer - Started (0.0.0.0:{0})", port);
                _buffer = new byte[1024];
                Console.WriteLine("TcpServer - Client Connected");
            }

            public Message Read() {
                if (_client == null) {
                    if (_server.Pending()) {
                        _client = _server.AcceptTcpClient();
                        Console.WriteLine("TcpServer - Connected");
                    }
                } else {
                    NetworkStream stream = _client.GetStream();

                    /*
                    if (_client.Client.Poll(1, SelectMode.SelectRead) && !stream.DataAvailable) {
                        Console.WriteLine("TcpServer - Disconnected");
                        _client = null;
                    }
                    */

                    //Console.WriteLine(stream.DataAvailable);

                    if (stream.DataAvailable) {
                        int n = stream.Read(_buffer, 0, _buffer.Length);
                        return new Message(_buffer, n);
                    }
                }

                return null;
            }

            public void Write(byte[] buffer, int length) {
                NetworkStream stream = _client.GetStream();
                stream.Write(buffer, 0, length);
            }

            public void Close() {
                if (_client != null) {
                    _client.Client.Shutdown(SocketShutdown.Both);
                }
                _server.Stop();
            }
        }

        class Serial : Startable {
            SerialPort _serialPort = new SerialPort();
            byte[] _buffer;

            public void Open() {
                string path = "COM1";

                _serialPort.PortName = path;
                _serialPort.BaudRate = 115200;
                _serialPort.Parity = Parity.None;
                _serialPort.DataBits = 8;
                _serialPort.Open();
                _buffer = new byte[1024];
                Console.WriteLine("Serial - Opened ({0})", path);
            }

            public Message Read() {
                int n = Math.Min(_serialPort.BytesToRead, _buffer.Length);
                if (n > 0) {
                    _serialPort.Read(_buffer, 0, n);
                    return new Message(_buffer, n);
                }
                return null;
            }

            public void Write(byte[] buffer, int length) {
                _serialPort.Write(buffer, 0, length);
            }

            public void Close() {
                _serialPort.Close();
            }
        }

        static void Relay(Uri srcUri, Uri dstUri) {
            var src = CreateFromScheme(srcUri.Scheme);
            var dst = CreateFromScheme(dstUri.Scheme);

            Console.WriteLine("Relay Open");

            src.Open();
            dst.Open();

            Console.WriteLine("Relay Opened");

            int n = 0;
            Message message;
            while (true) {
                message = src.Read();
                if (message != null) {
                    dst.Write(message.Buffer, message.Length);
                }

                message = dst.Read();
                if (message != null) {
                    src.Write(message.Buffer, message.Length);
                }

                Thread.Sleep(100);

                if (n > 200) {
                    break;
                }
                n += 1;
            }

            Console.WriteLine("Relay Stop");

            src.Close();
            dst.Close();

            Console.WriteLine("Relay Stopped");
        }

        private static Startable CreateFromScheme(string scheme) {
            if (scheme == "stdio") {
                throw new NotImplementedException();
            } else if (scheme == "tcp-server") {
                return new TcpServer();
            } else if (scheme == "tcp") {
                throw new NotImplementedException();
            } else if (scheme == "serial") {
                return new Serial();
            } else if (scheme == "file-write") {
                throw new NotImplementedException();
            }
            throw new NotImplementedException(string.Format("Invalid Scheme '{0}'", scheme));
        }

        static void SerialTest(string port) {
            SerialPort serialPort;
            serialPort = new SerialPort();
            serialPort.PortName = port;
            serialPort.BaudRate = 115200;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.ReadTimeout = 2000;
            serialPort.WriteTimeout = 2000;

            Console.WriteLine("Open Port:'{0}'", port);
            serialPort.Open();

            Console.WriteLine("Write");
            serialPort.Write("TDOO\r\n");

            Console.WriteLine("Wait");
            Thread.Sleep(200);

            int n = serialPort.BytesToRead;
            Console.WriteLine("Read Bytes:{0}", n);
            char[] buffer = new char[n];
            serialPort.Read(buffer, 0, n);
            for (int i = 0; i < n; i += 1) {
                Console.Write("{0}", buffer[i]);
            }
            Console.WriteLine();
        }

        static void Main(string[] args) {
            Console.WriteLine("Relay 1.0");

            // analyze command line parameters

            string command = "";
            string src = "";
            string dst = "";
            if (args.Length >= 1) {
                command = args[0];
            }
            if (args.Length >= 2) {
                src = args[1];
            }
            if (args.Length >= 3) {
                dst = args[2];
            }

            // execute command

            try {
                if (command == "version") {
                    //
                } else if (command == "serial-list") {
                    var names = SerialPort.GetPortNames();
                    foreach (string name in names) {
                        Console.WriteLine(name);
                    }
                } else if (command == "serial-test") {
                    SerialTest(src);
                } else if (command == "relay") {
                    var srcUri = new Uri(src);
                    var srcDst = new Uri(dst);
                    Relay(srcUri, srcDst);
                } else {
                    Console.WriteLine("Relay method src dst");
                }
            } catch (Exception e) {
                Console.WriteLine("Exception");
                Console.WriteLine(e);
            }
        }
    }
}
