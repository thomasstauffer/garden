#!/usr/bin/env python3


'''
https://www.ntfs.com/ntfs-partition-boot-sector.htm
https://flylib.com/books/en/2.48.1/ntfs_concepts.html

http://inform.pucp.edu.pe/~inf232/Ntfs/ntfs_doc_v0.5/attributes/index.html

https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/a82e9105-2405-4e37-b2c3-28c773902d85
https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc781134(v=ws.10)
http://inform.pucp.edu.pe/~inf232/Ntfs/ntfs_doc_v0.5/attributes/
'''

from collections import namedtuple
import struct

def clamp(value, min, max):
	if value < min:
		return min
	elif value > max:
		return max
	else:
		return value

def hd(raw):
	for x in range(0, len(raw), 16):
		print('{:08x} '.format(x), end='')
		for y in range(clamp(len(raw) - x, 0, 16)):
			i = x + y
			print('{:02x} '.format(raw[i]), end='')
		for y in range(clamp(len(raw) - x, 0, 16)):
			i = x + y
			print(chr(raw[i]) if (raw[i] > 32) and (raw[i] < 127) else '.', end='')
		print()

def unpack(raw, offset, type):
	size = struct.calcsize(type)
	unpacked = struct.unpack(type, raw[offset:offset + size])
	if len(unpacked) == 1:
		return unpacked[0]
	else:
		return unpacked

filename = 'disk3'
f = open(filename, 'rb')

Partition = namedtuple('Partition', 'bytes_per_sector sectors_per_cluster mft_offset mftmirr_offset')
def part_parse(raw):
	signature = unpack(raw, 3, '4s')
	assert(signature == b'NTFS')

	bytes_per_sector = unpack(raw, 0x0b, 'H')
	sectors_per_cluster = unpack(raw, 0x0d, 'B')

	total_sectors = unpack(raw, 0x28, 'Q')
	mft_cluster = unpack(raw, 0x30, 'Q')
	mftmirr_cluster = unpack(raw, 0x38, 'Q')

	c2b = bytes_per_sector * sectors_per_cluster

	# size = total_sectors * bytes_per_sector

	mft_offset = mft_cluster * c2b
	mftmirr_offset = mftmirr_cluster * c2b

	return Partition(bytes_per_sector, sectors_per_cluster, mft_offset, mftmirr_offset)

MFTEntry = namedtuple('MFDEntry', 'attribute_offset flags used_size allocated_size')
def mftentry_parse(raw):
	signature = unpack(raw, 0, '4s')
	assert(signature == b'FILE')
	attribute_offset = unpack(raw, 20, 'H')
	flags = unpack(raw, 22, 'H') # Flags 1 = In Use / 2 = Dir
	used_size = unpack(raw, 24, 'I')
	allocated_size = unpack(raw, 28, 'I')
	assert(allocated_size == 1024)
	return MFTEntry(attribute_offset, flags, used_size, allocated_size)

Attr = namedtuple('Attribute', 'type type_name length is_non_resident name_length name_offset flags identifier')
ATTR_TYPE_FILE_NAME, ATTR_TYPE_DATA, ATTR_TYPE_END = 0x30, 0x80, 0xFFFFFFFF
def attr_parse(raw):
	type = unpack(raw, 0, 'I')
	length = unpack(raw, 4, 'I')
	non_resisdent_flag = unpack(raw, 8, 'B')
	length_name = unpack(raw, 9, 'B')
	offset_name = unpack(raw, 10, 'H')
	flags = unpack(raw, 12, 'H')
	identifier = unpack(raw, 14, 'H')

	ATTR_TYPE_NAME = {
		0x10: '$STANDARD INFORMATION',
		0x20: '$ATTRIBUTE_LIST',
		ATTR_TYPE_FILE_NAME: '$FILE_NAME',
		0x60: '$VOLUME_NAME',
		0x70: '$VOLUME_INFORMATION',
		ATTR_TYPE_DATA: '$DATA',
		0xB0: '$BITMAP',
		ATTR_TYPE_END: '$END',
	}

	return Attr(type, ATTR_TYPE_NAME[type], length, non_resisdent_flag == 1, length_name, offset_name, flags, identifier)

def attr_name_parse(raw):
	length = unpack(raw, 64, 'B')
	namespace = unpack(raw, 65, 'B')
	filename_utf16 = raw[66:66 + length * 2]
	return filename_utf16.decode('utf-16')


f.seek(0)
raw = f.read(0x50)
part = part_parse(raw)

print(part.mft_offset, part.mftmirr_offset)

mft_entry_size = 1024

f.seek(part.mftmirr_offset + 1024 * 0)
raw = f.read(1024)

mftentry = mftentry_parse(raw)

print(mftentry_parse(raw))

offset = mftentry.attribute_offset
while True:
	attr = attr_parse(raw[offset:])
	print(attr)
	if attr.type == ATTR_TYPE_FILE_NAME:
		print(attr_name_parse(raw[offset + attr.name_offset:]))
	elif attr.type == ATTR_TYPE_END:
		break
	offset += attr.length
