
const CACHE_NAME = 'test';
const CACHE_URLS = [
	'large.png'
];

self.addEventListener('install', (event) => {
	self.skipWaiting();
	event.waitUntil(onInstall());
});

async function onInstall() {
	const cache = await self.caches.open(CACHE_NAME);
	//console.log(cache);
	//console.log(await cache.keys());
	return cache.addAll(CACHE_URLS);
}

self.addEventListener('fetch', event => {
	event.respondWith(onFetch(event.request));
});

async function onFetch(request) {
	const cache = await self.caches.open(CACHE_NAME);
	const cacheResponse = await cache.match(request);
	if (cacheResponse) {
		return cacheResponse;
	}
	return fetch(request).then((response) => {
		cache.put(request, response.clone());
		return response;
	});
}