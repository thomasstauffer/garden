
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

// https://registry.khronos.org/EGL/sdk/docs/man/
#include <EGL/egl.h>
#include <GLES2/gl2.h>

typedef const char* zstring;

// https://registry.khronos.org/EGL/sdk/docs/man/html/eglGetError.xhtml

void check_error() {
    EGLint error = eglGetError();
    if (error != EGL_SUCCESS) {
        fprintf(stderr, "eglGetError %i\n", error);
        exit(EXIT_FAILURE);
    }
}

void check_true(EGLBoolean result) {
    if (result != EGL_TRUE) {
        fprintf(stderr, "result != EGL_TRUE\n");
        exit(EXIT_FAILURE);
    }
}

int main() {
    check_error();

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    check_error();

    eglInitialize(display, NULL, NULL);
    check_error();

    //EGLSurface s = eglGetCurrentSurface(0);

    eglBindAPI(EGL_OPENGL_ES_API); // EGL_OPENGL_API
    check_error();

    EGLint config_size = 256;
    EGLConfig configs[config_size];
    EGLint num_configs = 0;

    eglGetConfigs(display, (EGLConfig*)&configs, config_size, &num_configs);
    check_error();

    EGLConfig config = configs[0];

    EGLint value;
    EGLBoolean result;
    result = eglGetConfigAttrib(display, config, EGL_DEPTH_SIZE, &value);
    check_error();
    printf("EGL_DEPTH_SIZE: %i\n", value);
    result = eglGetConfigAttrib(display, config, EGL_BUFFER_SIZE, &value);
    check_error();
    check_true(result);
    printf("EGL_BUFFER_SIZE: %i\n", value);

    EGLSurface surface = eglCreatePbufferSurface(display, config, NULL);
    check_error();

    EGLContext context = eglCreateContext(display, config, EGL_NO_CONTEXT, NULL);
    check_error();

    eglMakeCurrent(display, surface, surface, context);
    check_error();

    const GLubyte* vendor = glGetString(GL_VENDOR);
    printf("GL_VENDOR=%s\n", vendor);
    const GLubyte* renderer = glGetString(GL_RENDERER);
    printf("GL_RENDERER=%s\n", renderer);
    const GLubyte* version = glGetString(GL_VERSION);
    printf("GL_VERSION=%s\n", version);
    const GLubyte* shading_language_version = glGetString(GL_SHADING_LANGUAGE_VERSION);
    printf("GL_SHADING_LANGUAGE_VERSION=%s\n", shading_language_version);

    return EXIT_SUCCESS;
}
