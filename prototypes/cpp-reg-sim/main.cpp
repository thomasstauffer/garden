
#include <iostream>

struct Reg {
	uint32_t _value;
	Reg(uint32_t value) : _value(value) {
	}

	void operator=(uint32_t value) {
		std::cout << "write" << std::endl;
		_value = value;
	}

	operator uint32_t() const {
		std::cout << "read" << std::endl;
		return _value;
	}
};

int main() {
	Reg A(12);
	std::cout << A._value << std::endl;
	A = 13;
	std::cout << A._value << std::endl;
	uint32_t x = A;
	std::cout << x << std::endl;
}
