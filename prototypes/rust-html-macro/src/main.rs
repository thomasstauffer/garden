// may not be used on the stable release channel
//#![feature(trace_macros)]
//#![feature(log_syntax)]

macro_rules! html2 {
	($t:expr, $c:expr) => (concat!("<", $t, ">", $c, "</", $t, ">"));
}

macro_rules! htmln {
	($t:ident $($c:expr)*) => (concat!("<", stringify!($t), ">", $( $c ),* , "</", stringify!($t), ">"));
}

macro_rules! htmln2 {
	( $x:ident ) => ( stringify!($x) );

	( [ $($x:tt)* ] ) => ( htmln2!( $($x ) * ) );

	( $tag:ident $($c:tt)* ) => ( concat!("<", stringify!($tag), ">",  $( htmln2!($c) ),* ,"</", stringify!($tag), ">") );

	( $x:expr ) => ( $x );
}

fn main() {
	//log_syntax!("hello log_syntax", 1, true);

	//trace_macros!(false); // true

	let hw1 = "Hello World";
	let hw2 = String::from("Hello World");

	let h2 = html2!["body", html2!["p", "Hello"]];

	let hn = htmln![body "Huhu" htmln![p "Hello"] htmln![p htmln![i "World"]]];

	let hn2 = htmln2!(body [p "Hello"] [p [i "World !"]]);

	//trace_macros!(false);

	println!("{} {}", hw1, hw2);
	println!("{}", h2);
	println!("{}", hn);
	println!("{}", hn2);
}
