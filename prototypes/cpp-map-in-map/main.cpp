
#include <map>
#include <unordered_map>

struct my {
public:
	bool operator==(const my& other) {
		return true;
	}

	int a;
	int b;
};

namespace std {
    template<>
    struct hash<my> {
        std::size_t operator()(const my& obj) const {
            size_t ha = std::hash<int>()(obj.a);
            size_t hb = std::hash<int>()(obj.b);
			return (ha << 0) ^ (ha << 1);
        }
    };
}

int main() {
	std::unordered_map<my, std::unordered_map<int, int>> m;

	return 0;
}
