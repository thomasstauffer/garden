﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

[TestClass]
public class MyTest
{
	[TestMethod]
	public void FirstTest()
	{
		Assert.AreEqual(1, 1);
		Assert.AreEqual(2, 2);
	}

	[TestMethod]
	public void SecondTest()
	{
		Assert.AreEqual(1, 1);
		Assert.AreEqual(2, 2);
	}
}
