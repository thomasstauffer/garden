difference() {
    cube([4, 4, 2.5]);
    translate([0.1, 0.1, 0.1])
        cube([3.8, 3.8, 2.3]);
    // windows
    translate([2, 0, 1.4])
        cube([2, 1, 1.5], center=true);
    translate([2, 4, 1.4])
        cube([2, 1, 1.5], center=true);
    // door
    translate([3.8, 1.0, 0.1])
        cube([1, 1.5, 2.3]);
}
