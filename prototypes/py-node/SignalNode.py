#!/usr/bin/env python3

'''
open questions

- recursion e.g. 
- a node cannot have more than one input at the moment e.g. sum node where path
  a has more nodes than path b
- Delay Node?
- 10 gain nodes after each other, should not be delayed longer than just 1 gain
  node (assuming the calculation is faster then 1 sample period)
- GainGain should internalls also use connect, only this would allow composition
  for bigger systems
- CrossFading between two sounds
- WebAudio needs DelayNode if there is a cycle -> interesting implementation detail!
- https://github.com/WebAudio/web-audio-api/issues/75
- What is noflojs.org doing?
'''

SAMPLERATE = 10

class SignalNode:
	def __init__(self):
		self.connected = []

	def connect(self, node):
		self.connected.append(node)
		return node

	def emit(self, value):
		for node in self.connected:
			node.emit(node.process(value))

	# called for every sample, should be overriden by subclasses
	def process(self, value):
		return value

class PrintNode(SignalNode):
	def __init__(self, enable=True):
		super().__init__()
		self.enable = enable

	def process(self, value):
		if self.enable:
			print(value)
		return value

class CollectNode(SignalNode):
	def __init__(self):
		super().__init__()
		self.collection = []

	def process(self, value):
		self.collection.append(value)
		return value

class GainNode(SignalNode):
	def __init__(self, gain):
		super().__init__()
		self.gain = gain

	def process(self, value):
		return value * self.gain

class SquareNode(SignalNode):
	def __init__(self, frequency):
		super().__init__()
		self.frequency = frequency
		self._n = 0

	def generate(self):
		period = int(SAMPLERATE / self.frequency)
		if (self._n % period) < (period / 2):
			output = 0
		else:
			output = 1
		self.emit(output)
		self._n += 1

class DelayNode(SignalNode):
	def __init__(self, delayTime):
		super().__init__()
		self.delayTime = delayTime

class AverageNode(SignalNode):
	def __init__(self, delayTime):
		super().__init__()

class GainGainNode(SignalNode):
	def __init__(self, gain1, gain2):
		super().__init__()
		self.gain1Node = GainNode(gain1)
		self.gain2Node = GainNode(gain2)
		#self.gain1Node.connect(gain2Node)

	def process(self, value):
		value = self.gain1Node.process(value)
		value = self.gain2Node.process(value)
		return value

def graph(node):
	return node.__class__.__name__ + '(' + ','.join(map(graph, node.connected)) + ')'

def main():
	gen1 = SquareNode(frequency=2.0)
	print1 = PrintNode(False)
	gain1 = GainNode(2)
	gain2 = GainNode(3)
	print2 = PrintNode(False)
	gaingain1 = GainGainNode(4, 5)
	collect1 = CollectNode()
	collect2 = CollectNode()

	gen1.connect(print1).connect(collect1).connect(gain1).connect(gain2).connect(gaingain1).connect(print2).connect(collect2)

	print(graph(gen1))

	for i in range(2 * SAMPLERATE):
		gen1.generate()
	print(collect1.collection, collect2.collection)

	if False:
		import plotly.express as px
		fig = px.line(y=[collect1.collection, collect2.collection])
		fig.show()

main()
