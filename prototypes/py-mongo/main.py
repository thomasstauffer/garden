#!/usr/bin/env python3

"""
Start Mongo

docker run -p 27017:27017 -v $PWD/db:/data/db mongo

Run Test

python3 main.py

PyMongo Documentation

https://pymongo.readthedocs.io/en/stable/api/index.html

Mongo Commands

https://docs.mongodb.com/manual/reference/command/
"""

import pymongo  # pip3 install mongodb
import random
import time

client = pymongo.MongoClient('mongodb://localhost:27017/')
print('client', client)
print('list databases', client.list_database_names())
db_test = client['test']

def small_add():
	col_small = db_test['small']
	data = {'text': 'Hello World'}
	col_small.insert_one(data)

def small_info():
	col_small = db_test['small']
	print('count', col_small.count_documents({}))

ADD_NUMBER_OF_LARGE = 5

def large_add():
	col_large = db_test['large']
	col_large.create_index('111')  # only adds an index if it is not existing yet
	t1 = time.time()
	for n in range(ADD_NUMBER_OF_LARGE):
		data_large = {}
		for i in range(1000):
			key = str(i)
			data_large[key] = str(random.randint(1, 1000))  # random string from '1' up to '1000'
		col_large.insert_one(data_large)
	t2 = time.time()
	print('insert time [s]', t2 - t1)

def large_query():
	col_large = db_test['large']
	# 1. has an index, 2. has no index, 3. has also no index, maybe at the end of the document, 4. does not exist
	for key in ['111', '222', '999', '1111']:
		print('key', key)
		t1 = time.time()
		results = col_large.find({key: '1'})
		result_count = len(list(results))
		t2 = time.time()
		print('result count', result_count)
		print('query time', t2 - t1)

def large_info():
	col_large = db_test['large']
	count = col_large.count_documents({})
	print('count documents', count)
	print('index size', db_test.command('collstats', 'large')['indexSizes'])  # https://docs.mongodb.com/manual/reference/command/

def db_info():
	print('dbstats', db_test.command('dbstats'))

small_add()
small_info()
large_add()
large_query()
large_info()
db_info()
