#!/usr/bin/env python3

def load(filename):
	with open(filename, 'r') as file:
		return [ row.strip() for row in file.readlines() ]

def show(field):
	print("*")
	for row in field:
		print(row)
	print("*")

def transpose(field):
	return [ ''.join([ row[i] for row in field ]) for i in range(10) ]

def invert(O):
	return 'X' if O == 'O' else 'O'

def single_row(row, O):
	X = invert(O)

	# .OO -> XOO
	row = ''.join([ X if row[i:i+3] == '.'+O+O else row[i] for i in range(10) ])

	# OO. -> OOX
	row = ''.join([ X if row[i-2:i+1] == O+O+'.' else row[i] for i in range(10) ])

	# O.O -> OXO
	row = ''.join([ X if row[i-1:i+2] == O+'.'+O else row[i] for i in range(10) ])

	# 4 * O, 5 * X -> fill in the missing O
	if (row.count(O) == 4) and (row.count(X) == 5):
		row = ''.join([ O if e == '.' else e for e in row ])

	# 4 * O -> try to fill in an O, fill the rest with X, does this result in an XXX?
	if row.count(O) == 4:
		for i in range(10):
			if row[i] == '.':
				m = ''.join([ (O if i == j else X) if row[j] == '.' else row[j] for j in range(10) ])
				if (X+X+X) in m:
					row = row[:i] + X + row[i+1:]

	return row

def double_row(row, field):
	# compare rows, if 8 cells are identical, the other 2 must differ
	if row.count('.') == 2:
		for row_other in field:
			if row_other.count('.') == 0:
				count_eq = [ row[i] == row_other[i] for i in range(10) ].count(True)
				if count_eq == 8:
					return ''.join([ row[i] if row[i] == row_other[i] else invert(row_other[i]) for i in range(10) ])

	return row

def rule_rows(field):
	def rule_rows_h(field):
		field = [ single_row(row, 'O') for row in field ]
		field = [ single_row(row, 'X') for row in field ]
		field = [ double_row(row, field) for row in field ]
		return field

	field = rule_rows_h(field)
	field = transpose(rule_rows_h(transpose(field)))
	return field

field = load('binoxxo-hard-1.txt')
show(field)
for i in range(100):
	field = rule_rows(field)
	#show(field)
show(field)
