#!/usr/bin/env python3

import base45 # pip3 install base45
import cbor2 # pip3 install cbor2
import zlib
import json
import pprint
import base64

QRCODE = 'HC1:...<INSERT_YOUR_DATA_HERE>...'

assert QRCODE[:4] == 'HC1:'
s = base45.b45decode(QRCODE[4:])
t = zlib.decompress(s)
u = cbor2.loads(t)
v = cbor2.loads(u.value[2])
pprint.pprint(v)
