
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <variant>

/*
Calling lock twice in the same thread yields undefined behavior, which makes it
kinda hard to detect potential issues. It seems that mostly the
second lock seems to be ignored.

_mutex.lock()
_mutex.lock()
_mutex.lock()

just seems to work, if there is not at least one thread created. If pthread is
called manually pthread_mutex_lock, the behaviour is the same. Lock can be
called without any deadlock. Only if e.g. pthread_mutex_init is called (or if a
thread is created), then the behaviour changes and the the program ends in a
deadlock.
*/

// #define TRY_TO_DEADLOCK

template <typename T>
class db1 {
private:
	std::unordered_map<int, T> _map;

#ifdef TRY_TO_DEADLOCK
	std::mutex _mutex;
#else
	// alternative idea, instead of using a recursive mutex: pass _map to
	// callable in atomic (or specialized wrapper which does not lock)
	std::recursive_mutex _mutex;
#endif

public:
	explicit db1() {
	}

	void set(int key, const T& value) {
		std::scoped_lock lock(_mutex);
		_map[key] = value;
	}

	T get(int key, const T& default_value) {
		std::scoped_lock lock(_mutex);
		auto it = _map.find(key);
		return it == _map.end() ? default_value : it->second;
	}

	void atomic(const std::function<void()>& callable) {
		std::scoped_lock lock(_mutex);
		callable();
	}
};

template <typename T>
class db2 {
private:
	T& _data;
	std::mutex _mutex;

public:
	db2(T& data) : _data(data) {
	}

	template<typename U>
	U get(U T::*pointer) {
		std::scoped_lock lock(_mutex);
		return _data.*pointer;
	}

	template<typename U>
	void set(U T::*pointer, const U& value) {
		std::scoped_lock lock(_mutex);
		_data.*pointer = value;
	}

	template <typename C>
	void atomic(const C& callable) {
		std::scoped_lock lock(_mutex);
		callable(_data);
	}
};

struct mydata {
	int a;
	int b;
	bool c;
};

void thread_main(){};

int main() {
	db1<int> db1;

	mydata myd{3, 4, false};
	db2<mydata> db2(myd);

	std::thread thread(thread_main);

	std::cout << "db1" << std::endl;

	db1.set(1, 3);

	std::cout << "get " << db1.get(1, 666) << std::endl;
	std::cout << "get default " << db1.get(2, 666) << std::endl;

	int result = 0;
	db1.atomic([&] {
		int v = db1.get(1, 666);
		db1.set(1, v + 1);
		result = db1.get(1, 666);
	});
	std::cout << "inc " << result << std::endl;

	std::cout << "db2" << std::endl;

	std::cout << "get " << db2.get(&mydata::a) << std::endl;
	db2.set(&mydata::a, 9);
	std::cout << "get " << db2.get(&mydata::a) << std::endl;

	db2.atomic([&](mydata& data) { result = data.a + data.b; });
	std::cout << "calc " << result << std::endl;

	thread.join();
}
