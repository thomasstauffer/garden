
Idea: To have a simple in threadsafe in memory database for a multithreaded
programs. Whenever something must be shared, it would be in the database.

As a consequence would be, that data is then moved out of classes and they get
more and more stateless.

auto start = std::chrono::steady_clock::now();
...
auto stop = std::chrono::steady_clock::now();
auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
std::cout << elapsed.count() << std::endl;
