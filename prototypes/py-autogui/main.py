import time
import sys
import pyautogui

if False:
	pyautogui.screenshot('screenshot.png')
	sys.exit(1)


time.sleep(5.0)

targetxlo = 880
targetxhi = 900
targety = {
	'shooting': 418,
	'medicine': 527,
	'cooking': 555,
	'growing': 610,
	'artistic': 665,
	'research': 715,
}

for i in range(500):
	pyautogui.click(x=846, y=286)
	time.sleep(0.05)
	image = pyautogui.screenshot()
	def isset(x, y):
		pixel = image.getpixel((x, targety[y]))
		return pixel[1] == 66

	if image.getpixel((488, 527))[1] == 44:
		continue

	if isset(targetxhi+20, 'shooting'):
		break
	if isset(targetxhi, 'cooking'):
		break
	if isset(targetxhi+50, 'research'):
		break
