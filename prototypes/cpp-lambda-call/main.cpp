
#include <iostream>

struct alpha {
	void any(int a) {
		std::cout << a << std::endl;
	}

	void any(int a, int b) {
		std::cout << a << " " << b << std::endl;
	}
};

template<typename F, typename T, typename A1>
void callinh1(F f, void(T::*)(A1) const) {
	f(111);
}

template<typename F, typename T, typename A1, typename A2>
void callinh1(F f, void(T::*)(A1, A2) const) {
	f(111, 222);
}

template<typename F>
void callin(F f) {
	callinh1(f, &F::operator());
}

int main() {
	alpha an_alpha;
	
	callin([&](int a){ an_alpha.any(a); });
	callin([&](int a, int b){ an_alpha.any(a, b); });
	
	return 0;
}
