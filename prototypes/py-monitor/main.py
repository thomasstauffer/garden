

import time
import asyncio
import tornado.platform.asyncio
import tornado.ioloop
import tornado.web
import tornado.websocket
import json
import random


class MyWebSocketServer(tornado.websocket.WebSocketHandler):
	def __init__(self, application, request, **kwargs):
		self.clients = kwargs['clients']
		del kwargs['clients']
		super(MyWebSocketServer, self).__init__(application, request, **kwargs)

	def check_origin(self, origin):
		return True

	def open(self):
		print("WebSocket Opened")
		self.clients.append(self)

	#def on_message(self, message):
	#	self.write_message("You said: " + message)

	def on_close(self):
		print("WebSocket Closed")
		self.clients.remove(self)


class MyMainServer(tornado.web.RequestHandler):
	def get(self):
		html = open("main.html", "rb").read()
		self.write(html)
		#self.write("Hello Monitor" + EXAMPLE)
		#self.set_header('Content-Type', 'text/plain; charset=utf-8')  # Content-Type is somehow automagically set?

class MyWorker:
	def __init__(self, clients):
		self.clients = clients

	async def run(self):
		while True:
			await asyncio.sleep(2.0)
			message = {
				'time': time.time(),
				'log': ['Log ' + str(time.time())],
				'status': {
					'alpha': int(1000 * random.random()),
					'beta': int(1000 * random.random()),
				}
			}
			for client in self.clients:
				client.write_message(json.dumps(message))

def main():
	tornado.platform.asyncio.AsyncIOMainLoop().install()
	clients = []
	app = tornado.web.Application([
		(r"/", MyMainServer),
		(r"/websocket", MyWebSocketServer, {'clients': clients}),
	])
	app.listen(9999)
	loop = asyncio.get_event_loop()
	
	# just for windows to allow Ctrl+C
	async def interrupt():
		while True:
			await asyncio.sleep(0.1)
			
	print("Ready To Run")
	loop.create_task(MyWorker(clients).run())
	loop.create_task(interrupt())
	loop.run_forever()


if __name__ == "__main__":
	main()
