
= Idea

Applications can send a declarative UI JSON to a TCP Port. The py-monitor, send this information via web-socket to all web-browsers. The browser converts this to a goodlooking read-only UI.

Use Case: E.g. on a Raspberry Pi, which itself has some attached hardware, but has no UI itself to display the state of the Hardware.

Use mqtt?
