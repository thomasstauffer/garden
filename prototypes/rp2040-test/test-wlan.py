
import time
import sys
import socket

print(sys.implementation)

import network
print(dir(network))

ap = network.WLAN(network.AP_IF)
print(dir(ap))
#ap.config(ssid='Thomas', password='123456789')
ap.config(essid='Thomas')
ap.config(password='123456789')
ap.active(True)
#wlan.connect(ssid='Thomas')

while ap.active() == False:
  pass

print('Connection successful')
print(ap.ifconfig())

def web_page():
  html = """<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
  <body><h1>Hello, World!</h1></body></html>"""
  return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
  conn, addr = s.accept()
  print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  print('Content = %s' % str(request))
  response = web_page()
  conn.send('HTTP/1.0 200 OK\r\n\r\n' + response)
  conn.close()
