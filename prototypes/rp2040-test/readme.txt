
git clone https://github.com/raspberrypi/pico-sdk.git
git clone https://github.com/raspberrypi/picotool

# Pico

https://www.pi-shop.ch/raspberry-pi-pico

https://micropython.org/download/RPI_PICO/

2 x Cortex M0+ 133 MHz
264 kB SRAM
2 MB Flash
8 PIO

https://nuttx.apache.org/docs/latest/quickstart/install.html
https://nuttx.apache.org/docs/latest/platforms/arm/rp2040/index.html
export PICO_SDK_PATH=...
./tools/configure.sh -l raspberrypi-pico-w:usbnsh

# Pico W

https://www.pi-shop.ch/raspberry-pi-pico-w

https://micropython.org/download/RPI_PICO_W/

@ Pico 2

WLAN CYW43439
IEEE 802.11 b/g/n
Bluetooth 5.2
WPA3
SoftAP (4 Clients)

https://www.pi-shop.ch/raspberry-pi-pico-2

https://micropython.org/download/RPI_PICO2/

2 x Cortex M33 / 2 x RISC-V Hazard3 processors 150 MHz
520 SRAM
4 MB Flash
