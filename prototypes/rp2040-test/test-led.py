
import time
import machine

led = machine.Pin(25, machine.Pin.OUT)

while True:
    time.sleep(0.5)
    led.toggle()
