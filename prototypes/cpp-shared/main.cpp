
#include <functional>
#include <iostream>

/*
Put a lock inside a class to use the class optionally with/without locks

Ideally the interface should stay the same, so composition works the same.

struct IOther {}

struct  Manager {
	explicit Manager(IOther other) {}
}

struct Other : public IOther {}

Manager m1(otherWithLock)
Manager m2(otherWithoutLock)
*/

template <class T>
struct shared {
	T& _instance;

	shared(T& instance) : _instance(instance) {
	}

	using callable_type = std::function<void(T&)>;

	void doit(callable_type callable) {
		std::cout << "lock" << std::endl;
		callable(_instance);
		std::cout << "unlock" << std::endl;
	}
};

struct test_interface {
	virtual ~test_interface(){};
	virtual void hello() = 0;
};

struct test : public test_interface {
	void hello() override {
		std::cout << "Hello" << std::endl;
	}
};

int main() {
	test mytest;

	test_interface& myreference = mytest;
	myreference.hello();

	shared<test> myshared(mytest);
	myshared.doit([](auto& t) { t.hello(); });

	return 0;
}
