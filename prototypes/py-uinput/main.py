#!/usr/bin/env python3

import evdev # apt install python3-evdev
import sys
import time

# https://python-evdev.readthedocs.io/en/latest/tutorial.html#injecting-input

e = evdev.ecodes

# following were all tested on an Mint X11 system

k = e.KEY_HOME # move cursor to start of line
k = e.KEY_MAIL # mail program
k = e.KEY_WWW # browser
k = e.KEY_CALC # calculator
k = e.KEY_FILE # file manager
k = e.KEY_PRINT # prinscreen
k = e.KEY_HOMEPAGE # browser opens homepage
k = e.KEY_POWER # ask for restart/powerdown/...


cap = {
	e.EV_KEY : [e.BTN_LEFT, e.BTN_RIGHT],
	e.EV_ABS : [
		(e.ABS_X, evdev.AbsInfo(value=0, min=0, max=255, fuzz=0, flat=0, resolution=10)),
		(e.ABS_Y, evdev.AbsInfo(0, 0, 255, 0, 0, 10)),
	]
}
#ui = evdev.UInput(cap)

ui = evdev.UInput()

time.sleep(3.0)
print('Start')

if True:
	for name in ['KEY_A', 'KEY_B', 'KEY_C']:
		print(name)
		k = getattr(evdev.ecodes, name)
		ui.write(e.EV_KEY, k, 1)
		ui.write(e.EV_KEY, k, 0)
		ui.syn()
		time.sleep(1.0)
	
if False:
	print(dir(evdev.ecodes))
	for name in dir(evdev.ecodes):
		if name.startswith('KEY_'):
			print(name)
			k = getattr(evdev.ecodes, name)
			ui.write(e.EV_KEY, k, 1)
			ui.write(e.EV_KEY, k, 0)
			ui.syn()
			time.sleep(0.1)

if False:
	ui.write(e.EV_ABS, e.ABS_X, 0)
	ui.write(e.EV_ABS, e.ABS_Y, 0)
	ui.syn()
	ui.write(e.EV_KEY, e.BTN_RIGHT, 1)
	ui.syn()
	ui.write(e.EV_KEY, e.BTN_RIGHT, 0)
	ui.syn()

print('Stop')
time.sleep(3.0)

ui.close()
