class_name Network
extends Node

var _players

signal on_event(id, local, method, param)

func _init():
	_players = {}
	
func is_server():
	return get_tree().is_network_server()

func start_server(server_port: int, max_players: int):
	_connect()
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(server_port, max_players)
	get_tree().network_peer = peer
	
func start_client(server_ip: String, server_port: int):
	_connect()
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, server_port)
	get_tree().network_peer = peer
	
func _connect():
	get_tree().connect("network_peer_connected", self, "_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_network_peer_disconnected")

func _network_peer_connected(id):
	print("_network_peer_connected ", id)
	_players[id] = id
	emit_signal("on_event", id, false, "connected", {})
	
func _network_peer_disconnected(id):
	print("_network_peer_disconnected ", id)
	_players.erase(id)
	emit_signal("on_event", id, false, "disconnected", {})
	
func send(method, param):
	emit_signal("on_event", get_tree().get_network_unique_id(), true, method, param)
	for id in _players:
		rpc_id(id, "_recv", method, param)
	
remote func _recv(method, param):
	var id =  get_tree().get_rpc_sender_id()
	emit_signal("on_event", id, false, method, param)
