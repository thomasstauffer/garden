extends Node

const SERVER_PORT = 54321
const MAX_PLAYERS = 5
const SERVER_IP = "127.0.0.1"

var rng = RandomNumberGenerator.new()
var node_player_template = null;
var main_player = null;
var network = null
var myplayers = {}
var my_name = "Unknown"

func _ready():
	print("_ready")
	rng.randomize();
	node_player_template = preload("res://Player.tscn")
	network = Network.new()
	get_node(".").add_child(network)
	network.connect("on_event", self, "_on_event")
	var args = OS.get_cmdline_args()
	
	if args.size() >= 2 and args[1] == "server":
		my_name = "Server"
		network.start_server(SERVER_PORT, MAX_PLAYERS)
	else:
		my_name = "Client" + str(rng.randi_range(1, 9)) + str(rng.randi_range(1, 9))
		network.start_client(SERVER_IP, SERVER_PORT)
	print("Name:", my_name)

func _process(delta):
	var dx = 0
	var dy = 0
	if Input.is_action_pressed("ui_left"):
		dx = -1
	if Input.is_action_pressed("ui_right"):
		dx = 1
	if Input.is_action_pressed("ui_up"):
		dy = -1
	if Input.is_action_pressed("ui_down"):
		dy = 1
	if dx != 0 or dy != 0:
		var offset = Vector2(dx, dy) * 5.0
		network.send("position", {"position": main_player.position + offset})

func add_player(id, name):
	var node_player = node_player_template.instance()
	node_player.set_display_name(name)
	node_player.set_name(str(id))
	node_player.set_network_master(id)  # works also without calling this function
	get_node(".").add_child(node_player)
	return node_player
	
func _on_event(id, local, method, param):
	if method == "connected":
		# TODO if server send whole list?
		network.send("register", {"name": my_name})
	elif method == "disconnected":
		myplayers[id].queue_free()
		myplayers.erase(id)
	elif method == "register":
		var node = add_player(id, param.name)
		if local:
			main_player = node
		myplayers[id] = node
		node.position = Vector2(100, 100)
	elif method == "position":
		myplayers[id].position = param.position
