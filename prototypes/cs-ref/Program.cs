﻿using System;
using System.Linq;

class Program
{
    public static ref int GetReference(ref int r1, ref int r2)
    {
        return ref r1;
    }

    public static void Main(string[] args)
    {
        // https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/ref

        {
            var longLive = 1;
            ref var longLiveReference = ref longLive;
            {
                var shortLive = 2;
                _ = shortLive;

                // fail - narrower scope
                // longLiveReference = ref shortLive;
            }

            _ = longLiveReference;
        }

        {
            var i = 1;
            ref var iRef = ref i;
            ref var result = ref i;
            {
                var j = 2;
                _ = j;
                // fail - narrower scope
                // result = ref GetReference(ref iRef, ref j);
            }
        }
        
        // https://em-tg.github.io/csborrow/
    }
}
