#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dbus/dbus.h>

/*
Documentation

https://dbus.freedesktop.org/
https://dbus.freedesktop.org/doc/api/html/
https://dbus.freedesktop.org/doc/api/html/group__DBusShared.html
https://dbus.freedesktop.org/doc/dbus-java/api/org/freedesktop/DBus.Properties.html
https://dbus.freedesktop.org/doc/dbus-java/api/org/freedesktop/DBus.Introspectable.html

Testing

dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.freedesktop.DBus.Properties.GetAll string:org.example.TestInterface
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.freedesktop.DBus.Properties.Get string:org.example.TestInterface string:Version
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.freedesktop.DBus.Introspectable.Introspect
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.example.TestInterface.Ping
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.example.TestInterface.Echo string:Hello
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.example.TestInterface.EmitSignal
dbus-send --session --print-reply --dest=org.example.TestServer /org/example/TestObject org.example.TestInterface.Quit

Based On

https://github.com/fbuihuu/samples-dbus
https://www.matthew.ath.cx/misc/dbus
*/

typedef const char* zstring;

const zstring VERSION = "1.0";

bool g_quit = false;

zstring introspect_xml =
	DBUS_INTROSPECT_1_0_XML_DOCTYPE_DECL_NODE
	"<node>\n"

	"  <interface name='org.freedesktop.DBus.Introspectable'>\n"
	"    <method name='Introspect'>\n"
	"      <arg name='data' type='s' direction='out'/>\n"
	"    </method>\n"
	"  </interface>\n"

	"  <interface name='org.freedesktop.DBus.Properties'>\n"
	"    <method name='Get'>\n"
	"      <arg name='interface' type='s' direction='in'/>\n"
	"      <arg name='property' type='s' direction='in'/>\n"
	"      <arg name='value' type='s' direction='out'/>\n"
	"    </method>\n"
	"    <method name='GetAll'>\n"
	"      <arg name='interface' type='s' direction='in'/>\n"
	"      <arg name='properties' type='a{sv}' direction='out'/>\n"
	"    </method>\n"
	"  </interface>\n"

	"  <interface name='org.example.TestInterface'>\n"
	"    <property name='Version' type='s' access='read'/>\n"
	"    <method name='Ping'>\n"
	"      <arg type='s' direction='out'/>\n"
	"    </method>\n"
	"    <method name='Echo'>\n"
	"      <arg name='string' direction='in' type='s'/>\n"
	"      <arg type='s' direction='out'/>\n"
	"    </method>\n"
	"    <method name='EmitSignal'>\n"
	"    </method>\n"
	"    <method name='Quit'>\n"
	"    </method>\n"
	"    <signal name='OnEmitSignal'>\n"
	"    </signal>"
	"  </interface>\n"

	"</node>\n";

DBusHandlerResult get_properties_handler(zstring property, DBusConnection* conn, DBusMessage* reply) {
	if (!strcmp(property, "Version")) {
		dbus_message_append_args(reply, DBUS_TYPE_STRING, &VERSION, DBUS_TYPE_INVALID);
	} else {
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}

	if (dbus_connection_send(conn, reply, NULL)) {
		return DBUS_HANDLER_RESULT_HANDLED;
	}

	return DBUS_HANDLER_RESULT_NEED_MEMORY;
}

DBusHandlerResult getall_properties_handler(DBusConnection* conn, DBusMessage* reply) {
	DBusMessageIter array;
	DBusMessageIter dict;
	DBusMessageIter iter;
	DBusMessageIter variant;
	const zstring version = "Version";

	dbus_message_iter_init_append(reply, &iter);
	dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "{sv}", &array);
	dbus_message_iter_open_container(&array, DBUS_TYPE_DICT_ENTRY, NULL, &dict);
	dbus_message_iter_append_basic(&dict, DBUS_TYPE_STRING, &version);
	dbus_message_iter_open_container(&dict, DBUS_TYPE_VARIANT, "s", &variant);
	dbus_message_iter_append_basic(&variant, DBUS_TYPE_STRING, &VERSION);
	dbus_message_iter_close_container(&dict, &variant);
	dbus_message_iter_close_container(&array, &dict);
	dbus_message_iter_close_container(&iter, &array);

	if (dbus_connection_send(conn, reply, NULL)) {
		return DBUS_HANDLER_RESULT_HANDLED;
	}

	return DBUS_HANDLER_RESULT_NEED_MEMORY;
}

// TODO handle all cases in the same way

DBusHandlerResult message_handler(DBusConnection* conn, DBusMessage *message, void* data) {
	DBusHandlerResult result;
    DBusMessage* reply = NULL;
	DBusError err;
	bool* quit = (bool*)data;

	fprintf(stderr, "Request: %s.%s %s\n", 
		dbus_message_get_interface(message),
		dbus_message_get_member(message),
		dbus_message_get_path(message));

	dbus_error_init(&err);

	if (dbus_message_is_method_call(message, DBUS_INTERFACE_PROPERTIES, "Get")) {
		zstring interface;
		zstring property;

		if (!dbus_message_get_args(message, &err, DBUS_TYPE_STRING, &interface, DBUS_TYPE_STRING, &property, DBUS_TYPE_INVALID)) {
			goto fail;
		}

		if (!(reply = dbus_message_new_method_return(message))) {
			goto fail;
		}

		result = get_properties_handler(property, conn, reply);
		dbus_message_unref(reply);
		return result;
	}  else if (dbus_message_is_method_call(message, DBUS_INTERFACE_PROPERTIES, "GetAll")) {
		if (!(reply = dbus_message_new_method_return(message))) {
			goto fail;
		}

		result = getall_properties_handler(conn, reply);
		dbus_message_unref(reply);
		return result;
	} else if (dbus_message_is_method_call(message, DBUS_INTERFACE_INTROSPECTABLE, "Introspect")) {
		if (!(reply = dbus_message_new_method_return(message))) {
			goto fail;
		}

		dbus_message_append_args(reply, DBUS_TYPE_STRING, &introspect_xml, DBUS_TYPE_INVALID);
	} else if (dbus_message_is_method_call(message, "org.example.TestInterface", "Ping")) {
		zstring pong = "Pong";

		if (!(reply = dbus_message_new_method_return(message))) {
			goto fail;
		}

		dbus_message_append_args(reply, DBUS_TYPE_STRING, &pong, DBUS_TYPE_INVALID);
	} else if (dbus_message_is_method_call(message, "org.example.TestInterface", "Echo")) {
		zstring msg;

		if (!dbus_message_get_args(message, &err, DBUS_TYPE_STRING, &msg, DBUS_TYPE_INVALID)) {
			goto fail;
		}

		if (!(reply = dbus_message_new_method_return(message))) {
			goto fail;
		}

		dbus_message_append_args(reply, DBUS_TYPE_STRING, &msg, DBUS_TYPE_INVALID);
	} else if (dbus_message_is_method_call(message, "org.example.TestInterface", "EmitSignal")) {
		if (!(reply = dbus_message_new_signal("/org/example/TestObject", "org.example.TestInterface", "OnEmitSignal"))) {
			goto fail;
		}

		if (!dbus_connection_send(conn, reply, NULL)) {
			return DBUS_HANDLER_RESULT_NEED_MEMORY;
		}
		reply = dbus_message_new_method_return(message);
	} else if (dbus_message_is_method_call(message, "org.example.TestInterface", "Quit")) {
		*quit  = true;
		reply = dbus_message_new_method_return(message);
	} else {
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}

fail:
	if (dbus_error_is_set(&err)) {
		if (reply) {
			dbus_message_unref(reply);
		}
		reply = dbus_message_new_error(message, err.name, err.message);
		dbus_error_free(&err);
	}

	if (!reply) {
		return DBUS_HANDLER_RESULT_NEED_MEMORY;
	}

	result = DBUS_HANDLER_RESULT_HANDLED;
	if (!dbus_connection_send(conn, reply, NULL)) {
		result = DBUS_HANDLER_RESULT_NEED_MEMORY;
	}
	dbus_message_unref(reply);

	return result;
}

const DBusObjectPathVTable vtable = {
	.message_function = message_handler
};

bool init(DBusConnection* conn, DBusError* err, void* data) {
	int result;

	if (!conn) {
		fprintf(stderr, "dbus_bus_get: %s\n", err->message);
		return false;
	}

	result = dbus_bus_request_name(conn, "org.example.TestServer", DBUS_NAME_FLAG_REPLACE_EXISTING , err);
	if (result != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
		fprintf(stderr, "dbus_bus_request_name: %s\n", err->message);
		return false;
	}

	if (!dbus_connection_register_object_path(conn, "/org/example/TestObject", &vtable, data)) {
		fprintf(stderr, "dbus_connection_register_object_path\n");
		return false;
	}

	return true;
}

int main(void) {
	DBusConnection* conn = NULL;
	DBusError err;
	bool quit = false;

	dbus_error_init(&err);
	conn = dbus_bus_get(DBUS_BUS_SESSION, &err);

	if (!init(conn, &err, &quit)) {
		dbus_error_free(&err);
		return EXIT_FAILURE;
	}

	while (!quit) {
		dbus_connection_read_write_dispatch(conn, 100);
	}

	return EXIT_SUCCESS;
}
