import asyncio
from aiohttp import web

async def background_task():
	while True:
		print("Background ...")
		await asyncio.sleep(5.0)

async def handle(request):
	name = request.match_info.get('name', 'World')
	text = f'Hello {name}'
	return web.Response(text=text)

async def start_background_tasks(app):
	app['background_task'] = asyncio.create_task(background_task())

async def cleanup_background_tasks(app):
	app['background_task'].cancel()
	await app['background_task']

def main():
	app = web.Application()
	app.router.add_get('/', handle)
	app.router.add_get('/{name}', handle)

	app.on_startup.append(start_background_tasks)
	app.on_cleanup.append(cleanup_background_tasks)

	web.run_app(app)

main()
