#!/usr/bin/env python3

from scapy.all import *

def tcp_scan(target_ip, ports):
	for port in ports:
		syn_packet = IP(dst=target_ip) / TCP(dport=port, flags='S')
		response = sr1(syn_packet, timeout=1, verbose=0)

		if response is None:
			print(f'TCP port {port} is filtered (no response).')
		elif response.haslayer(ICMP):
			print(f'TCP port {port} is filtered (ICMP response).')
		elif response.haslayer(TCP):
			if response[TCP].flags == 0x12:
				# SYN-ACK received (open port)
				print(f'TCP port {port} is open.')
				# Send RST to close connection
				send(IP(dst=target_ip)/TCP(dport=port, flags='R'), verbose=0)
			elif response[TCP].flags == 0x14:
				# RST-ACK received (closed port)
				print(f'TCP port {port} is closed.')
			else:
				print(f'TCP port {port} is in unknown state.')
		else:
			print(f'TCP port {port} is in unknown state.')


def udp_scan(target_ip, ports):
	for port in ports:
		udp_packet = IP(dst=target_ip)/UDP(dport=port)
		response = sr1(udp_packet, timeout=2, verbose=0)

		if response is None:
			print(f'UDP port {port} is open or filtered (no response).')
		elif response.haslayer(ICMP):
			if response[ICMP].type == 3 and response[ICMP].code == 3:
				# type 3 = destination unreachable
				# code 3 = port unreachable
				print(f'UDP port {port} is is closed (ICMP response).')
			else:
				print(f'UDP port {port} is in unknown state (strange ICMP response).')
		elif response.haslayer(UDP):
			print(f'UDP port {port} is open (UDP response).')
		else:
			print(f'UDP port {port} is in unknown state.')


def icmp_scan(target_ip):
	# 3 = echo request
	icmp_packet = IP(dst=target_ip) / ICMP()
	response = sr1(icmp_packet, timeout=1, verbose=0)

	if response is None:
		print(f"ICMP no response")
	else:
		print(f"ICMP response received {response.summary()}")


target_ip = '192.168.0.1'

# FTP Control, FTP Data, SSH, Telnet, SMTP, DNS, HTTP, POP3, NetBIOS, HTTPS, SMB, VNC, HTTP
tcp_scan(target_ip, [20, 21, 22, 23, 25, 53, 80, 110, 139, 443, 445, 5900, 8080])

# DNS, DHCP, DHCP, NTP, NetBIOS, NetBIOS, SNMP, VPN, VPN, MDNS
udp_scan(target_ip, [53, 67, 68, 123, 137, 138, 161, 500, 4500, 5353])

icmp_scan(target_ip)
