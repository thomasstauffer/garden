#!/usr/bin/env python3

import sys
import time
import socket

HOST = '10.42.0.143'
PORT = 5000

def readline(s):
	result = b''
	while True:
		result += s.recv(1)
		if result.endswith(b'\n'):
			break
	return result


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setblocking(False)
s.connect((HOST, PORT))

period = 1.0 # seconds

for i in range(1):
	print('on')
	s.send(b'DIG:PIN LED0,1\r\n')
	time.sleep(period / 2.0)
	print('off')
	s.send(b'DIG:PIN LED0,0\r\n')
	time.sleep(period / 2.0)
	#data = s.recv(1)
	#print([data])

s.send(b'ACQ:RST\r\n')
s.send(b'ACQ:DEC 4\r\n')
s.send(b'ACQ:TRIG:DLY 0\r\n')
s.send(b'ACQ:START\r\n')
s.send(b'ACQ:TRIG NOW\r\n')
#s.send(b'ACQ:TRIG EXT_PE\r\n')

while True:
	s.send(b'ACQ:TRIG:STAT?\r\n')
	result = readline(s)
	print(result)
	if result == b'TD\r\n':
		break
	time.sleep(0.1)

s.send(b'ACQ:SOUR1:DATA?\r\n')
result = readline(s)

print(result[:100])

#data = result[1:-3].decode()
#data = list(map(float, data.split(',')))
#print(data)

