
https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/run-your-first-container

Docker on Windows can be in Linux or Windows mode. Windows Home does not supprt Docker Windows mode (2022).

& $Env:ProgramFiles\Docker\Docker\DockerCli.exe -SwitchDaemon

docker pull hello-world
docker run hello-world

docker pull alpine
docker run alpine /bin/echo Hello
docker run alpine ls -l /
docker run -it alpine /bin/sh

docker pull mcr.microsoft.com/windows/nanoserver:ltsc2022
docker run -it mcr.microsoft.com/windows/nanoserver:ltsc2022 cmd.exe

# mcr.microsoft.com/windows/servercore:ltsc2019

docker pull nanoserver/iis
