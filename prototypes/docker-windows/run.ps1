
# https://docs.microsoft.com/de-de/virtualization/windowscontainers/deploy-containers/system-requirements
# https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/hosted?view=azure-devops&tabs=yaml
# https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=Windows-10-and-11
# https://hub.docker.com/r/nanoserver/iis

docker build -t test .
docker run test
