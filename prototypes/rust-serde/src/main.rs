use bincode::Options;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct MyStruct {
    x: i32,
    y: i32,
    z: i16,
}

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn pack<T: Serialize>(data: &T) -> Result<Vec<u8>> {
    let bytes = bincode::serialize(&data)?;
    Ok(bytes)
}

fn unpack<'a, T: Deserialize<'a>>(bytes: &'a [u8]) -> Result<T> {
    let data =  bincode::deserialize(&bytes)?;
    Ok(data)
}

fn main() {
    println!("rust-serde");

    let in1: i32 = 123;
    let vec1 = pack(&in1).unwrap();
    let out1: i32 = bincode::deserialize(&vec1).unwrap();
    println!("{:?} {:?} {:?}", in1, vec1, out1);

    let in2 = MyStruct { x: 1, y: 2, z: 3 };
    let vec2 = bincode::serialize(&in2).unwrap();
    let out2: MyStruct = bincode::deserialize(&vec2).unwrap();
    println!("{:?} {:?} {:?}", in2, vec2, out2);

    let options = bincode::DefaultOptions::new()
        .with_fixint_encoding()
        .with_big_endian();
    let in3 = MyStruct { x: 1, y: 2, z: 3 };
    let vec3 = options.serialize(&in3).unwrap();
    let out3: MyStruct = options.deserialize(&vec3).unwrap();
    println!("{:?} {:?} {:?}", in3, vec3, out3);

    let in4 = MyStruct { x: 1, y: 2, z: 3 };
    let vec4 = pack(&in4).unwrap();
    let out4: MyStruct = unpack(&vec4).unwrap();
    println!("{:?} {:?} {:?}", in4, vec4, out4);
}
