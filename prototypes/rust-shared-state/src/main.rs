use std::thread;

struct KeyValue {
       key: i32,
       value: i32,       
}

fn hello() {
   let handle = thread::spawn(|| {
       println!("Thread: Hello");
   });

   println!("Main: Hello");

   handle.join().unwrap();
}

fn lifetime() {
   let a = KeyValue { key: 1, value: 2 };

   // a is immutable
   // a.key = 2;
   
   // b already borrows a, a cannot be used again
   // let b = a;

   {
      // assign a_copy a to get a borrow error
      let a_copy = KeyValue { key: a.key, value: a.value };
      // remove move, to get lifetime error
      let handle = thread::spawn(move || {
         println!("Thread: a_copy is {}", a_copy.key);
      });
      handle.join().unwrap();
   }

   println!("Main: a is {}", a.key);
}

fn pointer() {
   let a = 1;
   let _pa: *const i32 = &a;

   // pointer cannot be shared safely
   /*
   {
       let handle = thread::spawn(|| {
           println!("Thread: a is {}", *_pa);
       });
       handle.join().unwrap();
   }
   */

   println!("Main: a is {a}");
   // derefreference raw pointer is unsafe
   // println!("Main: a is {}", *_pa);
}

fn outlive() {
   let long = 1;
   let mut long_ptr = &long;
   println!("{}", *long_ptr);
   long_ptr = &long;
   {
      let _short = 2;
      // fail short does not live long enough
      // long_ptr = &_short;
   }
   
   println!("{}", *long_ptr);
}

fn main() {
   hello();
   lifetime();
   pointer();
   outlive();
}
