﻿//using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebSockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/*
Firefox

http://127.0.0.1:5000

Ctrl+Shift+I

var ws = new WebSocket('ws://127.0.0.1:5000/ws');
ws.onmessage = event => console.log(event.data);
ws.send('Hello');
*/

namespace CSWS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");

            var builder = Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(
                webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }
            );

            var host = builder.Build();

            Console.WriteLine("Ready");

            host.Run(); // block forever
        }
    }

    class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
        }

        private async Task Handler(HttpContext context)
        {
            await context.Response.WriteAsync("<h1>Hallo Thomas</h1><p>" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "</p>");
        }

        private async Task Echo(HttpContext context, WebSocket webSocket)
        {
            var buffer = new byte[1024 * 4];
            var answer = Encoding.ASCII.GetBytes("Hello");
            answer.CopyTo(buffer, 0);
            await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, answer.Length), WebSocketMessageType.Binary, false, CancellationToken.None);

            WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            while (!result.CloseStatus.HasValue)
            {
                await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, result.Count), result.MessageType, result.EndOfMessage, CancellationToken.None);
                result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }
            await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }

        private async Task Middleware(HttpContext context, Func<Task> next)
        {
            if (context.Request.Path == "/ws")
            {
                if (context.WebSockets.IsWebSocketRequest)
                {
                    WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                    await Echo(context, webSocket);
                }
                else
                {
                    context.Response.StatusCode = 400;
                }
            }
            else
            {
                await next();
            }
        }

        public void Configure(IApplicationBuilder app)
        {
            var wsOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
            };
            app.UseWebSockets(wsOptions);
            app.Use(Middleware);
            app.Run(Handler);
        }
    }
}
