
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>

void* operator new(std::size_t size) /* throw(std::bad_alloc) */
{
	void* pointer = std::malloc(size);
	std::cout << "new address:" << pointer << " size:" << size << std::endl;
	return pointer;
}

void operator delete(void* pointer) /* throw() */
{
	std::cout << "del address:" << pointer << std::endl;
	return std::free(pointer);
}

int add(int a, int b) {
	return a + b;
}

int main() {
	{
		std::cout << "vector" << std::endl;

		std::vector<int> v;

		v.resize(10);
		v.resize(100);
		v.resize(1000);

		std::cout << "~vector" << std::endl;
	}

	std::cout << std::endl;

	{
		std::cout << "set" << std::endl;

		std::set<int> s;

		s.insert(1);
		s.insert(2);
		s.insert(1);
		s.insert(1);

		s.erase(1);
		s.erase(1);
		s.erase(1);

		std::cout << "~set" << std::endl;
	}

	std::cout << std::endl;

	{
		std::cout << "map" << std::endl;

		std::map<int, int> m;
		m[1] = 1;
		m[2] = 2;
		m[3] = 3;
		m[4] = 4;

		std::cout << "map" << std::endl;
	}

	std::cout << std::endl;

	{
		std::cout << "~map<function>" << std::endl;

		int a = 1;
		int b = 2;

		std::map<int, std::function<int(int, int)>> m;
		m[1] = [&](int, int) -> int { return add(a, b); };
		m[2] = [&](int, int) -> int { return add(b, a); };
		m[3] = [&](int, int) -> int { return add(a, a); };
		m[4] = [&](int, int) -> int { return add(b, b); };

		std::cout << "~map<function>" << std::endl;
	}

	std::cout << std::endl;

	{
		std::cout << "~unordered_map" << std::endl;

		std::unordered_map<int, int> m;
		m[1] = 1;
		m[2] = 2;
		m[3] = 3;
		m[4] = 4;

		std::cout << "~unordered_map" << std::endl;
	}

	return 0;
}

/*
vector
new address:0x563f7bc7f2c0 size:40
new address:0x563f7bc7f2f0 size:400
del address:0x563f7bc7f2c0
new address:0x563f7bc7f490 size:4000
del address:0x563f7bc7f2f0
~vector
del address:0x563f7bc7f490

set
new address:0x563f7bc7f2c0 size:40
new address:0x563f7bc7f490 size:40
del address:0x563f7bc7f2c0
~set
del address:0x563f7bc7f490

map
new address:0x563f7bc7f490 size:40
new address:0x563f7bc7f2c0 size:40
new address:0x563f7bc7f4c0 size:40
new address:0x563f7bc7f4f0 size:40
map
del address:0x563f7bc7f4f0
del address:0x563f7bc7f4c0
del address:0x563f7bc7f2c0
del address:0x563f7bc7f490

~map<function>
new address:0x563f7bc7f520 size:72
new address:0x563f7bc7f570 size:72
new address:0x563f7bc7f5c0 size:72
new address:0x563f7bc7f610 size:72
~map<function>
del address:0x563f7bc7f610
del address:0x563f7bc7f5c0
del address:0x563f7bc7f570
del address:0x563f7bc7f520

~unordered_map
new address:0x563f7bc7f660 size:16
new address:0x563f7bc7f680 size:104
new address:0x563f7bc7f6f0 size:16
new address:0x563f7bc7f710 size:16
new address:0x563f7bc7f730 size:16
~unordered_map
del address:0x563f7bc7f730
del address:0x563f7bc7f710
del address:0x563f7bc7f6f0
del address:0x563f7bc7f660
del address:0x563f7bc7f680
*/
