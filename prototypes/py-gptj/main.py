#!/usr/bin/env python3

import banana_dev as banana # pip3 install banana-dev==3.0.1

text = 'lovely story about a racoon and an elephant.'

text = '''
A lovely story about a racoon and an elephant.
The racoon is very clever, and the elephant is very stupid.
The racoon is always trying to trick the elephant.
One day, the elephant is so angry that he throws a rock at the racoon.
The racoon is so scared that he runs away and hides under the tree.
The elephant picks up the tree and throws it at the racoon.
The tree hits the racoon and it falls down dead.
The elephant walks over to the racoon and sees that it is dead.
"Oh dear, my terrible elephant has killed the smart racoon," says the elephant.
"Now there is nothing more for me to eat, and I am hungry."
The elephant walks away to eat the tree.
The next day, the racoon comes crawling out from under the tree.
He's still alive, but the elephant doesn't know that.
And he doesn't see the racoon.
So he walks right on by the racoon.
'''

api_key='' # removed, get on https://app.banana.dev/
model_key='gptj'
model_inputs = { 'text': text, 'length': 50, 'temperature': 0.8, 'topK': 20, 'topP': 1.0}
out = banana.run(api_key, model_key, model_inputs)

print(out)
print(out['modelOutputs'][0]['output'])
