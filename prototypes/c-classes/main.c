
#include <stdlib.h>
#include <stdio.h>

#define HISTORY_LENGTH 32

typedef struct {
	double history[HISTORY_LENGTH];
	int index;
} MovingAverage;

void moving_average_init(MovingAverage* self) {
	for (int i = 0; i < HISTORY_LENGTH; i += 1) {
		self->history[i] = 0.0;
	}
	self->index = 0;
}

void moving_average_measurement(MovingAverage* self, double value) {
	self->history[self->index] = value;
	self->index = (self->index + 1) % HISTORY_LENGTH;
}

double moving_average_average(MovingAverage* self) {
	double sum = 0.0;
	for (int i = 0; i < HISTORY_LENGTH; i += 1) {
		sum += self->history[i];
	}
	return sum / HISTORY_LENGTH;
}

int main() {
	MovingAverage ma;

	moving_average_init(&ma);

	for (int i = 0; i < 1000; i += 1) {
		double noise = ((float)rand() / RAND_MAX) - 0.5;
		double base = i < 500 ? 10.0 : 5.0;
		double measurement = base + noise;
		moving_average_measurement(&ma, measurement);
		printf("%f,%f\n", measurement, moving_average_average(&ma));
	}
}
