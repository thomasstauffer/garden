
import random

class MovingAverage:
	def __init__(self):
		self.history = [0.0] * 32
		self.index = 0

	def measurement(self, value):
		self.history[self.index] = value
		self.index = (self.index + 1) % len(self.history)

	def average(self):
		return sum(self.history) / len(self.history)

ma = MovingAverage()
for i in range(1000):
	base = 10 if i < 500 else 5
	noise = random.random() - 0.5
	measurement = base + noise
	ma.measurement(measurement)
	print('{},{}'.format(measurement, ma.average()))
