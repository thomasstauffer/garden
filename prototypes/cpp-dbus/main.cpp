/*
https://gitlab.com/ubports/development/core/lib-cpp/dbus-cpp

busctl --user
busctl --user call org.tom.Service /org/tom/Service org.tom.Service DoubleInt64 x 123
*/

#include <core/dbus/announcer.h>
#include <core/dbus/asio/executor.h>
#include <core/dbus/bus.h>
#include <core/dbus/object.h>
#include <core/dbus/service.h>
#include <core/dbus/signal.h>
#include <core/dbus/skeleton.h>
#include <core/dbus/stub.h>
#include <core/dbus/types/stl/tuple.h>
#include <core/dbus/types/stl/vector.h>
#include <core/dbus/types/struct.h>

namespace dbus = core::dbus;

namespace test {

class IService {
  protected:
    struct DoubleInt64 {
        typedef IService Interface;
        inline static const std::string& name() {
            static const std::string s{"DoubleInt64"};
            return s;
        }
        inline static const std::chrono::milliseconds default_timeout() { return std::chrono::seconds{1}; }
    };

  public:
    virtual ~IService() = default;
};
} // namespace test

namespace core::dbus::traits {
template <> struct Service<test::IService> {
    inline static const std::string& interface_name() {
        static const std::string s{"org.tom.Service"};
        return s;
    }
};
} // namespace core::dbus::traits

namespace test {

class Service : public dbus::Skeleton<IService> {
  public:
    typedef std::shared_ptr<Service> Ptr;
    Service(const dbus::Bus::Ptr& bus)
        : dbus::Skeleton<IService>(bus),
          object(access_service()->add_object_for_path(dbus::types::ObjectPath("/org/tom/Service"))) {
        object->install_method_handler<IService::DoubleInt64>(
            std::bind(&Service::double_int64, this, std::placeholders::_1));
    }
    ~Service() noexcept = default;

  private:
    void double_int64(const dbus::Message::Ptr& msg) {
        int64_t in;
        msg->reader() >> in;
        
        int64_t out = in * 2;

        auto reply = dbus::Message::make_method_return(msg);
        reply->writer() << out;
        access_bus()->send(reply);
    }

    dbus::Object::Ptr object;
};

} // namespace test

int main(int, char**) {
    auto bus = std::make_shared<dbus::Bus>(dbus::WellKnownBus::session);
    bus->install_executor(core::dbus::asio::make_executor(bus));
    std::thread t{std::bind(&dbus::Bus::run, bus)};
    auto service = dbus::announce_service_on_bus<test::IService, test::Service>(bus);

    if (t.joinable()) {
        t.join();
    }

    return EXIT_SUCCESS;
}
