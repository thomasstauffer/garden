#!/usr/bin/env python3

import asyncio
import websockets

async def handle_client():
    uri = 'ws://localhost:8765'
    async with websockets.connect(uri) as websocket:
        for i in range(10):
            await websocket.send('Hello From Python ' + str(i))
            response = await websocket.recv()
            print(f'Received: {response}')
            await asyncio.sleep(2.0)

asyncio.run(handle_client())
