#!/usr/bin/env python3

import asyncio
import websockets

async def handle_peer(websocket, path):
    print(f'Client connected: {websocket.remote_address} {path}')
    async for message in websocket:
        print(f'Received: {message}')
        await websocket.send(message)
    print(f'Client disconnected')

async def main():
    async with websockets.serve(handle_peer, 'localhost', 8765):
        await asyncio.Future()

if __name__ == '__main__':
    asyncio.run(main())
