
% https://stackoverflow.com/questions/22708340/erlang-inets-httpd-with-ssl
% https://marcelog.github.io/articles/erlang_websocket_server_cowboy_tutorial.html

-module(webserver).

-export([start/0]).

-behaviour(supervisor).
-export([start_link/0]).
-export([init/1]).

-behaviour(application).
-export([start/2]).
-export([stop/1]).

-behavior(cowboy_handler).
-export([init/2]).

% generic

print(Text) ->
	io:fwrite("~s\n", [Text]).

print(Name, Value) ->
	io:fwrite("~s: ~p\n", [Name, Value]).

% webserver without any process

run_inets() ->
	inets:start(),
	{ok, Pid} = inets:start(httpd, [{port, 4444}, {server_name, "httpd_test"}, {server_root, "/tmp"}, {document_root, "/tmp"}, {bind_address, "localhost"}]),
	[{port, Port}] = httpd:info(Pid, [port]),
	print("Port", Port),
	timer:sleep(timer:seconds(30)),
	inets:stop(httpd, Pid).

% supervisor

start_link() ->
	print("supervisor start_link ..."),
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	print("supervisor init ..."),
	Procs = [],
	{ok, {{one_for_one, 1, 5}, Procs}}.

% application

start(_Type, _Args) ->
	print("application start ..."),
	Dispatch = cowboy_router:compile([
			%{'_', [{"/", ?MODULE, []}]}
	]),
	{ok, _} = cowboy:start_clear(my_http_listener, [{port, 8080}], #{env => #{dispatch => Dispatch}}),
	start_link().

stop(_State) ->
	print("application stop ..."),
	ok.
	
% cowboy_handler

init(Req0, State) ->
	print("cowboy_handler init ..."),
	Req = cowboy_req:reply(200, #{<<"content-type">> => <<"text/plain">>}, <<"Hello Erlang!">>, Req0),
	{ok, Req, State}.

% main

start() ->
	io:fwrite("Start\n"),
	ok = application:start(crypto),
	ok = application:start(asn1),
	ok = application:start(public_key),
	ok = application:start(ssl),
	ok = application:start(ranch),
	ok = application:start(cowlib),
	ok = application:start(cowboy),
	%start([], []), % 2021-03 not working anymore
	run_inets(),
	io:fwrite("End\n").
