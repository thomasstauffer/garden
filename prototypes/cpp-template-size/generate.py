#!/usr/bin/env python3

import sys

STD_ARRAY = '''
struct {name} {{
	std::array<int, {size}> _array;
	{name}() {{
		for (int i = 0; i < _array.size(); i += 1) {{
			_array[i] = (i + 1) * (i + 7) * (i + 13);
		}}
	}}
	int sum() const {{
		int result = 0;
		for (int i = 0; i < _array.size(); i += 1) {{
			result += _array[i];
		}}
		return result;
	}}
}};
'''

STD_VECTOR = '''
struct {name} {{
	std::vector<int> _array;
	{name}() {{
		for (int i = 0; i < _array.size(); i += 1) {{
			_array[i] = (i + 1) * (i + 7) * (i + 13);
		}}
	}}
	int sum() const {{
		int result = 0;
		for (int i = 0; i < _array.size(); i += 1) {{
			result += _array[i];
		}}
		return result;
	}}
}};
'''

ARRAY = '''
struct {name} {{
	int _array[{size}];
	{name}() {{
		for (int i = 0; i < {size}; i += 1) {{
			_array[i] = (i + 1) * (i + 7) * (i + 13);
		}}
	}}
	int sum() const {{
		int result = 0;
		for (int i = 0; i < {size}; i += 1) {{
			result += _array[i];
		}}
		return result;
	}}
}};
'''

STD_VARIANT = '''
struct {name} {{
	std::variant<int, float> _variant;
	{name}() : _variant({size}) {{
	}}
	int sum() const {{
		int result = 0;
		result += std::get<int>(_variant);
		return result;
	}}
}};
'''

MAIN_CALL = '''
	{name} _{name};
	sum += _{name}.sum();
'''

MAIN = '''
#include <iostream>
#include <array>
#include <vector>
#include <map>
#include <variant>

{functions}

int main() {{
	int sum = 0;
	{calls}
	std::cout << sum << std::endl;
	return 0;
}}
'''

LOOKUP = {
	'std::array': STD_ARRAY,
	'std::vector': STD_VECTOR,
	'std::variant': STD_VARIANT,
	'array': ARRAY,
}

def main():
	n = int(sys.argv[1])
	template = LOOKUP[sys.argv[2]]

	calls = []
	functions = []
	for i in range(n):
		name = 'struct{0:010d}'.format(i)
		calls += [ MAIN_CALL.format(name=name) ]
		size = ((i % 20) + 5) * 2
		functions += [ template.format(name=name, size=size) ]
	print(MAIN.format(calls=''.join(calls), functions=''.join(functions)))

main()
