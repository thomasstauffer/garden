
// Question: does any library do allow to draw correct arrow on connectors when lines intersect squares or circles?

'use strict';

const vis = require('vis');
const cytoscape = require('cytoscape');

function parseInput(input) {
	function getFromDict(dictionary, key, def) {
		return dictionary[key] || def;
	}
	const lines = input.split('\n');
	const items = _.flatten(_.map(lines, function(lineUntrimmed) {
		const line = lineUntrimmed.trim();
		if(line.length == 0) {
			return;
		}
		const indexStyle = line.indexOf('{');
		const hasStyle = indexStyle > 0
		const style = hasStyle ? JSON.parse(line.substring(indexStyle)) : {};
		const rest = hasStyle ? line.substring(0, indexStyle - 1) : line;
		const parts = rest.split(' ');
		if(parts.length == 1) {
			const id = parts[0];
			const label = getFromDict(style, 'label', id);
			const text = getFromDict(style, 'text', undefined);
			return [ [ id, {label: label, text: text} ] ];
		} else if (parts.length == 3) {
			const source = parts[0];
			const edge = parts[1];
			const target = parts[2];
			const label = getFromDict(style, 'label', '');
			return [
				[source, { id: source, label: source } ],
				[target, { id: target, label: target } ],
				{label: label, source: source, edge: edge, target: target},
			];
		} else {
			console.log('Cannot Parse Line: ' + line);
			return;
		}
	}));
	const nodes = _.fromPairs(_.filter(items, function(element) {
		return Array.isArray(element);
	}));
	const edges = _.filter(items, function(element) {
		return (element !== undefined) && (!Array.isArray(element));
	});
	return {nodes: nodes, edges: edges};
}

function createCytoscape(graph) {
	let elements = [];
	for(let id in graph.nodes) {
		const n = graph.nodes[id];
		elements.push({ data: { id: id, label: n.label } });
	}
	STYLES = { '-': ['none', 'none'], '->': ['none', 'triangle'], '<-': ['triangle', 'none'], '<->': ['triangle', 'triangle'] };
	for(let i = 0; i < graph.edges.length; i++) {
		const e = graph.edges[i];
		const sourceArrow = STYLES[e.edge][0];
		const targetArrow = STYLES[e.edge][1];
		elements.push({ data: { label: e.label, source: e.source, target: e.target, sourceArrow: sourceArrow, targetArrow: targetArrow } });
	}

	const cy = cytoscape({
		container: document.getElementById('GraphOutput'),
		/*
		elements: [
			{ data: { id: 'a', label: 'hi' } },
			{ data: { id: 'b' } },
			{ data: { id: 'c' } },
			{ data: { id: 'ab', source: 'a', target: 'b', label: 'ho', sourceArrow: 'none', targetArrow: 'triangle' } }

		],*/
		elements: elements,
		layout: {
			name: 'breadthfirst',
			circle: 'true'
		},
		style: [
			{ selector: 'node', style: {
				label: 'data(label)',
				'border-width': '0.2em',
				'border-color': '#66a',
				'width': '10em',
				'height': '3em',
				'background-color': '#99f',
				'shape': 'roundrectangle',
				'text-halign': 'center',
				'text-valign': 'center'
			}},
			{ selector: 'edge', style: {
				label: 'data(label)',
				'curve-style': 'bezier',
				'line-color': '#000',
				'source-arrow-color': '#000',
				'target-arrow-color': '#000',
				'source-arrow-shape': 'data(sourceArrow)',
				'target-arrow-shape': 'data(targetArrow)',
				'text-background-color': '#fff',
				'text-background-opacity': 1.0,
				'text-background-shape': 'roundrectangle'
			}}
		]
	});
}

function layoutCircle(nodes) {
	const radius = 500;
	for(let i = 0; i < nodes.length; i++) {
		const angle = Math.PI * 2.0 * (i / nodes.length);
		const x = Math.cos(angle) * radius;
		const y = Math.sin(angle) * radius;
		nodes[i].x = x;
		nodes[i].y = y;
	}
}

function layoutTreeRecursive(angleStart, fromId, rootId, graph, nodes, depth) {
	if(depth > 10) {
		return;
	}

	let nodeIds = [];
	for(let i = 0; i < graph.edges.length; i++) {
		const edge = graph.edges[i];
		if(edge.source == rootId) {
			if(edge.target != fromId) {
				nodeIds.push(edge.target);
			}
		}
		if(edge.target == rootId) {
			if(edge.source != fromId) {
				nodeIds.push(edge.source);
			}
		}
	}

	let rootNode = null;
	for(let j = 0; j < nodes.length; j++) {
		if(nodes[j].id == rootId) {
			rootNode = nodes[j];
		}
	}

	const radius = 400;
	let l = nodeIds.length;
	if(fromId != '') {
		l += 1;
	}
	let a = Math.PI * 2.0 / l;

	//console.log(rootId + ' ' + nodeIds.length + ' ' + (a * 180.0 / Math.PI));

	for(let i = 0; i < nodeIds.length; i++) {
		const nodeId = nodeIds[i];
		let n = null;
		for(let j = 0; j < nodes.length; j++) {
			if(nodes[j].id == nodeId) {
				n = nodes[j];
			}
		}

		const angle = angleStart - Math.PI + ((i+1) * a);
		const x = Math.cos(angle) * radius;
		const y = Math.sin(angle) * radius;

		n.x = rootNode.x + x;
		n.y = rootNode.y + y;

		layoutTreeRecursive(angle, rootId, nodeId, graph, nodes, depth + 1);
	}
}

function layoutTree(nodes, graph) {
	nodes[0].x = 0;
	nodes[0].y = 0;
	const firstId = Object.keys(graph.nodes)[0];
	layoutTreeRecursive(0.0, '', firstId, graph, nodes, 0);
}

function createVis(graph) {
	let nodes = [];
	for(let id in graph.nodes) {
		const n = graph.nodes[id];
		let label = n.label;
		if(n.text) {
			label += '\n' + n.text;
		}
		nodes.push({
			borderWidth: 3.0,
			shape: 'box',
			color: { background: '#eef', border: '#aaf' },
			label: label,
			id: id
		});
	}

	const ARROWS = { '-': '', '->': 'to', '<-': 'from', '<->':'from;to' };
	let edges = [];
	for(let i = 0; i < graph.edges.length; i++) {
		const e = graph.edges[i];
		edges.push({
			from: e.source,
			to: e.target,
			label: e.label,
			font: { strokeWidth: 5 },
			color: '#000',
			smooth: false,
			arrows: ARROWS[e.edge],
			length: 1000
		});
	}

	//layoutCircle(nodes);
	//layoutTree(nodes, graph);

	const container = document.getElementById('GraphOutput');
	const data = {
		nodes: new vis.DataSet(nodes),
		edges: new vis.DataSet(edges)
	};
	const options = {
		physics: false,
		layout: { randomSeed: 0 }
	};

	const network = new vis.Network(container, data, options);

	//let data = canvas.toDataURL(type, quality)
}

function canvasToPng(canvas) {
	if(canvas.nodeName !== 'CANVAS') {
		throw 'Not A Canvas';
	}

	const width = canvas.width;
	const height = canvas.height;
	const context = canvas.getContext('2d');
	const compositeOperation = context.globalCompositeOperation;
	context.globalCompositeOperation = 'destination-over';
	context.fillStyle = 'white';
	context.fillRect(0, 0, width, height);
	const data = canvas.toDataURL('image/png');
	const img = electron.nativeImage.createFromDataURL(data);
	return img.toPng();
}

function createD3(inputParam) {
	const input = inputParam;

	const w = 900;
	const h = 800;

	let dataNodes = [];
	let id = 0;
	let keytoid = {};
	for(let key in input.nodes) {
		keytoid[key] = id;
		dataNodes.push({name: key});
		id++;
	}
	let dataLinks = [];
	for(let i = 0; i < input.edges.length; i++) {
		dataLinks.push({source: keytoid[input.edges[i].source], target: keytoid[input.edges[i].target]});
	}

	const svg = d3.select('#GraphOutput')
		.append('svg')
		.attr('width', w)
		.attr('height', h);

	/*
	var simulation = d3.forceSimulation()
		.force('link', d3.forceLink().distance(500).strength(0.5))
		.force('charge', d3.forceManyBody())
		.force('center', d3.forceCenter(w / 2, h / 2));
	*/

	const link = svg.selectAll('line')
		.data(dataLinks)
		.enter()
		.append('line')
		.style('stroke', '#ccc')
		.style('stroke-width', 1);

	const node = svg.selectAll('circle')
		.data(dataNodes)
		.enter()
		.append('g')

	node.append('circle')
		.attr('stroke', '#ccc')
		.attr('stroke-width', '1')
		.attr('fill', '#eee')
		.attr('r', 30)

	node.append('text')
		.attr('text-anchor', 'middle')
		.attr('alignment-baseline', 'middle')
		.text(n => n.name);

	//node.call(force.drag);

	function ticked() {
		link
			.attr('x1', n => n.source.x)
			.attr('y1', n => n.source.y)
			.attr('x2', n => n.target.x)
			.attr('y2', n => n.target.y);
		node.attr('transform', n => 'translate(' + n.x + ',' + n.y + ')');
	}

	const simulation = d3.forceSimulation()
		//.force('link', d3.forceLink().distance(500).strength(0.2))
		.force('collide', d3.forceCollide(50))
		//.force('charge', d3.forceManyBody())
		//.force('center', d3.forceCenter(w / 2, h / 2))
		//.force('y', d3.forceY(0))
		//.force('x', d3.forceX(10))
		//.force("charge", d3.forceManyBody().strength(-100))
		.force("link", d3.forceLink())
		.force("center", d3.forceCenter(w / 2, h / 2))
		.nodes(dataNodes)
		.on('tick', ticked);

	simulation.force('link').links(dataLinks);
}

function render() {
	const input = document.getElementById('GraphInput').value;
	//createCytoscape(parseInput(input));
	//createSigma(parseInput(input));
	createVis(parseInput(input));
	//createD3(parseInput(input));
}

function runGraph() {
	const filename = 'graph.txt';
	const data = fs.readFileSync(filename, 'utf-8');
	const input = document.getElementById('GraphInput');
	input.value = data;

	render();
	document.getElementById('GraphRender').addEventListener('click', function() {
		render();
	});
	document.getElementById('GraphSavePNG').addEventListener('click', function() {
		const canvas = document.getElementById('GraphOutput').children[0].children[0];
		const data = canvasToPng(canvas);
		fs.writeFileSync('crow-output-graph.png', data);
	});
	document.getElementById('GraphSaveSVG').addEventListener('click', function() {
		throw 'Not Implemented';
	});
}

exports.init = runGraph;