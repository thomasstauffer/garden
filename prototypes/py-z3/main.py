#!/usr/bin/env python3

# pip3 install z3-solver

from z3 import *

def f1():
	x, y = Ints('x y')
	s = Solver()
	s.add((x % 4) + 3 * (y / 2) > x - y)

	print(s.sexpr())
	print(s.check())
	print(s.model())

def f2():
	x, y, n = Ints('x y n')
	s = Solver()
	s.add((x + 1) % n == y + x, n > 1)

	print(s.sexpr())
	print(s.check())
	print(s.model())

	# https://stackoverflow.com/questions/13395391/z3-finding-all-satisfying-models

f1()
f2()
