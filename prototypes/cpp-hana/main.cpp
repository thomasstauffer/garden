#include <boost/hana.hpp>
namespace hana = boost::hana;

#include <iostream>
#include <string>

struct Test {
	int32_t a;
	std::string b;
	int32_t c;
};

BOOST_HANA_ADAPT_STRUCT(Test, a, b, c);

std::string to_csv(int32_t value) { return std::to_string(value); }
std::string to_csv(std::string value) { return value; }

template <typename Ts>
std::string join(const Ts& ts, std::string sep) {
	return hana::fold(hana::intersperse(ts, sep), "", hana::_ + hana::_);
}

template <typename T> std::enable_if_t<hana::Struct<T>::value, std::string> to_csv(const T& t) {
	auto csv = hana::transform(hana::keys(t), [&](auto name) {
		const auto& member = hana::at_key(t, name);
		return to_csv(member);
	});
	return join(csv, ",");
}

int main() {
	std::cout << "Hello" << std::endl;

	constexpr auto values = hana::make_tuple(1, "2", 3);
	static_assert(values[hana::int_c<2>] == 3, "");
	std::cout << hana::at_c<2>(values) << std::endl;

	Test test{1, "Hello", 4};
	std::cout << to_csv(test) << std::endl;

	return 0;
}
