
import * as THREE from 'https://unpkg.com/three@0.158.0/build/three.module.js';
//import { OrbitControls } from 'https://unpkg.com/browse/three-orbitcontrols@2.110.3';

function createMesh(segments) {
	const geometry = new THREE.PlaneGeometry(20.0, 20.0, segments, segments);
	geometry.rotateX(-Math.PI / 2);
	const material = new THREE.MeshPhongMaterial({ color: 0x999999 });
	const mesh = new THREE.Mesh(geometry, material);
	mesh.position.set(0.0, 0.0, 0.0);
	mesh.visible = false;
	return mesh;
}

function init() {
	const width = 1200;
	const height = 800;

	// Camera

	const camera = new THREE.PerspectiveCamera(60, width / height, 1.0, 1000.0); // fov, aspect, near, far
	camera.position.set(0.0, 20.0, 0.0);
	camera.lookAt(0.0, 0.0, 0.0);

	// Scene

	const scene = new THREE.Scene();
	scene.background = new THREE.Color(0xeeeeee);

	const mesh500 = createMesh(500);
	scene.add(mesh500);
	const mesh1000 = createMesh(1000);
	scene.add(mesh1000);
	const mesh2000 = createMesh(2000);
	scene.add(mesh2000);
	const mesh4000 = createMesh(4000);
	scene.add(mesh4000);
	const mesh6000 = createMesh(6000);
	scene.add(mesh6000);

	// Light

	var lightAmbient = new THREE.AmbientLight(0x666666);
	scene.add(lightAmbient);

	const lightSpot = new THREE.SpotLight(0xffffff);
	lightSpot.position.set(0.0, 5.0, 0.0);
	scene.add(lightSpot);

	// Renderer

	const renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(width, height);
	document.body.appendChild(renderer.domElement);
	//new OrbitControls(camera, renderer.domElement);

	let timeLast = Date.now();
	let fpsAverage = 0.0;
	let count = 0;

	function animate() {
		const timeCurrent = Date.now();
		const fpsCurrent = Math.round(1000 / (timeCurrent - timeLast));
		timeLast = timeCurrent;
		if (isFinite(fpsCurrent)) {
			fpsAverage += (fpsCurrent - fpsAverage) * 0.05;
		}
		document.getElementById('stat').innerText = 'FPS:' + fpsAverage.toFixed(1) + ' MTris/Frame:' + (1e-6 * renderer.info.render.triangles).toFixed(1) + ' Calls/Frame:' + renderer.info.render.calls;
		requestAnimationFrame(animate);
		renderer.render(scene, camera);
		count += 1;
		camera.position.y = 20.0 + Math.cos(count * 0.01);
		camera.lookAt(0.0, 0.0, 0.0);

		mesh500.visible = document.getElementById('mesh500').checked;
		mesh1000.visible = document.getElementById('mesh1000').checked;
		mesh2000.visible = document.getElementById('mesh2000').checked;
		mesh4000.visible = document.getElementById('mesh4000').checked;
		mesh6000.visible = document.getElementById('mesh6000').checked;
	}

	animate();
}

window.addEventListener('load', init);
