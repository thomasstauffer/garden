
function gridMake(width, height, fill) {
	const grid = new Array(height);
	for (let y = 0; y < height; y += 1) {
		grid[y] = new Array(width);
		for (let x = 0; x < width; x += 1) {
			grid[y][x] = fill;
		}
	}
	return grid;
}

function gridFill(grid, x, y, width, height, what) {
	for (let yy = y; yy < (y + height); yy += 1) {
		for (let xx = x; xx < (x + width); xx += 1) {
			grid[yy][xx] = what;
		}
	}
}

const [GRASS, STREET, RESIDENTIAL, COMMERCIAL, INDUSTRIAL] = [0, 1, 2, 3, 4];
const BLOCK_SIZE = 20;
const COLORS = {
	[GRASS]: '#090',
	[STREET]: '#999',
	[RESIDENTIAL]: '#0f0',
	[COMMERCIAL]: '#00f',
	[INDUSTRIAL]: '#ff0',
};

function gridEach(grid, callback) {
	const height = grid.length;
	const width = grid[0].length;
	for (let y = 0; y < height; y += 1) {
		for (let x = 0; x < width; x += 1) {
			callback(x, y);
		}
	}
}

function gridEachNoBorder(grid, callback) {
	const height = grid.length - 1;
	const width = grid[0].length - 1;
	for (let y = 1; y < height; y += 1) {
		for (let x = 1; x < width; x += 1) {
			callback(x, y);
		}
	}
}

function gridNeighbors(grid, x, y) {
	return [grid[y - 1][x], grid[y + 1][x], grid[y][x - 1], grid[y][x + 1]];
}

function gridMinMax(grid) {
	let [min, max] = [Infinity, -Infinity];
	gridEach(grid, (x, y) => {
		const value = grid[y][x];
		if (!isFinite(value)) {
			return;
		}
		if (value < min) {
			min = value;
		}
		if (value > max) {
			max = value;
		}
	});
	return [min, max];
}

function gridCopyFromTo(from, to) {
	gridEachNoBorder(from, (x, y) => {
		to[y][x] = from[y][x];
	});
}

function showZone(ctx, grid) {
	gridEach(grid, (x, y) => {
		ctx.fillStyle = COLORS[grid[y][x]];
		ctx.fillRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
	});
}

function showMinMax(ctx, grid) {
	const [min, max] = gridMinMax(grid);
	const range = max - min;
	gridEach(grid, (x, y) => {
		const v = grid[y][x];
		if (isFinite(v)) {
			const normalized = (v - min) / range; // value between 0.0 and 1.0
			const r = Math.floor((1.0 - normalized) * 255);
			const g = Math.floor(normalized * 255);
			ctx.fillStyle = `rgba(${r}, ${g}, 0, 0.9)`;
		} else {
			ctx.fillStyle = 'rgba(0, 0, 0, 0.0)';
		}
		ctx.fillRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
	});
}

function showNumber(ctx, grid) {
	ctx.font = '10px Ubuntu';
	ctx.fillStyle = 'black';
	gridEach(grid, (x, y) => {
		ctx.fillText(grid[y][x] === Infinity ? '-' : grid[y][x], x * BLOCK_SIZE, (y + 1) * BLOCK_SIZE);
	});
}

function main() {
	const nodeView1 = document.getElementById('view1');
	const nodeView2 = document.getElementById('view2');

	const ctx1 = nodeView1.getContext('2d');
	const ctx2 = nodeView2.getContext('2d');

	const WIDTH = 20;
	const HEIGHT = 30;

	const zones = gridMake(WIDTH, HEIGHT, GRASS);
	const streetDistanceResidential = gridMake(WIDTH, HEIGHT, Infinity);
	const streetDistanceResidentialNext = gridMake(WIDTH, HEIGHT, Infinity);
	const streetDistanceCommercial = gridMake(WIDTH, HEIGHT, Infinity);
	const streetDistanceCommercialNext = gridMake(WIDTH, HEIGHT, Infinity);
	const streetDistanceIndustrial = gridMake(WIDTH, HEIGHT, Infinity);
	const streetDistanceIndustrialNext = gridMake(WIDTH, HEIGHT, Infinity);
	const pollution = gridMake(WIDTH, HEIGHT, 0);
	const pollutionFactory = gridMake(WIDTH, HEIGHT, 0);
	const pollutionFactoryNext = gridMake(WIDTH, HEIGHT, 0);
	const pollutionStreet = gridMake(WIDTH, HEIGHT, 0);
	const pollutionStreetNext = gridMake(WIDTH, HEIGHT, 0);

	let viewSelected = '';
	let actionSelected = '';

	gridFill(zones, 5, 5, 11, 13, STREET);
	gridFill(zones, 5 + 1, 5 + 1, 3, 3, RESIDENTIAL);
	gridFill(zones, 5 + 1, 5 + 5, 3, 3, COMMERCIAL);
	gridFill(zones, 5 + 1, 5 + 9, 3, 3, INDUSTRIAL);
	gridFill(zones, 5 + 7, 5 + 1, 3, 3, RESIDENTIAL);
	gridFill(zones, 5 + 7, 5 + 5, 3, 3, COMMERCIAL);
	gridFill(zones, 5 + 7, 5 + 9, 3, 3, RESIDENTIAL);
	gridFill(zones, 5 + 5, 5 + 0, 1, 13, GRASS);
	gridFill(zones, 5 + 1, 5, 10, 1, STREET);

	function init() {
		viewSelected = 'pollution';
		//viewSelected = 'street-distance-industrial';
		//actionSelected = 'place-street';
		actionSelected = 'place-industrial';
	}

	function update() {
		gridEachNoBorder(zones, (x, y) => {
			const [zoneTop, zoneBottom, zoneLeft, zoneRight] = gridNeighbors(zones, x, y);
			const zoneThis = zones[y][x];

			// BUG, this distance algorithm has a big issue, once e.g. a
			// industrial sector is removed, the distance to industrial is wrong
			// everywhere

			const zs = [RESIDENTIAL, COMMERCIAL, INDUSTRIAL];
			const ds = [streetDistanceResidential, streetDistanceCommercial, streetDistanceIndustrial];
			const dns = [streetDistanceResidentialNext, streetDistanceCommercialNext, streetDistanceIndustrialNext];
			for (let i = 0; i < 3; i += 1) {
				const [z, d, dn] = [zs[i], ds[i], dns[i]];

				if (zoneThis === z) {
					dn[y][x] = 0;
				} else if (zoneThis === STREET) {
					if ((zoneTop === z) || (zoneBottom === z) || (zoneLeft === z) || (zoneRight === z)) {
						dn[y][x] = 1;
					} else {
						const [dt, db, dl, dr] = gridNeighbors(d, x, y);
						const distanceMin = Math.min(dt, db, dl, dr);
						dn[y][x] = distanceMin + 1;
					}
				} else {
					dn[y][x] = Infinity;
				}
			}

			if (zoneThis === INDUSTRIAL) {
				pollutionFactoryNext[y][x] = 10;
			} else {
				const [t, b, l, r] = gridNeighbors(pollutionFactory, x, y);
				const p = Math.max(t, b, l, r);
				pollutionFactoryNext[y][x] = Math.max(0.0, p - 1);
			}

			if (zoneThis === STREET) {
				pollutionStreetNext[y][x] = 3;
			} else {
				const [t, b, l, r] = gridNeighbors(pollutionStreet, x, y);
				const p = Math.max(t, b, l, r);
				pollutionStreetNext[y][x] = Math.max(0.0, p - 1);
			}

		});

		gridEachNoBorder(zones, (x, y) => {
			pollution[y][x] = pollutionFactory[y][x] + pollutionStreet[y][x];
		});

		// dependency order -> e.g. street only on zones
		// TODO only use one temporary copy for next?

		gridCopyFromTo(streetDistanceResidentialNext, streetDistanceResidential);
		gridCopyFromTo(streetDistanceCommercialNext, streetDistanceCommercial);
		gridCopyFromTo(streetDistanceIndustrialNext, streetDistanceIndustrial);
		gridCopyFromTo(pollutionFactoryNext, pollutionFactory);
		gridCopyFromTo(pollutionStreetNext, pollutionStreet);
	}

	function view() {
		showZone(ctx1, zones);
		showZone(ctx2, zones);
		if (viewSelected === 'street-distance-residential') {
			showMinMax(ctx2, streetDistanceResidential);
			showNumber(ctx2, streetDistanceResidential);
		} else if (viewSelected === 'street-distance-commercial') {
			showMinMax(ctx2, streetDistanceCommercial);
			showNumber(ctx2, streetDistanceCommercial);
		} else if (viewSelected === 'street-distance-industrial') {
			showMinMax(ctx2, streetDistanceIndustrial);
			showNumber(ctx2, streetDistanceIndustrial);
		} else if (viewSelected === 'pollution') {
			showMinMax(ctx2, pollution);
			showNumber(ctx2, pollution);
		}
	}

	init();
	function step() {
		const timeUpdateStart = Date.now();
		update();
		const timeUpdateStop = Date.now();
		const dt = timeUpdateStop - timeUpdateStart;
		//console.log(dt);
		view();
	}

	function action(event) {
		const x = Math.floor(event.clientX / BLOCK_SIZE);
		const y = Math.floor(event.clientY / BLOCK_SIZE);

		if (actionSelected === 'place-grass') {
			zones[y][x] = GRASS;
		} else if (actionSelected === 'place-street') {
			zones[y][x] = STREET;
		} else if (actionSelected === 'place-residential') {
			zones[y][x] = RESIDENTIAL;
		} else if (actionSelected === 'place-commercial') {
			zones[y][x] = COMMERCIAL;
		} else if (actionSelected === 'place-industrial') {
			zones[y][x] = INDUSTRIAL;
		}
	}

	step();

	window.setInterval(step, 500);

	document.getElementById('step').addEventListener('click', step);

	nodeView1.addEventListener('click', action);

	document.querySelectorAll('#view > button').forEach(node => {
		node.addEventListener('click', () => {
			viewSelected = node.dataset.view;
		});
	});

	document.querySelectorAll('#action > button').forEach(node => {
		node.addEventListener('click', () => {
			actionSelected = node.dataset.action;
		});
	});
}

window.addEventListener('load', main);
