
#include <array>
#include <iostream>
#include <string>

using czstring = const char*;

template <typename T, size_t N>
class circular_array {
public:
	using array_type = std::array<T, N>;

	size_t _index;
	array_type _array;

public:
	class iterator {
		array_type& _array;
		size_t _index;

	public:
		explicit iterator(array_type& array, size_t index) : _array(array), _index(index) {
		}

		bool operator!=(const iterator& other) {
			return this->_index != other._index;
		}

		void operator++() {
			_index++;
		}

		T& operator*() {
			return _array[_index % N];
		}
	};

	explicit circular_array() : _index(0) {
	}

	void append(const T& item) {
		_array[_index] = item;
		_index = (_index + 1) % N;
	}

	iterator begin() {
		return iterator(_array, _index + N - 1);
	}

	iterator end() {
		return iterator(_array, _index + N + N);
	}
};

class log_entry {
private:
	enum class type {
		is_none,
		is_float,
		is_int32,
	};

	union field {
		float float_value;
		int32_t int32_value;
	};

	czstring _text;
	type _type;
	field _value;

public:
	explicit log_entry() : _text("-"), _type(type::is_none) {
	}

	explicit log_entry(czstring text) : _text(text), _type(type::is_none) {
	}

	explicit log_entry(czstring text, float value) : _text(text), _type(type::is_float), _value{.float_value = value} {
	}

	explicit log_entry(czstring text, int32_t value) : _text(text), _type(type::is_int32), _value{.int32_value = value} {
	}

	std::string to_string() const {
		switch (_type) {
		default:
		case type::is_none:
			return _text;
		case type::is_float:
			return _text + std::to_string(_value.float_value);
		case type::is_int32:
			return _text + std::to_string(_value.int32_value);
		}
	}
};

int main() {
	std::cout << "Log" << std::endl;

	circular_array<log_entry, 5> log;

	log.append(log_entry("hello world"));
	for (size_t i = 0; i < 5; i += 1) {
		log.append(log_entry("entry"));
	}
	log.append(log_entry("pi", 3.1415f));
	log.append(log_entry("four", 4));

	for (const auto& entry : log) {
		std::cout << entry.to_string() << std::endl;
	}

	return 0;
}
