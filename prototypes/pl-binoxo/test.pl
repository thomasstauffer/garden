
reads(alex, scifi).
reads(alex, fantasy).
reads(bernd, manuals).
reads(claude, scifi).
reads(claude, manuals).

readssame(N1, N2) :- reads(N1, T), reads(N2, T).

% $ swipl test.pl
% ?- readssame(alex, bernd).
% ?- readssame(alex, claude).

:- initialization(main).

a :- write('a').
b :- write('b').

x :- readssame(alex, bernd).


main :- a, b, write(X), nl, halt.

% main :- halt.

% X = [1].
