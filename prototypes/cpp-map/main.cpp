
#include <map>
#include <vector>

template <typename Key, typename T>
class mymap1 {
private:
	using M = std::map<Key, T>;
	M _data;

public:
	using const_iterator = typename M::const_iterator;

	T& operator[](const Key& key) {
		return _data[key];
	}

	const_iterator begin() const {
		return _data.begin();
	}

	const_iterator end() const {
		return _data.end();
	}

	const_iterator find(const Key& key) const {
		return _data.find(key);
	}
};

template <typename Key, typename T>
class mymap2 {
private:
	using V = std::vector<std::pair<const Key, T>>;
	V _data;

public:
	using const_iterator = typename V::const_iterator;

	// TODO only support insert? would allow to sort immediately
	// TODO when sort is implemented, than log2 search could be used
	T& operator[](const Key& key) {
		const auto it = find(key);
		std::size_t i = _data.size();
		if (it == end()) {
			_data.push_back(std::make_pair(key, T()));
		} else {
			i = it - begin();
		}
		return _data[i].second;
	}

	const_iterator begin() const {
		return _data.begin();
	}

	const_iterator end() const {
		return _data.end();
	}

	const_iterator find(const Key& key) const {
		// find_if can allocate memory
		for (std::size_t i = 0; i < _data.size(); i += 1) {
			if (_data[i].first == key) {
				return const_iterator(&_data[i]);
			}
		}
		return end();
	}
};

#include <iostream>

int main() {
	{
		mymap1<int, int> m;

		m[1] = 2;
		m[2] = 4;

		std::cout << m[1] << std::endl;
	}

	{
		mymap2<int, int> m;

		m[1] = 2;
		m[2] = 4;

		std::cout << m[1] << std::endl;
	}

	return EXIT_SUCCESS;
}
