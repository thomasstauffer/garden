
#include <functional>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

class string_splitter {
public:
	explicit string_splitter(const std::string& string, const std::string& delimiter) : _string(string), _delimiter(delimiter) {
	}

	class iterator {
	public:
		explicit iterator(const std::string& string, const std::string& delimiter, size_t position) : _string(string), _delimiter(delimiter), _position_start(position), _position_end(position), _position_next(position) {
			find_next();
		}

		void operator++() {
			find_next();
		}

		bool operator!=(const iterator& other) const {
			return this->_position_start != other._position_start;
		}

		std::string_view operator*() const {
			// std::cout << "* " << _position_start << " " << _position_end << " " << _position_next << std::endl;
			return std::string_view(_string).substr(_position_start, _position_end - _position_start);
		}

	private:
		void find_next() {
			_position_start = _position_next;
			_position_end = _string.find(_delimiter, _position_start);
			_position_next = _position_end;
			if (_position_next != std::string::npos) {
				_position_next += _delimiter.length();
			}
			// std::cout << "fn " << _position_start << " " << _position_end << " " << _position_next << std::endl;
		}

		const std::string& _string;
		const std::string& _delimiter;
		size_t _position_start;
		size_t _position_end;
		size_t _position_next;
	};

	iterator begin() {
		return iterator(_string, _delimiter, 0);
	}

	iterator end() {
		return iterator(_string, _delimiter, std::string::npos);
	}

private:
	const std::string _string;
	const std::string _delimiter;
};

template <typename T, typename... N>
constexpr auto make_array(N... args) -> std::array<T, sizeof...(args)> {
	return {args...};
}

class command_line {
public:
	using command_type = std::string;
	using arg_type = std::string;
	using args_type = std::vector<arg_type>;
	using callable_type = std::function<void(command_type, args_type)>;

	explicit command_line(callable_type error) : _error(error) {
	}

	void append(const command_type& command, callable_type callable) {
		_commands[command] = callable;
	}

	void execute(const std::string& line) const {
		command_type command;
		args_type args;
		size_t index = 0;
		for (const auto it : string_splitter(line, " ")) {
			if (index == 0) {
				command = command_type(it);
			} else {
				args.push_back(arg_type(it));
			}
			index += 1;
		}

		// std::cout << command << std::endl;

		const auto it = _commands.find(command);

		if (it != _commands.end()) {
			const auto callable = it->second;
			callable(command, args);
		} else {
			_error(command, args);
		}
	}

private:
	std::unordered_map<command_type, callable_type> _commands;
	callable_type _error;
};

void print(const command_line::command_type& command, const command_line::args_type& args) {
	std::cout << command << "(";
	for (const auto& arg : args) {
		std::cout << "[" << arg << "]";
	}
	std::cout << ")" << std::endl;
}

void error(const command_line::command_type& command, const command_line::args_type& args) {
	std::cout << "error " << command << "(";
	for (const auto& arg : args) {
		std::cout << "[" << arg << "]";
	}
	std::cout << ")" << std::endl;
}

int main() {
	for (const auto part : string_splitter("aa bbb cccc xxxxx yyyyyy zzzzzzz", " ")) {
		std::cout << "[" << part << "]" << std::endl;
	}

	const auto array = make_array<int>();

	command_line instance(error);
	instance.append("print", print);
	instance.execute("pint 1 2 3");
	instance.execute("print 2 3 4");

	return 0;
}
