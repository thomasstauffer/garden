extends KinematicBody

var velocity : Vector3 = Vector3()

const SPEED : float = 10.0

func _ready():
	pass

func _physics_process(delta):
	velocity = lerp(velocity, Vector3.ZERO, 0.1)

	var change : Vector3 = Vector3()
	if Input.is_action_pressed("ui_right"):
		change.x += 1
	if Input.is_action_pressed("ui_left"):
		change.x -= 1
	if Input.is_action_pressed("ui_down"):
		change.z += 1
	if Input.is_action_pressed("ui_up"):
		change.z -= 1

	velocity.x = change.x
	velocity.z = change.z
	velocity *= SPEED

	move_and_slide(velocity)
