extends Spatial

func _ready():
	pass

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene("res://scenes/start.tscn")

func _on_ButtonExit_pressed():
	get_tree().change_scene("res://scenes/start.tscn")
