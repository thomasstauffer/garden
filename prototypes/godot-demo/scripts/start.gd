extends Control

func _ready():
	pass

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

func _on_ButtonQuit_pressed():
	get_tree().quit()

func _on_ButtonSceneCollision_pressed():
	get_tree().change_scene("res://scenes/collision.tscn")

func _on_ButtonSceneParticles_pressed():
	get_tree().change_scene("res://scenes/particles.tscn")

func _on_ButtonSceneAnimation_pressed():
	get_tree().change_scene("res://scenes/animation.tscn")

func _on_ButtonSceneNavigation_pressed():
	get_tree().change_scene("res://scenes/navigation.tscn")

func _on_ButtonSceneWater_pressed():
	get_tree().change_scene("res://scenes/water.tscn")

func _on_ButtonSceneMirror_pressed():
	get_tree().change_scene("res://scenes/mirror.tscn")

func _on_ButtonSceneSkeleton_pressed():
	get_tree().change_scene("res://scenes/skeleton.tscn")
