
// https://github.com/Cyan4973/xxHash
function xxHash32(input, seed = 0) {
	function rotl(x, rotate) {
		return (x << rotate) | (x >>> (32 - rotate));
	}

	function round(acc, input) {
		acc += Math.imul(input, prime2);
		acc = rotl(acc, 13);
		acc = Math.imul(acc, prime1);
		return acc;
	}

	const prime1 = 0x9e3779b1;
	const prime2 = 0x85ebca77;
	const prime3 = 0xc2b2ae3d;
	const prime4 = 0x27d4eb2f;
	const prime5 = 0x165667b1;

	let v1 = (seed + prime1 + prime2) >>> 0;
	let v2 = (seed + prime2) >>> 0;
	let v3 = (seed + 0) >>> 0;
	let v4 = (seed - prime1) >>> 0;

	let hash = 0;

	if (input.length >= 4) {
		for (let i = 0; i < (input.length - 3); i += 4) {
			v1 = round(v1, input[i + 0]);
			v2 = round(v2, input[i + 1]);
			v3 = round(v3, input[i + 2]);
			v4 = round(v4, input[i + 3]);
		}
		hash = rotl(v1, 1) + rotl(v2, 7) + rotl(v3, 12) + rotl(v4, 18);
	} else {
		hash = seed + prime5;
	}

	hash += input.length * 4;

	// finalize

	const rest = input.length - (input.length % 4);
	for (let i = rest; i < input.length; i += 1) {
		hash += Math.imul(input[i], prime3);
		hash = Math.imul(rotl(hash, 17), prime4);
	}

	// avalanche

	hash ^= hash >>> 15;
	hash = Math.imul(hash, prime2);
	hash ^= hash >>> 13;
	hash = Math.imul(hash, prime3);
	hash ^= hash >>> 16;

	return hash >>> 0;
}

/*
Test outputs were created from xxhash
*/
function test() {
	const seed = 0;
	const input = [0x10111213, 0x20212223, 0x30313233, 0x40414243, 0x50515253, 0x60616263, 0x70717273, 0x80818283, 0x90919293];

	console.assert(xxHash32(input.slice(0, 1), seed) == 0x3ae682e8);
	console.assert(xxHash32(input.slice(0, 2), seed) == 0x53285325);
	console.assert(xxHash32(input.slice(0, 3), seed) == 0x134ca748);
	console.assert(xxHash32(input.slice(0, 4), seed) == 0x979c20d0);
	console.assert(xxHash32(input.slice(0, 5), seed) == 0x67f27c71);
	console.assert(xxHash32(input.slice(0, 6), seed) == 0x399a1a85);
	console.assert(xxHash32(input.slice(0, 7), seed) == 0x69149858);
	console.assert(xxHash32(input.slice(0, 8), seed) == 0x8cf214a);
	console.assert(xxHash32(input.slice(0, 9), seed) == 0x8ec21377);

	const inputLong = (new Array(1000)).fill(0).map((v, index) => ((index + 1) * 0x87654321) >>> 0);
	console.assert(xxHash32(inputLong, 0) == 0x0e491cb7);
	console.assert(xxHash32(inputLong, 0xFEEDBACC) == 0x21d08749);
}

/*
Intel(R) Core(TM) i7-4710HQ CPU @ 2.50GHz

2023-11-04

node v12.22.9 - 226 MByte/s
FF 117.0.1 - 1098 MByte/s
Brave 1.58.37 - 300 MByte/s
*/
function bench() {
	const size = 1 * 1000 * 1000;
	const repeat = 100;

	const input = (new Array(size)).fill(0).map(_ => Math.floor(2 ** 32 * Math.random()));

	let startTime = new Date();
	for (let i = 0; i < repeat; i += 1) {
		xxHash32(input, 0);
	}

	let endTime = new Date();
	let timeElapsed = endTime - startTime;
	console.log(((size * 4 * repeat) / (timeElapsed / 1000)) / (1000 ** 2), 'MBytes/s');
}

function float2uint32(...floats) {
	const float32Array = new Float32Array(floats);
	const uint32Array = new Uint32Array(float32Array.buffer);
	return Array.from(uint32Array)
}

test();
bench();
console.log(float2uint32(1.0, 1.0e10, 1.0e100))
