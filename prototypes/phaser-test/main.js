/* global Phaser */

const config = {
    type: Phaser.AUTO,
    width: 1280,
    height: 720,
    scale: {
        //mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    scene: {
        preload: preload,
        create: create,
    }
};

const game = new Phaser.Game(config);

function preload() {
    this.load.image('player', 'player.png');
    this.load.image('adventure/front', 'adventure-front.png');
    this.load.image('adventure/back', 'adventure-back.png');
    this.load.image('particle', 'particle.png');
}

// 0xff00ff -> '#ff00ff'
function colorNumberToString(color) {
    return '#' + color.toString(16).padStart(6, '0');
}

function create() {

    this.input.setDefaultCursor('default');
    const cursorInteractive = 'pointer';

    // Border

    const graphics = this.add.graphics();
    graphics.lineStyle(5, 0x009900, 1);
    graphics.strokeRoundedRect(0, 0, 1280, 720, 32);

    // Particles

    const particles = this.add.particles('particle');
    const emitter = particles.createEmitter({
        speed: 70,
        scale: { start: 2, end: 0 },
        blendMode: 'MULTIPLY',
        active: false,
    });

    // Scene

    const self = this;

    function addPlayer(x, y, name, color) {
        self.add.text(x, y, name, { font: '16px Ubuntu', fill: colorNumberToString(color) });
        const player = self.add.image(x, y + 50, 'player');
        player.setInteractive({ cursor: cursorInteractive });
        player.setTint(color);
        player.on('pointerup', () => {
            emitter.startFollow(player);
            emitter.active = true;
        });
        return player;
    }

    function addAdventureCard(x, y, isFront) {
        const [textureFront, textureBack] = ['adventure/front', 'adventure/back']
        const card = self.add.image(x, y, isFront ? textureFront : textureBack);
        card.setInteractive({ cursor: cursorInteractive });
        const tween = self.tweens.add({
            targets: card,
            scaleX: 0.0,
            ease: 'Power2',
            duration: 1000,
            onComplete: () => {
                card.scaleX = 1.0;
                card.setTexture(card.texture.key === textureFront ? textureFront : textureBack);
            },
            paused: true,
        });
        card.on('pointerdown', () => {
            tween.play();
        });
        return card;
    }

    for (let ny = 0; ny < 4; ny += 1) {
        for (let nx = 0; nx < 4; nx += 1) {
            const [x, y] = [400 + nx * 210, 200 + ny * 210];
            const isFront = (nx + ny) < 3;
            addAdventureCard(x, y, isFront);
        }
    }

    addPlayer(100, 100, 'Player 1', 0x000ff);
    addPlayer(100, 300, 'Player 2', 0x6600ff);
    addPlayer(100, 500, 'Player 3', 0xdd00ff);

    // Camera

    const camera = this.cameras.main;
    camera.setBackgroundColor('#cfc');
    this.input.on('wheel', (pointer, deltaX, deltaY, deltaZ) => {
        const factor = deltaZ < 0 ? 1 : -1;
        camera.zoom += factor * 0.05;
    });
}
