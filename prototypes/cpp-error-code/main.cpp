#include <system_error>
#include <iostream>

enum class Err1 {
	Ok = 0,
	A = 1,
	B,
};

enum class Err2 {
	Ok = 0,
	A = 2,
	B
};

struct ErrCategory : std::error_category {
	const char* _name;

	ErrCategory(const char* n) : _name(n) {
	}

	virtual const char* name() const noexcept override {
		return _name;
	}

	virtual std::string message(int ev) const override {
		return "message-todo " + std::string(_name);
	}
};

const ErrCategory theErr1Category("err1");
const ErrCategory theErr2Category("err2");

std::error_code make_error_code(Err1 e)
{
	return {static_cast<int>(e), theErr1Category};
}

std::error_code make_error_code(Err2 e)
{
	return {static_cast<int>(e), theErr2Category};
}

namespace std
{
	template <> struct is_error_code_enum<Err1> : true_type {};
	template <> struct is_error_code_enum<Err2> : true_type {};
}

int main() {
	std::cout << "Error Code" << std::endl;
	std::cout << static_cast<int>(Err1::A) << " " << static_cast<int>(Err2::A) << std::endl;

	std::error_code ec;

	if (!ec) {
		std::cout << "No Error" << std::endl;
	}

	ec = make_error_code(Err1::A);
	std::cout << ec << std::endl;
	std::cout << ec.value() << std::endl;
	std::cout << ec.message() << std::endl;

	ec = make_error_code(Err2::A);
	std::cout << ec << std::endl;
	std::cout << ec.value() << std::endl;
	std::cout << ec.message() << std::endl;

	return 0;
}
