
#include <iostream>

template <typename Interface>
class proxy {
private:
	Interface& _instance;

public:
	explicit proxy(Interface& instance) : _instance(instance) {
	}

	template <typename Callable, typename... Params>
	auto operator()(Callable callable, Params... params) -> decltype((_instance.*callable)(params...)) {
		std::cout << "via proxy" << std::endl;
		return (_instance.*callable)(params...);
	}
};

class test_interface {
public:
	virtual ~test_interface() {
	}

	virtual void print(int value) const = 0;
	virtual int modify(int value) = 0;
};

class test : public test_interface {
public:
	void print(int value) const override {
		std::cout << value << std::endl;
	}

	int modify(int value) override {
		return value * 2;
	}
};

int main() {
	std::cout << "proxy" << std::endl;

	test ze_test;
	ze_test.print(123);

	proxy<test_interface> ze_proxy{ze_test};

	ze_proxy(&test_interface::print, 123);
	std::cout << ze_proxy(&test_interface::modify, 123) << std::endl;
}
