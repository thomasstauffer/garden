
#include <iostream>
#include <chrono>

static int64_t t = 0;

struct MyClock {
    using duration = std::chrono::duration<int64_t, std::micro>;
    using time_point = std::chrono::time_point<MyClock>;

    static time_point now() noexcept {
        t += 111;
        return time_point(std::chrono::microseconds(t));
    }
};

std::chrono::microseconds currentTimeSinceStart() {
    return std::chrono::microseconds(1234567);
}

void f() {
	using namespace std::chrono;

    std::cout << duration_cast<duration<float, std::ratio<3600>>>(currentTimeSinceStart()).count() << std::endl;
    std::cout << duration_cast<microseconds>(currentTimeSinceStart()).count() << std::endl;
    std::cout << duration_cast<milliseconds>(currentTimeSinceStart()).count() << std::endl;
    std::cout << duration_cast<seconds>(currentTimeSinceStart()).count() << std::endl;

    std::cout << (MyClock::now() - MyClock::now()).count() << std::endl;

	auto a = microseconds(200);
	auto b = microseconds(400);

	auto c = b - a;

	std::cout << c.count() << "\n";

	auto d = MyClock::now();
	auto e = MyClock::now();

	std::cout << (e - d).count() << "\n";	
}

int main() {
	f();
    return 0;
}
