
#include <iostream>

struct S1 {
	void test() {
		std::cout << "S1 test" << std::endl;
	}

	void test_integer(int i) {
		std::cout << "S1 test integer " << i << std::endl;
	}
};

struct S2 {
	void test() {
		std::cout << "S2 test" << std::endl;
	}
};

void call_test(...) {
}

template <typename T>
auto call_test(T& t) -> decltype(t.test_integer(0)) { // decltype matters!
	t.test_integer(123);
}

int main() {
	S1 s1;
	S2 s2;

	call_test(s1);
	call_test(s2);
}