#!/usr/bin/env node

/*
server.js local: ok
server.js remote: ok
glitch.me: 502 ??? (same with wscat)
heroku: ok
*/

const WebSocket = require('ws');

const ws = new WebSocket('ws://127.0.0.1:8080');

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

ws.on('open', async () => {
	console.log('onopen');
	const random = Math.floor(1000 * Math.random());
	// eslint-disable-next-line no-constant-condition
	while (true) {
		await sleep(500);
		ws.send(JSON.stringify({type: 'client', message: `Hello from JS Client ${random}`}));
	}
});

ws.on('message', data => {
	console.log('onmessage', data);
});
