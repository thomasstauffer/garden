const ws = require('ws'); // https://github.com/websockets/ws
const fs = require('fs');
const http = require('http');
const https = require('https');

const PORT = process.env.PORT || 8080;
const USE_SSL = false; // instead of using SSL here use a proxy which handles SSL

const options = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem')
};

function main(req, res) {
	if (req.method === 'GET') {
		res.setHeader('Content-Type', 'text/plain');
		res.end('js-broadcast\n');
	}
}

const server = USE_SSL ? https.createServer(options, main) : http.createServer(options, main);

const wsserver = new ws.Server({server});

wsserver.on('connection', (ws, req) => {
	console.log('connection', req.connection.address());
	ws.on('message', data => {
		wsserver.clients.forEach(client => {
			//const isSelf = client === ws;
			const isOpen = client.readyState === ws.OPEN;
			if (isOpen) {
				client.send(data);
			}
		});
	});
});

console.log(`server started (port ${PORT}) ...`);

server.listen(PORT);
