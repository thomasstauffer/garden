#!/usr/bin/env python3

import asyncio
import websockets  # pip3 install websockets

async def hello():
	url = 'ws://127.0.0.1:8080'
	async with websockets.connect(url) as ws:
		await ws.send('Hello from Python Client')
		response = await ws.recv()
		print(response);

asyncio.get_event_loop().run_until_complete(hello())
