
pub fn time() {
    let resolution = nix::time::clock_getres(nix::time::ClockId::CLOCK_MONOTONIC).unwrap();
    println!("{}", resolution);

    let t_real = nix::time::clock_gettime(nix::time::ClockId::CLOCK_REALTIME).unwrap();
    println!("{}", t_real);

    let t_monotonic1 = nix::time::clock_gettime(nix::time::ClockId::CLOCK_MONOTONIC).unwrap();
    println!("{}", t_monotonic1);

    let t_monotonic_now = nix::time::clock_gettime(nix::time::ClockId::CLOCK_MONOTONIC).unwrap();

    let t_monotonic_wait_until = nix::sys::time::TimeSpec::new(t_monotonic_now.tv_sec() + 1, 0);

    nix::time::clock_nanosleep(
        nix::time::ClockId::CLOCK_MONOTONIC,
        nix::time::ClockNanosleepFlags::TIMER_ABSTIME,
        &t_monotonic_wait_until,
    )
    .unwrap();

    let t_monotonic2 = nix::time::clock_gettime(nix::time::ClockId::CLOCK_MONOTONIC).unwrap();
    println!("{}", t_monotonic2);

    let system_time = std::time::SystemTime::UNIX_EPOCH
        + std::time::Duration::new(t_real.tv_sec() as u64, t_real.tv_nsec() as u32);
    let utc: chrono::DateTime<chrono::Utc> = system_time.into();
    println!("{}", utc.format("%Y-%m-%d %H:%M:%S"));
}

pub fn lockall() {
    // https://docs.rs/nix/latest/nix/sys/mman/
    nix::sys::mman::mlockall(
        nix::sys::mman::MlockAllFlags::MCL_CURRENT | nix::sys::mman::MlockAllFlags::MCL_FUTURE,
    )
    .unwrap();
}

pub fn sched() {
    // elevated privileges necessary
    scheduler::set_self_policy(scheduler::Policy::Fifo, 80).unwrap();
}

fn main() {
    time();
    lockall();
    sched();
}
