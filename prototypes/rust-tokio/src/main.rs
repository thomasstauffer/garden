#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream, UdpSocket, UnixListener, UnixStream};

use std::error::Error;

/*
TCP

nc 127.0.0.1 8080
nc 127.0.0.1 8081

UDP

nc -u 127.0.0.1 8080
nc -u 127.0.0.1 8081

Unix Domain Socket

nc -U /tmp/rust-tokio-1
nc -U /tmp/rust-tokio-2

*/

fn oo_error() -> Result<(), Box<dyn Error>> {
    return Result::Err(Box::new(std::io::Error::new(
        std::io::ErrorKind::Other,
        "Oo",
    )));
}

async fn run_sleepy() -> Result<(), Box<dyn Error>> {
    for count in 0..12 {
        tokio::time::sleep(std::time::Duration::from_secs(5)).await;
        println!("Async task: Count {}", count);
    }
    Ok(())
}

async fn run_udp_server(server_address: &str) -> Result<(), Box<dyn Error>> {
    let socket = UdpSocket::bind(server_address).await?;
    println!("UDP server is listening on {}", server_address);

    let mut buf = vec![0; 1024];

    loop {
        let (len, client_address) = socket.recv_from(&mut buf).await?;
        println!("UDP received from {}: {:?}", client_address, &buf[..len]);
        socket.send_to(&buf[..len], &client_address).await?;
    }
}

async fn handle_tcp_client(
    mut socket: TcpStream,
    client_address: std::net::SocketAddr,
) -> Result<(), Box<dyn Error>> {
    // oo_error()?;

    loop {
        let mut buf = [0u8; 1024];
        let len = socket.read(&mut buf).await?;
        println!("TCP received from {}: {:?}", client_address, &buf[..len]);
        if len == 0 {
            return Ok(());
        }
        socket.write_all(&buf[0..len]).await?;
    }
}

async fn run_tcp_server(server_address: &str) -> Result<(), Box<dyn Error>> {
    //oo_error()?;

    let listener = TcpListener::bind(&server_address).await?;
    println!("TCP server is listening on: {}", server_address);

    loop {
        let (socket, client_address) = listener.accept().await?;
        println!("TCP server client connected from {}", client_address);

        let parallel = true;
        if parallel {
            tokio::spawn(async move {
                // TODO error not properly propagates out of this context, tokio::io?
                handle_tcp_client(socket, client_address).await.unwrap();
                //oo_error()?;
            });
        } else {
            handle_tcp_client(socket, client_address).await?;
        }

        // oo_error()?;
    }
}

async fn handle_unix_client(
    mut socket: UnixStream,
    client_address: tokio::net::unix::SocketAddr,
) -> Result<(), Box<dyn Error>> {
    loop {
        let mut buf = [0; 1024];
        let len = socket.read(&mut buf).await?;

        println!("Unix received from {:?}: {:?}", client_address, &buf[..len]);
        if len == 0 {
            return Ok(());
        }
        socket.write_all(&buf[0..len]).await?;
    }
}

async fn run_unix_server(socket_path: &str) -> Result<(), Box<dyn Error>> {
    if std::path::Path::new(socket_path).exists() {
        std::fs::remove_file(socket_path)?;
    }

    let listener = UnixListener::bind(socket_path).unwrap();
    println!("Unix server is listening on: {}", socket_path);

    loop {
        let (socket, client_address) = listener.accept().await?;
        tokio::spawn(async move {
            handle_unix_client(socket, client_address).await.unwrap();
        });
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let address1 = "127.0.0.1:8080";
    let address2 = "127.0.0.1:8081";

    tokio::try_join!(
        run_sleepy(),
        run_tcp_server(address1),
        run_tcp_server(address2),
        run_udp_server(address1),
        run_udp_server(address2),
        run_unix_server("/tmp/rust-tokio-1"),
        run_unix_server("/tmp/rust-tokio-2"),
    )?;

    Ok(())
}
