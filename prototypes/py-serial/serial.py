#!/usr/bin/env python3

import serial  # pip install pyserial

SERIAL_PORT = '/dev/ttymxc0'

ser = serial.Serial(SERIAL_PORT, 115200, timeout=5.0, parity=serial.PARITY_NONE)
print(ser.name)
ser.write(b'TEST\r\n')
data = ser.read(128)
print(data)
ser.close()
