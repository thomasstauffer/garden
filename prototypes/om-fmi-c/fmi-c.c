
/*
https://github.com/viproma/debian-fmilib/tree/master/ThirdParty/FMI/default/FMI2
https://github.com/modelica/fmi-standard/releases/tag/v2.0.2
https://www.asawicki.info/news_1420_generating_lib_file_for_dll_library

Notes:

clang.exe fmi-c.c -I . -o fmi-c.exe -L . -l Test3 && .\fmi-c.exe > out.csv

The lib inside the fmu has different functions names and cannot used for
compiling. fmi2GetVersion is just Test3_fmiGetVersion. Somehow
Test3_fmi2GetVersion was expected.

The dll is extracted from the fmu file. The folder can be deleted after, as it
seems it is not referenced anymore by the OpenModelica dll. For linking a lib
was created manually with dumpbin/lib.

1. dumpbin /exports Test3.dll > Test3.def
2. only keep the function names inside Test3.def, delete the rest
3. lib /def:Test3.def /out:Test3.lib /machine:amd64

So far I do not fully understand what the official way, to do it properly, is.
Maybe opening the dll manually and get the function adresses. This allows to
load multiple dlls then.

Modifing the fmi2Functions.h and then use:

#define FMI2_FUNCTION_PREFIX Test3_fmi

crashes the application.

When loading the dll manually and assigning the functions manually compilations
is done via:

clang.exe fmi-c.c -I . -o fmi-c.exe && fmi-c.exe
*/

#include "fmi2Functions.h"

#include <stdio.h>
#include <stdarg.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

const char *s2s(fmi2Status status)
{
    switch (status)
    {
    case fmi2OK:
        return "OK";
    case fmi2Warning:
        return "Warning";
    case fmi2Discard:
        return "Discard";
    case fmi2Error:
        return "Error";
    case fmi2Fatal:
        return "Fatal";
    case fmi2Pending:
        return "Pending";
    default:
        return "?";
    }
}

void cb_logger(fmi2ComponentEnvironment componentEnvironment,
               fmi2String instanceName,
               fmi2Status status,
               fmi2String category,
               fmi2String message, ...)
{
    printf("instance:%s status:%s category:%s\n", instanceName, s2s(status), category);

    va_list myargs;
    va_start(myargs, message);
    vprintf(message, myargs);
    va_end(myargs);
}

void *cb_alloc(size_t nobj, size_t size)
{
    // printf("alloc\n");
    return calloc(nobj, size);
}

void cb_free(void *obj)
{
    // printf("free\n");
    free(obj);
}

void cb_finished(fmi2ComponentEnvironment env, fmi2Status status)
{
    printf("finished\n");
}

void check(fmi2Status status)
{
    if (status != fmi2OK)
    {
        printf("Error: %s\n", s2s(status));
        exit(1);
    }
}

#define CONCAT(a, b) a##b
#define DEFFUN(name) CONCAT(name, TYPE)* name = (CONCAT(name, TYPE)*)GetProcAddress(hModule, #name)

int main()
{
    printf("fmi-c\n");

    HMODULE hModule = LoadLibrary("Test3.dll");

    DEFFUN(fmi2GetVersion);
    DEFFUN(fmi2GetTypesPlatform);
    DEFFUN(fmi2Instantiate);
    DEFFUN(fmi2SetupExperiment);
    DEFFUN(fmi2EnterInitializationMode);
    DEFFUN(fmi2ExitInitializationMode);
    DEFFUN(fmi2DoStep);
    DEFFUN(fmi2GetReal);
    DEFFUN(fmi2SetReal);
    DEFFUN(fmi2FreeInstance);

    const char *version = fmi2GetVersion();
    const char *platform = fmi2GetTypesPlatform();

    printf("%s\n", version);
    printf("%s\n", platform);

    fmi2CallbackFunctions cbs = {cb_logger, cb_alloc, cb_free, cb_finished, NULL};

    fmi2Component c = fmi2Instantiate("instance",
        fmi2CoSimulation,
        "{a99adaef-aaf8-493b-86e1-0798aae39ba7}",
        "file:///", // should be the path to the resource folder, but doesn't seem to matter for OpenModelica dlls
        &cbs,
        fmi2False,
        fmi2False);

    const double stopTime = 4.0;
    const double h = 0.25;
    double t = 0.0;

    fmi2Status s;
    s = fmi2SetupExperiment(c, fmi2False, 0.0, 0.0, fmi2False, stopTime);
    check(s);
    s = fmi2EnterInitializationMode(c);
    check(s);
    s = fmi2ExitInitializationMode(c);
    check(s);

    while ((t < stopTime) && (s == fmi2OK))
    {
        fmi2ValueReference vr[] = { 11 }; // VSin_v=11, VSin_V=12;
        fmi2Real v[1];

        fmi2GetReal(c, vr, 1, v);

        printf("%g,%g\n", t, v[0]);

        if (t > 2) {
            vr[0] = 12;
            v[0] = 150.0;
            fmi2SetReal(c, vr, 1, v);
        }

        s = fmi2DoStep(c, t, h, fmi2True);
        t += h;
    }

    check(s);
    fmi2FreeInstance(c);

    return 0;
}
