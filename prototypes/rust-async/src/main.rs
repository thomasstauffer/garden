use async_std::channel::bounded;
use async_std::channel::Receiver;
use async_std::channel::Sender;
use async_std::io::ReadExt;
use async_std::io::WriteExt;
use async_std::net::SocketAddr;
use async_std::net::TcpListener;
use async_std::net::TcpStream;
use async_std::net::UdpSocket;
use async_std::pin::Pin;
use async_std::prelude::Future;
use async_std::task;
use core::time::Duration;

async fn alive(id: i32) {
    for n in 0..3 {
        task::sleep(Duration::from_secs(1)).await;
        println!("Alive ID:{} N:{}", id, n);
    }
}

async fn producer(tx: Sender<i32>) {
    let mut n: i32 = 0;
    loop {
        n += 1;
        tx.send(n).await.unwrap();
        task::sleep(Duration::from_secs(1)).await;
    }
}

async fn udp_server(address: &str) {
    let socket = UdpSocket::bind(address).await.unwrap();
    println!("UDP server listening on {}", socket.local_addr().unwrap());

    let mut buf = vec![0u8; 1024];

    loop {
        let (len, addr) = socket.recv_from(&mut buf).await.unwrap();
        socket.send_to(&buf[..len], &addr).await.unwrap();
        println!(
            "UDP received from {}: {}",
            addr,
            std::str::from_utf8(&buf[..len]).unwrap()
        );
    }
}

async fn tcp_peer(mut stream: TcpStream, addr: SocketAddr) {
    println!("TCP peer open from {}", addr);

    let mut buf = vec![0u8; 1024];

    loop {
        let len = stream.read(&mut buf).await.unwrap();
        println!(
            "TCP peer received from {}: {}",
            addr,
            std::str::from_utf8(&buf[..len]).unwrap()
        );
        if len == 0 {
            break;
        }
        stream.write_all(&buf[0..len]).await.unwrap();
    }

    println!("TCP peer closed from {}", addr);
}

async fn tcp_server(address: &str) {
    let listener = TcpListener::bind(address).await.unwrap();
    println!("TCP server listening on {}", listener.local_addr().unwrap());

    // TODO is it easily possible and a idea worth to implement to also use select_all instead of task::spawn?

    loop {
        let (stream, addr) = listener.accept().await.unwrap();
        task::spawn(tcp_peer(stream, addr));
        //tcp_peer(stream, addr).await;
    }
}


use async_std::stream::StreamExt;

async fn tcp_server_comm(address: &str, mut rx: Receiver<i32>) {
    let listener = TcpListener::bind(address).await.unwrap();
    println!("TCP server listening on {}", listener.local_addr().unwrap());

    loop {
        let (mut stream, addr) = listener.accept().await.unwrap();
        println!("TCP peer open from {}", addr);

        while let Some(value) = rx.next().await {
            let msg = value.to_string().into_bytes();
            if let Err(_) = stream.write_all(&msg[0..msg.len()]).await {
                break;
            }
        }

        println!("TCP peer closed from {}", addr);
    }
}

#[async_std::main]
async fn main() {
    let (tx, rx) = bounded::<i32>(3);
    type MyFuture = Pin<Box<dyn Future<Output = ()>>>;
    let mut futures: Vec<MyFuture> = Vec::new();
    futures.push(Box::pin(udp_server("127.0.0.1:8080")));
    futures.push(Box::pin(tcp_server("127.0.0.1:8080")));
    futures.push(Box::pin(producer(tx)));
    futures.push(Box::pin(tcp_server_comm("127.0.0.1:8081", rx)));
    let always_alive = futures.len();

    let mut n: i32 = 0;
    while !futures.is_empty() {
        if futures.len() == always_alive {
            n += 1;
            futures.push(Box::pin(alive(n)));
        }
        let (_, _, remaining) = futures::future::select_all(futures).await;
        futures = remaining;
    }
}
