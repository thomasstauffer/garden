#!/usr/bin/env python3

import collections
import math
import signal
import sys
import time

import OMSimulator as oms  # pip3 install OMSimulator

from PyQt5.QtCore import Qt, QTimer, QRectF, QPointF, QSettings, QPoint, QSize
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QListWidget, QListWidgetItem, QCheckBox, QLabel, QLineEdit, QToolBar, QAction
from PyQt5.QtChart import QChart, QChartView, QLineSeries  # pip3 install pyqtchart

Signal = collections.namedtuple("Signal", "cref type kind model series points")


def limit(v, vmin, vmax):
	return min(max(v, vmin), vmax)


class SignalBase:
	def __init__(self, name):
		self.name = name
		self.series = QLineSeries()
		self.series.setName(self.name)
		self.points = []
		self.is_editable = False
		self.is_calculated = False

	def calc(self, time):
		pass

	def set(self, value):
		pass

	def get(self):
		return 0.0


class SignalFMI(SignalBase):
	def __init__(self, model, fmiSignal):
		key, value = fmiSignal
		name = key[len('model.root.system.'):]
		super().__init__(name)
		self.is_editable = value['type'] == 'Real'

		self._model = model
		self._key = key
		self._value = value
		self._cref = key[len('model.'):]

	def get(self):
		if self._value['type'] == 'Real':
			return self._model.getReal(self._cref)
		elif self._value['type'] == 'Enumeration':
			return self._model.getInteger(self._cref)
		elif self._value['type'] == 'Bool':
			return 1 if self._model.getBoolean(self._cref) else 0
		else:
			assert(False)

	def set(self, value):
		if self._value['type'] == 'Real':
			self._model.setReal(self._cref, value)


class SignalInterpolation(SignalBase):
	def __init__(self, name, table):
		super().__init__(name)
		self.is_calculated = True
		self._table = table
		self._y = 20.0

	def calc(self, time):
		i = 0
		length = len(self._table)

		for table_index in range(length):
			if self._table[table_index][0] >= time:
				break
			i = table_index

		if i < (length - 1):
			t0, t1 = self._table[i][0], self._table[i+1][0]
			y0, y1 = self._table[i][1], self._table[i+1][1]
			p = 0.0 if t1 == t0 else (time - t0) / (t1 - t0)
			dy = y1 - y0
			self._y = (dy * p) + y0
		else:
			self._y = self._table[length-1][1]

	def get(self):
		return self._y

	def set(self):
		pass


class SignalPID(SignalBase):
	def __init__(self, name, signalDesired, signalMeasured, signalOutput):
		super().__init__(name)
		self.is_calculated = True

		self.signalDesired = signalDesired
		self.signalMeasured = signalMeasured
		self.signalOutput = signalOutput

		self.target = 0

		self.i = 0
		self.e = 0
		self.e_previous = 0.0
		self.t_previous = 0.0

	def calc(self, t):
		DT = 0.005
		KP, KI, KD = 30, 10, 0

		self.target = self.signalDesired.get()

		e = self.target - self.signalMeasured.get()
		self.i += e * DT
		self.i = limit(self.i, 0, 5)
		d = (e - self.e_previous) / DT
		out = (KP * e) + (KI * self.i) + (KD * d)
		out = limit(out, 0, 24)

		self.signalOutput.set(out)
		self.e_previous = e
		self.t_previous = t

	def get(self):
		return self.target

	def set(self):
		pass


class SignalItemWidget(QWidget):
	def __init__(self, signal):
		super(SignalItemWidget, self).__init__()

		widgetCheckBox = QCheckBox(signal.name)
		widgetCheckBox.setChecked(signal.series.isVisible())
		widgetCheckBox.stateChanged.connect(self.on_check)
		#if signal.kind == 'parameter':
		#	widgetCheckBox.setStyleSheet('QCheckBox { color: blue }')

		widgetEdit = QLineEdit()
		widgetEdit.setText(str(signal.get()))
		widgetEdit.setReadOnly(not signal.is_editable)

		if signal.is_editable:
			widgetEdit.textChanged.connect(self.on_edit)
		else:
			widgetEdit.setStyleSheet('QLineEdit { color: red }')

		layout = QHBoxLayout()
		layout.addWidget(widgetCheckBox, 70)
		layout.addWidget(widgetEdit, 30)
		self.setLayout(layout)

		self.signal = signal
		self.widgetCheckBox = widgetCheckBox
		self.widgetEdit = widgetEdit

	def on_check(self, state):
		self.signal.series.setVisible(state == Qt.Checked)
		self.signal.series.clear()

	def on_edit(self):
		self.signal.set(float(self.widgetEdit.text()))


class Simulator(QMainWindow):
	def __init__(self):
		super(Simulator, self).__init__()

		timer = QTimer(self)
		timer.timeout.connect(self.tick)
		timer.start(50)

		toolbar = self.addToolBar('Main')
		actionPause = QAction('Pause', toolbar)
		actionResume = QAction('Resume', toolbar)
		actionPause.triggered.connect(self.pause)
		actionResume.triggered.connect(self.resume)
		toolbar.addAction(actionPause)
		toolbar.addAction(actionResume)

		widgetSignals = QListWidget(self)
		self.widgetSignals = widgetSignals

		chart = QChart()
		chart.legend().setVisible(True)
		chart.legend().setAlignment(Qt.AlignBottom)
		widgetChartView = QChartView(chart)
		widgetChartView.setRenderHint(QPainter.Antialiasing)
		widgetChartView.setRubberBand(QChartView.RectangleRubberBand)
		self.chart = chart
		#self.series = series

		widgetMain = QWidget(self)
		layoutMain = QHBoxLayout()
		layoutMain.addWidget(self.widgetSignals, 25)
		layoutMain.addWidget(widgetChartView, 75)
		widgetMain.setLayout(layoutMain)

		self.setCentralWidget(widgetMain)

		self.running = False
		self.time = 0.0
		self.min = 1000
		self.max = -1000

		self.every_second = time.time()

	def closeEvent(self, event):
		self.settings.setValue('size', self.size())
		self.settings.setValue('pos', self.pos())
		names = [ signal.name for signal in self.signals if signal.series.isVisible() ]
		self.settings.setValue('visible-signals', names)
		event.accept()

	def resume(self):
		self.running = True

	def pause(self):
		self.running = False
		for signal in self.signals:
			if signal.series.isVisible():
				signal.series.replace(signal.points)

	def tick(self):
		if not self.running:
			return

		t1 = time.time()

		MARGIN = 0.05
		T_STEP = 0.05
		SUBSTEPS = 10

		for n in range(SUBSTEPS):
			self.model.stepUntil(self.time)


			for signal in self.signals:
				if signal.is_calculated:
					signal.calc(self.time)

				if signal.series.isVisible():
					y = signal.get()
					if y < self.min:
						self.min = y - (abs(y) * MARGIN)
					if y > self.max:
						self.max = y + (abs(y) * MARGIN)
					signal.points.append(QPointF(self.time, y))

			self.time += T_STEP / SUBSTEPS

		MAX_POINT_THRESHOLD = 20000
		VISIBLE_SECONDS = 5.0
		VISIBLE_POINTS = int(VISIBLE_SECONDS * SUBSTEPS * (1 / T_STEP))
		count_points = 0
		for signal in self.signals:
			count_points += len(signal.points)
			if len(signal.points) > MAX_POINT_THRESHOLD:
				del signal.points[:int(MAX_POINT_THRESHOLD * 0.2)]

		t2 = time.time()

		for signal in self.signals:
			if signal.series.isVisible():
				signal.series.replace(signal.points[-VISIBLE_POINTS::10])

		axisH = self.chart.axes(Qt.Horizontal)[0]
		axisH.setRange(self.time - VISIBLE_SECONDS, self.time)
		axisV = self.chart.axes(Qt.Vertical)[0]
		axisV.setRange(self.min, self.max)

		t3 = time.time()

		t = time.time()
		if t > (self.every_second + 1.0):
			self.every_second = t
			print(int(1000 * (t2 - t1)), int(1000 * (t3 - t2)), count_points)

	def load(self, filename):
		# ~/.config/tom
		settings = QSettings('tom', 'FMISimulator')
		self.settings = settings

		model = oms.newModel('model')
		root = model.addSystem('root', oms.Types.System.SC)
		root.addSubModel('system', filename)
		model.resultFile = ''
		#model.stopTime = 1000  # ???
		model.instantiate()
		model.initialize()

		visible_signals = self.settings.value('visible-signals', [])

		def find_signal(name):
			for signal in signals:
				if signal.name == name:
					return signal

		self.widgetSignals.clear()
		signals = model.getAllSignals()
		signals = [SignalFMI(model, signal) for signal in signals.items()]

		# 1000 RPM = 105 rad/s
		# 10 RPM = 1.05 rad/s
		RAMP_SLEW = 5
		RAMP_Y = 20
		ramp = []
		y = 0.0
		for t in range(0, 30, 10):
			yo = RAMP_Y if y == 0 else 0
			ramp.append((t, y))
			ramp.append((t + RAMP_SLEW, yo))
			y = yo
		print(ramp)

		signals.append(SignalInterpolation('DesiredSpeed', ramp))
		signals.append(SignalPID('PID', find_signal('DesiredSpeed'),find_signal('speed.w'),find_signal('const.k')))

		for signal in signals:
			self.chart.addSeries(signal.series)
			signal.series.setVisible(signal.name in visible_signals)
			item = QListWidgetItem(self.widgetSignals)
			custom = SignalItemWidget(signal)
			item.setSizeHint(custom.minimumSizeHint())
			self.widgetSignals.setItemWidget(item, custom)
			self.widgetSignals.addItem(item)

		self.chart.createDefaultAxes()
		axisH = self.chart.axes(Qt.Horizontal)[0]
		axisH.setTitleText('Time')

		self.signals = signals
		self.model = model
		self.running = True

		if self.settings.value('size') == None:
			self.showMaximized()
		else:
			self.resize(self.settings.value('size', QSize(270, 225)))
			self.move(self.settings.value('pos', QPoint(50, 50)))
			self.show()


def main(filename):
	oms.setTempDirectory('/tmp/ompy')
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	app = QApplication(sys.argv)
	simulator = Simulator()
	simulator.load(filename)
	app.exec_()


if __name__ == '__main__':
	main(sys.argv[1])
