model Test
  Modelica.Electrical.Analog.Basic.Resistor R(R = 100, T_ref = 293.15, alpha = 1e-3, i(start = 0), useHeatPort = false) annotation(
    Placement(visible = true, transformation(origin = {10, 10}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.SineVoltage VSin(V = 220, f = 1) annotation(
    Placement(visible = true, transformation(origin = {-50, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 270)));
  Modelica.Electrical.Analog.Basic.Ground G annotation(
    Placement(visible = true, transformation(extent = {{-40, -40}, {-20, -20}}, rotation = 0)));
equation
  connect(G.p, VSin.n) annotation(
    Line(points = {{-30, -20}, {-50, -20}, {-50, 0}}, color = {0, 0, 255}));
  connect(G.p, R.n) annotation(
    Line(points = {{-30, -20}, {10, -20}, {10, 0}}, color = {0, 0, 255}));
  connect(VSin.p, R.p) annotation(
    Line(points = {{-50, 20}, {-50, 40}, {10, 40}, {10, 20}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "4.0.0")),
    Diagram,
    version = "");
end Test;