model Test2
  //Real MY=10; //(start=110, fixed=false);
  Modelica.Electrical.Analog.Basic.Resistor R(R = 100, T_ref = 293.15, alpha = 1e-3, i(start = 0), useHeatPort = false) annotation(
    Placement(visible = true, transformation(origin = {10, 10}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.SineVoltage VSin(V = 100, f = 1) annotation(
    Placement(visible = true, transformation(origin = {-50, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 270)));
  Modelica.Electrical.Analog.Basic.Ground G annotation(
    Placement(visible = true, transformation(extent = {{-40, -40}, {-20, -20}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage VCon(V = 100)  annotation(
    Placement(visible = true, transformation(origin = {-50, 50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(G.p, VSin.n) annotation(
    Line(points = {{-30, -20}, {-50, -20}, {-50, 0}}, color = {0, 0, 255}));
  connect(G.p, R.n) annotation(
    Line(points = {{-30, -20}, {10, -20}, {10, 0}}, color = {0, 0, 255}));
  connect(VSin.p, VCon.n) annotation(
    Line(points = {{-50, 20}, {-50, 40}}, color = {0, 0, 255}));
  connect(VCon.p, R.p) annotation(
    Line(points = {{-50, 60}, {-50, 80}, {10, 80}, {10, 20}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "4.0.0")));
end Test2;