﻿
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using System;
using System.Net;
using System.Threading.Tasks;

/*
dotnet add package Microsoft.Orleans.Core.Abstractions --version 3.7
dotnet add package Microsoft.Orleans.CodeGenerator.MSBuild --version 3.7
dotnet add package Microsoft.Orleans.Server --version 3.7
dotnet add package Microsoft.Orleans.Client --version 3.7

dotnet add package Microsoft.Extensions.Hosting
dotnet add package Microsoft.Extensions.Logging.Console
*/

public interface ISayHelloGrain : IGrainWithStringKey
{
    Task<string> SayHello(string name);
}

public class SayHelloGrain : Grain, ISayHelloGrain
{
    public Task<string> SayHello(string name)
    {
        return Task.FromResult($"Hello {name}");
    }
}

public class MyOrleans
{
    public static async Task Main(string[] args)
    {
        if (args.Length != 1)
        {
            return;
        }

        if (args[0] == "server")
        {
            await Server();
        }
        else if (args[0] == "client")
        {
            await Client();
        }
    }

    public static async Task Client()
    {
        var client = new ClientBuilder()
        .UseLocalhostClustering()
        .Build();

        using (client)
        {
            await client.Connect();

            var grain = client.GetGrain<ISayHelloGrain>("GrainID");
            var answer = await grain.SayHello("World");
            Console.WriteLine(answer);
        }
    }

    public static async Task Server()
    {
        var host = new HostBuilder()
        .UseOrleans(builder =>
        {
            builder.UseLocalhostClustering()
            .Configure<ClusterOptions>(options =>
            {
                options.ClusterId = "MyCluster";
                options.ServiceId = "MyService";
            })
            .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
            .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(SayHelloGrain).Assembly).WithReferences())
            .ConfigureLogging(logging => logging.AddConsole());
        }).Build();

        await host.StartAsync();

        Console.WriteLine("Press Enter to Terminate ...");
        Console.ReadLine();

        await host.StopAsync();
    }
}
