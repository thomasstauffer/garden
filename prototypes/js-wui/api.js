import * as wui from './wui.js';

function init() {
    return { count: 0 };
}

function update(state, event) {
    console.log(event.name);
    if (event.name == 'count++') {
        return [{ ...state, count: state.count + 1 }];
    }
    return [state];
}

function App(props) {
    const ipsum = 'Exercitation dolor reprehenderit deserunt non irure veniam occaecat velit nostrud in et. Est velit pariatur incididunt adipisicing excepteur laboris officia mollit pariatur excepteur quis culpa ex. ';

    return wui.h(wui.Main, {}, [
        wui.h(wui.GridH, { columns: [1, 3] }, [
            wui.h(wui.Group, {}, [
                wui.h(wui.EditLine, { text: '', placeholder: '🔎 Search' }),
                wui.h(wui.Text, { text: props.state.count }),
                wui.h(wui.Tree, {}),
            ]),
            wui.h(wui.FlowV, {}, [
                wui.h(wui.Group, {}, [
                    wui.h(wui.EditLine, { text: '', placeholder: 'Address, e.g. serial://COM5' }),
                    wui.h(wui.FlowH, {}, [
                        wui.h(wui.Button, { text: 'Set' }),
                        wui.h(wui.Button, { text: 'Get' }),
                        wui.h(wui.Button, { text: 'count++', onClick: 'count++' }),
                    ]),
                    wui.h(wui.EditLine, { text: '', placeholder: 'Property, e.g. /alpha/beta/gamma' }),
                    wui.h(wui.EditArea, { text: '', placeholder: 'Value' }),
                ]),
                wui.h(wui.Group, {}, [
                    wui.h(wui.Text, { text: ipsum + ipsum + ipsum + ipsum + ipsum }),
                ]),
                wui.h(wui.Group, {}, [
                    wui.h(wui.Message, { head: 'Info', body: 'Info ...', type: 'info' }),
                    wui.h(wui.Message, { head: 'Warning', body: 'Warning ...', type: 'warning' }),
                    wui.h(wui.Message, { head: 'Danger', body: 'Danger ...', type: 'danger' }),
                ]),
            ]),
        ]),
    ]);
}

function view(state) {
    return wui.h(App, { state: state })
}

wui.run(init, update, view);
