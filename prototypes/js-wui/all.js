import * as wui from './wui.js';

function init() {
    return { count: 3, text: 'Some text. To be edited.', fetched: 'Nothing fetched yet.' };
}

function update(state, event) {
    console.log(event.name);
    if (event.name == 'count++') {
        return [{ ...state, count: state.count + 1 }];
    } else if (event.name == 'textchanged') {
        return [{ ...state, text: event.value }];
    } else if (event.name == 'fetch') {
        return [state, () => wui.fetchText('./', 'fetch-result')];
    } else if (event.name == 'fetch-result') {
        return [{ ...state, fetched: event.value }];
    }
    return [state];
}

function App(props) {
    return wui.h(wui.Main, {}, [
        wui.h(wui.Button, { text: 'Button (count++)', onClick: 'count++' }),
        wui.h(wui.Text, { text: 'Count ' + props.state.count }),
        wui.h(wui.Group, {}, [
            wui.h(wui.Button, { text: 'Button (fetch)', onClick: 'fetch' }),
            wui.h(wui.Button, { text: 'Button 3' }),
            wui.h(wui.Text, { text: props.state.fetched }),
        ]),
        wui.h(wui.EditLine, { text: props.state.text, placeholder: 'Hint None', onInput: 'textchanged' }),
        wui.h(wui.Text, { text: props.state.text }),
        wui.h(wui.EditLine, { text: '', placeholder: 'Hint 1' }),
        wui.h(wui.EditArea, { text: '', placeholder: 'Hint 2' }),
        wui.h(wui.Message, { head: 'Info', body: 'Info ...', type: 'info' }),
        wui.h(wui.Message, { head: 'Warning', body: 'Warning ...', type: 'warning' }),
        wui.h(wui.Message, { head: 'Danger', body: 'Danger ...', type: 'danger' }),
    ]);
}

function view(state) {
    return wui.h(App, { state: state })
}

wui.run(init, update, view);
