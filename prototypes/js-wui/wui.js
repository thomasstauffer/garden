import { h, render } from 'https://unpkg.com/preact?module';

export { h };

const g = {
    state: {},
    update: null,
    view: null,
};

export async function fetchText(url, eventName) {
    const response = await fetch(url, { mode: 'same-origin' });
    const text = await response.text();
    const event = { name: eventName, value: text };
    return event;
}

export function run(init, update, view) {
    g.state = init();
    g.update = update;
    g.view = view;
    render(g.view(g.state), document.body);
}

function signal(event) {
    if (event.name === undefined) {
        return;
    }
    const currentState = g.state;
    const [newState, command] = g.update(currentState, event); // ??? remove await should be done via commands
    g.state = newState;
    if ((command !== undefined) && (command !== null)) {
        new Promise(async (resolve, _reject) => {
            const event = await command();
            signal(event);
            resolve();
        });
    }
    render(g.view(g.state), document.body);
}

export function Main(props) {
    return h('main', { class: 'wui-main wui-flow-v' }, props.children);
}

export function FlowH(props) {
    return h('div', { class: 'wui-flow-h' }, props.children);
}

export function GridH(props) {
    const style = 'grid-template-columns: ' + props.columns.map($ => $ + 'fr').join(' ');
    return h('div', {
        class: 'wui-grid-h',
        style: style
    }, props.children);
}

export function FlowV(props) {
    return h('div', { class: 'wui-flow-v' }, props.children);
}

export function Group(props) {
    return h('div', { class: 'wui-group' }, props.children);
}

export function Button(props) {
    return h('button', {
        class: 'wui-button',
        onClick: () => signal({ name: props.onClick })
    }, props.text);
}

export function Text(props) {
    return h('p', {}, props.text);
}

export function Tree(props) {
    return h('p', {}, 'Tree ...');
}

export function EditLine(props) {
    return h('input', {
        class: 'wui-editline',
        type: 'text',
        value: props.text,
        placeholder: props.placeholder,
        onChange: (event) => signal({ name: props.onChange, value: event.target.value }),
        onInput: (event) => signal({ name: props.onInput, value: event.target.value }),
    });
}

export function EditArea(props) {
    return h('textarea', {
        class: 'wui-editarea',
        type: 'text',
        placeholder: props.placeholder
    }, props.text);
}

export function Message(props) {
    return h('div', { class: 'wui-message-' + props.type }, [
        h('div', { class: 'wui-message-head' }, props.head),
        h('div', { class: 'wui-message-body' }, props.body),
    ]);
}
