#!/usr/bin/env python3

# https://arduinofactory.com/pyfirmata/

import pyfirmata  # pip install pyfirmata
import time

port = '/dev/ttyUSB0'

board = pyfirmata.Arduino(port)
LED_pin = board.get_pin('d:13:o')

print('Start')

for i in range(10):
	LED_pin.write(True)
	time.sleep(0.5)
	LED_pin.write(False)
	time.sleep(0.5)

board.exit()
