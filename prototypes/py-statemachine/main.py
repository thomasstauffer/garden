#!/usr/bin/env python3

class Switch:
	STATE_STOPPED, STATE_RUNNING, STATE_STOPPING = "S_STOPPED", "S_RUNNING", "S_STOPPING"
	EVENT_START, EVENT_STOP, EVENT_TIMER_EXPIRED = "E_START", "E_STOP", "E_TIMER_EXPIRED"

	def __init__(self):
		self.state = Switch.STATE_STOPPED
		self.time = 0
		self.timer = None

	def handle_event(self, event):
		if self.state == Switch.STATE_STOPPED:
			if event == Switch.EVENT_START:
				self.state = Switch.STATE_RUNNING
		elif self.state == Switch.STATE_RUNNING:
			if event == Switch.EVENT_STOP:
				self.state = Switch.STATE_STOPPING
				self.timer = self.time + 2
		elif self.state == Switch.STATE_STOPPING:
			if event == Switch.EVENT_TIMER_EXPIRED:
				self.state = Switch.STATE_STOPPED

	def start(self):
		self.handle_event(Switch.EVENT_START)

	def stop(self):
		self.handle_event(Switch.EVENT_STOP)

	def tick(self):
		if (self.timer != None) and (self.time >= self.timer):
			self.handle_event(Switch.EVENT_TIMER_EXPIRED)
		self.time += 1

	def get_state(self):
		return self.state

class StatePattern:
	class State:
		def start(self, env): return self
		def stop(self, env): return self
		def timerExpired(self, env): return self

	class Stopped(State):
		def start(self, env):
			return StatePattern.Running()

	class Running(State):
		def stop(self, env):
			env.timer = env.time + 2
			return StatePattern.Stopping()

	class Stopping(State):
		def timerExpired(self, env):
			return StatePattern.Stopped()

	def __init__(self):
		self.state = StatePattern.Stopped()
		self.time = 0
		self.timer = None

	def start(self):
		self.state = self.state.start(self)

	def stop(self):
		self.state = self.state.stop(self)

	def tick(self):
		if (self.timer != None) and (self.time >= self.timer):
			self.state = self.state.timerExpired(self)
		self.time += 1

	def get_state(self):
		return str(self.state.__class__.__name__)

for sm in [Switch(), StatePattern()]:
	print(sm.get_state())
	sm.stop()
	print(sm.get_state())
	sm.start()
	print(sm.get_state())
	sm.stop()
	print(sm.get_state())
	sm.tick()
	print(sm.get_state())
	sm.tick()
	print(sm.get_state())
	sm.tick()
	print(sm.get_state())
