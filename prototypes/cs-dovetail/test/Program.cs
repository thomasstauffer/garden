﻿using System.IO;
using System.Text;
using Xunit;
using System.Linq;
using System.Net.Http;
using System.Collections.Generic;

namespace test
{
    public class Test
    {
        [Fact]
        public void TestTrue()
        {
            Assert.True(1 == 1);
            //Assert.True(1 == 2);
        }

        private static string Get(string url)
        {
            var client = new HttpClient();
            var response = client.GetAsync(url).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            return content;
        }

        private static string PostString(string url, string data)
        {
            var client = new HttpClient();
            var content = new StringContent(data);
            var response = client.PostAsync(url, content).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            return result;
        }

        private static string PostForm(string url, params (string, string)[] data)
        {
            var client = new HttpClient();
            var content = new FormUrlEncodedContent(
                data.Select(item => new KeyValuePair<string, string>(item.Item1, item.Item2)).ToArray()
            );
            var response = client.PostAsync(url, content).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            return result;
        }

        private static string PostFile(string url, string name, string fileName, string data)
        {
            var client = new HttpClient();
            string boundary = "AnythingThatIsNotWithinData";
            var content = new MultipartFormDataContent(boundary);
            content.Add(new StreamContent(new MemoryStream(Encoding.UTF8.GetBytes(data))), name, fileName);
            var response = client.PostAsync(url, content).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            return result;
        }

        private string PTSV_HOST = "http://ptsv2.com/t/ohcnb-1616323849";
        private string PTSV_REPLY = "Thank you for this dump. I hope you have a lovely day!";

        [Fact]
        public void TestPTSVGet()
        {
            Assert.StartsWith("<!DOCTYPE html>", Get(PTSV_HOST));
        }

        [Fact]
        public void TestPTSVPostString()
        {
            Assert.Equal(PTSV_REPLY, PostString(PTSV_HOST + "/post", "Hello World"));
        }

        [Fact]
        public void TestPTSVPostForm()
        {
            Assert.Equal(PTSV_REPLY, PostForm(PTSV_HOST + "/post", ("h", "Hello"), ("w", "World")));
        }

        [Fact]
        public void TestPTSVPostFile()
        {
            Assert.Equal(PTSV_REPLY, PostFile(PTSV_HOST + "/post", "hw", "hw.txt", "Hello World"));
        }

        // private string LOCAL_HOST = "http://127.0.0.1:5000";
    }
}
