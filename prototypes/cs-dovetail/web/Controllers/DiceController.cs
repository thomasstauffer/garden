using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace web
{
    public class DiceController : Controller
    {
        private readonly DiceDbContext _context;

        public DiceController(DiceDbContext context)
        {
            DiceDbContext.Init(context);
            _context = context;
        }

        private static int D6()
        {
            var random = new Random();
            return (random.Next() % 6) + 1;
        }

        // curl -d DiceModelID=1 http://127.0.0.1:5000/api/Dice/DiceEventCreateRandom
        [HttpPost]
        public int DiceEventCreateRandom(int diceModelID)
        {
            var result = D6();
            _context.DiceEvents.Add(new DiceEvent { DiceModelID = diceModelID, Result = result });
            _context.SaveChanges();
            return result;
        }

        // curl -d DiceModelID=1 http://127.0.0.1:5000/api/Dice/DiveEvent
        [HttpGet]
        public JsonResult DiceEvent()
        {
            return new JsonResult(_context.DiceEvents.ToArray());
        }
    }
}
