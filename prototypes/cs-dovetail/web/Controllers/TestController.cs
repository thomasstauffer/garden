using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace web
{
    public class TestController : Controller
    {

        // curl -v -d x="Hello" http://127.0.0.1:5000/api/Test/Triple
        // > Content-Type: application/x-www-form-urlencoded
        // < Content-Type: text/plain; charset=utf-8
        [HttpPost]
        public string Triple(string x)
        {
            return x + x + x;
        }

        public class ABC
        {
            public string A { get; set; }
            public string B { get; set; }
            public string C { get; set; }
        }

        // curl -v http://127.0.0.1:5000/api/Test/ABCMake
        // < Content-Type: application/json; charset=utf-8
        [HttpGet]
        public JsonResult ABCMake(string x)
        {
            ABC abc = new ABC { A = "Hello A", B = "Hello B", C = "Hello C" };
            return new JsonResult(abc);
        }

        // curl -v http://127.0.0.1:5000/api/Test/ABCMakeDictionary
        // < Content-Type: application/json; charset=utf-8
        [HttpGet]
        public JsonResult ABCMakeDictionary(string x)
        {
            var abc = new Dictionary<string, string>() {
                { "A", "Hello A" },
                { "B", "Hello B" },
                { "C", "Hello C" },
            };
            return new JsonResult(abc);
        }

        // curl -v -d A="HelloA" -d B="HelloB" -d C="HelloC" http://127.0.0.1:5000/api/Test/ABCMerge
        // > Content-Type: application/x-www-form-urlencoded
        // < Content-Type: text/plain; charset=utf-8
        [HttpPost]
        public string ABCMerge(ABC abc)
        {
            return abc.A + abc.B + abc.C;
        }

        // curl -v -d A="HelloA" -d B="HelloB" -d C="HelloC" http://127.0.0.1:5000/api/Test/ABCMergeDictionary
        // > Content-Type: application/x-www-form-urlencoded
        // < Content-Type: text/plain; charset=utf-8
        [HttpPost]
        public string ABCMergeDictionary(Dictionary<string, string> abc)
        {
            return abc["A"] + abc["B"] + abc["C"];
        }

        // curl -v --header "Content-Type: application/json" --data '{"A":"HelloA","B":"HelloB","C":"HelloC"}' http://127.0.0.1:5000/api/Test/ABCMergeJSON
        // > Content-Type: application/json
        // < Content-Type: text/plain; charset=utf-8
        [HttpPost]
        public string ABCMergeJSON([FromBody] ABC abc)
        {
            return abc.A + abc.B + abc.C;
        }

        // curl -v -F "file=@test.csv" http://127.0.0.1:5000/api/Test/File
        // > Content-Type: multipart/form-data; boundary=...
        // < Content-Type: text/plain; charset=utf-8
        [HttpPost]
        public async Task<string> File(IFormFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                return Encoding.ASCII.GetString(memoryStream.ToArray());
            }
        }
    }
}