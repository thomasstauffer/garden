using Microsoft.EntityFrameworkCore;

namespace web
{
    public class DiceModel
    {
        public int DiceModelID { get; set; }
        public string Name { get; set; }
    }

    public class DiceEvent
    {
        public int DiceEventID { get; set; }
        public int DiceModelID { get; set; }
        public int Result { get; set; }
    }

    public class DiceDbContext : DbContext
    {
        // sqlite3 test.db "SELECT * FROM DiceModels"
        // sqlite3 test.db "SELECT * FROM DiceEvents"        
        public static void Init(DiceDbContext context)
        {
            if (context.Database.EnsureCreated())
            {
                var diceModel = new DiceModel { Name = "D6" };
                context.DiceModels.Add(diceModel);
                context.SaveChanges();
                context.DiceEvents.Add(new DiceEvent { DiceModelID = diceModel.DiceModelID, Result = 6 });
                context.SaveChanges();
            }
        }

        public DiceDbContext(DbContextOptions<DiceDbContext> options) : base(options)
        {
        }

        public DbSet<DiceModel> DiceModels { get; set; }
        public DbSet<DiceEvent> DiceEvents { get; set; }
    }
}