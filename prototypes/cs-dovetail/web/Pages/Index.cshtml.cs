﻿using System;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace web.Pages
{
    public class IndexModel : PageModel
    {
        public string ServerTime { get; set; }

        public IndexModel()
        {
        }

        public void OnGet()
        {
            ServerTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
    }
}
