
#include <functional>
#include <iostream>

template <typename T, typename E>
class result {
private:
	T _value;
	E _error;
	bool _is_value;

public:
	result(const T& value) : _value(value), _is_value(true) {
	}

	result(const E& error) : _error(error), _is_value(false) {
	}

	bool has_value() const {
		return _is_value;
	}

	bool has_error() const {
		return not _is_value;
	}

	T& value() {
		return _value;
	}

	const T& value() const {
		return _value;
	}

	const T& value_or(const T& default_value) {
		if (_is_value) {
			return _value;
		} else {
			return default_value;
		}
	}

	E& error() {
		return _error;
	}

	const E& error() const {
		return _error;
	}

	template <typename U>
	result<U, E> map() {
		if (_is_value) {
			return result<U, E>(static_cast<U>(_value));
		} else {
			return result<U, E>(_error);
		}
	}

	template <typename U>
	result<U, E> map(std::function<U(T)> function) {
		if (_is_value) {
			return result<U, E>(function(_value));
		} else {
			return result<U, E>(_error);
		}
	}
};

enum class error {
	a = 1000,
	b = 2000,
	c = 3000,
};

result<int, error> g(bool p2) {
	if (p2) {
		return error::b;
	} else {
		return 31415;
	}
}

result<float, error> f(bool p1, bool p2) {
	if (p1) {
		return error::a;
	} else {
		return g(p2).map<float>([](int value) { return value / 1.0e4f; });
	}
}

void p(const result<float, error>& result) {
	if (result.has_value()) {
		std::cout << "value " << result.value() << std::endl;
	} else {
		std::cout << "error " << static_cast<uint32_t>(result.error()) << std::endl;
	}
}

int main() {
	std::cout << "cpp-result" << std::endl;

	p(f(true, true));
	p(f(true, false));
	p(f(false, true));
	p(f(false, false));

	p(result<float, error>(error::c));
	std::cout << "value " << (result<float, error>(error::c).value_or(2.7182818f)) << std::endl;

	return 0;
}
