/*

$ gdb main
(gdb) disassemble main

fib

...
callq	_Z7measurev
...
callq	_Z3fibl
...
callq	_Z7measurev
...

calc

...
callq	_Z7measurev
callq	_Z7measurev
...

...
callq	_Z7measurev
leaq	(%rbx,%rbx), %rbx
callq	_Z7measurev
...

This example shows that the c compiler moves around code surrounded by atomic
operations if there is no observable effect.

https://stackoverflow.com/questions/37786547/enforcing-statement-order-in-c
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0342r0.html
*/

#include <ctime>
#include <iostream>
#include <atomic>
#include <mutex>

int64_t fib(int64_t x) {
	if (x == 0) {
		return 0;
	}

	if (x == 1) {
		return 1;
	}

	return fib(x - 1) + fib(x - 2);
}

int64_t calc(int64_t x) {
	return 2 * x;
}

std::atomic<int64_t> g_atomic;
std::mutex g_mutex;

__attribute__((noinline)) void measure() {
	std::cout << "time " << std::clock() << "\n";
}

int main() {

	int64_t in = std::clock() % 1234;

	int64_t result;

	measure();

	{
		//std::lock_guard<std::mutex> guard(g_mutex);
		g_atomic = 0;
		result = calc(in);
		g_atomic = 1;
	}

	measure();

	std::cout << "result " << result << "\n";

	return 0;
}
