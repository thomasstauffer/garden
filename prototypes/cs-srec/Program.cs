﻿
using System.Text;
using System.Diagnostics;

byte[] FromHex(string hex)
{
    byte[] raw = new byte[hex.Length / 2];
    for (int i = 0; i < raw.Length; i++)
    {
        raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
    }
    return raw;
}

string ToHex(byte[] bytes)
{
    var s = new StringBuilder();
    foreach (var t in bytes)
    {
        s.Append(t.ToString("X2"));
    }
    return s.ToString();
}

byte Check(byte[] bytes, int start, int end)
{
    int sum = 0;
    for (int i = start; i < end; i += 1)
    {
        sum += bytes[i];
    }
    return (byte)(255 - (sum & 0xFF));
}

//const int maxLineLength = 50 - 4 - 1 - 1 - 1;
const int maxLineLength = (256 / 2) - 4 - 1 - 1 - 1;
//const int maxLineLength = 16;

void LineOut(int address, List<byte> heapBytes)
{
    while (heapBytes.Count > 0)
    {
        var bytes = heapBytes.Take(maxLineLength);
        var outBytes = new List<byte>();
        outBytes.Add((byte)(bytes.Count() + 4 + 1));
        outBytes.Add((byte)((address >> 24) & 0xFF));
        outBytes.Add((byte)((address >> 16) & 0xFF));
        outBytes.Add((byte)((address >> 8) & 0xFF));
        outBytes.Add((byte)((address >> 0) & 0xFF));
        outBytes.AddRange(bytes);
        outBytes.Add(Check(outBytes.ToArray(), 0, outBytes.Count));
        Console.WriteLine("S3{0}", ToHex(outBytes.ToArray()));
        address += bytes.Count();
        heapBytes.RemoveRange(0, bytes.Count());
    }
}

// S3151A0080107B3B001B7D3B001B7F3B001BC61CFE4D9A

string[] linesIn = System.IO.File.ReadAllLines(@"ApplicationMainBoard.srec");
var linesOut = new List<string>();

int heapStartAddress = 0;
int heapAddress = 0;
var heapBytes = new List<byte>();

foreach (var line in linesIn)
{
    if (line.StartsWith("S3"))
    {
        var inBytes = FromHex(line.Substring(2));
        Debug.Assert(Check(inBytes, 0, inBytes.Length - 1) == inBytes[inBytes.Length - 1]);
        int inLength = inBytes[0];
        int inByteCount = inLength - 4 - 1;
        int inAddress = (inBytes[1] << 24) | (inBytes[2] << 16) | (inBytes[3] << 8) | (inBytes[4] << 0);

        if (heapAddress == 0)
        {
            heapAddress = inAddress;
            heapStartAddress = inAddress;
        }

        if (inAddress != heapAddress)
        {
            LineOut(heapStartAddress, heapBytes);
            heapStartAddress = inAddress;
        }

        heapAddress = inAddress + inByteCount;
        heapBytes.AddRange(inBytes.Skip(5).Take(inByteCount));
    }
    else
    {
        LineOut(heapStartAddress, heapBytes);
        Console.WriteLine(line);
        linesOut.Append(line);
    }
}
