
console.log('js-kenken');

// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
function shuffle(array) {
    const result = array.slice();
    for (let i = result.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const tmp = result[i];
        result[i] = result[j];
        result[j] = tmp;
    }
    return result;
}

function arrayMake(size) {
    return new Array(size).fill(null);
}

function gridCopy(grid) {
    return grid.map(row => row.slice());
}

function gridMake(size) {
    return arrayMake(size).map($ => arrayMake(size));
}

function gridMakeBoard(size) {
    const grid = gridMake(size).map((row, rowIndex) => row.map((_, colIndex) => ((colIndex + rowIndex) % size) + 1))
    return gridTranspose(shuffle(gridTranspose(shuffle(grid))));
}

function gridMap(grid, callable) {
    return grid.map((row, rowIndex) => row.map((cell, colIndex) => callable(cell, rowIndex, colIndex)));
}

function gridShow(grid) {
    grid.map(row => console.log(row));
}

function gridTranspose(grid) {
    const result = gridCopy(grid);
    for (let rowIndex = 0; rowIndex < grid.length; rowIndex += 1) {
        for (let colIndex = 0; colIndex < rowIndex; colIndex += 1) {
            result[rowIndex][colIndex] = grid[colIndex][rowIndex];
            result[colIndex][rowIndex] = grid[rowIndex][colIndex];
        }
    }
    return result;
}

const a = gridMakeBoard(5);
gridShow(a);
