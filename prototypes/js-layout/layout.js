
function juiToLayout(jui) {
	const widget = new JUIWidget();
	widget.layout = jui.layout;
	widget.text = jui.text;
	widget.children = (jui.children ?? []).map(child => juiToLayout(child));
	return widget;
}

function juiToDom(jui) {
	const node = document.createElement('div');
	node.innerText = jui.text ?? '';
	const layout = jui.layout ?? {};
	const styleWidth = layout.width ? `width: ${layout.width}px;` : '';
	const styleHeight = layout.height ? `height: ${layout.height}px;` : '';
	const styleDisplay = layout.algorithm === 'flex' ? 'display: flex;' : ''; // overflow: hidden;
	const styleFlexDirection = layout.direction ? `flex-direction: ${layout.direction};` : '';
	//const styleFlexGrow = layout.grow ? `flex-grow: ${layout.grow};` : '';
	const style = [styleWidth, styleHeight, styleDisplay, styleFlexDirection].join('');
	node.setAttribute('style', 'border: 1px solid #00f;' + style);
	(jui.children ?? []).forEach(child => node.appendChild(juiToDom(child)));
	return node;
}

/*
Is there a package for a browser (not node) somewhere? Everything seems outdated.

https://yogalayout.com/playground

https://www.npmjs.com/package/yoga-layout
https://github.com/facebook/yoga

https://www.npmjs.com/package/yoga-js
https://github.com/vincentriemer/yoga-js
*/
/*
function juiToYoga() {
	return null;
}
*/

class Painter {
	constructor(ctx, size) {
		this.ctx = ctx;
		this.size = size;
	}

	clear() {
		this.ctx.clearRect(0, 0, this.size.width, this.size.height);
	}

	rect(x, y, width, height, strokeColor, fillColor) {
		const offset = 0.5; // TODO describe canvas pixel center
		this.ctx.fillStyle = fillColor;
		this.ctx.fillRect(x + offset, y + offset, width, height);
		this.ctx.strokeStyle = strokeColor;
		this.ctx.strokeRect(x + offset, y + offset, width, height);
	}

	text(x, y, width, height, text, strokeColor) {
		// TODO clip to rect
		const metric = this.measureText(text);
		const rectX = x + (width / 2);
		const rectY = y + (height / 2);
		this.ctx.fillStyle = strokeColor;
		this.ctx.fillText(text, rectX - metric.centerX, rectY + metric.centerY);
	}

	measureText(text) {
		const metric = this.ctx.measureText(text);
		const width = metric.width;
		const height = metric.actualBoundingBoxAscent + metric.actualBoundingBoxDescent;
		return { width: width, height: height, centerX: width / 2, centerY: height / 2 };
	}
}

function merge(rectA, rectB) {
	const [widthLeft, widthRight] = rectA.x < rectB.x ? [rectA, rectB] : [rectB, rectA];

	const x = widthRight.x;
	const width = Math.min((widthLeft.x + widthLeft.width) - widthRight.x, widthRight.width);

	const [heightTop, heightBottom] = rectA.y < rectB.y ? [rectA, rectB] : [rectB, rectA];

	const y = heightBottom.y;
	const height = Math.min((heightTop.y + heightTop.height) - heightBottom.y, heightBottom.height);

	if ((width < 0) || (height < 0)) {
		return { x: 0, y: 0, width: 0, height: 0 };
	} else {
		return { x: x, y: y, width: width, height: height };
	}
}

function testMerge() {
	const a = { x: 100, y: 100, width: 200, height: 100 };
	const b = { x: 200, y: 150, width: 200, height: 100 };
	const c = { x: 400, y: 150, width: 200, height: 100 };

	const x = { x: 200, y: 150, width: 100, height: 50 };
	const y = { x: 0, y: 0, width: 0, height: 0 };

	function req(a, b) {
		return (a.x === b.x) && (a.y === b.y) && (a.width === b.width) && (a.height === b.height);
	}

	console.assert(req(merge(a, b), x));
	console.assert(req(merge(b, a), x));
	console.assert(req(merge(a, c), y));
}

class JUIWidget {
	constructor() {
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#00f', '#ccf');
		if (this.text) {
			painter.text(this.x, this.y, this.width, this.height, this.text, '#000');
		}
	}
}

class Label {
	constructor(text) {
		this.children = [];
		this.text = text;
		this.layout = {
			width: 100,
			height: 30, // TODO calculate based on font and text
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#f00', '#fcc');
		painter.text(this.x, this.y, this.width, this.height, this.text, '#000');
	}
}

class Button {
	constructor(text) {
		this.children = [];
		this.text = text;
		this.layout = {
			width: 150,
			//height: 30, // TODO calculate based on font and text
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#0f0', '#cfc');
		painter.text(this.x, this.y, this.width, this.height, this.text, '#000');
	}
}

class StackH {
	constructor(children) {
		this.children = children;
		this.layout = {
			algorithm: 'flex',
			gap: 50,
			direction: 'row',
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#000', '#ccc');
	}
}

class StackV {
	constructor(children) {
		this.children = children;
		this.layout = {
			algorithm: 'flex',
			gap: 50,
			direction: 'column',
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#000', '#ccc');
	}
}

class MathText {
	constructor(text) {
		this.text = text;
		this.children = [];
		this.layout = {
			width: text.length * 10,
			height: 30,
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#00f', '#ccf');
		painter.text(this.x, this.y, this.width, this.height, this.text, '#000');
	}
}

class MathDivision {
	constructor(children) {
		this.children = children;
		this.layout = {
			algorithm: 'flex',
			gap: 10,
			direction: 'column',
		};
	}

	paint(painter) {
		painter.rect(this.x, this.y, this.width, this.height, '#0f0', '#cfc');
		// TODO paint division inside gap
	}
}

function isNumber(value) {
	return (typeof value === 'number') && isFinite(value);
}

function layoutReset(widget) {
	if (isNumber(widget.layout.width)) {
		widget.width = widget.layout.width;
	} else {
		widget.width = undefined;
	}
	if (isNumber(widget.layout.height)) {
		widget.height = widget.layout.height;
	} else {
		widget.height = undefined;
	}
	widget.x = 0;
	widget.y = 0;

	widget.children.forEach(child => layoutReset(child));
}

function layoutStep1(widget) {
	if (widget.children.length > 0) {
		console.assert(widget.layout.algorithm === 'flex');
		if (widget.layout.direction === 'row') {
			widget.children.forEach(child => layoutStep1(child));
			const max = Math.max(...[0].concat(widget.children.map(child => child.height).filter($ => isNumber($))));
			if (isNumber(widget.layout.height)) {
				widget.height = widget.layout.height;
			} else {
				widget.height = max;
			}
			widget.children.forEach(child => {
				if (isNumber(child.layout.height)) {
					child.height = child.layout.height;
				} else if (isNumber(widget.height)) {
					child.height = widget.height;
				}
			});
			const sum = widget.children.reduce((acc, child) => {
				console.assert(isNumber(child.width));
				return acc + child.width;
			}, 0);
			if (isNumber(widget.layout.width)) {
				widget.width = widget.layout.width;
			} else {
				widget.width = sum;
			}
		} else if (widget.layout.direction === 'column') {
			widget.children.forEach(child => layoutStep1(child));
			const max = Math.max(...[0].concat(widget.children.map(child => child.width).filter($ => isNumber($))));
			if (isNumber(widget.layout.width)) {
				widget.width = widget.layout.width;
			} else {
				widget.width = max;
			}
			widget.children.forEach(child => {
				if (isNumber(child.layout.width)) {
					child.width = child.layout.width;
				} else if (isNumber(widget.width)) {
					child.width = widget.width;
				}
			});
			const sum = widget.children.reduce((acc, child) => {
				console.assert(isNumber(child.height));
				return acc + child.height;
			}, 0);
			if (isNumber(widget.layout.height)) {
				widget.height = widget.layout.height;
			} else {
				widget.height = sum;
			}
		} else {
			console.assert(false);
		}
	}
}

function layoutPosition(widget) {
	if (widget.children.length > 0) {
		console.assert(widget.layout.algorithm === 'flex');
		let x = widget.x;
		let y = widget.y;
		widget.children.forEach(child => {
			child.x = x;
			child.y = y;
			layoutPosition(child);
			// NOTE from perf perspective move this out of the loop?
			if (widget.layout.direction === 'row') {
				x += child.width;
			} else if (widget.layout.direction === 'column') {
				y += child.height;
			} else {
				console.assert(false);
			}
		});
	}
}

function layout(root) {
	console.log('--------');
	layoutReset(root);
	layoutStep1(root);
	layoutPosition(root);
	info(root);
}

function paint(root, painter) {
	painter.clear();
	const clip = false;

	function r(widget) {
		return { x: widget.x, y: widget.y, width: widget.width, height: widget.height };
	}

	function p(widget, region) {
		const cr = merge(region, r(widget));
		painter.ctx.save();
		if (clip) {
			const offset = 0.5;
			painter.ctx.beginPath();
			painter.ctx.rect(cr.x+offset, cr.y+offset, cr.width+offset, cr.height+offset);
			painter.ctx.clip();
		}
		widget.paint(painter);
		painter.ctx.restore();

		widget.children.forEach(child => p(child, cr));
	}

	p(root, r(root));
}

function info(widget) {
	console.log(`${widget.constructor.name} x:${widget.x} y:${widget.y} w:${widget.width} h:${widget.height}`);
	widget.children.forEach(child => info(child));
}

function tryIt(callable, def = undefined) {
	try {
		return callable();
	} catch {
		return def;
	}
}

function setupUi(root, painter) {
	function allWidgets(widget) {
		return [widget].concat(...widget.children.map(child => allWidgets(child)));
	}

	const widgets = allWidgets(root);
	const widgetNode = document.getElementById('widget');
	const layoutNode = document.getElementById('layout');
	let selectedIndex = 0;

	layoutNode.addEventListener('input', () => {
		const data = tryIt(() => JSON.parse(layoutNode.value));
		if (data !== undefined) {
			widgets[selectedIndex].layout = data;
			layout(root);
			paint(root, painter);
		}
	});

	widgetNode.addEventListener('change', () => {
		const index = widgetNode.selectedIndex;
		selectedIndex = index;
		layoutNode.value = JSON.stringify(widgets[selectedIndex].layout);
	});

	widgets.forEach((widget, index) => {
		const nodeOption = document.createElement('option');
		nodeOption.innerText = widget.constructor.name;
		nodeOption.dataset.index = index;
		widgetNode.appendChild(nodeOption);
	});


	layout(root);
	paint(root, painter);
}

function testH() {
	const button1 = new Button('Button 1');
	const button2 = new Button('Button 2');
	const label1 = new Label('Label 1');
	label1.layout.height = 500;
	const label2 = new Label('Label 2');
	label2.layout.height = 100;
	const label3 = new Label('Label 3');
	label3.layout.height = undefined;
	const label4 = new Label('Label 4');
	const label5 = new Label('Label 5');
	const stack1 = new StackH([label4, label5]);
	const stack2 = new StackH([label2, label3, stack1]);
	const stack3 = new StackH([button1, label1, stack2, button2]);

	stack3.layout.width = 900;
	stack3.layout.height = 400;

	return stack3;
}

function testV() {
	const label1 = new Label('Label 1');
	label1.layout.width = undefined;
	const label2 = new Label('Label 2');
	const label3 = new Label('Label 3');
	const button1 = new Button('Button 1');
	button1.layout.width = undefined;
	button1.layout.height = 40;
	const stack1 = new StackV([label1, label2, button1, label3]);

	stack1.layout.width = 300;
	stack1.layout.height = 600;

	return stack1;
}

function testEditor() {
	const menuLabel1 = new Label('ML 1');
	const menuLabel2 = new Label('ML 2');
	const menuLabel3 = new Label('ML 3');
	const menu = new StackH([menuLabel1, menuLabel2, menuLabel3]);

	const workspace = new Label('Workspace');
	workspace.layout.width = undefined;
	workspace.layout.height = 200; // TODO should be grow

	const commandLabel1 = new Label('CL 1');
	const commandLabel2 = new Label('CL 2');
	const commandLabel3 = new Label('CL 3');
	const command = new StackH([commandLabel1, commandLabel2, commandLabel3]);

	const statusLabel1 = new Label('SL 1');
	const statusLabel2 = new Label('SL 2');
	const statusLabel3 = new Label('SL 3');
	const status = new StackH([statusLabel1, statusLabel2, statusLabel3]);

	const editor = new StackV([menu, workspace, command, status]);
	editor.layout.width = 900;
	editor.layout.height = 600;

	return editor;
}

function testMath() {
	const mtab = new MathText('aaa+bbb');
	const mtcd = new MathText('c+d');
	const mdiv1 = new MathDivision([mtab, mtcd]);
	const mtef = new MathText('e+f');
	const mtgh = new MathText('g+h');
	const mdiv2 = new MathDivision([mtef, mtgh]);
	const mdiv3 = new MathDivision([mdiv1, mdiv2]);
	return mdiv3;
}

export function test() {
	const canvas = document.getElementById('output');
	const ctx = canvas.getContext('2d');
	ctx.font = '14px Ubuntu'; // TODO should be specified by the widget

	const painter = new Painter(ctx, { width: canvas.width, height: canvas.height });

	testMerge();

	const jui = {
		layout: { algorithm: 'flex', direction: 'row' },
		children: [
			{ text: 'Label 1', layout: { width: 100, height: 30 } },
			{ text: 'Label 2', layout: { width: 100 } },
		]
	};

	/*
	const jui = {
		layout: { algorithm: 'flex', direction: 'row', width: 900, height: 400 }, children: [
			{ text: 'Button 1', layout: { width: 150 } },
			{ text: 'Label 1', layout: { width: 100, height: 500 } },
			{
				layout: { algorithm: 'flex', direction: 'row' }, children: [
					{ text: 'Label 2', layout: { width: 100, height: 100 } },
					{ text: 'Label 3', layout: { width: 100, height: 50 } },
					{
						layout: { algorithm: 'flex', direction: 'row' }, children: [
							{ text: 'Label 4', layout: { width: 100, height: 50 } },
							{ text: 'Label 5', layout: { width: 100, height: 50 } },
						]
					},
				]
			},
			{ text: 'Button 2', layout: { width: 150 } },
		]
	};
	*/

	const juiNode = document.getElementById('jui');
	juiNode.innerHTML = '';
	juiNode.appendChild(juiToDom(jui));


	const root = testH();
	//const root = juiToLayout(jui);

	setupUi(root, painter);


}
