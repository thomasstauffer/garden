
= Thoughts

Take the idea from flexbox as basic idea. Maybe later add grid. Both will be known to more and more developers.

== Width/Height

itself: can be calculated by itself, probably depending on other properties of the element, but without any dependency to other elements
font-height: depends on the currently selected font of the element
text-width: depends on the current text of the element
fixed: fixed size, no calculations needed
calculated: ...

children: size can only be calculated if size of children is known

parent: size depends on the parent
expand: is as large as possible, but may depend on how much the parent allows (-> parent)


parent/children conflict example? StackH mit button+button oder button+label+button
in general -> if the innermost widget is expanding, nothing can be calculated, until a size is known from the outside

children/parent conflict?

== Auto Scrollbar

May come after there is not enough size and increase the widget size, which means a full relayout may be needed. There may be a worst case scenario where several scrollbar toggle each other which would result in a cycle.

== Minimum Size

Every widget may have an additional "MinimumSize" property. This may be e.g. relevant if the Grow property is set, because without a "MiniumSize" the widget may shrink to zero.

== 2 x Grow

(StackH (Label StackH StackH Label))

If both inner StackH have the grow flag, both should given the free space available.

== Qt Designer

Instead of having row and column which describes what "grow" is doing, Qt has an individual "grow" property for width and height.

== Math

Font size is reduced the more nested a part of the whole formula is. This has to be done before the layout is started. It is not a part of the layout engine itself.

== Other Layout Engines

2021-10-01

- stretch
-- written in rust
-- newer than yoga
-- not update since a year
- yoga
-- yoga-layout-static is quite simple to use with Node
-- no package found which works in a browser
- cassowarry
- browser flexbox
-- https://github.com/philipwalton/flexbugs
-- maybe a hint? https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#Layout

== Ambigous Examples

(StackH (Button))

Button grows. StackH cannot ask the Button to calculate its own size.

(StackH (Button Label Button))

As before. Label can be calculated but the width of all children can still not be calculated.

(StackH (Label Label))

Width of StackH is only known after all Labels are summed up.

= Widgets

fixed = can be set before the layout algorithm starts
auto = calculated withint the layout algorithm
grow = widgets can fill available space

== Label

width: fixed
height: fixed

== Button

width: auto
height: fixed

== Editor Line

width: fixed / auto / grow
height: fixed

== Image

width: fixed
height: fixed

== ImageH

Proportionally Scaled

width: grow
height: ???

Aspect ratio property necessary?

== StackH

width: auto
height: auto

== StackV

width: auto
height: auto

== Stack

-> StackH | StackV

== Checkbox

-> StackH(Image, Label)

== Radio

-> StackH(Image, Label)

== Border

width: auto
height: auto

== ScrollbarH

width: auto
height: fixed

== ScrollbarV

width: width
height: auto

== Area

width: grow
height: grow

The Area offers a bigger area for its children. But this new area should not be infinite, or components which expand would be infinitly large.

== Grid

width: grow
height: grow

== List

-> StackH(ScrollableArea, ScrollbarV)

== Text Editor

-> Area

== Image Editor

-> Area

== MathText

width: fixed
height: fixed

== MathDivision

width: auto
height: auto

== MathMatrix

width: auto
height: auto

== MathBracket

width: auto
height: auto

== MathIntegrateOrSum

width: auto
height: auto
