
; https://clojuredocs.org/
; https://www.tutorialspoint.com/clojure/index.htm

(println "string")
(println (clojure.string/replace "Hello World" #"Hello" "Our"))
;;(println (replace "Hello World" #"Hello" "Our"))
(println (str "Hello" "World"))
(println (format "Hello %d World" 123))
(def split clojure.string/split)
(println (split "Hello World" #" "))

(println "list")
(def mylist (list 1 2 4 8))
(println (list 1 2 4 8))
(println (count mylist))
(println (nth mylist 2))

(println "vector")
(println (vector 1 2 4 8))
(def myvec [1 2 4 8])
(println myvec)
(println (get myvec 2))
(println (myvec 2))
(doseq [v myvec] (print v))
(println)

(println "map")
(println (hash-map "a" "1" "b" "2" "c" "3"))
(def mymap {"a" 1 "b" 2 "c" 3})
(println mymap)
(println (keys mymap))
(println (vals mymap))
(println (get mymap "b"))
(println (mymap "b"))

(println *command-line-args*)
(doseq [arg *command-line-args*] (printf "[%s]" arg))
(println)

(println (+ 1/3 1/4))
(println (+ (/ 1 3) (/ 1 4)))

(println (bigint 666))
(println 666N)
(println (* 666N 666 666 666 666 666 666 666 666 666 666 666))

; (println (clojure.math.numeric-tower/expt 2 100))
; (println (math/expt 2 100))
; (require 'clojure.math.numeric-tower)
; (println (clojure.math.numeric-tower/expt 2 100))
