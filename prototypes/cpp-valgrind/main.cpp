
#include <mutex>
#include <thread>
#include <atomic>
#include <iostream>

int32_t g_int_unsafe = 0;
std::atomic<int32_t> g_int_atomic;
int32_t g_int_mutex = 0;

std::mutex g_mutex;

void f() {
	for(uint32_t i = 0; i < 10'000; i += 1) {
		g_int_unsafe++;
		g_int_atomic++;
		g_mutex.lock();
		g_int_mutex++;
		g_mutex.unlock();
	}
}

int main() {
	std::thread t1(f);
	std::thread t2(f);
	
	t1.join();
	t2.join();
	
	std::cout << g_int_unsafe << std::endl;
	std::cout << g_int_atomic << std::endl;
	std::cout << g_int_mutex << std::endl;
	
	return 0;
}
