
actor Main
	let rm: ResourceManager
	let alpha: Alpha
	let beta: Beta

	new create(env: Env) =>
		env.out.print("Main Start")
		rm = ResourceManager(env)
		alpha = Alpha(env, rm)
		beta = Beta(env, rm)
		env.out.print("Main End")

actor Alpha
	new create(env: Env, rm: ResourceManager) =>
		env.out.print("Alpha Created")

actor Beta
	new create(env: Env, rm: ResourceManager) =>
		env.out.print("Beta Created")


actor ResourceManager
	let printer: Printer
	let scanner: Scanner

	new create(env: Env) =>
		printer = Printer
		scanner = Scanner
		env.out.print("ResourceManager Created")

class Printer
	let _name: String

	new create() =>
		_name = "Printer"

class Scanner
	let _name: String

	new create() =>
		_name = "Scanner"
