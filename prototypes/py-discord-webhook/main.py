
import sys
import os
import discord # pip3 install discord

WEBHOOK_ID = os.environ['WEBHOOK_ID'] 
WEBHOOK_TOKEN = os.environ['WEBHOOK_TOKEN']

if False:
	webhook = discord.Webhook.partial(WEBHOOK_ID, WEBHOOK_TOKEN, adapter=discord.RequestsWebhookAdapter())

	# Send temperature as text
	webhook.send('hasta la vista')

	sys.exit(0)

if False:
	URL = 'https://discordapp.com/api/webhooks/' + WEBHOOK_ID + '/' + WEBHOOK_TOKEN

	# https://discordapp.com/developers/docs/resources/webhook

	from urllib.parse import urlencode
	from urllib.request import Request, urlopen
	import json

	post = {'username': 'T800', 'content': 'We will get you'}

	send = json.dumps(post).encode()

	print(send)

	request = Request(URL, data=send, headers={'Content-Type': 'application/json'})
	response = urlopen(request).read().decode()

	print(response)

if True:
	import requests
	import json

	URL = 'https://discordapp.com/api/webhooks/' + WEBHOOK_ID + '/' + WEBHOOK_TOKEN

	data = {'content': 'hasta la vista'}
	result = requests.post(URL, data=json.dumps(data), headers={"Content-Type": "application/json"})
	print(result)
