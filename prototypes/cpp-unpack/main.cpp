
#include <array>
#include <functional>
#include <iostream>
#include <set>
#include <type_traits>

enum class endianess {
	little,
	big
};

constexpr endianess system_endianess() {
	uint32_t test = 0x01020304;
	return (*reinterpret_cast<uint8_t*>(&test) == 0x04) ? endianess::little : endianess::big;
}

template <typename T>
constexpr T unpack2(const uint8_t* buffer, uint32_t offset, endianess e) {
	T result;
	auto ptr = reinterpret_cast<uint8_t*>(&result);
	const auto s = system_endianess();
	for (uint32_t i = 0; i < sizeof(T); i += 1) {
		auto n = s == e ? i : sizeof(T) - 1 - i;
		ptr[i] = buffer[offset + n];
	}
	return result;
}

void unpack_bits(const uint8_t* buffer, uint8_t start, uint8_t nr, uint32_t& out) {
	uint32_t result = 0;
	for (uint32_t i = 0; i < nr; i++) {
		uint32_t srcByteIndex = (i + start) / 8;
		uint32_t srcBitIndex = (i + start) % 8;
		uint32_t bit = (buffer[srcByteIndex] >> srcBitIndex) & 0b1;
		result |= bit << i;
	}

	out = result;
}

template <typename T>
constexpr void unpack(T& value, uint8_t* buffer, uint32_t offset) {
	value = 0;
	for (uint32_t i = 0; i < sizeof(T); i += 1) {
		value = value << 8;
		value |= buffer[offset + i];
	}
}

struct page1 {
	void unpack(uint8_t* buffer) {
		::unpack(a, buffer, 0);
		::unpack(b, buffer, 4);
		::unpack(c, buffer, 8);
	}

	uint8_t a;
	uint8_t b;
	uint8_t c;
};

struct page2 {
	void unpack(uint8_t* buffer) {
		::unpack(a, buffer, 0);
		::unpack(b, buffer, 4);
		::unpack(c, buffer, 8);
	}

	uint16_t a;
	uint16_t b;
	uint16_t c;
};

int main() {
	std::cout << "BitTwiddler" << std::endl;

	uint8_t buffer[] = {100, 0, 0, 0, 200, 0, 0, 0, 1, 1, 0, 0};

	page1 p1;
	page2 p2;

	p1.unpack(buffer);
	p2.unpack(buffer);

	std::cout << (int)p1.a << std::endl;
	std::cout << (int)p1.b << std::endl;
	std::cout << (int)p1.c << std::endl;

	std::cout << (int)p2.a << std::endl;
	std::cout << (int)p2.b << std::endl;
	std::cout << (int)p2.c << std::endl;

	return 0;
}
