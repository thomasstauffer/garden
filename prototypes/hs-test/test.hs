
-- ghci

twice1 :: (t -> t) -> t -> t
twice1 f x = f (f x)
-- :info twice1
-- twice1 (*2) 3

twice2 :: (t -> t) -> t -> t
twice2 f = f . f
-- :info twice2
-- twice2 (*2) 3

twice3 :: (t -> t) -> t -> t
twice3 f x = f $ f x
-- :info twice3
-- twice3 (*2) 3

(.<) :: (a -> b) -> (b -> c) -> a -> c
(.<) = flip (.)

-- import Control.Arrow

-- ((+1) . (*2)) 1
-- ((.) (+1) (*2)) 1

-- ((+1) .< (*2)) 1
-- ((+1) >>> (*2)) 1
-- ((flip (.)) (+1) (*2)) 1

-- list comprehension

tuple1 = [(i,j) | i <- [1,2], j <- [1..4]]

-- exception

-- read "a"::Int
-- div 10 0

-- add2 :: Float -> Float
-- add2 = (+) 2

-- squares1 :: (Num t, Enum t) => [t]
-- squares1 :: [Integer]
-- :info Num
-- :info Integer
squares1 = map (^2) [1..10]

-- squares2 :: (Num t, Enum t) => [t]
squares2 = [x^2 | x <- [1..10]]

toFloat :: Integral a => a -> Float
toFloat x = fromIntegral x :: Float
-- toFloat 2

data Expr = Val Int | Add Expr Expr

expr1 = Val 1
expradd12 = Add (Val 1) (Val 2)

--ff :: Int -> Float
--ff x = toFloat x
ff x = x + 1
-- ff 1

gg x y = x + y
-- gg 1 2

foldexpr1 :: Expr -> Int
foldexpr1 (Val x) = x
foldexpr1 (Add x y) = (foldexpr1 x) + (foldexpr1 y)
-- foldexpr1 (Val 2)
-- foldexpr1 (Add (Val 2) (Val 3))

foldexpr2 :: Num a => (Int -> a) -> Expr -> a
foldexpr2 f (Val x) = f x
foldexpr2 f (Add x y) = (foldexpr2 f x) + (foldexpr2 f y)
-- foldexpr2 ff (Val 3)
-- foldexpr2 ff (Add (Val 2) (Val 3))

foldexpr3 :: (Int -> a) -> (a -> a -> a) -> Expr -> a
foldexpr3 f g (Val x) = f x
foldexpr3 f g (Add x y) = g (foldexpr3 f g x) (foldexpr3 f g y)
-- foldexpr3 ff gg (Val 3)
-- foldexpr3 ff gg (Add (Val 3) (Val 4))
