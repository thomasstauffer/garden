
class TestSerialPort
{
    private string _name;

    public TestSerialPort(string name)
    {
        _name = name;
    }

    public void Open()
    {
        Console.WriteLine(_name + " Open");
    }

    public void Close()
    {
        Console.WriteLine(_name + " Close");
    }

    public int Available()
    {
        Console.WriteLine(_name + " Available");
        return 123;
    }

    public void Send(string send)
    {
        Console.WriteLine(_name + " Send: " + send);
    }

    public string Receive()
    {
        Console.WriteLine(_name + "Receive");
        return "Hello World";
    }
}

class WebSerialPort
{
    private const string htmlIndex = @"
    <html>
    <body>
    <h1>WebSerialPort</h1>
    </body>
    </html>";

    private static IResult KVs(params Tuple<string, string>[] kv)
    {
        var d = kv.ToDictionary(x => x.Item1, x => x.Item2);
        return Results.Json(d);
    }

    public struct Config
    {
        public int Baudrate { get; set; }
        public string Parity { get; set; }
    }

    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        var app = builder.Build();

        var ports = new Dictionary<string, TestSerialPort>();
        ports.Add("COM1", new TestSerialPort("COM1"));
        ports.Add("COM2", new TestSerialPort("COM2"));
        ports.Add("COM3", new TestSerialPort("COM3"));

        app.MapGet("/", () => Results.Text(htmlIndex, "text/html"));
        app.MapGet("/api/v1/time", () => DateTime.UtcNow.ToString("o", System.Globalization.CultureInfo.InvariantCulture));
        // curl http://localhost:5098/api/v1/port
        app.MapGet("/api/v1/port", () => Results.Json(ports.Keys));
        // curl -X POST  -H 'Content-Type: application/json' -d '{"baudrate":115200,"parity":"none"}' http://localhost:5098/api/v1/port/{name}/open
        app.MapPost("/api/v1/port/{port}/open", async (HttpRequest request) =>
        {
            string port = (request.RouteValues["Port"] ?? "").ToString() ?? "";
            ports[port].Open();
            var config = await request.ReadFromJsonAsync<Config>();
            Console.WriteLine(config.Baudrate.ToString() + " " + config.Parity);
            return Results.Json(port + " open");
        });
        app.MapPost("/api/v1/port/{port}/close", (string port) => { ports[port].Close(); Results.Json(port + "close"); });
        app.MapPost("/api/v1/port/{port}/flush", (string port) => { Results.Json(port + "flush"); });
        app.MapGet("/api/v1/port/{port}/available", (string port) =>
        {
            return Results.Json(ports[port].Available());
        });
        // curl http://localhost:5098/api/v1/port/{name}/receive
        app.MapGet("/api/v1/port/{port}/receive", (string port) => ports[port].Receive());
        // curl -X POST --data "Oo" http://localhost:5098/api/v1/port/{name}/send
        app.MapPost("/api/v1/port/{port}/send", (HttpRequest request) =>
        {
            string port = (request.RouteValues["Port"] ?? "").ToString() ?? "";
            var stream = request.BodyReader.AsStream();
            string body = (new StreamReader(stream)).ReadToEnd();
            ports[port].Send(body);
        });

        app.Run();
    }
}
