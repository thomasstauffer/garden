
#include <array>
#include <functional>
#include <iostream>
#include <map>
#include <set>

/*
does not properly allocate/deallocate, only useful for types without dynamic
allocation, just a short test, far away from being a proper implementation
*/
template <typename T, size_t N>
class set_array {
public:
	explicit set_array() : _count(0) {
	}

	void insert(const T& value) {
		if (contains(value)) {
			return;
		}
		if (_count == N) {
			return; // throw exception, if exceptions are enabled
		}
		_array[_count] = value;
		_count += 1;
		return;
	}

	bool contains(const T& key) {
		for (size_t i = 0; i < _count; i += 1) {
			if (_array[i] == key) {
				return true;
			}
		}
		return false;
	}

	void erase(const T& key) {
		//
	}

	void clear() {
		_count = 0;
	}

	size_t size() const {
		return _count;
	}

private:
	size_t _count;
	std::array<T, N> _array;
};

void assert(bool condition) {
	if (not condition) {
		std::cout << "assert failed" << std::endl;
		std::exit(1);
	}
}

void* operator new(size_t size) {
	void* pointer = std::malloc(size);
	std::cout << "malloc address:" << pointer << " size:" << size << std::endl;
	return pointer;
}

void operator delete(void* pointer) {
	std::cout << "free address:" << pointer << std::endl;
	std::free(pointer);
}

template <class Set>
void test() {
	Set set;

	assert(set.size() == 0);
	set.insert(123);
	assert(set.size() == 1);
	set.insert(123);
	assert(set.size() == 1);
	set.insert(234);
	set.insert(345);
	set.insert(456);
	assert(set.size() == 4);
	set.clear();
	assert(set.size() == 0);
}

int main() {
	std::cout << "set" << std::endl;

	test<std::set<int>>();
	test<set_array<int, 5>>();

	return 0;
}
