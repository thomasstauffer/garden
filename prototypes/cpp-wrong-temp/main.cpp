
#include <iostream>
#include <memory>

template <typename T>
struct stringifier {
public:
	explicit stringifier(const T& element) : _element(element) {}
	virtual ~stringifier() { std::cout << "~stringifier\n"; }
	virtual std::string to_string() {
		return std::to_string(_element);
	}
	const T& _element;
};

int main() {
	std::cout << "Hello" << std::endl;

	auto x(std::make_shared<stringifier<int32_t>>(123)); // while clang++ and vcc return a valid result, it is wrong because 123 is just a temporary?
	auto y(std::make_shared<stringifier<int32_t>>(234));
	std::cout << x->to_string() << std::endl;
	std::cout << y->to_string() << std::endl;
}
