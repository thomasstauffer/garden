
#include <fstream>
#include <functional>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

enum class TokenId {
	TagOpen,
	TagOpenEnd,
	TagOpenEndShort,
	TagClose,
	Attribute,
	Text,
	Comment,
};

enum class Context {
	Normal,
	TagStart,
};

const char* token_2_string(TokenId token_id) {
	const char* table[] = {"TagOpen", "TagOpenEnd", "TagClose", "Attribute", "Text"};
	return table[static_cast<uint32_t>(token_id)];
}

void xml_lexer(const std::string& text, std::function<void(TokenId tokenId, const std::string& key, const std::string& value)> callable) {
	const std::vector<std::tuple<TokenId, std::regex, Context, Context>> tokens = {
		{TokenId::Comment, std::regex("<!--.*?-->"), Context::Normal, Context::Normal},
		{TokenId::TagClose, std::regex("</([^>]+)>"), Context::Normal, Context::Normal},
		{TokenId::TagOpen, std::regex("<[?]?([^ \t\r\n?/>]+)[\t\r\n ]*"), Context::Normal, Context::TagStart},
		{TokenId::TagOpenEndShort, std::regex("[?/]>"), Context::TagStart, Context::Normal},
		{TokenId::TagOpenEnd, std::regex(">"), Context::TagStart, Context::Normal},
		{TokenId::Attribute, std::regex("([^=]+)[\t\r\n ]*=[\t\r\n ]*\"([^\"]*)\"[\t\r\n ]*"), Context::TagStart, Context::TagStart}, {TokenId::Text, std::regex("([^<]*)"), Context::Normal, Context::Normal},
	};

	uint32_t position = 0;
	const uint32_t length = text.length();

	const std::string empty("");
	Context context = Context::Normal;

	std::string lastKey("");

	while (position < length) {
		// std::string t = text.substr(position); // TODO use better regex search with startpos
		uint32_t advance = 0;

		auto it_begin = text.begin();
		std::advance(it_begin, position);
		auto it_end = text.end();

		for (const auto& token : tokens) {
			auto token_id = std::get<0>(token);
			auto token_re = std::get<1>(token);
			auto token_context_now = std::get<2>(token);
			auto token_context_next = std::get<3>(token);

			std::smatch match;
			std::regex_search(it_begin, it_end, match, token_re);

			if ((context == token_context_now) && (match.length() > 0) && (match.position() == 0)) {
				switch (token_id) {
				case TokenId::TagOpen:
					lastKey = match[1];
					callable(token_id, match[1], empty);
					break;
				case TokenId::TagClose:
					callable(token_id, match[1], empty);
					break;
				case TokenId::Text:
					callable(token_id, empty, match[1]);
					break;
				case TokenId::Attribute:
					callable(token_id, match[1], match[2]);
					break;
				case TokenId::TagOpenEndShort:
					callable(TokenId::TagClose, lastKey, empty);
					break;
				case TokenId::TagOpenEnd:
					break;
				}
				advance = match[0].length();
				context = token_context_next;
				break;
			}
		}

		if (advance == 0) {
			std::cout << position << std::endl;
			abort();
		}

		position += advance;
	}
}

void log(TokenId tokenId, const std::string& key, const std::string& value) {
	switch (tokenId) {
	case TokenId::TagOpen:
		std::cout << "Open: '" << key << "'" << std::endl;
		break;
	case TokenId::TagClose:
		std::cout << "Close: '" << key << "'" << std::endl;
		break;
	case TokenId::Text:
		std::cout << "Text: '" << value << "'" << std::endl;
		break;
	case TokenId::Attribute:
		std::cout << "Attribute: '" << key << "' = '" << value << "'" << std::endl;
		break;
	}
}

std::string load_file(const std::string& filename) {
	std::ifstream file(filename);
	std::stringstream buffer;
	buffer << file.rdbuf();
	return buffer.str();
}

int main() {
	std::string lastTag;

	auto get_param2 = [&lastTag](TokenId tokenId, const std::string& key, const std::string& value) {
		std::cout << key << std::endl;

		if ((lastTag == "ScalarVariable") && (tokenId == TokenId::Attribute)) {
			// std::cout << token_2_string(tokenId) << " *" << key << "* *" << value << "*" << std::endl;
		}
		if (tokenId == TokenId::TagOpen) {
			lastTag = key;
		}
	};

	const std::string test1 = "<t1 a=\"1\" b=\"2\"><t2>Text<t3/></t2></t1><t4 c=\"3\"/><t5/>";
	const std::string test2(load_file("openmodelica.xml"));

	// xml_lexer(test1, log);
	xml_lexer(test2, log);
}
