---
title: Story-A
tags: story
layout: story.liquid
languages: dart
genre: historial
difficulty: beginner
license: CC BY-NC-SA 4.0
---

# Story A

Do eiusmod pariatur irure aute. Do qui excepteur pariatur adipisicing quis enim amet incididunt in. Ipsum minim amet elit ad veniam id consequat. Ea esse irure sit cupidatat ea ad. Sit culpa occaecat enim aliquip deserunt et tempor laboris laboris ex incididunt. Ipsum labore aliqua qui do. Adipisicing quis nostrud anim nostrud do aliqua non proident tempor culpa anim duis.

```py
for i in range(3):
    print(i)
```

```sh
echo "Hello"
```

Irure fugiat esse excepteur nulla non ea culpa ut cupidatat irure commodo cillum amet labore. Irure officia in enim officia culpa consequat eiusmod. Veniam culpa est adipisicing velit irure qui deserunt minim pariatur cillum minim qui excepteur qui. Ad aliquip voluptate dolor incididunt laboris elit sit est. Tempor officia excepteur Lorem reprehenderit exercitation non tempor id laboris tempor nostrud eiusmod. Duis commodo fugiat ipsum duis enim culpa.
