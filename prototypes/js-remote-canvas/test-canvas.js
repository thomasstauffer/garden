document.body.innerHTML = '<canvas id="canvas"></canvas>';
const canvas = document.getElementById('canvas');
canvas.width  = window.innerWidth;
canvas.height = window.innerHeight;

const ctx = canvas.getContext('2d');

ctx.fillStyle = '#99f';
ctx.fillRect(0, 0, canvas.width, canvas.height);

ctx.strokeStyle = '#66f';
ctx.fillStyle = '#66f';

// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D

ctx.lineWidth = 10;
// Wall
ctx.strokeRect(75, 140, 150, 110);
// Door
ctx.fillRect(130, 190, 40, 60);
// Roof
ctx.beginPath();
ctx.moveTo(50, 140);
ctx.lineTo(150, 60);
ctx.lineTo(250, 140);
ctx.closePath();
ctx.stroke();
