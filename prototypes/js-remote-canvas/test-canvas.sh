#!/bin/sh

URL=http://127.0.0.1:4444/canvas

curl -X POST $URL/clear
curl -X POST -d strokeColor="red" -d lineWidth=10 -d x1=100 -d y1=100 -d x2=200 -d y2=100 $URL/line
curl -X POST -d strokeColor="blue" -d lineWidth=5 -d x=300 -d y=300 $URL/strokeCircle
curl -X POST -d strokeColor="purple" -d lineWidth=5 -d x=200 -d y=200 -d width=150 -d height=150 $URL/strokeRect
curl -X POST -d font="5em Ubuntu" -d x=300 -d y=150 -d text="Hallo" $URL/fillText
