#!/bin/sh

ADDRESS=http://127.0.0.1:4444

curl $ADDRESS/status
echo

sleep 3.0

curl -X POST --data "document.body.innerText = '';" $ADDRESS/js
sleep 1.0
curl -X POST --data "document.body.style.background = '#f00';" $ADDRESS/js
sleep 1.0
curl -X POST --data "document.body.style.background = '#0f0';" $ADDRESS/js
sleep 1.0
curl -X POST --data "document.body.style.background = '#00f';" $ADDRESS/js
sleep 1.0
curl -X POST --data "document.body.style.background = '#fff';" $ADDRESS/js
sleep 1.0
curl -X POST --data "Hello Remote Canvas" $ADDRESS/text
sleep 3.0
curl -X POST --data-binary @test-hw.js $ADDRESS/js
sleep 3.0
curl -X POST --data test-canvas.js $ADDRESS/jsfile
sleep 3.0
curl -X POST --data test-webgl.js $ADDRESS/jsfile
sleep 3.0
curl -X POST $ADDRESS/quit
