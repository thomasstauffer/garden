const electron = require('electron');
const http = require('http');
const os = require('os');
const fs = require('fs');
const querystring = require('querystring');

const PORT = 4444;

function getIPv4Address() {
	const networkInterfaces = os.networkInterfaces();
	for (const name in networkInterfaces) {
		if (name === 'lo') {
			continue;
		}
		const networkInterface = networkInterfaces[name];
		for (const entry of networkInterface) {
			if (entry.family === 'IPv4') {
				return entry.address;
			}
		}
	}
	return '127.0.0.1';
}

function getURL() {
	return `http://${getIPv4Address()}:${PORT}/`;
}

function executeJS(window, code) {
	//console.log(code);
	const pre = '(function () {';
	const post = '})();';
	window.webContents.executeJavaScript(pre + code + post);
	// TODO possible to escape local context
	// TODO catch exception?
}

let mainWindow = null;

function createWindow () {
	mainWindow = new electron.BrowserWindow({
		//fullscreen: true,
		width: 640,
		height: 480,
		webPreferences: {
			nodeIntegration: true
		}
	});
	mainWindow.loadFile('index.html');
	mainWindow.on('closed', () => {
		mainWindow = null;
	});
	executeJS(mainWindow, `document.getElementById('address').innerText = '${getURL()}'`);
	//win.webContents.openDevTools()
}

/*
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		electron.app.quit()
	}
})

app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		createWindow()
	}
})
*/

function responseStatus(res, code) {
	res.statusCode = code;
	res.end();
}

function responseJSON(res, data) {
	res.setHeader('Content-Type', 'application/json;charset=utf-8');
	res.end(JSON.stringify(data));
}

const HTTP_OK = 200;
const HTTP_NOT_FOUND = 404;
const HTTP_METHOD_NOT_ALLOWED = 405;

async function dataReadAsync(req) {
	return new Promise(resolve => {
		let data = '';
		req.on('data', (chunk) => {
			data += chunk;
		});
		req.on('end', () => {
			resolve(data);
		});
	});
}

function handleCanvas(command, options) {
	console.log(command);
	// TODO allows code injection everywhere here
	let code = '';
	if (command === 'clear') {
		code = `
		const ctx = getOrCreateContext2D();
		ctx.fillStyle = '${options.fillStyle || '#fff'}';
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		`;
	} else if (command === 'line') {
		code = `
		const ctx = getOrCreateContext2D();
		ctx.lineWidth = ${options.lineWidth || 1};
		ctx.strokeStyle = '${options.strokeColor || '#000'}';
		ctx.beginPath();
		ctx.moveTo(${options.x1 || 0}, ${options.y1 || 0});
		ctx.lineTo(${options.x2 || 100}, ${options.y2 || 100});
		ctx.closePath();
		ctx.stroke();
		`;
	} else if (command === 'strokeRect') {
		code = `
		const ctx = getOrCreateContext2D();
		ctx.lineWidth = ${options.lineWidth || 1};
		ctx.strokeStyle = '${options.strokeColor || '#000'}';
		ctx.beginPath();
		ctx.strokeRect(${options.x || 0}, ${options.y || 0}, ${options.width || 100}, ${options.height || 100});
		ctx.closePath();
		ctx.stroke();
		`;
	} else if (command === 'strokeCircle') {
		const radius = options.radius || 100;
		code = `
		const ctx = getOrCreateContext2D();
		ctx.lineWidth = ${options.lineWidth || 1};
		ctx.strokeStyle = '${options.strokeColor || '#000'}';
		ctx.beginPath();
		ctx.ellipse(${options.x || 0}, ${options.y || 0}, ${radius}, ${radius}, 0, 0, Math.PI * 2.0);
		ctx.closePath();
		ctx.stroke();
		`;
	} else if (command === 'fillText') {
		code = `
		const ctx = getOrCreateContext2D();
		ctx.font = '${options.font || '1em Ubuntu'}';
		ctx.fillStyle = '${options.fillColor || '#000'}';
		ctx.fillText('${options.text || '?'}', ${options.x || 0}, ${options.y || 0});
		`;
	}

	if (code.length > 0) {
		console.log(code);
		executeJS(mainWindow, code);
		return true;
	}
	return false;
}

/*
curl http://127.0.0.1:4444/status
*/
async function handler(req, res) {
	console.log(req.method, req.url);
	if (req.method === 'GET') {
		if (req.url === '/status') {
			const status = {
				running: true,
			};
			responseJSON(res, status);
		} else {
			responseStatus(res, HTTP_NOT_FOUND);
		}
	} else if (req.method === 'POST') {
		if (req.url === '/js') {
			const code = await dataReadAsync(req);
			executeJS(mainWindow, code);
			responseStatus(res, HTTP_OK);
		} else if (req.url === '/jsfile') {
			const path = await dataReadAsync(req);
			const code = fs.readFileSync(path);
			executeJS(mainWindow, code);
			responseStatus(res, HTTP_OK);
		} else if (req.url === '/text') {
			const text = await dataReadAsync(req);
			// TODO string injection!
			const code = `document.body.innerHTML = '<main><h1>${text}</h1></main>';`;
			executeJS(mainWindow, code);
			responseStatus(res, HTTP_OK);
		} else if (req.url === '/quit') {
			responseStatus(res, HTTP_OK);
			electron.app.quit();
		} else if (req.url.startsWith('/canvas/')) {
			const data = await dataReadAsync(req);
			const qs = querystring.parse(data);
			const command = req.url.split('/')[2]; // /canvas/init -> init
			const done = handleCanvas(command, qs);
			responseStatus(res, done ? HTTP_OK : HTTP_NOT_FOUND);
		} else {
			responseStatus(res, HTTP_NOT_FOUND);
		}
	} else {
		responseStatus(res, HTTP_METHOD_NOT_ALLOWED);
	}
}

const server = http.createServer(handler);
server.listen(PORT, () => {
	console.log(`${getURL()}`);
});

electron.app.whenReady().then(createWindow);
