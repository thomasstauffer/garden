﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace reader
{
    class TaskQueue
    {
        private Queue<Func<Task>> _tasks = new();
        private bool _quit = false;
        private SemaphoreSlim _signal = new(1);

        public void Add(Func<Task> f)
        {
            _tasks.Enqueue(f);
            _signal.Release();
            //Console.WriteLine(_signal.CurrentCount);
        }

        public void Quit()
        {
            _quit = true;
        }

        public async Task Run()
        {
            while (true)
            {
                var func = _tasks.Dequeue();
                await func();
                if (_quit) { return; }
                await _signal.WaitAsync();
            }
        }
    }

    class Program
    {
        static async Task MyTask()
        {
            Console.WriteLine("1");
            await Task.Delay(TimeSpan.FromMilliseconds(500));
            Console.WriteLine("2");
            await Task.Delay(TimeSpan.FromMilliseconds(500));
            Console.WriteLine("3");
        }

        static async Task MyException()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(0));
            throw new Exception("Oo");
        }
        
        public static async Task WhenAllException(params Task[] tasks)
        {
            var taskList = tasks.ToList();
            while (taskList.Any())
            {
                var completedTask = await Task.WhenAny(taskList);
                await completedTask;
                taskList.Remove(completedTask);
            }
        }        


        static async Task Main(string[] args)
        {
            var delay = Task.Delay(TimeSpan.FromMilliseconds(5000));

            /*
            var t1 = MyTask();
            var t2 = MyTask();
            await Task.WhenAll(t1, t2, delay);
            */

            var q = new TaskQueue();
            q.Add(MyTask);
            q.Add(MyTask);
            //q.Add(MyException);
            q.Add(MyTask);
            q.Add(async () => { q.Quit(); await Task.Delay(TimeSpan.FromSeconds(0)); });
            Console.WriteLine("WhenAll");
            //await Task.WhenAll(q.Run(), delay);
            await WhenAllException(q.Run(), delay);
        }
    }
}
