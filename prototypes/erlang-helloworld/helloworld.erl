
-module(helloworld).

-export([main/1]).
-export([start/0]).
-export([process_print/3]).

% f=float e=exponential s=string p=int, float, list, tuple, string
print(N) ->
	io:fwrite("~p\n", [N]).

convert(N, m) ->
	N * 1.0;
convert(N, mm) ->
	N * 1000.0;
convert(N, km) ->
	N / 1000.0.

% lists are linked list, nth is O(N)
times(_, []) -> void;
times(F, [H|T]) ->
	F(H),
	times(F, T).

process_print(Main_PID, What, Times) ->
	Fn1 = fun(_) ->
		print(What)
	end,
	times(Fn1, lists:seq(1, Times)),
	%timer:sleep(timer:seconds(1)),
	Main_PID ! finished.

process_start() ->
	spawn(?MODULE, process_print, [self(), "Hello", 3]),
	spawn(?MODULE, process_print, [self(), "World", 3]),
	receive
		_ -> void
	end,
	receive
		_ -> void
	end.

hw() ->
	io:fwrite("Hello World\n"),
	% functions
	print(convert(2222.0, m)),
	print(convert(2222.0, km)),
	% processes
	process_start(), % seems to have problems with escript
	% lists
	List = [3.0, 5.0, 7.0],
	print(List),
	print({hello, world}),
	[H | _] = List,
	print(H),
	Rep = lists:seq(1, 20, 5),
	Fn = fun(I) -> print(I) end,
	print(Rep),
	times(Fn, Rep),
	% maps
	Map = #{a => 1, b => 2, c => 3},
	Map2 = Map#{a => 111},
	print(Map),
	#{a := V} = Map,
	#{a := V2} = Map2,
	print(V),
	print(V2),
	io:fwrite("Adios World\n").

% escript helloworld.erl

main(_) -> hw().

% erlc helloworld.erl
% erl -noshell -s helloworld start -s init stop

start() -> hw().
