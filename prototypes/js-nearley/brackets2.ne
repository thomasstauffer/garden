@{%

const moo = require('moo');
const lexer = moo.compile({
	l3: /\[\[\[/,
	l2: /\[\[/,
	l1: /\[/,
});

%}

@lexer lexer

main -> elements {% id %}
elements -> element:+ {% id %}
element -> (one | two | three) {% data => data[0][0][0] %}
generic[L] -> $L {% data => 'B' + data[0][0].value.length %}
one -> generic[%l1]
two -> generic[%l2]
three -> generic[%l3]
