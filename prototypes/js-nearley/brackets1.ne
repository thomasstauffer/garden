@{%

const moo = require('moo');
const lexer = moo.compile({
	l3: /\[\[\[/,
	l2: /\[\[/,
	l1: /\[/,
});

%}

@lexer lexer

main -> elements {% id %}
elements -> element:+ {% id %}
element -> (one | two | three) {% data => data[0][0] %}
one -> %l1 {% data => 'B' + data[0].value.length %}
two -> %l2 {% data => 'B' + data[0].value.length %}
three -> %l3 {% data => 'B' + data[0].value.length %}
