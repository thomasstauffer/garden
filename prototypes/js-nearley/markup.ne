
@{%

/*
***ab*cd** ***ab**cd*
lexer cannot decide itself if "***" is "**,*" or "*,**"
*/

const moo = require('moo');
const lexer = moo.compile({
	alpha: /[a-zA-Z ]+/,
	bolditalic: /\*\*\*/,
	bold: /\*\*/,
	italic: /\*/,
	strike: /~/,
	underline: /_/,
});

%}

@lexer lexer

main -> elements {% id %}

elements -> element:* {% id %}

element -> (normal | bold | italic | strike | underline | bolditalic) {% data => data[0][0] %}

bold -> %bold elements %bold {% data => ({type:'*', value:data[1] }) %}
italic -> %italic elements %italic {% data => ({type:'**', value:data[1] }) %}
strike -> %strike elements %strike {% data => ({type:'~', value:data[1] }) %}
underline -> %underline elements %underline {% data => ({type:'_', value:data[1] }) %}
bolditalic -> (bolditalic1 | bolditalic2) {% data => data[0][0] %}
bolditalic1 -> %bolditalic elements %bold elements %italic {% data => ({type:'*', value:[{type:'**', value:data[1]}, data[3]]}) %}
bolditalic2 -> %bolditalic elements %italic elements %bold {% data => ({type:'**', value:[{type:'*', value:data[1]}, data[3]]}) %}

#special[XX] -> $XX elements $XX {% data => ({type:'BOLD', text:data[1] }) %}
#bold -> special[%bold %bold]
#italic -> special[%italic]
#strike -> special[%strike]
#underline -> special[%underline]

normal -> %alpha {% data => data[0].value %}
