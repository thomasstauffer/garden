
main -> elements {% id %}

# ambigous
#elements -> element:+ {% id %}
#element -> (one | two | three) {% data => data[0][0] %}

elements -> three:* elementrest {% data => data[0].concat(data[1]) %}
elementrest -> (two | one):? {% data => data[0] ?? [] %}

one -> "[" {% data => 'B' + data[0].length %}
two -> "[[" {% data => 'B' + data[0].length %}
three -> "[[[" {% data => 'B' + data[0].length %}
