use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct My {
    a: i32,
    b: String,
    c: bool,
}

fn main() {
    println!("rust-json");

    {
        let s = r#"{"a": 1234, "b": "bb", "c": false}"#;
        let j = json::parse(&s).unwrap();
        let n: i64 = j["a"].as_i64().unwrap();
        println!("{:?} {:?} {:?} {:?} {}", j, j["a"], j["b"], j["c"], n);

        let my: My = serde_json::from_str(s).unwrap();
        println!("{} {} {}", my.a, my.b, my.c);
    }

    {
        let my = My {
            a: 1,
            b: "Hello".to_string(),
            c: false,
        };
        let s1 = serde_json::to_string(&my).unwrap();
        println!("{}", s1);
    }
}
