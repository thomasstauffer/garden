
extern crate num;
use num::Num;

extern crate num_bigint;
use num_bigint::{BigInt};

extern crate num_traits;
use num_traits::{Zero, One};

fn is_divisible<T: Num>(number: T, divisor: T) -> bool {
	(number % divisor) == T::zero()
}

fn problem_1() -> i64 {
	let mut sum = 0;
	for number in 1 .. 1000 {
		if is_divisible(number, 3) || is_divisible(number, 5) {
			sum += number;
		}
	}
	sum
}

fn problem_2() -> i64 {
	let mut fib1 = 1;
	let mut fib2 = 1;
	let mut sum = 0;
	while fib2 < 4000000 {
		if is_divisible(fib2, 2) {
			sum += fib2;
		}
		let new = fib1 + fib2;
		fib1 = fib2;
		fib2 = new;
	}
	sum
}

fn factorize(number: i64) -> Vec<i64> {
	let mut factors: Vec<i64> = Vec::new();
	let mut n = number;
	while n > 1 {
		let mut found = false;
		let mut factor = 2;
		while (!found) && (factor <= n) {
			if is_divisible(n, factor) {
				factors.push(factor);
				n /= factor;
				found = true;
			}
			factor += 1;
		}
	}
	factors
}

fn problem_3() -> i64 {
	assert!(factorize(13195) == vec![5, 7, 13, 29]);
	let v = factorize(600851475143);
	v[v.len() - 1]
}

fn to_digits(number: i64) -> Vec<i8> {
	let mut digits: Vec<i8> = Vec::new();
	let mut n = number;
	while n > 0 {
		digits.insert(0, (n % 10) as i8);
		n /= 10;
	}
	digits
}

fn is_palindrome(number: i64) -> bool {
	let digits = to_digits(number);
	let len = digits.len();
	for i in 0 .. digits.len() / 2 {
		if digits[i] != digits[len - i - 1] {
			return false
		}
	}
	true
}

fn problem_4() -> i64 {
	let mut palindromes: Vec<i64> = Vec::new();
	for n1 in 100 .. 1000 {
		for n2 in 100 .. 1000 {
			let n12 = n1 * n2;
			if is_palindrome(n12) {
				palindromes.push(n12);
			}
		}
	}
	let max: i64 = *palindromes.iter().max().unwrap();
	max
}

fn problem_25() -> i64 {
	let mut fib1 = BigInt::zero();
	let mut fib2 = BigInt::one();
	let mut n = 1;
	while n < 1000000 {
		let new = fib1 + &fib2;
		fib1 = fib2;
		fib2 = new;
		let len = fib1.to_str_radix(10).len();
		//println!("{} {} {}", n, fib1, len);
		if len >= 1000 {
			return n
		}
		n += 1
	}
	n
}

fn power5(number: i64) -> i64 {
	number * number * number * number * number
}

#[allow(unused)]
fn power4(number: i64) -> i64 {
	number * number * number * number
}

// TODO fn power(base: i64, exponent: i64) -> i64 { }

fn problem_30() -> i64 {
	let mut result = 0;
	for i in 2 .. 1000000 {
		let digits = to_digits(i);
		let mut sum5: i64 = 0;
		for digit in &digits {
			sum5 += power5(*digit as i64);
		}
		if sum5 == i {
			result += i;
		}	
	}
	result
}

fn factorial(number: i64) -> i64 {
	let mut product: i64 = 1;
	for i in 2 .. (number + 1) {
		product *= i;
	}
	product
}

fn factorial_digit(digit: i8) -> i64 {
	match digit {
		0 => 1,
		1 => 1,
		2 => 2,
		3 => 6,
		4 => 24,
		5 => 120,
		6 => 720,
		7 => 5040,
		8 => 40320,
		9 => 362880,
		_ => panic!(),
	}
}

fn problem_34() -> i64 {
	assert_eq!(factorial(4), 24);
	let mut sum: i64 = 0;
	for n in 3 .. 50000 {
		let digits = to_digits(n);
		let mut sum_factorial: i64 = 0;
		for digit in digits {
			sum_factorial += factorial_digit(digit);
		}
		if sum_factorial == n {
			sum += n;
		}
	}
	sum
}

fn pentagonal(n: i64) -> i64 {
	n * ((3 * n) - 1) / 2
}

fn is_pentagonal(x: i64) -> bool {
	// solve(x=(3*n^2-n)/2, n)
	let n = ((((x * 24) + 1) as f64).sqrt() + 1.0) / 6.0; // TODO does this fail for large/small values?
	(n - n.floor()).abs() < 1.0e-9
}

fn problem_44() -> i64 {
	let bound = 3000;
	for j in 1 .. bound {
		for k in 1 .. bound {
			let sum = pentagonal(j) + pentagonal(k);
			let diff = pentagonal(j) - pentagonal(k);
			let is_sum_p = is_pentagonal(sum);
			if is_sum_p {
				let is_diff_p = is_pentagonal(diff);
				if is_diff_p {
					//println!("{} {} {} {} {}", j, k, pentagonal(j), pentagonal(k), pentagonal(j) - pentagonal(k))
					return pentagonal(j) - pentagonal(k)
				}
			}
		}
	}
	0
}

fn from_digits(digits: &Vec<i8>) -> i64 {
	let mut result: i64 = 0;
	for digit in digits {
		result *= 10;
		result += *digit as i64;
	}
	result
}

fn reverse(number: i64) -> i64 {
	let digits = to_digits(number);
	let len = digits.len();
	let mut reverse = vec![0; len];
	for i in 0 .. len {
		reverse[i] = digits[len - 1 - i];
	}
	from_digits(&reverse)
}

fn is_lychrel(number: i64, max_iterations: i64) -> bool {
	let mut n = number;
	for _ in 0 .. max_iterations {
		n = n + reverse(n);
		//println!("{}", n);
		if is_palindrome(n) {
			return false;
		}
	}
	true
}

fn problem_55() -> i64 {
	assert_eq!(from_digits(&vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1]), 12345678987654321);
	assert_eq!(reverse(123456789), 987654321);
	assert!(!is_lychrel(47, 50));
	assert!(!is_lychrel(349, 50));
	let mut n = 0;
	let max_iteration = 29; // TODO should be 50 according to problem but this leads to overflow with i64 and needs bigint
	for i in 1 .. 10000 {
		if is_lychrel(i, max_iteration) {
			n += 1;
		}
	}
	n
}

fn is_whole_number(value: f64) -> bool {
	(value - value.floor()).abs() < 1.0e-9
}

fn triangle(n: i64) -> i64 {
	(n * (n + 1)) / 2
}

fn is_triangle(x: i64) -> bool {
	// solve(x=(n*(n+1))/2, n)
	let n = ((((x * 8) + 1) as f64).sqrt() - 1.0) / 2.0;
	is_whole_number(n)
}

fn square(n: i64) -> i64 {
	n * n
}

fn is_square(x: i64) -> bool {
	// solve(x=n*n, n)
	let n = (x as f64).sqrt();
	is_whole_number(n)
}

fn hexagonal(n: i64) -> i64 {
	n * ((2 * n) - 1)
}

fn is_hexagonal(x: i64) -> bool {
	// solve(x=n*((2 * n) - 1), n)
	let n = ((((x * 8) + 1) as f64).sqrt() + 1.0) / 2.0;
	is_whole_number(n)
}

fn heptagonal(n: i64) -> i64 {
	(n * ((5 * n) - 3)) / 2
}

fn is_heptagonal(x: i64) -> bool {
	// solve(x=(n*((5 * n) - 3))/2, n)
	let n = ((((x * 40) + 9) as f64).sqrt() + 3.0) / 10.0;
	is_whole_number(n)
}

fn octagonal(n: i64) -> i64 {
	n * ((3 * n) - 2)
}

fn is_octagonal(x: i64) -> bool {
	// solve(x=n*((3 * n) - 2), n)
	let n = ((((x * 3) + 1) as f64).sqrt() + 1.0) / 3.0;
	is_whole_number(n)
}

#[allow(dead_code)]
fn is_polygonal(x: i64) -> bool {
	is_triangle(x) || is_square(x) || is_pentagonal(x) || is_hexagonal(x) || is_heptagonal(x) || is_octagonal(x)
}

#[allow(dead_code)]
fn which_polygonal(x: i64) -> Vec<i8> {
	let mut result: Vec<i8> = vec![];
	if is_triangle(x) {
		result.push(3);
	}
	if is_square(x) {
		result.push(4);
	}
	if is_pentagonal(x) {
		result.push(5);
	}
	if is_hexagonal(x) {
		result.push(6);
	}
	if is_heptagonal(x) {
		result.push(7);
	}
	if is_octagonal(x) {
		result.push(8);
	}
	result
}

// TODO write it with only one vector and swap elements?
// TODO ref type
fn permutations(head: Vec<i64>, rest: Vec<i64>) -> Vec<Vec<i64>> {
	if rest.len() == 0 {
		return vec![head];
	}
	let mut result: Vec<Vec<i64>> = vec![]; 
	for i in 0 .. rest.len() {
		let mut new_head = head.clone();
		let mut new_rest = rest.clone();
		new_head.push(new_rest[i]);
		new_rest.remove(i);
		let mut x = permutations(new_head, new_rest);
		result.append(&mut x);
	}
	result
}

fn problem_61() -> i64 {
	assert_eq!(permutations(vec![], vec![0, 1, 2]).len(), 6);
	
	let mut polygonals: Vec<Vec<i64>> = vec![vec![], vec![], vec![], vec![], vec![], vec![]];
	for i in 1 .. 200 {
		for j in 0 .. 6 {
			let n = match j {
				0 => triangle(i),
				1 => square(i),
				2 => pentagonal(i),
				3 => hexagonal(i),
				4 => heptagonal(i),
				5 => octagonal(i),
				_ => panic!(),
			};
			if (n >= 1000) && (n <= 9999) {
				polygonals[j].push(n);
			}
		}
	}
	
	for permuation in permutations(vec![], vec![0, 1, 2]) {
		let (i, j, k) = (permuation[0] as usize, permuation[1] as usize, permuation[2] as usize);
		for pi in &polygonals[i] {
			for pj in &polygonals[j] {
				if (pi % 100) != (pj / 100) {
					continue
				}
				for pk in &polygonals[k] {
					if (pj % 100) != (pk / 100) {
						continue
					}
					if (pk % 100) != (pi / 100) {
						continue
					}
					//println!("{} {} {}", pi, pj, pk);
				}
			}
		}
	}

	for permuation in permutations(vec![], vec![0, 1, 2, 3, 4, 5]) {
		let (i, j, k, l, m, n) = (permuation[0] as usize, permuation[1] as usize, permuation[2] as usize, permuation[3] as usize, permuation[4] as usize, permuation[5] as usize);
		for pi in &polygonals[i] {
			for pj in &polygonals[j] {
				if (pi % 100) != (pj / 100) {
					continue
				}
				for pk in &polygonals[k] {
					if (pj % 100) != (pk / 100) {
						continue
					}
					for pl in &polygonals[l] {
						if (pk % 100) != (pl / 100) {
							continue
						}
						for pm in &polygonals[m] {
							if (pl % 100) != (pm / 100) {
								continue
							}
							for pn in &polygonals[n] {
								if (pm % 100) != (pn / 100) {
									continue
								}
								if (pn % 100) != (pi / 100) {
									continue
								}
								let sum = pi + pj + pk + pl + pm + pn;
								//println!("{} {} {} {} {} {} {}", pi, pj, pk, pl, pm, pn, sum);
								return sum;
							}
						}
					}
				}
			}
		}
	}
	0
}

fn partition(sum: i64, summand: i64, max: i64) -> i64 {
	if sum == max {
		return 1
	}
	if sum + summand > max {
		return 0
	}
	let mut count = 0;
	let max_n = (max - sum) / summand;
	for n in 0 .. max_n + 1 {
		count += partition(sum + (n * summand), summand + 1, max)
	}
	count
}

fn problem_76() -> i64 {
	partition(0, 1, 50) - 1
}


#[allow(dead_code)]
fn sum(vec: &Vec<i64>) -> i64 {
	//vec.iter().fold(0, |sum, x| sum + x)
	vec.iter().sum()
}

#[allow(dead_code)]
fn product(vec: &Vec<i64>) -> i64 {
	vec.iter().fold(1, |product, x| product * x)
}

#[allow(dead_code)]
fn is_in_vec(vec: &Vec<Vec<i64>>, what: &Vec<i64>) -> bool {
	// vec.iter().find(|&& ref x| x == what).is_some() // almost same speed as contains
	vec.contains(what)
}

#[allow(dead_code)]
fn partition_old(/*v: Vec<i64>,*/ sum: i64, max: i64, index: usize /*, all: &mut Vec<Vec<i64>>*/, count: &mut i64) {
	//let s = sum(&v);
	let s = sum;
	
	if s == max {
		//println!("{:?}", v);
		//all.push(v.clone());
		*count += 1;
		return
	}
	
	if s > max {
		return
	}
	
	
	if index >= max as usize {
		return
	}
	
	let vv = (max - sum) / (index as i64 + 1);
	
	for i in 0 .. vv + 1 {
		/*
		let mut n = v.clone();
		for _ in 0 .. i {
			n.push(index as i64 + 1);
		}
		*/
		let s = sum + (i as i64 * (index as i64 + 1));
		partition_old(/*vec![],*/ s, max, index + 1, /*all,*/ count)
	}
}

#[allow(dead_code)]
fn test() {
	assert_eq!(problem_1(), 233168);
	assert_eq!(problem_2(), 4613732);
	assert_eq!(problem_3(), 6857);
	assert_eq!(problem_4(), 906609);
	assert_eq!(problem_25(), 4782);
	assert_eq!(problem_30(), 443839);
	assert_eq!(problem_34(), 40730);
	assert_eq!(problem_44(), 5482660);
	assert_eq!(problem_55(), 249);
	assert_eq!(problem_61(), 28684);
	assert_eq!(problem_76(), 204225 /* 100 = 190569291 takes a few seconds too much atm */);
}

fn main() {
	test();
}
