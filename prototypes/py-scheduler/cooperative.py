#!/usr/bin/env python3

import time


class TaskManager:
	def __init__(self):
		self.list = []
		self.now = 0.0

	def call_immediately(self, callable):
		self.call_in(callable, 0.0)

	def call_in(self, callable, duration):
		self.list.append((callable, self.now + duration))

	def run(self):
		for i in range(10):
			callable, time_point = self.list[0]
			duration = time_point - self.now
			self.now += duration
			time.sleep(duration)
			callable()
			self.list.pop(0)
			self.list.sort(key=lambda e: e[1])
			if len(self.list) == 0:
				break


class App:
	def __init__(self, tm):
		self.tm = tm

	def a(self):
		print('a')
		self.tm.call_in(self.b, 0.1)
		self.tm.call_in(self.a, 0.2)

	def b(self):
		print('b')


def main():
	tm = TaskManager()
	app = App(tm)
	tm.call_immediately(app.a)
	tm.run()


main()
