﻿using Google.OrTools.LinearSolver;
using System;

namespace ConsoleGarden
{
    // https://core.ac.uk/reader/82123790
    public class MasudaBicycle
    {
        public static void Solve()
        {
            var solver = Solver.CreateSolver("GLOP");

            const int n = 3; // number of people

            double[] w = [1, 2, 1]; // walk speeds
            double[] b = [6, 8, 6]; // bike speeds

            var x = new Variable[n]; // time spent walking forward
            var u = new Variable[n]; // time spent walking backward
            var y = new Variable[n]; // time spent bicycling forward
            var z = new Variable[n]; // time spend bicycling backward

            const double d = 100.0; // distance

            for (int i = 0; i < n; i++)
            {
                x[i] = solver.MakeNumVar(0.0, double.PositiveInfinity, "x" + i);
                u[i] = solver.MakeNumVar(0.0, double.PositiveInfinity, "u" + i);
                y[i] = solver.MakeNumVar(0.0, double.PositiveInfinity, "y" + i);
                z[i] = solver.MakeNumVar(0.0, double.PositiveInfinity, "z" + i);
            }
            
            var t = solver.MakeNumVar(0.0, double.PositiveInfinity, "t"); // time it takes for the slowest person to arrive

            // time spent on all activities <= t

            for (int i = 0; i < n; i++)
            {
                var c = solver.MakeConstraint(double.NegativeInfinity, 0.0);
                c.SetCoefficient(x[i], 1);
                c.SetCoefficient(u[i], 1);
                c.SetCoefficient(y[i], 1);
                c.SetCoefficient(z[i], 1);
                c.SetCoefficient(t, -1);
            }

            // everyone must at least reach the distance

            for (int i = 0; i < n; i++)
            {
                var c = solver.MakeConstraint(d, double.PositiveInfinity);
                c.SetCoefficient(x[i], w[i]);
                c.SetCoefficient(u[i], -w[i]);
                c.SetCoefficient(y[i], b[i]);
                c.SetCoefficient(z[i], -b[i]);
            }

            // bicycle should also be used <= t

            var bt = solver.MakeConstraint(double.NegativeInfinity, 0.0);
            for (int i = 0; i < n; i++)
            {
                bt.SetCoefficient(y[i], 1);
                bt.SetCoefficient(z[i], 1);
            }
            bt.SetCoefficient(t, -1);

            // bicycle should be used <= d

            var bd = solver.MakeConstraint(0.0, d);
            for (int i = 0; i < n; i++)
            {
                bd.SetCoefficient(y[i], b[i]);
                bd.SetCoefficient(z[i], -b[i]);
            }

            var objective = solver.Objective();

            objective.SetCoefficient(t, 1);

            objective.SetMinimization();

            var result = solver.Solve();
            Console.WriteLine(result);

            var s = new double[n];
            for (int i = 0; i < n; i++)
            {
                s[i] = x[i].SolutionValue() * w[i] - u[i].SolutionValue() * w[i] + y[i].SolutionValue() * b[i] - z[i].SolutionValue() * b[i];
            }

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("wf:{0:0.00} wb:{1:0.00} bf:{2:0.00} bb:{3:0.00} - t:{4:0.00}", x[i].SolutionValue(), u[i].SolutionValue(), y[i].SolutionValue(), z[i].SolutionValue(), s[i]);
            }

        }
    }
}
