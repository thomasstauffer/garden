﻿using System;

/*
https://developers.google.com/optimization/examples
*/
namespace ConsoleGarden
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Optimization Problems");

            StiglerDiet.Solve();
            MasudaBicycle.Solve();
            Manufacturing.Solve();
        }
    }
}
