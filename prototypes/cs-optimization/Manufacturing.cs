﻿using Google.OrTools.Sat;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleGarden
{
    public class Manufacturing
    {
        // https://developers.google.com/optimization/scheduling/job_shop
        public static void Solve()
        {
            const int machine0 = 0;
            const int machine1 = 1;
            const int machine2 = 2;

            (int machine, int duration)[][] allJobs = [
                [(machine0, 3), (machine1, 2), (machine2, 2)],
                [(machine0, 2), (machine2, 1), (machine1, 4)],
                [(machine1, 4), (machine2, 3)],
            ];

            var numMachines = allJobs.Max(job => job.Max(task => task.machine)) + 1;

            var maxDuration = allJobs.Sum(job => job.Sum(task => task.duration));

            var model = new CpModel();

            // Variables

            var allTasks = new Dictionary<(int jobID, int taskID), (IntVar start, IntVar end, IntervalVar interval)>();
            var machineIntervals = Enumerable.Range(0, numMachines).Select(_ => new List<IntervalVar>()).ToArray();
            for (int jobID = 0; jobID < allJobs.Length; jobID += 1)
            {
                var job = allJobs[jobID];
                for (int taskID = 0; taskID < job.Length; taskID += 1)
                {
                    var task = job[taskID];
                    var suffix = $"_{jobID}_{taskID}";
                    var start = model.NewIntVar(0, maxDuration, "start" + suffix);
                    var end = model.NewIntVar(0, maxDuration, "end" + suffix);
                    var interval = model.NewIntervalVar(start, task.duration, end, "interval" + suffix);
                    allTasks[(jobID, taskID)] = (start, end, interval);
                    machineIntervals[task.machine].Add(interval);
                }
            }

            // NoOverlap

            for (int machine = 0; machine < numMachines; machine += 1)
            {
                model.AddNoOverlap(machineIntervals[machine]);
            }

            // Precedences

            for (int jobID = 0; jobID < allJobs.Length; jobID += 1)
            {
                for (int taskID = 0; taskID < allJobs[jobID].Length - 1; taskID += 1)
                {
                    var key = (jobID, taskID);
                    var nextKey = (jobID, taskID + 1);
                    model.Add(allTasks[nextKey].start >= allTasks[key].end);
                }
            }

            // Objective

            IntVar objVar = model.NewIntVar(0, maxDuration, "makespan");

            var ends = new List<IntVar>();
            for (int jobID = 0; jobID < allJobs.Length; jobID += 1)
            {
                var key = (jobID, allJobs[jobID].Length - 1);
                ends.Add(allTasks[key].end);
            }
            model.AddMaxEquality(objVar, ends);
            model.Minimize(objVar);

            var solver = new CpSolver();
            var status = solver.Solve(model);

            // Output

            Console.WriteLine("Solution");
            Console.WriteLine(status);

            var taskResults = allJobs.SelectMany((job, jobID) => job.Select((task, taskID) =>
            {
                var key = (jobID, taskID);
                int start = (int)solver.Value(allTasks[key].start);
                int end = (int)solver.Value(allTasks[key].end);
                return (task.machine, jobID, taskID, start, end);
            })).OrderBy(task => task.start);

            foreach (var job in taskResults.GroupBy(task => task.jobID).OrderBy(job => job.Key))
            {
                Console.WriteLine("Job {0}", job.Key);
                foreach (var task in job)
                {
                    Console.WriteLine("\tMachine {0} TaskID {2} Start {3} End {4} ", task.machine, task.jobID, task.taskID, task.start, task.end);
                }
            }

            foreach (var machine in taskResults.GroupBy(task => task.machine))
            {
                Console.WriteLine("Machine {0}", machine.Key);
                foreach (var task in machine)
                {
                    Console.WriteLine("\tJobID {1} TaskID {2} Start {3} End {4} ", task.machine, task.jobID, task.taskID, task.start, task.end);
                }
            }
        }
    }
}
