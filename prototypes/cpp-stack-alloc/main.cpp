
#include <array>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <set>
#include <vector>

/*
g++ -Wall -g -std=c++14 -O2 main.cpp -o main && ./main
*/

/*
https://stackoverflow.com/questions/31727791/contiguous-memory-allocation-for-several-small-stdvectors
*/

/*
only useful if allocate uses size = 1

allocate and deallocate are both O(1)

keeps two buffers

- one buffer with N cells with the actual content
- second buffer has as size of N and remembers which cells are reserved

ideas

to be more cache friendly, create a struct for and keep just one buffer/array.

*/
template <typename T, size_t N>
class custom_alloc {
public:
	using size_type = size_t;
	using pointer = T*;
	using value_type = T;

	template <class U>
	struct rebind {
		using other = custom_alloc<U, N>;
	};

	explicit custom_alloc() noexcept : _count_allocate(0) {
		// std::cout << "custom_alloc" << std::endl;
		for (size_t i = 0; i < N; i += 1) {
			_free[i] = i;
		}
		_smallestN = N;
		_freeN = N;
	}

	~custom_alloc() noexcept {
		std::cout << "custom_alloc count:" << _count_allocate << " smallest:" << _smallestN << std::endl;
		// std::cout << "~custom_alloc" << std::endl;
	}

	pointer allocate(size_type size) {
		// search would be necessary if size not 1
		if(size != 1) {
			std::cout << "size not one: " << size << std::endl;
			std::abort();
		}

		_count_allocate += 1;

		if (_freeN == 0) {
			// bad_alloc?
			std::cout << "no space" << std::endl;
			std::abort();
		}

		_freeN -= 1;
		if (_freeN < _smallestN) {
			_smallestN = _freeN;
		}

		size_t index = _free[_freeN] * sizeof(value_type);
		return pointer(&_buffer[index]);
	}

	void deallocate(pointer p, size_type size) noexcept {
		size_t index = p - pointer(_buffer);
		_free[_freeN] = index;
		_freeN += 1;
	}

private:
	uint32_t _count_allocate; // statistics
	size_t _smallestN; // statistics: how much of the space was used
	size_t _freeN;
	std::array<size_t, N> _free;
	uint8_t _buffer[sizeof(value_type) * N];
};

template <typename Allocator>
void measure() {
	auto start = std::chrono::system_clock::now();

	std::set<int, std::less<int>, Allocator> s;

	std::srand(0); // to be deterministic in the test

	for (uint32_t i = 0; i < 10'000'000; i += 1) {
		s.insert(std::rand() % 1000);
		s.erase(std::rand() % 1000);
	}

	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "elapsed ms:" << elapsed.count() << std::endl;
}

/*
g++ -O3 main.cpp -o main
./main
elapsed ms:1030
elapsed ms:969
custom_alloc count:5002821 smallest:453
*/

int main() {
	measure<std::allocator<int>>();
	measure<custom_alloc<int, 1000>>();

	return 0;
}
