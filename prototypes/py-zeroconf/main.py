#!/usr/bin/env python3

'''
https://github.com/python-zeroconf/python-zeroconf
https://python-zeroconf.readthedocs.io/en/latest/
http://www.dns-sd.org/ServiceTypes.html
'''

import sys
import socket
import time

# apt install python3-zeroconf
from zeroconf import ServiceBrowser, ServiceListener, Zeroconf, ZeroconfServiceTypes, ServiceInfo


class Listener(ServiceListener):
    def update_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        self.log(zc, type_, name, 'update')

    def remove_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        self.log(zc, type_, name, 'remove')
        print(f'Service {name} removed')

    def add_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        self.log(zc, type_, name, 'add')

    def log(self, zc: Zeroconf, type_: str, name: str, action: str):
        info = zc.get_service_info(type_, name)
        address = socket.inet_ntoa(info.addresses[0])
        port = info.port
        print(f'{name} {address}:{port}: {action}')


def listen(zeroconf: Zeroconf):
    listener = Listener()
    browser = ServiceBrowser(zeroconf, '_http._tcp.local.', listener)


def register(zeroconf: Zeroconf):
    service_name = 'alr'
    service_type = '_http._tcp.local.'
    port = 8080
    #address = socket.gethostbyname(socket.gethostname())
    address = '172.24.103.108'
    info = ServiceInfo(
        service_type,
        f'{service_name}.{service_type}',
        addresses=[socket.inet_aton(address)],
        port=port,
        properties={},
    )
    print(f'{service_name}.{service_type} {address}:{port}: register')
    zeroconf.register_service(info)


def main():
    zeroconf = Zeroconf()

    register(zeroconf)
    listen(zeroconf)

    while True:
        time.sleep(10.0)

    #zeroconf.unregister_service(info)
    zeroconf.close()

main()
