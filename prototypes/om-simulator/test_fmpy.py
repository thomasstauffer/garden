#!/usr/bin/env python3

"""
pip install pyqtgraph
python -m fmpy.gui

pip install dash
pip install dash_bootstrap_components
python -m fmpy.webapp Test.fmu

https://github.com/CATIA-Systems/FMPy/blob/master/fmpy/examples/custom_input.py
"""

import fmpy  # pip install fmpy
import numpy as np
import fmpy.fmi2


def run_and_plot():
	filename = 'Test.fmu'

	fmpy.dump(filename)
	result = fmpy.simulate_fmu(filename)

	import fmpy.util
	fmpy.util.plot_result(result)

def main():
	filename = 'Test.fmu'
	model_description = fmpy.read_model_description(filename)

	for variable in model_description.modelVariables:
		print(variable.name)

main()
