#!/usr/bin/env python3

import OMSimulator as oms  # pip3 install OMSimulator

t = 0.0
T_STEP = 0.1
T_END = 2.0

print(oms.version)

oms.setTempDirectory('/tmp/ompy')
model = oms.newModel('model')
root = model.addSystem('root', oms.Types.System.SC)
root.addSubModel('system', 'Test.fmu')
model.resultFile = ''
model.stopTime = T_END
model.instantiate()
model.initialize()

signals = model.getAllSignals()
for signal in signals:
	print(signal, signals[signal]['type'])

for i in range(1 + int(T_END / T_STEP)):
	t = i * T_STEP
	model.stepUntil(t)
	print(t, model.getReal('root.system.R.i'))

model.terminate()
model.delete()
