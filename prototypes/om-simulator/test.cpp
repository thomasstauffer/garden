/*
https://github.com/OpenModelica/OMSimulator
*/

#include <stdio.h>

#include <OMSimulator.h>

void f() {
	printf("%s\n", oms_getVersion());

	oms_setTempDirectory("/tmp/om");
	oms_newModel("model");
	oms_setLoggingLevel(0);
	oms_addSystem("model.root", oms_system_sc);
	oms_addSubModel("model.root.system", "./Test.fmu");
	oms_setResultFile("model", "", 0); // "" = do not create a file
	oms_setStopTime("model", 1e3);
	oms_instantiate("model");
	oms_initialize("model");

	for(double t = 0.0; t < 2.0; t += 0.1) {
		oms_stepUntil("model", t);
		double value;
		oms_getReal("model.root.system.R.i", &value);
		printf("%f %f\n", t, value);
	}

	oms_terminate("model");
}

int main() {
	f();
}
