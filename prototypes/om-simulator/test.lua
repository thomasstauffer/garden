
print(oms_getVersion())

oms_setTempDirectory("/tmp/om")
oms_newModel("model")
oms_addSystem("model.root", oms_system_sc)
oms_addSubModel("model.root.system", "./Test.fmu")
oms_setResultFile("model", "") -- "" = do not create a file
oms_setStopTime("model", 1e3)
oms_instantiate("model")
oms_initialize("model")

for t = 0, 2.0, 0.1 do
	oms_stepUntil("model", t)
	value, status = oms_getReal("model.root.system.R.i")
	print(t, value)
end

oms_terminate("model")
