
git clone https://github.com/rauc/rauc.git

apt install libtool automake libjson-glib-dev libssl-dev
./autogen.sh

Without D-Bus

./configure --disable-service
make

With D-Bus

./configure --disable-service
make
sudo make install
systemctl start rauc.service
systemctl status rauc.service
