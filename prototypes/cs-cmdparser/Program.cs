﻿
// dotnet add package System.CommandLine --version 2.0.0-beta4.22272.1
// dotnet add package System.CommandLine.NamingConventionBinder --version 2.0.0-beta4.22272.1

using System;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;

class Program
{
    public static void Run(string arg1, int opt1, int opt2)
    {
        Console.WriteLine("arg1 {0}", arg1);
        Console.WriteLine("opt1 {0}", opt1);
        Console.WriteLine("opt2 {0}", opt2);
    }

    public static void Main(string[] args)
    {
        var arg1 = new Argument<string>("arg1", "Argument 1");
        var opt1 = new Option<int>("--opt1", "Option 1");
        var opt2 = new Option<int>("--opt2", "Option 2");
        opt2.SetDefaultValue(666);

        var cmd = new RootCommand() { arg1, opt1, opt2 };
        cmd.Handler = CommandHandler.Create<string, int, int>(Run);
        cmd.Invoke(args);
    }
}
