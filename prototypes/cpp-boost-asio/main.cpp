
#include <boost/asio.hpp>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>

// using boost::asio::ip::tcp;
using boost::asio::local::stream_protocol;

class logger {
public:
	void log(std::string msg) {
		std::cout << msg << std::endl;
	}
};

class session : public std::enable_shared_from_this<session> {
public:
	session(stream_protocol::socket socket, logger& log) : _socket(std::move(socket)), _log(log) {
	}

	void start() {
		do_read();
	}

private:
	void do_read() {
		auto self(shared_from_this());
		_socket.async_read_some(boost::asio::buffer(_data, max_length), [this, self](boost::system::error_code ec, std::size_t length) {
			if (!ec) {
				_log.log("echo");
				do_write(length);
			}
		});
	}

	void do_write(std::size_t length) {
		auto self(shared_from_this());
		boost::asio::async_write(_socket, boost::asio::buffer(_data, length), [this, self](boost::system::error_code ec, std::size_t /*length*/) {
			if (!ec) {
				do_read();
			}
		});
	}

	logger& _log;
	stream_protocol::socket _socket;
	static constexpr size_t max_length = 1024;
	char _data[max_length];
};

// tcp::endpoint(tcp::v4(), port))
class server {
public:
	server(boost::asio::io_context& io_context, logger& log, std::string _socketpath) : _acceptor(io_context, stream_protocol::endpoint(_socketpath)), _log(log) {
		do_accept();
	}

private:
	void do_accept() {
		_acceptor.async_accept([this](boost::system::error_code ec, stream_protocol::socket socket) {
			if (!ec) {
				std::make_shared<session>(std::move(socket), _log)->start();
			}
			do_accept();
		});
	}

	logger& _log;
	stream_protocol::acceptor _acceptor;
};

// nc -U /tmp/asio
int main(int argc, char* argv[]) {
	try {

		std::string _socketpath = "/tmp/asio";
		unlink(_socketpath.c_str());

		boost::asio::io_context io_context;
		logger log;
		server s(io_context, log, _socketpath);
		std::cout << "start" << std::endl;

		io_context.run();
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
