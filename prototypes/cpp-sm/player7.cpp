
#include <iostream>
#include <tuple>
#include <algorithm>
#include <sstream>

/*
g++ -O0 player7.cpp -o player7 && ./player7
*/

using czstring = const char*;

constexpr bool always_true() {
	return true;
}

constexpr void no_action() {}

struct event {
	czstring _name;
	constexpr event(czstring name) : _name(name) {}
};

constexpr event no_event("");

struct stateid {
	czstring _name;
	constexpr stateid(czstring name) : _name(name) {}
};

constexpr stateid no_state("");

template<typename Guard = bool(*)(), typename Action = void(*)()>
struct transition {
	Guard _guard;
	event _event;
	Action _action;
	stateid _to;
	constexpr explicit transition(Guard guard, event event, Action action, stateid to) : _guard(guard), _event(event), _action(action), _to(to) {}
	constexpr explicit transition() : _guard(always_true), _event(no_event), _action(no_action), _to(no_state) {}
	template<typename G>
	constexpr auto guard(G guard) { return transition<G, Action>(guard, _event, _action, _to); }
	constexpr auto on(event event) { return transition(_guard, event, _action, _to); }
	template<typename A>
	constexpr auto action(A action) { return transition<Guard, A>(_guard, _event, action, _to); }
	constexpr auto to(stateid to) { return transition(_guard, _event, _action, to); }
};

template<typename OnEnter = void(*)(), typename OnExit = void(*)(), typename Transitions = decltype(std::tuple())>
struct state {
	stateid _id;
	OnEnter _on_enter;
	OnExit _on_exit;
	Transitions _transitions;
	constexpr explicit state(stateid id, OnEnter on_enter, OnExit on_exit, Transitions transitions) : _id(id), _on_enter(on_enter), _on_exit(on_exit), _transitions(transitions) {}
	constexpr explicit state(stateid id) : _id(id), _on_enter(no_action), _on_exit(no_action), _transitions(std::tuple()) {}
	template<typename OE>
	constexpr auto on_enter(OE on_enter) { return state<OE, OnExit, Transitions>(_id, on_enter, _on_exit, _transitions); }
	template<typename OE>
	constexpr auto on_exit(OE on_exit) { return state<OnEnter, OE, Transitions>(_id, _on_enter, on_exit, _transitions); }
	template<typename T>
	constexpr auto transitions(T transitions) { return state<OnEnter, OnExit, T>(_id, _on_enter, _on_exit, transitions); }
};

template<typename Transition>
void to_mermaid_handle_transition(std::stringstream& ss, czstring from, Transition transition) {
	ss << from << " --> " << transition._to._name << " : " << transition._event._name << "\n";
}

template<typename State>
void to_mermaid_handle_state(std::stringstream& ss, State state) {
	std::apply([&](auto&... transitions) { (to_mermaid_handle_transition(ss, state._id._name, transitions), ...); }, state._transitions);
}

template<typename Model>
std::string to_mermaid() {
	std::stringstream ss;
	ss << "stateDiagram-v2\n";
	auto states = Model().make();
	ss << "[*] --> " << std::get<0>(states)._id._name << "\n";
	std::apply([&](auto&... states) { (to_mermaid_handle_state(ss, states), ...); }, states);
	return ss.str();
}

template<typename Model>
struct machine {
	stateid _state = stateid("off");

	using States = decltype(std::declval<Model>().make());

	Model _model;
	const States _states;

	machine() : _states(_model.make()) {}

	constexpr czstring current() const {
		return _state._name;
	}
	
	template<typename Tuple, typename Action>
	void each(Tuple tuple, Action action) {
		std::apply([&](auto&... ts) { (action(ts), ...); }, tuple);
	}

	void process(event e) {
		each(_states, [&](auto& state) {
			if (state._id._name == _state._name) {
				each(state._transitions, [&](auto& transition) {
					if ((transition._event._name == e._name) && (transition._guard())) {
						// TODO leave the function now, or else more then one transitions may happen
						state._on_exit();
						transition._action();
						_state = transition._to._name;
						each(_states, [&](auto& state_new) {
							if(state_new._id._name == _state._name) {
								state_new._on_enter();
							}
						});
					}
				});
			}
		});
	}
};

struct player {
	struct events {
		static constexpr auto run = event("run");
		static constexpr auto pause = event("pause");
		static constexpr auto continue_ = event("continue");
		static constexpr auto issue = event("issue");
		static constexpr auto shutdown = event("shutdown");
	};

	struct states {
		static constexpr auto off = stateid{"off"};
		static constexpr auto on = stateid{"on"};
		static constexpr auto running = stateid{"running"};
		static constexpr auto paused = stateid{"paused"};
		static constexpr auto error = stateid{"error"};
	};

	int _counter = 0;

	void action_enter_running() {
		std::cout << "Action Enter running" << std::endl;
	}

	bool guard_is_ready() {
		_counter += 1;
		//std::cout << "Guard Is Ready " << _counter << std::endl;
		return _counter >= 5;
	}

	constexpr auto make() {
		return std::tuple(
			state(states::off).transitions(std::tuple(
				transition().guard([this]{ return guard_is_ready(); }).on(events::run).to(states::running)
			)),
			state(states::running).on_enter([this]{ action_enter_running(); }).transitions(std::tuple(
				transition().on(events::pause).to(states::paused),
				transition().on(events::issue).to(states::error),
				transition().on(events::shutdown).to(states::off)
			)),
			state(states::paused, no_action, no_action, std::tuple(
				transition().on(events::shutdown).to(states::off),
				transition().on(events::continue_).to(states::running)
			)),
			state(states::error, no_action, no_action, std::tuple(
				transition().on(events::shutdown).to(states::off)
			))
		);
	}
};

int main() {
	std::cout << "State Machine" << std::endl;

	machine<player> p1;
	for (int i = 0; i < 7; i += 1) {
		std::cout << p1.current() << std::endl;
		p1.process(player::events::run);
	}
	std::cout << p1.current() << std::endl;
	p1.process(player::events::pause);
	std::cout << p1.current() << std::endl;
	p1.process(player::events::continue_);
	std::cout << p1.current() << std::endl;
	p1.process(player::events::issue);
	std::cout << p1.current() << std::endl;
	p1.process(player::events::shutdown);
	std::cout << p1.current() << std::endl;
	
	std::cout << to_mermaid<player>() << std::endl;
}
