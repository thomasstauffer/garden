
- The state machine resembles a trivialized player for something not be defined here (video, audio, ...)
- SCXML can be read/edited by QtDesigner (with the SCXML extension enabled)
- some issues/notes/nice to knows which applies at least up to C++17:
-- constexpr does not work together with type erasure (e.g. std::function does not work)
-- auto does not work for a non static class member
