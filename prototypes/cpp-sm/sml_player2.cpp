#include <boost/sml.hpp>
#include <iostream>

namespace sml = boost::sml;

class my_sm {
public:
	auto operator()() {
		using namespace sml;

		auto guard = [&] {
			i += 1;
			return i > 1;
		};
		auto action = [] { std::cout << "action:enter_running" << std::endl; };

		return make_transition_table(
			*"Off"_s + "run"_e[guard] = "Running"_s,
			"Running"_s + "pause"_e = "Paused"_s,
			"Paused"_s + "continue"_e = "Running"_s,
			"Running"_s + "issue"_e = "Error"_s,
			"Error"_s + "shutdown"_e = "Off"_s,
			"Paused"_s + "shutdown"_e = "Off"_s,
			"Running"_s + "shutdown"_e = "Off"_s,
			"Running"_s + sml::on_entry<_> / action
		);
	}

	int i = 0;
};

void print_state(sml::sm<my_sm>& sm) {
	sm.visit_current_states([](auto state) { std::cout << "state:" << state.c_str() << std::endl; });
}

int main() {
	using namespace sml;

	sm<my_sm> sm;

	print_state(sm);
	sm.process_event("run"_e());
	print_state(sm);
	sm.process_event("run"_e());
	print_state(sm);
	sm.process_event("pause"_e());
	print_state(sm);
	sm.process_event("continue"_e());
	print_state(sm);
	sm.process_event("issue"_e());
	print_state(sm);
	sm.process_event("shutdown"_e());
	print_state(sm);

	return 0;
}
