/*
Compared to main3, this one tries to implement everything with constexpr. It
also uses std::array and would have the same problem as main3 and just avoid
them because everything is static (where auto is allowed). The idea (which may
no work out) is that the definition/model of the state machine is static, and
separate class is created based on the definition/model which then may contain
instance variables.

Advantage:

- Quite readable currently

Disadvantages:

- Still O(n) lookup time
- actions and guards cannot access any member variables and currently also do
  not support a context, which would be a work around for member variables. If a
  context is added the constexpr variant would only support guards/actions which
  only context as a const, therefore it may only be usefull if they also can
  return a context.

TODO:

- pass context to to actions and guards

*/

#include <array>
#include <iostream>

using czstring = const char*;
using action_callable = void (*)();
using guard_callable = bool (*)();

constexpr void action_none() {
}

constexpr bool guard_true() {
	return true;
}

struct event {
	const czstring _name;
	constexpr event(czstring name) : _name(name) {
	}
};

struct state {
	const czstring _name;
	const action_callable _on_enter;
	const action_callable _on_exit;

	constexpr state(czstring name, action_callable on_enter, action_callable on_exit) : _name(name), _on_enter(on_enter), _on_exit(on_exit) {
	}
	constexpr state(czstring name) : state(name, action_none, action_none) {
	}

	constexpr state on_enter(action_callable action) {
		return state(_name, action, _on_exit);
	}

	constexpr state on_exit(action_callable action) {
		return state(_name, _on_enter, action);
	}
};

static constexpr auto event_none = event("none");
static constexpr auto state_none = state("none");

struct transition {
	const state& _from;
	const event& _on;
	const state& _to;
	const action_callable _action;
	const guard_callable _guard;

	constexpr transition(const state& from, const event& on, const state& to, guard_callable guard, action_callable action) : _from(from), _on(on), _to(to), _action(action), _guard(guard) {
	}

	constexpr transition() : transition(state_none, event_none, state_none, guard_true, action_none) {
	}

	constexpr transition from(const state& s) {
		return transition(s, _on, _to, _guard, _action);
	}

	constexpr transition on(const event& e) {
		return transition(_from, e, _to, _guard, _action);
	}

	constexpr transition to(const state& s) {
		return transition(_from, _on, s, _guard, _action);
	}

	constexpr transition guard(guard_callable guard) {
		return transition(_from, _on, _to, guard, _action);
	}

	constexpr transition action(action_callable action) {
		return transition(_from, _on, _to, _guard, action);
	}
};

template <typename... T>
constexpr auto make_transitions(T... args) -> std::array<transition, sizeof...(args)> {
	return std::array<transition, sizeof...(args)>{args...};
}

template <typename Context>
struct sm {
	const transition* _transitions;
	state const* _state;
	const size_t _size;
	const Context& _context;

	template <size_t N>
	sm(const std::array<transition, N>& transitions, const Context& context) : _transitions(&transitions[0]), _state(&transitions[0]._from), _size(N), _context(context){};

	void process(const event& e) {
		for (size_t i = 0; i < _size; ++i) {
			if ((&_transitions[i]._from == _state) and (&_transitions[i]._on == &e)) {
				if (_transitions[i]._guard()) {
					_state->_on_exit();
					_transitions[i]._action();
					_state = &_transitions[i]._to;
					_state->_on_enter();
					return;
				}
			}
		}
	}
};

template <typename T>
struct sm_constexpr {
	const T& _transitions;
	const state& _state;
	constexpr sm_constexpr(const T& transitions, const state& s) : _transitions(transitions), _state(s) {
	}

	constexpr sm_constexpr process(const event& e) const {
		for (size_t i = 0; i < _transitions.size(); ++i) {
			if ((&_transitions[i]._from == &_state) and (&_transitions[i]._on == &e)) {
				if (_transitions[i]._guard()) {
					_transitions[i]._action();
					return sm_constexpr(_transitions, _transitions[i]._to);
				}
			}
		}
		return sm_constexpr(_transitions, _state);
	}
};

struct my_sm_model {
	// model

	static constexpr auto event_run = event("event_run");
	static constexpr auto event_issue = event("event_issue");
	static constexpr auto event_pause = event("event_pause");
	static constexpr auto event_shutdown = event("event_shutdown");
	static constexpr auto event_continue = event("event_continue");

	static void action_enter_running() {
		std::cout << "action:enter_running" << std::endl;
	}

	static constexpr auto state_off = state("state_off");
	static constexpr auto state_running = state("state_running").on_enter(action_enter_running);
	static constexpr auto state_error = state("state_error");
	static constexpr auto state_paused = state("state_paused");

	static bool guard_running() {
		static int i = 0; // unacceptable, should be provided as context
		i += 1;
		return i > 1;
	}

	static constexpr auto t1 = transition().from(state_off).on(event_run).to(state_running).guard(guard_running);
	static constexpr auto t2 = transition().from(state_running).on(event_pause).to(state_paused);
	static constexpr auto t3 = transition().from(state_paused).on(event_continue).to(state_running);
	static constexpr auto t4 = transition().from(state_running).on(event_issue).to(state_error);
	static constexpr auto t5 = transition().from(state_error).on(event_shutdown).to(state_off);
	static constexpr auto t6 = transition().from(state_paused).on(event_shutdown).to(state_off);
	static constexpr auto t7 = transition().from(state_running).on(event_shutdown).to(state_off);

	static constexpr auto transitions = make_transitions(t1, t2, t3, t4, t5, t6, t7);

	using transitions_type = decltype(transitions);

	static constexpr auto transitions_constexpr = make_transitions(transition(state_off, event_run, state_running, guard_true, action_none), transition(state_running, event_shutdown, state_off, guard_true, action_none));

	using transitions_constexpr_type = decltype(transitions_constexpr);
};

struct my_context {};

int main() {
	std::cout << "State Machine" << std::endl;

	my_context context;

	// important to not use auto in the following line as this would not allow to use it with inside a class (at least up to C++20)
	sm a_sm(my_sm_model::transitions, context);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_run);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_run);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_pause);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_continue);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_issue);
	std::cout << "state:" << a_sm._state->_name << std::endl;
	a_sm.process(my_sm_model::event_shutdown);
	std::cout << "state:" << a_sm._state->_name << std::endl;

	constexpr auto cm = sm_constexpr(my_sm_model::transitions_constexpr, my_sm_model::state_off).process(my_sm_model::event_run).process(my_sm_model::event_shutdown);
	constexpr czstring cm_state_name = cm._state._name;
	std::cout << cm_state_name << std::endl;

	return 0;
}