/*
Also uses std::array, but tries to only story the necessary transitions, inside
an instance of a class. The use of std::array makes it necessary to provide the
type and the number of elements e.g. std::array<transitions, 5>, but the
assumption that the user of a state machine library has to count the number of
transitions seems to be an ugly idea. Normally for this problem auto is very
useful, but auto does currently (2023) not work for instance variables. There
are some unofficial patches for this functionality, so maybe this is not true
anymore in the future.

This implementation does not even have the functionality to change the state.

Advantages:

- Uses less memory than main1

Disadvantages:

- Ugly to write a state machine
- O(n) or O(log(n)) time for a transition (depending if the transitions are
  sorted or not)
*/

#include <array>
#include <iostream>

using czstring = const char*;

struct state;
struct event;
struct builder;
struct transition;

template <typename Key, typename Value, std::size_t Size>
struct map {
	std::array<std::pair<Key, Value>, Size> _data;

	constexpr Value at(const Key& key) const {
		for (const auto& item : _data) {
			if (item.first == key) {
				return item.second;
			}
		}
		throw std::range_error("Not Found");
	}
};

struct state {
	int _id;
	czstring _name;

	explicit constexpr state(int id, czstring name) : _id(id), _name(name) {
	}
};

struct event {
	int _id;
	czstring _name;

	explicit constexpr event(int id, czstring name) : _id(id), _name(name) {
	}
};

struct transition {
	constexpr transition() {
	}

	explicit constexpr transition(state& from, state& to, event& event) {
	}
};

template <size_t Size>
struct statemachine {

	std::array<transition, Size> _transitions;

	void process(const event& event) {
	}
};

struct builder {
	int new_state_id = 0;
	int new_event_id = 0;

	constexpr state make_state(czstring name) {
		state s(new_state_id, name);
		new_state_id += 1;
		return s;
	}

	constexpr event make_event(czstring name) {
		event e(new_event_id, name);
		new_event_id += 1;
		return e;
	}

	template <size_t Size>
	constexpr statemachine<Size> make(const transition (&array)[Size]) const {
		statemachine<Size> sm{};
		for (size_t i = 0; i < Size; i += 1) {
			sm._transitions[i] = array[i];
		}
		return sm;
	}
};

struct demo {
	builder b;

	state s_off = b.make_state("off");
	state s_error = b.make_state("error");
	state s_running = b.make_state("running");
	state s_paused = b.make_state("paused");

	event e_run = b.make_event("run");
	event e_pause = b.make_event("pause");
	event e_continue = b.make_event("continue");
	event e_issue = b.make_event("issue");
	event e_shutdown = b.make_event("shutdown");

	transition t_1 = transition(s_off, s_running, e_run);
	transition t_2 = transition(s_running, s_paused, e_pause);
	transition t_3 = transition(s_running, s_error, e_issue);
	transition t_4 = transition(s_paused, s_running, e_continue);
	transition t_5 = transition(s_running, s_off, e_shutdown);
	transition t_6 = transition(s_paused, s_off, e_shutdown);
	transition t_7 = transition(s_error, s_off, e_shutdown);

	// ugly to provide number of transitions
	statemachine<7> s = b.make({t_1, t_2, t_3, t_4, t_5, t_6, t_7});
};

int main() {
	std::cout << "State Machine" << std::endl;

	// constexpr map<int, int, 2> m{{{{1, 1}, {2, 2}}}};

	constexpr demo a_demo;
	constexpr int new_state_id = a_demo.b.new_state_id;
	std::cout << new_state_id << std::endl;

	constexpr auto sm = a_demo.b.make({a_demo.t_1, a_demo.t_2, a_demo.t_3, a_demo.t_4, a_demo.t_5, a_demo.t_6, a_demo.t_7});
	constexpr int sm_size = sm._transitions.size();
	std::cout << sm_size << std::endl;
}
