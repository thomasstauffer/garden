#!/usr/bin/env python3

class State:
    def __init__(self, name):
        self.name = name
        self._on_enter = lambda context: None
        self._on_exit = lambda context: None

    def on_enter(self, action):
        self._on_enter = action
        return self

    def on_exit(self, action):
        self._on_exit = action

class Event:
    def __init__(self, name):
        self.name = name

class Transition:
    def __init__(self):
        self._from = None
        self._event = None
        self._guard = lambda context: True
        self._action = lambda context: None
        self._to = None

    def from_(self, state):
        self._from = state
        return self

    def guard(self, guard):
        self._guard = guard
        return self

    def event(self, event):
        self._event = event
        return self

    def action(self, action):
        self._action = action
        return self

    def to(self, state):
        self._to = state
        return self

class Machine:
    def __init__(self, transitions, context):
        self._context = context
        self._transitions = transitions
        self._state = self._transitions[0]._from

    def current(self):
        return self._state.name

    def process(self, event):
        for t in self._transitions:
            if (self._state == t._from) and (t._event == event) and t._guard(self._context):
                self._state._on_exit(self._context)
                t._action(self._context)
                self._state = t._to
                self._state._on_enter(self._context)
                break

class Player:
    def enter_running(self):
        print('Action Enter Running', self.counter)
    
    def guard_is_ready(self):
        self.counter += 1
        return self.counter >= 5

    run = Event('run')
    pause = Event('pause')
    continue_ = Event('continue')
    issue = Event('issue')
    shutdown = Event('shutdown')

    Off = State('Off')
    Running = State('Running').on_enter(enter_running)
    Paused = State('Paused')
    Error = State('Error')

    Transitions = [
        Transition().from_(Off).guard(guard_is_ready).event(run).to(Running),
        Transition().from_(Running).event(pause).to(Paused),
        Transition().from_(Running).event(issue).to(Error),
        Transition().from_(Running).event(shutdown).to(Off),
        Transition().from_(Paused).event(shutdown).to(Off),
        Transition().from_(Paused).event(continue_).to(Running),
        Transition().from_(Error).event(shutdown).to(Off),
    ]

    def __init__(self, counter):
        self.counter = counter
        self.sm = Machine(Player.Transitions, self)

p1 = Player(0)
print('Current State', p1.sm.current())
for i in range(7):
    p1.sm.process(Player.run)
    print('Current State', p1.sm.current(), 'Counter', p1.counter)

p2 = Player(2)
print('Current State', p2.sm.current())
for i in range(7):
    p2.sm.process(Player.run)
    print('Current State', p2.sm.current(), 'Counter', p2.counter)
print('Current State', p2.sm.current())
p2.sm.process(Player.pause)
print('Current State', p2.sm.current())
p2.sm.process(Player.continue_)
print('Current State', p2.sm.current())
p2.sm.process(Player.issue)
print('Current State', p2.sm.current())
p2.sm.process(Player.shutdown)
print('Current State', p2.sm.current())
