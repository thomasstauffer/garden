#!/usr/bin/env python3

import transitions  # pip3 install transitions
import transitions.extensions

class Player:
	def __init__(self):
		self.count = 0

		state_running = transitions.State(name='Running', on_enter=self.enter_running)
		machine = transitions.Machine(self, states=['Off', 'Error', state_running, 'Paused'], initial='Off')
		machine.add_transition(trigger='run', source='Off', dest='Running', conditions=[self.is_ready])
		machine.add_transition(trigger='pause', source='Running', dest='Paused')
		machine.add_transition(trigger='continue_', source='Paused', dest='Running')
		machine.add_transition(trigger='issue', source='Running', dest='Error')
		machine.add_transition(trigger='shutdown', source='Error', dest='Off')
		machine.add_transition(trigger='shutdown', source='Paused', dest='Off')
		machine.add_transition(trigger='shutdown', source='Running', dest='Off')

	def enter_running(self):
		print("action:enter_running")

	def is_ready(self):
		self.count += 1
		return self.count > 1

class PlayerHierarchical:
	def __init__(self):
		self.count = 0

		state_running = transitions.extensions.nesting.NestedState(name='Running', on_enter=self.enter_running)
		states = ['Off', {'name': 'On', 'children': ['Error', state_running, 'Paused'], 'initial': 'Running'}]
		machine = transitions.extensions.HierarchicalMachine(self, states=states, initial='Off')
		machine.add_transition(trigger='run', source='Off', dest='On_Running', conditions=[self.is_ready])
		machine.add_transition(trigger='pause', source='On_Running', dest='On_Paused')
		machine.add_transition(trigger='continue_', source='On_Paused', dest='On_Running')
		machine.add_transition(trigger='issue', source='On_Running', dest='On_Error')
		machine.add_transition(trigger='shutdown', source='On', dest='Off')

	def enter_running(self):
		print("action:enter_running")

	def is_ready(self):
		self.count += 1
		return self.count > 1

for player in [Player(), PlayerHierarchical()]:
	print(player)
	print('state:' + player.state)
	for event in [player.run, player.run, player.pause, player.continue_, player.issue, player.shutdown]:
		# print('event:' + str(event.func))
		event()
		print('state:' + player.state)
