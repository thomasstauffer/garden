/*
Compared to main1 this version uses std::vector or std::unordered_map instead of
std::array.

Advantage:

- std::map is more readable, nothing fancy at all
- can easily be put inside a class, as the types are all
- O(1) for a transition for both std::vector and std::map

Disadvantages:

- std::map is a bit too code and data hungry on embedded systems.
- heavily relies on memory allocation
- For std::vector, also as main1, uses too much memory, if std::vector would
  just store the necessary transitions the O(1) lookup would not work anymore,
  std::binary_search could at least be used O(log(N)).
- Very unlikely that the compiler optimizes everything into a big switch/case.
- Still uses std::function (else it would be impossible to use
  std::vector/std::map)
*/

#include <functional>
#include <iostream>
#include <list>

#define WITH_MAP

#ifdef WITH_MAP
#include <unordered_map>
#else
#include <vector>
#endif

using czstring = const char*;
using callable_action = std::function<void()>;
using callable_guard = std::function<bool()>;

class maker;
class event;
class state;
class machine;

class transition {
	friend maker;
	friend machine;

private:
	state* _from;
	event* _on;
	state* _to;
	callable_action _action;
	callable_guard _guard;

public:
	explicit transition() : _from(nullptr), _on(nullptr), _to(nullptr), _action(), _guard() {
	}

	transition& from(state* state) {
		_from = state;
		return *this;
	}

	transition& on(event* event) {
		_on = event;
		return *this;
	}

	transition& to(state* state) {
		_to = state;
		return *this;
	}

	transition& action(const callable_action& action) {
		_action = action;
		return *this;
	}

	transition& guard(const callable_guard& guard) {
		_guard = guard;
		return *this;
	}
};

class event {
	friend maker;
	friend machine;

private:
	czstring _name;
	uint32_t _id;

	explicit event(czstring name, uint32_t id) : _name(name), _id(id) {
	}
};

class state {
	friend maker;
	friend machine;

private:
	czstring _name;
	uint32_t _id;
	state* _parent;
	callable_action _entry;
	callable_action _exit;
#ifdef WITH_MAP
	std::unordered_map<event*, transition*> _transitions;
#else
	std::vector<transition*> _transitions;
#endif

	explicit state(czstring name, uint32_t id, state* parent) : _name(name), _id(id), _parent(parent) {
	}

public:
	state& entry(callable_action action) {
		_entry = action;
		return *this;
	}

	state& exit(callable_action action) {
		_exit = action;
		return *this;
	}
};

class machine {
public:
	state* _current;

	state* find_state(event* event) {
		state* s = _current;
#ifdef WITH_MAP
		while (s->_transitions[event] == nullptr) {
#else
		while (s->_transitions[event->_id] == nullptr) {
#endif
			if (s->_parent != nullptr) {
				s = s->_parent;
			} else {
				return nullptr;
			}
		}
		return s;
	}

	void process(event* event) {
		if (event == nullptr) {
			return;
		}
		state* s = find_state(event);
		if (s == nullptr) {
			return;
		}
#ifdef WITH_MAP
		transition& t = *s->_transitions[event];
#else
		transition& t = *s->_transitions[event->_id];
#endif
		if ((t._guard == nullptr) or (t._guard())) {
			if (t._to == _current) {
				t._action();
			} else {
				if (_current->_exit != nullptr) {
					_current->_exit();
				}
				t._action();
				std::cout << "goto " << _current->_name << std::endl;
				_current = t._to;
				if (_current->_entry != nullptr) {
					_current->_entry();
				}
			}
		}
	}
};

class maker {
public:
	std::list<event> _events;
	std::list<state> _states;
	std::list<transition> _transitions;

public:
	event* create_event(czstring name) {
		_events.push_back(event(name, _events.size()));
		return &_events.back();
	}

	state* create_state(czstring name) {
		_states.push_back(state(name, _states.size(), nullptr));
		return &_states.back();
	}

	state* create_state(czstring name, state* parent) {
		_states.push_back(state(name, _states.size(), parent));
		return &_states.back();
	}

	void operator+=(transition& transition) {
		_transitions.push_back(transition);
#ifdef WITH_MAP
		if ((transition._from != nullptr) and (transition._on != nullptr)) {
			transition._from->_transitions[transition._on] = &_transitions.back();
		}
#endif
	}

	machine create_machine(state* initial) {
		machine m;

		m._current = initial;

#ifdef WITH_MAP
#else
		for (state& state : _states) {
			state._transitions.resize(_events.size());
		}

		for (transition& transition : _transitions) {
			auto stateIt = _states.begin();
			std::advance(stateIt, transition._from->_id);
			stateIt->_transitions[transition._on->_id] = &transition;
		}
#endif

		return m;
	}
};

callable_action action_print(czstring message) {
	return [message] { std::cout << message << std::endl; };
}

bool guard_always_ok() {
	return true;
}

void action_do_nothing() {
}

int main() {
	std::cout << "State Machine" << std::endl;

	maker maker;

	auto e_run = maker.create_event("run");
	auto e_error = maker.create_event("error");
	auto e_pause = maker.create_event("pause");
	auto e_continue = maker.create_event("continue");

	auto s_root = maker.create_state("root");
	auto s_off = maker.create_state("off", s_root);
	auto s_running = maker.create_state("running", s_root);
	auto s_paused = maker.create_state("paused", s_root);
	auto s_error = maker.create_state("error", s_root);

	maker += transition().from(s_root).on(e_error).to(s_error).action(action_print("errorhappaned")).guard(guard_always_ok);
	maker += transition().from(s_off).on(e_run).to(s_running).action(action_print("startrunning")).guard(guard_always_ok);
	s_running->entry(action_print("onentry running"));
	s_running->exit(action_print("onexit running"));
	maker += transition().from(s_running).on(e_pause).to(s_paused).action(action_do_nothing).guard(guard_always_ok);
	maker += transition().from(s_paused).on(e_continue).to(s_running).action(action_do_nothing).guard(guard_always_ok);
	s_error->entry(action_print("onentry error"));

	auto machine = maker.create_machine(s_off);

	machine.process(e_run);
	machine.process(e_pause);
	machine.process(e_continue);
	machine.process(e_error);

	return 0;
}
