#include <boost/sml.hpp>
#include <iostream>

namespace sml = boost::sml;

struct stop {};
struct heat {};
struct run {};

auto event_stop = sml::event<stop>;
auto event_heat = sml::event<heat>;
auto event_run = sml::event<run>;

auto state_idle = sml::state<struct idle>;
auto state_heating = sml::state<struct heating>;
auto state_running = sml::state<struct running>;

// https://github.com/boost-experimental/sml/issues/185
// https://pdfs.semanticscholar.org/1849/3175642909909196e99b90a6af0bf3ef803b.pdf

auto guard_emergency = [] {
	std::cout << "Check Guard Emergency..." << std::endl;
	return true;
};

auto action_run = [] { std::cout << "Run..." << std::endl; };
auto action_heat = [] { std::cout << "Heat..." << std::endl; };
auto action_stop = [] { std::cout << "Stop..." << std::endl; };
auto action_emergency_stop = [] { std::cout << "Emergency Stop..." << std::endl; };

class machine_heat_run {
public:
	auto operator()() {
		using namespace sml;
		return make_transition_table(
			* state_heating + event_run / action_run = state_running
			, state_running = X
		);
	}
};

class machine_stop {
public:
	auto operator()() {
		using namespace sml;
		return make_transition_table(
			* state_idle + event_heat / action_heat = state<machine_heat_run>
			, state<machine_heat_run> + event_stop / action_stop = state_idle
		);
	}
};

class machine {
public:
	auto operator()() {
		using namespace sml;
		return make_transition_table(
			* state_idle + event_heat / action_heat = state_heating
			, state_heating + event_run / action_run = state_running
			, state_running + event_stop / action_stop = state_idle
			, state_heating + event_stop / action_stop = state_idle
		);
	}
};

class machine_2 {
public:
	auto operator()() {
		using namespace sml;
		return make_transition_table(
			* state_idle + event_run / action_run = state_running
			, state_running + event_stop / action_stop = state_idle
			, state_running [ guard_emergency ] / action_emergency_stop = state_idle
		);
	}
};

void f() {
	/*
	sml::sm<machine> a_machine;
	a_machine.process_event(heat{});
	a_machine.process_event(heat{}); // nothing happens
	a_machine.process_event(run{});
	a_machine.process_event(run{}); // nothing happens
	a_machine.process_event(stop{});
	a_machine.process_event(stop{}); // nothing happens

	a_machine.process_event(run{}); // nothing happens

	a_machine.process_event(heat{});
	a_machine.process_event(stop{});
	*/

	sml::sm<machine_stop> a_machine_3;
	a_machine_3.process_event(heat{});
	a_machine_3.process_event(run{});
	a_machine_3.process_event(stop{});
	a_machine_3.process_event(heat{});
	a_machine_3.process_event(stop{});

	sml::sm<machine_2> a_machine2;

	/*
	a_machine2.process_event(run{});
	a_machine2.process_event(stop{}); // nothing happens
	*/
}

int main() {
	f();
}
