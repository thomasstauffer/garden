
#include <iostream>
#include <tuple>
#include <algorithm>

/*
g++ -O0 player1.cpp -o player1 && ./player1
*/

using czstring = const char*;

struct event {
	czstring _name;
	constexpr event(czstring name) : _name(name) {}
};

struct stateid {
	czstring _name;
	constexpr stateid(czstring name) : _name(name) {}
};

template<typename Guard, typename Action>
struct transition {
	Guard _guard;
	event _event;
	Action _action;
	stateid _to;
	constexpr transition(Guard guard, event event, Action action, stateid to) : _guard(guard), _event(event), _action(action), _to(to) {}
};

template<typename OnEnter, typename OnExit, typename Transitions>
struct state {
	stateid _name;
	OnEnter _on_enter;
	OnExit _on_exit;
	Transitions _transitions;
	constexpr state(stateid name, OnEnter on_enter, OnExit on_exit, Transitions transitions) : _name(name), _on_enter(on_enter), _on_exit(on_exit), _transitions(transitions) {}
};

static constexpr bool always_true() {
	return true;
}

static constexpr void no_action() {}

template<typename Model>
struct machine {
	stateid _state = stateid("Off");

	using States = decltype(std::declval<Model>().make());

	Model _model;
	const States _states;

	machine() : _states(_model.make()) {}

	constexpr czstring current() const {
		return _state._name;
	}
	
	template<typename Tuple, typename Action>
	void each(Tuple tuple, Action action) {
		std::apply([&](auto&... ts) { (action(ts), ...); }, tuple);
	}

	void process(event e) {
		each(_states, [&](auto& state) {
			if (state._name._name == _state._name) {
				each(state._transitions, [&](auto& transition) {
					if ((transition._event._name == e._name) && (transition._guard())) {
						// TODO leave the function now, or else more then one transitions may happen
						state._on_exit();
						transition._action();
						_state = transition._to._name;
						each(_states, [&](auto& state_new) {
							if(state_new._name._name == _state._name) {
								state_new._on_enter();
							}
						});
					}
				});
			}
		});
	}

	/*
	czstring current() const {
		czstring result = "?";
		std::apply([this, &result](auto&... ss) {
			([this, &result](auto& s) {
				if (s._name._name == _state._name) {
					std::cout << "* ";
					result = s._name._name;
				}
				std::cout << s._name._name << std::endl;
			}(ss), ...);
		}, _states);
		return result;
	}
	*/
	
	/*
	template<size_t I = 0>
	czstring current_2() const {
		if constexpr (I < std::tuple_size<States>()) {
			if (_state == I) {
				auto state = std::get<I>(_states);
				return state._name._name;
			}
			return current_2<I + 1>();
		}
		return "?";
	}
	*/
};

struct player {
	static constexpr auto run = event{"run"};
	static constexpr auto pause = event{"pause"};
	static constexpr auto continue_ = event{"continue"};
	static constexpr auto issue = event{"issue"};
	static constexpr auto shutdown = event{"shutdown"};

	static constexpr auto Off = stateid{"Off"};
	static constexpr auto On = stateid{"On"};
	static constexpr auto Running = stateid{"Running"};
	static constexpr auto Paused = stateid{"Paused"};
	static constexpr auto Error = stateid{"Error"};

	int _counter = 0;

	void action_enter_running() {
		std::cout << "Action Enter Running " << _counter << std::endl;
	}

	bool guard_is_ready() {
		_counter += 1;
		//std::cout << "Guard Is Ready " << _counter << std::endl;
		return _counter >= 5;
	}

	constexpr auto make() {
		return std::tuple(
			state(Off, no_action, no_action, std::tuple(
				transition([this]{ return guard_is_ready(); }, run, no_action, Running)
			)),
			state(On, no_action, no_action, std::tuple()),
			state(Running, [this]{ action_enter_running(); }, no_action, std::tuple(
				transition(always_true, pause, no_action, Paused),
				transition(always_true, issue, no_action, Error),
				transition(always_true, shutdown, no_action, Off)
			)),
			state(Paused, no_action, no_action, std::tuple(
				transition(always_true, shutdown, no_action, Off),
				transition(always_true, continue_, no_action, Running)
			)),
			state(Error, no_action, no_action, std::tuple(
				transition(always_true, shutdown, no_action, Off)
			))
		);
	}
};

int main() {
	std::cout << "State Machine" << std::endl;

	machine<player> p1;
	for (int i = 0; i < 7; i += 1) {
		std::cout << p1.current() << std::endl;
		p1.process(player::run);
	}
	std::cout << p1.current() << std::endl;
	p1.process(player::pause);
	std::cout << p1.current() << std::endl;
	p1.process(player::continue_);
	std::cout << p1.current() << std::endl;
	p1.process(player::issue);
	std::cout << p1.current() << std::endl;
	p1.process(player::shutdown);
	std::cout << p1.current() << std::endl;
}
