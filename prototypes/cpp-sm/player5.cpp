/*
Almost the same as main4, but instead of using std::array, stores everything
inside types and uses a tuple for this. This allows maximum freedom, as
arbitrary types can be mixed inside the tuple. In a few tests it looked like the
compiler could unfold the state transitions in switch/case alike constructs. But
if this holds true for more complex examples needs to be shown. Because the
state machine itself is created withint a function, auto can be used very
freely.

Advantages:

- Seems the most promising related to the idea that the compiler should recreate
  a switch/case alike structure which has O(1) transition time.

Disadvantages:

- Most complicated implementation so far.

*/

#include <array>
#include <iostream>
#include <tuple>
#include <type_traits>

using czstring = const char*;

/*
template <typename... Args, typename Visitor>
void visit_tuple(const std::tuple<Args...>& tuple, Visitor&& visitor) {
	(void)std::initializer_list<void*>{(std::forward<Visitor>(visitor)(std::get<Args>(tuple)), nullptr)...};
}
*/

/*
template <typename Tuple, typename Visitor, size_t I = 0>
void visit_tuple(Tuple& tuple, Visitor visitor) {
	if constexpr (I < std::tuple_size<Tuple>()) {
		visitor(std::get<I>(tuple));
		visit_tuple<Tuple, Visitor, I + 1>(tuple, visitor);
	}
}
*/

template <typename Model>
struct fsm {
	// using Transitions = decltype(((Model*)nullptr)->make_transitions()); // nonnull warning with clang
	using Transitions = decltype(std::declval<Model>().make_transitions());

	Model _model;
	Transitions _transitions;
	int _state; // stores the last transitions, the actual state is the "to" element of this transition

	fsm() : _transitions(_model.make_transitions()), _state(0) {
	}

	template <typename TransitionFrom, typename TransitionTo, typename Event>
	bool process(TransitionFrom transition_from, TransitionTo transition_to, Event event) {
		if (std::is_same<decltype(transition_from.state_to), decltype(transition_to.state_from)>::value) {
			if (std::is_same<Event, decltype(transition_to.event)>::value) {
				if (transition_to.guard()) {
					transition_to.state_from._exit();
					transition_to.state_to._enter();
					return true;
				}
			}
		}
		return false;
	}

	template <typename TransitionFrom, typename Event, size_t ITo = 0>
	void process(TransitionFrom transition_from, Event event) {
		if constexpr (ITo < std::tuple_size<Transitions>()) {
			auto transition_to = std::get<ITo>(_transitions);
			bool result = process(transition_from, transition_to, event);
			if (result) {
				_state = ITo;
				return;
			}
			return process<TransitionFrom, Event, ITo + 1>(transition_from, event);
		}
		return;
	}

	template <typename Event, size_t IFrom = 0>
	void process(Event event) {
		if constexpr (IFrom < std::tuple_size<Transitions>()) {
			if (_state == IFrom) {
				process(std::get<IFrom>(_transitions), event);
				return;
			}
			process<Event, IFrom + 1>(event);
		}
	}

	template <size_t I = 0>
	czstring get_state() {
		if constexpr (I < std::tuple_size<Transitions>()) {
			if (_state == I) {
				auto transition = std::get<I>(_transitions);
				return transition.state_to._name;
			}
			return get_state<I + 1>();
		}
		return "?";
	}
};

void action_none() {
}

// using Action = decltype(action_none);
using Action = void (*)();

template <typename ActionEnter, typename ActionExit>
struct state {
	czstring _name;
	ActionEnter _enter;
	ActionExit _exit;
	state(czstring name, ActionEnter enter, ActionExit exit) : _name(name), _enter(enter), _exit(exit) {
	}
};

struct state_simple : public state<Action, Action> {
	state_simple(czstring name) : state(name, action_none, action_none) {
	}
};

template <typename StateFrom, typename Event, typename StateTo, typename Guard>
struct transition {
	StateFrom state_from;
	Event event;
	StateTo state_to;
	Guard guard;
};

template <typename StateFrom, typename Event, typename StateTo, typename Guard>
auto make_transition(StateFrom state_from, Event event, StateTo state_to, Guard guard) {
	return transition<StateFrom, Event, StateTo, Guard>{state_from, event, state_to, guard};
}

struct my_fsm {
	struct event_a_type {
		int a;
	};
	struct event_b_type {};

	struct event_run_type {
	} event_run;
	struct event_issue_type {
	} event_issue;
	struct event_pause_type {
	} event_pause;
	struct event_shutdown_type {
	} event_shutdown;
	struct event_continue_type {
	} event_continue;

	struct state_off_type : public state_simple {
	} state_off{{"off"}};
	struct state_running_type : public state<Action, Action> {
	} state_running{{"running", [] { std::cout << "action:enter_running" << std::endl; }, action_none}};
	struct state_error_type : public state_simple {
	} state_error{{"error"}};
	struct state_paused_type : public state_simple {
	} state_paused{{"paused"}};

	auto make_transitions() {
		auto guard_true = [] { return true; };

		auto guard = [&] {
			_i += 1;
			return _i > 1;
		};

		// pointers/references for states are not a good idea if the struct are created inside here, they get out of context

		auto t0 = make_transition(state_off, event_shutdown_type{}, state_off, guard_true); // initial
		auto t1 = make_transition(state_off, event_run_type{}, state_running, guard);
		auto t2 = make_transition(state_running, event_pause_type{}, state_paused, guard_true);
		auto t3 = make_transition(state_paused, event_continue_type{}, state_running, guard_true);
		auto t4 = make_transition(state_running, event_issue_type{}, state_error, guard_true);
		auto t5 = make_transition(state_error, event_shutdown_type{}, state_off, guard_true);
		auto t6 = make_transition(state_paused, event_shutdown_type{}, state_off, guard_true);
		auto t7 = make_transition(state_running, event_shutdown_type{}, state_off, guard_true);

		auto ts = std::make_tuple(t0, t1, t2, t3, t4, t5, t6, t7);

		return ts;
	}

	int _i = 0;
};

int main() {
	std::cout << "State Machine" << std::endl;

	fsm<my_fsm> a_fsm;

	// std::cout << typeid(a_fsm.Transitions).name() << std::endl;

	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_run_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_run_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_pause_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_continue_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_issue_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
	a_fsm.process(my_fsm::event_shutdown_type{});
	std::cout << "state:" << a_fsm.get_state() << std::endl;
}
