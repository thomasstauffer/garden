/*
For events an enum is used. Currently the enum needs a COUNT enum, to know the
biggest value of the enum. Evaluating enums at compile would also be possible
see e.g.: https://taylorconor.com/blog/enum-reflection/.

Every state contains a std::array with a possible target state for each possible
event.

Advantage:

- Transition lookup is O(1).
- Quite simple/readable
- No virtual methods -> optimization at least easier possible (AFAIK
  devirtualization is still not done in all possible cases)

Disadvatange:

- Biggest disadvantage of this method is certainly that the more events there are,
the bigger the transitions tables get, while most of them may be unused.
- Uses std::function -> constexpr not possible

If the state transitions is O(1) it was not tested if current (2023) compilers
(with some more help like const/constexpr) are smart enough to maybe realize
that in the end everything is just a big switch/case
*/

#include <array>
#include <functional>
#include <iostream>
#include <set>

using callable_action = std::function<void()>;
using callable_guard = std::function<bool()>;
using czstring = const char*;

enum class events : uint8_t { run = 0, error, pause, continuing, shutdown, COUNT };
#define NUM_EVENTS (int)(events::COUNT)

struct state {
	czstring _name;
	const state* _parent;
	callable_action _entry;
	callable_action _do;
	callable_action _exit;

	struct transition {
		state* destination;
		callable_action action;
		callable_guard guard;
	};

	std::array<transition, NUM_EVENTS> _transitions;

	explicit state(czstring name) : _name(name), _parent(nullptr) {
		init();
	}

	explicit state(czstring name, state& parent) : _name(name), _parent(&parent) {
		init();
	}

	void init() {
		for (uint32_t i = 0; i < NUM_EVENTS; i += 1) {
			_transitions[i].destination = nullptr;
		}
	}

	void on_entry(callable_action action) {
		_entry = action;
	}

	void on_do(callable_action action) {
		_do = action;
	}

	void on_exit(callable_action action) {
		_exit = action;
	}

	void add_transition(events event, state& destination, callable_action action, callable_guard guard) {
		transition t{&destination, action, guard};
		_transitions[(int)event] = t;
	}

	const transition& get_transition(events event) const {
		return _transitions[(int)event];
	}
};

struct statemachine {
	state* _current;

	explicit statemachine() : _current(nullptr) {
	}

	void set_initial(state& initial) {
		_current = &initial;
	}

	const state* find_state(events event) const {
		const state* s = _current;
		while (s->get_transition(event).action == nullptr) {
			if (s->_parent != nullptr) {
				s = s->_parent;
			} else {
				return nullptr;
			}
		}
		return s;
	}

	void process(events event) {
		const state* s = find_state(event);
		if (s == nullptr) {
			return;
		}
		const state::transition& t = s->get_transition(event);
		if ((t.guard == nullptr) or (t.guard())) {
			if (t.destination == _current) {
				t.action();
			} else {
				if (_current->_exit != nullptr) {
					_current->_exit();
				}
				t.action();
				_current = t.destination;
				if (_current->_entry != nullptr) {
					_current->_entry();
				}
			}
		}
	}
};

bool guard_always_ok() {
	return true;
}

void action_nothing() {
}

void collect(std::set<const state*>& collection, const state* current) {
	if (current == nullptr) {
		return;
	}
	bool is_in = collection.find(current) != collection.end();
	if (is_in) {
		return;
	}
	collection.insert(current);
	collect(collection, current->_parent);
	for (auto t : current->_transitions) {
		collect(collection, t.destination);
	}
}

void print(state& start) {
	std::cout << "print" << std::endl;
	std::set<const state*> collection;
	collect(collection, &start);
	const state* root = nullptr;
	for (auto s : collection) {
		if (s->_parent == nullptr) {
			root = s;
		}
		std::cout << "collected " << s->_name << std::endl;
	}
	std::cout << "root " << root->_name << std::endl;
}

struct my_sm {
	int i;

	state s_root = state("root");
	state s_off = state("off", s_root);
	state s_running = state("running", s_root);
	state s_paused = state("paused", s_root);
	state s_error = state("error", s_root);

	statemachine sm = statemachine();

	my_sm() {
		auto guard_is_ready = [&]{
			i += 1;
			return i > 1;
		};

		auto action_print = []{
			std::cout << "action:onentry running" << std::endl;
		};

		i = 0;
		sm.set_initial(s_off);

		s_root.add_transition(events::error, s_error, action_nothing, guard_always_ok);
		s_off.add_transition(events::run, s_running, action_nothing, guard_is_ready);
		s_running.add_transition(events::pause, s_paused, action_nothing, guard_always_ok);
		s_running.on_entry(action_print);
		s_paused.add_transition(events::continuing, s_running, action_nothing, guard_always_ok);
		s_running.add_transition(events::shutdown, s_off, action_nothing, guard_always_ok);
		s_error.add_transition(events::shutdown, s_off, action_nothing, guard_always_ok);
		s_paused.add_transition(events::shutdown, s_off, action_nothing, guard_always_ok);
	}
};

int main() {
	std::cout << "State Machine" << std::endl;

	my_sm a_sm;

	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::run);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::run);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::pause);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::continuing);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::error);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;
	a_sm.sm.process(events::shutdown);
	std::cout << "state:" << a_sm.sm._current->_name << std::endl;

	// print(s_off);

	return 0;
}
