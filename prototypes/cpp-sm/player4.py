#!/usr/bin/env python3

class State:
    def __init__(self, name):
        self.name = name
        self._on_enter = lambda: None
        self._on_exit = lambda: None
        self._parent = None
        
    def parent(self, parent):
        self._parent = parent
        return self

    def on_enter(self, action):
        self._on_enter = action
        return self

    def on_exit(self, action):
        self._on_exit = action
        return self

class Event:
    def __init__(self, name):
        self.name = name

class Transition:
    def __init__(self):
        self._from = None
        self._event = None
        self._guard = lambda: True
        self._action = lambda: None
        self._to = None

    def from_(self, state):
        self._from = state
        return self

    def guard(self, guard):
        self._guard = guard
        return self

    def event(self, event):
        self._event = event
        return self

    def action(self, action):
        self._action = action
        return self

    def to(self, state):
        self._to = state
        return self

class Machine:
    def __init__(self, transitions, initial_state):
        self._transitions = transitions
        self._state = initial_state

    def current(self):
        return self._state.name
        
    def find_transition(self, event):
        lookup_state = self._state
        while lookup_state != None:
            for t in self._transitions:
                if (lookup_state == t._from) and (t._event == event) and t._guard():
                    return t
            lookup_state = lookup_state._parent
        return None
        
    def find_eventless_transition(self, to_state):
        for t in self._transitions:
            if (to_state == t._from) and (t._event is None) and t._guard():
                return t
        return None

    def process(self, event):
        lookup_state = self._state
        transition = self.find_transition(event)
        if transition is None:
            return
            
        transition._action()
        to_state = transition._to
        
        while True:
            t = self.find_eventless_transition(to_state)
            if t is None:
                break
            to_state = t._to

        self._state._on_exit()
        self._state = to_state
        self._state._on_enter()


class Player:
    run = Event('run')
    pause = Event('pause')
    continue_ = Event('continue_')
    issue = Event('issue')
    shutdown = Event('shutdown')

    def __init__(self, counter):
        Off = State('Off')
        On = State('On')
        Running = State('Running').on_enter(self.enter_running).parent(On)
        Paused = State('Paused').parent(On)
        Error = State('Error').parent(On)

        transitions = [
            Transition().from_(Off).guard(self.guard_is_ready).event(Player.run).to(On),
            Transition().from_(Running).event(Player.pause).to(Paused),
            Transition().from_(Running).event(Player.issue).to(Error),
            Transition().from_(Paused).event(Player.continue_).to(Running),
            Transition().from_(On).event(Player.shutdown).to(Off),
            Transition().from_(On).to(Running),
        ]

        self.counter = counter
        self.sm = Machine(transitions, Off)

    def enter_running(self):
        print('Action Enter Running', self.counter)
    
    def guard_is_ready(self):
        self.counter += 1
        return self.counter >= 5

p1 = Player(0)
print('Current State', p1.sm.current())
for i in range(7):
    p1.sm.process(Player.run)
    print('Current State', p1.sm.current(), 'Counter', p1.counter)

p2 = Player(2)
print('Current State', p2.sm.current())
for i in range(7):
    p2.sm.process(Player.run)
    print('Current State', p2.sm.current(), 'Counter', p2.counter)
print('Current State', p2.sm.current())
p2.sm.process(Player.pause)
print('Current State', p2.sm.current())
p2.sm.process(Player.continue_)
print('Current State', p2.sm.current())
p2.sm.process(Player.issue)
print('Current State', p2.sm.current())
p2.sm.process(Player.shutdown)
print('Current State', p2.sm.current())
