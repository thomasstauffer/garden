﻿using System;

class Program {
	public static async Task A() {
		for (int i = 0; i < 5; i += 1) {
 			Console.WriteLine("A");
			await Task.Delay(1000);
		}
	}

	public static async Task B() {
		for (int i = 0; i < 5; i += 1) {
			Console.WriteLine("B");
			await Task.Delay(1000);
		}
	}

	public static async Task C() {
		while (true) {
			// ugly as fu
			while (!Console.KeyAvailable) {
				await Task.Delay(100);
			}
			var consoleKeyInfo = Console.ReadKey(true);
			Console.WriteLine("Read {0}", consoleKeyInfo.Key);
		}
	}

	public static void Main(string[] args)
	{
		var task1 = A();
		var task2 = B();
		var task3 = C();

		Task.WhenAll(task1, task2).Wait();

		// TODO wait for task3 but provide a cancellationToken to task3
	}
}
