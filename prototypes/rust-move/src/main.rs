
use std::collections::HashMap;

/*
str - string slice
&str - reference to string slice
String - string on the heap
&'static str - hardcoded string slice
*/

fn map_println(map: &HashMap<&str, String>, key: &str) {
    match map.get(key) {
        Some(value) => println!("{}", value),
        None => println!("Not Found"),
    }
}

/*
// fails, argument key must live at least as long as the key inside the hashmap
fn map_set_fail(map: &mut HashMap<&str, String>, key: &str, value: String) {
    map.insert(key, value);
}
*/

fn map_set<'a>(map: &mut HashMap<&'a str, String>, key: &'a str, value: String) {
    map.insert(key, value);
}

fn main() {
    let mut map = HashMap::<&str, String>::new();
    
    map.insert("Hello", "World".to_string());

    map_println(&map, "Hello");
    map_println(&map, "Anything");
   
    map_set(&mut map, "Hello", "World 2".to_string());

    map_println(&map, "Hello");

    // fails is not a &str
    // map.insert(format!("{}{}", "Hel", "lo"), "World".to_string());
    
    let hello = format!("{}{}", "Hel", "lo");
    map.insert(&hello, "World".to_string());

    // fail, temporary is freed
    // map.insert(&format!("{}{}", "Hel", "lo"), "World".to_string());

    map_set(&mut map, "Hello", "World".to_string());

    map_println(&map, "Hello");
}
