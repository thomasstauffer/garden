
#include <chrono>
#include <iostream>

int docalc(int a, int b) {
	return (a * 5 + b * 4) / (a + b);
}

int main() {
	int n = 0;

	auto docalc2 = [](int a, int b) { return docalc(a, b); };

	auto start = std::chrono::steady_clock::now();

	for (int i = 0; i < 100'000'000; i += 1) {
		n += docalc2(i, i + 1);
	}

	auto end = std::chrono::steady_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << n << std::endl;
	std::cout << elapsed.count() << std::endl;

	return 0;
}
