
const BASE64_STRING = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

function base64encode(string) {
	let output = '';
	const stringSafe = string + '\0\0\0'; // ugly
	for (let index = 0; index < string.length; index += 3) {
		let value = 0;
		for (let i = 0; i < 3; i += 1) {
			value = value * 256 + stringSafe.charCodeAt(index + i);
		}
		const n = Math.min(string.length - index, 3);
		for (let i = 0; i < 4; i += 1) {
			output += i <= n ? BASE64_STRING[Math.floor(value / Math.pow(64, 3 - i)) % 64] : '=';
		}
	}
	return output;
}

function base64decode(string) {
	let output = '';
	for (let index = 0; index < string.length; index += 4) {
		let value = 0;
		let n = 3; // ugly
		for (let i = 0; i < 4; i += 1) {
			n -= string[index + i] == '=' ? 1 : 0;
			value = (value * 64) + (string[index + i] == '=' ? 0 : BASE64_STRING.indexOf(string[index + i]));
		}
		for (let i = 0; i < n; i += 1) {
			output += String.fromCharCode(Math.floor(value / Math.pow(256, 2 - i)) % 256);
		}
	}
	return output;
}

console.assert('Hello World' === base64decode(base64encode('Hello World')));
console.assert('HelloWorld' == base64decode(base64encode('HelloWorld')));
console.assert(base64encode('Hello World') == 'SGVsbG8gV29ybGQ=');
console.assert(base64encode('HelloWorld') == 'SGVsbG9Xb3JsZA==');
console.assert(base64decode('SGVsbG8gV29ybGQ=') == 'Hello World');
console.assert(base64decode('SGVsbG9Xb3JsZA==') == 'HelloWorld');
