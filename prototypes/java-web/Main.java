
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Main {
    private static int port = 8080;

    public static void main(String[] args) throws Exception {
        var db = new DB();
        System.out.println("http://127.0.0.1:" + port);
        var server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new IndexHandler(db));
        server.setExecutor(null);
        server.start();
    }
}

class DB {
    private List<String> _data = new ArrayList<String>();
    private Object _lock = new Object();

    void add(String value) {
        synchronized (_lock) {
            _data.add(value);
        }
    }

    List<String> data() {
        synchronized (_lock) {
            return List.copyOf(_data);
        }
    }
}

class IndexHandler implements HttpHandler {
    private DB _db = null;

    IndexHandler(DB db) {
        _db = db;
    }

    @Override
    public void handle(HttpExchange ex) throws IOException {
        var queryString = ex.getRequestURI().getRawQuery();
        if (queryString != null) {
            if (queryString.equals("add")) {
                _db.add(String.valueOf(new Random().nextInt(100)));
            }
        }
        var list = String.join(",", _db.data());
        var response = (list + "<p>Hello Java</p><a href=\"/?add\">add</a>").getBytes();
        ex.getResponseHeaders().set("Content-Type", "text/html");
        ex.sendResponseHeaders(200, response.length);
        var os = ex.getResponseBody();
        os.write(response);
        os.close();
    }
}
