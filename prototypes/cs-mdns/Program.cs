﻿using System;
using System.Threading;
using Makaretu.Dns;
using Tmds.MDns;

/*
Test setup two computers in the same WLAN both running systemd-resolved. With the following dnssd files.

/etc/systemd/dnssd/http.dnssd 

[Service]
Name=%H
Type=_http._tcp
Port=8888

The python3 package zeroconf also sees the services.

*/

class Program
{
    public static void Main(string[] args)
    {
        //MainTmds();
        MainMakaretu();
    }

    public static void MainTmds()
    {
        var serviceBrowser = new ServiceBrowser();
        serviceBrowser.ServiceAdded += (sender, e) =>
        {
            Console.WriteLine("ServiceAdded {0}", e);
        };
        serviceBrowser.ServiceRemoved += (sender, e) =>
        {
            Console.WriteLine("ServiceAdded {0}", e);
        };
        serviceBrowser.ServiceChanged += (sender, e) =>
        {
            Console.WriteLine("ServiceChanged {0}", e);
        };
        serviceBrowser.StartBrowse("_http._tcp");
        Console.WriteLine("Tmds Query Ongoing");
        // with Wireshark an answer is seen, which matches the query, but Tmds.MDns does somehow not recognize it
        Thread.Sleep(10000);
    }

    public static void MainMakaretu()
    {
        Console.WriteLine("Makaretu");
        var serviceDiscovery = new ServiceDiscovery();
        serviceDiscovery.ServiceDiscovered += (sender, serviceName) =>
        {
            Console.WriteLine("ServiceDiscovered {0}", serviceName);
        };
        serviceDiscovery.ServiceInstanceDiscovered += (sender, discoveryEvent) =>
        {
            Console.WriteLine("ServiceInstanceDiscovered {0}", discoveryEvent.ServiceInstanceName);
        };

        serviceDiscovery.QueryAllServices(); // = _services._dns-sd._udp.local
        serviceDiscovery.QueryServiceInstances("_http._tcp");
        Console.WriteLine("Makaretu Query Ongoing");
        Thread.Sleep(10000);
    }
}
