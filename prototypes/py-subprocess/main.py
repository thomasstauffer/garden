#!/usr/bin/env python3

import subprocess

PARAMS = ['tr', '?', 'X']
print('popen', PARAMS)
p = subprocess.Popen(PARAMS, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
print('poll', p.poll())
WRITE = b'1?1'
print('write', WRITE)
p.stdin.write(WRITE)
print('communicate', p.communicate())
p.wait(1.0)
print('poll', p.poll())
