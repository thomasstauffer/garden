
#include <thread>
#include <iostream>
//#include <queue>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <optional>
//#include <vector>
//#include <functional>
//#include <chrono>

//#define ENABLE_BUG_NO_LOCK // detected by -fsanitize=thread
//#define ENABLE_FUZZER

template <typename T>
class blocking_queue
{
private:
	std::mutex m_mutex;
	std::condition_variable m_condition;
	std::deque<T> m_queue; // GUARDED_BY(m_mutex);

public:
	void push(T const& value) {
		{
			std::unique_lock lock(this->m_mutex);
			m_queue.push_front(value);
		}
		this->m_condition.notify_one();
	}

	T pop() {
		// TODO ifdef to not use lock and use thread sanitizer from clang to detect it?
		// TODO timeout?

#ifdef ENABLE_BUG_NO_LOCK
		while(this->m_queue.empty()) {};
#else
		std::unique_lock lock(this->m_mutex);
		this->m_condition.wait(lock, [=]{ return !this->m_queue.empty(); });
#endif

		//T rc(std::move(this->m_queue.back()));
		T result = this->m_queue.back(); // as fast a move ctor?
		this->m_queue.pop_back();
		return result;
	}
};

struct mymessage {
	int32_t id;
	std::string text;
};

uint32_t g_count;
blocking_queue<std::optional<const mymessage>> g_print_queue;

void print_task() {
	while(true) {
		std::optional<const mymessage> msgopt = g_print_queue.pop();
		if(msgopt.has_value()) {
			const mymessage msg = msgopt.value();
			//auto msg = std::move(msgopt.value());
			std::cout << "Task " << msg.id << " " << msg.text << "\n";
		} else {
			return;
		}
	}
}

void task(int32_t id, const std::string& message) {
	mymessage msg1 = {.id = id, .text = "Hello"};
	mymessage msg2 = {.id = id, .text = message};
	mymessage msg3 = {.id = id, .text = "Adios"};

	{
		g_print_queue.push(msg1);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		g_print_queue.push(msg2);
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		g_print_queue.push(msg3);
	}

#if 0
	// outputs get mixed
	std::cout << "Task " << id << " Hello\n";
	g_count += 1;
	std::cout << "Task " << id << " " << message << "\n";
	std::cout << "Task " << id << " Adios\n";
#endif
}

int32_t average(int32_t a, int32_t b) {
	return (a + b) / 2; // detected by -fsanitize=undefined
}

int vulnerable(const char *arg) {
	if((arg[0] == 't') && (arg[1] == 'e') && (arg[2] == 's') && (arg[3] == 't') && (arg[4] == 'm') && (arg[5] == 'e')) {
		if(arg[6] == 0) {
			return 0;
		} else {
			int** z = (int**)((void*)(arg+4));
			return **z;
		}
	}
	return 0;
}

// -fsanitize=fuzzer
extern "C" int LLVMFuzzerTestOneInput(const uint8_t* data, size_t size) {
	vulnerable((const char*) data);
	return 0;
}

#ifndef ENABLE_FUZZER

int main() {
	std::cout << "Hello\n";

	std::cout << "Average " << average(2000000000, 2000000000) << "\n";

	std::thread tprint(print_task);
	std::thread t1(task, 1, "Hi");
	std::thread t2(task, 2, "Ho");
	t1.join();
	t2.join();
	// wait a little bit until we send the "exit" message to the printing task
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	g_print_queue.push(std::nullopt);
	tprint.join();

	std::cout << "Adios\n";
}

#endif
