
#include <array>
#include <iostream>
#include <string>

template <typename T, int N>
class circular_array {
public:
	using array_type = std::array<T, N>;

	circular_array() : _index(0) {
	}

	void append(const T& value) {
		_array[_index] = value;
		_index = (_index + 1) % N;
	}

	class iterator {
	public:
		explicit iterator(int index, array_type& array) : _index(index), _array(array) {
		}

		void operator++() {
			_index += 1;
		}

		bool operator!=(const iterator& other) const {
			return this->_index != other._index;
		}

		T& operator*() const {
			return _array[_index % N];
		}

	private:
		int _index;
		array_type& _array;
	};

	iterator begin() {
		return iterator(_index, this->_array);
	}

	iterator end() {
		return iterator(_index + N, this->_array);
	}

private:
	array_type _array;
	uint32_t _index;
};

using czstring = const char*;

std::string replaceAll(const std::string& s, const std::string& from, const std::string& to) {
	std::string str(s);
	if (from.empty()) {
		return str;
	}
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
	return str;
}

class log_entry {
public:
	enum class type { none, an_int32, an_float };

	union fields {
		float float_value;
		int32_t int32_value;
	};

	czstring _text;
	type _type;
	fields _field;

	explicit log_entry() : _text("-"), _type(type::none) {
	}

	explicit log_entry(czstring text) : _text(text), _type(type::none) {
	}

	explicit log_entry(czstring text, float value) : _text(text), _type(type::an_float), _field{.float_value = value} {
	}

	explicit log_entry(czstring text, int32_t value) : _text(text), _type(type::an_int32), _field{.int32_value = value} {
	}

	std::string as_string() const {
		switch (_type) {
		default:
		case type::none:
			return _text;
		case type::an_int32:
			return replaceAll(_text, "{}", std::to_string(_field.int32_value));
		case type::an_float:
			return replaceAll(_text, "{}", std::to_string(_field.float_value));
		}
	}
};

int main() {
	circular_array<log_entry, 10> circular;

	std::cout << "size log_entry " << sizeof(log_entry) << std::endl;
	std::cout << "size circular " << sizeof(circular) << std::endl;

	circular.append(log_entry("hello world"));
	circular.append(log_entry("int {} value", 123));
	circular.append(log_entry("float {} value", 3.14f));

	for (uint32_t i = 0; i < 12; i += 1) {
		circular.append(log_entry("fill"));
	}
	circular.append(log_entry("float e {}", 2.71f));
	circular.append(log_entry("int 4 {}", 4));

	for (const auto& e : circular) {
		std::cout << e._text << " | " << e.as_string() << std::endl;
	}
}
