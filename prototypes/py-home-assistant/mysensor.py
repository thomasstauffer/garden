#!/usr/bin/env python3

import flask # pip3 install flask
import json
import random

api = flask.Flask(__name__)

@api.route('/sensor', methods=['GET'])
def get_sensor():
	return json.dumps({'co2': random.randint(400, 500), 't': random.randint(18, 24), 'rh': random.randint(40, 60)})
	
if __name__ == '__main__':
	api.run()
