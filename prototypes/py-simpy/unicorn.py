#!/usr/bin/env python3

import simpy
import matplotlib.pyplot as plt

env = simpy.Environment()


class Storage:
	def __init__(self, name, initial, restock_below, refill):
		self.name = name
		self.container = simpy.Container(env, init=initial)
		self.restock_below = restock_below
		self.refill = refill

	def _restock(self):
		if self.container.level < self.restock_below:
			yield env.timeout(30)
			self.container.put(self.refill)

	def get(self, amount):
		env.process(self._restock())
		return self.container.get(amount)


class Factory():
	def __init__(self):
		self.time_starts = []
		self.assembly_times = []
		self.head_levels = []
		self.body_levels = []
		self.tail_levels = []
		self.leg_levels = []

	def _assemble(self):
		head_storage = Storage('head', initial=0, restock_below=3, refill=4)
		body_storage = Storage('body', initial=0, restock_below=2, refill=5)
		tail_storage = Storage('tail', initial=0, restock_below=3, refill=5)
		leg_storage = Storage('leg', initial=0, restock_below=10, refill=8)
		count = 0

		while True:
			time_start = env.now

			head_level = head_storage.container.level
			body_level = body_storage.container.level
			tail_level = tail_storage.container.level
			leg_level = leg_storage.container.level

			head = head_storage.get(1)
			body = body_storage.get(1)
			tail = tail_storage.get(1)
			legs = leg_storage.get(4)

			yield head & body & tail & legs
			yield env.timeout(10)  # assembly time

			time_stop = env.now

			self.time_starts.append(time_start)
			self.assembly_times.append(time_stop - time_start)
			self.head_levels.append(head_level)
			self.body_levels.append(body_level)
			self.tail_levels.append(tail_level)
			self.leg_levels.append(leg_level)

			count += 1
			print('unicorn finished', time_start, time_stop, count)

	def run(self):
		env.process(self._assemble())
		env.run(1000)

	def analyze(self):
		plt.figure(num=None, figsize=(18, 8))
		plt.plot(self.time_starts, self.head_levels, label='heads')
		plt.plot(self.time_starts, self.body_levels, label='bodys')
		plt.plot(self.time_starts, self.tail_levels, label='tails')
		plt.plot(self.time_starts, self.leg_levels, label='legs')
		plt.plot(self.time_starts, self.assembly_times, label='assembly time')
		plt.xlabel('time')
		plt.legend()
		plt.show()

factory = Factory()
factory.run()
factory.analyze()
