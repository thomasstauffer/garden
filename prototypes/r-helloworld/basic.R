
# Variables

x <- 10
y <- 2
y <- 3

# Math

x * y
x + y
x / y
x %/% y

# Vector

1:10
sum(1:10)
1:10 |> sum()
exp(1:10)
p1 <- c(11, 12, 34, 36)
p1
p2 <- c(27, 31, 31, 42)
p2
win <- p1 > p2
which(win)
p1[win]
p2[win]
sum(win)
any(win)
all(win)
length(p1)

# String

s <- "Hello World"
nchar(s)

# Matrix

m <- matrix(c(2, 3, 4, 5, 6, 7), ncol=2, byrow=TRUE)
m
m[1,]
m[,1]
m[1,1]
m[1,2]

n <- matrix(c(1, 0, 1, 0, 1, 1), ncol=3, byrow=TRUE)
n

m %*% n

# Functions

gcd <- function(a, b) {
  while (b != 0) {
    t <- b
    b <- a %% b
    a <- t
  }
  return(a)
}

gcd(7*13, 3*13)
