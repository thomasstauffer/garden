
library(ggplot2)

d = 0:10
df <- data.frame(x = d, y = d ^ 2)

ggplot(df, aes(x = x, y = y)) + geom_point() + geom_line()
ggsave("plot.png")

browseURL("plot.png")  
