import {default as yoga} from 'yoga-layout-prebuilt';

const root = yoga.Node.create();
root.setWidth(500);
root.setHeight(300);

const node1 = yoga.Node.create();
node1.setWidth(100);
node1.setHeight(100);

const node2 = yoga.Node.create();
node2.setWidth(100);
node2.setHeight(100);

root.insertChild(node1, 0);
root.insertChild(node2, 1);

root.calculateLayout(0, 0, yoga.DIRECTION_LTR);

console.log(root.getComputedLayout());
console.log(node1.getComputedLayout());
console.log(node2.getComputedLayout());
