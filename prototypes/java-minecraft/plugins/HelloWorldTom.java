import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public final class HelloWorldTom extends JavaPlugin {
	@Override
	public void onEnable() {
		this.getLogger().info("Hello");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("Goodbye");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("dub")) {
			getLogger().info("dub");
			//Bukkit.broadcastMessage("dub dubi dub");
			for (Player player : Bukkit.getServer().getOnlinePlayers()) {
				var item = player.getItemInHand();
				item.setAmount(item.getAmount() * 2);
			}			
			return true;
		}
		return false; 
	}	
}
