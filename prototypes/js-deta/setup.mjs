
import deta from 'deta'; // npm install deta
import dotenv from 'dotenv'; // npm install dotenv
import process from 'process';

dotenv.config()

const project = deta.Deta(process.env.SECRET);
const db = project.Base('test');

await db.put({ displaedName: 'Lisa', badgeStorySubmitted: true });
await db.put({ displaedName: 'Bart', badgeStorySubmitted: false });

const { items } = await db.fetch();
items.forEach(async (item) => {
	console.log(item)
})
