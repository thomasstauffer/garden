#!/usr/bin/env python3

SAMPLERATE = 10

class SignalNode:
	def __init__(self):
		self.connected = []

	def connect(self, node):
		self.connected.append(node)
		return node

	def sample(self, value):
		self.emit(self.process(value))

	def process(self, value):
		return value

	def emit(self, value):
		for node in self.connected:
			node.sample(value)

class PrintNode(SignalNode):
	def __init__(self, enable=True):
		super().__init__()
		self.enable = enable

	def process(self, value):
		if self.enable:
			print(value)
		return value

class CollectNode(SignalNode):
	def __init__(self):
		super().__init__()
		self.collection = []

	def process(self, value):
		self.collection.append(value)
		return value

class GainNode(SignalNode):
	def __init__(self, gain):
		super().__init__()
		self.gain = gain

	def process(self, value):
		return value * self.gain

class SquareNode(SignalNode):
	def __init__(self, frequency):
		super().__init__()
		self.frequency = frequency
		self._n = 0

	def generate(self):
		period = int(SAMPLERATE / self.frequency)
		if (self._n % period) < (period / 2):
			output = 0
		else:
			output = 1
		self.emit(output)
		self._n += 1

class DelayNode(SignalNode):
	def __init__(self, delayTime):
		super().__init__()
		self.delayTime = delayTime

class AverageNode(SignalNode):
	def __init__(self, delayTime):
		super().__init__()

class GainGainNode(SignalNode):
	def __init__(self, gain1, gain2):
		super().__init__()
		self.gain1Node = GainNode(gain1)
		self.gain2Node = GainNode(gain2)
		#self.gain1Node.connect(gain2Node)

	def process(self, value):
		value = self.gain1Node.process(value)
		value = self.gain2Node.process(value)
		return value

def main():
	sin = SquareNode(2.0)
	print1 = PrintNode(False)
	gain1 = GainNode(2)
	gain2 = GainNode(3)
	print2 = PrintNode(False)
	gg = GainGainNode(4, 5)
	c1 = CollectNode()
	c2 = CollectNode()

	sin.connect(print1).connect(c1).connect(gain1).connect(gain2).connect(gg).connect(print2).connect(c2)

	for i in range(2 * SAMPLERATE):
		sin.generate()
	print(c1.collection, c2.collection)

	if False:
		import plotly.express as px
		fig = px.line(y=[c1.collection, c2.collection])
		fig.show()

main()
