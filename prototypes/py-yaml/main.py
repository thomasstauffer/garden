#!/usr/bin/env python3

import yaml  # pip install pyyaml
import pprint

s = open('test.yaml', 'r').read()
o = yaml.load(s, Loader=yaml.FullLoader)

pprint.pprint(o)
