
#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using czstring = const char*;
using timer_callback = std::function<void()>;

class timer_thread {
private:
	timer_callback _callback;
	std::chrono::milliseconds _milliseconds;
	std::thread _thread;

public:
	explicit timer_thread(timer_callback callback, std::chrono::milliseconds milliseconds) : _callback(callback), _milliseconds(milliseconds), _thread(&timer_thread::run, this) {
	}

private:
	void run() {
		while (true) {
			std::this_thread::sleep_for(_milliseconds);
			_callback();
		}
	}
};

class timer_manager {
public:
	class timer_coop {
	private:
		timer_callback _callback;
		std::chrono::milliseconds _milliseconds;

		friend timer_manager;

		explicit timer_coop(timer_callback callback, std::chrono::milliseconds milliseconds) : _callback(callback), _milliseconds(milliseconds) {
		}
	};

private:
	using tt = std::pair<timer_coop, std::chrono::steady_clock::time_point>;

	std::vector<tt> _timers;

	static bool compare(tt lhs, tt rhs) {
		return lhs.second < rhs.second;
	}

public:
	timer_coop create(timer_callback callback, std::chrono::milliseconds milliseconds) {
		timer_coop t(callback, milliseconds);
		_timers.push_back(tt(t, std::chrono::steady_clock::now() + t._milliseconds));
		return t;
	}

	void run(int milliseconds) {
		using namespace std::chrono;

		const auto duration = std::chrono::milliseconds(milliseconds);
		const auto start = system_clock::now();
		std::sort(_timers.begin(), _timers.end(), compare);

		while ((system_clock::now() - start) < duration) {
			auto next = _timers[0].second;
			_timers[0].second += _timers[0].first._milliseconds;
			std::sort(_timers.begin(), _timers.end(), compare);
			std::this_thread::sleep_until(next);
			_timers[0].first._callback();
		}
	}
};

int main() {
	std::cout << "Timer" << std::endl;

#if 0
	timer_thread timer1([] { std::cout << "timer 500" << std::endl; }, std::chrono::milliseconds(500));
	timer_thread timer2([] { std::cout << "timer 1000" << std::endl; }, std::chrono::milliseconds(1000));

	std::this_thread::sleep_for(std::chrono::milliseconds(4000));
#else
	timer_manager tm;

	auto timer1 = tm.create([] { std::cout << "timer 500" << std::endl; }, std::chrono::milliseconds(500));
	auto timer2 = tm.create([] { std::cout << "timer 1000" << std::endl; }, std::chrono::milliseconds(1000));

	tm.run(4000);
#endif

	return 0;
}
