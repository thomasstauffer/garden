/*

STUN Servers

https://gist.github.com/mondain/b0ec1cf5f60ae726202e

stun.solnet.ch:3478
stun.e-fon.ch:3478
stun.gmx.net:3478
stun.gmx.de:3478
stun.l.google.com:19302
*/

function sleep(milliSeconds) {
	return new Promise(resolve => setTimeout(resolve, milliSeconds));
}

function debug(...messages) {
	document.getElementById('debug').innerText += '> ' + messages.join(' ') + '\n';
}

function dcSetup(name, dc) {
	dc.onopen = (_event) => {
		debug('dc', 'onopen', name, dc.readyState);
	};
	dc.onclose = (_event) => {
		debug('dc', 'onclose', name, dc.readyState);
	};
	dc.onmessage = (event) => {
		debug('dc', 'onmessage', name, event.data);
	};
}

class Signaller {
	constructor() {
		this.listeners = [];
	}

	addEventListener(listener) {
		this.listeners.push(listener);
	}

	async send(event) {
		if (event !== null) {
			//await sleep(100);
			this.listeners.forEach(listener => listener(event));
		}
	}
}

function pcCreate(signaller, role) {
	function make(resolve) {
		const configuration = {};
		//const configuration = { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };
		const pc = new RTCPeerConnection(configuration);

		if (role === 'offerer') {
			// TODO offerer and waiter?
			pc.onicecandidate = async (event) => {
				//debug('pc', 'onicecandidate', name, event);
				await signaller.send({ type: 'candidate', who: role, data: event.candidate });
			};
			pc.onnegotiationneeded = async (_event) => {
				//debug('pc', 'onnegotiationneeded', name, event);
				const offer = await pc.createOffer();
				await pc.setLocalDescription(offer);
				await signaller.send({ type: 'offer', who: role, data: offer });
			};
			const dc = pc.createDataChannel('chat');
			resolve(dc);
		} else if (role === 'answerer') {
			pc.ondatachannel = (event) => {
				//debug('pc', 'ondatachannel', name, event);
				const dc = event.channel;
				resolve(dc);
			};
		}

		signaller.addEventListener(async (event) => {
			if (event.who === role) {
				return;
			}
			//debug('pc', 'signaller', name, event);
			if (event.type === 'offer') {
				await pc.setRemoteDescription(event.data);
				const answer = await pc.createAnswer();
				await pc.setLocalDescription(answer);
				await signaller.send({ type: 'answer', who: role, data: answer });
			} else if (event.type === 'answer') {
				await pc.setRemoteDescription(event.data);
			} else if (event.type === 'candidate') {
				await pc.addIceCandidate(event.data);
			}
		});
	}
	return new Promise(resolve => make(resolve));
}

async function main() {
	debug('js-webrtc');

	const signaller = new Signaller();
	signaller.addEventListener((_event) => {
		//debug('Signaller', 'send', event);
	});

	const pcAnswerer = pcCreate(signaller, 'answerer');
	const pcOfferer = pcCreate(signaller, 'offerer');
	const dcOfferer = await pcOfferer;
	dcSetup('offerer', dcOfferer);
	const dcAnswerer = await pcAnswerer;
	dcSetup('answerer', dcAnswerer);
	await sleep(500);
	dcAnswerer.send('Message From Answerer');
	dcOfferer.send('Message From Offerer');
}

window.addEventListener('load', main);
