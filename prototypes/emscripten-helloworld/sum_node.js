
const Module = require('./sum.js')

Module.onRuntimeInitialized = async (_) => {
	const sum = Module.cwrap('sum', 'number', ['number', 'number']);
	console.log(sum(111, 222))
	console.log(Module._sum(222, 333));
};
