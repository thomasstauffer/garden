
#include <array>
#include <chrono>
#include <condition_variable>
#include <coroutine>
#include <functional>
#include <future>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

/*

Two tasks:

collect: Collecting data from a sensor, ideally exactly every 100 milliseconds, keeps always the last few measurements

process: Asynchronously reading the sensor data, and process it

*/

template <typename T, size_t Size>
class circular_array {
public:
	void append(const T& value) {
		_array[_index % Size] = value;
		_index += 1;
	}

	const T& operator[](size_t index) const {
		return _array[(index + _index) % Size];
	}

	size_t size() const {
		return Size;
	}

private:
	size_t _index;
	std::array<T, Size> _array;
};

template <class T>
class concurrent_queue {
public:
	void enqueue(T t) {
		std::lock_guard lock(_mutex);
		_queue.push(t);
		_condition.notify_one();
	}

	T dequeue() {
		std::unique_lock lock(_mutex);
		while (_queue.empty()) {
			_condition.wait(lock);
		}
		T val = _queue.front();
		_queue.pop();
		return val;
	}

	bool is_empty() {
		std::unique_lock lock(_mutex);
		return _queue.empty();
	}

private:
	std::queue<T> _queue;
	std::mutex _mutex;
	std::condition_variable _condition;
};

double sensor_read() {
	std::this_thread::sleep_for(std::chrono::milliseconds(5)); // reading of the sensor takes time
	return 1.0;
}

using sensor_type = circular_array<double, 10>;

double sensor_calculate(const sensor_type& array) {
	double result = 0.0;
	std::this_thread::sleep_for(std::chrono::milliseconds(5)); // calculation also takes some time
	for (size_t i = 0; i < array.size(); i += 1) {
		result += array[i];
	}
	return result;
}

const double EXIT = -1.0; // use e.g. a struct or a pair with a dedicated exit flag

// Multiple Threads & Mutex = mtm

class mtm_sensor {
public:
	void measure() {
		double sensor_value = sensor_read();
		std::lock_guard lock(_mutex);
		_sensor_values.append(sensor_value);
	}

	double calculate() {
		std::lock_guard lock(_mutex);
		return sensor_calculate(_sensor_values);
	}

private:
	sensor_type _sensor_values;
	std::mutex _mutex;
};

mtm_sensor mtm_sensor_instance;

void mtm_task_collect() {
	auto time = std::chrono::steady_clock::now();
	for (uint32_t n = 0; n < 10; n += 1) {
		mtm_sensor_instance.measure();
		time += std::chrono::milliseconds(100);
		std::this_thread::sleep_until(time);
	}
}

void mtm_task_process() {
	for (uint32_t n = 0; n < 100; n += 1) {
		double result = mtm_sensor_instance.calculate();
		std::cout << result << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

void mtm_main() {
	std::cout << "mtm" << std::endl;

	std::thread thread_collect(mtm_task_collect);
	std::thread thread_process(mtm_task_process);

	thread_collect.join();
	thread_process.join();
}

// Multiple Threads & Database = mtdb

template <typename T>
class db {
public:
	template <typename Callable>
	void atomic(const Callable& callable) {
		std::scoped_lock lock(_mutex);
		callable(_data);
	}

private:
	T _data;
	std::mutex _mutex;
};

class mtdb_sensor {
public:
	void measure() {
		double sensor_value = sensor_read();
		_db_sensor.atomic([&](sensor_type& data) { data.append(sensor_value); });
	}

	double calculate() {
		double result = 0.0;
		_db_sensor.atomic([&](sensor_type& data) { result = sensor_calculate(data); });
		return result;
	}

private:
	db<sensor_type> _db_sensor;
};

mtdb_sensor mtdb_sensor_instance;

void mtdb_task_collect() {
	auto time = std::chrono::steady_clock::now();
	for (uint32_t n = 0; n < 10; n += 1) {
		mtdb_sensor_instance.measure();
		time += std::chrono::milliseconds(100);
		std::this_thread::sleep_until(time);
	}
}

void mtdb_task_process() {
	for (uint32_t n = 0; n < 100; n += 1) {
		double result = mtdb_sensor_instance.calculate();
		std::cout << result << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

void mtdb_main() {
	std::cout << "mtdb" << std::endl;

	std::thread thread_collect(mtdb_task_collect);
	std::thread thread_process(mtdb_task_process);

	thread_collect.join();
	thread_process.join();
}

// Multiple Threads & Queues = mtq

concurrent_queue<double> mtq_queue;

void mtq_task_collect() {
	auto time = std::chrono::steady_clock::now();
	for (uint32_t n = 0; n < 10; n += 1) {
		double sensor_value = sensor_read();
		mtq_queue.enqueue(sensor_value);
		time += std::chrono::milliseconds(100);
		std::this_thread::sleep_until(time);
	}
	mtq_queue.enqueue(EXIT);
}

void mtq_task_process() {
	sensor_type array;

	while (true) {
		if (not mtq_queue.is_empty()) {
			double value = mtq_queue.dequeue();
			if (value == EXIT) {
				return;
			}
			array.append(value);
		}
		double result = sensor_calculate(array);
		std::cout << result << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

void mtq_main() {
	std::cout << "mtq" << std::endl;

	std::thread thread_collect(mtq_task_collect);
	std::thread thread_process(mtq_task_process);

	thread_collect.join();
	thread_process.join();
}

// Multiple Threads & Active Object = mtao

class matao_sensor {
public:
	explicit matao_sensor() : _exit(false), _thread(&matao_sensor::_run, this) {
	}

	void measure() {
		_queue.enqueue([this]() {
			double sensor_value = sensor_read();
			_sensor_values.append(sensor_value);
		});
	}

	std::promise<double>& calculate(std::promise<double>& result) {
		// TODO helgrind reports an issue inside the promise/future
		_queue.enqueue([this, &result]() { result.set_value(sensor_calculate(_sensor_values)); });
		return result;
	}

	void exit() {
		_queue.enqueue([this]() { _exit = true; });
		_thread.join();
	}

private:
	void _run() {
		while (not _exit) {
			auto callable = _queue.dequeue();
			callable();
		}
	}

	concurrent_queue<std::function<void()>> _queue;
	bool _exit;
	sensor_type _sensor_values;
	std::thread _thread;
};

matao_sensor matao_sensor_instance;

void mtao_task_collect() {
	auto time = std::chrono::steady_clock::now();
	for (uint32_t n = 0; n < 10; n += 1) {
		matao_sensor_instance.measure();
		time += std::chrono::milliseconds(100);
		std::this_thread::sleep_until(time);
	}
}

void mtao_task_process() {
	for (uint32_t n = 0; n < 100; n += 1) {
		std::promise<double> result_promise;
		double result = matao_sensor_instance.calculate(result_promise).get_future().get();
		std::cout << result << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

void mtao_main() {
	std::cout << "mtao" << std::endl;

	std::thread thread_collect(mtao_task_collect);
	std::thread thread_process(mtao_task_process);

	thread_collect.join();
	thread_process.join();
}

// Single Thread & Scheduler = sts

uint32_t stsched_task_collect_n = 0;
sensor_type stsched_sensor_values;

std::chrono::milliseconds stsched_task_collect() {
	stsched_sensor_values.append(sensor_read());
	return std::chrono::milliseconds(100);
}

uint32_t stsched_task_process_n = 0;

std::chrono::milliseconds stsched_task_process() {
	double result = sensor_calculate(stsched_sensor_values);
	std::cout << result << std::endl;
	return std::chrono::milliseconds(10);
}

void stsched_main() {
	std::cout << "stsched" << std::endl;

	auto time = std::chrono::steady_clock::now();

	struct task_entry {
		std::function<std::chrono::milliseconds()> callable;
		std::chrono::steady_clock::time_point time;
	};

	std::array<task_entry, 2> tasks{{
		{stsched_task_collect, time + std::chrono::nanoseconds(1)},
		{stsched_task_process, time},
	}};

	while ((std::chrono::steady_clock::now() - time) < std::chrono::milliseconds(2000)) {
		std::sort(tasks.begin(), tasks.end(), [](task_entry& a, task_entry& b) { return a.time < b.time; });
		std::this_thread::sleep_until(tasks[0].time);
		tasks[0].time += tasks[0].callable();
	}
}

// Single Thread & Coroutines = stcoro

// TODO unfinished

struct stcoro_task {
	struct promise_type {
		promise_type() = default;

		stcoro_task get_return_object() {
			return {};
		}

		std::suspend_never initial_suspend() {
			return {};
		}

		std::suspend_always final_suspend() noexcept {
			return {};
		}

		void unhandled_exception() {
		}
	};
};

std::queue<std::coroutine_handle<>> stcoro_coroutine_handles;

struct stcoro_sleep {
	stcoro_sleep(std::chrono::milliseconds duration) : _duration(duration) {
	}

	constexpr bool await_ready() const noexcept {
		return false;
	}

	void await_suspend(std::coroutine_handle<> coroutine_handle) const noexcept {
		stcoro_coroutine_handles.push(coroutine_handle);
	}

	void await_resume() const noexcept {
	}

	std::chrono::milliseconds _duration;
};

stcoro_task stcoro_task_collect() {
	for (uint32_t n = 0; n < 10; n += 1) {
		std::cout << "A" << n << std::endl;
		co_await stcoro_sleep(std::chrono::milliseconds(50));
	}
}

stcoro_task stcoro_task_process() {
	for (uint32_t n = 0; n < 10; n += 1) {
		std::cout << "B" << n << std::endl;
		co_await stcoro_sleep(std::chrono::milliseconds(100));
	}
}

void stcoro_main() {
	std::cout << "stcoro" << std::endl;

	stcoro_task_collect();
	stcoro_task_process();

	while (not stcoro_coroutine_handles.empty()) {
		auto coroutine_handle = stcoro_coroutine_handles.front();
		stcoro_coroutine_handles.pop();
		coroutine_handle.resume();
	}
}

// Main

int main() {
	std::cout << "Hello Concurrency" << std::endl;

	// mtm_main();
	// mtdb_main();
	// mtq_main();
	// mtao_main();
	// stsched_main();
	stcoro_main();

	matao_sensor_instance.exit();

	return 0;
}