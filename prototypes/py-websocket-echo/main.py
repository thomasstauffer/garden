#!/usr/bin/env python3

import starlette
import starlette.applications
import uvicorn

def status(request):
	'''
	$ curl http://127.0.0.1:8000
	'''
	return starlette.responses.PlainTextResponse('Up and Alive\n')

async def ws(websocket):
	await websocket.accept()
	try:
		while True:
			text = await websocket.receive_text()
			await websocket.send_text(text)
	except starlette.websockets.WebSocketDisconnect:
		pass

routes = [
	starlette.routing.WebSocketRoute('/ws', ws),
	starlette.routing.WebSocketRoute('/', ws),
	starlette.routing.Route('/status', status),
]
app = starlette.applications.Starlette(debug=True, routes=routes)

if __name__ == '__main__':
	uvicorn.run('main:app', host='0.0.0.0', port=8000, log_level='info')
