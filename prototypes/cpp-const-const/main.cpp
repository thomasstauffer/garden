
#include <stdint.h>

typedef int32_t* pit;

typedef const int32_t* cpit;

int main() {
	pit p1 = nullptr;
	p1 = nullptr;
	*p1 = 0;

	const pit p2 = nullptr;
	//p2 = nullptr; // invalid
	*p2 = 0;
	
	cpit p3 = nullptr;
	p3 = nullptr;
	//*p3 = 0; // invalid

	const cpit p4 = nullptr;
	//p4 = nullptr;
	//*p4 = 0; // invalid
	
	int32_t* p5 = nullptr;
	p5 = nullptr;
	*p5 = 0;
		
	const int32_t* p6 = nullptr;
	p6 = nullptr;
	//*p6 = 0; // invalid

	int32_t* const p7 = nullptr;
	//p7 = nullptr; // invalid
	*p7 = 0;
	
	const int32_t* const p8 = nullptr;
	//p8 = nullptr; // invalid
	//*p8 = 0; // invalid
}
