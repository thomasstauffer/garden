#!/usr/bin/env python3

import socket

ADDRESS = ('localhost', 54321)

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
server.bind(ADDRESS)
server.listen(5)

while True:
	print('Wait ...')
	client, client_address = server.accept()
	print('Connected', client_address)
	while True:
		try:
			received = client.recv(1024)
		except Exception as e:
			print(e)
			break
		if len(received) == 0:
			print('Disconnected')
			break
		print('Received', received)
		client.send(received)
