#!/usr/bin/env python3

import socket
import select

ADDRESS = ('localhost', 54321)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDRESS)
client.setblocking(False)

ep = select.epoll()
ep.register(client.fileno(), select.EPOLLIN)

for i in range(5):
	send = b'Hello World'
	client.send(send)
	events = ep.poll(10.0)
	received = client.recv(1024)
	print('Received', received)

client.close()
ep.close()
