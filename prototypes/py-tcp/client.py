#!/usr/bin/env python3

import socket

ADDRESS = ('localhost', 54321)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDRESS)

for i in range(5):
	send = b'Hello World'
	client.send(send)
	received = client.recv(1024)
	print('Received', received)
	
client.close()
