#!/usr/bin/env python3

# 2024-04-01: tested on Acer Laptop in X11 (x11) and on console (KMSDRM)

import time
import ctypes
import math

# apt install python3-sdl2 python3-opengl

import sdl2 # https://github.com/py-sdl/py-sdl2
from OpenGL import GL
from OpenGL.GL import shaders

def print_sdl_info():
	for i in range(sdl2.SDL_GetNumVideoDrivers()):
		print('SDL Video Driver {0}: {1}'.format(i, sdl2.SDL_GetVideoDriver(i).decode()))
	print('SDL Current Video Driver: {0}'.format(sdl2.SDL_GetCurrentVideoDriver().decode()))

	render_info = sdl2.SDL_RendererInfo()
	for i in range(sdl2.SDL_GetNumRenderDrivers()):
		sdl2.SDL_GetRenderDriverInfo(i, ctypes.byref(render_info))
		print('SDL Renderer {0}: {1}'.format(i, render_info.name.decode()))

	window = sdl2.SDL_GL_GetCurrentWindow()
	renderer = sdl2.SDL_GetRenderer(window)
	sdl2.SDL_GetRendererInfo(renderer, ctypes.byref(render_info))
	print('SDL Current Renderer: {0}'.format(render_info.name.decode()))

def print_opengl_info():
	print('GL_VENDOR: {0}'.format(GL.glGetString(GL.GL_VENDOR).decode()))
	print('GL_RENDERER: {0}'.format(GL.glGetString(GL.GL_RENDERER).decode()))
	print('GL_VERSION: {0}'.format(GL.glGetString(GL.GL_VERSION).decode()))
	print('GL_SHADING_LANGUAGE_VERSION: {0}'.format(GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION).decode()))

def main_2d():
	width, height = 800, 600

	sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
	window = sdl2.SDL_CreateWindow(b'OpenGL', sdl2.SDL_WINDOWPOS_UNDEFINED, sdl2.SDL_WINDOWPOS_UNDEFINED, width, height, 0)
	renderer = sdl2.SDL_CreateRenderer(window, 0, sdl2.SDL_RENDERER_ACCELERATED)
	#renderer = sdl2.SDL_CreateRenderer(window, 1, 0)

	print_sdl_info()

	for i in range(10):
		print(i)
		sdl2.SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
		sdl2.SDL_RenderClear(renderer);

		sdl2.SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
		rect = sdl2.SDL_Rect(0, 0, width // 2, height // 2)
		sdl2.SDL_RenderFillRect(renderer, rect)

		sdl2.SDL_RenderPresent(renderer)

		sdl2.SDL_Delay(100)

	sdl2.SDL_DestroyWindow(window)
	sdl2.SDL_Quit()

VERTEX_SHADER_110 = '''
#version 110
attribute vec3 position;
uniform mat4 transform;
varying vec3 color;
void main() {
	color = position;
	gl_Position = vec4(position, 1.0) * transform;
}
'''

VERTEX_SHADER_330 = '''
#version 330 core
in vec3 position;
out vec3 pos;
uniform mat4 transform;
void main() {
	pos = position;
	gl_Position = vec4(position, 1.0) * transform;
}
'''

FRAGMENT_SHADER_110 = '''
#version 110
varying vec3 color;
void main() {
	gl_FragColor = vec4(normalize(color), 1.0);
}
'''

FRAGMENT_SHADER_330 = '''
#version 330 core
out vec4 fragColor;
in vec3 pos;
void main() {
	fragColor = vec4(normalize(pos), 1.0);
	//fragColor = vec4(0.0, 1.0, 0.0, 1.0);
}
'''

VERTEX_SHADER = {
	110: VERTEX_SHADER_110,
	330: VERTEX_SHADER_330,
}

FRAGMENT_SHADER = {
	110: FRAGMENT_SHADER_110,
	330: FRAGMENT_SHADER_330,
}

def triangle3d():
	s = 0.5
	return [
		-s, -s, 0.0,
		s, -s, 0.0,
		0.0, s, 0.0
	]

def triangle3dmany():
	s = 0.0005
	v = []
	N = 1000
	for nx in range(N):
		for ny in range(N):
			x = (nx - (N // 2)) / N
			y = (ny - (N // 2)) / N
			v += [
				-s + x, -s + y, 0.0,
				s + x, -s + y, 0.0,
				0.0 + x, s + y, 0.0
			]
	return v

def obj(filenname):
	vs = []
	ts = []

	for line in open(filenname, 'r').readlines():
		if line.startswith('v'):
			v = list(map(float, line.split(' ')[1:]))
			vs += [ v ]
		if line.startswith('f'):
			f = list(map(lambda x: int(x.split('/')[0]), line.split(' ')[1:]))
			v0 = vs[f[0]-1]
			v1 = vs[f[1]-1]
			v2 = vs[f[2]-1]
			ts += v0 + v1 + v2

	return ts

def identity():
	return [
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	]

def translation(x, y, z):
	return [
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1,
	]	

def rotx(angle):
	sa, ca = math.sin(angle), math.cos(angle)
	return [
		1, 0, 0, 0,
		0, ca, -sa, 0,
		0, sa, ca, 0,
		0, 0, 0, 1,
	]

def roty(angle):
	sa, ca = math.sin(angle), math.cos(angle)
	return [
		ca, 0, sa, 0,
		0, 1, 0, 0,
		-sa, 0, ca, 0,
		0, 0, 0, 1,
	]	

def rotz(angle):
	sa, ca = math.sin(angle), math.cos(angle)
	return [
		ca, -sa, 0, 0,
		sa, ca, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	]	

def main_3d():
	sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)

	if False:
		sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_MAJOR_VERSION, 3)
		sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_MINOR_VERSION, 3)
		sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_PROFILE_MASK, sdl2.SDL_GL_CONTEXT_PROFILE_CORE)
		sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_DOUBLEBUFFER, 1)

	width, height = 1024, 768

	window = sdl2.SDL_CreateWindow(b'OpenGL', 0, 0, width, height, sdl2.SDL_WINDOW_OPENGL)
	context = sdl2.SDL_GL_CreateContext(window)
	sdl2.SDL_CreateRenderer(window, 0, sdl2.SDL_RENDERER_ACCELERATED)

	print_sdl_info()
	print_opengl_info()

	VERSION = 330

	program = shaders.compileProgram(
		shaders.compileShader(VERTEX_SHADER[VERSION], GL.GL_VERTEX_SHADER),
		shaders.compileShader(FRAGMENT_SHADER[VERSION], GL.GL_FRAGMENT_SHADER)
	)
	GL.glUseProgram(program)

	#v = triangle3dmany()
	v = obj('horse.obj')

	vertex_positions = (len(v) * ctypes.c_float)(*v)

	vertex_positions_buffer = GL.glGenBuffers(1)
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vertex_positions_buffer)
	GL.glBufferData(GL.GL_ARRAY_BUFFER, len(vertex_positions) * 4, vertex_positions, GL.GL_STATIC_DRAW)
	
	positionLocation = GL.glGetAttribLocation(program, 'position')
	GL.glVertexAttribPointer(positionLocation, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, None)
	GL.glEnableVertexAttribArray(positionLocation)

	transformLocation = GL.glGetUniformLocation(program, 'transform')

	GL.glViewport(0, 0, width, height);
	GL.glClearColor(1.0, 0, 1.0, 1.0)
	GL.glEnable(GL.GL_DEPTH_TEST)

	t1 = time.time()
	n = 360
	for i in range(n):
		GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

		m = (16 * ctypes.c_float)(*roty(math.radians(i)))
		GL.glUniformMatrix4fv(transformLocation, 1, GL.GL_FALSE, m)

		GL.glDrawArrays(GL.GL_TRIANGLES, 0, len(vertex_positions) // 3)

		sdl2.SDL_GL_SwapWindow(window)

		# sdl2.SDL_Delay(50)
	t2 = time.time()
	print('{0:.2f} FPS'.format(n / (t2 - t1)))

	sdl2.SDL_GL_DeleteContext(context)
	sdl2.SDL_DestroyWindow(window)
	sdl2.SDL_Quit()

#main_2d()
main_3d()
