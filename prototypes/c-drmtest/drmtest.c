#include <drm.h>
#include <drm_mode.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

/*
Explains CRTCs, Planes, Encoders, Connectors, ...
https://manpages.debian.org/testing/libdrm-dev/drm-kms.7.en.html

struct drm_mode_modeinfo
https://www.kernel.org/doc/html/next/gpu/drm-uapi.html#c.drm_mode_modeinfo
*/

typedef const char* zstring;

void check_error(zstring file, int line, int code) {
	if (code != 0) {
		char msg[128];
		snprintf(msg, sizeof(msg), "%s:%i", file, line);
		perror(msg);
		exit(1);
	}
}

#define CHECK_ERROR(code) check_error(__FILE__, __LINE__, code)

#define MAX_RES 10
#define MAX_CONN 10

int main(int argc, zstring argv[]) {
	if (argc < 3) {
		printf("%s /dev/dri/card0 0\n", argv[0]);
		return 2;
	}

	printf("DRI: %s\n", argv[1]);
	int conn_selected = atoi(argv[2]);
	int mode_selected = 0;

	int fd  = open(argv[1], O_RDWR | O_CLOEXEC);

	struct drm_mode_card_res res = {0};

	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_GETRESOURCES, &res));

	uint32_t res_fbs[MAX_RES] = {0};
	uint32_t res_crtcs[MAX_RES] = {0};
	uint32_t res_connectors[MAX_RES] = {0};
	uint32_t res_encoders[MAX_RES] = {0};
	res.fb_id_ptr = (uint64_t)res_fbs;
	res.crtc_id_ptr = (uint64_t)res_crtcs;
	res.connector_id_ptr = (uint64_t)res_connectors;
	res.encoder_id_ptr = (uint64_t)res_encoders;

	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_GETRESOURCES, &res));

	printf("FB Count: %d\n", res.count_fbs);
	printf("Connector Count: %d\n", res.count_connectors);
	printf("CRTC Count: %d\n", res.count_crtcs);
	printf("Encoder Count: %d\n", res.count_encoders);

	printf("Selected Connector: %i\n", conn_selected);

	struct drm_mode_get_connector conn = {0};
	conn.connector_id = res_connectors[conn_selected];

	int conn_res = ioctl(fd, DRM_IOCTL_MODE_GETCONNECTOR, &conn);
	if (conn_res != 0) {
		printf("Not Connected\n");
		return 1;
	}

	struct drm_mode_modeinfo conn_modes[MAX_CONN] = {0};
	uint32_t conn_props[MAX_CONN] = {0};
	uint32_t conn_prop_values[MAX_CONN] = {0};
	uint32_t conn_encoders[MAX_CONN] = {0};
	conn.modes_ptr = (uint64_t)conn_modes;
	conn.props_ptr = (uint64_t)conn_props;
	conn.prop_values_ptr = (uint64_t)conn_prop_values;
	conn.encoders_ptr = (uint64_t)conn_encoders;

	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_GETCONNECTOR, &conn));

	printf("Encoders Count: %i\n", conn.count_encoders);
	printf("Modes Count: %i\n", conn.count_modes);
	printf("Encoder ID: %i\n", conn.encoder_id);

	for(int mode_index = 0; mode_index < conn.count_modes; mode_index += 1) {
		printf("\tMode: %i\n", mode_index);
		printf("\tName: %s\n", conn_modes[mode_index].name);
		printf("\tHorizontal Display Size: %i\n", conn_modes[mode_index].hdisplay);
		printf("\tVertical Display Size: %i\n", conn_modes[mode_index].vdisplay);
	}

	struct drm_mode_create_dumb create_dumb = {0};
	struct drm_mode_map_dumb map_dumb = {0};
	struct drm_mode_fb_cmd cmd_dumb = {0};

	int width = conn_modes[mode_selected].hdisplay;
	int height = conn_modes[mode_selected].vdisplay;

	create_dumb.width = width;
	create_dumb.height = height;
	create_dumb.bpp = 32;
	create_dumb.flags = 0;
	create_dumb.pitch = 0;
	create_dumb.size = 0;
	create_dumb.handle = 0;
	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_CREATE_DUMB, &create_dumb));

	cmd_dumb.width = create_dumb.width;
	cmd_dumb.height = create_dumb.height;
	cmd_dumb.bpp = create_dumb.bpp;
	cmd_dumb.pitch = create_dumb.pitch;
	cmd_dumb.depth = 24;
	cmd_dumb.handle = create_dumb.handle;
	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_ADDFB, &cmd_dumb));

	map_dumb.handle = create_dumb.handle;
	CHECK_ERROR(ioctl(fd,DRM_IOCTL_MODE_MAP_DUMB,&map_dumb));

	void* fb = mmap(0, create_dumb.size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, map_dumb.offset);

	printf("Frambuffer: %p\n", fb);

	CHECK_ERROR(ioctl(fd, DRM_IOCTL_SET_MASTER, 0));

	struct drm_mode_get_encoder enc = {0};
	enc.encoder_id = conn.encoder_id;
	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_GETENCODER, &enc));

	struct drm_mode_crtc crtc = {0};
	crtc.crtc_id = enc.crtc_id;
	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_GETCRTC, &crtc));

	crtc.fb_id = cmd_dumb.fb_id;
	crtc.set_connectors_ptr = (uint64_t)&res_connectors[conn_selected];
	crtc.count_connectors = 1;
	crtc.mode = conn_modes[0];
	crtc.mode_valid = 1;
	CHECK_ERROR(ioctl(fd, DRM_IOCTL_MODE_SETCRTC, &crtc));

	CHECK_ERROR(ioctl(fd, DRM_IOCTL_DROP_MASTER, 0));

	for (int n = 0; n < 10; n += 1) {
		int color = rand() & 0x00ffffff;
		for (int y = 0; y < height; y += 1) {
			for (int x = 0; x < width; x += 1) {
				int offset = (y * width) + x;
				*(((uint32_t*)fb)+offset) = color;
			}
		}
		usleep(500 * 1000);
	}
}
