
#include <boost/python.hpp>

char const* greet()
{
	return "Hello Boost Python";
}

BOOST_PYTHON_MODULE(tbp)
{
	using namespace boost::python;
	def("greet", greet);
}
