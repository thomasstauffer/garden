
import * as THREE from 'https://cdn.skypack.dev/three';
import { OrbitControls } from 'https://cdn.skypack.dev/three/examples/jsm/controls/OrbitControls.js';
//import * as BufferGeometryUtils from 'https://cdn.skypack.dev/three/examples/jsm/utils/BufferGeometryUtils.js';

const VERTICES_BOX_LEFT = [
	-1.0, -1.0, -1.0,
	-1.0, -1.0, 1.0,
	-1.0, 1.0, 1.0,
	-1.0, 1.0, 1.0,
	-1.0, 1.0, -1.0,
	-1.0, -1.0, -1.0,
];

const VERTICES_BOX_RIGHT = [
	1.0, 1.0, 1.0,
	1.0, -1.0, 1.0,
	1.0, -1.0, -1.0,
	1.0, -1.0, -1.0,
	1.0, 1.0, -1.0,
	1.0, 1.0, 1.0,
];

const VERTICES_BOX_BOTTOM = [
	-1.0, -1.0, -1.0,
	1.0, -1.0, -1.0,
	1.0, -1.0, 1.0,
	1.0, -1.0, 1.0,
	-1.0, -1.0, 1.0,
	-1.0, -1.0, -1.0,
];

const VERTICES_BOX_TOP = [
	1.0, 1.0, 1.0,
	1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, 1.0, 1.0,
	1.0, 1.0, 1.0,
];

const VERTICES_BOX_FRONT = [
	1.0, 1.0, -1.0,
	1.0, -1.0, -1.0,
	-1.0, -1.0, -1.0,
	-1.0, -1.0, -1.0,
	-1.0, 1.0, -1.0,
	1.0, 1.0, -1.0,
];

const VERTICES_BOX_BACK = [
	-1.0, -1.0, 1.0,
	1.0, -1.0, 1.0,
	1.0, 1.0, 1.0,
	1.0, 1.0, 1.0,
	-1.0, 1.0, 1.0,
	-1.0, -1.0, 1.0,
];

function gridCreate(sizeX, sizeY, sizeZ) {
	const grid = new Array(sizeZ);
	for (let z = 0; z < sizeZ; z += 1) {
		grid[z] = new Array(sizeY);
		for (let y = 0; y < sizeY; y += 1) {
			grid[z][y] = new Array(sizeX);
		}
	}
	return grid;
}

function gridIterate(grid, callable) {
	const sizeX = grid[0][0].length;
	const sizeY = grid[0].length;
	const sizeZ = grid.length;
	for (let z = 0; z < sizeZ; z += 1) {
		for (let y = 0; y < sizeY; y += 1) {
			for (let x = 0; x < sizeX; x += 1) {
				callable(grid, x, y, z);
			}
		}
	}
}

function createWorld(scene) {
	const materials = {
		'dirt': new THREE.Vector3(0.8, 0.5, 0.0),
		'grass': new THREE.Vector3(0.2, 0.8, 0.0),
		'stone': new THREE.Vector3(0.3, 0.3, 0.3),
	};

	const CHUNK_SIZE = 20;
	const world = gridCreate(Math.floor(100 / CHUNK_SIZE), Math.floor(50 / CHUNK_SIZE), Math.floor(100 / CHUNK_SIZE));
	gridIterate(world, (grid, x, y, z) => {
		grid[z][y][x] = {
			chunk: gridCreate(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE),
		};
	});

	function addVoxel(from, vertices, colors, pos, material) {
		const mat = materials[material];
		for (let i = 0; i < from.length; i += 3) {
			vertices.push(from[i + 0] * 0.5 + pos.x);
			vertices.push(from[i + 1] * 0.5 + pos.y);
			vertices.push(from[i + 2] * 0.5 + pos.z);
			colors.push(mat.x);
			colors.push(mat.y);
			colors.push(mat.z);
		}
	}

	gridIterate(world, (_, wx, wy, wz) => {
		const chunk = world[wz][wy][wx].chunk;
		gridIterate(chunk, (_, bx, by, bz) => {
			const x = (wx * CHUNK_SIZE) + bx;
			const y = (wy * CHUNK_SIZE) + by;
			const z = (wz * CHUNK_SIZE) + bz;
			const height = Math.floor(Math.sin(x * 0.2) * Math.cos(z * 0.2) * 5.0) + 10;
			const material = y < (height - 8) ? 'stone' : (y < (height - 4) ? 'dirt' : (y < height ? 'grass' : 'air'));
			chunk[bz][by][bx] = material;
		});
	});

	const tz = (world.length * CHUNK_SIZE) / 2.0;
	const tx = (world[0][0].length * CHUNK_SIZE) / 2.0;

	let voxelCount = 0;
	const CHUNK_SIZE_1 = CHUNK_SIZE - 1;
	gridIterate(world, (_, wx, wy, wz) => {
		let vertices = new Array();
		let colors = new Array();
		const chunk = world[wz][wy][wx].chunk;
		gridIterate(chunk, (_, bx, by, bz) => {
			const pos = new THREE.Vector3(bx, by, bz);
			const material = chunk[bz][by][bx];

			if (material === 'air') {
				return;
			}

			if ((bx == 0) || (chunk[bz][by][bx - 1] === 'air')) {
				addVoxel(VERTICES_BOX_LEFT, vertices, colors, pos, material);
			}
			if ((bx == CHUNK_SIZE_1) || (chunk[bz][by][bx + 1] === 'air')) {
				addVoxel(VERTICES_BOX_RIGHT, vertices, colors, pos, material);
			}
			if ((by == 0) || (chunk[bz][by - 1][bx] === 'air')) {
				addVoxel(VERTICES_BOX_BOTTOM, vertices, colors, pos, material);
			}
			if ((by == CHUNK_SIZE_1) || (chunk[bz][by + 1][bx] === 'air')) {
				addVoxel(VERTICES_BOX_TOP, vertices, colors, pos, material);
			}
			if ((bz == 0) || (chunk[bz - 1][by][bx] === 'air')) {
				addVoxel(VERTICES_BOX_FRONT, vertices, colors, pos, material);
			}
			if ((bz == CHUNK_SIZE_1) || (chunk[bz + 1][by][bx] === 'air')) {
				addVoxel(VERTICES_BOX_BACK, vertices, colors, pos, material);
			}
		});

		const geometry = new THREE.BufferGeometry();
		geometry.setAttribute('position', new THREE.BufferAttribute(new Float32Array(vertices), 3));
		geometry.setAttribute('color', new THREE.BufferAttribute(new Float32Array(colors), 3));
		geometry.computeVertexNormals();
		geometry.computeBoundingSphere();
		const mesh = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ vertexColors: THREE.VertexColors }));
		scene.add(mesh);
		const x = (wx * CHUNK_SIZE) - tx;
		const y = (wy * CHUNK_SIZE);
		const z = (wz * CHUNK_SIZE) - tz;
		mesh.position.set(x, y, z);
		//mesh.visible = (wx + wz) % 3 == 0;
	});

	console.log('Voxels', voxelCount);
}

function init() {
	const width = 1200;
	const height = 800;

	// Scene

	const scene = new THREE.Scene();
	scene.background = new THREE.Color(0xeeeeee);
	createWorld(scene);

	// Camera

	const camera = new THREE.PerspectiveCamera(60, width / height, 1.0, 1000.0); // fov, aspect, near, far
	camera.position.set(0.0, 20.0, 20.0);
	camera.lookAt(0.0, 0.0, 0.0);

	// Light

	var lightAmbient = new THREE.AmbientLight(0x666666);
	scene.add(lightAmbient);

	const lightSpot = new THREE.SpotLight(0xffffff);
	lightSpot.position.set(0.0, 50.0, 0.0);
	scene.add(lightSpot);

	// Renderer

	//const nodeCanvas = document.getElementById('canvas');
	const renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	//renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setSize(width, height);
	//renderer.shadowMap.enabled = true;
	//renderer.shadowMap.type = THREE.PCFSoftShadowMap;
	new OrbitControls(camera, renderer.domElement);
	document.body.appendChild(renderer.domElement);

	let timeLast = Date.now();
	let fpsAverage = 0.0;
	let frameCount = 0;

	function animate() {
		requestAnimationFrame(animate);
		if ((frameCount % 1) === 0) {
			const timeCurrent = Date.now();
			const timeDelta = (timeCurrent - timeLast) / 1000.0;
			timeLast = timeCurrent;
			if (timeDelta > 0.0) {
				const fpsCurrent = 1.0 / timeDelta;
				fpsAverage += (fpsCurrent - fpsAverage) * 0.05;
			}
			document.getElementById('stat').innerText = 'FPS:' + fpsAverage.toFixed(1) + ' MTris/Frame:' + (1e-6 * renderer.info.render.triangles).toFixed(2) + ' Calls/Frame:' + renderer.info.render.calls;
			renderer.render(scene, camera);
		}
		frameCount += 1;
	}

	animate();
}

window.addEventListener('load', init);
