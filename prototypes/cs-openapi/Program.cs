﻿using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Writers;
using CsvHelper;
using System.Globalization;

/*
https://github.com/microsoft/OpenAPI.NET
https://joshclose.github.io/CsvHelper/examples/reading/reading-by-hand/
*/

public record struct PathDefinition(string path, bool hasGet, bool hasPut);

class Program
{
    static void AddPath(OpenApiDocument document, PathDefinition pathDef)
    {
        Console.WriteLine(pathDef.path);
        var pathItem = new OpenApiPathItem();
        pathItem.Operations = new Dictionary<OperationType, OpenApiOperation>();
        if (pathDef.hasGet)
        {
            var operation = new OpenApiOperation
            {
                Description = "???",
                Responses = new OpenApiResponses
                {
                    ["200"] = new OpenApiResponse
                    {
                        Description = "OK"
                    }
                }
            };
            pathItem.Operations[OperationType.Get] = operation;
        }
        if (pathDef.hasPut)
        {
            var operation = new OpenApiOperation
            {
                Description = "???",
                Responses = new OpenApiResponses
                {
                    ["200"] = new OpenApiResponse
                    {
                        Description = "OK"
                    }
                }
            };
            pathItem.Operations[OperationType.Put] = operation;
        }

        document.Paths[pathDef.path] = pathItem;
    }

    static void Main(string[] args)
    {
        var document = new OpenApiDocument();
        document.Info = new OpenApiInfo { Version = "0.0.1", Title = "Test" };
        document.Servers.Add(new OpenApiServer { Url = "http://127.0.0.1:44444" });
        document.Paths = new OpenApiPaths();

        using (var reader = new StreamReader("api.csv"))
        using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
        {
            while (csv.Read())
            {
                var pathDefinition = new PathDefinition
                {
                    path = csv.GetField(0),
                    hasGet = csv.GetField(1) == "GET",
                    hasPut = csv.GetField(2) == "PUT"
                };
                AddPath(document, pathDefinition);
            }
        }

        using (var writer = new StreamWriter("out.json"))
        {
            var json = new OpenApiJsonWriter(writer);
            document.SerializeAsV3(json);
        }
    }
}
