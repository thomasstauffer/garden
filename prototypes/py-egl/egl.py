
import ctypes

_egl = ctypes.CDLL('libEGL.so')

EGL_SUCCESS = 0x3000
EGL_BAD_ACCESS = 0x3002
EGL_BAD_ALLOC = 0x3003
EGL_BAD_ATTRIBUTE = 0x3004
EGL_BAD_CONFIG = 0x3005
EGL_BAD_CONTEXT = 0x3006
EGL_BAD_CURRENT_SURFACE = 0x3007
EGL_BAD_DISPLAY = 0x3008
EGL_BAD_SURFACE = 0x3009
EGL_BAD_MATCH = 0x300A
EGL_BAD_PARAMETER = 0x300B
EGL_BAD_NATIVE_PIXMAP = 0x300C
EGL_BAD_NATIVE_WINDOW = 0x300D
EGL_CONTEXT_LOST = 0x300E

EGL_BIND_TO_TEXTURE_RGB = 0x3039
EGL_BIND_TO_TEXTURE_RGBA = 0x303A

EGL_ALPHA_SIZE = 0x3021
EGL_BLUE_SIZE = 0x3022
EGL_BUFFER_SIZE = 0x3020
EGL_COLOR_BUFFER_TYPE = 0x303F
EGL_CONFIG_CAVEAT = 0x3027
EGL_CONFIG_ID = 0x3028
EGL_CORE_NATIVE_ENGINE = 0x305B
EGL_DEPTH_SIZE = 0x3025
EGL_DISPLAY_SCALING = 0x2710
EGL_DONT_CARE = -1
EGL_DRAW = 0x3059
EGL_EXTENSIONS = 0x3055
EGL_GREEN_SIZE = 0x3023
EGL_HEIGHT = 0x3056
EGL_HORIZONTAL_RESOLUTION = 0x3090
EGL_LARGEST_PBUFFER = 0x3058
EGL_LEVEL = 0x3029
EGL_MAX_PBUFFER_HEIGHT = 0x302A
EGL_MAX_PBUFFER_PIXELS = 0x302B
EGL_MAX_PBUFFER_WIDTH = 0x302C
EGL_NATIVE_RENDERABLE = 0x302D
EGL_NATIVE_VISUAL_ID = 0x302E
EGL_NATIVE_VISUAL_TYPE = 0x302F
EGL_NONE = 0x3038
EGL_NON_CONFORMANT_CONFIG = 0x3051
EGL_NOT_INITIALIZED = 0x3001
EGL_NO_TEXTURE = 0x305C
EGL_OPAQUE_TYPE = 0x3024
EGL_PBUFFER_BIT = 0x0001
EGL_PIXMAP_BIT = 0x0002
EGL_READ = 0x305A
EGL_RED_SIZE = 0x3024
EGL_SAMPLES = 0x3031
EGL_SAMPLE_BUFFERS = 0x3032
EGL_SLOW_CONFIG = 0x3050
EGL_STENCIL_SIZE = 0x3026
EGL_SURFACE_TYPE = 0x3033
EGL_TRANSPARENT_BLUE_VALUE = 0x3035
EGL_TRANSPARENT_GREEN_VALUE = 0x3036
EGL_TRANSPARENT_RED_VALUE = 0x3037
EGL_TRANSPARENT_RGB = 0x3052
EGL_TRANSPARENT_TYPE = 0x3034
EGL_VENDOR = 0x3053
EGL_VERSION = 0x3054
EGL_WIDTH = 0x3057
EGL_WINDOW_BIT = 0x0004

EGL_OPENGL_ES_API = 0x30A0
EGL_OPENVG_API = 0x30A1

EGL_BACK_BUFFER = 0x3084
EGL_SINGLE_BUFFER = 0x3085

EGL_CONFORMANT = 0x3042
EGL_NON_CONFORMANT_CONFIG = 0x3051
EGL_MAX_SWAP_INTERVAL = 0x3089
EGL_MIN_SWAP_INTERVAL = 0x30BC

EGL_NO_ENGINE = 0x3098
EGL_SOFTWARE = 0x3089

EGLint = ctypes.c_int
EGLenum = ctypes.c_uint
EGLBoolean = ctypes.c_uint
EGLConfig = ctypes.c_void_p
EGLContext = ctypes.c_void_p
EGLDisplay = ctypes.c_void_p
EGLSurface = ctypes.c_void_p
EGLNativeDisplayType = ctypes.c_void_p

EGL_NO_CONTEXT = ctypes.c_void_p(0)
EGL_NO_DISPLAY = ctypes.c_void_p(0)
EGL_NO_SUFACE = ctypes.c_void_p(0)

EGL_DEFAULT_DISPLAY = ctypes.c_void_p(0)

def eglErrorString(error_code):
	error_strings = {
		EGL_SUCCESS: 'EGL_SUCCESS',
		EGL_NOT_INITIALIZED: 'EGL_NOT_INITIALIZED',
		EGL_BAD_ACCESS: 'EGL_BAD_ACCESS',
		EGL_BAD_ALLOC: 'EGL_BAD_ALLOC',
		EGL_BAD_ATTRIBUTE: 'EGL_BAD_ATTRIBUTE',
		EGL_BAD_CONFIG: 'EGL_BAD_CONFIG',
		EGL_BAD_CONTEXT: 'EGL_BAD_CONTEXT',
		EGL_BAD_CURRENT_SURFACE: 'EGL_BAD_CURRENT_SURFACE',
		EGL_BAD_DISPLAY: 'EGL_BAD_DISPLAY',
		EGL_BAD_SURFACE: 'EGL_BAD_SURFACE',
		EGL_BAD_MATCH: 'EGL_BAD_MATCH',
		EGL_BAD_PARAMETER: 'EGL_BAD_PARAMETER',
		EGL_BAD_NATIVE_PIXMAP: 'EGL_BAD_NATIVE_PIXMAP',
		EGL_BAD_NATIVE_WINDOW: 'EGL_BAD_NATIVE_WINDOW',
		EGL_CONTEXT_LOST: 'EGL_CONTEXT_LOST',
	}

	if error_code in error_strings:
		return error_strings[error_code]
	else:
		return f'Unknown EGL error code: 0x{error_code:X}'

_egl.eglGetError.argtypes = []
_egl.eglGetError.restype = EGLenum
eglGetError = _egl.eglGetError

_egl.eglInitialize.argtypes = [EGLDisplay, ctypes.POINTER(EGLint), ctypes.POINTER(EGLint)]
_egl.eglInitialize.restype = EGLBoolean
eglInitialize = _egl.eglInitialize

_egl.eglTerminate.argtypes = [EGLDisplay]
_egl.eglTerminate.restype = EGLBoolean
eglTerminate = _egl.eglTerminate

_egl.eglQueryString.argtypes = [EGLDisplay, EGLint]
_egl.eglQueryString.restype = ctypes.c_char_p
eglQueryString = _egl.eglQueryString

_egl.eglGetConfigs.argtypes = [EGLDisplay, ctypes.POINTER(EGLConfig), EGLint, ctypes.POINTER(EGLint)]
_egl.eglGetConfigs.restype = EGLBoolean
eglGetConfigs = _egl.eglGetConfigs

_egl.eglChooseConfig.argtypes = [EGLDisplay, ctypes.POINTER(EGLint), ctypes.POINTER(EGLConfig), EGLint, ctypes.POINTER(EGLint)]
_egl.eglChooseConfig.restype = EGLBoolean

eglChooseConfig = _egl.eglChooseConfig

_egl.eglGetConfigAttrib.argtypes = [EGLDisplay, EGLConfig, EGLint, ctypes.POINTER(EGLint)]
_egl.eglGetConfigAttrib.restype = EGLBoolean
eglGetConfigAttrib = _egl.eglGetConfigAttrib

_egl.eglCreateWindowSurface.argtypes = [EGLDisplay, EGLConfig, ctypes.c_void_p, ctypes.POINTER(EGLint)]
_egl.eglCreateWindowSurface.restype = EGLSurface
eglCreateWindowSurface = _egl.eglCreateWindowSurface

_egl.eglCreatePbufferSurface.argtypes = [EGLDisplay, EGLConfig, ctypes.POINTER(EGLint)]
_egl.eglCreatePbufferSurface.restype = EGLSurface
eglCreatePbufferSurface = _egl.eglCreatePbufferSurface

_egl.eglCreatePixmapSurface.argtypes = [EGLDisplay, EGLConfig, ctypes.c_void_p, ctypes.POINTER(EGLint)]
_egl.eglCreatePixmapSurface.restype = EGLSurface
eglCreatePixmapSurface = _egl.eglCreatePixmapSurface

_egl.eglDestroySurface.argtypes = [EGLDisplay, EGLSurface]
_egl.eglDestroySurface.restype = EGLBoolean
eglDestroySurface = _egl.eglDestroySurface

_egl.eglBindAPI.argtypes = [EGLenum]
_egl.eglBindAPI.restype = EGLBoolean
eglBindAPI = _egl.eglBindAPI

_egl.eglCreateContext.argtypes = [EGLDisplay, EGLConfig, EGLContext, ctypes.POINTER(EGLint)]
_egl.eglCreateContext.restype = EGLContext
eglCreateContext = _egl.eglCreateContext

_egl.eglDestroyContext.argtypes = [EGLDisplay, EGLContext]
_egl.eglDestroyContext.restype = EGLBoolean
eglDestroyContext = _egl.eglDestroyContext

_egl.eglMakeCurrent.argtypes = [EGLDisplay, EGLSurface, EGLSurface, EGLContext]
_egl.eglMakeCurrent.restype = EGLBoolean
eglMakeCurrent = _egl.eglMakeCurrent

_egl.eglSwapBuffers.argtypes = [EGLDisplay, EGLSurface]
_egl.eglSwapBuffers.restype = EGLBoolean
eglSwapBuffers = _egl.eglSwapBuffers

_egl.eglGetDisplay.argtypes = [EGLNativeDisplayType]
_egl.eglGetDisplay.restype = EGLDisplay
eglGetDisplay = _egl.eglGetDisplay

