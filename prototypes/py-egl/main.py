#!/usr/bin/env python3

# https://registry.khronos.org/EGL/sdk/docs/man/

import egl
import ctypes

def check_error():
	error_code = egl.eglGetError()
	if error_code != egl.EGL_SUCCESS:
		print(egl.eglErrorString(error_code))

check_error()
display = egl.eglGetDisplay(egl.EGL_DEFAULT_DISPLAY)
check_error()
egl.eglInitialize(display, None, None)
check_error()
egl.eglBindAPI(egl.EGL_OPENGL_ES_API)
check_error()
config_size = 256
configs = (config_size * egl.EGLConfig)()
num_configs = egl.EGLint(0)
egl.eglGetConfigs(display, configs, config_size, ctypes.byref(num_configs))
check_error()
print(num_configs)
config = configs[0]
value = egl.EGLint(0)
egl.eglGetConfigAttrib(display, config, egl.EGL_DEPTH_SIZE, ctypes.byref(value))
check_error()
print(value)
egl.eglGetConfigAttrib(display, config, egl.EGL_BUFFER_SIZE, ctypes.byref(value))
check_error()
print(value)
surface = egl.eglCreatePbufferSurface(display, config, None)
check_error()
context = egl.eglCreateContext(display, config, egl.EGL_NO_CONTEXT, None)
check_error()
egl.eglMakeCurrent(display, surface, surface, context)
check_error()

import OpenGL.GL as GL

print(GL.glGetString(GL.GL_VENDOR))
print(GL.glGetString(GL.GL_RENDERER))
print(GL.glGetString(GL.GL_VERSION))
print(GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION))
