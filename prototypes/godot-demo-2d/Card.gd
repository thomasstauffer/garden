extends Area2D

func _ready():
	pass
	
func flip_to_front():
	$AnimationPlayer.play("FlipToFront")

signal my_clicked(source)

func _input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed():
			emit_signal("my_clicked", self)

#func _input(event):
#	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
