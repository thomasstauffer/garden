extends Node2D

var players = []

func _ready():
	players = [$Player1, $Player2, $Player3]

func _on_card_clicked(card):
	card.flip_to_front()

func _on_player_clicked(player):
	for p in players:
		p.select(p == player)
