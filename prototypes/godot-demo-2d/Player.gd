extends Area2D

export var MyColor = Color(1, 0, 1)

func _ready():
	$Sprite.modulate = MyColor

func select(select: bool):
	$Particles2D.emitting = select

signal my_clicked(source)

func _input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed():
			emit_signal("my_clicked", self)
