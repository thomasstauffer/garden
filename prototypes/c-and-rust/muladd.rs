#![no_std]

#[no_mangle]
pub fn muladd(a: i32, b: i32, c: i32) -> i32 {
	return a * (b + c);
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}
