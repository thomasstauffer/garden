
#def log(msg: int) -> None: # -> mypy error
def log(msg: str) -> None:
    print(msg)


#eval('None') # -> bandit error

log('Hello')
