
method Main() {
    var add := Add(3, 1);
    var div := Div(add, 2);
    var pow := Pow(2, 8);

    print div;
    print " ";
    print pow;
    print "\n";
}

method Add(a: int, b: int) returns (result: int)
{
    result := a + b;
}

method Div(a: int, b: int) returns (result: int)
    requires b > 0
{
    result := a / b;
}

function PowFunction(base: int, exponent: int): int
    requires exponent > 0 
{
    if exponent == 1 then 
        base
    else 
        base * PowFunction(base, exponent - 1)
}

method Pow(base: int, exponent: int) returns (result: int)
    requires exponent > 0 
    requires base > 0
    ensures result == PowFunction(base, exponent)
{

    result := base;
    var e := 1;
    while (e < exponent)
        invariant 0 <= e <= exponent
        invariant result == PowFunction(base, e)
    {
        e := e + 1;
        result := result * base;
    }
}
