
#include <iostream>
#include <cstddef>

// https://pzemtsov.github.io/2016/11/06/bug-story-alignment-on-x86.html

struct a {
	uint8_t i8;
	uint16_t i16;
};

struct b {
	uint8_t i8;
	uint16_t i16;
	double f64;
};

uint8_t c[5];
uint8_t d[13];
alignas(0x10) uint8_t e[13];

int main() {
	std::cout << alignof(a) << std::endl; // 2 on x86-64
	std::cout << alignof(b) << std::endl; // 8 on x86-64
	std::cout << alignof(std::max_align_t) << std::endl; // 16 on x86-64

	std::cout << &c << std::endl;
	std::cout << &d << std::endl;
	std::cout << &e << std::endl;
}
