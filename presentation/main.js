
function setup() {
	document.addEventListener('wheel', (event) => {
		changePage(CURRENT, event.deltaY > 0.0 ? 1 : -1);
	});

	document.addEventListener('keydown', (event) => {
		if (['Home'].includes(event.key)) {
			changePage(START, 0);
		} else if (['End'].includes(event.key)) {
			changePage(END, 0);
		} else if (['Enter', ' ', 'ArrowRight', 'ArrowDown', 'PageDown', '+'].includes(event.key)) {
			const count = event.altKey || event.shiftKey ? 5 : 1;
			changePage(CURRENT, count);
			return;
		} else if (['ArrowLeft', 'ArrowUp', 'PageUp', 'Backspace', '-'].includes(event.key)) {
			const count = event.altKey || event.shiftKey ? -5 : -1;
			changePage(CURRENT, count);
		}
	});

	const pages = document.querySelectorAll('section');
	pages.forEach((title, index) => {
		console.log(index + ' ' + title.children[0].textContent);
	})

	const locationPageIndex = parseInt(new URL(window.location.href).searchParams.get('page'));
	let currentPageIndex = isNaN(locationPageIndex) ? 0 : locationPageIndex; // TODO ugly - refactor state

	const [CURRENT, START, END] = [1, 2, 3]

	function changePage(from, changeIndex) {
		const length = pages.length;

		if (from == CURRENT) {
			currentPageIndex = currentPageIndex + changeIndex;
		} else if (from === START) {
			currentPageIndex = changeIndex;
		} else if (from === END) {
			currentPageIndex = length - 1 + changeIndex;
		}

		currentPageIndex = (currentPageIndex + length) % length;

		window.history.pushState(null, '', '?page=' + currentPageIndex);
		pages.forEach((page, index) => {
			page.classList.toggle('hide', index != currentPageIndex);
		});
	}

	changePage(0, CURRENT);
}

window.addEventListener('load', setup);
