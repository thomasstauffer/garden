import Browser
import Html
import Html.Attributes
import Html.Events
import Svg
import Svg.Events
import Svg.Attributes
import Random
import Time
import Basics

main = Browser.element { init = init, update = update, subscriptions = subscriptions, view = view }

type alias Model = {time : Int, parameter : Float}

init : () -> (Model, Cmd Msg)
init _ = ({ time = 1, parameter = 5.0 }, Cmd.none)

type Msg = Tick Time.Posix | ParameterChange Float

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Tick time ->
      ({ model | time = model.time + 1 }, Cmd.none)
    ParameterChange delta ->
      ({ model | parameter = model.parameter + delta }, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model = Time.every 20 Tick

width = 600.0
height = 600.0

svgLine : (Float, Float) -> (Float, Float) -> Svg.Svg Msg
svgLine (x1, y1) (x2, y2) = Svg.line [
    Svg.Attributes.x1 (String.fromFloat x1), Svg.Attributes.y1 (String.fromFloat y1),
    Svg.Attributes.x2 (String.fromFloat x2), Svg.Attributes.y2 (String.fromFloat y2),
    Svg.Attributes.strokeWidth "2", Svg.Attributes.stroke "#f0f"
  ] []

svgCircle : Float -> (Float, Float) -> Svg.Svg Msg
svgCircle r (x, y) = Svg.circle [Svg.Attributes.cx (String.fromFloat x), Svg.Attributes.cy (String.fromFloat y),
  Svg.Attributes.r (String.fromFloat r), Svg.Attributes.fill "#f0f"] []

svgPoint : (Float, Float) -> Svg.Svg Msg
svgPoint = svgCircle 1

anglesUnitCircle : List Float

scale : Float -> (Float, Float) -> (Float, Float)

translate : Float -> (Float, Float) -> (Float, Float)

view : Model -> Html.Html Msg
view model =
  let
    svgElements = [svgPoint (200, 200)]
  in
    Html.div [] [
      Html.p [] [
        Html.text (" Parameter: " ++ String.fromFloat model.parameter ++ " "),
        Html.button [ Html.Events.onClick (ParameterChange -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (ParameterChange 1) ] [ Html.text "+" ],
        Html.text (" Time: " ++ (String.fromInt model.time))
      ],
      Svg.svg [
        Svg.Attributes.width (String.fromFloat width),
        Svg.Attributes.height (String.fromFloat height),
        Svg.Attributes.style "background-color: #eee"
      ] svgElements
    ]
