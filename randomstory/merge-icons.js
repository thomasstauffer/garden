
const fs = require('fs');
const path = require('path');

async function* walkAsync(dirName) {
	for await (const entry of await fs.promises.opendir(dirName)) {
		if (entry.isDirectory()) {
			yield* await walkAsync(path.join(dirName, entry.name));
		} else if (entry.isFile()) {
			yield [dirName, entry.name];
		}
	}
}

async function main() {
	const re = new RegExp('<path ([^>]+)>'); // carefully, only works for game-icons at the moment
	const paths = [];
	for await (const [dirName, fileName] of walkAsync('../resources')) {
		const fullName = path.join(dirName, fileName);
		if (!fullName.endsWith('.svg')) {
			continue;
		}
		const fileNameWithoutExt = fileName.slice(0, -4);
		const data = fs.readFileSync(fullName, 'utf-8');
		const match = re.exec(data);
		const icon = `<path id="${fileNameWithoutExt}" ${match[1]}>`;
		paths.push(icon);
	}
	const icons = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">${paths.join('')}</svg>`;
	fs.writeFileSync('icons.svg', icons);
}

main();
