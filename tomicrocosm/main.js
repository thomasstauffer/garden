import * as u from './universe.js';
import { html, toDOM } from './../crow/html.js';
import { Store } from './../crow/store.js';

function htmlRowString(name, value) {
	return html('tr', html('th', name), html('td', value));
}

function htmlRowNumber(name, ...values) {
	const value = values.map(value => u.toEngineering(value)).join(' / ');
	return html('tr', html('th', name), html('td', value));
}

function rowsBody(body) {
	const equator = u.circleCircumference(body.radius);
	const surface = u.sphereSurface(body.radius);
	const volume = u.sphereVolume(body.radius);
	const density = u.density(body.mass, volume);
	const gravity = u.gravity(body.mass, body.radius);
	const escapeVelocity = u.escapeVelocity(body.mass, body.radius);

	return [
		htmlRowString('Name', body.name),
		htmlRowNumber('Masse [kg]', body.mass),
		htmlRowNumber('Radius [m]', body.radius),
		htmlRowNumber('Gravitation [m/s²]', gravity),
		htmlRowNumber('Äquator / Equator [m]', equator),
		htmlRowNumber('Volumen [m³]', volume),
		htmlRowNumber('Oberfläche [m²]', surface),
		htmlRowNumber('Dichte [kg/m³]', density),
		htmlRowNumber('Radius [m]', body.radius),
		htmlRowNumber('Masse [kg]', body.mass),
		htmlRowNumber('Fluchtgeschwindigkeit [m/s]', escapeVelocity),
	];
}

function viewMoon(moon) {
	const header = html('h4', 'Mond');
	const table = html('table', ...rowsBody(moon));
	return html('div', header, table);
}

function viewPlanet(planet) {
	const orbitalPeriod = u.orbitalPeriod(planet.semiMajorAxis, u.sunMass, planet.mass);
	const orbitalPeriodDays = u.timeSecondsToDays(orbitalPeriod);
	const orbitalSpeed = u.circleCircumference(planet.semiMajorAxis) / orbitalPeriod;
	const rotationalSpeed = 7.292115e-5; // rad/s
	const rotationalPeriod = u.angleDegreeToRadian(360.0) / rotationalSpeed;
	const rotationalPeriodHours = u.timeSecondsToHours(rotationalPeriod);
	const apparentBrightness = u.apparentBrightness(u.sunLuminosity, planet.semiMajorAxis);
	const temperatureKelvin = u.temperatureSunBody(u.sunRadius, u.sunTemperature, planet.radius, planet.semiMajorAxis, planet.albedoBond, 0.0);
	const temperatureCelsius = u.temperatureKelvinToCelsius(temperatureKelvin);
	const greenhouseEffect = 0.0;
	const temperatureGreenhouseKelvin = u.temperatureSunBody(u.sunRadius, u.sunTemperature, planet.radius, planet.semiMajorAxis, planet.albedoBond, greenhouseEffect);
	const temperatureGreenhouseCelsius = u.temperatureKelvinToCelsius(temperatureGreenhouseKelvin);

	const header = html('h3', 'Planet');
	const table = html('table',
		...rowsBody(planet),
		htmlRowNumber('Umlaufzeit [s]/[d]', orbitalPeriod, orbitalPeriodDays),
		htmlRowNumber('Umlaufgeschwindigkeit [m/s]', orbitalSpeed),
		htmlRowNumber('Aphelion [m]', planet.aphelion),
		htmlRowNumber('Perihelion [m]', planet.perihelion),
		htmlRowNumber('Exzentrizität', planet.eccentricity),
		htmlRowNumber('Rotationsgeschwindigkeit [rad/s]', rotationalSpeed),
		htmlRowNumber('Rotationsperiode [s]/[h]', rotationalPeriod, rotationalPeriodHours),
		htmlRowNumber('Grosse Halbachse [m]', planet.semiMajorAxis),
		htmlRowNumber('Scheinbare Helligkeit / Solar Konstante [W/m²]', apparentBrightness),
		htmlRowNumber('Temperatur [°K]/[°C]', temperatureKelvin, temperatureCelsius),
		htmlRowNumber('Treibhaus Effekt', greenhouseEffect),
		htmlRowNumber('Temperatur + Treibhaus Effekt [°K]/[°C]', temperatureGreenhouseKelvin, temperatureGreenhouseCelsius),
		htmlRowNumber('Zusammensetzung', 0),
		htmlRowNumber('Albedo (Bond) [-]', planet.albedoBond),
		htmlRowNumber('Atmosphäre', 0),
		htmlRowNumber('Druck [Pa]', 0),
		htmlRowNumber('Zusammensetzung', 0),
		htmlRowNumber('Temperaturgradient', 0),
		htmlRowNumber('Hydrosphäre', 0),
		htmlRowNumber('Biosphäre', 0),
		htmlRowNumber('Lebensformen', 0),
		htmlRowNumber('Tilt ???', 0),
		htmlRowNumber('Temperatur Min/Avg/Max', 0),
		htmlRowNumber('Jahreszeiten?', 0),
	);
	const moons = planet.moons.map(moon => viewMoon(moon));
	const others = [
		html('h4', 'Name'),
		html('h4', 'Charakter'),
		html('h4', 'Lebewesen'),
		html('h4', 'Pflanze'),
		html('h4', 'Gegenstand'),
		html('h4', 'Gruppierung'),
		html('h4', 'Verlies'),
		html('h4', 'Stadt'),
	];
	return html('div', header, table, ...moons, ...others);
}

function viewStar(star) {
	const luminosity = u.blackBodyLuminositySphere(star.radius, star.temperature);
	const header = html('h3', 'Stern');
	const table = html('table',
		...rowsBody(star),
		htmlRowNumber('Typ', 0),
		htmlRowNumber('Temperatur [K]', star.temperature),
		htmlRowNumber('Leuchtkraft [W]', luminosity), // Luminosity
	);
	return html('div', header, table);
}

function viewSystem(system) {
	const radius = Math.max(...system.planets.map(planet => planet.aphelion));
	const massStars = system.stars.reduce((acc, sun) => acc + sun.mass, 0);
	const mass = massStars; // TODO + planets + moons ...

	const header = html('h2', 'Sonnensystem');
	const table = html('table',
		htmlRowString('', html('a', { 'href': 'view-system.html' }, '3D')),
		htmlRowString('Name', system.name),
		htmlRowNumber('Radius [m]', radius),
		htmlRowNumber('Mass [m]', mass),
	);
	const stars = system.stars.map(star => viewStar(star));
	const planets = system.planets.map(planet => viewPlanet(planet));
	return html('div', header, table, ...stars, ...planets);
}

function viewGalaxy(galaxy) {
	const header = html('h1', 'Galaxie');
	const table = html('table',
		htmlRowString('Name', galaxy.name),
		htmlRowNumber('Radius [m]', 0),
	);
	const systems = galaxy.systems.map(planet => viewSystem(planet));
	return html('div', header, table, ...systems);
}

function view(state) {
	// TODO first create new view which calculates everything (depending on a timestep?)

	const input = html('button', { 'onclick': 'galaxy-random' }, 'Randomize'); // TODO add type here, to allow also other params
	return html('div',
		viewGalaxy(state.galaxy),
		input,
	);
}

function randomInteger(startInclusive, endExclusive) {
	const range = endExclusive - startInclusive;
	return Math.floor(Math.random() * range) + startInclusive;
}

function updateGalaxy(galaxy, event) {
	if (event === 'galaxy-random') {
		return {
			name: '???',
			systems: [{
				name: '???',
				stars: [{
					name: 'Sonne',
					radius: u.sunRadius,
					mass: u.sunMass,
					temperature: u.sunTemperature,
				}],
				planets: [{
					name: 'Erde',
					radius: u.earthRadius,
					mass: u.earthMass,
					eccentricity: u.earthEccentricity,
					semiMajorAxis: u.earthSemiMajorAxis,
					albedoBond: u.earthAlbedoBond,
					aphelion: u.earthAphelion,
					perihelion: u.earthPerihelion,
					moons: [{
						name: 'Mond',
						radius: u.moonRadius,
						mass: u.moonMass,
					}]
				}, {
					name: 'Mars',
					radius: u.marsRadius,
					mass: u.marsMass,
					eccentricity: u.marsEccentricity,
					semiMajorAxis: u.marsSemiMajorAxis,
					albedoBond: u.marsAlbedoBond,
					aphelion: u.marsAphelion,
					perihelion: u.marsPerihelion,
					moons: [],
				}, {
					name: 'Test Earth',
					radius: u.earthRadius * (randomInteger(90, 110) * 0.01),
					mass: u.earthMass * (randomInteger(90, 110) * 0.01),
					eccentricity: u.earthEccentricity,
					semiMajorAxis: u.earthSemiMajorAxis,
					albedoBond: u.earthAlbedoBond,
					aphelion: u.earthAphelion,
					perihelion: u.earthPerihelion,
					moons: [],
				}],
			}],
		};
	} else {
		return galaxy;
	}
}

function update(state, event) {
	return {
		galaxy: updateGalaxy(state.galaxy, event),
	};
}

async function main() {
	u.test();

	function dispatch(event) {
		store.dispatchEvent(event);
		const main = html('div', { 'id': 'main' }, view(store.getState()));
		//const main = html('div', {'id': 'main'}, html('h1', 'hi'));
		console.log(main);
		const node = toDOM(main, dispatch);
		console.log(node);
		document.getElementById('main').replaceWith(node);
	}

	const initialState = {};

	const store = new Store(initialState);
	store.addEventListener(update);

	dispatch('galaxy-random');
}

window.addEventListener('load', main);
