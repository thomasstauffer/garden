import { THREE } from './modules.js';
//import * as u from './universe.js';

/*
http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/
https://www.redblobgames.com/x/1843-planet-generation/
https://www.redblobgames.com/x/1842-delaunay-voronoi-sphere/
https://gamedev.stackexchange.com/questions/60630/how-do-i-find-the-circumcenter-of-a-triangle-in-3d

http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml
*/

function createWireframe(geometry) {
	const wireframe = new THREE.WireframeGeometry(geometry);
	const line = new THREE.LineSegments(wireframe);
	line.material.depthTest = true;
	//line.material.opacity = 0.25;
	//line.material.transparent = true;
	//line.material.linewidth = 3.0;
	line.material.color = new THREE.Color(0, 0, 0);
	return line;
}

// O(geometry.faces)
// for every face return a list of the 3 face indices adjacent to that face
function findAdjacentFaces(geometry) {
	const t1 = performance.now();

	const faces = geometry.faces;
	// TODO check first if vertices are really are unique, if they share position we have an error
	const vertices = geometry.vertices;

	const verticesToFace = new Array(vertices.length);
	for (let vertexIndex = 0; vertexIndex < vertices.length; vertexIndex += 1) {
		verticesToFace[vertexIndex] = [];
	}
	faces.forEach((face, faceIndex) => {
		verticesToFace[face.a].push(faceIndex);
		verticesToFace[face.b].push(faceIndex);
		verticesToFace[face.c].push(faceIndex);
	});
	for (let i = 0; i < vertices.length; i += 1) {
		console.assert((verticesToFace[i].length === 5) || (verticesToFace[i].length === 6), verticesToFace[i].length);
	}

	const list = [];
	faces.forEach((face, faceIndex) => {
		const a = verticesToFace[face.a];
		const b = verticesToFace[face.b];
		const c = verticesToFace[face.c];
		const l = [];
		for (let i = 0; i < a.length; i += 1) {
			if ((a[i] != faceIndex) && (b.indexOf(a[i]) >= 0)) {
				l.push(a[i]);
				break;
			}
		}
		for (let i = 0; i < a.length; i += 1) {
			if ((a[i] != faceIndex) && (c.indexOf(a[i]) >= 0)) {
				l.push(a[i]);
				break;
			}
		}
		for (let i = 0; i < b.length; i += 1) {
			if ((b[i] != faceIndex) && (c.indexOf(a[i]) >= 0)) {
				l.push(b[i]);
				break;
			}
		}
		list.push(l);
	});
	console.assert(faces.length === list.length);

	const t2 = performance.now();
	// THREE.IcosahedronGeometry(1.0, 3) -> ~9 ms
	// THREE.IcosahedronGeometry(1.0, 5) -> ~14 ms
	// THREE.IcosahedronGeometry(1.0, 6) -> ~36 ms
	console.info(`Find Adjacent Faces ${t2 - t1}[ms]`);

	return list;
}

function floodFill(geometry, adjacentFaces, numberOfColors) {
	const t1 = performance.now();

	const faces = geometry.faces;

	function randomInt(upperExclusive) {
		return Math.floor(Math.random() * upperExclusive);
	}

	const colors = new Array(faces.length).fill(0);
	for (let i = 0; i < numberOfColors; i += 1) {
		colors[randomInt(faces.length)] = i + 1;
	}

	const iterationsMax = 10000;

	let iterations = 0;
	for (iterations = 0; iterations < iterationsMax; iterations += 2) {
		for (let i = 0; i < faces.length; i += 1) {
			if (colors[i] !== 0) {
				const adj = adjacentFaces[i];
				if (colors[adj[0]] === 0) {
					colors[adj[0]] = colors[i];
				}
				if (colors[adj[1]] === 0) {
					colors[adj[1]] = colors[i];
				}
				if (colors[adj[2]] === 0) {
					colors[adj[2]] = colors[i];
				}
			}
		}
		let loop = false;
		for (let i = 0; i < faces.length; i += 1) {
			if (colors[i] === 0) {
				loop = true;
				break;
			}
		}
		if (!loop) {
			break;
		}
	}

	const t2 = performance.now();
	console.info(`Flood Fill ${t2 - t1}[ms] Iterations:${iterations}`);
	return colors;
}

/*
function colorizeByColor(geometry, colors) {
	const faces = geometry.faces;
	const table = [
		[1.0, 1.0, 1.0],
		[1.0, 0.0, 0.0],
		[0.0, 1.0, 0.0],
		[0.0, 0.0, 1.0],
		[1.0, 1.0, 0.0],
		[0.0, 1.0, 1.0],
		[1.0, 0.0, 1.0],
	];
	faces.forEach((face, faceIndex) => {
		const rgb = table[colors[faceIndex]]; // TODO overflow
		face.color.setRGB(...rgb);
	});
}
*/

function clamp(value, min, max) {
	if (value < min) {
		return min;
	} else if (value > max) {
		return max;
	} else {
		return value;
	}
}

function colorizeByHeight(geometry) {
	const faces = geometry.faces;
	const vertices = geometry.vertices;
	faces.forEach(face => {
		const ha = vertices[face.a].length();
		const hb = vertices[face.b].length();
		const hc = vertices[face.c].length();
		const h = (ha + hb + hc) / 3;
		let color;
		const seaLevel = 1.05;
		if (h < seaLevel) {
			const alpha = clamp((h - 1.0) / 0.05, 0.0, 1.0);
			color = new THREE.Color(0.0, 0.0, 0.4).lerp(new THREE.Color(0.1, 0.1, 1.0), alpha);
		} else {
			const alpha = clamp((h - 1.05) / 0.05, 0.0, 1.0);
			color = new THREE.Color(0.0, 0.2, 0.0).lerp(new THREE.Color(0.0, 1.0, 0.0), alpha);
		}
		face.color.set(color);
	});
}

function crazyPlateTectonics(geometry, adjacentFaces, colors, scaling) {
	const faces = geometry.faces;
	const vertices = geometry.vertices;
	faces.forEach((face, faceIndex) => {
		const c = colors[faceIndex];
		const ca = colors[adjacentFaces[faceIndex][0]];
		const cb = colors[adjacentFaces[faceIndex][1]];
		const cc = colors[adjacentFaces[faceIndex][2]];
		const height = ((c !== ca) || (c !== cb) || (c != cc)) ? scaling : 1.0;
		vertices[face.a].multiplyScalar(height);
		vertices[face.b].multiplyScalar(height);
		vertices[face.c].multiplyScalar(height);
	});
}

function filterFaces(geometry, iterations) {
	const t1 = performance.now();
	const faces = geometry.faces;
	const vertices = geometry.vertices;
	for (let n = 0; n < iterations; n += 1) {
		faces.forEach(face => {
			const ha = vertices[face.a].length();
			const hb = vertices[face.b].length();
			const hc = vertices[face.c].length();
			const h = (ha + hb + hc) / 3;
			vertices[face.a].normalize().multiplyScalar(h);
			vertices[face.b].normalize().multiplyScalar(h);
			vertices[face.c].normalize().multiplyScalar(h);
		});
	}
	const t2 = performance.now();
	console.info(`Filter ${t2 - t1}[ms]`);
}

function main() {
	const width = 1024;
	const height = 768;

	const scene = new THREE.Scene();
	scene.background = new THREE.Color(0xeeeeee);

	const fov = 70.0;
	const near = 0.1;
	const far = 100.0;
	const camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
	camera.position.set(0.0, 0.0, 3.0);
	camera.lookAt(0.0, 0.0, 0.0);

	const renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(width, height);

	const controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls;

	const lightAmbient = new THREE.AmbientLight(0xcccccc);
	scene.add(lightAmbient);

	const lightSpot = new THREE.SpotLight(0xffffff, 0.5);
	lightSpot.position.set(0.0, 0.0, 10.0);
	lightSpot.target.position.set(0.0, 0.0, 0.0);
	scene.add(lightSpot);

	const t1 = performance.now();
	//const geometry = new THREE.BoxGeometry(1.0, 1.0, 1.0);
	const geometry = new THREE.IcosahedronGeometry(1.0, 5);
	//const geometry = new THREE.TetrahedronGeometry(1.0, 5);
	//const geometry = new THREE.OctahedronGeometry(1.0, 5);
	//const material = new THREE.MeshBasicMaterial({color: 0xff00ff});
	const t2 = performance.now();
	console.info(`Geometry ${t2 - t1}[ms] Faces:${geometry.faces.length} Vertices:${geometry.vertices.length}`);

	const adjacentFaces = findAdjacentFaces(geometry);
	const colors = floodFill(geometry, adjacentFaces, 6);
	crazyPlateTectonics(geometry, adjacentFaces, colors, 1.15);
	filterFaces(geometry, 7);
	//colorizeByColor(geometry, colors);
	colorizeByHeight(geometry);

	scene.add(createWireframe(geometry));
	const material = new THREE.MeshPhongMaterial({ color: 0xffffff, vertexColors: THREE.FaceColors });
	const sphere = new THREE.Mesh(geometry, material);
	scene.add(sphere);

	function animate() {
		//cube.position.set(cube.position.x + 0.001, cube.position.y, cube.position.z);
		requestAnimationFrame(animate);
		renderer.render(scene, camera);
	}
	animate();

	document.body.appendChild(renderer.domElement);

	//resize();
	//window.addEventListener('resize', resize, false);
}

window.addEventListener('load', main);
