/*
Basics

https://www.e-education.psu.edu/astro801/

Database

https://www.princeton.edu/~willman/planetary_systems/Sol/
https://exoplanetarchive.ipac.caltech.edu/
http://simbad.u-strasbg.fr/simbad/

Planets

https://www.quora.com/How-many-suns-can-a-solar-system-have
https://en.wikipedia.org/wiki/Giant_planet

Pressure

https://sciencing.com/calculate-atmospheric-pressure-2644.html

Magnetic

https://www.tcd.ie/Physics/people/Peter.Gallagher/lectures/PY4A03/pdfs/PY4A03_lecture14n15_magnetospheres.ppt.pdf
https://link.springer.com/article/10.1007/s11214-009-9621-7

Tidals

https://en.wikipedia.org/wiki/Tidal_heating
https://en.wikipedia.org/wiki/Tidal_locking
https://en.wikipedia.org/wiki/Tidal_acceleration

Solar Wind

http://www.astronomy.ohio-state.edu/~ryden/ast825/ch11.pdf
*/

// Constants

// https://en.wikipedia.org/wiki/Gravitational_constant
export const gravitationalConstant = 6.67408e-11; // [m3 * kg-1 * s-2]

// https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_constant
export const stefanBoltzmannConstant = 5.670367e-8; // [W * m-2 * K-4]

// Units

// https://en.wikipedia.org/wiki/Astronomical_unit
export const astronomicalUnit = 149597870700;

// https://en.wikipedia.org/wiki/Parsec
export const parsec = astronomicalUnit * ((360 * 60 * 60) / (2.0 * Math.PI)); // [m]

// https://en.wikipedia.org/wiki/Speed_of_light
export const lightSpeed = 299792458; // [m/s]

// https://en.wikipedia.org/wiki/Julian_year_(astronomy)
export const hour = 60.0 * 60.0; // [s]
export const day = 24 * hour; // [s]
export const year = 365.25 * day; // [s]

// https://en.wikipedia.org/wiki/Light-year
export const lightYear = lightSpeed * year; // [m]

// https://en.wikipedia.org/wiki/Kelvin
// https://en.wikipedia.org/wiki/Celsius
export const zeroCelsius = 273.15; // [°K]

// Data

// https://en.wikipedia.org/wiki/Alpha_Centauri
export const distanceSunAlphaCenatauri = 4.37 * lightYear; // [m]

// https://en.wikipedia.org/wiki/Sun
export const sunRadius = 695.7e6; // [m]
export const sunMass = 1.9885e30; // [kg]
export const sunTemperature = 5772; // [K]
export const sunLuminosity = 3.828e26; // [W]

// https://en.wikipedia.org/wiki/Earth
export const earthRadius = 6371e3; // [m]
export const earthMass = 5.97237e24; // [kg]
export const earthAphelion = 152.1e9; // [m]
export const earthPerihelion = 147.095e9; // [m]
export const earthEccentricity = 0.0167086; // [-]
export const earthSemiMajorAxis = 149598023000; // [m]
export const earthGravity = 9.807; // [m/s2]
export const earthAlbedoBond = 0.306; // [-]

// https://en.wikipedia.org/wiki/Moon
export const moonRadius = 1737.1e3; // [m]
export const moonMass = 7.342e22; // [kg]

// https://en.wikipedia.org/wiki/Mrs
export const marsRadius = 3389.5e3; // [m]
export const marsMass = 6.4171e23; // [kg]
export const marsSemiMajorAxis = 227939200000; // [m]
export const marsEccentricity = 0.0934;
export const marsAlbedoBond = 0.25; // [-]
export const marsAphelion = 249200000000; // [m]
export const marsPerihelion = 206700000000; // [m]

// Display

export function toEngineering(value) {
	const valueAbs = Math.abs(value);
	const sign = value < 0.0 ? '-' : '';
	if (valueAbs === 0.0) {
		return '0.0';
	} else if ((valueAbs <= 1000.0) && (valueAbs >= 0.001)) {
		return sign + valueAbs.toFixed(3);
	} else if (valueAbs >= 1.0) {
		for (let exponent = 0; exponent < (3 * 100); exponent += 3) {
			const mantissa = valueAbs / (10.0 ** exponent);
			if (mantissa < 1000.0) {
				return sign + mantissa.toFixed(3) + 'e' + exponent;
			}
		}
	} else if (valueAbs >= 0.0) {
		for (let exponent = 0; exponent < (3 * 100); exponent += 3) {
			const mantissa = valueAbs * (10.0 ** exponent);
			if (mantissa >= 1.0) {
				return sign + mantissa.toFixed(3) + 'e-' + exponent;
			}
		}
	}
	console.trace();
	throw `Cannot Convert Number '${value}'`;
}

// Unit Conversion

export function temperatureKelvinToCelsius(kelvin) {
	return kelvin - zeroCelsius;
}

export function angleDegreeToRadian(degree) {
	return (Math.PI * 2.0) * (degree / 360.0);
}

export function timeSecondsToHours(seconds) {
	return seconds / hour;
}

export function timeSecondsToDays(seconds) {
	return seconds / day;
}

export function distanceMetersToLightYears(meters) {
	return meters / lightYear;
}

// Geometry

export function circleArea(radius) {
	return (radius ** 2.0) * Math.PI;
}

export function circleCircumference(radius) {
	return radius * 2.0 * Math.PI;
}

export function sphereSurface(radius) {
	return (radius ** 2.0) * 4.0 * Math.PI;
}

export function sphereVolume(radius) {
	return (radius ** 3.0) * (4.0 / 3.0) * Math.PI;
}

// https://en.wikipedia.org/wiki/Eccentricity_(mathematics)
export function eccentricity(semiMajorAxis, semiMinorAxis) {
	if (semiMajorAxis < semiMinorAxis) {
		throw 'Semi Major Axis < Semi Minor Axis';
	}
	return Math.sqrt(1.0 - ((semiMinorAxis / semiMajorAxis) ** 2.0));
}

function testGeometry() {
	assertAlmostEqual(eccentricity(1.0, 1.0), 0.0, 1.0e-6);
	assertAlmostEqual(eccentricity(1000.0, 1.0), 0.999, 1.0e-3);
}

// Gravity

export function gravity(mass, radius) {
	return gravitationalConstant * mass / (radius ** 2.0);
}

// https://en.wikipedia.org/wiki/Escape_velocity
// https://de.wikipedia.org/wiki/Fluchtgeschwindigkeit_(Raumfahrt)
export function escapeVelocity(mass, radius) {
	return Math.sqrt((2.0 * gravitationalConstant * mass) / radius);
}

function testGravity() {
	assertAlmostEqual(gravity(earthMass, earthRadius), earthGravity, 0.1);
}

// Motion

// https://en.wikipedia.org/wiki/Orbital_period
export function orbitalPeriod(semiMajorAxis, massBig, massSmall) {
	return Math.PI * 2.0 * Math.sqrt((semiMajorAxis ** 3.0) / (gravitationalConstant * (massBig + massSmall)));
}

// Temperature

// https://en.wikipedia.org/wiki/Apparent_magnitude
// https://www.e-education.psu.edu/astro801/content/l4_p4.html
// Solar Constant
export function apparentBrightness(luminosity, distance) {
	return luminosity / sphereSurface(distance);
}

// https://en.wikipedia.org/wiki/Black-body_radiation
// https://en.wikipedia.org/wiki/Luminosity
export function blackBodyLuminosity(surface, temperature) {
	return stefanBoltzmannConstant * surface * (temperature ** 4.0);
}

export function blackBodyLuminositySphere(radius, temperature) {
	return blackBodyLuminosity(sphereSurface(radius), temperature);
}

export function blackBodyTemperatureSphere(power, radius) {
	return (power / (sphereSurface(radius) * stefanBoltzmannConstant)) ** (1.0 / 4.0);
}

// https://earthscience.stackexchange.com/questions/12093/how-can-we-calculate-the-temperature-of-the-atmosphere-including-the-greenhouse
// http://acmg.seas.harvard.edu/people/faculty/djj/book/bookchap7.html
// http://saspcsus.pbworks.com/w/file/fetch/64696386/planet%20temperatures%20with%20surface%20cooling%20parameterized.pdf
// https://www.lpl.arizona.edu/~showman/greenhouse.html
// https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_law
export function temperatureSunBody(sunRadius, sunTemperature, bodyRadius, distance, albedoBond, greenhouseEffect) {
	const powerSun = blackBodyLuminositySphere(sunRadius, sunTemperature);
	const solarConstant = powerSun / sphereSurface(distance);
	// calculate the maximum power which a body receives from a sun
	const powerSunBody = solarConstant * circleArea(bodyRadius);
	// only part of the sunlight is converted into heat (infrared), the rest goes traight back into the universe
	const powerNotReflected = (1.0 - albedoBond) * powerSunBody;
	// due to the greenhouse effect, the surface cannot directly radiate back into the universe

	// Thomas: I have never seen this kind of (too simplistic) model on the internet so far, so beware ...
	// TODO use a model where we account how much greenhouse gases are in the atmosphere
	// greenhouseEffect 0.0 -> no atmosphere, infrared fully radiated by surface
	// greenhouseEffect 1.0 -> infrared fully radiated by atmosphere (meaning of > 1.0?)
	const powerWithGreenhouse = powerNotReflected * (1.0 + greenhouseEffect);
	return blackBodyTemperatureSphere(powerWithGreenhouse, bodyRadius);
}

function testTemperature() {
	assertAlmostEqual(blackBodyLuminositySphere(sunRadius, sunTemperature) / sphereSurface(earthSemiMajorAxis), 1361, 1.0);
}

// Unsorted

export function density(mass, volume) {
	return mass / volume;
}

// Test

/*
function assert(condition) {
	if(!condition) {
		console.assert(false);
		console.trace();
		throw 'Assert Failed';
	}
}
*/

function assertAlmostEqual(a, b, epsilon) {
	if (isNaN(a) || isNaN(b) || (Math.abs(a - b) > epsilon)) {
		console.assert(false);
		console.trace();
		throw `Assert Equal Failed '${a}' != '${b}'`;
	}
}

export function test() {
	const tests = [
		testGravity,
		testGeometry,
		testTemperature,
	];
	tests.forEach((test) => test());
}
