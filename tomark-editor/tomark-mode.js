/* global CodeMirror */

CodeMirror.defineMode('tomark', function () {
	function startState() {
		return {
			state: '',
		};
	}

	function token(stream, state) {
		if (state.state === '') {
			if (stream.sol()) {
				const header = stream.match(/^(=+).*$/);
				if (header) {
					return 'header' + header[1].length;
				}
			}
		}
		stream.next();
		return null;
	}

	return {
		startState: startState,
		token: token,
	};
});
