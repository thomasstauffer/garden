/* global CodeMirror */

import * as q from '../crow/q.js';
//import * as sys from '../tomany/sys.js';
import * as tomark from '../tomark/tomark.js';

function jumpToLine(line) {
	const editor = g_editor;
	const y = editor.charCoords({ line: line, ch: 0 }, 'local').top;
	const height = editor.getScrollerElement().offsetHeight;
	editor.scrollTo(null, y - (height * 0.1));
}

function TOCClicked(e) {
	const line = e.target.dataset.line;
	jumpToLine(line);
}

function paneShow(id, show) {
	document.getElementById(id).style.display = show ? '' : 'none';
}

// eslint-disable-next-line no-unused-vars
function showStatus(value) {
	const nodeStatus = document.getElementById('status');
	nodeStatus.textContent = value;
	nodeStatus.classList.add('highlight');
	setTimeout(function () {
		nodeStatus.classList.remove('highlight');
	}, 500);
}

function process(editor) {
	const text = editor.getValue();
	const nodeOutput = document.getElementById('output');
	const nodeTOC = document.getElementById('toc-list');

	let html = '';
	let ast = [];
	let toc = '';
	let output = '';

	try {
		ast = tomark.compile(text);
		html += tomark.astToHtml(ast);
		html = html.split('<img src="').join('<img src="../Documents/');

		toc += tomark.astToTOC(ast, 0);
	} catch (error) {
		console.log(error);
		html += '<p>' + error.message + '</p>';
		html += '<p>' + JSON.stringify(error.location) + '</p>';
	}

	output += '<div class="page">' + html + '</div>';
	//output += '<pre>' + q.formatPretty(ast) + '</pre>';

	nodeOutput.innerHTML = output;
	nodeTOC.innerHTML = toc;

	const links = nodeTOC.querySelectorAll('a');
	[].forEach.call(links, function (link) {
		link.addEventListener('click', TOCClicked);
	});
}

let g_editor = null;

function main() {
	const nodeEditor = document.getElementById('editor');
	const editor = CodeMirror.fromTextArea(nodeEditor, {
		lineNumbers: true,
		tabSize: 4,
		indentUnit: 4,
		indentWithTabs: true,
		matchBrackets: true,
		lineWrapping: true,
		//viewportMargin: Infinity,
	});
	g_editor = editor;

	function delay(milliseconds, func) {
		let timeout = setTimeout(undefined, 0);
		return function () {
			clearTimeout(timeout);
			timeout = setTimeout(func, milliseconds);
		};
	}

	editor.on('changes', delay(300, function () {
		process(editor);
	}));

	editor.on('cursorActivity', delay(200, function () {
		const line = editor.getCursor().line;
		const nodeOutput = document.getElementById('output');
		for (let i = (line - 5); i < (line + 20); i++) {
			const nodeLine = document.getElementById('line' + i);
			if (nodeLine !== null) {
				nodeOutput.scrollTop = nodeLine.offsetTop;
				break;
			}
		}
	}));

	function save() {
		/*
		const fileName = document.getElementById('explorer-filename').value;
		sys.writeFile(fileName, editor.getValue()).then(function() {
			showStatus('Saved');
		});
		*/
	}

	document.addEventListener('keydown', function (e) {
		//console.log(e);
		if (e.ctrlKey && (e.key === 's')) {
			e.preventDefault();
			save();
		}
	});

	const defaultFileName = q.parseURL(window.location.search).fileName;
	const fileName = defaultFileName === undefined ? 'test.tomark' : defaultFileName;
	document.getElementById('explorer-filename').value = fileName;

	/*
	sys.readFile(fileName).then(function(data) {
		editor.setValue(data);
	});
	*/

	/*
	sys.readDir().then(function(files) {
		const node = document.getElementById('explorer-files');
		q.each(q.sort(q.filter(files, x => x.endsWith('.tomark'))), function(fileName) {
			node.innerHTML += q.format('<a href="?fileName={0}">{0}</a></br>', fileName);
		});
	});
	*/

	const panes = q.map([].slice.call(document.querySelectorAll('#menu > div')), x => x.id);
	function hideAll() {
		q.each(panes, pane => paneShow(pane, false));
	}
	hideAll();
	paneShow(panes[0], true);
	q.each(panes, function (pane) {
		document.getElementById('button-' + pane).addEventListener('click', function () {
			hideAll();
			paneShow(pane, true);
		});
	});

	let gridMode = 0;
	document.getElementById('button-resize').addEventListener('click', function () {
		gridMode = (gridMode + 1) % 2;
		//document.body.style['background-color'] = 'red';
		if (gridMode === 0) {
			document.body.style['grid-template-columns'] = 'auto 40% 35%';
		} else if (gridMode === 1) {
			document.body.style['grid-template-columns'] = 'auto 40% 50%';
		}
	});
	document.getElementById('button-load').addEventListener('click', function () {
		/*
		const fileName = document.getElementById('explorer-filename').value;
		sys.readFile(fileName).then(function(data) {
			editor.setValue(data);
			process(editor);
			showStatus('Loaded');
		});
		*/
	});
	document.getElementById('button-save').addEventListener('click', function () {
		save();
	});
	document.getElementById('button-process').addEventListener('click', function () {
		process(editor);
	});
	document.getElementById('button-generate').addEventListener('click', function () {
		/*
		const fileName = 'Generated-' + document.getElementById('explorer-filename').value + '.html';
		const text = editor.getValue();
		const ast = tomark.parse(text);
		const html = tomark.astToHtml(ast);
		sys.readFile('template.css').then(function(data) {
			const HEADER1 = '<html><meta charset="utf-8"/>';
			const HEADER2 = '<body>';
			const FOOTER = '</body></html>';
			//const STYLE = '<style>' + data + '</style>';
			const STYLE = ' <link rel="stylesheet" type="text/css" href="template.css">';
			sys.writeFile(fileName, HEADER1 + STYLE + HEADER2 + html + FOOTER);
		});
		*/
	});
}

window.onload = main;
