model ResistorBridge
  constant Real us = 10.0;
  Real is;
  constant Real r1 = 200.0;
  Real ur1;
  Real ir1;
  constant Real r2 = 100.0;
  Real ur2;
  Real ir2;
  constant Real r3 = 25.0;
  Real ur3;
  Real ir3;
  constant Real r4 = 50.0;
  Real ur4;
  Real ir4;
  constant Real r5 = 10.0;
  Real ur5;
  Real ir5;

  Real ua;
  Real ub;
  Real uc;
  Real ud;
equation

  is = ir1 + ir2;
  ir1 = ir3 + ir5;
  ir2 + ir5 = ir4;

  ua = us;
  ua - ub = ur1;
  ua - uc = ur2;
  ub - ud = ur3;
  uc - ud = ur4;
  ub - uc = ur5;
  ud = 0;

  /*
  is = ir1 + ir2;
  ir1 = ir3 + ir5;
  ir2 + ir5 = ir4;

  ur3 = us - ur1;
  ur4 = us - ur2;
  ur5 = ur3 - ur4;
  */

  ir1 = ur1 / r1;
  ir2 = ur2 / r2;
  ir3 = ur3 / r3;
  ir4 = ur4 / r4;
  ir5 = ur5 / r5;
end ResistorBridge;
