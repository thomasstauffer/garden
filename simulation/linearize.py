#!/usr/bin/env python3

import components
import matplotlib.pyplot as plot
import numpy


def linearize(y, dy, x0, x):
	return y(x0) + ((x - x0) * dy(x0))  # first taylor approximation


def linearize2(y, dy, x0):
	'''
	Same as linearize, return the coefficients which can be used in a linear equation

	y = a * x + b
	y = dy(x0) * x + (y(x0) - dy(x0) * x0)
	y = y(x0) + (dy(x0) * (x - x0))
	'''
	a = dy(x0)
	y = y(x0)
	b = y - a * x0
	return a, b


def diff_approx(f, x0, dx):
	x1, x2 = x0 - dx, x0 + dx
	y1 = f(x1)
	y2 = f(x2)
	if dx == 0:
		return 0
	a = (y2 - y1) / (x2 - x1)
	return a


def test1():
	'''
	y = sin(x * k) + x
	y' = k * cos(x * k) + 1
	'''

	k = 4

	def y(x):
		return numpy.sin(x * k) + x

	def dy(x):
		return k * numpy.cos(x * k) + 1

	x = numpy.linspace(start=0.0, stop=3.0, num=61)

	y1 = y(x)
	y2 = linearize(y, dy, 0.5, x)
	y3 = linearize(y, dy, 2.0, x)
	a, b = linearize2(y, dy, 1.0)
	y4 = a * x + b

	plot.figure(figsize=(16, 9), tight_layout=True)
	plot.plot(x, y1, label='y', marker='.')
	plot.plot(x, y2, label='x0=0.5')
	plot.plot(x, y3, label='x0=2.0')
	plot.plot(x, y4, label='x0=1.0')
	plot.grid()
	plot.legend()
	plot.show()


def test2():
	'''
	y = exp(k * cos(x))
	y' = -k * exp(k * cos(x)) * sin(x)
	'''

	k = 2.0

	def y(x):
		return numpy.exp(k * numpy.cos(x))

	def dy(x):
		return -k * numpy.exp(k * numpy.cos(x)) * numpy.sin(x)

	x = numpy.linspace(start=0.0, stop=7.0, num=200)

	y1 = y(x)
	y2 = dy(x)
	y3 = diff_approx(y, x, 1e-6)

	err = numpy.max(numpy.abs(y2 - y3))
	print('err', err)

	plot.figure(figsize=(16, 9), tight_layout=True)
	plot.plot(x, y1, '-', label='y')
	plot.plot(x, y2, '-', label='dy')
	plot.plot(x, y3, '--', label='~dy')
	plot.grid()
	plot.legend()
	plot.show()


test1()
test2()
