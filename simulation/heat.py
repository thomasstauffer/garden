#!/usr/bin/env python3

#import matplotlib.animation
import matplotlib.pyplot as plot
import numpy


'''
https://en.wikipedia.org/wiki/Heat_equation

diff(u(t, x), t) = alpha * diff(u(t, x), x, 2)

(u(t+1, x) - u(t, x)) / dt = alpha * (u(t, x+1) - 2*u(t, x) + u(t, x-1)) / dx^2
'''

def heat():
	ALPHA = 0.5

	X = 10
	T = 60

	NUM_X = 20
	NUM_T = 2000

	DX = X / (NUM_X - 1)
	DT = T / (NUM_T - 1)

	# stability
	print(DT, DX**2/(4 * ALPHA))

	u = numpy.zeros((NUM_T, NUM_X))

	u[:, 0] = 100
	u[:, -1] = 300

	for t in range(0, NUM_T - 1):
		for x in range(1, NUM_X - 1):
			u[t + 1, x] = DT * (((ALPHA / DX ** 2) * (u[t, x+1] - 2 * u[t, x] + u[t, x-1])) + ((1 / DT) * (u[t, x])))

	plot.figure(figsize=(16, 9), tight_layout=True)
	x = numpy.linspace(start=0, stop=X, num=NUM_X)

	MAX_N = 10
	for n in range(MAX_N):
		t = int(NUM_T * n / MAX_N)
		plot.plot(x, u[t], label=str(t) + ' / ' + str(NUM_T))
	plot.grid()
	plot.legend()
	plot.show()

#heat()

'''
https://en.wikipedia.org/wiki/Convection%E2%80%93diffusion_equation

https://floringh.gitbooks.io/computational-fluid-dynamics/content/1dLinearConvection.html

diff(u(t, x), t) + c * diff(u(t, x), x)

'''

def convection():
	X = 10
	T = 60

	NUM_X = 200
	NUM_T = 20000

	dx = X / (NUM_X - 1)
	dt = T / (NUM_T - 1)

	u = numpy.zeros((NUM_T, NUM_X))

	width = int(NUM_X / 5)
	for i in range(width):
		u[0, i + width // 2] = numpy.sin(i * numpy.pi / width)

	for t in range(0, NUM_T - 1):
		for x in range(1, NUM_X - 1):
			uxn = u[t, x+1]
			uxp = u[t, x-1]
			ut = u[t, x]
			uxd = ((uxn - uxp) / (2 * dx))
			if True:
				c = -0.05
			else:
				# just an interesting test case, which results in a discontinuity after a few seconds. Would a multivalued function solve this? Instead of change this to the weak form.
				c = (ut * -0.015)
			u[t + 1, x] = c * uxd * dt + ut

	plot.figure(figsize=(16, 9), tight_layout=True)
	x = numpy.linspace(start=0, stop=X, num=NUM_X)

	MAX_N = 5
	for n in range(MAX_N):
		t = int(NUM_T * n / MAX_N)
		plot.plot(x, u[t], label=str(t) + ' / ' + str(NUM_T))
	plot.grid()
	plot.legend()
	plot.show()

#convection()
