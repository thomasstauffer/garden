PK     ��O�B�H         mimetypetext/x-wxmathmlPK     ��O�T�D    
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
It also is part of the windows installer for maxima
(http://maxima.sourceforge.net).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     ��O�%[b�  �     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 18.02.0   -->
<!--https://andrejv.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="7">

<cell type="code">
<input>
<editor type="input">
<line>restart;</line>
</editor>
</input>
<output>
<mth><lbl>(%o1) </lbl><v>restart</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>load(abs_integrate);</line>
</editor>
</input>
<output>
<mth><t breakline="true">ARRSTORE: use_fast_arrays=false; allocate a new property hash table for $INTABLE2</t><lbl>(%o2) </lbl><st>/usr/share/maxima/5.41.0/share/contrib/integration/abs_integrate.mac</st>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>eq: &apos;diff(u, t) - &apos;diff(ul, t) = ul;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="eq">(%o3) </lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><v>u</v></d><v>−</v><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><v>ul</v></d><v>=</v><v>ul</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>expand(ode2(eq, ul, t));</line>
</editor>
</input>
<output>
<mth><lbl>(%o4) </lbl><v>ul</v><v>=</v><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e><h>*</h><in def="false">><r><v></v></r><r><v></v></r><r><e><r><s>%e</s></r><r><v>t</v></r></e><h>*</h><r><p><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><v>u</v></d></p></r></r><r><s>d</s><v>t</v></r></in><v>+</v><v>%c</v><h>*</h><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol: expand(ic1(%, t=0, ul=10));</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol">(%o5) </lbl><v>ul</v><v>=</v><v>−</v><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e><h>*</h><r><p><at><r><in def="false">><r><v></v></r><r><v></v></r><r><e><r><s>%e</s></r><r><v>t</v></r></e><h>*</h><r><p><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><v>u</v></d></p></r></r><r><s>d</s><v>t</v></r></in></r><r><v>t</v><v>=</v><n>0</n></r></at></p></r><v>+</v><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e><h>*</h><in def="false">><r><v></v></r><r><v></v></r><r><e><r><s>%e</s></r><r><v>t</v></r></e><h>*</h><r><p><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><v>u</v></d></p></r></r><r><s>d</s><v>t</v></r></in><v>+</v><n>10</n><h>*</h><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>rhs(ev(sol, u=10));</line>
</editor>
</input>
<output>
<mth><lbl>(%o6) </lbl><n>10</n><h>*</h><e><r><s>%e</s></r><r><v>−</v><v>t</v></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>wxplot2d(%, [t, 0, 5]);</line>
</editor>
</input>
<output>
<mth><lbl>(%t7) </lbl><img>image1.png</img><lbl>(%o7) </lbl><v></v>
</mth></output>
</cell>

</wxMaximaDocument>PK     ��O����0  �0  
   image1.png�PNG

   IHDR  X  �   �W��   bKGD � � �����    IDATx���y|L��?��d�DAbI	�4i� ������HE-UT{����ی����u�+u�(E[�~C�����	M��������?����e�<��y����LOg>wN_��<�9sFWRR"""�����H$!i����4�EHDD��"$""Mc�����H�X�DD�i,B""�4!i����4�EHDD���"<r䈳�s�����={��۷l�r�����X�_�%%%˖-�իW^^^�3EEEaaaO?���k�,X0lذӧO�IDD����g�����~�iܸqo��Vi��Ǉ��feeY[[5j����g�}&:)	`%:�ɍ7����C�ʟIJJ���)mA ���;v��/���i�:�ŷ��IDdd�\������LNN���}�C{{�[�nU���P\|;/�D�7mB)�t�xP���(Ǐ��N��&��aE�������sss+nVzm�޽��"""�X�~~~���EEE����+�x�V�b�Z,�Ν;7j�(222??۶mk֬9rdeo٢d4""R���#����"66v�ĉ������QQQ�[��l�'������HDD��Jv�С�{� ����Vm��駱};2Y2IDFF���
���(ǏBv���at:�ܹ%�O����H�$��V����wo�ŉADD&�"|�'�Dq1N�����L�E�h}�`�f�!���4X���"$"2cR��TL�ߛ7Ѭ._����@DD*Ƌe̖�Z���ݢs�	��$4���DD�EX%<MHDd�X�UҮ22p��DDdl,�*��@�^�7�bVWG��̒���*��K��\���\��VnTNDT=����k�͛c�~�9��ȨX���p�a5��b�&�!��Ȩ�\�U�}�݅�h�IIh�H`(""��9B�ge��`^;JDdVX��ӷ/WG��̊��X�T�槧��W���RT(""��Ҩ&4iww��':	������ƍ�C�����_?�ƊADDF��"<p�@PP���C�V�֮][�70���t�Dш�HQZ,���̾}����'33sѢEcǎݳgO��uKK����Q""3��"��矝��"##mmm;w�<jԨŋW��}���DDfB�E������X�������ӕm����` з/�oǝ;
�%"R#����@t���;�t�̙'�|2**jĈ�N�z�g7n|���[>�;1]���л���Ƀ�#�F˖-�-[6mڴ:u�<x�����ի���L������",,,�ڵ�իW/]�t���+W�t�С�/2` bb ��C�E������k׮�u�[�.::z����}__��"!���H9V�иq�+W�?>--���'::��˫�ӿ?֭C�6FHDDʑ�Ħb~�w�N��&��W2�zIz������ZX�&Mp��ܔED�R�����BC�~��DDT,�Z0�EHD$7)���y�4?'�=��T�u�""��Ҩ98 0[���ADD5�"�����:JD$1)�����4��t숴4XZ*��H��4�Q��ps�޽�sQ��� ,11�CQ���`�@�]+:���PT��~������Eh���DDD�aOIJ�K]S�K�Ѹ1�EӦ�ED�R����YY�^2CD$�ф�c��!�������*�Z���<4i�ӧ��j�PDD*ťQ���EH6l������EhL\%"����X�Tw��&��q���L��H��4JprB��X�Nt""�2���իE� "�*�r��L����Gݺ&
ED�R\��ѣG�t�R�n��͛��GF|eGGt��߬'"��F�0<<��矿~���͛?��uF=���Q""�h����Ο?_\\\:�����S��_��؁�7���DDd*Z,B[[۷�zkڴi666���cƌ�ի�_���Q��IA�E@��/^�877w߾}˗/_�jUe[��e0����=��~2ZZ""�1�
D��!)��͛7O�:5))���~�y��;wVܲ�W@]����q�"�YOD«F�q����������666�}ggt��X�*��0888==��O?-..>q�ļy�n�w��(�����޶m�f̘q��IWW�ɓ'O�<����f��:Z��}G�HC$]�2�bj�S��ǰa0�l��H�$-B-.�*f�P����DD�PR��bjyt�����pFH�stDϞ������*�"4�!C����CQ夜�*�����\<���и��B��F�������G�9���,B�1�~+:U�Ehr�z��%�:%:=���,,0d��Nt""z��Ƿ�B�S�DD�E��`e�D� "�
X�
y�y~���H���·b�����t놋aii��#"R~�����=���E� "�{��3b�%"R)���1�4��U����Eԩc��$"R.��#4l�������s�]X��z��X!:�E�i�b�>�/�1��'Ѩ�_��H�4J�V�c?� :��E���#��7�CџX�J{�df"!At""�"T��F����s m�e�t�j׮��F��ʕ((P�=�����X�C�����{�:;;GFF*�����͛�|O""z0)/u5����nݺu��q�ܹ��t�/Y��XDG�ⵉ�Đ��R�6��K�Μ9399�N%7=3�N�Ɂ�;Ѹ�)^��H I�P�K������̙3cƌ�Z��}g�Q�������[������ѡjH��6������_�r�!Ehң�={0v,�� �<DD���P2���|�tФ��`a��{E�?Z.�_~��gϞb3������H�4Z�%%%)))>>>bc���hdg�MAD�iR��*F���<�,^zɤoBD��#��x�%,Y":���ŅHL���H�X��YZb�DE��AD�UR��*F���s�Щ.^�^o�""2!�#���@�6X�Vt""Mb�¸q\%"C�i�b��߹ww����K�w#"2	.�R���;��AD�=R��b�<��p����y���)Q�pFH����@����DD�"T�W^���Ci�PEBCq����HKX�*ba��_��+:��HybS1ʟ��̄�7RRP���oKDd�X�������Ft""͐��#��f�^���'��)��DD��!G��ptĖ-�si�P�^y_~):�6H9�U��i���h�����C�7'"�!.�����a�H,Z$:�H�ފxt���������N��Ug�dL^^���}':��c������sHxtED$�z�� ;w��ADd�4Z����u��mذ�[o�U\\,:у��*��Bt""���"suu�|������,Y":у���x����ADd���§������Ϧ����� HII���qww������97n`�<�)��McfhqFx�С�m�Ο?��ǧU�Vqqql�R�{���I��;\����=��`�U :TY� @VV֞={Z�h�f͚����c�:99�5�?�i�`�"��� DD1'�v�g�z�^����W~~~�<y�U�D�z��^����sGt""s��"���-(((��������p�Z�U+~����$�X�!!!�����7���O�:�`���Ç������s��z""��b����;w�����������_=z��P�kk�ŉ�ADdv���U1��x�J,[��[E� "����̪��PRC�"%���ADd^X�Ұ�Ɣ)��S�9��̋��XŨm���OO��OO�Q��*PۘYE���������?�9��̈��[`gggϚ5KT��:�~���9��̅���W�^MHH 0lذ��������iӦ���)I����Ѱ!�_t"�{�s�|$�����v�ZFF���k�:�N�׿�⋑���GR�N={��8s�����E�c�#�1tDDDtt����w���h�o�):�]T;f>��B��믁���S�Q�NMLDϞ8s���I�c�é.��>GU���������&:џ�<f>��B��sTU����BBp���DG!"��1�!T����C��� �'�D�N�����Hr�+���ȍ��k߾}ZZ����

D�R���ÿ���%"��!�����_~����:}�����M��O�.:��m�V���7�s�L�����?��S�֭����_�~�ҥ:���+�D��ݿ���C���Q�H��?f>�g�����6lx�����7��Vt""i��۶m����?��/ڶm+0��͚�>@a��DDrR�46!!�w��z�>--�������[�liӦ��Id�懄`�p����D�m����Qi�7nlذ����=�X����ׯ/$�,;5>�G#)	�֢����2f�G��KJJt:��w�h����Ä	�s��I4f�M��K�߿���������k۶m��݇���q���DD�QoN�6�믿���_�n�o�!:��u����p��DD�QW�����������������B����*�3���9�����"�2e���?���oggg������ݺu�����F_|��.���q__�V����BD�՝�,**�ꫯ,X��o�3&??_���]�������������Ã~�O���ΡS'$%�A�Q�H{�3K�kF��������ݻ:t��!S�K||��3gBBB�k׮���<<��s��nDD��*�]�v_�|y����/~��F����j�w9�|ZZ���_ll�I�ƏS�ƺ{#&1��3���HK���̚�`�U :T�k�������'''gѢE�7o.}2:::22����F|�۷o�����o��FBB��?�\q3I��ӧ��u|���D�1����������fff�?q��A㾑�]?����s��%㾾Xӧ#:�N��AD$uaxx��������ѣ�~޸��lذ�u���O�<���e����S�B���DD���ilJJ���mӦMM����>>>ӧO���v���nݺ=zT�R�i>�[��ヵkѡ��(D�����X�x���ۭ[�L�...����������ƍ[�`�[Pj��0��=��ڻ��xԨQ���;w�ܹs��8�ݔ**B@ >����(D�����
���iee����g�ooo�qdݩ�6něo��1XY��BD 阩��ѳg�zyy��_��2e��,�_?4k�E�D� "R1յ��^}�U�)�Hzts��D�쉤$�mc"�I�LՅ>u�ԢE�N�8q��-GGG��#G��m%I�S����ptħ���AD�N�1S]K��֭k߾}FFF�=�ҽ{�+W������Il�l,_��d�9��TI]������W_�����'�l���+���N��$=����O�k֭���̚�c��BשS'++�����'�ܹӰa�7n(�GҝZQ~>��1o�=� "2&I�Lu-�>��Ss��)(((���h���;v��������k(,��He������aaa/^��񱷷���MNNnԨQll��ہJztS��P<�,&M���̔�c��B���III999���~~~�;w��3s�t�V&)	=z����":
�#I�L)C+Fҝ��&�����AD�H�1S�Њ�t�>����6! @t"2;����
��윗���T��&%�N}�ŋ��7ؽ:��(Dd^$3����������~�Ν��GD����;�ኋ�I�0r��(Dd^$3U:**��/�<r�� ��;���Ch(��*:
�I�Lu}��K/����.:�9k�Æ���E� "R)�[1��TEN���t)z���̅�c��f��|�^}����	�"Ԯ��͟g""��r�I��Uw�<:t��������'����5o���«���AD$�P�M�ի��;�9���r�I���u� ���h�@t"���c��g�w��	�3g�� �u������AD$���p���Ǐ�B>� Ǐc�j�9����"ܼy����{��-:�*��X�S� #Ct""ei�322&N��r�J�^/:�Ztꄑ#1y��DD��h���ӦM���䖺{ӧ�������h�9�H���ѡjH�E8o޼���I�&Ue�{�w��X��'#+Kt"R7��PR��P5$奮�ԪU��g�Z[[�u떕�U�=6m�TqKI/���o~���D$I�L)C�RZZZ�����0aB�֭�x�ƍW�RҝZKyyh����"�i%:� nnn��S�����[P�lm�d	""Э5����Ĥlo�Hztc3g"!�։�AD�t����2T���t	K���ADdbR��b$=�1��D���x<���(D$I�L��R~~00lŞ�̙���I�n�k�@x{�OD� "Փt̔2�b$ݩ�u�ڵ����mY���$3��Iw���ލ�Cq�0�5"zI�L�#�G����c�h��BDdl,B���H��OE� "26)����t�o"/�cG�ĠS'�Q�H�$39#��j�b�Pdf��BDd<R��b$=�1�w������gXZ��BD*#��!UϜ9��`�?�HD�"e{+FңS�z:`�"����BDj"�)eh�H�S��/<�iS�Q�H5$3�4J5ѥ^�=�;wDG!"�)�[1��(��ÆA��7߈�BD� ��!ՐN��(>��EG!"�)�[1��(��9a�*t�.:
�&��!Պ��-���xQt"�aRm��i��[�DG!"�>)����t�/ċ/";�WC����t�䌐�㫯p�2��O�9����EHơ�#&?��+DG!"�+��|4h����-е��4DDU��aLLL�V�[�l�`��q̇�~�C���)�Q���F���t�ܹ'�xbݺu}��9z�hPPжm�:w�\qKIO�
�����#���Q�HA���Z\���|�r�z� dgg[YYխ[Wt(�2v,ΜA�~رu�NCD�P]�W��͛7����u�6a�??�ʶ����⫚9s��1cP\,:
���`�U :TI9�5���b ���!!!3f̘4iR�m$��ĝ;������EG!"EH:fjtF���������ܸq�W����X��a�L�Q��*��"���(���[z�����7����扎BDT	-aǎ/]�������`ǎQQQ�Ǐ�l�� .�|����A�\ϭ��N�2%!!���}�̙Æ{�f��w����	�7ߠO�Q��d$3��Iw�:�ك�p�Y�� �Q��4$3��4JBa�
��c�>�Q����"$���e��.$"a�����ҥáC��`����ŢE���p��(DDڼ�(	7` 

��8�i#:i���4 BC�i��X�$Li���y!��"$�ʻp��k':i��4����?����E�!"��U�$���X�
C�`�z�Q�H{X��
={b�FL����EG!"���(�E��ز}���m����4D�,BR�܉޽���w�����A�;�+F�;����e�틮]�����=�<$3��Iw��y�W+V��Vt"�I�Lo�99a�F����׮�NCDf�EH*ec�+н;:wFJ��4Dd�x���N�>@������i{"2	�I�Əǲe�w߉�BD�H�����įY:q`�̚�KI�TJ�1S�Њ�t����W1t(���r%��E�!�
$3yhM�h�[���O=��$�i��\�I&VV�;3g�Gl� :���]����g͚%�\^�F�ºu���a0��Xt"���033s���Ǐ��ʊ��[�lٗ_~):UO�N8p ;v��g��):�L�E�����K/Y[[���EDD�ݻWt(����u+Z�F���$��b,]����qqq�:u�lcݽ�B)�
����ǘ?����p��H1�AW��P5$奮ƒ��7bĈ7nlܸ��Ʀ��^
�A.`�0ԭ�o�����4DZ%阩�a�����ݻ���lذ�-Hqw�Ν@�vؾ]t"��F���ѣ�:u8p���o˟�1VV��C,]�^�*�(  GIDAT�{Pt "�����ZJOOoӦʹi�F�Y����]�*n)�4_�\��1��,_oo�i��D�1S�3�ŋgdd̘1�ٟF�-:M�Fظ�F�K|�%��!�G���#���:}�G��_�f�D�!� I�L-�I#����/x�t���E�!"����#�����1�OO,\��E�!2_������k����[��y֐��!e{+Fң��ѣx��� *
�?.:�ّt�䌐4$  ��4O?�O>AA��@D�,B�L��}���D�s��@D$���X�H:ͧ*Z�S� $��P���G�1�3BҮ�q������+�D%e{+Fң���'���8w���gD�!���c���#�N�����ԩh�}ě�Մ�c&�F�ʄ�!)	�:�KL���,с�H,B����⭷����B��b�\ܹ#:����~..�?;v`�N��b�
��DD&#�z�b$]�&#ڽ3f��u̙��0�i��M�1S�Њ�t���m܈w߅^�ٳ":�ZI:fJZ1��T2��b����`�,��):��H:fJZ1��T2��"�Z�ٳ��w�A߾��Dg"RI�L)C+FҝJ�VT�ի������ߟuHH;fJZ1��TRFI	֯ǬY(,ěob�PX[��D$��c���#�N%�mތ��O����	pv�HI�L~�������֭X�'N�eKL���gEg"�*cG@ �/GB�����c�Dr�r�I��$���X�����1i�����LD�'阩��#G�y>�L��&�����X�
͛�wp���XD� -��e˖���+//Ot2[���6!>w�C��b�j��NFDw�hΞ={�ܹ������&xya�\\��ѣ�p!�5�o��Iѱ��f����5i��СC]�tyȤP��nR�3g�d	�-��'^xC��~}љ��A�1S�3B777]������`0q42-[��q��y�v�eK��.��L��ѡjH��6��rFH�ݼ��~���HL���c�Ht�(:Q�H:fjtFH�NN;;v`�~������1ǎ�NF�,B"�h���'N�?��x�	HJ��Ȭ��T�m[|�Μ���y}���＃}�P\,:�ّr=W1��w��))��CX�11�v <={��Ft2�{I:fJZ1��T2cg�`�Z�Y��D���P�뇆E�" �)eh�H�SI�\Al,6oƖ-x�I���`�oKK��H�$3��Iw*iJ~>�oG\�mCz:z�F�~��������H:fJZ1��TҬ�Tlڄ�8����@p0��ѵ+��E'#m�t̔2�b$ݩD��ؿ[�b�6>�v����F�Npp�̗�c���#�N%�ۭ[ؽ۷#>G�"  �����;���$3��Iw*Qen��/�`�v�����7�tA�.x�i���G�t̔2�b$ݩDU�����ݻ�o����]�6m`k+:IH�1S�Њ�t����#>��ط'O��O=����Sx�	~+��D�1S�Њ�t��R^�Łؿ��#=e:v��{�L�1S�Њ�t�׍8tǎ��8��T��!  �Z�U+�igg�I$3��Iw*�Ieg#!�?..h�O>	__����'�׋NI"H:fJZ1��T"%��Y;��$$&��	����~~��G��x�	<�8�Q$3��Iw*�X��HIAb"�GB��p�<�5��/�x>>��'�5�N':+��c���#�N%R���=��D�:�S�p�,RRp�:x�q������psc;JL�1S�Њ�t�I!;))e�x�$��q�,��м9<<�f�e����@�1S�Њ�t��+/�Ε�9{g����q�t:xx�iS4m�-Т{M��I�@��H:fJZ1��T"󓕅s琖�p�ΟGZRSq�24(��ƍѬ�4AӦps��4I�L)C+FҝJ�%%HO�ٳ�pW���E��#5�.!-NNe�ؠ4@�fh�..h���Q#��͎�c���#�N%�RW���e\��kא���T\��+Wp�*�\AV\\��ww4l���Wײ']\ʞ���t̔2�b$ݩDT��Dff�l��5dd�=SZ��q�F,�߆��ޠ��\��_���c���#�N5��`0D���C9�|��e�X^��v��Ov6�_ǹs��|��\\��PV�����pr*k���tt��y�#�)eh�H�SM�E)~��Q�+�(n�ĵk�������DVrr����7�z4;7n��Ͳ�o���/����TV����W������mYe֭�W���ή��W!eh�H�SM�E)~��Q���GQ:�,��7p�FYA����?���۷q�6rr����e���XZ��	u�gi5:9��vv�Sz�_mjgW��z��� kk���SVVeϋ�(��@����	&�߿�q�Ɵ�y���E'""m����������ƭ[e5y����f^rs���?����e�Z�YIIY��=_P�����89�Ҳ�b����P�|i��[�����_ۗW����_��b���EDDlڴi�Ν�:|������\DDUbe�z��s���vp�&��PT��7QPP6����(.F~>�� ~���eU
 'F�#����Z�����ʲ��0j�(WW��>��▒N�M�E)~��Q��GQNҏB�3¤�$�����cǎ�6����GQ��C9~��QHM�E���c_�� ����u�����І����Bt ���sss����:��)""������/99�����abb�����HDD$���s�΍5������߶mۚ5kF�):���s����'Ntuuuuu���jݺ��PDD$������F���ʱ��H�X�DD�i,B""�4!i��a���{��ioo߲e���׋�#ޑ#G�������k׮��@GGGww�Y�fi��똘�V�Z9::�l�r����w�Ν���9s�"�_|����.:QՔP%
}}}�}��۷o�����۟:uJt(a����.]Z�~}�^/:�0u�֍���������Z�h1o�<ѡ�8{����M\\\IIɑ#G������#:�`S�N����={�� "<���߾����lщ��3�J���/^�������ӧODD�E�f���s�΍��D�������^z������/""b�޽�C����q���>}� ��ζ���[���P"m޼y����{�D����3g΄��4h׮]�U	��R��ĉb#	4nܸcǎ�"R@@�ҥKK�^PPשS'���W��͛7����u�6a�???щ���Ș8q�ʕ+�z��,"�?>--���/66vҤI�Ǐ����J�x��*���5i�����*���7bĈ&M�L�8Qt��_����Ҽy�I�&�N$Ƌ/�8m�4޻�y�湹�vvv <==�=��_������h�V���D���ڽ{w�6��؈�#�����������q�V�^-:����+,,��A�}J[����ϥK���:a��kMT�ѣG;u�4p�������Vta�������֫WO`�-Z�{��z��9;;���~��}��J�6��'O������D_��^EEE������;w�n����p��1ѡ;p����MKKsuu����/����J���L�O>�$??���NNN�ׯJ�K�.��SHH�믿���.:������O
����9::n߾]t�*a>Lrrrpp���S˖-W�Z%:�x/�Y�f�w��ϊ%́���������;�qT!,,L�_�ػwo``�������+Dǩ*�i������H�X�DD�i,B""�4!i����4�EHDD��"$""Mc�����H�X�DD�i,B""�4!i��H2���W�^���|��$��� :��`I�ڵk�#�!�L"""222��d��Y����H2�7^�re�^�D!2������H�X�D���t�#�!�d�z}rrrNN�� Df�EH$�1c�L�6���>��L�Q""�4���H�X�DD�i,B""�4!i����4����/|{�y�    IEND�B`�PK      ��O�B�H                       mimetypePK      ��O�T�D    
             5   format.txtPK      ��O�%[b�  �               t  content.xmlPK      ��O����0  �0  
             �  image1.pngPK      �   bC    