#!/usr/bin/env python3

'''
  +--------------+ a
  |              |
  |       +------+------+
  |       |             |
  |     +-+-+         +-+-+
  |     |   |         |   |
  |  R1 |   |      R2 |   |
  |     |   |         |   |
  |     +-+-+    R5   +-+-+
+-+-+     |   +-----+   |
        b +---+     +---+ c
 +++      |   +-----+   |
  |     +-+-+         +-+-+
  |     |   |         |   |
  |  R3 |   |      R4 |   |
  |     |   |         |   |
  |     +-+-+         +-+-+
  |       |             |
  |       +------+------+
  |              |
  +--------------+ d

KCL

a: is + -ir1 + -ir2 = 0
b: ir1 + -ir3 + -ir5 = 0
c: ir2 + -ir4 + ir5 = 0

a: is - ((us-ub)*g1) - ((us-uc)*g2) = 0
b: ((us-ub)*g1) - (ub*g3) - ((ub-uc)*g5) = 0
c: ((us-uc)*g2) - (uc*g4) + ((ub-uc)*g5) = 0
'''

import numpy
import sympy
import ngspice
import om
import scipy.optimize


R1 = 200.0
R2 = 100.0
R3 = 25.0
R4 = 50.0
R5 = 10.0
US = 10.0


def resistor_bridge_num():
	g1 = 1 / R1
	g2 = 1 / R2
	g3 = 1 / R3
	g4 = 1 / R4
	g5 = 1 / R5

	# ub, uc, is
	l = numpy.array([
		[g1, g2, 1],
		[-g5-g3-g1, g5, 0],
		[g5, -g5-g4-g2, 0],
	])

	r = numpy.array([
		g2*US+g1*US,
		-g1*US,
		-g2*US,
	])

	ub, uc, is_ = numpy.linalg.solve(l, r)
	ur3, ur4 = ub, uc
	return ur3, ur4


def resistor_bridge_num2():
	def model(x):
		ub, uc, is_ = x
		f0 = is_ - ((US-ub)/R1) - ((US-uc)/R2)
		f1 = ((US-ub)/R1) - (ub/R3) - ((ub-uc)/R5)
		f2 = ((US-uc)/R2) - (uc/R4) + ((ub-uc)/R5)
		return [f0, f1, f2]

	ub, uc, is_ = scipy.optimize.root(fun=model, x0=[0.0, 0.0, 0.0]).x
	ur3, ur4 = ub, uc
	return ur3, ur4


def resistor_bridge_sym():
	ub, uc, is_ = sympy.symbols('ub uc is')

	a = sympy.Eq(is_ - ((US-ub)/R1) - ((US-uc)/R2), 0)
	b = sympy.Eq(((US-ub)/R1) - (ub/R3) - ((ub-uc)/R5), 0)
	c = sympy.Eq(((US-uc)/R2) - (uc/R4) + ((ub-uc)/R5), 0)

	result = sympy.solve((a, b, c), ub, uc, is_)
	return result[ub], result[uc]


def resistor_bridge_spice():
	circuit = f'''
	netlist
	vu a 0 {US}
	rr1 a b {R1}
	rr2 a c {R2}
	rr3 b 0 {R3}
	rr4 c 0 {R4}
	rr5 b c {R5}
	.end
	'''
	t, ub, uc = ngspice.simulate(circuit=circuit, variables=['b', 'c'], t_step=0.1, t_end=1.0)
	ur3, ur4 = ub[-1], uc[-1]
	return ur3, ur4


def resistor_bridge_om():
	MODEL = f'''
	model ResistorBridge
	constant Real us = {US};
	Real is;
	constant Real r1 = {R1};
	Real ur1;
	Real ir1;
	constant Real r2 = {R2};
	Real ur2;
	Real ir2;
	constant Real r3 = {R3};
	Real ur3;
	Real ir3;
	constant Real r4 = {R4};
	Real ur4;
	Real ir4;
	constant Real r5 = {R5};
	Real ur5;
	Real ir5;

	Real ua;
	Real ub;
	Real uc;
	Real ud;
	equation

	is = ir1 + ir2;
	ir1 = ir3 + ir5;
	ir2 + ir5 = ir4;

	ua = us;
	ua - ub = ur1;
	ua - uc = ur2;
	ub - ud = ur3;
	uc - ud = ur4;
	ub - uc = ur5;
	ud = 0;

	ir1 = ur1 / r1;
	ir2 = ur2 / r2;
	ir3 = ur3 / r3;
	ir4 = ur4 / r4;
	ir5 = ur5 / r5;
	end ResistorBridge;
	'''
	t, ur3, ur4 = om.simulate(MODEL, 'ResistorBridge', ['ur3', 'ur4'], t_step=0.1, t_end=1.0)
	return float(ur3[-1]), float(ur4[-1])


def main():
	# -> 1.864, 2.203
	print(resistor_bridge_num())
	print(resistor_bridge_num2())
	print(resistor_bridge_sym())
	print(resistor_bridge_spice())
	print(resistor_bridge_om())

main()
