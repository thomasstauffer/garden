model VoltageDivider
  // u [V] / i [A] / r [Ohm]
  Real us = 10.0;
  Real is;
  Real r1 = 4000.0;
  Real ur1;
  Real ir1;
  Real r2 = 1000.0;
  Real ur2;
  Real ir2;
equation
  is = ir1;
  ir1 = ir2;
  ur1 = us - ur2;
  ir1 = ur1 / r1;
  ir2 = ur2 / r2;
end VoltageDivider;
