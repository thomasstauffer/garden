#!/usr/bin/env python3

"""

1 J = 1 W * s

Conduction

https://en.wikipedia.org/wiki/Thermal_conductivity

[k] = [W / (m * k)]

1D

Q/dt = -k*A*dT/dx

dt = Time Difference
dx = Distance between two points
dT = Temperature Difference

https://en.wikipedia.org/wiki/List_of_thermal_conductivities

Air 0.02
Water 0.6
Copper 401
Aluminium 237

Capacity

https://en.wikipedia.org/wiki/Heat_capacity

C = dQ/dT

dQ = "Wärmemenge" [J]
dT = Temperature Difference [°K]

J / K

Specific

J / (K * kg)


https://en.wikipedia.org/wiki/Table_of_specific_heat_capacities

Water 4181
Air 1003
Aluminium 897
Copper 385
"""

import matplotlib.pyplot as plot
import numpy
import scipy.integrate


R = 1.0  # [Ohm]
U = 24.0  # [V]
DC = 20  # [%]
P = (U ** 2) / R  # [W]

C = 200  # Heat Capacity [J/K]
G = 1  # Heat Conductance [W/K]
TEMP = 0  # [C]
T_END = 600.0  # [s]

def show(xlabel, x, ylabel, y):
	plot.figure(figsize=(16, 9), tight_layout=True)
	plot.xlabel(xlabel)
	plot.ylabel(ylabel)
	if type(x) == list:
		for i in range(len(x)):
			plot.plot(x[i], y[i])
	else:
		plot.plot(x, y)
	plot.grid()
	plot.show()


def heat():
	def p_at_t(t):
		if (t - numpy.floor(t)) > 0.2:
			p = 0
		else:
			p = P
		return -p


	def model_alg(x, t, cap_temp):
		p = p_at_t(t)

		cap_q, fix_q = x

		con_dtemp = cap_temp - TEMP

		con_q = G * con_dtemp

		con_a_q = con_q
		con_b_q = -con_q
		z0 = cap_q + p + con_a_q
		z1 = fix_q + con_b_q

		return [z0, z1]

	def solve_scipy(t, cap_temp):
		result = scipy.optimize.root(fun=model_alg, x0=[0.0, 0.0], args=(t, cap_temp))
		cap_q, fix_q = result.x
		return cap_q, fix_q

	def solve_manual(t, cap_temp):
		p = p_at_t(t)
		con_dtemp = cap_temp - TEMP
		con_q = G * con_dtemp
		cap_q = -p - con_q
		fix_q = -con_q
		return cap_q, fix_q

	def model_ode(t, y):
		cap_temp = y[0]
		cap_q, fix_q = solve_manual(t, cap_temp)
		cap_temp_diff = cap_q / C
		return [cap_temp_diff]

	def ode_scipy():
		result = scipy.integrate.solve_ivp(fun=model_ode, t_span=(0.0, T_END), y0=[TEMP], max_step=0.2)
		return result.t, result.y[0]

	def ode_euler():
		pass
		ts = numpy.linspace(0.0, T_END, int(50 * T_END))
		h = ts[1] - ts[0]
		ys = []

		cap_temp = TEMP
		for t in ts:
			cap_temp = cap_temp + (h * model_ode(t, [cap_temp])[0])
			ys += [cap_temp]

		return ts, ys

	t, y = ode_euler()
	#t, y = ode_scipy()

	show('t', t, 's(t)', y)

#heat()


def hs(kp, ki):
	dt = 0.01
	t = 0.0

	ts = []
	cap_temps = []
	cap_temp = TEMP

	pi_dt = 0.0

	#kp = 0.01
	#ki = 0.001
	p_i = 0.0

	p_dt = 5.0

	temp_set = 100.0
	out = 0.0

	while t < 600.0:
		# PI

		if pi_dt > p_dt:
			pi_dt = 0.0

			temp_actual = cap_temp

			temp_err = temp_set - temp_actual
			p_i += temp_err * p_dt

			out = (temp_err * kp) + (p_i * ki)

			out = int(out * 16) / 16  # bit reduction

			if out < 0.0:
				out = 0.0
			if out > 1.0:
				out = 1.0

			print(out)

		p = -P * out

		# Model

		con_dtemp = cap_temp - TEMP
		con_q = G * con_dtemp
		cap_q = -p - con_q
		fix_q = -con_q
		cap_temp_diff = cap_q / C
		cap_temp = cap_temp + (dt * cap_temp_diff)

		# Log

		ts += [t]
		cap_temps += [cap_temp]

		# Step

		t += dt
		pi_dt += dt

	return ts, cap_temps

def heat_sim():

	"""
	ts0, cap_temps0 = hs(0.02, 0.0)
	ts1, cap_temps1 = hs(0.02, 0.01)
	ts2, cap_temps2 = hs(0.02, 0.02)
	ts3, cap_temps3 = hs(0.02, 0.04)
	"""

	ts0, cap_temps0 = hs(0.005, 0.00)
	ts1, cap_temps1 = hs(0.005, 0.00001)
	ts2, cap_temps2 = hs(0.005, 0.00002)
	ts3, cap_temps3 = hs(0.005, 0.00004)

	show('t [s]', [numpy.array(ts0), numpy.array(ts1), numpy.array(ts2), numpy.array(ts3)], 'cap_temp [°C]', [numpy.array(cap_temps0), numpy.array(cap_temps1), numpy.array(cap_temps2), numpy.array(cap_temps3)])

heat_sim()

