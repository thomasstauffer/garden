model HeatCapacityConvection
  Modelica.Electrical.Analog.Sources.PulseVoltage pulse(V = 24, period = 30, width = 50) annotation(
    Placement(visible = true, transformation(origin = {-150, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-150, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor res(R = 10, useHeatPort = true) annotation(
    Placement(visible = true, transformation(origin = {-90, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Thermal.HeatTransfer.Components.HeatCapacitor cap_alu(C = 200, T(displayUnit = "degC", start = 273.15)) annotation(
    Placement(visible = true, transformation(origin = {-60, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature air(T(displayUnit = "degC") = 273.15) annotation(
    Placement(visible = true, transformation(origin = {70, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Thermal.HeatTransfer.Components.ThermalConductor conduct_alu_water(G = 10) annotation(
    Placement(visible = true, transformation(origin = {-30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Components.HeatCapacitor cap_water(C = 100, T(displayUnit = "degC", start = 273.15))  annotation(
    Placement(visible = true, transformation(origin = {0, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Components.ThermalConductor conduct_water_air(G = 1)  annotation(
    Placement(visible = true, transformation(origin = {30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(ground.p, res.p) annotation(
    Line(points = {{-150, -40}, {-90, -40}, {-90, -20}}, color = {0, 0, 255}));
  connect(res.heatPort, cap_alu.port) annotation(
    Line(points = {{-80, -10}, {-60, -10}, {-60, 0}}, color = {191, 0, 0}));
  connect(pulse.p, res.n) annotation(
    Line(points = {{-150, 0}, {-150, 20}, {-90, 20}, {-90, 0}}, color = {0, 0, 255}));
  connect(pulse.n, ground.p) annotation(
    Line(points = {{-150, -20}, {-150, -40}}, color = {0, 0, 255}));
  connect(conduct_alu_water.port_a, cap_alu.port) annotation(
    Line(points = {{-40, -10}, {-60, -10}, {-60, 0}}, color = {191, 0, 0}));
  connect(conduct_alu_water.port_b, cap_water.port) annotation(
    Line(points = {{-20, -10}, {0, -10}, {0, 0}}, color = {191, 0, 0}));
  connect(conduct_alu_water.port_b, conduct_water_air.port_a) annotation(
    Line(points = {{-20, -10}, {20, -10}}, color = {191, 0, 0}));
  connect(conduct_water_air.port_b, air.port) annotation(
    Line(points = {{40, -10}, {60, -10}}, color = {191, 0, 0}));
  annotation(
    uses(Modelica(version = "4.0.0")),
  Diagram(coordinateSystem(extent = {{-200, -100}, {200, 100}})),
  version = "");
end HeatCapacityConvection;