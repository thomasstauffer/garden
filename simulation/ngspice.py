#!/usr/bin/env python3

'''
* PULSE(<VL> <VH> <TD> <TR> <TF> <PW> <PER> <PHASE>)
'''

import os
import subprocess
import tempfile
import numpy

def simulate(circuit, variables, t_step, t_end):
	# TODO with tempfile.NamedTemporaryFile() as fp
	filename = tempfile.mkstemp(prefix='ngspice')[1]
	stdin = f'''
	source {filename}
	; op
	tran {t_step}s {t_end}s
	'''.encode()
	for var in variables:
		stdin += b'print ' + var.encode() + b'\n'
	circuit = circuit.strip().replace('\t', '')
	open(filename, 'w').write(circuit)
	process = subprocess.Popen(['ngspice', '-p'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate(stdin)
	os.remove(filename)
	assert len(stderr) == 0, stderr
	lines = stdout.decode('ascii').split('\n')
	for i, line in enumerate(lines[:100]):
		if 'No. of Data Rows' in line:
			start = i + 1
	lines = lines[start:-2]
	time = []
	values = []
	nth = -1
	for line in lines:
		line = line.strip()
		if len(line) == 0:
			continue
		if line == '\x0c':
			continue
		if line.startswith('---'):
			continue
		if line.startswith('Index'):
			continue
		if line.startswith('netlist'):
			nth += 1
			values += [[]]
			continue
		if line.startswith('Transient Analysis'):
			continue
		line_index, line_time, line_value = line.strip().split('\t')
		if nth == 0:
			time += [float(line_time)]
		values[nth] += [float(line_value)]
	results = [numpy.array(time)] + [numpy.array(x) for x in values]
	return results

def test():
	CIRCUIT = '''netlist
	vu a 0 10
	rr1 a b 1k
	rr2 b 0 4k
	.end
	'''

	result = simulate(CIRCUIT, ['a', 'b'])
	assert result == [10.0, 8.0]

if __name__ == '__main__':
	test()
