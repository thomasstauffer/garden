model Rectifier

function Diode1
  input Real u;
  output Real i;
  protected constant Real a = 1e-6;
  protected constant Real b = 0.04;
  protected constant Real r = 1e8;
algorithm
  i := exp((u - 0.6) / 0.1);
end Diode1;

function Diode2
  input Real u;
  output Real i;
  protected constant Real a = 1e-6;
  protected constant Real b = 0.04;
  protected constant Real r = 1e8;
algorithm
  i := (a * (exp(u/b) - 1.0)) + (u/r);
end Diode2;

  constant Real pi = Modelica.Constants.pi;
  constant Real f = 1.0;
  constant Real us_peak = 2.0;
  Real us;
  Real is;
  constant Real r = 100.0;
  Real ur;
  Real ir;
  Real ud1;
  Real id1;
  Real ud2;
  Real id2;
  Real ud3;
  Real id3;
  Real ud4;
  Real id4;
  Real ua;
  Real ub;
  Real uc;
  Real ud;
  constant Real a = 1e-6;
  constant Real b = 0.04;
  constant Real c = 1;
  constant Real d = 1e8;

equation
  us = sin(2.0 * pi * time * f) * us_peak;

  is = (-id1) + id2;
  (-id1) = id3 + (-ir);
  id2 + (-ir) = (-id4);
  
  ua = us;
  ud = 0;
  ua - uc = ud2;
  ub - ua = ud1;
  ub - ud = ud3;
  ud - uc = ud4;
  uc - ub = ur;
  
  ir = ur / r;
  id1 = Diode2(ud1);
  id2 = Diode2(ud2);
  id3 = Diode2(ud3);
  id4 = Diode2(ud4);
end Rectifier;
