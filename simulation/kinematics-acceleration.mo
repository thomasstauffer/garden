model KinematicsAcceleration
  Real a;
  Real v;
  Real s;
initial equation
  v = 0;
  s = 0;
equation
  a = if time < 1 then 1 else (if time < 2 then 0 else -1);
  der(v) = a;
  der(s) = v;
end KinematicsAcceleration;
