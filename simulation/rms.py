#!/usr/bin/env python3

import sympy

def sin():
	t, u, n = sympy.symbols('t u n')
	e1 = sympy.sin(t * 2 * sympy.pi)
	sympy.textplot(e1, 0, 2, W=50, H=10)
	e2 = sympy.integrate((u * e1) ** 2, (t, 0, 1))
	e3 = sympy.Eq(e2, (u / n) ** 2)
	e4 = sympy.solve(e3, n)[1]
	print(e4)

sin()  # -> sqrt(2)

def saw():
	t, u, n = sympy.symbols('t u n')
	e1 = t - sympy.floor(t)  # or e1 = t % 1.0
	e1_simplified = t  # only valid from 0..1
	sympy.textplot(e1, 0, 2, W=50, H=10)
	e2 = sympy.integrate((u * e1_simplified) ** 2, (t, 0, 1))
	e3 = sympy.Eq(e2, (u / n) ** 2)
	e4 = sympy.solve(e3, n)[1]
	print(e4)

saw()  # -> sqrt(3)
