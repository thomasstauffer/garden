#!/usr/bin/env python3

'''
  +-----------+ a
  |           |
  |         +-+-+
  |         |   |
  |       R |   |
  |         |   |
  |         +-+-+
+-+-+         |
              | b
 +++          |
  |           |
  |         +-+-+
  |       C
  |         +-+-+
  |           |
  |           |
  +-----------+ c

C

ic = C * diff(uc(t), t)
diff(uc(t), t) = ic / C
'''

import matplotlib.pyplot as plot
import numpy
import scipy.integrate
import sympy
import ngspice
import om
import components


R = 1000.0
C = 100e-6
L = 1e2
T_END = 3.0
US_PEAK = 1.0
US_RISE_FALL_T = 0.001
vpulse = components.vpulse(dc_off=0.0, dc_on=US_PEAK, frequency=1.0/2.0, delay=1.0, rise=US_RISE_FALL_T, fall=US_RISE_FALL_T)
vpulse_diff = components.vpulse_diff(dc_off=0.0, dc_on=US_PEAK, frequency=1.0/2.0, delay=1.0, rise=US_RISE_FALL_T, fall=US_RISE_FALL_T)


def serial_rc_num():
	def model(t, y):
		'''
		ic = (us - uc(t)) / R
		diff(uc(t), t) = (us - uc) / (R * C)
		'''
		uc = y
		duc = (vpulse(t) - uc) / (R * C)
		return duc

	uc0 = 0.0
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, T_END), y0=[uc0], max_step=0.01)
	return result.t, result.y[0]


def serial_rc_num2():
	'''
	ic = C * diff(uc(t), t)
	'''

	# TODO rewrite nonlocal
	NUM = 500
	T_STEP = T_END / (NUM - 1)
	uc_prev, is_prev = 0.0, 0.0
	def system(t):
		nonlocal uc_prev, is_prev
		us = vpulse(t)

		def model(x):
			uc, is_ = x
			ir = ((us - uc) / R)
			duc = (uc - uc_prev) / T_STEP
			ic = C * duc
			f0 = is_ - ir
			f1 = ir - ic
			return [f0, f1]

		result = scipy.optimize.root(fun=model, x0=[uc_prev, is_prev])
		uc_prev, is_prev = result.x
		return result.x[0]

	t = numpy.linspace(start=0.0, stop=T_END, num=NUM)
	uc = numpy.vectorize(system)(t)
	return t, uc


def serial_rc_sym():
	pass


def serial_rc_spice():
	circuit = f'''
	netlist
	v a 0 DC 0 PULSE (0v {US_PEAK}v 1s 1us 1us 1s)
	r a b {R}
	c b 0 {C}
	.end
	'''
	t, uc = ngspice.simulate(circuit=circuit, variables=['b'], t_step=0.01, t_end=T_END)
	return t, uc


def serial_rc_om():
	MODEL = '''
	model SerialRC
	constant Real r = 1000.0;
	constant Real c = 100e-6;
	Real us;
	Real uc;
	Real ic;
	Real ur;
	Real ir;
	initial equation
	uc = 0;
	equation
	us = if time < 1 then 0 else (if time < 2 then 1 else 0);
	ir = ic;
	ur = us - uc;
	ur = r * ir;
	der(uc) = ic / c;
	end SerialRC;
	'''
	return om.simulate(MODEL, 'SerialRC', ['uc'], t_step=0.01, t_end=T_END)


def serial_rl_num():
	def model(t, y):
		'''
		L

		ul = L * diff(il(t), t)
		diff(il(t), t) = ul / L

		RL

		ul = us - (il * R)
		diff(il(t), t) = (us - (il * R)) / L
		'''
		il = y
		dil = (vpulse(t) - (il * R)) / L
		return dil

	il0 = 0.0
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, T_END), y0=[il0], max_step=0.001)

	ils = result.y[0]
	urs =  ils * R
	ucs = numpy.vectorize(vpulse)(result.t)
	uls = ucs - urs

	return result.t, uls


def serial_rl_num2():
	def model(t, y):
		'''
		L

		ul = L * diff(il(t), t)
		diff(il(t), t) = ul / L

		il = 1 / L * int(ul(t), t)

		RL

		(us(t) - ul(t)) / R = 1/L * int(ul(t), t)
		-> * L, L / R -> LR
		(us(t) - ul(t)) * LR = int(ul(t), t)
		-> diff
		diff(us(t), t) * LR - diff(ul(t), t) * LR = ul(t)
		-> LSH <-> RHS
		diff(ul(t), t) = (diff(us(t), t) * LR - ul(t)) / LR
		'''
		LR = L / R
		us_, ul = y
		dus = vpulse_diff(t)
		dul = (dus * LR - ul) / LR
		return [dus, dul]

	us0 = 0.0
	ul0 = 0.0
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, T_END), y0=[us0, ul0], max_step=0.001)
	return result.t, result.y[1]


def serial_rl_sym():
	pass


def serial_rl_spice():
	circuit = f'''
	netlist
	v a 0 DC 0 PULSE (0v {US_PEAK}v 1s 1us 1us 1s)
	r a b {R}
	l b 0 {L}
	.end
	'''
	t, ul = ngspice.simulate(circuit=circuit, variables=['b'], t_step=0.01, t_end=T_END)
	return t, ul


def serial_rl_om():
	MODEL = '''
	model SerialRL
	constant Real r = 1000.0;
	constant Real l = 1e2;
	Real us;
	Real ul;
	Real il;
	Real ur;
	Real ir;
	initial equation
	il = 0;
	equation
	us = if time < 1 then 0 else (if time < 2 then 1 else 0);
	ir = il;
	ur = us - ul;
	ur = r * ir;
	der(il) = ul / l;
	end SerialRL;
	'''
	return om.simulate(MODEL, 'SerialRL', ['ul'], t_step=0.01, t_end=T_END)


def serial():
	methods = [serial_rc_num, serial_rc_num2, serial_rc_spice, serial_rc_om, serial_rl_num, serial_rl_num2, serial_rl_spice, serial_rl_om]
	plot.figure(figsize=(16, 9), tight_layout=True)
	t = numpy.linspace(start=0.0, stop=T_END, num=100)
	us = numpy.vectorize(vpulse)(t)
	plot.plot(t, us, label='us')

	for i, method in enumerate(methods):
		translate_view = i * 0.01
		t, u = method()
		plot.plot(t + translate_view, u, label=method.__name__)
	plot.grid()
	plot.legend()
	plot.show()


serial()
