model SerialRD
  constant Real pi = Modelica.Constants.pi;
  constant Real us_peak = 2.0;
  Real us;
  Real is;
  constant Real r = 100.0;
  Real ur;
  Real ir;
  Real ud;
  Real id;
  
function Diode
  input Real u;
  output Real i;
  protected constant Real is = 1e-7;
  protected constant Real vt = 0.05;
algorithm
  i := (is * (exp(u / vt) - 1.0));
end Diode;

equation
  us = sin(2.0 * pi * time) * us_peak;
  is = ir;
  ir = id;
  ur = us - ud;
  ir = ur / r;
  id = Diode(ud);
end SerialRD;
