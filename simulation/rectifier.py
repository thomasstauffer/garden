#!/usr/bin/env python3

import matplotlib.pyplot as plot
import numpy
import scipy.integrate
import scipy.optimize
import sympy
import ngspice
import om
import components


R = 100.0
US_PEAK = 4.0
T_END = 2.0
vsin = components.vsin(dc=0.0, ac=US_PEAK, frequency=1.0)


def rectifier_num():
	'''
	KCL

	a: is + id1 - id2 = 0
	b: ir - id1 - id3 = 0
	c: id2 + id4 - ir = 0

	a: is + d(ud1) - d(ud2) = 0
	b: ir - d(ud1) - d(ud3) = 0
	c: d(ud2) + d(ud4) - (ur/R) = 0

	ur = uc - ub
	ud1 = ub - us
	ud2 = us - uc
	ud3 = ub
	ud4 = -uc
	a: is + d(ud1) - d(ud2) = 0
	b: (ur/R) - d(ud1) - d(ud3) = 0
	c: d(ud2) + d(ud4) - (ur/R) = 0
	'''

	d = components.diode_shockley

	def system(t):
		def model(x):
			ub, uc, is_ = x
			us = vsin(t)
			ur = uc - ub
			ud1 = ub - us
			ud2 = us - uc
			ud3 = ub
			ud4 = -uc
			a = is_ + d(ud1) - d(ud2)
			b = (ur/R) - d(ud1) - d(ud3)
			c = d(ud2) + d(ud4) - (ur/R)
			return [a, b, c]
		return model

	num = 300
	t_step = T_END / num

	ts = []
	urs = []

	# TODO rewrite and use vectorize?
	ub, uc, is_ = 0.0, 0.0, 0.0
	for i in range(num):
		t = i * t_step
		ub, uc, is_ = scipy.optimize.root(fun=system(t), x0=[ub, uc, is_]).x
		ud = uc - ub

		ts += [t]
		urs += [ud]

	return numpy.array(ts), numpy.array(urs)


def rectifier_num2():
	def linearize_diff(f, fdiff, x0):
		a = fdiff(x0)
		y = f(x0)
		b = y - a * x0
		return a, b

	'''
	a: is + (ad1*(ub-us)+bd1) - (ad2*(us-uc)+bd2) = 0
	b: ((uc-ub)*G) - (ad1*(ub-us)+bd1) - (ad3*ub+bd3) = 0
	c: (ad2*(us-uc)+bd2) + (ad4*-uc+bd4) - ((uc-ub)*G) = 0

	Expand

	a: ad1*ub  + ad2*uc + is - ad2*us - ad1*us - bd2 + bd1 = 0
	b: - ad3*ub - ad1*ub - G*ub + G*uc + ad1*us - bd3 - bd1 = 0
	c: G*ub - ad4*uc - ad2*uc - G*uc + ad2*us + bd4 + bd2 = 0
	'''

	# TODO rewrite without nonlocal
	G = 1 / R
	ud1_prev, ud2_prev, ud3_prev, ud4_prev = 0.0, 0.0, 0.0, 0.0
	def system(t):
		nonlocal ud1_prev, ud2_prev, ud3_prev, ud4_prev
		ad1, bd1 = linearize_diff(components.diode_shockley, components.diode_shockley_diff, ud1_prev)
		ad2, bd2 = linearize_diff(components.diode_shockley, components.diode_shockley_diff, ud2_prev)
		ad3, bd3 = linearize_diff(components.diode_shockley, components.diode_shockley_diff, ud3_prev)
		ad4, bd4 = linearize_diff(components.diode_shockley, components.diode_shockley_diff, ud4_prev)
		us = vsin(t)
		lhs = numpy.array([
			[ad1, ad2, 1],
			[-ad3 - ad1 - G, G, 0],
			[G, -ad4 - ad2 - G, 0],
		])
		rhs = numpy.array([
			-(-ad2*us - ad1*us - bd2 + bd1),
			-(ad1*us - bd3 - bd1),
			-(ad2*us + bd4 + bd2),
		])
		ub, uc, is_ = numpy.linalg.solve(lhs, rhs)
		ud1, ud2, ud3, ud4, ur = ub - us, us - uc, ub, -uc, uc - ub
		ud1_prev, ud2_prev, ud3_prev, ud4_prev = ud1, ud2, ud3, ud4
		return ur

	t = numpy.linspace(start=0.0, stop=T_END, num=300)
	ud = numpy.vectorize(system)(t)
	return t, ud


def rectifier_spice():
	circuit = f'''
	netlist
	v a 0 DC 0 SIN (0v {US_PEAK}v 1Hz)
	r c b {R}
	d1 b a
	d2 a c
	d3 b 0
	d4 0 c
	.end
	'''
	t, ub, uc = ngspice.simulate(circuit=circuit, variables=['b', 'c'], t_step=0.01, t_end=T_END)
	ur = uc - ub
	return t, ur


def rectifier_om():
	MODEL = f'''
	model Rectifier

	function Diode
	input Real u;
	output Real i;
	protected constant Real is = 10e-9;
	protected constant Real vt = 0.05;
	algorithm
	i := (is * (exp(u / vt) - 1.0));
	end Diode;

	constant Real pi = {numpy.pi};
	constant Real us_peak = {US_PEAK};
	Real us;
	Real is;
	constant Real r = {R};
	Real ur;
	Real ir;
	Real ud1;
	Real id1;
	Real ud2;
	Real id2;
	Real ud3;
	Real id3;
	Real ud4;
	Real id4;
	Real ua;
	Real ub;
	Real uc;
	Real ud;
	constant Real a = 1e-6;
	constant Real b = 0.04;
	constant Real c = 1;
	constant Real d = 1e8;

	equation
	us = sin(2.0 * pi * time) * us_peak;

	is = (-id1) + id2;
	(-id1) = id3 + (-ir);
	id2 + (-ir) = (-id4);

	ua = us;
	ud = 0;
	ua - uc = ud2;
	ub - ua = ud1;
	ub - ud = ud3;
	ud - uc = ud4;
	uc - ub = ur;

	ir = ur / r;
	id1 = Diode(ud1);
	id2 = Diode(ud2);
	id3 = Diode(ud3);
	id4 = Diode(ud4);
	end Rectifier;
	'''
	return om.simulate(MODEL, 'Rectifier', ['ur'], t_step=0.01, t_end=T_END)


def main():
	methods = [rectifier_num, rectifier_num2, rectifier_spice, rectifier_om]
	plot.figure(figsize=(16, 9), tight_layout=True)
	t = numpy.linspace(start=0.0, stop=T_END, num=100)
	us = numpy.vectorize(vsin)(t)
	plot.plot(t, us, label='us')
	for i, method in enumerate(methods):
		t, u = method()
		plot.plot(t, u, label=method.__name__)
	plot.grid()
	plot.legend()
	plot.show()


main()
