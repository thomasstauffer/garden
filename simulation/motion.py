#!/usr/bin/env python3

import sympy
import math
import scipy.integrate
import matplotlib.pyplot as plot

'''
https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html
'''

def show(xlabel, x, ylabel, y):
	plot.figure(figsize=(16, 9), tight_layout=True)
	plot.xlabel(xlabel)
	plot.ylabel(ylabel)
	if type(x) == list:
		for i in range(len(x)):
			plot.plot(x[i], y[i])
	else:
		plot.plot(x, y)
	plot.grid()
	plot.show()

def velocity_from_acceleration():
	'''
	diff(v(t), t) = a(t)
	'''

	t_end = 10.0
	a = 10.0

	def symbolic1():
		tt, aa = sympy.symbols('tt aa')
		vv = sympy.Function('vv')
		expr = sympy.dsolve(sympy.Derivative(vv(tt), tt) - aa)
		return expr.rhs.subs('C1', 0).subs(aa, a).subs(tt, t_end)

	def symbolic2():
		t = sympy.symbols('t')
		expr = sympy.integrate(a, t)  # -> a*t
		return expr.subs(t, t_end)

	def model(t, y):
		return a

	v0 = 0.0
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=[v0])

	print(symbolic1())
	print(symbolic2())
	print(result.y[0][-1])
	show('t', result.t, 'v(t)', result.y[0])

#velocity_from_acceleration()

def position_from_acceleration():
	'''
	diff(v(t), t) = a(t)
	diff(s(t), t) = v(t)
	'''

	t_end = 10.0
	a = 10.0

	def symbolic1():
		tt, aa = sympy.symbols('tt aa')
		ss = sympy.Function('ss')
		expr = sympy.dsolve(sympy.Derivative(ss(tt), tt, tt) - aa)
		return expr.rhs.subs('C1', 0).subs('C2', 0).subs(aa, a).subs(tt, t_end)

	def symbolic2():
		t = sympy.symbols('t')
		expr = sympy.integrate(sympy.integrate(a, t), t)  # -> a*t**2/2
		return expr.subs(t, t_end)

	def model(t, y):
		s, v = y
		return [v, a]

	s0 = 0.0
	v0 = 0.0
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=[s0, v0], max_step=0.1)

	print(symbolic1())
	print(symbolic2())
	print(result.y[0][-1])
	show('t', result.t, 's(t)', result.y[0])

#position_from_acceleration()

def throw_ball():
	'''
	Second Order

	diff(diff(sx(t), t), t) = ax(t)
	diff(diff(sy(t), t), t) = ay(t)

	Separate First Order

	diff(vx(t), t) = ax(t)
	diff(vy(t), t) = ay(t)
	diff(sx(t), t) = vx(t)
	diff(sy(t), t) = vy(t)
	'''

	g = 9.81

	def model(t, y):
		sx, sy, vx, vy = y
		ax, ay = 0.0, -g
		return [vx, vy, ax, ay]

	sx, sy = [], []
	for angle_degree in [0, 30, 45, 60, 90]:
		angle = math.radians(angle_degree)
		sx0, sy0 = 0.0, 0.0
		v = 20.0
		vx0, vy0 = math.cos(angle) * v, math.sin(angle) * v
		result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, 5.0), y0=[sx0, sy0, vx0, vy0], max_step=0.01)
		sx.append(result.y[0])
		sy.append(result.y[1])

	show('x', sx, 'y', sy)

#throw_ball()

def position_from_power_vel_pow():
	'''
	E = (m * v**2) / 2
	'''

	m = 1000  # [kg]
	t_end = 30.0  # [s]
	p_on = 100e3  # W ~= 134 hp

	def symbolic1():
		tt = sympy.symbols('tt')
		ss = sympy.Function('ss')
		expr = sympy.dsolve(sympy.Derivative(ss(tt), tt) - sympy.sqrt(p_on * tt * 2 / m))
		return expr.rhs.subs('C1', 0).subs(tt, t_end)

	def symbolic2():
		t = sympy.symbols('t')
		eq = sympy.integrate(sympy.sqrt(p_on * t * 2 / m), t)  # -> 9.428*t**(3/2)
		return eq.subs(t, t_end)  # -> 1549.1933

	def model(t, y):
		s, e = y
		v = math.sqrt(e * 2 / m)
		p = p_on
		return [v, p]

	s0 = 0.0
	e0 = 1e-30
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=[s0, e0], max_step=0.1)

	print(symbolic1())
	print(symbolic2())
	print(result.y[0][-1])
	show('t', result.t, 's(t)', result.y[0])

#position_from_power_vel_pow()

def position_from_power_vel_pow_via_force():
	'''
	E = (m * v**2) / 2
	P = F * v
	'''

	m = 1000  # [kg]
	t_end = 30.0  # [s]
	p_on = 100e3  # W ~= 134 hp

	def model(t, y):
		s, e = y

		v = math.sqrt(e * 2 / m)
		fengine = p_on / v
		fresist = 0.0  # various resistances which depend on v
		f = fengine - fresist
		p = f * v

		return [v, p]

	s0 = 0.0
	e0 = 1e-30
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=[s0, e0], max_step=0.1)

	print(result.y[0][-1])

	show('t', result.t, 's(t)', result.y[0])

#position_from_power_vel_pow_via_force()

def position_from_power_vel_acc():
	m = 1000  # [kg]
	t_end = 30.0  # [s]
	p_on = 100e3  # W ~= 134 hp

	def symbolic1():
		tt = sympy.symbols('tt')
		ss = sympy.Function('ss')
		vv = sympy.Function('vv')
		eq1 = sympy.Derivative(vv(tt), tt) - (p_on / (vv(tt) * m))  # vv(tt) = 14.142*sqrt(tt))
		res1 = sympy.dsolve(eq1)[1].rhs.subs('C1', 0)
		eq2 = sympy.Derivative(ss(tt), tt) - res1
		res2 = sympy.dsolve(eq2).rhs.subs('C1', 0).subs(tt, t_end)  # ss(tt) = 9.428*tt**(3/2)
		return res2

	def symbolic2():
		# TODO how to do it with integration? manual substitution like in the previous example
		return 0

	def model(t, y):
		s, v = y
		f = p_on / v
		a = f / m
		return [v, a]

	s0 = 0.0
	v0 = 1e-30
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=[s0, v0], max_step=0.1)

	print(symbolic1())
	print(symbolic2())
	print(result.y[0][-1])
	show('t', result.t, 's(t)', result.y[0])

#position_from_power_vel_acc()
