#!/usr/bin/env python3

'''
https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html

diff(s(t), t) = v(t)
'''

import matplotlib.pyplot as plot
import numpy
import scipy.integrate
import sympy

MAX_STEP = 10000.0
T_END = 100.0
S0 = 0.0

def v(t):
	return numpy.sin(t) + t

def plot_v():
	t = numpy.linspace(start=0.0, stop=T_END, num=1000)
	y = v(t)
	plot.plot(t, y)
	plot.grid()
	plot.show()

# plot_v()

def symbolic():
	t = sympy.symbols('t')
	s = sympy.Function('s')
	expr = sympy.dsolve(sympy.Derivative(s(t), t) - (sympy.sin(t) + t))
	return expr.rhs.subs('C1', 0).subs(t, T_END)

def model(t, y):
	return v(t)

def whole():
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, T_END), y0=[S0], max_step=MAX_STEP)
	return result.y[0][-1]

def stepwise():
	s = S0
	t_step = T_END / 5.0
	t = 0.0
	while t < T_END:
		result = scipy.integrate.solve_ivp(fun=model, t_span=(t, t + t_step), y0=[s], max_step=MAX_STEP)
		s = result.y[0][-1]
		assert result.t[0] == t
		assert result.t[-1] == t + t_step
		t += t_step
	assert t == T_END
	return s

print(symbolic())
print(whole())
print(stepwise())
