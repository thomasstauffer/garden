#!/usr/bin/env python3

'''
  +-----------+ a
  |           |
  |         +---+
  |         |   |
  |      R1 |   | 1kOhm
  |         |   |
  |         +---+
+---+         |
              | b
 +-+          |
  |         +---+
  |         |   |
  |      R2 |   | 4kOhm
  |         |   |
  |         +---+
  |           |
  +-----------+ c

KCL

a: is + -ir1 = 0
b: ir1 + -ir2 = 0

Substitute ir1/ir2

a: is - ((us-ub)/r1) = 0
b: ((us-ub)/r1) - (ub/r2) = 0

Substitute r

a: is - ((us-ub)*g1) = 0
b: ((us-ub)*g1) - (ub*g2) = 0

Factorization

a: us*-g1 + ub*g1 + 1*is = 0
b: us*g1 + ub*(-g1-g2) + 0*is = 0

Move us->

a: ub*g1 + 1*is = us*g1
b: ub*(-g1-g2) + 0*is = -us*g1
'''

import numpy
import sympy
import ngspice
import om

R1 = 1000.0
R2 = 4000.0
US = 10.0


def voltage_divider_num():
	g1 = 1 / R1
	g2 = 1 / R2

	l = numpy.array([
		[g1, 1],
		[-g1-g2, 0],
	])

	r = numpy.array([
		US*g1,
		-US*g1,
	])

	result = numpy.linalg.solve(l, r)
	return result[0]


def voltage_divider_sym():
	ub, is_ = sympy.symbols('ub is')

	a = sympy.Eq(is_ - ((US-ub)/R1), 0)
	b = sympy.Eq(((US-ub)/R1) - (ub/R2), 0)

	result = sympy.solve((a, b), ub, is_)
	return float(result[ub])


def voltage_divider_spice():
	circuit = f'''
	netlist
	vu a 0 {US}
	rr1 a b {R1}
	rr2 b 0 {R2}
	.end
	'''
	return ngspice.simulate(circuit=circuit, variables=['b'], t_step=0.1, t_end=1.0)[1][-1]

def voltage_divider_om():
	MODEL = f'''
	model VoltageDivider
	// u [V] / i [A] / r [Ohm]
	Real us = {US};
	Real is;
	Real r1 = {R1};
	Real ur1;
	Real ir1;
	Real r2 = {R2};
	Real ur2;
	Real ir2;
	equation
	is = ir1;
	ir1 = ir2;
	ur1 = us - ur2;
	ir1 = ur1 / r1;
	ir2 = ur2 / r2;
	end VoltageDivider;
	'''
	return float(om.simulate(MODEL, 'VoltageDivider', ['ur2'], t_step=0.1, t_end=1.0)[1][-1])


def main():
	# -> 8.0
	print(voltage_divider_num())
	print(voltage_divider_sym())
	print(voltage_divider_spice())
	print(voltage_divider_om())


main()
