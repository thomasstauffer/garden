#!/usr/bin/env python3

import matplotlib.pyplot as plot
import numpy


def vsin(dc, ac, frequency):
	omega = 2.0 * numpy.pi * frequency

	def model(t):
		return numpy.sin(omega * t) * ac + dc
	return model


def vsin_diff(dc, ac, frequency):
	omega = 2.0 * numpy.pi * frequency

	def model(t):
		return omega * numpy.cos(omega * t) * ac
	return model


def diode_shockley(u, is_=10e-9, vt=0.05):
	i = is_ * (numpy.exp(u / vt) - 1.0)
	return i


# TODO this is wrong, du is missing, fix or remove and fix everywhere where it is used (rectifier, serial-rd, ...), is there a lower error if it is fixed?
def diode_shockley_diff(u, is_=10e-9, vt=0.05):
	i = (is_ * numpy.exp(u / vt)) / vt
	return i


def vpulse(dc_off, dc_on, frequency, delay, rise, fall):
	t_full = (1.0 / frequency)
	t_half = t_full / 2.0
	assert (rise + fall) < t_full

	def model(t):
		if t < delay:
			return dc_off
		else:
			t = (t - delay) % t_full
			if t < rise:
				a = (dc_on - dc_off) / rise
				return a * t + dc_off
			elif t < (t_half):
				return dc_on
			elif t < (t_half + fall):
				a = -(dc_on - dc_off) / fall
				return dc_on + a * (t - t_half)
			else:
				return dc_off
	return model


def vpulse_diff(dc_off, dc_on, frequency, delay, rise, fall):
	'''
	vpulse_diff(t) = diff(vpulse(t), t)
	'''
	t_full = (1.0 / frequency)
	t_half = t_full / 2.0
	assert (rise + fall) < t_full

	def model(t):
		if t < delay:
			return 0
		else:
			t = (t - delay) % t_full
			if t < rise:
				a = (dc_on - dc_off) / rise
				return a
			elif t < (t_half):
				return 0
			elif t < (t_half + fall):
				a = -(dc_on - dc_off) / fall
				return a
			else:
				return 0
	return model


def main():
	t = numpy.linspace(start=0.0, stop=3.0, num=91)
	plot.figure(figsize=(16, 9), tight_layout=True)
	plot.plot(t, numpy.vectorize(vpulse(dc_off=1.0, dc_on=2.0, frequency=1.0, delay=1.0, rise=0.2, fall=0.4))(t), label='vpulse', marker='.')
	plot.plot(t, numpy.vectorize(vpulse_diff(dc_off=1.0, dc_on=2.0, frequency=1.0, delay=1.0, rise=0.2, fall=0.4))(t), label='vpulse_diff', marker='.')
	plot.plot(t, numpy.vectorize(vsin(dc=0.0, ac=1.0, frequency=1.0))(t), label='vsin', marker='.')
	plot.plot(t, numpy.vectorize(vsin_diff(dc=0.0, ac=1.0, frequency=1.0))(t), label='vsin_diff', marker='.')
	plot.grid()
	plot.legend()
	plot.show()


if __name__ == '__main__':
	main()
