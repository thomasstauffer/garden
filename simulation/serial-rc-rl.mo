model SerialRCRL
  constant Real r = 1000.0;
  constant Real c = 100e-6;
  constant Real l = 1e2;
  Real us;
  //Real is;
  Real uc;
  Real ic;
  Real ur1;
  Real ir1;
  Real ul;
  Real il;
  Real ur2;
  Real ir2;
initial equation
  uc = 0;
  il = 0;
equation
  us = if time < 1 then 0 else (if time < 2 then 1 else 0);
  
  ir1 = ic;
  ur1 = us - uc;
  ur1 = r * ir1;
  der(uc) = ic / c;
  
  ir2 = il;
  ur2 = us - ul;
  ur2 = r * ir2;
  der(il) = ul / l;
end SerialRCRL;