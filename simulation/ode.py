#!/usr/bin/env python3

import matplotlib.pyplot as plot
import numpy
import scipy.integrate
import scipy.optimize


G = 6.674e-11
M_EARTH = 5.972e24
R_EARTH_MOON = 3.844e8
T_MOON = 27 * 24 * 3600
V_MOON = 2.0 * numpy.pi * R_EARTH_MOON / T_MOON
T_END = T_MOON

SX0, SY0 = R_EARTH_MOON, 0
VX0, VY0 = 0, V_MOON


def model_earth_moon(t, y):
	'''
	Newton's Universal Graviation
	f = g * ((m1 * m2) / r**2)
	'''
	sx, sy, vx, vy = y

	a = G * M_EARTH / (R_EARTH_MOON ** 2.0)
	s_length = numpy.sqrt((sx ** 2) + (sy ** 2))
	ax_dir, ay_dir = -sx / s_length, -sy / s_length  # pointing toward the center
	ax, ay = a * ax_dir, a * ay_dir

	return [vx, vy, ax, ay]


def run_scipy_solveivp(model, t_end, t_max_step, y0):
	result = scipy.integrate.solve_ivp(fun=model, t_span=(0.0, t_end), y0=y0, max_step=t_max_step)
	return result.y


def run_scipy_odeint(model, t_end, t_max_step, y0):
	def swap(y, t):
		return model(t, y)
	num = t_end / t_max_step
	result = scipy.integrate.odeint(func=swap, y0=y0, t=numpy.linspace(start=0.0, stop=t_end, num=num))
	return numpy.transpose(result)


def forward_euler(fun, t_end, t_step, y0):
	y = numpy.array(y0)
	result = [y]
	t = 0.0
	while t < t_end:
		y = y + (t_step * numpy.array(fun(t, y)))
		result += [y]
		t += t_step
	return numpy.transpose(result)


def run_forward_euler(model, t_end, t_max_step, y0):
	result = forward_euler(fun=model, t_end=t_end, t_step=t_max_step, y0=y0)
	return result


def heun(fun, t_end, t_step, y0):
	t_step2 = t_step / 2.0
	y = numpy.array(y0)
	result = [y]
	t = 0.0
	while t < t_end:
		g = numpy.array(fun(t, y))
		y_predict = y + t_step * g
		for i in range(2):
			y_next = y + t_step2 * (g + numpy.array(fun(t + t_step, y_predict)))
			err = (y_next - y_predict) / y_next
			y_predict = y_next
		y = y_next
		result += [y]
		t += t_step
	return numpy.transpose(result)


def run_heun(model, t_end, t_max_step, y0):
	result = heun(fun=model, t_end=t_end, t_step=t_max_step, y0=y0)
	return result


def modified_euler(fun, t_end, t_step, y0):
	t_step2 = t_step / 2.0
	y = numpy.array(y0)
	result = [y]
	t = 0.0
	while t < t_end:
		y_predict = y + (t_step2 * numpy.array(fun(t, y)))
		y = y + (t_step * numpy.array(fun(t + t_step2, y_predict)))
		result += [y]
		t += t_step
	return numpy.transpose(result)


def run_modified_euler(model, t_end, t_max_step, y0):
	result = modified_euler(fun=model, t_end=t_end, t_step=t_max_step, y0=y0)
	return result


def backward_euler(fun, t_end, t_step, y0):
	'''
	https://en.wikipedia.org/wiki/Backward_Euler_method

	y_n+1 = yn + h* f(t_n+1, y_n+1)
	'''

	y = numpy.array(y0)
	result = [y]
	t = 0.0

	def model(yn, t, y):
		return yn - (y + t_step * numpy.array(fun(t + t_step, yn)))

	while t < t_end:
		y = scipy.optimize.root(fun=model, x0=y, args=(t, y)).x
		result += [y]
		t += t_step
	return numpy.transpose(result)


def run_backward_euler(model, t_end, t_max_step, y0):
	result = backward_euler(fun=model, t_end=t_end, t_step=t_max_step, y0=y0)
	return result


def trapezoid(fun, t_end, t_step, y0):
	'''
	https://en.wikipedia.org/wiki/Trapezoidal_rule_(differential_equations)
	'''

	y = numpy.array(y0)
	result = [y]
	t = 0.0

	def model(yn, t, y):
		fy = numpy.array(fun(t, y))
		return yn - (y + (t_step / 2.0) * (fy + numpy.array(fun(t + t_step, yn))))

	while t < t_end:
		y = scipy.optimize.root(fun=model, x0=y, args=(t, y)).x
		result += [y]
		t += t_step

	result = numpy.transpose(result)
	return result


def run_trapezoid(model, t_end, t_max_step, y0):
	result = trapezoid(fun=model, t_end=t_end, t_step=t_max_step, y0=y0)
	return result


def run_fdm(model, t_end, t_max_step, y0):
	'''
	= Forward Euler?
	'''

	h = t_max_step
	n = int(t_end / h)
	y = [0] * n
	y[0] = numpy.array(y0)
	for i in range(0, n - 1):
		t = i * h
		y[i+1] = y[i] + (h * numpy.array(model(t, y[i])))
	return numpy.transpose(y)


def earth_moon():
	methods = [run_scipy_solveivp, run_scipy_odeint, run_forward_euler, run_heun, run_modified_euler, run_backward_euler, run_trapezoid, run_fdm]
	print(SX0, SY0)
	plot.figure(figsize=(9, 9), tight_layout=True)
	for i, method in enumerate(methods):
		translate_x = i * 1e5
		result = method(model_earth_moon, T_MOON, 500.0, [SX0, SY0, VX0, VY0])
		print(method.__name__, int(result[0][-1]), int(result[1][-1]))
		plot.plot(result[0] - translate_x, result[1], label=method.__name__)
	plot.grid()
	plot.legend()
	plot.show()


def model_expsin(t, y):
	'''
	y = exp(sin(t)) + t
	y' = exp(sin(t)) * cos(t) + 1
	'''
	return numpy.exp(numpy.sin(t)) * numpy.cos(t) + 1


def expsin():
	T_END = 10.0
	methods = [run_scipy_solveivp, run_scipy_odeint, run_forward_euler, run_heun, run_modified_euler, run_backward_euler, run_trapezoid]
	methods = [run_scipy_solveivp, run_scipy_odeint, run_forward_euler, run_heun, run_modified_euler, run_backward_euler, run_trapezoid, run_fdm]
	analytical = numpy.exp(numpy.sin(T_END)) + T_END
	print(analytical)
	for method in methods:
		result = method(model_expsin, T_END, 1e-2, [1.0])
		err = abs(result[0][-1] - analytical)
		print(method.__name__, result[0][-1], err)


#earth_moon()
expsin()
