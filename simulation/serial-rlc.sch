<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-60,1269,800,1.1,0,21>
  <Grid=10,10,1>
  <DataSet=lrc.dat>
  <DataDisplay=lrc.dpl>
  <OpenDisplay=1>
  <Script=lrc.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 100 540 0 0 0 0>
  <Vrect V1 1 100 210 18 -26 0 1 "1 V" 1 "1 s" 1 "1 s" 1 "1 ns" 0 "1 ns" 0 "0 ns" 0>
  <R R1 1 240 130 15 -26 0 1 "100 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <.TR TR1 1 360 480 0 65 0 0 "lin" 0 "0" 1 "4 s" 1 "1000" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <C C1 1 240 270 17 -26 0 1 "1 uF" 1 "" 0 "neutral" 0>
  <L L1 1 240 370 10 -26 0 1 "1 uH" 1 "" 0>
</Components>
<Wires>
  <240 20 240 100 "" 0 0 0 "">
  <100 240 100 540 "" 0 0 0 "">
  <100 20 240 20 "" 0 0 0 "">
  <100 20 100 180 "" 0 0 0 "">
  <240 160 240 240 "UC" 270 170 45 "">
  <240 300 240 340 "" 0 0 0 "">
  <240 400 240 540 "" 0 0 0 "">
  <100 540 240 540 "" 0 0 0 "">
  <240 20 240 20 "UV" 270 -10 0 "">
</Wires>
<Diagrams>
  <Rect 520 457 602 427 3 #c0c0c0 1 00 1 0 0.2 2 1 -0.1 0.2 1.1 1 -1 0.2 1 315 0 225 "" "" "">
	<"UV.Vt" #0000ff 0 3 0 0 0>
	<"UC.Vt" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
</Paintings>
