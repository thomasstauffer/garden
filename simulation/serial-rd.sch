<Qucs Schematic 0.0.20>
<Properties>
  <View=0,0,1058,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=diode.dat>
  <DataDisplay=diode.dpl>
  <OpenDisplay=1>
  <Script=diode.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Diode D1 1 360 330 15 -26 0 1 "1e-15 A" 1 "1" 1 "10 fF" 1 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.0 Ohm" 0 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <GND * 5 180 440 0 0 0 0>
  <.TR TR1 1 580 70 0 59 0 0 "lin" 1 "0" 1 "1 ms" 1 "100" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <R R1 1 360 210 15 -26 0 1 "100 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <Vac V1 1 180 250 18 -26 0 1 "2 V" 1 "1 kHz" 0 "0" 0 "0" 0>
</Components>
<Wires>
  <180 80 180 220 "" 0 0 0 "">
  <180 80 360 80 "" 0 0 0 "">
  <360 80 360 180 "" 0 0 0 "">
  <360 240 360 300 "VD" 390 250 36 "">
  <360 360 360 440 "" 0 0 0 "">
  <180 440 360 440 "" 0 0 0 "">
  <180 280 180 440 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 560 537 451 257 3 #c0c0c0 1 00 1 0 0.0001 0.001 1 -2.27762 1 1.35688 1 -1 0.5 1 315 0 225 "" "" "" "">
	<"VD.Vt" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
</Paintings>
