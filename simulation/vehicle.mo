model Vehicle
  Real e_kin;
  Real p_el;
  Real e_el;
  Real e_err;
  Modelica.Mechanics.Translational.Sensors.SpeedSensor velocity annotation(
    Placement(visible = true, transformation(origin = {50, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Sensors.PositionSensor position annotation(
    Placement(visible = true, transformation(origin = {50, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Sensors.AccSensor acceleration annotation(
    Placement(visible = true, transformation(origin = {50, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass(a(fixed = false, start = 0), m = 1000, s(fixed = false, start = 0), v(fixed = false, start = 0)) annotation(
    Placement(visible = true, transformation(origin = {10, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.IdealRollingWheel wheel(radius = 0.2) annotation(
    Placement(visible = true, transformation(origin = {-50, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia(J = 0, phi(displayUnit = "rad")) annotation(
    Placement(visible = true, transformation(origin = {-110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-210, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.RotationalEMF emf(k = 0.9)  annotation(
    Placement(visible = true, transformation(origin = {-170, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantCurrent current(I = 10)  annotation(
    Placement(visible = true, transformation(origin = {-210, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
equation
  e_kin = mass.m * (mass.v ^ 2) / 2;
  p_el = emf.v * emf.i;
  der(e_el) = p_el;
  e_err = e_kin - e_el;
  connect(mass.flange_b, position.flange) annotation(
    Line(points = {{20, 30}, {30, 30}, {30, -90}, {40, -90}}, color = {0, 127, 0}));
  connect(mass.flange_b, velocity.flange) annotation(
    Line(points = {{20, 30}, {32, 30}, {32, -50}, {40, -50}}, color = {0, 127, 0}));
  connect(mass.flange_b, acceleration.flange) annotation(
    Line(points = {{20, 30}, {34, 30}, {34, -10}, {40, -10}}, color = {0, 127, 0}));
  connect(wheel.flangeT, mass.flange_a) annotation(
    Line(points = {{-40, 30}, {0, 30}}, color = {0, 127, 0}));
  connect(inertia.flange_b, wheel.flangeR) annotation(
    Line(points = {{-100, 30}, {-60, 30}}));
  connect(emf.n, ground.p) annotation(
    Line(points = {{-170, 20}, {-170, -20}, {-210, -20}}, color = {0, 0, 255}));
  connect(emf.flange, inertia.flange_a) annotation(
    Line(points = {{-160, 30}, {-120, 30}}));
  connect(current.n, emf.p) annotation(
    Line(points = {{-210, 40}, {-210, 60}, {-170, 60}, {-170, 40}}, color = {0, 0, 255}));
  connect(current.p, ground.p) annotation(
    Line(points = {{-210, 20}, {-210, -20}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "4.0.0")),
    Diagram(coordinateSystem(extent = {{-300, 200}, {300, -200}}), graphics = {Text(origin = {-70, -30}, extent = {{-70, 10}, {70, -10}}, textString = "see Translational.Components.Vehicle
...", fontSize = 10, horizontalAlignment = TextAlignment.Left), Text(origin = {-140, 50}, extent = {{-20, 10}, {20, -10}}, textString = "torque [N/m]", fontSize = 10), Text(origin = {-20, 50}, extent = {{-20, 10}, {20, -10}}, textString = "force [N]", fontSize = 10), Text(origin = {90, 70}, extent = {{-70, 10}, {70, -10}}, textString = "wind + inclination + rollingresistance", fontSize = 10, horizontalAlignment = TextAlignment.Left)}),
    version = "");
end Vehicle;