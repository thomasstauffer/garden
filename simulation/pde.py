#!/usr/bin/env python3

import sympy
import numpy

'''
TODO stability
'''

'''
f(t, x) = x + a * t
diff(f, t) = a
diff(f, x) = 1
diff(f, t) / diff(f, x) = a
diff(t, t) = a * diff(f, x)

(f(t+1, x) - f(t, x)) / dt = a * ((f(t, x+1) - f(t, x - 1)) / (2 * dx))
'''

a = 0.4


def pde1_analytical(t, x):
	return x + a * t


def pde1_fdm():
	NUM_T = 101
	NUM_X = 101
	dt = 10.0 / (NUM_T - 1)
	dx = 10.0 / (NUM_X - 1)

	f = numpy.zeros((NUM_T, NUM_X))

	# BC & IC

	for it in range(NUM_T):
		t = it * dt
		f[it, 0] = pde1_analytical(t, 0.0)
		f[it, -1] = pde1_analytical(t, 10.0)
	for ix in range(NUM_X):
		x = ix * dx
		f[0, ix] = pde1_analytical(0.0, x)

	for it in range(0, NUM_T - 1):
		for ix in range(1, NUM_X - 1):
			t = it * dt
			x = ix * dx
			fxn = f[it, ix+1]
			fxp = f[it, ix-1]
			ft = f[it, ix]
			fxd = ((fxn - fxp) / (2 * dx))
			f[it+1, ix] = a * fxd * dt + ft

	def ev(t, x):
		it = int(t / dt)
		ix = int(x / dx)
		return f[it, ix]

	return ev

'''
f(t, x) = x + a * t
diff(f, t) = a
diff(f, x) = 1
diff(f, t) - diff(f, x) = a - 1
diff(t, t) = (a - 1) + diff(f, x)
'''

def pde2_analytical(t, x):
	return x + a * t


def pde2_fdm():
	NUM_T = 101
	NUM_X = 101
	dt = 10.0 / (NUM_T - 1)
	dx = 10.0 / (NUM_X - 1)

	f = numpy.zeros((NUM_T, NUM_X))

	# BC & IC

	for it in range(NUM_T):
		t = it * dt
		f[it, 0] = pde2_analytical(t, 0.0)
		f[it, -1] = pde2_analytical(t, 10.0)
	for ix in range(NUM_X):
		x = ix * dx
		f[0, ix] = pde2_analytical(0.0, x)

	for it in range(0, NUM_T - 1):
		for ix in range(1, NUM_X - 1):
			t = it * dt
			x = ix * dx
			fxn = f[it, ix+1]
			fxp = f[it, ix-1]
			ft = f[it, ix]
			fxd = ((fxn - fxp) / (2 * dx))
			f[it+1, ix] = (a - 1 + fxd) * dt + ft

	def ev(t, x):
		it = int(t / dt)
		ix = int(x / dx)
		return f[it, ix]

	return ev


'''
f(t, x) = x^2 + a * t
diff(f, t) = a
diff(f, x) = 2 * x
diff(f, t) / diff(f, x) = a / (2 * x)
'''


def pde3_analytical(t, x):
	'''
	TODO
	fails with
	x + a * t ** 2.0
	'''
	return x ** 2.0 + a * t


def pde3_fdm():
	NUM_T = 101
	NUM_X = 101
	dt = 10.0 / (NUM_T - 1)
	dx = 10.0 / (NUM_X - 1)

	f = numpy.zeros((NUM_T, NUM_X))

	# BC & IC

	for it in range(NUM_T):
		t = it * dt
		f[it, 0] = pde3_analytical(t, 0.0)
		f[it, -1] = pde3_analytical(t, 10.0)
	for ix in range(NUM_X):
		x = ix * dx
		f[0, ix] = pde3_analytical(0.0, x)

	for it in range(0, NUM_T - 1):
		for ix in range(1, NUM_X - 1):
			t = it * dt
			x = ix * dx
			fxn = f[it, ix+1]
			fxp = f[it, ix-1]
			ft = f[it, ix]
			fxd = ((fxn - fxp) / (2 * dx))
			k = a / (2 * x)
			f[it+1, ix] = k * fxd * dt + ft

	def ev(t, x):
		it = int(t / dt)
		ix = int(x / dx)
		return f[it, ix]

	return ev


def main():
	fdm1 = pde1_fdm()
	fdm2 = pde2_fdm()
	fdm3 = pde3_fdm()
	for t in [4.0, 9.0]:
		for x in [1.0, 3.0, 5.0, 9.0]:
			s1 = pde1_analytical(t, x)
			s2 = fdm1(t, x)
			s3 = pde2_analytical(t, x)
			s4 = fdm2(t, x)
			s5 = pde3_analytical(t, x)
			s6 = fdm3(t, x)
			print(t, x, '=>', round(s1, 3), round(s2, 3), round(s3, 3), round(s4, 3), round(s5, 3), round(s6, 3))


if __name__ == '__main__':
	main()
