#!/usr/bin/env python3

import matplotlib.animation
import matplotlib.pyplot as plt
import numpy

XY = 40
AXY = 0.05
T = 100
AT = 0.1

m = numpy.zeros((T, XY, XY))

for t in range(T):
	for y in range(XY):
		for x in range(XY):
			m[t, y, x] = numpy.sin(AXY * x * AXY * y + AT * t)

def animate(t):
	print(t)
	return plt.pcolormesh(m[int(t) % T]),

t = frames=numpy.linspace(0, T - 1, T)
fig = plt.figure(figsize=(5, 5), tight_layout=True)
ani = matplotlib.animation.FuncAnimation(fig, animate, frames=t, interval=50, blit=True)

plt.show()
