model RectifierGraphical
  Modelica.Electrical.Analog.Semiconductors.Diode d1 annotation(
    Placement(visible = true, transformation(origin = {-30, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Semiconductors.Diode d2 annotation(
    Placement(visible = true, transformation(origin = {30, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Semiconductors.Diode d3 annotation(
    Placement(visible = true, transformation(origin = {-30, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Semiconductors.Diode d4 annotation(
    Placement(visible = true, transformation(origin = {30, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {80, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor r annotation(
    Placement(visible = true, transformation(origin = {80, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.SineVoltage sineVoltage(V = 10, f = 1)  annotation(
    Placement(visible = true, transformation(origin = {-80, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(d2.n, r.p) annotation(
    Line(points = {{40, 20}, {80, 20}, {80, 0}}, color = {0, 0, 255}));
  connect(d3.n, d4.p) annotation(
    Line(points = {{-20, -20}, {20, -20}}, color = {0, 0, 255}));
  connect(d1.n, d2.p) annotation(
    Line(points = {{-20, 20}, {20, 20}}, color = {0, 0, 255}));
  connect(d2.n, d4.n) annotation(
    Line(points = {{40, 20}, {40, -20}}, color = {0, 0, 255}));
  connect(d1.p, d3.p) annotation(
    Line(points = {{-40, 20}, {-40, -20}}, color = {0, 0, 255}));
  connect(d3.p, r.n) annotation(
    Line(points = {{-40, -20}, {-60, -20}, {-60, -60}, {80, -60}, {80, -20}}, color = {0, 0, 255}));
  connect(ground.p, r.n) annotation(
    Line(points = {{80, -80}, {80, -20}}, color = {0, 0, 255}));
  connect(sineVoltage.p, d1.n) annotation(
    Line(points = {{-80, 10}, {-80, 40}, {-20, 40}, {-20, 20}}, color = {0, 0, 255}));
  connect(sineVoltage.n, d3.n) annotation(
    Line(points = {{-80, -10}, {-80, -40}, {-20, -40}, {-20, -20}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "4.0.0")),
    Diagram,
    version = "");
end RectifierGraphical;
