#!/usr/bin/env python3

import matplotlib.pyplot as plot
import numpy
import scipy.integrate
import scipy.optimize
import sympy
import ngspice
import om
import components
import collections
import gekko


R = 100.0
G = 1 / R
US_PEAK = 2.0
T_END = 2.0
vsin = components.vsin(dc=0.0, ac=US_PEAK, frequency=1.0)
vsin_diff = components.vsin_diff(dc=0.0, ac=US_PEAK, frequency=1.0)


class Struct:
	def __init__(self, **entries):
		self.__dict__.update(entries)


def serial_rd_num():
	'''
	i = diode(ud)
	i = (us - ud) / R

	(us - ud) / R = diode(ud)
	((us - ud) / R) - diode(ud) = 0
	'''

	def system(t):
		def model(x):
			ud = x[0]
			us = vsin(t)
			return [((us - ud) / R) - components.diode_shockley(ud)]
		return scipy.optimize.root(fun=model, x0=[US_PEAK]).x[0]

	t = numpy.linspace(start=0.0, stop=T_END, num=100)
	ud = numpy.vectorize(system)(t)
	return t, ud


def serial_rd_num2():
	'''
	y = a * x + b
	'''

	def linearize(f, x0, dx):
		x1, x2 = x0 - dx, x0 + dx
		y1 = f(x1)
		y2 = f(x2)
		a = (y2 - y1) / (x2 - x1)
		b = y1 - a * x1
		return a, b

	def linearize_diff(f, fdiff, x0):
		a = fdiff(x0)
		y = f(x0)
		b = y - a * x0
		return a, b

	# TODO diode_shockley_diff also needs dx0/dt
	# TODO revisit taylor approx, is this the same?
	# TODO second order taylor approx and then solving via nonlinear root finding scipy.optimize.root?
	SWITCH = 2
	def system(t, prev=Struct(ud=0.0)):
		if SWITCH == 1:
			ad, bd = linearize_diff(components.diode_shockley, components.diode_shockley_diff, prev.ud)
		elif SWITCH == 2:
			ad, bd = linearize(components.diode_shockley, prev.ud, 1e-6)
		us = vsin(t)
		ud = (us * G - bd) / (ad + G)
		prev.ud = ud
		return ud

	t = numpy.linspace(start=0.0, stop=T_END, num=300)
	ud = numpy.vectorize(system)(t)
	return t, ud


def serial_rd_num3():
	'''
	is = (us - ud) / R
	(us - ud) / R = diode(ud)

	diff((us - ud) / R, t) = diff(diode(ud), t)
	'''

	D_IS, D_VT = 10e-9, 0.05

	def ode(t, y, prev=Struct(dud=0.0)):
		ud = y[0]

		def root(x):
			dud = x
			dus = vsin_diff(t)
			did = (D_IS * dud * numpy.exp(ud / D_VT)) / D_VT
			f = ((dus - dud) / R) - did
			return f

		dud = scipy.optimize.root(fun=root, x0=prev.dud).x[0]
		prev.dud = dud
		return [dud]

	ud0 = 0.0
	result = scipy.integrate.solve_ivp(fun=ode, t_span=(0.0, T_END), y0=[ud0], max_step=0.01)
	return result.t, result.y[0]


def serial_rd_spice():
	circuit = f'''
	netlist
	v a 0 DC 0 SIN (0v {US_PEAK}v 1Hz)
	r a b {R}
	d b 0
	.end
	'''
	t, ud = ngspice.simulate(circuit=circuit, variables=['b'], t_step=0.01, t_end=T_END)
	return t, ud


def serial_rd_om():
	MODEL = f'''
	model SerialRD
	constant Real pi = {numpy.pi};
	constant Real us_peak = 2.0;
	Real us;
	Real is;
	constant Real r = 100.0;
	Real ur;
	Real ir;
	Real ud;
	Real id;
	function Diode
	input Real u;
	output Real i;
	protected constant Real is = 10e-9;
	protected constant Real vt = 0.05;
	algorithm
	i := (is * (exp(u / vt) - 1.0));
	end Diode;
	equation
	us = sin(2.0 * pi * time) * us_peak;
	is = ir;
	ir = id;
	ur = us - ud;
	ir = ur / r;
	id = Diode(ud);
	end SerialRD;
	'''
	return om.simulate(MODEL, 'SerialRD', ['ud'], t_step=0.01, t_end=T_END)


def serial_rd_gekko():
	'''
	(us - ud) / R) = (d_is * (exp(ud / d_vt) - 1.0))
	'''
	SWITCH = 1
	if SWITCH == 1:
		# gekko is quite slow (2021-11-23)
		return numpy.array([0]), numpy.array([0])

	m = gekko.GEKKO()

	d_is = 10e-9
	d_vt = 0.05
	us = m.Var(0.0)
	ud = m.Var(0.0)
	t = m.Var(0.0)

	m.Equation(t.dt() == 1)
	m.Equation(us == m.sin(2.0 * numpy.pi * t) * US_PEAK)
	m.Equation((us - ud) / R == (d_is * (m.exp(ud / d_vt) - 1.0)))

	m.time = numpy.linspace(start=0.0, stop=T_END, num=110)
	m.options.IMODE = 7  # Sequential (SQS)
	m.solve()

	return m.time, ud


def main():
	methods = [serial_rd_num, serial_rd_num2, serial_rd_spice, serial_rd_om, serial_rd_gekko]
	methods = [serial_rd_num, serial_rd_num2, serial_rd_num3]
	plot.figure(figsize=(16, 9), tight_layout=True)
	t = numpy.linspace(start=0.0, stop=T_END, num=100)
	us = numpy.vectorize(vsin)(t)
	plot.plot(t, us, label='us')
	for i, method in enumerate(methods):
		translate_view = i * 0.01 * 0
		t, u = method()
		plot.plot(t + translate_view, u, label=method.__name__)
	plot.grid()
	plot.legend()
	plot.show()


main()
