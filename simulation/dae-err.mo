model Error
  function Diode
    input Real u;
    output Real i;
    protected constant Real is = 1e-7;
    protected constant Real vt = 0.05;
  algorithm
    i := (is * (exp(u / vt) - 1.0));
  end Diode;

  constant Real pi = Modelica.Constants.pi;
  constant Real us_peak = 2.0;
  constant Real r = 100.0;
  Real ud1;
  Real ud2;
  Real ud3;
  Real us;
  Real err12;
  Real err13;

initial equation
  us = 0;
  ud1 = 0;
  ud2 = 0;
  ud3 = 0;
equation
  us = sin(2.0 * pi * time) * us_peak;
  (us - ud1) / r = Diode(ud1);
  der((us - ud2) / r) = der(Diode(ud2));
  (der(us) - der(ud3)) / r = der(Diode(ud3));
  err12 = ud1 - ud2;
  err13 = ud1 - ud3;
end Error;
