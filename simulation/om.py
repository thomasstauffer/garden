#!/usr/bin/env python3

import tempfile
import os
import shutil
import OMPython


def simulate(model, name, variables, t_step, t_end):
	'''
	https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html
	https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/simulationflags.html
	'''
	dir_tmp = tempfile.mkdtemp(prefix='openmodelica')
	dir_wd_original = os.getcwd()
	os.chdir(dir_tmp)
	open('model.mo', 'w').write(model.strip())
	system = OMPython.ModelicaSystem(fileName='model.mo', modelName=name)
	system.setSimulationOptions([f'stepSize={t_step}', f'stopTime={t_end}'])
	system.simulate(simflags='-lv=-LOG_SUCCESS')  # disable log output
	results = system.getSolutions(['time'] + variables)
	os.chdir(dir_wd_original)
	shutil.rmtree(dir_tmp)
	return results


def test():
	MODEL = '''
	model Solve
	Real a = 3.0;
	Real b;
	Real c = 1.0;
	equation
	a = b + c;
	end Solve;
	'''

	b = float(simulate(MODEL, 'Solve', ['b'])[0])
	assert b == 2.0


if __name__ == '__main__':
	test()
