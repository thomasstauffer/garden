# Install Docker

```
$ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
$ sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
$ sudo apt-get update
$ sudo apt-get install docker-engine
$ sudo service docker start
$ sudo usermod -aG docker $USER
```

# dumb-init

https://github.com/Yelp/dumb-init

# CouchDB

http://docs.couchdb.org/en/stable/install/unix.html

# Apache

TOOD

# Docker

```
$ docker -v
Docker version 17.05.0-ce, build 89658be
$ docker run hello-world
$ docker run -it ubuntu bash
$ docker images
$ docker images | grep '<none>' | awk '{print $3}' | xargs docker rmi
```
