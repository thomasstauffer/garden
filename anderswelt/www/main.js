/* global PouchDB TILES */

'use strict';

// Generic Functions

Object.prototype.freeze = function () {
	Object.freeze(this);
	return this;
};

function nowMilliseconds() {
	return new Date().getTime();
}

// Globals

const config = {
	//server: 'http://username:password@127.0.0.1:5984/',
	//server: 'http://127.0.0.1:5984/',
	server: '', // local only tests
	database: 'world',
	map: 'my_small_map',
	//tiles: 'thomas',
	//tilesRandom: ['grass', 'mountain', 'dungeon'],
	tiles: 'thomasindoor',
	tilesRandom: ['empty', 'empty', 'floor'],
	log: true,
}.freeze();

// Application

function log(title, message) {
	if (config.log) {
		console.log(title + ' ' + nowMilliseconds());
		if (message !== undefined) {
			console.log(message);
		}
	}
}

function tileClick(db, map, event) {
	const tool = document.querySelector('input[name="menu-tool"]:checked').value;
	const node = event.target;
	const x = node.getAttribute('data-x');
	const y = node.getAttribute('data-y');

	if (tool === 'inspect') {
		const id = node.getAttribute('data-id');
		const r = node.getAttribute('data-r');
		const info = node.getAttribute('data-info');
		const ref = node.getAttribute('data-ref');
		document.getElementById('menu-id-' + id).checked = true;
		document.getElementById('menu-r').value = r;
		document.getElementById('menu-info').value = info;
		document.getElementById('menu-ref').value = ref;
	} else if (tool === 'draw') {
		const id = document.querySelector('input[name="menu-id"]:checked').value;
		const r = document.getElementById('menu-r').value;
		const info = document.getElementById('menu-info').value;
		const ref = document.getElementById('menu-ref').value;
		const data = { id: id, r: r, info: info, ref: ref };
		const updater = doc => {
			doc.tiles[y][x] = data;
			return doc;
		};
		db.upsert(map, updater).then(function () {
			log('upsert');
		}).catch(function (err) {
			log('upsert error', err);
		});
	}
}

function updateView(db, map, doc) {
	const nodeMap = document.getElementById('map');
	nodeMap.innerHTML = '';

	const height = 80;
	const width = 80;
	const height2 = height / 2;
	const width2 = width / 2;

	function keys(object) {
		const keys = Object.keys(object);
		keys.sort(function (a, b) {
			return a - b; // js by default sorts strings
		});
		return keys;
	}

	let maxX = 0;
	let maxY = 0;

	const nodeFragment = document.createDocumentFragment();

	const keysY = keys(doc.tiles);
	for (let indexY = 0; indexY < keysY.length; indexY++) {
		const tileY = keysY[indexY];
		const keysX = keys(doc.tiles[tileY]);
		for (let indexX = 0; indexX < keysX.length; indexX++) {
			const tileX = keysX[indexX];

			const tile = doc.tiles[tileY][tileX];
			const filename = 'tiles/' + config.tiles + '/' + tile.id + '.png';

			const x = tileX * width;
			const y = tileY * height;

			maxX = Math.max(maxX, x);
			maxY = Math.max(maxY, y);

			const nodeImage = document.createElementNS('http://www.w3.org/2000/svg', 'image');
			nodeImage.setAttribute('x', x);
			nodeImage.setAttribute('y', y);
			nodeImage.setAttribute('width', width);
			nodeImage.setAttribute('height', height);
			nodeImage.setAttribute('href', filename);
			nodeImage.setAttribute('data-x', tileX);
			nodeImage.setAttribute('data-y', tileY);
			nodeImage.setAttribute('data-id', tile.id);
			nodeImage.setAttribute('data-r', tile.r);
			nodeImage.setAttribute('data-info', tile.info);
			nodeImage.setAttribute('data-ref', tile.ref);
			nodeImage.setAttribute('transform', 'rotate(' + (tile.r) + ' ' + (x + width2) + ' ' + (y + height2) + ')');
			nodeImage.addEventListener('mousedown', event => tileClick(db, map, event));
			nodeFragment.appendChild(nodeImage);

			const nodeInfo = document.createElementNS('http://www.w3.org/2000/svg', 'text');
			nodeInfo.setAttribute('x', x);
			nodeInfo.setAttribute('y', y + height);
			nodeInfo.setAttribute('font-size', '30pt');
			//nodeInfo.setAttribute('text-anchor', 'middle');
			nodeInfo.setAttribute('fill', '#f00');
			nodeInfo.setAttribute('class', 'tileinfo');
			nodeInfo.appendChild(document.createTextNode(String.fromCharCode(0x2606)));
			if (tile.info.length > 0) {
				nodeFragment.appendChild(nodeInfo);
			}

			const nodeRef = document.createElementNS('http://www.w3.org/2000/svg', 'text');
			nodeRef.setAttribute('x', x + 30);
			nodeRef.setAttribute('y', y + height);
			nodeRef.setAttribute('font-size', '30pt');
			//nodeRef.setAttribute('text-anchor', 'middle');
			nodeRef.setAttribute('fill', '#ff0');
			nodeRef.setAttribute('class', 'tileref');
			nodeRef.appendChild(document.createTextNode(String.fromCharCode(0x2606)));
			if (tile.ref.length > 0) {
				nodeFragment.appendChild(nodeRef);
			}
		}
	}

	nodeMap.appendChild(nodeFragment);
	nodeMap.setAttribute('width', maxX + width);
	nodeMap.setAttribute('height', maxY + height);
}

function tileAdd(tiles, x, y, data) {
	if (tiles[y] === undefined) {
		tiles[y] = {};
	}
	tiles[y][x] = data;
}

function tilesClear(doc) {
	doc.tiles = {};
	return doc;
}

function tilesEmpty(doc) {
	const countX = document.getElementById('menu-count-x').value;
	const countY = document.getElementById('menu-count-y').value;
	const tiles = {};
	for (let y = 0; y < countY; y++) {
		for (let x = 0; x < countX; x++) {
			const data = { id: 'empty', r: 0, info: '', ref: '' };
			tileAdd(tiles, x, y, data);
		}
	}
	doc.tiles = tiles;
	return doc;
}

function tilesRandom(doc) {
	const countX = document.getElementById('menu-count-x').value;
	const countY = document.getElementById('menu-count-y').value;
	const tiles = {};
	for (let y = 0; y < countY; y++) {
		for (let x = 0; x < countX; x++) {
			const id = config.tilesRandom[Math.floor(Math.random() * config.tilesRandom.length)];
			const data = { id: id, r: 0, info: '', ref: '' };
			tileAdd(tiles, x, y, data);
		}
	}
	doc.tiles = tiles;
	return doc;
}

function queryString() {
	const query = window.location.href.split('?')[1] || '';
	const params = query.split('&');
	let result = {};
	for (let i = 0; i < params.length; i++) {
		const param = params[i].split('=');
		result[param[0]] = param[1];
	}
	return result;
}

function setupTiles() {
	const nodeTiles = document.getElementById('tiles');
	const tiles = TILES[config.tiles];
	const path = config.tiles;
	let html = '';
	for (let i = 0; i < tiles.length; i += 1) {
		const name = tiles[i];
		html += `<div class="radio"><input type="radio" id="menu-id-${name}" name="menu-id" value="${name}" checked><label for="menu-id-${name}"><img src="tiles/${path}/${name}.png" width="50" height="50"/></label></div>`;
	}
	nodeTiles.innerHTML = html;
}

function main() {
	log('hello');

	setupTiles();
	const query = queryString();
	log('query', query);

	const database = config.database;

	const map = query['map'] || config.map;
	log('map', map);

	const db = new PouchDB(config.server + database);

	db.info().then(function (info) {
		log('info', info);
	}).catch(function (error) {
		log('info error (CORS?)', error);
	});

	db.get(map).then(function (doc) {
		updateView(db, map, doc);
		const nodeMap = document.getElementById('map');
		const mapWidth = nodeMap.getAttribute('width');
		const mapHeight = nodeMap.getAttribute('height');
		window.scrollTo((mapWidth - window.innerWidth) / 2, (mapHeight - window.innerHeight) / 2);
	}).catch(function (error) {
		log('get error', error);
		upsertFunction(tilesEmpty)();
	});

	db.changes({
		since: 'now',
		live: true,
		include_docs: true
	}).on('change', function (change) {
		log('changes change');
		updateView(db, map, change.doc);
	}).on('error', function (err) {
		log('changes error', err);
	});

	function upsertFunction(updater) {
		return function () {
			db.upsert(map, updater).then(function () {
				log('upsert');
			}).catch(function (err) {
				log('upsert error', err);
			});
		};
	}

	document.getElementById('menu-ref-follow').addEventListener('click', function () {
		const ref = document.getElementById('menu-ref').value;
		const path = window.location.pathname;
		const newpath = path + '?map=' + ref;
		window.location = newpath;
	});

	document.getElementById('menu-clear').addEventListener('click', upsertFunction(tilesClear));
	document.getElementById('menu-empty').addEventListener('click', upsertFunction(tilesEmpty));
	document.getElementById('menu-random').addEventListener('click', upsertFunction(tilesRandom));
}

window.addEventListener('load', main);