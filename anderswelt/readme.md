
# Anderswelt

Collaborative Fantasy Map Making

## CouchDB

http://docs.couchdb.org
http://guide.couchdb.org/
http://guide.couchdb.org/draft/validation.html

## Validation Examples

{
"_id": "_design/deny",
"validate_doc_update": "function(newDoc, oldDoc, userCtx) { throw({forbidden : 'not able now!'}); }"
}

{
"_id": "_design/jsonsize",
"validate_doc_update": "function(newDoc, oldDoc, userCtx) { if(JSON.stringify(newDoc).length > 100000) { throw({forbidden : 'too big!'}); } }"
}

{
"_id": "_design/counttiles",
"validate_doc_update": "function(newDoc, oldDoc, userCtx) { if(Object.keys(newDoc.tiles).length > 20) { throw({forbidden : 'too many tiles!'}); } }"
}

## Ideas

- Revisions
- Undo
- Diff UI
- Improved dumb-init
- Custom Tiles
- Validate
