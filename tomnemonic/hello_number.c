
//#include <stdint.h>
//#include <stdbool.h>

typedef unsigned int uint32_t;

#define true 1
#define false 0

void w32(void* address, uint32_t value) {
	volatile uint32_t* ptr = (volatile uint32_t*)(address);
	*ptr = value;
}

uint32_t r32(void* address) {
	volatile uint32_t* ptr = (volatile uint32_t*)(address);
	return *ptr;
}

void _start() {
	void* scratch = (void*)(0x10000000);
	void* hex0 = (void*)(0x20000000);
	void* hex1 = (void*)(0x20000004);
	void* hex2 = (void*)(0x20000008);
	void* hex3 = (void*)(0x2000000c);
	
	//int i;
	//void* hex0 = &i;
	
	while(true) {
		for(uint32_t value = 0; value < 16; value += 1) {
			w32(hex0, value);
			for(uint32_t i = 0; i < 1000000; i += 1) {
				w32(scratch, i);
			}
		}
	}
}
