
module sevensegment(
		input wire clk, input wire rst,
		input wire [31:0] bus_addr, input wire [31:0] bus_in, output reg [31:0] bus_out, input wire bus_write, input wire bus_select,
		output reg [6:0] pins
	);
	reg [31:0] control;
	initial begin
		bus_out <= 0;
		pins <= ~7'b0000000;
		control <= 0;
	end
	always @ (posedge clk) begin
		if (rst) begin
			bus_out <= 0;
			pins <= ~7'b0000000;
			control <= 0;
		end else begin
			// TODO what is the best way to store it and immediately do it (instead of two cycles, one cycle)
			if (bus_select && bus_write) begin
				control <= bus_in;
			end
			if (control[4]) begin
				case (control[3:0])
					0: pins <= ~7'b0111111;
					1: pins <= ~7'b0000110;
					2: pins <= ~7'b1011011;
					3: pins <= ~7'b1001111;
					4: pins <= ~7'b1100110;
					5: pins <= ~7'b1101101;
					6: pins <= ~7'b1111101;
					7: pins <= ~7'b0000111;
					8: pins <= ~7'b1111111;
					9: pins <= ~7'b1101111;
					10: pins <= ~7'b1110111;
					11: pins <= ~7'b1111100;
					12: pins <= ~7'b0111001;
					13: pins <= ~7'b1011110;
					14: pins <= ~7'b1111001;
					15: pins <= ~7'b1110001;
				endcase
			end else begin
				pins <= ~7'b0000000;
			end
		end
	end
endmodule