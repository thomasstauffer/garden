
module gpio(
		input wire clk, input wire rst,
		input wire [31:0] bus_addr, input wire [31:0] bus_in, output reg [31:0] bus_out, input wire bus_write, input wire bus_select,
		input wire [9:0] input_pins, output reg [9:0] output_pins
	);
	initial begin
		output_pins = 0;
		bus_out <= 0;
	end
	always @ (posedge clk) begin
		if (rst) begin
			output_pins <= 0;
			bus_out <= 0;
		end else begin
			if (bus_select) begin
				if (bus_write) begin
					output_pins <= bus_in[9:0];
				end else begin
					bus_out[9:0] <= input_pins;
				end
			end
		end
	end
endmodule