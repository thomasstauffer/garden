
module system(input wire clk, input wire rst);

	wire [6:0] hex0;
	wire [6:0] hex1;
	wire [6:0] hex2;
	wire [6:0] hex3;
	reg [9:0] sw = 10'b101;
	wire [9:0] ledr;
	wire [3:0] vga_r;
	wire [3:0] vga_g;
	wire [3:0] vga_b;
	wire vga_hsync;
	wire vga_vsync;


	wire [31:0] bus_addr;
	wire [31:0] bus_out;
	wire [31:0] bus_in;
	wire bus_write;

	/*
	wire bus_select_ram0 = bus_addr[14:8] == 8'h05;
	wire bus_select_rom0 = bus_addr[14:8] == 8'h06;
	wire bus_select_uart0 = bus_addr[14:8] == 8'h08;
	wire bus_select_sram0 = bus_addr[14:8] == 8'h09;
	wire bus_select_quadspi = bus_addr[14:8] == 8'hff;
	wire bus_select_spi = bus_addr[14:8] == 8'hff;
	wire bus_select_i2c = bus_addr[14:8] == 8'hff;
	wire bus_select_sdram = bus_addr[14:8] == 8'hff;
	*/

	wire bus_select_sevensegment0 = bus_addr[15:8] == 8'h01;
	wire [31:0] sevensegment0_bus_in;
	sevensegment sevensegment0(clk, rst, bus_addr, bus_out, sevensegment0_bus_in, bus_write, bus_select_sevensegment0, hex0);

	wire bus_select_sevensegment1 = bus_addr[15:8] == 8'h02;
	wire [31:0] sevensegment1_bus_in;
	sevensegment sevensegment1(clk, rst, bus_addr, bus_out, sevensegment1_bus_in, bus_write, bus_select_sevensegment1, hex1);

	wire bus_select_sevensegment2 = bus_addr[14:8] == 8'h03;
	wire [31:0] sevensegment2_bus_in;
	sevensegment sevensegment2(clk, rst, bus_addr, bus_out, sevensegment2_bus_in, bus_write, bus_select_sevensegment2, hex2);

	wire bus_select_sevensegment3 = bus_addr[14:8] == 8'h04;
	wire [31:0] sevensegment3_bus_in;
	sevensegment sevensegment3(clk, rst, bus_addr, bus_out, sevensegment3_bus_in, bus_write, bus_select_sevensegment3, hex3);

	wire bus_select_vga0 = bus_addr[14:8] == 8'h05;
	wire [31:0] vga_bus_in;
	vga_640x480 vga0(clk, rst, bus_addr, bus_out, vga_bus_in, bus_write, bus_select_vga0, vga_r, vga_g, vga_b, vga_hsync, vga_vsync);

	wire bus_select_gpio0 = bus_addr[14:8] == 8'h06;
	wire [31:0] gpio0_bus_in;
	gpio gpio0(clk, rst, bus_addr, bus_out, gpio0_bus_in, bus_write, bus_select_gpio0, sw[9:0], ledr[9:0]);

	assign bus_in = gpio0_bus_in;
	cpu cpu0(clk, rst, bus_addr, bus_out, bus_in, bus_write);

	initial begin
		//$monitor("t=%3d | ss0=%x | ss1=%x", $time, sevensegment0_pins, sevensegment1_pins);
		$monitor("t=%3d | addr:%x min:%x mout:%x | gpioi=%x | gpioo=%x", $time, bus_addr, bus_in, bus_out, sw[9:0], ledr[9:0]);
	end

endmodule
