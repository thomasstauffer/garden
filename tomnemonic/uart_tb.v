`timescale 10ns / 10ns

module uart_tb;
	reg clk;
	initial begin
		clk <= 0;
	end
	always begin
		#1 clk = ~clk;
	end

	initial begin
		$dumpfile("uart.vcd");
		$dumpvars(0, clk, uart_tx_pin, uart_tx0);
	end

	wire uart_tx_pin;

	reg load = 0;
	reg [7:0] data = 8'b00111100;

	uart_tx uart_tx0(clk, load, data, uart_tx_pin);

	initial begin
		#10000
		load <= 1;
		#2
		load <= 0;
		#1000000
		$finish;
	end
endmodule