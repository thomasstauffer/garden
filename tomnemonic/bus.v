
module bus_slave (
		input wire clk, input wire rst,
		input wire [31:0] bus_addr, input wire [31:0] bus_in, output reg [31:0] bus_out, input wire bus_write, input wire bus_select,
		output wire [31:0] regsflat
	);
	/* #(parameter REGS = 4) */
	// TODO using anythign than r[0] does not work?
	localparam REGS = 1;
	reg [31:0] regs [0:REGS - 1];
	assign regsflat = {regs[0]};
	integer i;
	initial begin
		for(i = 0; i < REGS; i = i + 1) begin
			regs[i] <= 0;
		end
		bus_out <= 0;
	end
	always @ (posedge clk) begin
		if (rst) begin
			for(i = 0; i < REGS; i = i + 1) begin
				regs[i] <= 0;
			end
			bus_out <= 0;
		end else begin
			if (bus_select && bus_write) begin
				regs[0] <= bus_in;
			end
		end
	end
endmodule

module bus_master (
		input wire clk, input wire rst,
		input wire [31:0] addr, input wire [31:0] out, output reg [31:0] in, input wire cycle, input wire write,
		output reg [31:0] bus_addr, input wire [31:0] bus_in, output reg [31:0] bus_out, output reg bus_cycle, output reg bus_write
	);
	always @ (posedge clk) begin
		if (rst) begin
			in <= 0;
			bus_out <= 0;
			bus_write <= 0;
			bus_cycle <= 0;
		end else begin
			if (cycle) begin
				bus_addr <= addr;
				if (write) begin
					bus_out <= out;
				end else begin
					in <= bus_in;
				end
			end
		end
	end
endmodule
	
