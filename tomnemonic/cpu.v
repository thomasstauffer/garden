`timescale 1ns / 1ns

`include "opcodes.v"

`define DEBUG

module cpu(input wire clk, input wire rst, output reg [31:0] bus_addr, output reg [31:0] bus_out, input wire [31:0] bus_in, output reg bus_write);
	localparam REGS = 16;

	integer i;

	reg [7:0] mem [0:256 - 1];
	reg [31:0] pc;
	reg [31:0] r [0:REGS - 1];
	initial begin
		$readmemh("test.hex", mem);
		pc <= 0;
		for(i = 0; i < REGS; i = i + 1) begin
			r[i] <= 0;
		end
		bus_addr <= 0;
		bus_out <= 0;
		bus_write <= 0;
	end

	wire [7:0] op = mem[pc];
	wire [3:0] rd = mem[pc + 1][7:4];
	wire [3:0] ra = mem[pc + 1][3:0];
	wire [3:0] rb = mem[pc + 2][7:4];
	//wire [3:0] rc = mem[pc + 2][3:0];
	//wire [7:0] u8 = mem[pc + 3];
	//wire [15:0] u16 = {mem[pc + 2], mem[pc + 3]};
	wire signed [15:0] s16 = {mem[pc + 2], mem[pc + 3]};

	always @ (posedge clk)
	begin
		if (rst) begin
			pc <= 0;
			for(i = 0; i < REGS; i = i + 1) begin
				r[i] <= 0;
			end
			bus_addr <= 0;
			bus_out <= 0;
			bus_write <= 0;
		end else begin
			pc <= pc + 4;
			bus_write <= 0;
			bus_addr <= 0;
			case (op)
				`OP_NOP : ;
				`OP_BP: begin $finish; end
				`OP_LD : begin bus_addr <= r[ra]; r[rd] <= bus_in; end
				`OP_ST : begin bus_addr <= r[rd]; bus_out <= r[ra]; bus_write <= 1; end
				`OP_MV : r[rd] <= r[ra];
				`OP_MVI : r[rd] <= s16;
				`OP_JMP : begin r[rd] <= pc + 4; pc <= r[ra]; end
				`OP_JMPI : begin r[rd] <= pc + 4; pc <= pc + s16; end
				`OP_BRE : begin if (r[rd] == r[ra]) begin pc <= $signed(pc) + s16; end end
				`OP_BRNE : begin if (r[rd] != r[ra]) begin pc <= $signed(pc) + s16; end end
				`OP_BRL : begin if (r[rd] < r[ra]) begin pc <= $signed(pc) + s16; end end
				`OP_BRLE : begin if (r[rd] <= r[ra]) begin pc <= $signed(pc) + s16; end end
				`OP_CMPE : r[rd] <= r[ra] == r[rb];
				`OP_CMPNE : r[rd] <= r[ra] != r[rb];
				`OP_CMPL : r[rd] <= r[ra] < r[rb];
				`OP_CMPLE : r[rd] <= r[ra] <= r[rb];
				`OP_AND : r[rd] <= r[ra] & r[rb];
				`OP_OR : r[rd] <= r[ra] | r[rb];
				`OP_NOT : r[rd] <= ~r[ra];
				`OP_XOR : r[rd] <= r[ra] ^ r[rb];
				`OP_SHLL : r[rd] <= r[ra] << r[rb];
				`OP_SHLR : r[rd] <= r[ra] >> r[rb];
				`OP_SHAL : r[rd] <= r[ra] <<< r[rb];
				`OP_SHAR : r[rd] <= r[ra] >>> r[rb];
				`OP_ADD : r[rd] <= r[ra] + r[rb];
				`OP_SUB : r[rd] <= r[ra] - r[rb];
				`OP_MUL : r[rd] <= r[ra] + /* * */ r[rb];
				`OP_DIV : r[rd] <= r[ra] + /* / */ r[rb];
				`OP_REM : r[rd] <= r[ra] + /* % */ r[rb];
			endcase
		end
	end

`ifdef DEBUG
	initial begin
		$monitor("t=%3d | rst=%1d | pc=%h | op=%h | r0=%h | r1=%h | r2=%h | r3=%h", $time, rst, pc, op, r[0], r[1], r[2], r[3]);
	end
`endif

endmodule
