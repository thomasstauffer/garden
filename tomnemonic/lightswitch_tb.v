`timescale 1ms / 1ms

module lightswitch_tb;
	reg clk;
	always begin
		#5 clk = ~clk;
	end

	reg a;
	reg b;
	reg c;
	wire enabled;

	lightswitch ls(clk, a, b, c, enabled);

	reg [9:0] testvectornum;
	reg [4:0] testvectors [7:0]; // TODO how to automatically adjust?
	reg enabledexpected;
	reg finish;
	initial begin
		$readmemb("lightswitch.tv", testvectors); // TODO fix warning
		testvectornum = 0;
		clk <= 0;
		finish <= 0;
	end

	task displaystate (reg [3*8:1] prefix);
	begin
		$display("%s t=%3d | a=%b | b=%b | c=%b | enabled=%b | expectedenabled=%b | testvectornum=%2d", prefix, $time, a, b, c, enabled, enabledexpected, testvectornum);
	end
	endtask

	initial begin
		#1000;
		while (finish == 0) begin
			#100
			{a, b, c, enabledexpected, finish} <= testvectors[testvectornum];
			displaystate("===");
			#100;
			displaystate("???");
			if (enabledexpected !== enabled) begin
				$error("ERR");
				$fatal(1);
			end
			testvectornum += 1;
		end
	end

	initial begin
		#100000;
		$finish;
	end
endmodule
