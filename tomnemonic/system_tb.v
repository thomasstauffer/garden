`timescale 10ns / 10ns

module system_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	reg rst = 1;

	system system0(clk, rst);

	initial begin
		repeat(3) @ (negedge clk);
		rst <= 0;
		repeat(1000) @ (negedge clk);
		$finish;
	end

	initial begin
		$dumpfile("system.vcd");
		$dumpvars(0, system0);
	end
endmodule