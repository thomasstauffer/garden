
/*
clk = 50 MHz

Baudrate = 115200
8.68us
Divider 434 = 115207.37 Baud

Baudrate = 9600
104.17us
Divider 5208 = 9600.61 Baud
*/

module uart_tx(input wire clk, input wire load, input wire [7:0] data, output reg tx_pin);
	reg [3:0] count;

	initial begin
		count <= 4'b1111;
		tx_pin <= 1;
	end

	wire clk_en;
	clkenable #(5208) clkenable0 (clk, clk_en);

	always @ (posedge clk) begin
		if (load) begin
			count <= 0;
		end
		if (clk_en) begin
			if (!load) begin
				if (count < 4'b1111) begin
					count <= count + 1;
				end
			end
			case (count)
				0: tx_pin <= 0;
				1: tx_pin <= data[0];
				2: tx_pin <= data[1];
				3: tx_pin <= data[2];
				4: tx_pin <= data[3];
				5: tx_pin <= data[4];
				6: tx_pin <= data[5];
				7: tx_pin <= data[6];
				8: tx_pin <= data[7];
				default: tx_pin <= 1;
			endcase
		end
	end
endmodule

module uart_rx(input wire clk, output reg [7:0] data, input wire rx_pin_2);
	wire clk_en;
	clkenable #(1302 - 1) clkenable0 (clk, clk_en); // 4 x oversampling

	reg rx_pin_1;
	reg rx_pin;

	reg active;
	reg [6:0] count;
	reg [(4*9)-1:0] bits;

	initial begin
		data <= 8'h42;
		active <= 0;
		count <= 0;
		bits <= 0;
	end

	always @ (posedge clk) begin
		rx_pin_1 <= rx_pin_2;
		rx_pin <= rx_pin_1;

		if (clk_en) begin

			count <= count + 1'b1;
			bits[count] <= rx_pin;

			if (!active) begin
				if ((rx_pin == 0) && (count < 3)) begin
					active <= 1;
				end else begin
					count <= 0;
				end
			end

			if (count == 36) begin
				active <= 0;
				count <= 0;
				data[0] <= bits[7:4] >= 2;
				data[1] <= bits[11:8] >= 2;
				data[2] <= bits[15:12] >= 2;
				data[3] <= bits[19:16] >= 2;
				data[4] <= bits[23:20] >= 2;
				data[5] <= bits[27:24] >= 2;
				data[6] <= bits[31:28] >= 2;
				data[7] <= bits[35:32] >= 2;
			end
		end
	end
endmodule