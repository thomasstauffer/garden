`timescale 10ns / 10ns

module reset_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	wire rst0;
	reset #(0) reset0 (clk, rst0);

	wire rst1;
	reset #(1) reset1 (clk, rst1);

	wire rst5;
	reset #(5) reset5 (clk, rst5);

	wire rst10;
	reset #(10) reset10 (clk, rst10);

	initial begin
		repeat(30) @ (negedge clk);
		$finish;
	end

	initial begin
		$dumpfile("reset.vcd");
		$dumpvars(0, clk, rst0, rst1, rst5, rst10);
	end
endmodule