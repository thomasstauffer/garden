
/*
http://martin.hinner.info/vga/timing.html

Front Porch, Sync, Back Porch is implemented according to the diagramm in "Theory of Operation" on

https://www.digikey.com/eewiki/pages/viewpage.action?pageId=15925278
*/

/*
22 * 11 * 20 ns -> 4840 ns
*/
module vga_sim(
		input wire clk, input wire rst,
		output wire [3:0] r, output wire [3:0] g, output wire [3:0] b, output wire hsync, output wire vsync
	);
	reg [7:0] h = 0;
	reg [7:0] v = 0;

	localparam H_ACTIVE = 10;
	localparam H_FRONT_PORCH = 2;
	localparam H_SYNC = 4;
	localparam H_BACK_PORCH = 6;
	localparam H_TOTAL = H_ACTIVE + H_FRONT_PORCH + H_SYNC + H_BACK_PORCH;

	localparam V_ACTIVE = 5;
	localparam V_FRONT_PORCH = 1;
	localparam V_SYNC = 2;
	localparam V_BACK_PORCH = 3;
	localparam V_TOTAL = V_ACTIVE + V_FRONT_PORCH + V_SYNC + V_BACK_PORCH;

	always @ (posedge clk) begin
		if (rst) begin
			h <= 0;
			v <= 0;
		end else begin
			if (h == (H_TOTAL - 1)) begin
				h <= 0;
				if (v == (V_TOTAL - 1)) begin
					v <= 0;
				end else begin
					v <= v + 1'b1;
				end
			end else begin
				h <= h + 1'b1;
			end
		end
	end

	wire active = (h >= 0) && (h < H_ACTIVE) && (v >= 0) && (v < V_ACTIVE);

	assign hsync = (h >= (H_ACTIVE + H_FRONT_PORCH)) && (h < (H_ACTIVE + H_FRONT_PORCH + H_SYNC));
	assign vsync = (v >= (V_ACTIVE + V_FRONT_PORCH)) && (v < (V_ACTIVE + V_FRONT_PORCH + V_SYNC));
	assign r = active ? 4'b1111 : 4'b0000;
	assign g = active ? 4'b1111 : 4'b0000;
	assign b = active ? 4'b1111 : 4'b0000;
endmodule

module vga_800x600_test(
		input wire clk50mhz, input wire rst,
		output wire [3:0] r, output wire [3:0] g, output wire [3:0] b, output wire hsync, output wire vsync
	);
	reg [10:0] h = 0;
	reg [10:0] v = 0;

	localparam H_ACTIVE = 800;
	localparam H_FRONT_PORCH = 56;
	localparam H_SYNC = 120;
	localparam H_BACK_PORCH = 64;
	localparam H_TOTAL = H_ACTIVE + H_FRONT_PORCH + H_SYNC + H_BACK_PORCH; // 1040

	localparam V_ACTIVE = 600;
	localparam V_FRONT_PORCH = 37;
	localparam V_SYNC = 6;
	localparam V_BACK_PORCH = 23;
	localparam V_TOTAL = V_ACTIVE + V_FRONT_PORCH + V_SYNC + V_BACK_PORCH; // 666

	always @ (posedge clk50mhz) begin
		if (rst) begin
			h <= 0;
			v <= 0;
		end else begin
			if (h == (H_TOTAL - 1)) begin
				h <= 0;
				if (v == (V_TOTAL - 1)) begin
					v <= 0;
				end else begin
					v <= v + 1'b1;
				end
			end else begin
				h <= h + 1'b1;
			end
		end
	end

	wire active = (h >= 0) && (h < H_ACTIVE) && (v >= 0) && (v < V_ACTIVE);

	localparam MARGIN = 20;
	wire leftright = (h == MARGIN) || (h == (H_ACTIVE - 1 - MARGIN));
	wire topbottom = (v == MARGIN) || (v == (V_ACTIVE - 1 - MARGIN));

	assign hsync = (h >= (H_ACTIVE + H_FRONT_PORCH)) && (h < (H_ACTIVE + H_FRONT_PORCH + H_SYNC));
	assign vsync = (v >= (V_ACTIVE + V_FRONT_PORCH)) && (v < (V_ACTIVE + V_FRONT_PORCH + V_SYNC));
	assign r = active && leftright ? 4'b1111 : 4'b0000;
	assign g = active && topbottom ? 4'b1111 : 4'b0000;
	assign b = active ? 4'b1111 : 4'b0000;
endmodule

`define ENABLE_VRAM

module vga_640x480(
		input wire clk50mhz, input wire rst,
		input wire [31:0] bus_addr, input wire [31:0] bus_in, output wire [31:0] bus_out, input wire bus_write, input wire bus_select,
		output wire [3:0] r, output wire [3:0] g, output wire [3:0] b, output wire hsync, output wire vsync
	);
	wire [31:0] regsflat;
	wire [31:0] regs [0:0];
	assign {regs[0]} = regsflat;
	bus_slave bus_slave0(clk50mhz, rst, bus_addr, bus_in, bus_out, bus_write, bus_select, regsflat);

	reg [9:0] h = 0;
	reg [9:0] v = 0;
	reg en = 0;

	localparam MODE_TEST = 0;
	localparam MODE_GREEN = 1;
	localparam MODE_VRAM = 2; // 80 * 60 * 12 = 57'600 bits
	localparam MODE_TILES = 3; // (tiles 128 * 8 * 16 * 2 = 32'768 bits) + (80 * 30 * 8 = 19'200 bits)

	localparam H_ACTIVE = 640;
	localparam H_FRONT_PORCH = 16;
	localparam H_SYNC = 96;
	localparam H_BACK_PORCH = 48;
	localparam H_TOTAL = H_ACTIVE + H_FRONT_PORCH + H_SYNC + H_BACK_PORCH; // 800

	localparam V_ACTIVE = 480;
	localparam V_FRONT_PORCH = 10;
	localparam V_SYNC = 2;
	localparam V_BACK_PORCH = 33;
	localparam V_TOTAL = V_ACTIVE + V_FRONT_PORCH + V_SYNC + V_BACK_PORCH; // 525

	reg [3:0] pr = 4'b0000;
	reg [3:0] pg = 4'b0000;
	reg [3:0] pb = 4'b0000;

	assign r = pr;
	assign g = pg;
	assign b = pb;

	always @ (posedge clk50mhz) begin
		if (rst) begin
			h <= 0;
			v <= 0;
			en <= 0;
		end else begin
			en <= !en;
			if (en) begin
				if (h == (H_TOTAL - 1)) begin
					h <= 0;
					if (v == (V_TOTAL - 1)) begin
						v <= 0;
					end else begin
						v <= v + 1'b1;
					end
				end else begin
					h <= h + 1'b1;
				end
			end
		end
	end

	wire active = (h >= 0) && (h < H_ACTIVE) && (v >= 0) && (v < V_ACTIVE);
	assign hsync = !((h >= (H_ACTIVE + H_FRONT_PORCH)) && (h < (H_ACTIVE + H_FRONT_PORCH + H_SYNC)));
	assign vsync = !((v >= (V_ACTIVE + V_FRONT_PORCH)) && (v < (V_ACTIVE + V_FRONT_PORCH + V_SYNC)));

	localparam MARGIN = 20;
	wire leftright = (h == MARGIN) || (h == (H_ACTIVE - 1 - MARGIN));
	wire topbottom = (v == MARGIN) || (v == (V_ACTIVE - 1 - MARGIN));

`ifdef ENABLE_VRAM
	localparam VRAM_WIDTH = 40;
	localparam VRAM_HEIGHT = 30;
	wire [9:0] x = h >> $clog2(640 / VRAM_WIDTH);
	wire [9:0] y = v >> $clog2(480 / VRAM_HEIGHT);

	reg [3:0] vram [VRAM_HEIGHT - 1:0][VRAM_WIDTH - 1:0][2:0];
	integer iy;
	integer ix;
	initial begin
		for(iy = 0; iy < VRAM_HEIGHT; iy = iy + 1) begin
			for(ix = 0; ix < VRAM_WIDTH; ix = ix + 1) begin
				vram[iy][ix][0] = ix[3:0] & 4'b1111;
				vram[iy][ix][1] = 4'b0000;
				vram[iy][ix][2] = iy[3:0] & 4'b1111;
			end
		end
	end
`endif

	always @ (posedge clk50mhz) begin
		if (rst || (!active)) begin
			pr <= 4'b0000;
			pg <= 4'b0000;
			pb <= 4'b0000;
		end else begin
			case (regs[0][1:0])
				MODE_TEST: begin
					pr <= leftright ? 4'b1111 : 4'b0000;
					pg <= topbottom ? 4'b1111 : 4'b0000;
					pb <= 4'b1111;
				end
				MODE_GREEN: begin
					pr <= 4'b0000;
					pg <= 4'b1111;
					pb <= 4'b0000;
				end
				MODE_VRAM: begin
`ifdef ENABLE_VRAM
					pr <= vram[y][x][0];
					pg <= vram[y][x][1];
					pb <= vram[y][x][2];
`else
					pr <= 4'b1111;
					pg <= 4'b0000;
					pb <= 4'b0000;
`endif
				end
				MODE_TILES: begin
				end
				default: begin
					pr <= 4'b0000;
					pg <= 4'b0000;
					pb <= 4'b0000;
				end
			endcase
		end
	end
endmodule