`timescale 10ns / 10ns

module sevensegment_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	reg rst = 1;

	reg [31:0] bus_addr = 0;
	reg [31:0] bus_out = 0;
	wire [31:0] bus_in;
	reg bus_write_and_select = 0;
	wire [6:0] pins;

	sevensegment sevensegment0(clk, rst, bus_addr, bus_out, bus_in, bus_write_and_select, bus_write_and_select, pins);

	initial begin
		repeat(3) @ (negedge clk);
		rst <= 0;
		repeat(1) @ (negedge clk);
		bus_addr <= 1;
		bus_out <= 'h15;
		bus_write_and_select <= 1;
		repeat(1) @ (negedge clk);
		bus_write_and_select <= 0;
		repeat(1) @ (negedge clk);
		bus_addr <= 0;
		bus_out <= 'h11;
		bus_write_and_select <= 1;
		repeat(1) @ (negedge clk);
		bus_write_and_select <= 0;
		repeat(3) @ (negedge clk);
		$finish;
	end

	initial begin
		$dumpfile("sevensegment.vcd");
		$dumpvars(0, sevensegment0, pins);
	end
endmodule