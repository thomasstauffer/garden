
module clkenable #(parameter DIVISOR = 100000000)(input wire clk, input wire rst, output reg enable);
	reg [$clog2(DIVISOR):0] counter = 0;
	initial begin
		enable <= 0;
	end
	always @ (posedge clk) begin
		if (rst) begin
			counter <= DIVISOR;
			enable <= 0;
		end else begin
			if (counter == DIVISOR) begin
				enable <= 1;
				counter <= 0;
			end else begin
				enable <= 0;
				counter <= counter + 1;
			end
		end
	end
endmodule
