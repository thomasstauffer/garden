`timescale 10ns / 10ns

module cpu_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	reg rst = 1;

	cpu cpu0(clk, rst);

	initial begin
		repeat(1) @ (negedge clk);
		rst <= 0;
		repeat(100) @ (negedge clk);
		$finish;
	end

	initial begin
		$dumpfile("cpu.vcd");
		$dumpvars(0, cpu0);
	end
endmodule