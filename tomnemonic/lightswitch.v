module lightswitch(input wire clk, input wire a, input wire b, input wire c, output wire out);
	reg enabled;

	initial begin
		enabled <= 0;
	end

	always @ (posedge clk) begin
		enabled <= a | b | c;
	end

	assign out = enabled;
endmodule