`timescale 10ns / 10ns

module clkenable_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	reg rst = 1;
	wire enable;

	clkenable #(0) clkenable0 (clk, rst, enable);
	clkenable #(1) clkenable1 (clk, rst, enable);
	clkenable #(2) clkenable2 (clk, rst, enable);
	clkenable #(5) clkenable5 (clk, rst, enable);

	initial begin
		repeat(3) @ (negedge clk);
		rst <= 0;
		repeat(30) @ (negedge clk);
		$finish;
	end

	initial begin
		$dumpfile("clkenable.vcd");
		$dumpvars(0, clkenable0, clkenable1, clkenable2, clkenable5);
	end
endmodule