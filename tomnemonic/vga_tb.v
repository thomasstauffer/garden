`timescale 10ns / 10ns

module vga_tb;
	reg clk = 1;
	always begin
		#1;
		clk = ~clk;
	end

	reg rst = 1;
	reg [1:0] mode = 1;
	wire [3:0] vga_r;
	wire [3:0] vga_g;
	wire [3:0] vga_b;
	wire vga_hsync;
	wire vga_vsync;

	vga_sim vga_sim0(clk, rst, vga_r, vga_g, vga_b, vga_hsync, vga_vsync);

	initial begin
		#10
		rst <= 0;
		#2000
		$finish;
	end

	initial begin
		$dumpfile("vga.vcd");
		$dumpvars(0, vga_sim0);
	end
endmodule