
module reset #(parameter CYCLES=50000000)(input wire clk, output wire rst);
	reg [$clog2(CYCLES):0] counter = 0;

	assign rst = counter < CYCLES;

	always @ (posedge clk) begin
		if (counter < CYCLES) begin
			counter <= counter + 1;
		end
	end
endmodule