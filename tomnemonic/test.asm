
:start

; test
# test

 nop    ; nop

; hex0

mvi r0, 0x0100
mvi r1, 0x15
st r0, r1

; hex1

mvi r0, 0x0200
mvi r1, 0x17
st r0, r1

; switch -> video

mvi r0, 0x600
mvi r1, 0x300
mvi r2, 0x500
:switchmode
ld r3, r0
ld r3, r0
ld r3, r0
;ld r3, r0
;ld r3, r0
;st r0, r3
;st r1, r3
;st r2, r3
nop
nop
nop

bp
nop

jmpi r4, switchmode

; video

jmpi r0, sub
mvi r0, 0x0500
mvi r1, 0x0001
st r0, r1

jmpi r0, sub
mvi r0, 0x0500
mvi r1, 0x0002
st r0, r1

mvi r0, 0x7777

bp

; wait 1 sec

:sub
mvi r1, 1
mvi r2, 1
mvi r3, 1 ; timeout shift 26 = ca. 1.3 s
shll r2, r2, r3
:loop
sub r2, r2, r1
brne r2, r1, loop
jmp r0, r0

bp

:forever
jmpi r0, forever

mvi r0, 0
mvi r1, 0x2222
mvi r2, 0x3333
add r3, r1, r2 ; add
sub r3, r2, r1

nop

mvi r0, 0
mvi r1, 1
mvi r2, 7
mvi r3, 0

:label

sub r2, r2, r1
add r3, r3, r1
brne r2, r0, label

nop

bp
