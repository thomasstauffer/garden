const vscode = require('vscode');

function hello() {
	console.log('tomark: hello');

	vscode.window.showInformationMessage('Hello');
}

function preview() {
	console.log('tomark: preview');

	const panel = vscode.window.createWebviewPanel(
		'tomark.preview',
		'TomArk Preview',
		vscode.ViewColumn.One,
		{ enableFindWidget: true, enableScripts: false });

	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		return;
	}

	const text = editor.document.getText();
	const html = text.split('\n').reduce((acc, line) => {
		// TODO simplified use tomark module
		if (line.startsWith('= ')) {
			return acc + '<h1>' + line + '</h1>';
		} else if (line.startsWith('== ')) {
			return acc + '<h2>' + line + '</h2>';
		} else if (line.startsWith('=== ')) {
			return acc + '<h3>' + line + '</h3>';
		} else if (line.startsWith('==== ')) {
			return acc + '<h4>' + line + '</h4>';
		} else {
			return acc + '<p>' + line + '</p>';
		}
	}, '');

	panel.webview.html = html;
}

function quotation() {
	console.log('tomark: quotation');

	const editor = vscode.window.activeTextEditor;
	const selections = editor.selections;

	editor.edit(editBuilder => {
		selections.forEach(selection => {
			const text = editor.document.getText(selection);
			// TODO this depends heavily on the language
			const replaced = text.replace(/([\s\.])"/g, '$1»').replace(/"([\s\.])/g, '«$1');
			editBuilder.replace(selection, replaced);
		});
	});
}

function activate(context) {
	console.log('tomark: activate');

	context.subscriptions.push(vscode.commands.registerCommand('tomark.hello', hello));
	context.subscriptions.push(vscode.commands.registerCommand('tomark.preview', preview));
	context.subscriptions.push(vscode.commands.registerCommand('tomark.quotation', quotation));

	/*
	context.subscriptions.push(vscode.languages.registerDocumentSymbolProvider(
		{ scheme: '', language: 'tomark' }, new SymbolProvider()
	));
	*/

	const outlineProvider = new OutlineProvider();
	vscode.window.registerTreeDataProvider('tomark.outline', outlineProvider);
}

function reSearchAll(pattern, text) {
	const re = RegExp(pattern, 'gm');
	const matches = [];
	let match;
	while ((match = re.exec(text)) !== null) {
		matches.push([match.index, match]);
	};
	return matches;
}

class SymbolProvider {
	provideDocumentSymbols(document, token) {
		// https://code.visualstudio.com/api/references/vscode-api#SymbolKind
		console.log('tomark: SymbolProvider::provideDocumentSymbols');
		const position1 = document.positionAt(0);
		const symbol1 = new vscode.DocumentSymbol('Name', 'Detail',
			vscode.SymbolKind.Class, new vscode.Range(position1, position1), new vscode.Range(position1, position1));
		const position2 = document.positionAt(10);
		const symbol2 = new vscode.DocumentSymbol('Name', 'Detail',
			vscode.SymbolKind.Class, new vscode.Range(position2, position2), new vscode.Range(position2, position2));
		return [symbol1, symbol2];
	}
}

class OutlineProvider {
	constructor() {
		this._onDidChangeTreeData = new vscode.EventEmitter();
		this.onDidChangeTreeData = this._onDidChangeTreeData.event;
		vscode.window.onDidChangeActiveTextEditor(editor => this.update(editor));
		this.update(vscode.window.activeTextEditor);
	}

	static tocFromText(text) {
		const matches = reSearchAll('^([=]+)([^\n]+)', text);

		const toc = {children: {}, name: '', index: 0};
		let current1 = '';
		let current2 = '';
		let current3 = '';
		let current4 = '';

		matches.forEach(match => {
			const name = match[1][2].trim();
			const indent = Math.max(Math.min(match[1][1].length, 4), 1);
			const index = match[0];
			const key = name + '-' + index;

			if(indent === 1) {
				current1 = key;
				toc.children[current1] = {children: {}, name: name, index: index};
			}
			if(indent === 2) {
				current2 = key;
				toc.children[current1].children[current2] = {children: {}, name: name, index: index};
			}
			if(indent === 3) {
				current3 = key;
				toc.children[current1].children[current2].children[current3] = {children: {}, name: name, index: index};
			}
			if(indent === 4) {
				current4 = key;
				toc.children[current1].children[current2].children[current3].children[current4] = {children: {}, name: name, index: index};
			}
		});

		return toc;
	}

	update(editor) {
		console.log('tomark: OutlineProvider::update');

		const document = editor.document;
		const text = document.getText();

		this.treeItems = {'': [null, []]};

		if(document.languageId !== 'tomark') {
			this._onDidChangeTreeData.fire();
			return;
		}

		let toc = {};
		try{
			toc = OutlineProvider.tocFromText(text);
		} catch(e) {
			console.log(e);
		}

		this.treeItems = {};
		const create = (key, value) => {
			const treeItem = new vscode.TreeItem(value.name);
			const childrenKeys = Object.keys(value.children);
			if (childrenKeys.length > 0) {
				treeItem.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
			} else {
				treeItem.collapsibleState = vscode.TreeItemCollapsibleState.None;
			}
			const position = document.positionAt(value.index);
			treeItem.command = {
				command: 'revealLine',
				title: '',
				arguments: [{ lineNumber: position.line, at: 'top' }]
			};
			this.treeItems[key] = [treeItem, childrenKeys];
			childrenKeys.forEach(childKey => {
				create(childKey, value.children[childKey]);
			});
		}
		create('', toc);

		this._onDidChangeTreeData.fire();
	}

	getChildren(key) {
		return this.treeItems[key === undefined ? '' : key][1];
	}

	getTreeItem(key) {
		return this.treeItems[key][0];
	}
}

function deactivate() {
	console.log('tomark: deactivate');
}

module.exports = {
	activate,
	deactivate
}
