
import peg from '../Libraries/peg-0.10.0/peg-0.10.0.js';

import * as q from '../crow/q.js';
import * as tomath from '../tomath/tomath.js';
import * as format from '../crow/format.js';

const grammar = `

{
const flatten = (array) => array.flat();
const line = () => location().start.line;
}

start = nl* tags:line* { return tags; }

line = line_tag / line_no_tag / line_empty

line_tag = tag:tag params:param* values:(embed / text)* eol { return [[tag, line()]].concat(flatten(params)).concat(values); }
line_no_tag = params:param* text:text values:(embed / text)* eol { return [['~', line()]].concat(flatten(params)).concat([text]).concat(values); }
line_empty = (nl+ eof?) { return [['~', line()]]; }

embed = tag:tag params:param+ { return [[tag, line()]].concat(flatten(params)); }

text = chars:text_char+ { return chars.join(''); }
text_char = !eol !embed !param_cooked_close char:$((escape .) / .) { return char.slice(-1); }

param = param:(param_raw3 / param_raw2 / param_raw1 / param_cooked) { return param; }

param_raw3 = "[[[" chars:(!"]]]" char:. { return char; })* "]]]" { return chars.join(''); }
param_raw2 = "[[" chars:(!"]]" char:. { return char; })* "]]" { return chars.join(''); }
param_raw1 = "[" chars:(!"]" char:. { return char; })* "]" { return chars.join(''); }

param_raw = "[" body:param_raw_body "]" { return body; }
param_raw_body = param_raw / chars:param_raw_char* { return chars.join(''); }
param_raw_char = !"]" char:. { return char; }

param_cooked = param_cooked_open nl* values:(line / embed / text)* param_cooked_close { return values; }

escape = "\\\\"
nl = [\\n\\r]
param_cooked_open = "{"
param_cooked_close = "}"
tag = $ ("~"+ / "@"+ / "%"+ / "!"+ / "?"+ / "$"+ / "&"+ / "#"+ / "^"+ / "<"+ / ">"+ / "|"+ / "="+ / "+"+ / "*"+ / "-"+ / "_"+ / "/"+)
eof = !.
eol = nl / eof

`;

// the first element in an element in the AST is always a list, where the first elment is a tag and the second is the line number
const TAG = 0;
const LINE = 1;

function tag(name, value) {
	return '<' + name + ' class="tomark-' + name + '">' + value + '</' + name + '>';
}

/*
function defaultValue(value, defaultValue) {
	return value === undefined ? defaultValue : value;
}
*/

// TODO somehting akin to a visitor-pattern to not repeat everything, that is trully stupid from my side
// TODO move grammar into it's own file, also add test cases

function astToTOCIter(ast, level) {
	return q.reduce(ast, function (result, value, i) {
		const current = ast[i];
		if (q.isArray(current)) {
			return result + astToTOC(current, level);
		} else {
			return result + tag('span', current);
		}
	}, '');
}

export function astToTOC(ast, level) {
	const current = ast;
	const op = current[0][TAG];
	if (op.startsWith('°')) {
		return astToTOCIter(current.slice(1), level);
	} else if (op.startsWith('=')) {
		const jumpTo = '<a href="#" data-line="' + current[0][LINE] + '">-&gt;</a>';

		const levelNew = op.length;

		// TODO this is buggy, isn't it? add some test cases somewhere
		const openCount = Math.max(levelNew - level, 0);
		//const closeCount = Math.max(level - levelNew, 0);

		const openTags = '<ul>'.repeat(openCount);
		const closeTags = '</ul>'.repeat(openCount);

		return openTags + tag('li', astToTOCIter(current.slice(1), levelNew) + ' ' + jumpTo) + closeTags;
	}
	return '';
}

function escapeHtml(unsafe) {
	return unsafe
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/'/g, '&quot;')
		.replace(/'/g, '&apos;'); // HTML5 only
}

function astToHtmlIter(ast) {
	return q.reduce(ast, function (result, value, i) {
		const current = ast[i];
		const prev = i > 0 ? ast[i - 1] : [['', 0]];
		const next = i < (ast.length - 1) ? ast[i + 1] : [['', 0]];

		if (q.isArray(current)) {
			return result + astToHtml(current, prev, next);
		} else {
			return result + tag('span', escapeHtml(current));
		}
	}, '');
}

function parseMath(string) {
	try {
		return tomath.toString(tomath.parse(tomath.tokenize(string)));
	} catch (error) {
		console.error('Math Conversion Error');
		console.log(error);
	}
	return '<span>' + escapeHtml(string) + '</span>';
}

// https://a.b.c:88/path -> b.c
function getHostName(url) {
	if (url.startsWith('http')) {
		return new URL(url).hostname.split('.').slice(-2).join('.');
	} else {
		return url;
	}
}

export function astToHtml(current, prev = [['', 0]], next = [['', 0]]) {
	const op = current[0][0];
	const prevOp = prev[0][0];
	const nextOp = next[0][0];
	const line = '<span id="line' + current[0][LINE] + '">' + '' + '</span>';

	if (op.startsWith('°')) {
		return astToHtmlIter(current.slice(1));
	} else if (op.startsWith('~')) {
		return line + tag('p', astToHtmlIter(current.slice(1)));
	} else if (op.startsWith('=')) {
		const count = op.length;
		return line + tag('h' + count, astToHtmlIter(current.slice(1)));
	} else if (op.startsWith('!')) {
		const src = current[current.length - 1];
		return line + '<img src="' + src + '">';
	} else if (op.startsWith('@')) {
		if (current.length === 2) {
			return line + tag('pre', escapeHtml(current[1]));
		} else if (current.length >= 3) {
			if (current[1] === 'embed') {
				return current[3];
			} else if (current[1] === 'format') {
				return line + tag('code', format.toString(format.tokenize(current[3].trim(), current[2])));
			} else if (current[1] === 'run') {
				//console.log(current[2], current[3]);
				// child_process is not available in the browser, fallback? function should not be async
				//import * as child_process from 'child_process';
			} else if (current[1] === 'include') {
				console.error('include Not Implemented');
			} else if (current[1] === 'toc') {
				console.error('toc Not Implemented');
				return line + tag('toc');
			} else {
				console.log('Unknown Format Length ' + current.length + ' ' + JSON.stringify(current));
			}
		} else {
			console.log('Unknown Format ' + current.length + current);
		}
		return '';
	} else if (op.startsWith('*')) {
		return line + tag('strong', astToHtmlIter(current.slice(1)));
	} else if (op.startsWith('/')) {
		return line + tag('em', astToHtmlIter(current.slice(1)));
	} else if (op.startsWith('_')) {
		return line + tag('u', astToHtmlIter(current.slice(1)));
	} else if (op.startsWith('#')) {
		return '';
	} else if (op.startsWith('?')) {
		return '';
	} else if (op.startsWith('&')) {
		const ref = current.length > 1 ? current.slice(-1)[0] : '';
		const name = current.length > 2 ? current.slice(-2)[0] : getHostName(ref);
		return line + '<a href="' + ref + '">' + name + '</a>';
	} else if (op.startsWith('%')) {
		return '';
	} else if (op.startsWith('<')) {
		return astToHtmlIter(current.slice(1));
	} else if (op.startsWith('>')) {
		return '';
	} else if (op.startsWith('^')) {
		return '';
	} else if (op.startsWith('$')) {
		//html += '`' + current[1] +'`'
		if (current.length < 2) {
			return '';
		}
		const html = parseMath(current[1]);
		return line + '<span>' + html + '</span>';
	} else if (op.startsWith('-') || op.startsWith('+')) {
		let countOpen = Math.max(op.length - prevOp.length, 0);
		if (prevOp[0] !== op[0]) {
			countOpen = op.length;
		}
		let countClose = op.length - nextOp.length;
		countClose = Math.max(op.length - nextOp.length, 0);
		if (nextOp[0] !== op[0]) {
			countClose = op.length;
		}
		const name = { '-': 'ul', '+': 'ol' }[op[0]];
		const open = ('<' + name + ' class="tomark-' + name + '">').repeat(countOpen);
		const items = tag('li', astToHtmlIter(current.slice(1)));
		const close = ('</' + name + '>').repeat(countClose);
		return line + open + items + close;
	} else if (op.startsWith('|')) {
		let elems = [''];
		for (let i = 1; i < current.length; i++) {
			if (q.isArray(current[i])) {
				elems[elems.length - 1] += astToHtml(current[i]);
			} else {
				const tds = current[i].split('|');
				elems[elems.length - 1] += tds[0];
				for (let j = 1; j < tds.length; j++) {
					//if(elems[elems.length - 1].length > 0) {
					//}
					elems.push('');
					elems[elems.length - 1] += tds[j];
				}
			}
		}

		if (elems[elems.length - 1] == '') {
			elems.pop();
		}

		const open = (op !== prevOp) ? '<table class="tomark-table">' : '';
		const rows = '<tr><td class="tomark-td">' + elems.join('</td><td class="tomark-td">') + '</td></tr>';
		const close = (op !== nextOp) ? '</table>' : '';
		return line + open + rows + close;
	}

	console.log(current);
	throw 'Not Implemented';
}

export function compile(text) {
	let ast = parser.parse(text);
	// TODO explain °
	ast.unshift(['°', 0]);
	return ast;
}

let parser = undefined;
try {
	parser = peg.generate(grammar);
} catch (err) {
	console.log(err);
}

export function toHTML(text) {
	const ast = compile(text);
	//const toc = astToTOC(ast, 0);
	return astToHtml(ast);
}

function replaceSpecial(s) {
	let result = s;
	// Guillemets
	// https://de.wikipedia.org/wiki/Anf%C3%BChrungszeichen
	result = result.replace(/ "/g, ' «');
	result = result.replace(/" /g, '» ');
	result = result.replace(/^"/g, '«');
	result = result.replace(/"$/g, '»');
	// https://typefacts.com/artikel/apostroph
	result = result.replace(/'/g, '‘');
	// https://de.wikipedia.org/wiki/Auslassungspunkte
	result = result.replace(/\.\.\./g, '…');
	// Minus - vs Dash -
	return result;
}

export function test() {
	function assertEqualAst(string, expectedAst) {
		const parsedAst = parser.parse(string);
		if (!q.isEqual(parsedAst, expectedAst)) {
			console.trace();
			console.log('Parsed:', JSON.stringify(parsedAst));
			console.log('Expected:', JSON.stringify(expectedAst));
			throw 'Parser Test Failure';
		}
	}

	assertEqualAst('\n\n', []); // TODO normally empty lines are not deleted, keep this also?

	assertEqualAst('ab', [[['~', 1], 'ab']]);

	assertEqualAst('a\nb', [[['~', 1], 'a'], [['~', 2], 'b']]);

	// at this stage is is important to keep empty lines, else it is impossible to merge later (lists, tables, ...)
	assertEqualAst('a\n\nb', [[['~', 1], 'a'], [['~', 2]], [['~', 3], 'b']]);

	assertEqualAst('=ab', [[['=', 1], 'ab']]);
	assertEqualAst('= ab', [[['=', 1], ' ab']]);
	assertEqualAst('=     ab', [[['=', 1], '     ab']]);

	assertEqualAst('=ab', [[['=', 1], 'ab']]);
	assertEqualAst('=[1]ab', [[['=', 1], '1', 'ab']]);
	assertEqualAst('=[1][2][3][4][5]ab', [[['=', 1], '1', '2', '3', '4', '5', 'ab']]);

	assertEqualAst('=[1]ab', [[['=', 1], '1', 'ab']]);
	assertEqualAst('=[[1]]ab', [[['=', 1], '1', 'ab']]);
	assertEqualAst('=[[[1]]]ab', [[['=', 1], '1', 'ab']]);
	//is it possible to implement something like HERE-DOCUMENT with PEG.js easily, to also support arbitrary number of []
	//assertEqualAst('=[[[[[[1]]]]]]ab', [[['=', 1], '1', 'ab']]);

	assertEqualAst('={1}ab', [[['=', 1], '1', 'ab']]);

	assertEqualAst('ab c@d.e fg', [[['~', 1], 'ab c@d.e fg']]);
	assertEqualAst('ab ![cd] ef', [[['~', 1], 'ab ', [['!', 1], 'cd'], ' ef']]);
	assertEqualAst('ab *{cd} ef', [[['~', 1], 'ab ', [['*', 1], 'cd'], ' ef']]);

	assertEqualAst('~*{txt}', [[['~', 1], [['*', 1], 'txt']]]);

	// TODO check html output, e.g. table afer table (they should not be merged)

	q.assertEqual(replaceSpecial('...'), '…');

	function onlyLineTags(ast) {
		return ast.slice(1).map(_ => _[0][TAG]);
	}

	q.assertEqual(onlyLineTags(compile('')), []);
	q.assertEqual(onlyLineTags(compile('=')), ['=']);
	q.assertEqual(onlyLineTags(compile('=xyz')), ['=']);
	q.assertEqual(onlyLineTags(compile('=xyz\n')), ['=']);
	q.assertEqual(onlyLineTags(compile('=xyz\n=xyz\n')), ['=', '=']);
}
