
import * as tomark from './tomark.js';
import fs from 'fs';

const HEADER = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>TomArk</title>
<style>

body {
	font-family: Ubuntu;
	column-count: 2;
	column-gap: 1em;
	column-rule: 1px solid #000;
}

.tomark-format-keyword {
	color: blue;
}

.tomark-format-number {
	color: teal;
}

.tomark-format-name {
	color: darkblue;
}

.tomark-format-op {
	color: gray;
}

.tomark-format-string {
	color: orange;
}

ul {
	list-style-type: square;
}

td {
	border: 1px dotted lightgrey;
}

@media screen {

body {
	width: 80em;
	margin: 1em auto;
}

}

ul {
	list-style-type: square;
}

</style>
</head>
<body>
`;

const FOOTER = `
</body>
</html>
`;

const args = process.argv.slice(2);

const src = args[0];
const dst = args[1];

console.log('Src:', src);
console.log('Dst:', dst);

const text = fs.readFileSync(src, 'utf8');

const ast = tomark.compile(text);
const html = HEADER + tomark.astToHtml(ast) + FOOTER;

fs.writeFileSync(dst, html);
