
import * as q from './q.js';
import * as error from './error.js';

export function equal(a, b) {
	if (!q.isEqual(a, b)) {
		error.Assert(a + ' == ' + b);
	}
}

export function almostEqual(a, b, epsilon) {
	if (Math.abs(a - b) >= epsilon) {
		error.Assert(a + ' ~= ' + b);
	}
}

export function isTrue(arg) {
	equal(arg, true);
}

export function isFalse(arg) {
	equal(arg, false);
}

export function isType(variable, type) {
	if ((variable === undefined) || (variable === null) || (variable.constructor !== type)) {
		error.InvalidArgument('Invalid Type "' + variable + '" != ' + type);
	}
}

export function test() {
	equal(1, 1);
	equal([1, 2, 3], [1, 2, 3]);
	equal({a: 1, b: 2}, {b: 2, a: 1});
	isTrue(true);
	isFalse(false);
	class TestClass {}
	isType(0, Number);
	isType([], Array);
	isType('', String);
	isType({}, Object);
	isType(new TestClass(), TestClass);
}
