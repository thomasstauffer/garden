import * as q from './q.js';

/*

seed -> 0 .. 0.999...
next() -> 0 .. 0.999...

https://en.wikipedia.org/wiki/List_of_random_number_generators
*/

// https://en.wikipedia.org/wiki/Linear_congruential_generator
export class LCG {
	constructor(seed) {
		console.assert((seed >= 0.0) && (seed < 1.0));

		this.a = 1103515245;
		this.c = 12345;
		this.m = 2147483648; // 2 ** 32

		this.state = Math.floor(seed * (this.m));

		console.assert((this.state >= 0) && (this.state < this.m));
	}

	next() {
		this.state = ((this.a * this.state) + this.c) % this.m;
		return this.state / this.m;
	}

	getState() {
		return this.state;
	}

	setState(state) {
		this.state = state;
	}
}

// http://vigna.di.unimi.it/ftp/papers/xorshiftplus.pdf
// https://de.wikipedia.org/wiki/Xorshift
// https://v8.dev/blog/math-random
export class XORShift128 {
	constructor(seed) {
		console.assert((seed >= 0.0) && (seed < 1.0));

		this.x = 123456789;
		this.y = 362436069;
		this.z = 521288629;
		this.w = 88675123;
	}

	next() {
		const t = (this.x ^ (this.x << 11));
		this.x = this.y;
		this.y = this.z;
		this.z = this.w;
		this.w ^= (this.w >>> 19) ^ t ^ (t >>> 8);
		if (this.w < 0) {
			this.w = this.w + (2 ** 32);
		}

		//return this.w;
		return this.w / (2 ** 32);
	}

	getState() {
		return [this.x, this.y, this.z, this.w];
	}

	setState(state) {
		[this.x, this.y, this.z, this.w] = state;
	}
}

export class Random {
	constructor(generator) {
		this.generator = generator;
	}

	// adding get/setState is against the Liskov Substitution Principle

	next() {
		return this.generator.next();
	}

	nextInt(lowerInclusive, upperExclusive) {
		const range = upperExclusive - lowerInclusive;
		return Math.floor(this.next() * range) + lowerInclusive;
	}
}

// random test https://softwareengineering.stackexchange.com/questions/147134/how-should-i-test-randomness

export function isChiSquared() {
	q.errorNotImplemented();
}

function isChiSquaredTest() {
	q.assertTrue(true);
}

export function test() {
	const random = new Random(new XORShift128(0.5));
	//const random = new Random(new LCG(0.5));

	const testSetGet = new XORShift128(0.5);
	//console.log(testSetGet.getState());
	testSetGet.setState(testSetGet.getState());
	//console.log(testSetGet.getState());

	/*
	for(let i = 0; i < 5; i += 1) {
		console.log(random.next());
	}
	for(let i = 0; i < 5; i += 1) {
		console.log(random.nextInt(0, 5));
	}
	*/

	isChiSquaredTest();

	const n = 5000;
	const randomUpper = 5;
	const histogram = {};
	for (let i = 0; i < randomUpper; i += 1) {
		histogram[i] = 0;
	}
	for (let i = 0; i < n; i += 1) {
		histogram[random.nextInt(0, randomUpper)] += 1;
	}
	//console.log(histogram);
}
