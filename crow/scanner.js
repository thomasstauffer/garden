import * as error from './error.js';
import * as assert from './assert.js';

/*
Takes the first match (not the longest).
*/
export function scanner(rulesRaw, string) {
	const rules = rulesRaw.map($ => ({...$, re: new RegExp($.re, 's')}));
	const tokens = [];
	let index = 0; // TODO const, rewrite in a functional style?
	const forever = true;
	while (forever) {
		const rest = string.slice(index);
		const head = string.substring(0, index);
		const line = head.split('\n').length;
		const column = head.length - head.lastIndexOf('\n');
		if (rest === '') {
			break;
		}
		const indexPrevious = index;
		rules.some(rule => {
			const result = rule.re.exec(rest);
			if (result !== null) {
				if (!rule.skip) {
					const value = result.length > 1 ? result[1] : result[0]; // take the first capture group, if there is one, if not take everything
					tokens.push({name: rule.name, value: value, line: line, column: column, offset: index}); // TODO add location
				}
				index += result[0].length;
				return true; // return early
			}
		});
		if (indexPrevious === index) {
			// TODO create appropriate error with location? how to return it? catch it? exceptions return an object in case there is one?
			error.IllegalState(line + ':' + column + ' ' + rest.trim().substring(0, 66));
		}
	}
	return tokens;
}

export function test() {
	const rules = [
		{ name: 'name', re: '^[a-zA-Z]+', skip: false },
		{ name: 'number', re: '^[0-9]+', skip: false },
		{ name: '_', re: '^[ \n]+', skip: true },
	];

	const tokens1 = scanner(rules, 'Hello World');
	assert.equal(tokens1.length, 2);

	const tokens2 = scanner(rules, 'Hello\n 123\nWorld');
	assert.equal(tokens2[0], { name: 'name', value: 'Hello', line: 1, column: 1, offset: 0 });
	assert.equal(tokens2[1], { name: 'number', value: '123', line: 2, column: 2, offset: 7 });
	assert.equal(tokens2[2], { name: 'name', value: 'World', line: 3, column: 1, offset: 11 });
	assert.equal(tokens2.length, 3);
}
