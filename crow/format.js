import * as scanner from './scanner.js';
import * as html from './html.js';

const SH = [
	{ name: 'keyword', re: '^(for|done|do|if|else|in)', skip: false },
	{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_]*', skip: false },
	{ name: 'number', re: '^([0-9]+(\\.[0-9]+)?)', skip: false },
	{ name: 'paren', re: '^[(){}\\[\\]]', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const JAVASCRIPT = [
	{ name: 'keyword', re: '^(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)', skip: false },
	{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_]*', skip: false },
	{ name: 'paren', re: '^[(){}\\[\\]]', skip: false },
	{ name: 'op', re: '^[-+*/!=&|]+', skip: false },
	{ name: 'string', re: '^\'[^\']*\'', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const PYTHON = [
	{ name: 'keyword', re: '^(and|as|assert|break|class|continue|def|del|elif|else|except|False|finally|for|from|global|if|import|in|is|lambda|None|nonlocal|not|or|pass|raise|return|True|try|while|with|yield)', skip: false },
	{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_]*', skip: false },
	{ name: 'paren', re: '^[(){}\\[\\]]', skip: false },
	{ name: 'op', re: '^[-+*/!=&|]+', skip: false },
	{ name: 'string', re: '^\'[^\']*\'', skip: false },
	{ name: 'string', re: '^"[^"]*"', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const LISP = [
	{ name: 'keyword', re: '^(lambda)', skip: false },
	{ name: 'paren', re: '^[()]', skip: false },
	{ name: 'string', re: '^"[^"]*"', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const XML = [
	{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_]*', skip: false },
	{ name: 'paren', re: '^(<|>|/>)', skip: false },
	{ name: 'string', re: '^"[^"]*"', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const CPP = [
	{ name: 'keyword', re: '^(class|public|private|protected|typedef|void|template|struct)', skip: false },
	{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_]*', skip: false },
	{ name: 'paren', re: '^[(){}\\[\\]]', skip: false },
	{ name: 'op', re: '^[-+*/!=&|]+', skip: false },
	{ name: 'string', re: '^"[^"]*"', skip: false },
	{ name: 'whitespace', re: '^.', skip: false },
];

const FORMATS = {
	'sh': SH,
	'javascript': JAVASCRIPT,
	'python': PYTHON,
	'lisp': LISP,
	'xml': XML,
	'cpp': CPP,
};

export function tokenize(input, format) {
	return scanner.scanner(FORMATS[format], input);
}

function toIntermediate(tokens) {
	const prefix = 'tomark-format-';
	const elements = tokens.map(token => {
		return token.name === 'whitespace' ? token.value : html.html('span', { 'class': prefix + token.name }, token.value);
	});
	return html.html('span', ...elements);
}

export function toString(tokens) {
	return html.toHTML(toIntermediate(tokens));
}

export function test() {

}
