
function makeError(name, message) {
	const error = new Error(message);
	error.name = name;
	return error;
}

export function NotImplemented(message) {
	console.trace();
	throw makeError('Error Not Implemented', message);
}

export function InvalidArgument(message) {
	console.trace();
	throw makeError('Error Invalid Argument', message);
}

export function IllegalState(message) {
	console.trace();
	throw makeError('Error Illegal State', message);
}

export function Assert(message) {
	console.trace();
	throw makeError('Error Assert', message);
}

export function test() {
}
