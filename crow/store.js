import * as q from './q.js';

export class Store {
	constructor(initialState) {
		this.state = initialState;
		this.listeners = [];
	}

	addEventListener(listener) {
		this.listeners.push(listener);
	}

	dispatchEvent(event) {
		this.listeners.forEach(listener => {
			this.state = listener(this.state, event);
		});
	}

	getState() {
		return this.state;
	}
}

export function test() {
	const store = new Store({a: 1});

	store.addEventListener((state, event) => {
		if (event === 'inc-a') {
			return {a: state.a + 1};
		}
		return {a: state.a};
	});

	q.assertEqual(store.getState().a, 1);
	store.dispatchEvent('inc-a');
	q.assertEqual(store.getState().a, 2);
	store.dispatchEvent('inc-any');
	q.assertEqual(store.getState().a, 2);
}
