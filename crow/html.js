import * as q from './q.js';
import * as error from '../crow/error.js';

export function html(tag, ...args) {
	return [tag, args];
}

export function mathml(tag, ...args) {
	const MATHML_NAMESPACE = 'http://www.w3.org/1998/Math/MathML';
	return [[MATHML_NAMESPACE, tag], args];
}

export function svg(tag, ...args) {
	const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
	return [[SVG_NAMESPACE, tag], args];
}

function escape(string) {
	return string.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
}

export function toHTML(tree) {
	const [tag, params] = tree;
	const attributes = [];
	const children = [];
	const tagName = q.getType(tag) === Array ? tag[1] : tag;
	params.forEach(param => {
		if (q.getType(param) === Object) {
			Object.keys(param).forEach(name => {
				if (name === 'onclick') {
					const value = JSON.stringify(param[name]).replaceAll('"', '\'');
					attributes.push(`onclick="dispatch(${value})"`);
				} else {
					attributes.push(`${name}="${param[name]}"`);
				}
			});
		} else if (q.getType(param) === String) {
			children.push(escape(param));
		} else if (q.getType(param) === Number) {
			children.push(param);
		} else if (q.getType(param) === Array) {
			children.push(toHTML(param));
		} else {
			error.NotImplemented('Unknown Type ' + q.getType(param));
		}
	});
	const html = `<${[tagName, ...attributes].join(' ')}>${children.join('')}</${tagName}>`;
	return html;
}

function dispatchLog(event) {
	console.log(event);
}

export function toDOM(tree, dispatch=dispatchLog) {
	const [tag, params] = tree;
	const node = q.getType(tag) === Array ? document.createElementNS(tag[0], tag[1]) : document.createElement(tag);
	params.forEach(param => {
		if (q.getType(param) === Object) {
			Object.keys(param).forEach(name => {
				if (name === 'onclick') {
					node.addEventListener('click', () => {
						// e.preventDefault(); maybe this is necessary for correct behaviour for checkbox mark/unmark?
						dispatch(param[name]);
					});
				} else {
					node.setAttribute(name, param[name]);
				}
			});
		} else if ((q.getType(param) === String) || (q.getType(param) === Number)) {
			node.appendChild(document.createTextNode(param));
		} else if (q.getType(param) === Array) {
			node.appendChild(toDOM(param, dispatch));
		} else {
			error.NotImplemented('Unknown Type ' + q.getType(param));
		}
	});
	return node;
}

/*
Assumes that the DOM node is only modified by this function. The DOM is never
read by this function.

A first version worked on the DOM directly. But especially handling input
elements was somewhat cumbersome. Properties und attributes in the DOM are mixed
(e.g. checked for a checkbox). If the DOM is never read, all the relevant
information needs to be stored in the tree.

TODO

- If some parts are deleted or changed (e.g. a text node is changed into a
  regular element) the result is quite likely insane.
*/
function updateParam(node, paramsCurrent, paramsNew, dispatch, stat) {
	const paramsNewCount = paramsNew.length;
	let childIndex = 0;
	for (let i = 0; i < paramsNewCount; i += 1) {
		const paramCurrent = paramsCurrent[i] ?? {};
		const paramNew = paramsNew[i];
		const typeCurrent = q.getType(paramCurrent);
		const typeNew = q.getType(paramNew);

		if (typeNew === Object) {
			q.assertTrue(typeCurrent === Object); // TODO cannot iterate if typeCurrent is not an object
			Object.keys(paramNew).forEach(name => {
				const attributeNew = paramNew[name];
				const attributeCurrent = paramCurrent[name]; // may intentionally be undefined
				if (!q.isEqual(attributeNew, attributeCurrent)) {
					if (name === 'onclick') {
						const clone = node.cloneNode(true); // cloneNode does not copy event listeners
						node.replaceWith(clone);
						node = clone;
						node.addEventListener('click', () => {
							dispatch(attributeNew);
						});
						stat.addEventListener += 1;
					} else {
						node.setAttribute(name, attributeNew);
						stat.setAttribute += 1;
					}
				}
			});
			// delete old attributes
			Object.keys(paramCurrent).forEach(nameCurrent => {
				if (!(nameCurrent in paramNew)) {
					node.removeAttribute(nameCurrent);
				}
			});
		} else if ((typeNew === String) || (typeNew === Number)) {
			let nodeChild = node.childNodes[childIndex];
			if (!((typeCurrent === String) || (typeCurrent === Number))) {
				nodeChild = document.createTextNode(paramNew);
				node.appendChild(nodeChild); // TODO replaceWith?
				stat.createTextNode += 1;
			} else {
				if (paramNew !== paramCurrent) {
					q.assertTrue(nodeChild.nodeType === Node.TEXT_NODE);
					nodeChild.textContent = paramNew;
					stat.textContent += 1;
				}
			}
			childIndex += 1;
		} else if (typeNew === Array) {
			let nodeChild = node.childNodes[childIndex];
			if (typeCurrent !== Array) {
				nodeChild = document.createElement('span'); // add dummy element, it is replaced by the following update call
				// TODO can this be unified?
				node.appendChild(nodeChild);
			}
			childIndex += 1;
			updateTag(nodeChild, paramCurrent, paramNew, dispatch, stat);
		} else {
			error.NotImplemented('Unknown Type ' + typeNew);
		}
	}
	for (let i = childIndex; i < node.childNodes.length; i += 1) {
		node.childNodes[childIndex].remove(); // deliberately not i
	}
}

function updateTag(node, treeCurrent, treeNew, dispatch, stat) {
	if (q.getType(treeCurrent) !== Array) {
		treeCurrent = html('?TAG?');
	}

	const [tagCurrent, paramsCurrent] = treeCurrent;
	const [tagNew, paramsNew] = treeNew;

	if (!q.isEqual(tagNew, tagCurrent)) {
		const nodeNew = q.getType(tagNew) === Array ? document.createElementNS(tagNew[0], tagNew[1]) : document.createElement(tagNew);
		stat.createElement += 1;
		node.replaceWith(nodeNew);
		node = nodeNew;
		updateParam(node, [], paramsNew, dispatch, stat); // node was recreated and all children are therefore removed
	} else {
		updateParam(node, paramsCurrent, paramsNew, dispatch, stat);
	}
}

export function updateDOM(node, treeCurrent, treeNew, dispatch=dispatchLog) {
	const stat = {
		addEventListener: 0,
		createElement: 0,
		createTextNode: 0,
		textContent: 0,
		setAttribute: 0,
	};
	updateTag(node, treeCurrent, treeNew, dispatch, stat);
	//console.log(stat);
	return stat;
}

export function test() {
	function assert(tree, html) {
		q.assertEqual(toHTML(tree), html);
	}

	assert(html('p'), '<p></p>');
	assert(html('p', 'test'), '<p>test</p>');
	assert(html('p', {id: 666}, 'test'), '<p id="666">test</p>');
	// TODO object keys are not sorted and therefore result is not predictable
	assert(html('p', {id: 666, style: 'color: red'}, 'test'), '<p id="666" style="color: red">test</p>');
	assert(html('p', html('span')), '<p><span></span></p>');
	assert(html('p', html('span'), html('span')), '<p><span></span><span></span></p>');
	assert(html('p', html('span'), 'text', html('span')), '<p><span></span>text<span></span></p>');
	assert(html('p', html('em', 'text')), '<p><em>text</em></p>');

	assert(mathml('math'), '<math></math>');
	assert(svg('svg'), '<svg></svg>');
}
