/*
Ideas

- Immutable set functions for array, dict, string, ... immutable.js? Bad idea
  because it introduces a new set of containers which are not compatible with
  anything else.
- immer?
- Take some ideas from https://davidwalsh.name/javascript-tricks

Principles

- No DOM functions

*/

export function errorNotImplemented(message) {
	console.trace();
	throw Error('Not Implemented: ' + message);
}

export function errorInvalidArgument(message) {
	console.trace();
	throw Error('Invalid Argument: ' + message);
}

export function errorIllegalState(message) {
	console.trace();
	throw Error('Illegal State: ' + message);
}

export function errorAssert(message) {
	console.trace();
	throw Error('Assert: ' + message);
}

export function stacktrace(stripLevels=1) {
	let stack = '';
	try {
		new {};
	} catch (exception) {
		stack = exception.stack;
	}
	const lines = stack.split('\n').splice(stripLevels);
	return lines.join('\n');
}

export function helloQ() {
	console.log('Hello Q');
}

export function assertEqual(a, b) {
	if (!isEqual(a, b)) {
		console.trace();
		throw format('assert {0} != {1}', a, b);
	}
}

export function assertAlmostEqual(a, b, epsilon) {
	if (Math.abs(a - b) >= epsilon) {
		console.trace();
		throw format('assert {0} !~= {1}', a, b);
	}
}

export function assertTrue(arg) {
	assertEqual(arg, true);
}

export function assertFalse(arg) {
	assertEqual(arg, false);
}

export function getType(arg) {
	if (arg === undefined) {
		return undefined;
	}
	if (arg === null) {
		return null;
	}
	return arg.constructor;
}

export function assertType(variable, type) {
	if ((variable === undefined) || (variable === null) || (variable.constructor !== type)) {
		errorInvalidArgument('Invalid Type "' + variable + '" != ' + type);
	}
}

function assertTest() {
	assertTrue(true);
	assertFalse(false);

	class TestClass {}

	assertTrue(getType(0) === Number);
	assertTrue(getType(NaN) === Number);
	assertTrue(getType(Infinity) === Number);
	assertTrue(getType([]) === Array);
	assertTrue(getType('') === String);
	assertTrue(getType({}) === Object);
	assertTrue(getType(new TestClass()) === TestClass);
	assertTrue(getType(null) === null);
	assertTrue(getType(undefined) === undefined);

	assertType(0, Number);
	assertType([], Array);
	assertType('', String);
	assertType({}, Object);
	assertType(new TestClass(), TestClass);
}

export function isArray(arg) {
	return Array.isArray(arg);
}

function isArrayTest() {
	assertTrue(isArray([]));
	assertFalse(isArray(0));
	assertFalse(isArray(''));
	assertFalse(isArray({}));
}

export function identity(arg) {
	return arg;
}

function identityTest() {
	assertEqual(identity(1), 1);
	assertEqual(identity([]), []);
	assertEqual(identity({}), {});
	assertEqual(identity('x'), 'x');
}

export function getClass(object) {
	if (typeof(object) !== 'object') {
		return undefined;
	}
	if (object === null) {
		return null;
	}
	return object.constructor.name;
}

function getClassTest() {
	assertEqual(getClass(0), undefined);
	assertEqual(getClass(''), undefined);
	assertEqual(getClass(null), null);
	assertEqual(getClass([]), 'Array');
	assertEqual(getClass({}), 'Object');
	assertEqual(getClass({a: 1, b: 2}), 'Object');
	assertEqual(getClass(undefined), undefined);
}

export function isEqual(a, b) {
	if (typeof(a) != typeof(b)) {
		return false;
	}
	if (typeof(a) === 'string') {
		return a === b;
	} else if (typeof(a) === 'boolean') {
		return a === b;
	} else if (typeof(a) === 'undefined') {
		return a === b;
	} else if (typeof(a) === 'number') {
		return a === b;
	} else if (typeof(a) === 'object') {
		const aClass = getClass(a);
		const bClass = getClass(b);
		if (aClass !== bClass) {
			return false;
		}
		if (isArray(a) && isArray(b)) {
			if (a.length !== b.length) {
				return false;
			}
			for (let i = 0; i < a.length; i++) {
				if (!isEqual(a[i], b[i])) {
					return false;
				}
			}
			return true;
		} else if ((a === null) && (b === null)) {
			return true;
		} else if ((aClass === 'Object') && (bClass === 'Object')) {
			const aKeys = sort(Object.keys(a));
			const bKeys = sort(Object.keys(b));
			if (!isEqual(aKeys, bKeys)) {
				return false;
			}
			return every(aKeys, key => isEqual(a[key], b[key]));
		}
	}

	console.log(typeof(a), typeof(b));
	errorNotImplemented();
}

function isEqualTest() {
	assertTrue(isEqual(1, 1));
	assertTrue(isEqual(0, 0));
	assertFalse(isEqual(1, 2));

	assertTrue(isEqual(1, 1.0)); // TODO?
	//assertFalse(NaN, NaN); // TODO?

	assertTrue(isEqual('xyz', 'xyz'));
	assertFalse(isEqual('xyz', 'zyx'));

	assertTrue(isEqual(undefined, undefined));
	assertTrue(isEqual(null, null));

	assertTrue(isEqual([], []));
	assertTrue(isEqual([1, 2], [1, 2]));
	assertTrue(isEqual([1, [2, [3, 4]]], [1, [2, [3, 4]]]));
	assertFalse(isEqual([1, 2], [2, 1]));
	assertFalse(isEqual([1, [2, [3, 4]]], [1, [2, [3]]]));

	assertTrue(isEqual({}, {}));
	assertFalse(isEqual({a: 1, b: 2}, {a: 1}));
	assertFalse(isEqual({a: 1}, {a: 2}));
}

export function each(collection, iteratee) {
	if (isArray(collection)) {
		for (let key = 0; key < collection.length; key++) {
			const value = collection[key];
			const result = iteratee(value, key, collection);
			if (result !== undefined) {
				return result;
			}
		}
	} else if (typeof(collection) === 'object') {
		for (let key in collection) {
			const value = collection[key];
			const result = iteratee(value, key, collection);
			if (result !== undefined) {
				return result;
			}
		}
	} else {
		errorNotImplemented();
	}
}

function eachTest() {
	assertEqual(each([1, 2, 3], _item => 9), 9);
	const a = [0, 0, 0];
	each([1, 2, 3], (item, index) => {a[index] = item;});
	assertEqual(a, [1, 2, 3]);
}

export function map(collection, iteratee) {
	if (isArray(collection)) {
		let result = [];
		for (let key = 0; key < collection.length; key++) {
			const value = collection[key];
			result.push(iteratee(value, key, collection));
		}
		Object.freeze(result);
		return result;
	} else if (typeof(collection) === 'object') {
		let result = {};
		for (let key in collection) {
			const value = collection[key];
			result[key] = iteratee(value, key, collection);
		}
		Object.freeze(result);
		return result;
	} else {
		console.log(collection);
		errorNotImplemented();
	}
}

function mapTest() {
	assertEqual(map([]), []);
	assertEqual(map([1, 2, 3], x => x * 2), [2, 4, 6]);
	assertEqual(map({}), {});
	assertEqual(map({a: 1, b: 2}, x => x + 1), {a: 2, b: 3});
}

export function reduce(collection, iteratee=identity, accumulator) {
	if (isArray(collection)) {
		const start = accumulator === undefined ? 1 : 0;
		accumulator = accumulator === undefined ? collection[0] : accumulator;
		for (let key = start; key < collection.length; key++) {
			const value = collection[key];
			accumulator = iteratee(accumulator, value, key, collection);
		}
		Object.freeze(accumulator);
		return accumulator;
	} else if (typeof(collection) === 'object') {
		if (accumulator === undefined) {
			errorNotImplemented();
		}
		// TODO sort keys?
		for (let key in collection) {
			const value = collection[key];
			accumulator = iteratee(accumulator, value, key, collection);
		}
		Object.freeze(accumulator);
		return accumulator;
	} else {
		console.log(collection);
		errorNotImplemented();
	}
}

function reduceTest() {
	const add = (a, b) => a + b;
	assertEqual(reduce([], add), undefined);
	assertEqual(reduce([1, 2, 3], add), 6);
	assertEqual(reduce({a: 1, b: 2}, (a, b) => a + b, 0), 3);
}

export function filter(collection, predicate=identity) {
	if (isArray(collection)) {
		let result = [];
		for (let key = 0; key < collection.length; key++) {
			const value = collection[key];
			if (predicate(value) === true) {
				result.push(value);
			}
		}
		Object.freeze(result);
		return result;
	} else if (typeof(collection) === 'object') {
		let result = {};
		for (let key in collection) {
			const value = collection[key];
			if (predicate(value) === true) {
				result[key] = value;
			}
		}
		Object.freeze(result);
		return result;
	} else {
		console.log(collection);
		errorNotImplemented();
	}
}

function filterTest() {
	assertEqual(filter([1, 2, 3, 4], x => x % 2 === 0).length, 2);
	assertEqual(filter([1, 2, 3, 4], x => x >= 3)[0], 3);
}

function unionSlow(a, b) {
	if (isArray(a) && isArray(b)) {
		const aSet = new Set(a);
		const bSet = new Set(b);
		const result = Array.from(new Set([...aSet, ...bSet]));
		Object.freeze(result);
		return result;
	} else {
		console.log(a, b);
		errorNotImplemented();
	}
}

function union(arrays) {
	// TODO iteratee=identity
	if (isArray(arrays)) {
		const keys = {};
		each(arrays, array => {
			each(array, item => {
				keys[item] = undefined;
			});
		});
		const result = Object.keys(keys);
		Object.freeze(result);
		return result;
	} else {
		console.log(arguments);
		errorNotImplemented();
	}
}

function unionTest() {
	assertEqual(union([]), []);
	assertEqual(union([['1', '2', '3']]), ['1', '2', '3']);
	assertEqual(union([['1', '2', '3'], ['2', '3', '4']]), ['1', '2', '3', '4']);
}

export function merge(objects, iteratee=identity) {
	if (isArray(objects)) {
		const unionKeys = union(map(objects, keys));
		return pairsToObject(map(unionKeys, key => {
			return [key, iteratee(map(objects, x => x[key]))];
		}));
	} else {
		console.log(objects);
		errorNotImplemented();
	}
}

function mergeTest() {
	assertEqual(merge([{a: 1}, {a: 2}, {a: 3}], sum), {a: 6});
	assertEqual(merge([{a: 1}, {a: 2}, {a: 3}]), {a: [1, 2, 3]});
	function undefined0(items) {
		return map(items, x => x === undefined ? 0 : x);
	}
	assertEqual(merge([{a: 1}, {a: 2, b: 3}, {a: 4, b: 5, c: 6}], undefined0), {a: [1, 2, 4], b: [0, 3, 5], c: [0, 0, 6]});
}

export function zip(collection) {
	return times(collection[0].length, n => {
		return map(collection, element => {
			return element[n];
		});
	});
}

function zipTest() {
	assertEqual(zip([[1]]), [[1]]);
	assertEqual(zip([[1], [2]]), [[1, 2]]);
	assertEqual(zip([[1, 3, 5, 7], [2, 4, 6, 8]]), [[1, 2], [3, 4], [5, 6], [7, 8]]);
}

export function unzip(collection) {
	return zip(collection);
}

function unzipTest() {
	// assertEqual(unzip([[]]), []); // TODO what to expect here?
	assertEqual(unzip([[1]]), [[1]]);
	assertEqual(unzip([[1, 2]]), [[1], [2]]);
	assertEqual(unzip([[1, 2], [3, 4], [5, 6], [7, 8]]), [[1, 3, 5, 7], [2, 4, 6, 8]]);
	assertEqual(unzip([[1, 2, 3], [4, 5, 6]]), [[1, 4], [2, 5], [3, 6]]);
}

export function sum(collection, iteratee=identity) {
	return reduce(collection, (a, b) => a + iteratee(b), 0);
}

function sumTest() {
	assertEqual(sum([]), 0);

	assertEqual(sum([1]), 1);
	assertEqual(sum([1, 2]), 3);
	assertEqual(sum([1, 2, 3]), 6);

	assertEqual(sum({a: 1, b: 2}), 3);
}

export function product(collection, iteratee=identity) {
	return reduce(collection, (a, b) => a * iteratee(b), 1);
}

function productTest() {
	assertEqual(product([]), 1);

	assertEqual(product([1]), 1);
	assertEqual(product([1, 2]), 2);
	assertEqual(product([1, 2, 3]), 6);

	assertEqual(product({a: 1, b: 2}), 2);
}

export function concat(...arrays) {
	let result = [];
	each(arrays, array => {
		result = result.concat(array);
	});
	return result;
}

function concatTest() {
	assertEqual(concat([1, 2], [3], [4, 5, 6]), [1, 2, 3, 4, 5, 6]);
	// TODO by design?
	assertEqual(concat([1, 2], 3), [1, 2, 3]);
	assertEqual(concat([1, 2], 3, 4, 5), [1, 2, 3, 4, 5]);
}

export function sort(collection, iteratee=identity) {
	function callback(a, b) {
		// TODO for number
		//iteratee(a) - iteratee(b)
		return iteratee(a) < iteratee(b) ? -1 : 1;
	}
	if (isArray(collection)) {
		const result = collection.concat().sort(callback);
		Object.freeze(result);
		return result;
	} else if (typeof(collection) === 'object') {
		const result = {};
		Object.keys(collection).sort().forEach(function(key) {
			result[key] = collection[key];
		});
		Object.freeze(result);
		return result;
	} else {
		console.log(collection);
		errorNotImplemented();
	}
}

function sortTest() {
	assertEqual(sort([]), []);
	assertEqual(sort([1]), [1]);
	assertEqual(sort([1, 2, 3]), [1, 2, 3]);
	assertEqual(sort([2, 1, 3]), [1, 2, 3]);
	assertEqual(sort([1.2, 1.1, 1.3]), [1.1, 1.2, 1.3]);
	assertEqual(sort(['c', 'b', 'a']), ['a', 'b', 'c']);
}

function sortBy(array, ...criterias) {
	const result = array.slice(0);
	result.sort((a, b) => {
		for (let i = 0; i < criterias.length; i += 1) {
			const ca = criterias[i](a);
			const cb = criterias[i](b);
			if ((i < criterias.length) && (ca === cb)) {
				continue;
			}
			return ca > cb ? 1 : -1; // > = ascending, < = descending
		}
	});
	return result;
}

function sortByTest() {
	const data = [
		['a', 'y', 1, 'm'],
		['b', 'x', 3, 'n'],
		['b', 'x', 3, 'm'],
		['a', 'x', 1, 'm'],
		['b', 'x', 2, 'm'],
	];
	const expected = [
		['a', 'x', 1, 'm'],
		['a', 'y', 1, 'm'],
		['b', 'x', 2, 'm'],
		['b', 'x', 3, 'm'],
		['b', 'x', 3, 'n'],
	];
	const result = sortBy(data, i => i[0], i => i[1], i => i[2], i => i[3]);
	assertEqual(result, expected);
}

function groupBy(array, iteratee) {
	return array.reduce((acc, item) => {
		const key = iteratee(item);
		if (!(key in acc)) {
			acc[key] = [];
		}
		acc[key].push(item);
		return acc;
	}, {});
}

function groupByTest() {
	const data = [
		[1, 'a'],
		[2, 'd'],
		[2, 'c'],
		[3, 'e'],
		[1, 'b'],
	];
	const expected = {
		1: [[1, 'a'], [1, 'b']],
		2: [[2, 'd'], [2, 'c']],
		3: [[3, 'e']],
	};
	const result = groupBy(data, i => i[0]);
	assertEqual(result, expected);
}

export function every(collection, predicate=identity) {
	return reduce(map(collection, predicate), (a, b) => ((a === true) && (b === true)), true);
}

function everyTest() {
	assertTrue(every([]));
	assertTrue(every([true]));
	assertTrue(every([true, true]));
	assertFalse(every([true, false]));
	assertFalse(every([false]));
	assertFalse(every([false, false]));
}

export function some(collection, predicate=identity) {
	return reduce(map(collection, predicate), (a, b) => ((a === true) || (b === true)), false);
}

function someTest() {
	assertFalse(some([]));
	assertTrue(some([true]));
	assertTrue(some([true, true]));
	assertTrue(some([true, false]));
	assertFalse(some([false]));
	assertFalse(some([false, false]));
}

export function find(collection, predicate=identity) {
	//return filter(collection, predicate)[0];
	return each(collection, value => predicate(value) ? value : undefined);
}

function findTest() {
	assertEqual(find([[1, 2], [3, 4], [5, 6]], x => x[0] === 3), [3, 4]);
}

export function flatten(collection) {
	return reduce(collection, (accumulator, value) => accumulator.concat(value), []);
}

function flattenTest() {
	assertEqual(flatten([1, [2, 3], 4]), [1, 2, 3, 4]);
}

export function format(string, ...args) {
	return string.replace(/{(\d+)}/g, (match, number) => {
		const argnumber = parseInt(number);
		// TODO use ??
		return typeof(args[argnumber]) !== 'undefined' ? args[argnumber] : '?';
	});
}

function formatTest() {
	assertEqual(format('{0}={1}', 'A', 'B'), 'A=B');
	assertEqual(format('{1}={}={0}', 'A', 'B'), 'B={}=A');
	assertEqual(format('xyz', 'A', 'B'), 'xyz');
}

export function formatPretty(value, indent=0) {
	function hasSubArray(arg) {
		if (isArray(arg)) {
			return some(arg, value => isArray(value));
		}
		return false;
	}

	const indentString = '  '.repeat(indent);
	let text = '';
	if (!hasSubArray(value)) {
		text = indentString + JSON.stringify(value);
	} else {
		text += indentString + '[\n';
		for (let i = 0; i < value.length; i++) {
			text += this.formatPretty(value[i], indent + 1);
			if (i < (value.length - 1)) {
				text += ',';
			}
			text += '\n';
		}
		text += indentString + ']';
	}
	return text;
}

function formatPrettyTest() {
}

// TODO strip wich also works for arrays and other stuff
export function strip(s, characterSet) {
	// https://stackoverflow.com/estions/3561493/is-there-a-regexp-escape-function-in-javascript
	function escape(s) {
		return s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
	}
	characterSet = escape(characterSet);
	const re = new RegExp('^[' + characterSet + ']+|[' + characterSet + ']+$', 'g');
	return s.replace(re, '');
}

//const _ = {};

function stripTest() {
	assertEqual(strip('', ''), '');
	assertEqual(strip('X X', ' '), 'X X');
	assertEqual(strip('  X X  ', ' '), 'X X');
	assertEqual(strip('-', '-'), '');
	assertEqual(strip('-', 'a-b'), '');
	assertEqual(strip('-ab', 'a-b'), '');
}

export function partial(callable, ...fixedArgs) {
	return function(...variableArgs) {
		const args = fixedArgs.concat(variableArgs);
		return callable.apply(null, args);
	};
}

function partialTest() {
	const add = (a, b) => a + b;
	const add3 = partial(add, 3);
	assertEqual(add3(5), 8);

	// TODO allow _
	//const div = (a, b) => a / b;
	// assertEqual(partial(div, _, 2))(4))
}

export function curry(callable) {
	function helper(callable, argsPrevious) {
		return function(...args) {
			const argsAll = [].concat(argsPrevious, args);
			return argsAll.length === callable.length ? callable(...argsAll) : helper(callable, argsAll);
		};
	}
	return helper(callable, []);
}

function curryTest() {
	const add = (a, b, c, d) => a + b + c + d;
	const addCurried = curry(add);

	assertEqual(addCurried(1)(2)(3)(4), 10);
	assertEqual(addCurried(1)(2)(3, 4), 10);
	assertEqual(addCurried(1)(2, 3, 4), 10);
	assertEqual(addCurried(1, 2)(3)(4), 10);
	assertEqual(addCurried(1, 2)(3, 4), 10);
	assertEqual(addCurried(1, 2, 3)(4), 10);
	assertEqual(addCurried(1, 2, 3, 4), 10);

	const f1 = addCurried(1);
	assertEqual(f1(2, 3, 4), 10);
	assertEqual(f1(2, 3)(4), 10);
	const f12 = addCurried(1, 2);
	assertEqual(f12(3, 4), 10);
	assertEqual(f12(3)(4), 10);
	const f123 = addCurried(1, 2, 3);
	assertEqual(f123(4), 10);
	assertEqual(f123()()()(4), 10);

	const addMul = (a, b, c) => a + (b * c);
	assertEqual(addMul(1, 2, 3), 7);
	assertEqual(curry(addMul)(1)(2)(3), 7);

	// TODO allow _
	//assertEqual(addCurried(_)(1, 2, 3, 4), 10);
	//assertEqual(addCurried(1, 2)(_)(3, 4), 10);
}

// pipe(f, g, h)(x) -> h(g(f(x)))
export function pipe(callableFirst, ...callablesRest) {
	return function(...args) {
		const accFirst = callableFirst.apply(null, args);
		return reduce(callablesRest, (value, callable) => callable(value), accFirst);
	};
}

function pipeTest() {
	const mul2 = x => x * 2;
	const div2 = x => x / 2;
	const inc = x => x + 1;
	const dec = x => x - 1;

	assertEqual(div2(dec(inc(mul2(3)))), 3);

	assertEqual(pipe(mul2, inc, dec, div2)(4), 4);
	assertEqual(pipe((x, y, z) => x + y + z, mul2)(2, 3, 4), 18);
}

// window.location.search
export function parseURL(value) {
	const decode = value => decodeURIComponent(value);
	const search = new RegExp('([^=]+)=([^&]*)&?', 'g');
	let match;
	let result = {};
	// TODO regexes/search to pairs/list/pairsToObject
	// skip '?' at the beginning
	while ((match = search.exec(value.substring(1)))) {
		result[decode(match[1])] = decode(match[2]);
	}
	return result;
}

function parseURLTest() {
	assertEqual(parseURL('?a=b&c=d'), {'a': 'b', 'c': 'd'});
	assertEqual(parseURL('?'), {});
	assertEqual(parseURL('?a=b&a=c'), {'a': 'c'});
}

export function tryCatch(callable, def, debug=true) {
	try {
		return callable();
	} catch (e) {
		if (debug) {
			console.error(e);
		}
	}
	return def;
}

function tryCatchTest() {
	function throwTest() {
		throw 'Test';
	}
	assertEqual(tryCatch(throwTest, {}, false), {});
}

export function timeIt(iteratee, iterations, log=true) {
	function now() {
		// return performance.now();
		return Date.now();
	}

	const t1 = now();
	//times(iterations, iteratee);
	for (let i = 0; i < iterations; i += 1) {
		iteratee();
	}
	const t2 = now();
	const deltaMilliseconds = Math.floor(t2 - t1); // depending on the JS environment may be a fraction or not
	if (log) {
		console.log(deltaMilliseconds);
	}
	return deltaMilliseconds;
}

function timeItTest() {
	const n = 100;
	const log = false;
	timeIt(() => union([[1, 2, 3], [2, 3, 4]]), n, log);
	timeIt(() => unionSlow([1, 2, 3], [2, 3, 4]), n, log);
	timeIt(() => union([[1, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9]]), n, log);
	timeIt(() => unionSlow([1, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9]), n, log);
}

export function range(startInclusive, endExclusive, step=1) {
	let result = [];
	for (let i = startInclusive; i < endExclusive; i += step) {
		result.push(i);
	}
	return result;
}

function rangeTest() {
	assertEqual(range(1, 1), []);
	assertEqual(range(1, 2), [1]);
	assertEqual(range(1, 5), [1, 2, 3, 4]);
	assertEqual(range(1, 10, 2), [1, 3, 5, 7, 9]);
}

export function histogram(collection) {
	if (!isArray(collection)) {
		errorNotImplemented();
	}

	// TODO functional way? reduce?
	let histogram = {};
	each(collection, item => {
		histogram[item] = item in histogram ? histogram[item] + 1 : 1;
	});
	return histogram;
}

function histogramTest() {
	assertEqual(histogram([]), {});
	assertEqual(histogram([1]), {1: 1});
	assertEqual(histogram([3, 3, 2, 1, 2, 3]), {1: 1, 2: 2, 3: 3});
}

/*
object -> object
array -> array
int -> array
string -> array
*/
export function toIterable(value) {
	if (isArray(value)) {
		return value;
	} else if (typeof(value) === 'string') {
		return [...value];
	} else if (typeof(value) === 'object') {
		return value;
	} else {
		errorNotImplemented();
	}
}

function toIterableTest() {
	assertEqual(toIterable([]), []);
	assertEqual(toIterable([1, 2]), [1, 2]);
	assertEqual(toIterable({}), {});
	assertEqual(toIterable({a: 1, b: 2}), {a: 1, b: 2});
	assertEqual(toIterable(''), []);
	assertEqual(toIterable('xyz'), ['x', 'y', 'z']);
	//assertEqual(toIterable(0), [0]);
	//assertEqual(toIterable(0xf), [1, 1, 1, 1]);
}

export function keys(collection) {
	if (isArray(collection)) {
		return range(0, length(collection));
	} else if (typeof(collection) === 'object') {
		return Object.keys(collection);
	} else {
		errorNotImplemented();
	}
}

function keysTest() {
	assertEqual(keys([]), []);
	assertEqual(keys([4, 5, 6]), [0, 1, 2]);
	assertEqual(keys({}), []);
	assertEqual(keys({a: 1, b: 2}), ['a', 'b']);
}

export function length(value) {
	if (isArray(value)) {
		return value.length;
	} else if (typeof(value) === 'string') {
		return value.length;
	} else if (typeof(value) === 'object') {
		return keys(value).length;
	} else {
		errorNotImplemented();
	}
}

function lengthTest() {
	assertEqual(length([]), 0);
	assertEqual(length([1, 2, 3]), 3);
	assertEqual(length({}), 0);
	assertEqual(length({a: 0, b: 0}), 2);
	assertEqual(length(''), 0);
	assertEqual(length('xyz'), 3);
}

export function array(...values) {
	const result = values;
	Object.freeze(result);
	return result;
}

function arrayTest() {
	assertEqual(array(), []);
	assertEqual(array(1), [1]);
	assertEqual(array(1, 2, 3), [1, 2, 3]);
	assertEqual(array('a', 'b'), ['a', 'b']);
}

export function dict() {
	const result = {};
	Object.freeze(result);
	return result;
}

function dictTest() {
	assertEqual(dict(), {});
}

export function string() {
	const result = '';
	Object.freeze(result);
	return result;
}

function stringTest() {
	assertEqual(string(), '');
}

export function clamp(value, min, max) {
	if (value < min) {
		return min;
	} else if (value > max) {
		return max;
	} else {
		return value;
	}
}

function clampTest() {
	assertEqual(clamp(2.0, 1.0, 3.0), 2.0);
	assertEqual(clamp(0.0, 1.0, 3.0), 1.0);
	assertEqual(clamp(4.0, 1.0, 3.0), 3.0);
}

// TODO move to random, but verify usage

// uses the internal Math.random which renders this to an unpure function
export function randomInt(startInclusive, endExclusive) {
	const range = endExclusive - startInclusive;
	return Math.floor(Math.random() * range) + startInclusive;
}

function randomIntTest() {
	each(range(0, 1000), () => {
		const result = randomInt(11, 14);
		assertTrue((result >= 11) && (result <= 13));
	});
}

// https://www.alanzucconi.com/2015/09/16/how-to-sample-from-a-gaussian-distribution/
// TODO random function should be a parameter, but even if random is provided as param how to hide the state?
// TODO have something which is more like a dice distribution with a better defined minimum/maximum
export function randomGauss(mu, sigma) {
	let x;
	let y;
	let r;

	do {
		x = (2.0 * Math.random()) - 1.0;
		y = (2.0 * Math.random()) - 1.0;
		r = (x * x) + (y * y);
	} while ((r >= 1.0) || (r === 0.0));

	r = Math.sqrt((-2.0 * Math.log(r)) / r);

	const random = x * r;

	return mu + (random * sigma);
}

function randomGaussTest() {
	const n = 100;
	const mu = 5;
	const randomValues = times(n, () => randomGauss(mu, 0.1)); // ca. 68% are within one sigma
	const hist = histogram(map(randomValues, x => Math.round(x)));
	assertTrue(hist[mu] > (n * 0.9)); // there is a chance this may fail
}

// TODO this is also not a proper function like randInt, may be should also be in random
export function choice(collection, probabilities=undefined, random=randomInt) {
	if (isArray(collection)) {
		let index = random(0, length(collection)); // for uniform

		if (probabilities !== undefined) {
			const p = probabilities;
			const psum = sum(p);
			// TODO next one is very slow on large lists, idea sum + filter in reduce
			// TODO also quite expensive, maybe create a seaprate function with just this functionality
			const psummed = reduce(p, (acc, value, key) => concat(acc, (length(acc) === 0 ? 0 : acc[key - 1]) + value), []);
			assertEqual(psummed[length(psummed) - 1], psum);
			const r = random(0, psum);
			index = length(filter(psummed, x => x <= r));
		}

		return collection[index];
	} else if (typeof(collection) === 'object') {
		errorNotImplemented();
	} else {
		errorNotImplemented();
	}
}

function choiceTest() {
	assertEqual(choice([1, 2, 3], [2, 4, 6], () => 0), 1);
	each(range(0, 1000), () => {
		assertEqual(choice([1], undefined, () => 0), 1);
		assertEqual(choice([1, 1, 2, 3, 5, 8], undefined, (_start, _end) => 5), 8);
	});
	// TODO test with histogram
}

// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle

// TODO also use a non pure random function
export function shuffle(collection) {
	if (!isArray(collection)) {
		errorNotImplemented();
	}

	function swap(c, i, j) {
		const tmp = c[i];
		c[i] = c[j];
		c[j] = tmp;
	}

	const result = collection.slice();
	for (let i = 0; i < (result.length - 1); i += 1) {
		const j = randomInt(0, result.length);
		swap(result, i, j);
	}
	Object.freeze(result);
	return result;
}

function shuffleTest() {
	const n = 100;
	const input = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
	times(n, () => {
		const shuffled = shuffle(input);
		const sorted = sort(shuffled);
		assertEqual(input, sorted);
	});
}

export function pairsToObject(collection) {
	let result = {};
	each(collection, item => {
		result[item[0]] = item[1];
	});
	Object.freeze(result);
	return result;
}

function pairsToObjectTest() {
	assertEqual(pairsToObject([]), {});
	assertEqual(pairsToObject([['a', 1], ['b', 2]]), {a: 1, b: 2});
}

export function times(n, iteratee=identity) {
	return map(range(0, n), iteratee);
}

function timesTest() {
	assertEqual(times(0), []);
	assertEqual(times(2, _ => 0), [0, 0]);
	assertEqual(times(3), [0, 1, 2]);
}

export function repeat(item, n) {
	let result = [];
	for (let i = 0; i < n; i += 1) {
		result.push(item);
	}
	return result;
}

function repeatTest() {
	assertEqual(repeat(1, 3), [1, 1, 1]);
	assertEqual(repeat([1, 2], 3), [[1, 2], [1, 2], [1, 2]]);
}

// TODO the lading \n probably should not be part of this function
export function detectIndentation(text) {
	const re = new RegExp('\n( +|\t+)'); // eslint-disable-line no-control-regex
	const matches = re.exec(text);
	if (matches === null) {
		const result = {};
		Object.freeze(result);
		return result;
	}
	const character = matches[1][0];
	const count = matches[1].length;
	const result = {character: character, count: count};
	Object.freeze(result);
	return result;
}

function detectIndentationTest() {
	const x = (character, count) => ({character: character, count: count});

	assertEqual(detectIndentation('\n  abc'), x(' ', 2));
	assertEqual(detectIndentation('\n    def'), x(' ', 4));
	assertEqual(detectIndentation('\n\t\t'), x('\t', 2));

	assertEqual(detectIndentation(''), {});
	assertEqual(detectIndentation('abc'), {});
}

export function capitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function capitalizeTest() {
	assertEqual(capitalize('hello'), 'Hello');
	assertEqual(capitalize('Hello'), 'Hello');
	assertEqual(capitalize('h'), 'H');
	assertEqual(capitalize('H'), 'H');
	assertEqual(capitalize(''), '');
}

// https://en.wikipedia.org/wiki/N-gram

export function ngramCompile(examples, length) {
	if (!isArray(examples)) {
		errorNotImplemented();
	}

	function isDefined(value) {
		return value !== undefined;
	}

	return map(merge(map(examples, example => {
		return pairsToObject(map(range(0, example.length - length), i => {
			const prefix = example.substr(i, length);
			const suffix = example[i + length];
			return [prefix, suffix];
		}));
	})), items => filter(items, isDefined));
}

export function ngramGenerate(table, length) {
	const tableKeys = keys(table);
	const tableKeyLength = tableKeys[0].length;
	return reduce(times(length - tableKeyLength), acc => {
		const lookup = acc.substr(-tableKeyLength);
		const next = lookup in table ? choice(table[lookup]) : '';
		return acc + next;
	}, choice(tableKeys));
}

function ngramTest() {
	const table = ngramCompile(['abcd', 'abce', 'bce'], 2);
	assertEqual(table, {'ab': ['c', 'c'], 'bc': ['d', 'e', 'e']});
	each(times(100), () => {
		const result = ngramGenerate(table, 4);
		assertTrue((result === 'abcd') || (result === 'bcd') || (result === 'bce') || (result === 'abce'));
	});
}

export function cartesianProduct(...iterables) {
	const count = product(iterables, $ => $.length);
	const div = reduce(iterables.slice(1), (acc, item) => [acc[0] * item.length].concat(acc), [1]);

	return times(count, n => {
		return map(iterables, (element, elementIndex) => {
			const index = Math.floor(n / div[elementIndex]) % element.length;
			return element[index];
		});
	});
}

function cartesianProductTest() {
	assertEqual(cartesianProduct([1, 2], [3, 4, 5]), [[1, 3], [1, 4], [1, 5], [2, 3], [2, 4], [2, 5]]);
	assertEqual(cartesianProduct([1], [2, 3]), [[1, 2], [1, 3]]);
	assertEqual(cartesianProduct([1, 2], [3, 4], [5, 6]), [[1, 3, 5], [1, 3, 6], [1, 4, 5], [1, 4, 6], [2, 3, 5], [2, 3, 6], [2, 4, 5], [2, 4, 6]]);
}

// TODO
export function combinations() {
}

function combinationsTest() {
	assertEqual(1, 1);
}

// TODO
export function permutations() {
}

function permutationsTest() {
	assertEqual(1, 1);
}

/*
export function template() {
	NotImplemented();
}

function templateTest() {
	assertEqual(1, 1);
}
*/

export function test() {
	const tests = [
		assertTest,
		isArrayTest,
		identityTest,
		getClassTest,
		isEqualTest,
		eachTest,
		mapTest,
		reduceTest,
		filterTest,
		unionTest,
		mergeTest,
		zipTest,
		unzipTest,
		sumTest,
		productTest,
		concatTest,
		sortTest,
		sortByTest,
		groupByTest,
		everyTest,
		someTest,
		findTest,
		flattenTest,
		formatPrettyTest,
		formatTest,
		stripTest,
		partialTest,
		curryTest,
		pipeTest,
		parseURLTest,
		tryCatchTest,
		timeItTest,
		rangeTest,
		histogramTest,
		toIterableTest,
		keysTest,
		lengthTest,
		arrayTest,
		dictTest,
		stringTest,
		clampTest,
		randomIntTest,
		randomGaussTest,
		choiceTest,
		shuffleTest,
		pairsToObjectTest,
		timesTest,
		repeatTest,
		detectIndentationTest,
		capitalizeTest,
		ngramTest,
		cartesianProductTest,
		combinationsTest,
		permutationsTest,
	];

	tests.forEach(test => test());
}
