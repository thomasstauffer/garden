/*
https://en.wikipedia.org/wiki/Recursive_descent_parser
*/

import * as q from './../crow/q.js';

export function id(x) {
	return x;
}

/*
TODO only functions? pass/return state?
*/
export class Parser {
	constructor(tokens) {
		this.tokens = tokens;
		this.index = 0;
	}

	expect(tokenName) {
		if (this.index >= this.tokens.length) {
			return false;
		}
		if (this.tokens[this.index].name === tokenName) {
			return true;
		}
		return false;
	}

	take(tokenName) {
		const current = this.tokens[this.index];
		if (current.name !== tokenName) {
			console.log('Expected');
			q.errorNotImplemented(tokenName);
		}
		this.index += 1;
		return current;
	}

	// TODO rename or -> some, and -> all, there is no backtracking

	or(args, action=id) {
		q.assertType(args, Array);
		function f() {
			for (let i = 0; i < args.length; i += 1) {
				const result = args[i]();
				if (result !== undefined) {
					return action(result);
				}
			}
		}
		return f;
	}

	and(args, action=id) {
		q.assertType(args, Array);
		function f() {
			const children = [];
			for (let i = 0; i < args.length; i += 1) {
				const child = args[i]();
				if (child === undefined) {
					return;
				}
				children.push(child);
			}
			return action(children);
		}
		return f;
	}

	repeat(arg, action=id) {
		const self = this;
		function f() {
			const children = [];
			while (self.index < self.tokens.length) {
				const child = arg();
				if (child === undefined) {
					break;
				}
				children.push(child);
			}
			return action(children);
		}
		return f;
	}

	token(arg, action=id) {
		const self = this;
		function f() {
			if (!self.expect(arg)) {
				return;
			}
			return action(self.take(arg));
		}
		return f;
	}
}

export function test() {

}
