import * as q from './q.js';

export class Localization {
	constructor(locale = 'en') {
		this.defaultLocale = locale;
		this.locale = locale;
		this.phrases = {};
	}

	async _fetch() {
		//const links = document.querySelectorAll('link[type="application/l10n"]');
		//console.log(links[0].dataset.lang);
		//const url = links[0].href;
		//const response = await fetch(url);
		//const raw = await response.text();
	}

	addPhrases(phrases) {
		this.phrases = {...this.phrases, ...phrases};
	}

	setLocale(locale) {
		this.locale = locale;
	}

	getLocale() {
		return this.locale;
	}

	static async create(locale) {
		const object = new Localization(locale);
		await object._fetch();
		return object;
	}

	// http://userguide.icu-project.org/formatparse/messages
	t(message) {
		return this.phrases[this.locale][message];
	}
}

export async function test() {
	const localization = await Localization.create('en');
	localization.addPhrases({'en': {'hello': 'Hello'}, 'de': {'hello': 'Hallo'}});
	q.assertEqual(localization.t('hello'), 'Hello');
	localization.setLocale('de');
	q.assertEqual(localization.t('hello'), 'Hallo');
}
