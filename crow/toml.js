/*
https://github.com/toml-lang/toml
*/

import * as q from './q.js';
import * as scanner from './scanner.js';
import * as parser from './parser.js';

function toStatements(string) {
	const TOKENS = [
		{name: 'valuetriple', re: '^"""([\\s\\S]*?)"""', skip: false}, // 'm' multiline does not seem to work for .
		{name: 'valuesingle', re: '^"(.*?)"', skip: false},
		// TODO add name?
		{name: 'value', re: '^[a-zA-Z0-9_\\-\\.]+', skip: false},
		{name: 'comment', re: '^#[^\\n]*', skip: false},
		{name: '=', re: '^=', skip: false},
		{name: '[', re: '^\\[\\[', skip: false},
		{name: ']', re: '^\\]\\]', skip: false},
		{name: '{', re: '^\\[', skip: false},
		{name: '}', re: '^\\]', skip: false},
		{name: '_', re: '^[ \r\t\n]+', skip: true}, // eslint-disable-line no-control-regex
	];

	const tokens = scanner.scanner(TOKENS, string);
	const p = new parser.Parser(tokens);

	// TODO create symbols?
	const start = () => p.repeat(p.or([assign, comment, object, array]))();
	const assign = () => p.and([value, p.token('='), value], $ => ['=', $[0].value, $[2].value])();
	const comment = () => p.and([p.token('comment')], () => [])();
	const object = () => p.and([p.token('{'), value, p.token('}')], $ => ['{', $[1].value])();
	const array = () => p.and([p.token('['), value, p.token(']')], $ => ['[', $[1].value])();
	const value = () => p.or([p.token('valuetriple'), p.token('valuesingle'), p.token('value')])();

	//console.log(tokens);
	const statements = start();
	//console.log(statements);
	return statements;
}

export function parse(value) {
	const statements = toStatements(value);

	function isStringNumber(value) {
		return value.match(/^-?\d+(\.\d+)?$/);
	}

	const result = {};
	let currentSection = result;

	q.each(statements, item => {
		if (item[0] === '=') {
			const value = isStringNumber(item[2]) ? parseFloat(item[2]) : item[2];
			currentSection[item[1]] = value;
		} else if (item[0] === '{') {
			currentSection = {};
			result[item[1]] = currentSection;
		} else if (item[0] === '[') {
			currentSection = {};
			if (result[item[1]] === undefined) {
				result[item[1]] = [];
			}
			result[item[1]].push(currentSection);
		}
	});

	return result;
}

const TOML_TEST = `
AString = "String"
AInteger = 20
AFloat = 0.4
AMultilineString = """
String
String
String
"""

[Category]
Name1 = "Value1"
Name2 = "Value2"

[[MultiCategory]]
Name1 = "Value1"
Name2 = "Value2"

[[MultiCategory]]
Name1 = "Value1"
Name2 = "Value2"
`;

export function test() {
	function checkJustParser(string) {
		const parsed = parse(string);
		//console.log(parsed);
		q.assertType(parsed, Object);
	}

	checkJustParser(TOML_TEST);

	function checkEqual(string, expected) {
		const parsed = parse(string);
		if (!q.isEqual(parsed, expected)) {
			console.log('Parsed', parsed);
			console.log('Expected', expected);
			q.assertTrue(false);
		}
	}

	checkEqual('a=b', {a: 'b'});
	checkEqual('a="b"', {a: 'b'});
	checkEqual('a=1', {a: 1});
	checkEqual('\n\na=b\n\n', {a: 'b'});
	checkEqual(' a \t= b \t', {a: 'b'});
	checkEqual('a="""b"""', {a: 'b'});
	checkEqual('a="""b"c"""', {a: 'b"c'});
	checkEqual('a=b\nc=d', {a: 'b', c: 'd'});
	checkEqual('a=b\n# comment\nc=d', {a: 'b', c: 'd'});
	checkEqual('a=b\n[c]\nd=e', {a: 'b', c: {d: 'e'}});
	checkEqual('[[x]]\na=b\n[[x]]\nc=d', {x: [{a: 'b'}, {c: 'd'}]});
}
