#!/usr/bin/env python3

import re
import operator
import functools

def read_rec(string):
	tokens = list(filter(None, string.replace('(', ' ( ').replace(')', ' ) ').split(' ')))
	re_number = re.compile('^[0-9]+$')
	re_bool = re.compile('^true|false$')
	def parse(l, index):
		if l is None:
			if tokens[index] == '(':
				return parse([], index + 1)
			elif type(tokens[index]) == int:
				return tokens[index], index + 1
		else:
			if tokens[index] == ')':
				return l, index + 1
			else:
				list2, index2 = parse(None, index)
				return parse(l + [list2], index2)
	return parse(None, 0)
# print(read_rec('(1 (2 3 4 (5 6)) 7 8)'))

def read(string):
	tokens = list(filter(None, string.replace('(', ' ( ').replace(')', ' ) ').split(' ')))
	re_number = re.compile('^[0-9]+$')
	re_bool = re.compile('^true|false$')
	def parse(index):
		token = tokens[index]
		if token == '(':
			index += 1
			expr = []
			while tokens[index] != ')':
				parsed, index = parse(index)
				expr += [parsed]
			return expr, index
		elif re_number.match(token):
			return float(token), index + 1
		elif re_bool.match(token):
			return token == 'true', index + 1
		else:
			return token, index + 1
	return parse(0)[0]

def eval(expr):
	if type(expr) == float:
		return expr
	elif type(expr) == bool:
		return expr
	elif type(expr) == list:
		if expr[0] == '+':
			return functools.reduce(operator.add, map(eval, expr[1:]))
		elif expr[0] == '*':
			return functools.reduce(operator.mul, map(eval, expr[1:]))

print(eval(read('(+ 1 2)')))
print(eval(read('(+ 1 (+ 2 3))')))
print(eval(read('(* 2 3)')))
print(eval(read('(+ 1 (+ 3 (* 6 7)))')))
