
function isFloat(string) {
	return string.match(/[0-9]+(\.[0-9]+)?/g) !== null;
	//return !isNaN(parseFloat(string)); // TODO timeit
}

export function read(value) {
	const tokens = value.replace(/\(/g, ' ( ').replace(/\)/g, ' ) ').split(/ /g).filter(value => value !== '');

	function parse(tokens) {
		const token = tokens.shift();
		if (token === '') {
			throw 'Unexpected Empty Token';
		} else if (token === ')') {
			throw 'Unexpected Token )';
		} else if (token === '(') {
			let expression = [];
			while (tokens[0] !== ')') {
				expression.push(parse(tokens));
			}
			tokens.shift(); // )
			return expression;
		} else if ((token === 'true') || (token === 'false')) {
			return token === 'true';
		} else if (isFloat(token)) {
			return parseFloat(token);
		} else {
			return token;
		}
	}

	const expression = parse(tokens);
	return expression;
}

/*
list-minimum -> reduce
list-maximum -> reduce
list-length
list-index
list-at

number-tostring

string-join
string-left
string-right
string-replace
string-find/index
*/

export function environmentDefault() {
	const first = [{
		'pi': Math.PI,
		'sin': ([x]) => Math.sin(x),
		'cos': ([x]) => Math.cos(x),
		'max': list => Math.max.apply(null, list),
		'min': list => Math.min.apply(null, list),
		'+': list => list.reduce((sum, current) => sum + current),
		'*': list => list.reduce((product, current) => product * current),
		'-': list => list.reduce((acc, current) => acc - current),
		'/': list => list.reduce((acc, current) => acc / current),
		'=': list => list.every(current => current === list[0]),
		'<': list => list.slice(0, -1).every((current, index) => current < list[index + 1]),
		'>': list => list.slice(0, -1).every((current, index) => current > list[index + 1]),
		'<=': list => list.slice(0, -1).every((current, index) => current <= list[index + 1]),
		'>=': list => list.slice(0, -1).every((current, index) => current >= list[index + 1]),
		'apply': ([f, list]) => f(list),
		//'map': ([f, list]) => list.map(f),
		'first': list => list[0],
		'tail': list => list.slice(1),
		'length': ([list]) => list.length,
		'cons': ([value, list]) => [value].concat(list),
	}, undefined];

	// chain just for test
	return [{ 'zero': 0, 'one': 1 }, first];
}

export function evaluate(expression, environment) {
	function zipObject(keys, values) {
		if (keys.length === 0) {
			return {};
		}
		return Object.assign(...keys.map((k, i) => ({ [k]: values[i] })));
	}

	function lookup(symbol, environment) {
		const [current, next] = environment;
		if (symbol in current) {
			return current[symbol];
		} else if (next === undefined) {
			throw `'${symbol}' Not Found`;
		} else {
			return lookup(symbol, next);
		}
	}

	if ((typeof expression) === 'string') {
		return lookup(expression, environment);
	} else if ((typeof expression) === 'number') {
		return expression;
	} else if ((typeof expression) === 'boolean') {
		return expression;
	} else if (expression[0] === 'if') {
		const [_, condition, ontrue, onfalse] = expression;
		const e = evaluate(condition, environment) ? ontrue : onfalse;
		return evaluate(e, environment);
	} else if (expression[0] === 'quote') {
		const [_, subexpression] = expression;
		return subexpression;
	} else if (expression[0] === 'def') {
		const [_, name, subexpression] = expression;
		const result = evaluate(subexpression, environment);
		// TODO throw error?
		const [currentEnvironment, __] = environment;
		if (!(name in currentEnvironment)) {
			currentEnvironment[name] = result;
		}
		return result;
	} else if (expression[0] === 'log') {
		const [_, subexpression] = expression;
		const result = evaluate(subexpression, environment);
		console.log(result);
		return result;
	} else if (expression[0] === 'logenv') {
		console.log(environment);
		return 0;
	} else if (expression[0] === 'lambda') {
		const [_, parameters, body] = expression;
		return args => {
			const env = [zipObject(parameters, args), environment];
			return evaluate(body, env);
		};
	} else if (expression[0] === 'do') {
		return expression.slice(1).reduce((acc, current) => evaluate(current, environment), null);
	} else {
		const f = evaluate(expression[0], environment);
		const args = expression.slice(1).map(arg => evaluate(arg, environment));
		if ((typeof f) === 'function') {
			return f(args);
		} else {
			return f;
		}
	}
}

export function print(expression) {
	if (Array.isArray(expression)) {
		return '(' + expression.map(print).join(' ') + ')';
	} else {
		return expression.toString();
	}
}

function testTrue(condition) {
	if (condition !== true) {
		throw 'Test Failed';
	}
}

/*
function testRead(string, expected) {
	const result = read(string);
	if(result !== expected) {
		console.trace();
		throw `Test Result '${string}' -> '${result}' Expected '${expected}'`;
	}
}
*/

function testReadEval(string, expected) {
	const result = evaluate(read(string), environmentDefault());
	if (result !== expected) {
		console.trace();
		throw `Test Result '${string}' -> '${result}' Expected '${expected}'`;
	}
}

export function test() {
	// atoms
	testTrue(isFloat('1.1'));
	testTrue(isFloat('1'));
	testTrue(!isFloat('a'));
	testTrue(!isFloat(''));

	// + * - /
	testReadEval('(+ 1 2 3)', 1 + 2 + 3);
	testReadEval('(+ 1 (+ 2 3))', 1 + 2 + 3);
	testReadEval('(* 1 2 3 4)', 1 * 2 * 3 * 4);
	testReadEval('(- 6 4)', 6 - 4);
	testReadEval('(/ 24 4 3)', 24 / 4 / 3);
	testReadEval('(+ 1)', 1);

	// = < > <= >=
	testReadEval('(= 1 2)', false);
	testReadEval('(= 1 1)', true);
	testReadEval('(= 1 1 2)', false);
	testReadEval('(= 1 1 1 1)', true);
	testReadEval('(> 1 2)', false);
	testReadEval('(< 1 2)', true);
	testReadEval('(< 1 2 3 4)', true);
	testReadEval('(< 1 2 3 2)', false);
	testReadEval('(> 4 3 2 1)', true);
	testReadEval('(<= 1 1 3)', true);
	testReadEval('(>= 2 2 1)', true);

	testReadEval('(do (def a 1) (def b 2) (def c 3) (+ a b c))', 6);
	testReadEval('(do (def a 1) (def a 2) (+ a a))', 2); // cannot overwrite

	testReadEval('(if (= 1 1) 3 -3)', 3);
	testReadEval('(if (= 1 2) 3 -3)', -3);

	// white space
	testReadEval('(quote  1)', 1);
	testReadEval(' ( quote  1 ) ', 1);

	testReadEval('(length (quote (1 2 3)))', 3);
	testReadEval('(length (quote (+ 1 2 3)))', 4);
	testReadEval('(apply + (quote (1 2 3)))', 1 + 2 + 3);

	// lambda
	testReadEval('(do (def f (lambda (a) a)) (f 8))', 8);
	testReadEval('((lambda (a b c) (+ a b c)) 1 2 3)', 1 + 2 + 3);
	testReadEval('((lambda (a) (+ a a a)) 2)', 2 + 2 + 2);

	// lambda & zero parameters
	testReadEval('(do (def f (lambda () 3)) (f))', 3);
	testReadEval('((lambda () 11))', 11);

	// scroping
	testReadEval('(do (def x 1) (def f (lambda () (do (def x 2) x))) (f))', 2);
	testReadEval('(do (def x 8) (def f (lambda () x)) (do (def x 16) (f)))', 8); // not really lexical scoping (def does not overwrite)
	testReadEval('(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) (f)))))', 8); // lexical scoping
	testReadEval('(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) x))))', 16);

	// more complex functions
	testReadEval('(do (def add (lambda (a) (+ a a))) (add 4))', 4 + 4);
	testReadEval('(do (def range (lambda (a b) (if (= a b) (quote ()) (cons a (range (+ a 1) b))))) (apply + (range 0 3)))', 0 + 1 + 2);

	// recursion
	testReadEval('(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 5))', 5 * 4 * 3 * 2 * 1);

	// high order function
	// TODO for any amount of parameters? or even currying?
	testReadEval('(do (def add (lambda (x) (+ x 2))) (def mul (lambda (x) (* x 2))) (def compose (lambda (f g) (lambda (x) (f (g x))))) ((compose add mul) 3))', (3 * 2) + 2);

	/*
	assertResult(('(quote "Hello")'), 'Hello');
	assertResult(('(quote 1)'), 1);
	assertResult(('(quote 1.23)'), 1.23);
	assertResult(('(quote (1 2 3))'), [1, 2, 3]);
	assertResult(('(quote one)'), {A: 'one'});
	assertResult(('(quote (one))'), [{A: 'one'}]);
	assertResult(('(eval (quote (one)))'), 1);
	assertResult(('(one)'), 1);
	assertResult(('(length 1 2 3)'), 3);
	assertResult(('(list 1 2 3 4)'), [1, 2, 3, 4]);
	//assertResult(('(1)'), 1);
	//assertResult(('("1")'), '1');
	//assertResult(('(id (length (\' 1 2 3)))');
	//assertResult(('(id (+ 1 2 3 4))');
	*/
}
