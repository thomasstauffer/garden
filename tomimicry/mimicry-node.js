/*global process*/
'use strict';

import * as mimicry from './mimicry.js';

const command = process.argv.length > 2 ? process.argv[2] : '';
const parameter = process.argv.length > 3 ? process.argv[3] : '';

if ((command === 'status') && (parameter === 'who')) {
	console.log('Mimicry JavaScript');
} else if (command === 'run') {
	const env = mimicry.environmentDefault();
	console.log(mimicry.print(mimicry.evaluate(mimicry.read(parameter), env)));
} else if (command === 'selftest') {
	mimicry.test();
} else if (command === 'measure') {
	const env = mimicry.environmentDefault();
	const exprRead = mimicry.read(parameter);
	let sum = 0.0;
	const start = Date.now();
	for (let i = 0; i < 1000; i += 1) {
		sum += mimicry.evaluate(exprRead, env);
	}
	const stop = Date.now();
	const elapsedMs = Math.ceil(stop - start);
	console.log(`(${elapsedMs} ${sum})`);
} else {
	throw 'Unknown Command ' + command;
}
