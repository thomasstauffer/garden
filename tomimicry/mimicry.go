package main

import "fmt"
import "os"
import "strconv"
import "math"
import "time"
import "strings"
import "regexp"

type table map[string]function

type env struct {
	parent *env
	table  table
}

type lambda struct {
	args list
	body *expr
	env  *env
}

type function func(list) expr

const (
	Symbol = iota
	Number
	Bool
	List
	Lambda
)

// TODO this should be an union/sum type, the internet says that interface{} is the solution? at the moment this is just a bad lazy solution
// https://tour.golang.org/methods/16
type expr struct {
	kind       int
	symbolItem string
	numberItem float64
	boolItem   bool
	listItem   list
	lambdaItem lambda
}

type list []expr

func exprFromSymbol(arg string) expr {
	return expr{kind: Symbol, symbolItem: arg}
}

func exprFromNumber(arg float64) expr {
	return expr{kind: Number, numberItem: arg}
}

func exprFromBool(arg bool) expr {
	return expr{kind: Bool, boolItem: arg}
}

func exprFromList(arg list) expr {
	return expr{kind: List, listItem: arg}
}

func exprFromLambda(arg list, body *expr, env *env) expr {
	return expr{kind: Lambda, lambdaItem: lambda{args: arg, body: body, env: env}}
}

func exprToSymbol(arg expr) string {
	if exprKind(arg) != Symbol {
		panic("not a symbol")
	}
	return arg.symbolItem
}

func exprToNumber(arg expr) float64 {
	if exprKind(arg) != Number {
		panic("not a number")
	}
	return arg.numberItem
}

func exprToBool(arg expr) bool {
	if exprKind(arg) != Bool {
		panic("not a bool")
	}
	return arg.boolItem
}

func exprToList(arg expr) list {
	if exprKind(arg) != List {
		panic("not a list")
	}
	return arg.listItem
}

func exprToLambda(arg expr) lambda {
	if exprKind(arg) != Lambda {
		panic("not a lamda")
	}
	return arg.lambdaItem
}

func exprKind(arg expr) int {
	return arg.kind
}

// print

func printList(list list) string {
	var s []string
	for _, expr := range list {
		s = append(s, print(expr))
	}
	return "(" + strings.Join(s, " ") + ")"
}

func print(expr expr) string {
	switch exprKind(expr) {
	case Symbol:
		return expr.symbolItem
	case Number:
		return strconv.FormatFloat(expr.numberItem, 'f', -1, 64)
	case Bool:
		return strconv.FormatBool(expr.boolItem)
	case List:
		return printList(expr.listItem)
	}
	panic("unknown kind")
}

// env

func binOp(list list, op func(a float64, b float64) float64) expr {
	result := exprToNumber(list[0])
	for _, item := range list[1:] {
		result = op(result, exprToNumber(item))
	}
	return exprFromNumber(result)
}

func envDefault() env {
	e := env{parent: nil, table: make(table)}
	envSet("pi", func(args list) expr { return exprFromNumber(math.Pi) }, e)
	envSet("sin", func(args list) expr { return exprFromNumber(math.Sin(exprToNumber(args[0]))) }, e)
	envSet("cos", func(args list) expr { return exprFromNumber(math.Cos(exprToNumber(args[0]))) }, e)
	envSet("+", func(args list) expr { return binOp(args, func(a float64, b float64) float64 { return a + b }) }, e)
	envSet("-", func(args list) expr { return binOp(args, func(a float64, b float64) float64 { return a - b }) }, e)
	envSet("*", func(args list) expr { return binOp(args, func(a float64, b float64) float64 { return a * b }) }, e)
	envSet("/", func(args list) expr { return binOp(args, func(a float64, b float64) float64 { return a / b }) }, e)
	envSet("<=", func(args list) expr { return exprFromBool(exprToNumber(args[0]) <= exprToNumber(args[1])) }, e)
	envSet("length", func(args list) expr { return exprFromNumber(float64(len(exprToList(args[0])))) }, e)

	eChild := env{parent: &e, table: make(table)}
	envSet("zero", func(args list) expr { return exprFromNumber(0) }, eChild)
	envSet("one", func(args list) expr { return exprFromNumber(1) }, eChild)
	return eChild
}

func envSet(symbol string, f function, env env) {
	if _, ok := env.table[symbol]; ok {
		panic("symbol already set")
	} else {
		env.table[symbol] = f
	}
}

// TODO return callable? like makeCallable -> envCallable
func envCall(symbol string, args list, env env) expr {
	if expr, ok := env.table[symbol]; ok {
		return expr(args)
	} else if env.parent != nil {
		return envCall(symbol, args, *env.parent)
	}
	panic("symbol not found")
}

// read

func readToken(tokens []string, index *int) expr {
	reNumber := regexp.MustCompile("^[0-9]+(\\.[0-9]+)?$")
	reBool := regexp.MustCompile("^(true|false)$")
	token := tokens[*index]
	*index += 1
	if token == "(" {
		var l []expr
		for tokens[*index] != ")" {
			l = append(l, readToken(tokens, index))
		}
		*index += 1
		return exprFromList(l)
	} else if reNumber.MatchString(token) {
		value, _ := strconv.ParseFloat(token, 64)
		return exprFromNumber(value)
	} else if reBool.MatchString(token) {
		value, _ := strconv.ParseBool(token)
		return exprFromBool(value)
	} else {
		return exprFromSymbol(token)
	}
	panic("invalid token")
}

func read(s string) expr {
	tokensWithEmpty := strings.Split(strings.ReplaceAll(strings.ReplaceAll(s, "(", " ( "), ")", " ) "), " ")
	var tokens []string
	for _, token := range tokensWithEmpty {
		if token != "" {
			tokens = append(tokens, token)
		}
	}
	index := 0
	return readToken(tokens, &index)
}

// eval

func makeCallableLambda(lambda lambda) function {
	return func(args list) expr {
		envNew := env{parent: lambda.env, table: make(table)}
		if len(lambda.args) != len(args) {
			panic("argument number mismatch")
		}
		for i := 0; i < len(args); i += 1 {
			name := lambda.args[i]
			value := args[i]
			envSet(exprToSymbol(name), makeCallable(value), envNew)
		}
		return eval(*lambda.body, envNew)
	}
}

func makeCallable(e expr) function {
	if exprKind(e) == Lambda {
		return makeCallableLambda(exprToLambda(e))
	} else {
		return func(args list) expr {
			return e
		}
	}
}

func evalArgs(list list, env env) list {
	var l []expr
	for _, expr := range list {
		l = append(l, eval(expr, env))
	}
	return l
}

func evalList(list list, env env) expr {
	head := list[0]
	if exprKind(head) == Symbol {
		switch exprToSymbol(head) {
		case "quote":
			return list[1]
		case "lambda":
			return exprFromLambda(exprToList(list[1]), &list[2], &env)
		case "if":
			if exprToBool(eval(list[1], env)) {
				return eval(list[2], env)
			} else {
				return eval(list[3], env)
			}
		case "def":
			name := exprToSymbol(list[1])
			value := eval(list[2], env)
			envSet(name, makeCallable(value), env)
			return value
		case "do":
			args := evalArgs(list[1:], env)
			return args[len(args)-1]
		default:
			args := evalArgs(list[1:], env)
			return envCall(exprToSymbol(head), args, env)
		}
	} else if exprKind(head) == List {
		f := eval(head, env)
		args := evalArgs(list[1:], env)
		return makeCallable(f)(args)
	}
	panic("unknown kind")
}

func eval(expr expr, env env) expr {
	switch exprKind(expr) {
	case Symbol:
		return envCall(exprToSymbol(expr), nil, env)
	case Number:
		return expr
	case Bool:
		return expr
	case List:
		return evalList(exprToList(expr), env)
	}
	panic("unknown kind")
}

// main

func main() {
	if len(os.Args) != 3 {
		panic("invalid number of arguments")
	}

	if (os.Args[1] == "status") && (os.Args[2] == "who") {
		fmt.Println("Mimicry Go")
		return
	} else if (os.Args[1] == "status") && (os.Args[2] == "selftest") {
		env := envDefault()
		envSet("xyz", func(list) expr { return exprFromNumber(0) }, env)
		e := exprFromList([]expr{exprFromBool(false), exprFromNumber(1), exprFromSymbol("hello")})
		fmt.Println(print(e))
		fmt.Println(print(exprFromBool(false)))
		fmt.Println(print(read("(1 2 3)")))
		fmt.Println(print(read("(1 2 3 hallo false true)")))
		fmt.Println(print(read("(+ 2 (+ 3 4 (x 4 3)))")))
		fmt.Println(print(eval(read("1"), env)))
		fmt.Println(print(eval(read("true"), env)))
		fmt.Println(print(eval(read("one"), env)))
		fmt.Println(print(eval(read("(+ 2 3)"), env)))
		fmt.Println(print(eval(read("(* 1 2 3)"), env)))
		fmt.Println(print(eval(read("(+ 0 (if false 1 2))"), env)))
		fmt.Println(print(eval(read("(def x 2)"), env)))
		fmt.Println(print(eval(read("(do (def y 4) (+ y y y))"), env)))
		fmt.Println(print(eval(read("((lambda (a b) (+ a b)) 1 2)"), env)))
	} else if os.Args[1] == "run" {
		env := envDefault()
		fmt.Println(print(eval(read(os.Args[2]), env)))
	} else if os.Args[1] == "measure" {
		exprRead := read(os.Args[2])
		sum := 0.0
		start := time.Now()
		for i := 0; i < 1000; i += 1 {
			env := envDefault()
			exprEval := eval(exprRead, env)
			sum += exprToNumber(exprEval)
		}
		elapsed := time.Since(start)
		ms := elapsed.Nanoseconds() / 1000000
		fmt.Printf("(%d %g)", ms, sum)
	} else {
		panic("unknown command")
	}
}
