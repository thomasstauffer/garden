# https://www.tutorialspoint.com/execute_julia_online.php

# types

struct Env
	parent::Int # TODO Env
	table::Dict{String,String} # TODO function pointer
end

struct Lambda
	body::String
	args::String
	env::Env
end

abstract type Expr end

const List = Array{Expr,1}

struct ExprSymbol <: Expr value::String end
struct ExprNumber <: Expr value::Float64 end
struct ExprBool <: Expr value::Bool end
struct ExprList <: Expr value::List end
struct ExprLambda <: Expr value::Lambda end

# https://discourse.julialang.org/t/recursive-type-union/2972/6
# multi dispatch on it? as it seems a featuer of Julia?

# print

print(expr::ExprSymbol) = expr.value
print(expr::ExprNumber) = string(expr.value)
print(expr::ExprBool) = string(expr.value)
print(expr::ExprList) = string("(", join(map(item -> print(item) , expr.value), " "), ")")
print(expr::ExprLambda) = string("(lambda ", print(expr.value.args), print(expr.value.body), ")")

#println(print(ExprSymbol("a-symbol")))
#println(print(ExprNumber(0)))
#println(print(ExprList([ExprNumber(1), ExprSymbol("oo"), ExprNumber(2)])))

# read

function read(s::String)
    tokens = filter(item -> length(item) > 0, split(replace(replace(s, "(" => " ( "), ")" => " ) "), " "))
    i = 1
    function readToken()
      token = tokens[i]
      matchNumber = match(r"[0-9]+", token)
      matchBool = match(r"true|false", token)
      i += 1
      if token == "("
        l = List()
        while tokens[i] != ")"
          push!(l, readToken())
        end
        i += 1
        return ExprList(l)
      elseif matchNumber != nothing
        return ExprNumber(parse(Float64, matchNumber.match))
      elseif matchBool != nothing
        return ExprBool(parse(Bool, matchBool.match))
      else
        return ExprSymbol(token)
      end
    end
    return readToken()
end

println(print(read("(1 symbol 3)")))

# env

envDefault() = Env(0, Dict())

env = envDefault()
println(env.parent)

# eval

# test

#println(versioninfo())
#println(now())