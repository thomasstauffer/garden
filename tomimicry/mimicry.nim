
# nim.exe compile --cc:vcc --run mimicry.nim

import math
import strutils
import sequtils
import tables
import re
import os
import times

#echo "Mimicry Nim"

# Types

type
  ExprKind {.pure.} = enum ExprNumber, ExprSymbol, ExprBool, ExprList, ExprLambda
  List = seq[Expr]
  Lambda = ref object
    args: List
    body: Expr
    env: Env
  Expr = ref object
    case kind: ExprKind
      of ExprSymbol: valueSymbol: string
      of ExprNumber: valueNumber: float64
      of ExprBool: valueBool: bool
      of ExprList: valueList: List
      of ExprLambda: valueLambda: Lambda
  Function = (proc(list: List): Expr)
  Env = ref object
    parent: Env
    table: Table[string, Function]

proc makeSymbol(value: string): Expr = Expr(kind: ExprSymbol, valueSymbol: value)
proc makeNumber(value: float64): Expr = Expr(kind: ExprNumber, valueNumber: value)
proc makeBool(value: bool): Expr = Expr(kind: ExprBool, valueBool: value)
proc makeList(value: List): Expr = Expr(kind: ExprList, valueList: value)

proc makeLambda(args: List, body: Expr, env: Env): Expr = Expr(kind: ExprLambda, valueLambda: Lambda(args: args, body: body, env: env))

proc getSymbol(expr: Expr): string = expr.valueSymbol
proc getNumber(expr: Expr): float64 = expr.valueNumber
proc getBool(expr: Expr): bool = expr.valueBool
proc getList(expr: Expr): List = expr.valueList

# Print

proc print(expr: Expr): string =
  case expr.kind
    of ExprSymbol: $(expr.valueSymbol)
    of ExprNumber: $(expr.valueNumber)
    of ExprBool: $(expr.valueBool)
    of ExprList: "(" & expr.valueList.mapIt(print(it)).join(" ") & ")"
    of ExprLambda: "(lambda " & print(makeList(expr.valueLambda.args)) & " " & print(expr.valueLambda.body) & ")"

# Read

proc read(s: string): Expr =
  let tokens = s.replace("(", " ( ").replace(")", " ) ").split(" ").filterIt(it != "").toSeq()
  var i = 0
  proc readToken(): Expr =
    let token = tokens[i]
    i += 1
    if token == "(":
      var l : List = @[]
      while tokens[i] != ")":
        l.add(readToken())
      i += 1
      return makeList(l)
    elif token.match(re"[0-9]+"):
      return makeNumber(token.parseFloat())
    elif token.match(re"true|false"):
      return makeBool(token.parseBool())
    return makeSymbol(token)
  readToken();

# Env

proc envDefault(): Env =
  let env = Env(parent: nil)
  env.table["pi"] = proc(args: List): Expr = makeNumber(3.14159265359)
  #echo 3.1415.sin()
  env.table["sin"] = proc(args: List): Expr = makeNumber(getNumber(args[0]).sin)
  env.table["cos"] = proc(args: List): Expr = makeNumber(getNumber(args[0]).cos)
  env.table["+"] = proc(args: List): Expr = makeNumber(args.mapIt(getNumber(it)).foldl(a + b))
  env.table["-"] = proc(args: List): Expr = makeNumber(args.mapIt(getNumber(it)).foldl(a - b))
  env.table["*"] = proc(args: List): Expr = makeNumber(args.mapIt(getNumber(it)).foldl(a * b))
  env.table["/"] = proc(args: List): Expr = makeNumber(args.mapIt(getNumber(it)).foldl(a / b))
  env.table["<="] = proc(args: List): Expr = makeBool(getNumber(args[0]) <= getNumber(args[1])) # TODO arbitrary amount all?, any?
  env.table["length"] = proc(args: List): Expr = makeNumber(float64(args[0].valueList.len))
  env.table["zero"] = proc(args: List): Expr = makeNumber(0.0)
  env.table["one"] = proc(args: List): Expr = makeNumber(1.0)
  return env

proc envSet(symbol: string, f: Function, env: Env) =
  if symbol in env.table:
    assert false
  env.table[symbol] = f

proc envCall(symbol: string, args: List, env: Env): Expr =
  if symbol in env.table:
    env.table[symbol](args)
  elif env.parent != nil:
    envCall(symbol, args, env.parent)
  else:
    assert false
    makeBool(false)

# Eval

proc eval(expr: Expr, env: Env): Expr;

proc makeCallable(lam: Lambda): Function =
  let f = proc(args: List): Expr =
    let envNew = Env(parent: lam.env)
    for index, item in lam.args.pairs:
      closureScope:
        let val = args[index]
        envSet(getSymbol(item), proc(args: List): Expr = val, envNew)
    eval(lam.body, envNew)
  return f

proc evalList(list: List, env: Env): Expr =
  let head = list[0];
  let tail = list[1..^1];
  if head.kind == ExprSymbol:
    case getSymbol(head) # TODO return return?
      of "quote": return list[1]
      of "lambda":
        return makeLambda(getList(list[1]), list[2], env)
      of "def":
        let value = eval(list[2], env)
        if value.kind == ExprLambda:
          envSet(getSymbol(list[1]), makeCallable(value.valueLambda), env)
        else:
          envSet(getSymbol(list[1]), proc(args: List): Expr = value, env)
        return value
      of "do":
        discard list[1..^1].mapIt(eval(it, env))
        return eval(list[^1], env)
      of "if": return if getBool(eval(list[1], env)): eval(list[2], env) else: eval(list[3], env)
      else: return envCall(getSymbol(head), tail.mapIt(eval(it, env)), env)
  elif head.kind == ExprList:
    let f = eval(head, env)
    let args = list[1..^1].mapIt(eval(it, env))
    return makeCallable(f.valueLambda)(args)
  else:
    assert false

proc eval(expr: Expr, env: Env): Expr =
  case expr.kind
    of ExprSymbol: envCall(getSymbol(expr), @[], env)
    of ExprList: evalList(getList(expr), env)
    of ExprNumber: expr
    of ExprBool: expr
    else:
      assert false
      makeBool(false)

let command = if paramCount() >= 1: $(paramStr(1)) else: ""
let parameter = if paramCount() >= 2: $(paramStr(2)) else: ""

case command
  of "who":
    echo("Mimicry Nim")
  of "selftest":
    let env = envDefault()
    echo(print(read("(111 222 333 hello true (1 2 3) true)")))
    echo(print(eval(read("(+ 1 2 3 4 5)"), env)))
    echo(print(eval(read("(* 1 2 3 4 5)"), env)))
    echo(print(eval(read("(- 6 2)"), env)))
    echo(print(eval(read("(/ 6 2)"), env)))
    echo(print(eval(read("pi"), env)))
    echo(print(eval(read("(sin pi)"), env)))
    echo(print(eval(read("(cos pi)"), env)))
    echo(print(eval(read("(length (quote (1 2 3 4 5)))"), env)))
    echo(print(eval(read("(do (def f 1) (def g 99) (+ f g))"), env)))
    echo(print(eval(read("(if (<= 3 2) 55 66)"), env)))
    echo(print(eval(read("(do (def add (lambda (a b) (+ a b))) (add 9 11))"), env)))
  of "run":
    let env = envDefault()
    echo(print(eval(read(parameter), env)))
  of "measure":
    var sum: float64 = 0.0
    let exprRead = read(parameter)
    let start = now()
    for i in 0..999:
      let env = envDefault()
      sum += getNumber(eval(exprRead, env))
    let stop = now()
    let ms = (stop - start).inMilliseconds();
    echo("(", ms, ' ', sum, ")")
  else:
    assert false
