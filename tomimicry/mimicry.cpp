
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <regex>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <variant>

/*
Types
*/

struct Environment;

using EnvironmentPtr = std::shared_ptr<Environment>;
using EnvironmentWeakPtr = std::weak_ptr<Environment>;

struct Expression;

using ExpressionPtr = std::shared_ptr<const Expression>; // TODO unique_ptr?
//using ExpressionUniquePtr = std::unique_ptr<const Expression>;

using List = std::list<Expression>; // TODO const expression?

using Function = std::function<Expression(const List)>;

struct Symbol {
	const std::string value;
	//explicit Symbol(const std::string& arg): value(arg) {}
	explicit Symbol(std::string arg): value(std::move(arg)) {}
};

struct Number {
	const double value;
	explicit Number(const double arg): value(arg) {}
};

struct Bool {
	const bool value;
	explicit Bool(const bool arg): value(arg) {}
};

struct String {
	const std::string value;
	//explicit String(const std::string& arg): value(arg) {}
	explicit String(std::string arg): value(std::move(arg)) {}
};

struct Lambda {
	const List args;
	const ExpressionPtr body; // TODO this should be unique_ptr but it seems that we make a copy somewhere of Lambda, ... why?
	const EnvironmentWeakPtr env; // TODO if this is not a weak reference there is a cycle, create a tiny diagram to show the cycle
	//explicit Lambda(const List& args, const ExpressionPtr& body, const EnvironmentWeakPtr& env): args(args), body(body), env(env) {}
	explicit Lambda(List args, ExpressionPtr body, EnvironmentWeakPtr env): args(std::move(args)), body(std::move(body)), env(std::move(env)) {}
};

struct Expression {
	const std::variant<const Symbol, const Number, const Bool, const String, const Lambda, const List> expression;
	//explicit Expression(const Symbol& arg): expression(arg) {}
	//explicit Expression(const Number& arg): expression(arg) {}
	//explicit Expression(const Bool& arg): expression(arg) {}
	//explicit Expression(const String& arg): expression(arg) {}
	//explicit Expression(const Lambda& arg): expression(arg) {}
	//explicit Expression(const List& arg): expression(arg) {}
	explicit Expression(Symbol arg): expression(std::move(arg)) {}
	explicit Expression(Number arg): expression(std::move(arg)) {}
	explicit Expression(Bool arg): expression(std::move(arg)) {}
	explicit Expression(String arg): expression(std::move(arg)) {}
	explicit Expression(Lambda arg): expression(std::move(arg)) {}
	explicit Expression(List arg): expression(std::move(arg)) {}
};

struct Environment {
	// maybe add a tiny std::vector<std::tuple<std::string, Function>> to hold up to e.g. 10 elements?
	std::map<std::string, Function> table; // unordered_map is slower inside measure, altough it has O(1) lookup and map has O(log(n))
	EnvironmentPtr parent;
};

/*
Helper
*/

// alternative would be to add an "inline const std::string" into each class, but this "debug" information should not be part of the class/struct itself
template<typename T> std::string className() { return "Unknown"; }
template<> std::string className<Symbol>() { return "Symbol"; }
template<> std::string className<Number>() { return "Number"; }
template<> std::string className<Bool>() { return "Bool"; }
template<> std::string className<String>() { return "String"; }
template<> std::string className<Lambda>() { return "Lambda"; }

template <typename T> T extractAs(const Expression& expr) {
	if (std::holds_alternative<T>(expr.expression)) {
		return std::get<T>(expr.expression);
	}
	throw std::runtime_error("Not A " + className<T>());
}

/*
Read

Converts a string into a tree of expressions e.g.

"(+ 1 2)" -> Expression(List(Expression(Symbol(+)), Expression(Number(1)), Expression(Number(2))))
*/

Expression read(const std::string& string, std::string::const_iterator& it) {
	// TODO current regexps do not care if there is something afterwards, is this a good/bad idea?
	// TODO raw string?
	// TODO regexp should be global and not recompiled for every call of read
	std::regex tokenSymbol("^[a-zA-Z\\-+*/=<>][a-zA-Z0-9\\-+*/=<>]*");
	std::regex tokenNumber("^[0-9]+(\\.[0-9]+)?");
	std::regex tokenBool("^(true|false)");
	std::regex tokenString("^\"[^\"]*\"");
	std::smatch match;

	while (*it == ' ') {
		std::advance(it, 1);
	}

	if (it == string.cend()) {
		throw std::runtime_error("Unexpected End ");
	} else if (*it == '(') {
		std::advance(it, 1);
		List list;
		while (*it != ')') {
			list.push_back(read(string, it));
		}
		std::advance(it, 1); // )
		return Expression(list);
	} else if (std::regex_search(it, string.cend(), match, tokenBool)) {
		std::advance(it, match.str().size());
		return Expression(Bool(match.str() == "true"));
	} else if (std::regex_search(it, string.cend(), match, tokenSymbol)) {
		std::advance(it, match.str().size());
		return Expression(Symbol(match.str()));
	} else if (std::regex_search(it, string.cend(), match, tokenNumber)) {
		std::advance(it, match.str().size());
		return Expression(Number(std::stod(match.str())));
	}

	throw std::runtime_error("Unknown Character '" + std::string(1, *it) + "'");
}

Expression read(const std::string& string) {
	auto it = string.cbegin();
	return read(string, it);
}

/*
Print

Convert a tree of expressions back into a string e.g.

Expression(List(Expression(Number(1)), Expression(List(Expression(Number(2)), Expression(Number(3)))), Expression(Number(4)))) -> "(1 (2 3))"
*/

// TDOO std::ostream should be return value?

void print(std::ostream& stream, const Expression& expr);
void print(std::ostream& stream, const List& list);

void print(std::ostream& stream, const Symbol& arg) {
	stream << arg.value;
}

void print(std::ostream& stream, const Number& arg) {
	stream << arg.value;
}

void print(std::ostream& stream, const Bool& arg) {
	if (arg.value) {
		stream << "true";
	} else {
		stream << "false";
	}
}

void print(std::ostream& stream, const String& arg) {
	stream << "\"" << arg.value << "\"";
}

void print(std::ostream& stream, const Lambda& arg) {
	stream << "(lambda ";
	print(stream, arg.args);
	stream << " ";
	print(stream, *arg.body);
	stream << ")";
}

void print(std::ostream& stream, const List& list) {
	// TODO is there a simpler idea to print everything but the last ' '
	stream << '(';
	const auto last = std::prev(list.cend());
	for (auto it = list.cbegin(); it != list.cend(); ++it) {
		print(stream, *it);
		if (it != last) {
			stream << ' ';
		}
	}
	stream << ')';
}

void print(std::ostream& stream, const Expression& expr) {
	std::visit([&stream](const auto& arg) { print(stream, arg); }, expr.expression);
}

/*
Environment

Store variables. All variables are internally handled as functions, even simple numbers.
*/

using ArithmeticBinaryOperation = double(double,double);
Function makeArithmeticBinaryOperation(ArithmeticBinaryOperation f) {
	return [f](const auto& args) {
		double result = std::accumulate(std::next(args.cbegin()), args.cend(), extractAs<const Number>(args.front()).value, [f](const auto& arg1, const auto& arg2) {
			return f(arg1, extractAs<const Number>(arg2).value);
		});
		return Expression(Number(result));
	};
}

using RelationalBinaryOperation = bool(double,double);
Function makeRelationalBinaryOperation(RelationalBinaryOperation f) {
	return [f](const auto& args) {
		// TODO support arbitrary amount of args
		double arg1 = extractAs<const Number>(args.front()).value;
		double arg2 = extractAs<const Number>(*std::next(args.cbegin())).value;
		return Expression(Bool(f(arg1, arg2)));
	};
}

using ArithmeticUnaryOperation = double(double);
Function makeArithmeticUnaryOperation(ArithmeticUnaryOperation f) {
	return [f](const auto& args) {
		double arg = extractAs<const Number>(args.front()).value;
		return Expression(Number(f(arg)));
	};
}

EnvironmentPtr environmentDefault() {
	auto env = std::make_shared<Environment>();
	env->table["+"] = makeArithmeticBinaryOperation([](double a, double b) { return a + b; });
	env->table["*"] = makeArithmeticBinaryOperation([](double a, double b) { return a * b; });
	env->table["-"] = makeArithmeticBinaryOperation([](double a, double b) { return a - b; });
	env->table["/"] = makeArithmeticBinaryOperation([](double a, double b) { return a / b; });
	env->table["sin"] = makeArithmeticUnaryOperation(std::sin);
	env->table["cos"] = makeArithmeticUnaryOperation(std::cos);
	env->table["<="] = makeRelationalBinaryOperation([](double a, double b) { return a <= b; });
	env->table["length"] = [](const auto& args) {
		const auto& head = args.front();
		if (std::holds_alternative<const List>(head.expression)) {
			const auto& list = std::get<const List>(head.expression);
			return Expression(Number(list.size()));
		} else {
			throw std::runtime_error("Not A List");
		}
	};
	env->table["pi"] = [](const auto& args) { return Expression(Number(3.14159265359)); }; // TODO still no pi constant in std?

	// envTest is just here to test parent member, can be removed
	auto envTest = std::make_shared<Environment>();
	envTest->table["zero"] = [](const auto& args) { return Expression(Number(0.0)); };
	envTest->table["one"] = [](const auto& args) { return Expression(Number(1.0)); };
	envTest->parent = std::move(env);

	return envTest;
}

// TODO use std::forward on List&& ?
Expression environmentCall(const Symbol& symbol, const EnvironmentPtr& env, const List& list) {
	auto it = env->table.find(symbol.value);
	if (it != env->table.end()) {
		const auto& callable = it->second;
		return callable(list);
	} else if (env->parent) {
		return environmentCall(symbol, env->parent, list);
	}
	throw std::runtime_error("Symbol '" + symbol.value + "' Not Found");
}

/*
Evaluate

Only works on expression trees and evaluates them. E.g.

Expression(List(Expression(Symbol(+)), Expression(Number(1)), Expression(Number(2)))) -> Expression(Number(3))
*/

Expression evaluate(const Expression& expr, const EnvironmentPtr& env);

Function makeCallable(const Lambda& lambda) {
	return [lambda](const auto& args) {
		auto envNew = std::make_shared<Environment>(); // TODO create this environment when Lambda is created and just empty it? ... think about this some longer
		envNew->parent = lambda.env.lock();
		// C++11 size is O(1)
		if (lambda.args.size() != args.size()) {
			throw std::runtime_error("Argument Number Mismatch");
		}
		auto it1 = lambda.args.cbegin();
		auto it2 = args.cbegin();
		while (it1 != lambda.args.cend()) {
			const auto& symbol = std::get<const Symbol>(it1->expression);
			const auto& value = *it2;
			envNew->table[symbol.value] = [&value](const auto& args) { return value; }; // use makeCallable and create EnvSet?
			++it1;
			++it2;
		}
		return evaluate(*lambda.body, envNew);
	};
}

Function makeCallable(const Expression& expr) {
	if (std::holds_alternative<const Lambda>(expr.expression)) {
		return makeCallable(std::get<const Lambda>(expr.expression));
	} else {
		return [expr](const auto& args) { return expr; };
	}
}

List evaluateTail(const List& list, const EnvironmentPtr& env) {
	auto tail = List();
	/*
	for (auto it = std::next(list.cbegin()); it != list.cend(); ++it) {
		tail.push_back(evaluate(*it, env));
	}
	*/
	std::for_each(std::next(list.cbegin()), list.cend(),[&tail, &env](const Expression& expr) {
		tail.push_back(evaluate(expr, env));
	});
	return tail;
}

// TODO call is called 3 times in this function and once in another evaluate function -> something feels wrong, this should be simpler, or describe why there are this 4 cases
Expression evaluate(const List& list, const EnvironmentPtr& env) {
	if (list.empty()) {
		throw std::runtime_error("Cannot Evaluate Empty List"); // classic lisp returns NIL
	}
	const auto& head = list.front().expression;
	if (std::holds_alternative<const Symbol>(head)) {
		const auto& symbol = std::get<const Symbol>(head);
		// TODO most ifs do not check if there are enough arguments and just blindly iterate
		if (symbol.value == "quote") {
			return *std::next(list.cbegin());
		} else if (symbol.value == "print") {
			const auto& result = evaluate(*std::next(list.cbegin()), env);
			print(std::cout, result); // TODO remove std::cout
			return result;
		} else if (symbol.value == "lambda") {
			auto it = std::next(list.cbegin());
			const auto& args = *it++;
			const auto& body = *it;
			return Expression(Lambda(std::get<const List>(args.expression), std::make_shared<Expression>(body), env));
		} else if (symbol.value == "def") {
			auto it = std::next(list.cbegin());
			const auto& name = extractAs<const Symbol>(*it++).value;
			const auto& value = evaluate(*it, env);
			// only add new definition if it is not already existing
			if (env->table.count(name) == 0) {
				env->table[name] = makeCallable(value);
			} else {
				throw std::runtime_error("Cannot Re-Assign Variable");
			}
			return value;
		} else if (symbol.value == "do") {
			auto it = std::next(list.cbegin());
			for (; it != std::prev(list.cend()); ++it) {
				evaluate(*it, env);
			}
			return evaluate(*it, env);
		} else if (symbol.value == "if") {
			auto it = std::next(list.cbegin());
			const auto& condition = *it++;
			const auto& onTrue = *it++;
			const auto& onFalse = *it;
			const auto& result = extractAs<const Bool>(evaluate(condition, env)).value ? onTrue : onFalse;
			return evaluate(result, env);
		} else {
			const auto& args = evaluateTail(list, env);
			return environmentCall(symbol, env, args);
		}
	} else if (std::holds_alternative<const List>(head)) {
		const auto& args = evaluateTail(list, env);
		const auto& f = evaluate(std::get<const List>(head), env);
		//const auto& g = std::get<Lambda>(f.expression); // TODO replacing f with the code leads to -> bad_variant_access wit clang & bad_alloc with GCC? why? this only happens if Lambda uses a sharedptr for environment (which leads to memory leaks)
		return makeCallable(f)(args);
	} else {
		throw std::runtime_error("Cannot Evaluate Head");
	}
}

Expression evaluate(const Expression& expr, const EnvironmentPtr& env) {
	if (std::holds_alternative<const Symbol>(expr.expression)) {
		const auto& symbol = std::get<const Symbol>(expr.expression);
		return environmentCall(symbol, env, List());
	} else if (std::holds_alternative<const List>(expr.expression)) {
		return evaluate(std::get<const List>(expr.expression), env);
	} else {
		return expr;
	}
}

/*
Test

(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 150))

5.71338e+265
ca. 180ms (clang++ 6.0.0 -O3)
ca. 211ms (g++ 7.4.0 -O3)

import time
def factorial(n):
	if n <= 1:
		return 1
	else:
		return n * factorial(n - 1)
sum = 0.0
t1 = time.time()
for i in range(1000):
	sum += factorial(150)
t2 = time.time()
print(t2 - t1)
print(sum)

5.71338e+265
ca. 24ms (python3)

function factorial(n) {
	if (n <= 1) {
		return 1;
	} else {
		return n * factorial(n - 1);
	}
}
let sum = 0.0;
const t1 = performance.now();
for (let i = 0; i < 1000000; i += 1) {
	sum += factorial(150);
}
const t2 = performance.now();
console.log(t2 - t1);
console.log(sum);

5.71338+268
ca. 735 ms (firefox 66.0.3)
*/

void measure() {
	double sum = 0.0;
	const auto& exprRead = read("(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 150))");
	//const auto& expression = evaluate(read("(+ 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3)"), env);
	for (int i = 0; i < 1000; i += 1) {
		auto env = environmentDefault();
		const auto& exprEval = evaluate(exprRead, env);
		sum += extractAs<const Number>(exprEval).value;
	}
	std::cout << sum << "\n";
}

void test(const std::string& input, const double expected) {
	auto env = environmentDefault();
	double result = extractAs<const Number>(evaluate(read(input), env)).value;
	const double epsilon = 1.0e-3;
	if (std::abs(result - expected) > epsilon) {
		throw std::runtime_error("Test Failed " + input + " -> " + std::to_string(result) + " != " + std::to_string(expected));
	}
}

void testError(const std::string& input) {
	auto env = environmentDefault();
	std::string what;
	try {
		evaluate(read(input), env);
	} catch (const std::runtime_error& error) {
		what = error.what();
	}
	// TODO compare with expectedError, define all error strings globally first, what to do with errors which are assembled during run time?
	if (what.empty()) {
		throw std::runtime_error("Test Error Failed");
	}
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t* data, size_t size) {
	std::string str(reinterpret_cast<const char*>(data), size);
	auto env = environmentDefault();
	try {
		//read(str);
		const auto& expr = evaluate(read(str), env);
		//std::cout << str << " ";
		print(std::cout, expr);
		std::cout << "\n";
	} catch (const std::out_of_range& error) {
		// at least one occurence happened in std::stod
	} catch (const std::runtime_error& error) {
		//
	}
	return 0;
}

/*
Main
*/

int main(int argc, char* argv[]) {
	const auto command = argc >= 2 ? std::string(argv[1]) : "";
	const auto parameter = argc >= 3 ? std::string(argv[2]) : "";

	if ((command == "status") && (parameter == "who")) {
		std::cout << "\"Mimicry C++17\"" << std::endl;
	} else if (command == "selftest") {
		test("(+ 1)", 1);
		test("(+ 1.1)", 1.1);
		test("(+ 1.1 2.2)", 1.1 + 2.2);
		test("(+ 1.1 2.2 3.3)", 1.1 + 2.2 + 3.3);
		test(" (  +   1    2     3)      ", 6);
		test("(+ 1 (+ 2 3) (+ 4 5))", 1 + 2 + 3 + 4 + 5);
		test("(* 3 4)", 3 * 4);
		test("(- 5 3)", 5 - 3);
		test("(/ 24.0 4.0 3.0)", 24.0 / 4.0 / 3.0);
		test("(zero)", 0);
		test("(one)", 1);
		test("(sin 3.1415)", 0.0);
		test("(cos 3.1415)", -1.0);
		test("(+ zero one one one)", 0 + 1 + 1 + 1);
		test("(length (quote ()))", 0);
		test("(length (quote (1)))", 1);
		test("(length (quote (1 2)))", 2);
		test("(length (quote (1 2 3 4 5 6)))", 6);
		test("(do (+ 1 1) (+ 2 2) (+ 3 3))", 3 + 3);
		test("(do (+ 1 1))", 1 + 1);
		test("(if (<= 1 2) (+ 3 3) (+ 4 4))", 3 + 3);
		test("(if (<= 3 2) (+ 3 3) (+ 4 4))", 4 + 4);
		test("(do (def e 2.718) (+ e))", 2.718);
		test("((lambda (a b) (+ a b)) 1 2)", 3);
		test("((lambda () 1))", 1);
		// following examples leak memory
		test("(do (def add (lambda (a b) (+ a b))) (add 3 4))", 7);
		test("(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 5))", 5 * 4 * 3 * 2 * 1);
		test("(do (def x 1) (def f (lambda () (do (def x 2) x))) (f))", 2);
		//test("(do (def x 8) (def f (lambda () x)) (do (def x 16) (f)))", 8);
		test("(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) (f)))))", 8);
		test("(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) x))))", 16);

		testError(" ");
		testError("(1 2 3)");

		const std::list<Expression> exprs = {
			//const Expression expr1 = Expression(Atom(Symbol("+")));
			Expression(Number(1.0)),
			Expression(Bool(true)),
			Expression(String("Hello World")),
			Expression({ Expression(Symbol("+")), Expression(Number(3.0)), Expression(Number(5.0)) }),
			Expression({ Expression(Symbol("quote")), Expression({ Expression(Number(1.0)), Expression(Number(2.0)), Expression(Number(3.0)) }) }),
			Expression({ Expression(Symbol("length")), Expression({ Expression(Symbol("quote")), Expression({ Expression(Number(1.0)), Expression(Number(2.0)), Expression(Number(3.0)) }) }) }),
			read("(<= 1 2)"),
			read("(do (print (+ 1 1)) (print (+ 2 2)))"),
			read("(lambda (a b) (+ a b))"),
		};
	} else if (command == "run") {
		auto env = environmentDefault();
		print(std::cout, evaluate(read(parameter), env));
		std::cout << std::endl;
	} else if (command == "measure") {
		const auto& exprRead = read(parameter);
		const auto start = std::chrono::steady_clock::now();
		double sum = 0.0;
		for (int i = 0; i < 1000; i += 1) {
			auto env = environmentDefault();
			const auto& exprEval = evaluate(exprRead, env);
			sum += extractAs<const Number>(exprEval).value;
		}
		const auto stop = std::chrono::steady_clock::now();
		const auto time = stop - start;
		std::cout << "(" << std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << " " << std::setprecision(17) << sum << ")" << std::endl;
	} else {
		throw std::runtime_error("Unknown Command");
	}

	return 0;
}
