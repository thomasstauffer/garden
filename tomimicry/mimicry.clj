
(defn mprint [expr]
	(cond
		(list? expr) (str "(" (clojure.string/join " " (map mprint expr)) ")")
		(string? expr) (str "\"" expr "\"")
		:else (str expr)
	)
)

(defn mread [string]
	(let [
		replace clojure.string/replace
		split clojure.string/split
		notempty? (fn [s] (not= s ""))
		tokens (filter notempty? (split (replace (replace string #"\(" " ( ") #"\)" " ) ") #" "))
		p (fn pp [l index]
			(let [index1 (+ index 1) token (nth tokens index)]
				(if (nil? l)
					(cond
						(= token "true") [true index1]
						(= token "false") [false index1]
						(some? (re-find #"^[0-9]+\.[0-9]+$" token)) [(Double/parseDouble token) index1]
						(= token "(") (pp '() index1)
						:else [token index1]
					)
					(if (= token ")")
						[l index1]
						(let [li (pp nil index)] (pp (concat l (list (li 0))) (li 1))) ; TODO build left recursive? cons/conj
					)
				)
			)
		)
	] ((p nil 0) 0))
)

(defn meval []
)

(defn who []
	(println "Mimicry Clojure")
)

(defn selftest []
	(println (mprint '(1.2 "s" 3.4 do)))
	(println (mread "12.4"))
	(println (mread "true"))
	(println (mread "false"))
	(println (mread "(1.0 2.0)"))
	(println (mread "(1.0 2.0 3.0 (4.0 5.0 (6.0 7.0) 8.0) 9.0 10.0)"))
	(println (mread "(do (print (+ 1.0 2.0)) (print (- 2.0 3.0)))"))
)

(defn run [code])

(defn measure [code])

(defn main []
	(let [
		command (nth *command-line-args* 0 nil)
		parameter (nth *command-line-args* 1 nil)
		]
		(cond
			(and (= command "status") (= parameter "who")) (who)
			(= command "selftest") (selftest)
			(= command "run") (run)
			(= command "measure") (measure)
		)
	)
)

(main)
