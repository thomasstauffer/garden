
extern crate regex;
use regex::Regex;
use std::collections::HashMap;

// 	TODO create simple regex and remove cargo, to have a tiny self contained file

/*
str: stack
&str: ref stack
String: heap
*/

fn expr_as_symbol(expr: &MExpr) -> &String {
	if let MExpr::MSymbol(value) = expr {
		return &value;
	}
	panic!();
}

// TODO generic possible?
fn expr_as_number(expr: &MExpr) -> f64 {
	if let MExpr::MNumber(value) = expr {
		return value.clone();
	}
	panic!();
}

fn expr_from_number(value: f64) -> MExpr {
	return MExpr::MNumber(value);
}

fn expr_as_bool(expr: &MExpr) -> bool {
	if let MExpr::MBool(value) = expr {
		return value.clone();
	}
	panic!();
}

fn expr_from_bool(value: bool) -> MExpr {
	return MExpr::MBool(value);
}

type MList = Vec<MExpr>;

fn expr_as_list(expr: &MExpr) -> &MList {
	if let MExpr::MList(value) = expr {
		return value;
	}
	panic!();
}

#[derive(Clone)]
struct MLambda {
	args: MList,
	body: Box<MExpr>,
	//env: &MEnv,
	//env: &'a MEnv,
	//const ExpressionPtr body; // TODO this should be unique_ptr but it seems that we make a copy somewhere of Lambda, ... why?
	//const EnvironmentWeakPtr env; // TODO if this is not a weak reference there is a cycle, create a tiny diagram to show the cycle
}

#[derive(Clone)]
enum MExpr {
	MSymbol(String),
	MNumber(f64),
	MBool(bool),
	MLambda(MLambda),
	MList(MList),
}

type MFunction = Fn(&[MExpr]) -> MExpr;

struct MEnv {
	//EnvironmentPtr parent;
	table: HashMap<String, Box<MFunction>>,
}

// read

fn nth_char(string: &str, index: usize) -> char {
	return string.chars().nth(index).unwrap();
}

// TODO why lifetime?
fn read_match<'a>(string: &'a str, re: &Regex, index: usize) -> &'a str {
	let mtch = re.find_at(string, index);
	if mtch.is_some() {
		if mtch.unwrap().start() == index {
			return mtch.unwrap().as_str();
		}
	}
	return "";
}

fn read_index(string: &str, index: &mut usize) -> MExpr {
	let token_symbol = Regex::new(r"[a-zA-Z\+*-/=<>][a-zA-Z0-9\+*-/=<>]*").unwrap();
	let token_number = Regex::new(r"[0-9]+(\.[0-9]+)?").unwrap();
	let token_bool = Regex::new(r"(true|false)").unwrap();
	//let token_string = Regex::new(r#"^"[^"]*""#).unwrap();

	while nth_char(string, *index) == ' ' {
		*index += 1;
	}

	if nth_char(string, *index) == '(' {
		*index += 1; // (
		let mut list = MList::new();
		while nth_char(string, *index) != ')' {
			let expr = read_index(string, index);
			list.push(expr);
		}
		*index += 1; // )
		return MExpr::MList(list);
	}

	let number_result = read_match(string, &token_number, *index);
	if number_result.len() > 0 {
			let value = number_result.parse::<f64>().unwrap();
			*index += number_result.len();
			return MExpr::MNumber(value);
	}

	let bool_result = read_match(string, &token_bool, *index);
	if bool_result.len() > 0 {
			let value = bool_result.parse::<bool>().unwrap();
			*index += bool_result.len();
			return MExpr::MBool(value);
	}

	let symbol_result = read_match(string, &token_symbol, *index);
	if symbol_result.len() > 0 {
			let value = symbol_result.to_string();
			*index += symbol_result.len();
			return MExpr::MSymbol(value);
	}

	panic!();
}

fn read(string: &str) -> MExpr {
	// TODO version without mut
	let mut index : usize = 0;
	return read_index(string, &mut index);
}

// print

fn print_list(list: &MList) -> String {
	let s : Vec<String> = list.into_iter().map(|expr| print(expr)).collect(); // TODO shortcut without collect and just join?
	return format!("({})", s.join(" "));
}

fn print(expr: &MExpr) -> String {
	match expr {
		MExpr::MSymbol(value) => format!("{}", value),
		MExpr::MNumber(value) => format!("{}", value),
		MExpr::MBool(value) => format!("{}", value),
		MExpr::MLambda(value) => format!("(lambda {} {})", print_list(&value.args), print(&value.body)),
		MExpr::MList(list) => print_list(list),
	}
}

// env

fn env_default_add(args: &[MExpr]) -> MExpr {
	let floats : Vec<f64> = args.into_iter().map(|arg| expr_as_number(&arg)).collect();
	let sum = floats.into_iter().fold(0.0f64, |a, i| a + i); // TODO without default?
	return expr_from_number(sum);
}

fn env_add(env: &mut MEnv, name: &str, function: Box<MFunction>) {
	if env.table.contains_key(name) {
		panic!("Cannot Re-Assign Variable");
	} else {
		env.table.insert(name.to_string(), function);
	}
}

fn env_default() -> MEnv {
	// can Box::new moved into env_add?
	let mut env = MEnv{table: HashMap::new()};
	env_add(&mut env, "+", Box::new(env_default_add));
	env_add(&mut env, "<=", Box::new(|args| expr_from_bool(expr_as_number(&args[0]) <= expr_as_number(&args[1]))));
	env_add(&mut env, "sin", Box::new(|args| expr_from_number(expr_as_number(&args[0]).sin())));
	env_add(&mut env, "pi", Box::new(|_| expr_from_number(3.14159265359)));
	env_add(&mut env, "length", Box::new(|args| expr_from_number(expr_as_list(&args[0]).len() as f64)));
	return env;
}

fn env_call(name: &String, args: &[MExpr], env: &MEnv) -> MExpr {
	// TODO check parent
	let callable = &env.table[name];
	return callable(args);
}

// eval

fn eval_args(args: &[MExpr], env: &mut MEnv) -> MList {
	let mut results = MList::new();
	for expr in args {
		results.push(eval(expr, env));
	}
	return results;
}

fn eval_list(list: &[MExpr], env: &mut MEnv) -> MExpr {
	let head = &list[0];
	if let MExpr::MSymbol(value) = head {
		return match &value[..] {
			"quote" => list[1].clone(),
			"console-print" => {
				print!("{}", print(&eval(&list[1], env)));
				return list[1].clone();
			},
			"lambda" => {
				let args = expr_as_list(&list[1]).clone();
				let body = Box::new(list[2].clone());
 				return MExpr::MLambda(MLambda{args: args, body: body}); // TODO add env will be tricky
			},
			"def" => {
				let name = expr_as_symbol(&list[1]);
				let value = eval(&list[2], env);
				let result = value.clone();
				env_add(env, name, Box::new(move |_| value.clone())); // TODO understand better what happens here
				return result;
			},
			"do" => list[1..].into_iter().fold(expr_from_number(0.0), |_, expr| eval(&expr,  env)), // TODO without default?
			"if" => if expr_as_bool(&eval(&list[1], env)) { eval(&list[2], env) } else { eval( &list[3], env) },
			_ => env_call(&value, &eval_args(&list[1..], env), env),
		}
	} else if let MExpr::MList(_value) = head {
		let f = eval(head, env);
		if let MExpr::MLambda(lambda) = f {
			return eval_lambda(&lambda, &list[1..]);
		} else {
			panic!();
		}
		//return makeCallable(f)(args);
		//panic!("eval_list list");
	} else {
		panic!("Cannot Evaluate Head");
	}
}

fn eval_lambda(lambda: &MLambda, args: &[MExpr]) -> MExpr {
	if lambda.args.len() != args.len() {
		panic!("Arguments Number Mismatch");
	}
	let count = lambda.args.len();
	let mut env = env_default(); // TODO add env from lambda
	let args_evaled : MList = args.into_iter().map(|expr| eval(expr, &mut env)).collect();
	for i in 0..count {
		let arg = (args_evaled[i]).clone();
		env_add(&mut env, expr_as_symbol(&lambda.args[i]), Box::new(move |_| arg.clone()));
	}
	return eval(&lambda.body, &mut env);
}

fn eval(expr: &MExpr, env: &mut MEnv) -> MExpr {
	match expr {
		MExpr::MSymbol(value) => env_call(&value, &MList::new(), env),
		MExpr::MList(value) => eval_list(&value, env),
		MExpr::MBool(_) => expr.clone(),
		MExpr::MNumber(_) => expr.clone(),
		_ => panic!("Cannot Eval"),
	}
}

// test

fn test_read_print(string: &str) {
	assert_eq!(print(&read(string)), string);
}

fn test(string: &str, expected: f64) {
	let mut env = env_default();
	let expr = eval(&read(string), &mut env);
	let epsilon : f64 = 1.0e-6;
	match expr {
		MExpr::MNumber(value) => assert!((value - expected).abs() < epsilon, "{} = {}", value, expected),
		_ => {
			println!("{}", print(&expr));
			panic!("Test Failed");
		},
	}
}

fn main() {
	println!("Mimicry Rust");

	test_read_print("0");
	test_read_print("()");
	test_read_print("(12 34 56 true 78 (4 5 6))");

	test("0.0", 0.0);
	test("1.0", 1.0);
	test("9.9", 9.9);

	test("(length (quote (1 2 3)))", 3.0);

	test("(+ 1)", 1.0);
	test("(+ 1 2)", 1.0 + 2.0);
	test("(+ 1 2 3)", 1.0 + 2.0 + 3.0);
	test("(+ 1 (+ 2 3))", 1.0 + 2.0 + 3.0);
	test("(+ 1 (+ 2 (+ 3 4)))", 1.0 + 2.0 + 3.0 + 4.0);
	test("(sin 3.14159265359)", 0.0);
	test("(sin pi)", 0.0);

	//test("(console-print 1)", 1.0);

	test("(do (+ 1 1))", 1.0 + 1.0);
	test("(do (+ 1 1) (+ 2 2) (+ 3 3))", 3.0 + 3.0);

	test("(if (<= 1 2) (+ 3 3) (+ 4 4))", 3.0 + 3.0);
	test("(if (<= 3 2) (+ 3 3) (+ 4 4))", 4.0 + 4.0);

	test("(do (def e 2.718) (+ e))", 2.718);

	test("((lambda () 1))", 1.0);
	test("((lambda (a b) (+ a b)) 1 2)", 3.0);

	//test("(do (def add (lambda (a b) (+ a b))) (add 3 4))", 7.0);

	//test("(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 5))", 5 * 4 * 3 * 2 * 1);
	//test("(do (def x 1) (def f (lambda () (do (def x 2) x))) (f))", 2.0);

	//test("(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) (f)))))", 8.0);
	//test("(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) x))))", 16.0);

}
