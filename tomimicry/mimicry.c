
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>

typedef char* zstring;
typedef char const* czstring;

//#define GC_DISABLE

// Assert

#define ASSERT(condition) assert(condition, __LINE__)

void assert(bool condition, int line) {
	if (!condition) {
		printf("Assert Failed (Line:%i)\n", line);
		exit(1);
	}
}

// GC

/*
If someone is allocating new memory, all possible pointers have to be at the
front. Struct need to have a layout which also respects this.

TODO describe issue with rodata/data
*/

#define GC_MAGIC 0x7ee11ee7

struct GCHeader {
	struct {
		uint32_t magic;
		uint8_t n_pointers;
		bool tagged;
		uint16_t size;
		struct GCHeader* next;
	} info;
	const void* pointers[4]; // is not allocated just used for traversal
};

struct GC {
	int overall_alloc_count;
	int overall_alloc_bytes;
	int overall_freed_count;
	int overall_freed_bytes;
	struct GCHeader* alloc_start;
	struct GCHeader* alloc_end;
	struct GCHeader* alloc_first;
};

struct GC g_gc;

void gcInit() {
	struct GCHeader* header = NULL;
	ASSERT(sizeof(header->info) == (2 * sizeof(intptr_t)));
	g_gc.overall_alloc_count = 0;
	g_gc.overall_alloc_bytes = 0;
	g_gc.overall_freed_count = 0;
	g_gc.overall_freed_bytes = 0;
	g_gc.alloc_start = (void*) ~((intptr_t) 0);
	g_gc.alloc_end = NULL;
	g_gc.alloc_first = NULL;
}

void gcStat() {
	int count = 0;
	int count_tagged = 0;
	const struct GCHeader* current = g_gc.alloc_first;
	while (current != NULL) {
		ASSERT(current->info.magic == GC_MAGIC);
		count += 1;
		count_tagged += current->info.tagged ? 1 : 0;
		current = current->info.next;
	}

	printf("-- GC --\n");
	printf("Overall Alloc Count: %i\n", g_gc.overall_alloc_count);
	printf("Overall Alloc Bytes: %i\n", g_gc.overall_alloc_bytes);
	printf("Overall Freed Count: %i\n", g_gc.overall_freed_count);
	printf("Overall Freed Bytes: %i\n", g_gc.overall_freed_bytes);
	printf("Start: %p\n", g_gc.alloc_start);
	printf("End: %p\n", g_gc.alloc_end);
	printf("Current Alloc Count: %i\n", count);
	printf("Current Alloc Count Tagged: %i\n", count_tagged);
}

void* gcAlloc(int size, int n_pointers) {
#ifdef GC_DISABLE
	return malloc(size);
#endif
	g_gc.overall_alloc_count += 1;
	g_gc.overall_alloc_bytes += size;
	struct GCHeader* header = NULL;
	int size_header_info = sizeof(header->info);
	void* ptr = malloc(size_header_info + size);
	header = ptr;
	if (header < g_gc.alloc_start) {
		g_gc.alloc_start = ptr;
	}
	if (header > g_gc.alloc_end) {
		g_gc.alloc_end = ptr;
	}
	ASSERT(((intptr_t)ptr & 0b11) == 0); // aligned?
	header->info.magic = GC_MAGIC;
	header->info.n_pointers = n_pointers;
	header->info.next = g_gc.alloc_first;
	header->info.size = size;
	header->info.tagged = false;
	g_gc.alloc_first = header;
	void* result = ptr + size_header_info;
	// all possible pointers are NULL, if all structs are initialized properly this would not be necessary
	memset(result, 0, size);
	return result;
}

void gcMark(const void* root) {
#ifdef GC_DISABLE
	return;
#endif

	struct GCHeader* header = NULL;
	int size_header_info = sizeof(header->info);
	header = (struct GCHeader*) (root - size_header_info); // the aliased part is still const

	if((header < g_gc.alloc_start) || (header > g_gc.alloc_end)) {
		// it could also be that the object is not on the heap and instead in the constant section (e.g. string constants or global objects), add this kind of test
		return;
	}
	ASSERT(header->info.magic == GC_MAGIC);
	if (header->info.tagged) {
		return;
	}
	header->info.tagged = true;
	for (int i = 0; i < header->info.n_pointers; i += 1) {
		const void* ptr = header->pointers[i];
		if (ptr != NULL) {
			gcMark(header->pointers[i]);
		}
	}
};

void gcSweep() {
#ifdef GC_DISABLE
	return;
#endif

	struct GCHeader* current = g_gc.alloc_first;
	int size_header_info = sizeof(current->info);
	struct GCHeader* prev = NULL;
	while (current != NULL) {
		ASSERT(current->info.magic == GC_MAGIC);
		struct GCHeader* next = current->info.next;
		if (current->info.tagged) {
			current->info.tagged = false;
			prev = current;
		} else {
			g_gc.overall_freed_count += 1;
			g_gc.overall_freed_bytes += current->info.size;
			// overwrite intentionally, only to detect bugs
			memset(current, 0, size_header_info + current->info.size);
			if (current == g_gc.alloc_first) {
				g_gc.alloc_first = next;
			} else {
				prev->info.next = next;
			}
			// TODO if region gets smaller? adjust start/end
			//free(current);
		}
		current = next;
	}
};

// String

bool charIsDigit(char value) {
	return (value >= '0') && (value <= '9');
}

bool charIsAlpha(char value) {
	return (value >= 'a') && (value <= 'z');
}

bool charIsOp(char value) {
	return (value == '+') || (value == '-') || (value == '*') || (value == '/') || (value == '<') || (value == '>') || (value == '=');
}

bool charIsDot(char value) {
	return value == '.';
}

bool (*IS_NUMBER[])(char) = {charIsDigit, charIsDot};
bool (*IS_SYMBOL[])(char) = {charIsAlpha, charIsOp};

#define LENGTH(x) (sizeof(x) / sizeof(x[0]))

int strMatch(const czstring start, bool (* const is[])(char), int count) {
	czstring end = start;
	while (end != NULL) {
		bool found = false;
		for (int i = 0; i < count; i += 1) {
			if (is[i](*end)) {
				found = true;
				break;
			}
		}
		if (!found) {
			break;
		}
		end += 1;
	}
	return end - start;
}

bool strEqual(czstring str1, czstring str2) {
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	return (len1 == len2) && (strcmp(str1, str2) == 0); // TODO strcmp("ab", "abc")?
}

czstring strLeft(czstring string, int length) {
	char* s = gcAlloc(length, 0);
	strncpy(s, string, length);
	return s;
}

// Types

#define LAMBDA_N_POINTERS 3
struct Lambda {
	const struct List* params;
	const struct Expr* body;
	struct Env* env;
};

#define LIST_N_POINTERS 2
struct List {
	const struct Expr* expr;
	struct List* next;
};

enum ExprKind { SYMBOL, NUMBER, BOOLEAN, LAMBDA, LIST };

#define EXPR_N_POINTERS 1
struct Expr {
	union {
		const struct Lambda* lambda;
		const struct List* list;
		czstring symbol;
	};
	union {
		double number;
		bool boolean;
	};
	enum ExprKind kind;
};

const struct Expr* makeExprSymbol(czstring symbol) {
	struct Expr* result = gcAlloc(sizeof(struct Expr), EXPR_N_POINTERS);
	result->kind = SYMBOL;
	result->symbol = symbol;
	return result;
}

const struct Expr* makeExprNumber(double number) {
	struct Expr* result = gcAlloc(sizeof(struct Expr), EXPR_N_POINTERS);
	result->kind = NUMBER;
	result->number = number;
	return result;
}

const struct Expr* makeExprBoolean(bool boolean) {
	struct Expr* result = gcAlloc(sizeof(struct Expr), EXPR_N_POINTERS);
	result->kind = BOOLEAN;
	result->boolean = boolean;
	return result;
}

const struct Expr* makeExprLambda(const struct List* params, const struct Expr* body, struct Env* env) {
	struct Lambda* lambda = gcAlloc(sizeof(struct Lambda), LAMBDA_N_POINTERS);
	lambda->params = params;
	lambda->body = body;
	lambda->env = env;
	struct Expr* result = gcAlloc(sizeof(struct Expr), EXPR_N_POINTERS);
	result->kind = LAMBDA;
	result->lambda = lambda;
	return result;
}

const struct Expr* makeExprList(const struct List* list) {
	struct Expr* result = gcAlloc(sizeof(struct Expr), EXPR_N_POINTERS);
	result->kind = LIST;
	result->list = list;
	return result;
}

#define MAP_N_POINTERS 3
struct Map {
	czstring key;
	const struct Expr* param;
	struct Map* next;
	const struct Expr* (*callable)(const struct List*, const struct Expr*, struct Env*); // assuming that callables are never dynamically allocated
};

struct Map* makeMap(czstring key, const struct Expr* (*callable)(const struct List*, const struct Expr*, struct Env*), const struct Expr* param, struct Map* next) {
	struct Map* result = gcAlloc(sizeof(struct Map), MAP_N_POINTERS);
	result->key = key;
	// TODO slows down measure quite a bit
	// keys are sometimes not on the heap which is an issue for the GC at the moment
	//result->key = strLeft(key, strlen(key));
	result->callable = callable;
	result->param = param;
	result->next = next;
	return result;
}

#define ENV_N_POINTERS 2
struct Env {
	struct Map* map;
	struct Env* parent;
};

struct Map* envSearch(czstring symbol, const struct Env* env) {
	struct Map* current = env->map;
	while (current != NULL) {
		if (strEqual(current->key, symbol)) {
			return current;
		}
		current = current->next;
	}
	if (env->parent != NULL) {
		return envSearch(symbol, env->parent);
	}
	printf("Symbol Not Found (%s)", symbol);
	exit(1);
}

// Read

bool findBoolean(czstring string, int* length, const struct Expr** expr) {
	*length = 0;
	// TODO check length or at least describe the idea that NULL should be somewhere
	if ((string[0] == 't') && (string[1] == 'r') && (string[2] == 'u') && (string[3] == 'e')) {
		*length = 4;
		*expr = makeExprBoolean(true);
	} else if ((string[0] == 'f') && (string[1] == 'a') && (string[2] == 'l') && (string[3] == 's') && (string[4] == 'e')) {
		*length = 5;
		*expr = makeExprBoolean(false);
	}
	return *length > 0;
}

double strToDouble(czstring string) {
	double value;
	sscanf(string, "%lf", &value);
	return value;
}

bool findNumber(czstring string, int* length, const struct Expr** expr) {
	// TODO can this find functions be written a bit more concise?
	*length = strMatch(string, IS_NUMBER, LENGTH(IS_NUMBER));
	if (*length > 0) {
		*expr = makeExprNumber(strToDouble(string));
	}
	return *length > 0;
}

bool findSymbol(czstring string, int* length, const struct Expr** expr) {
	*length = strMatch(string, IS_SYMBOL, LENGTH(IS_SYMBOL));
	if (*length > 0) {
		*expr = makeExprSymbol(strLeft(string, *length));
	}
	return *length > 0;
}

const struct Expr* readIt(czstring* it) {
	const struct Expr* expr;
	int length = 0;

	// skip whitespace
	while (**it == ' ') {
		*it += 1;
	}

	if (**it == 0) {
		//
	} else if (**it == '(') {
		*it += 1;
		struct List* first = NULL;
		struct List* prev = NULL;
		while (**it != ')') {
			struct List* result = gcAlloc(sizeof(struct List), LIST_N_POINTERS);
			if (first == NULL) {
				first = result;
			} else {
				prev->next = result;
			}
			result->expr = readIt(it);
			result->next = NULL;
			prev = result;
		}
		*it += 1; // ')'
		return makeExprList(first);
	} else if (findBoolean(*it, &length, &expr)) {
		*it += length;
		return expr;
	} else if (findNumber(*it, &length, &expr)) {
		*it += length;
		return expr;
	} else if (findSymbol(*it, &length, &expr)) {
		*it += length;
		return expr;
	}

	// TODO return error?
	printf("Unexpected String (%s)\n", *it);
	exit(1);
}

const struct Expr* read(czstring string) {
	return readIt(&string);
}

// Print

void print(const struct Expr* expr) {
	if (expr->kind == SYMBOL) {
		printf("%s", expr->symbol);
	} else if (expr->kind == NUMBER) {
		printf("%g", expr->number);
	} else if (expr->kind == BOOLEAN) {
		printf(expr->boolean ? "true" : "false");
	} else if (expr->kind == LAMBDA) {
		printf("(lambda ");
		print(makeExprList(expr->lambda->params));
		printf(" ");
		print(expr->lambda->body);
		printf(")");
	} else if (expr->kind == LIST) {
		const struct List* list = expr->list;
		printf("(");
		while (list != NULL) {
			print(list->expr);
			list = list->next;
			if (list != NULL) {
				printf(" ");
			}
		};
		printf(")");
	} else {
		printf("Invalid Expression\n");
		exit(1);
	}
}

// Eval

const struct Expr* eval(const struct Expr* expr, struct Env* env);
const struct Expr* callableParam(const struct List* args, const struct Expr* param, struct Env* env);

const struct Expr* evalEnv(czstring symbol, const struct List* args, struct Env* env) {
	struct Map* current = envSearch(symbol, env);
	return current->callable(args, current->param, env);
}

const struct Expr* evalLambda(const struct Lambda* lambda, const struct List* args, struct Env* env) {
	struct Env* envNew = gcAlloc(sizeof(struct Env), ENV_N_POINTERS);
	envNew->map = NULL;
	envNew->parent = lambda->env;
	const struct List* params = lambda->params;
	while (params != NULL) {
		czstring key = params->expr->symbol;
		const struct Expr* value = eval(args->expr, env);
		envNew->map = makeMap(key, callableParam, value, envNew->map);
		params = params->next;
		args = args->next;
	}
	return eval(lambda->body, envNew);
}

const struct Expr* evalList(const struct List* list, struct Env* env) {
	const struct Expr* head = list->expr;
	struct List* args = list->next;
	if (head->kind == SYMBOL) {
		return evalEnv(head->symbol, args, env);
	} else if (head->kind == LIST) {
		const struct Expr* callable = evalList(head->list, env);
		if (callable->kind == LAMBDA) {
			return evalLambda(callable->lambda, args, env);
		} else {
			// TODO add test case, maybe this if can be moved somewhere else
			printf("Not Implemented (%i)\n", callable->kind);
			exit(1);
		}
	}
	printf("Cannot Eval Head (%i)\n", head->kind);
	exit(1);
}

const struct Expr* eval(const struct Expr* expr, struct Env* env) {
	if (expr->kind == SYMBOL) {
		return evalEnv(expr->symbol, NULL, env);
	} else if (expr->kind == LIST) {
		return evalList(expr->list, env);
	} else {
		return expr;
	}
}

// Environment

// TODO the following 4 share the same code

const struct Expr* callableAdd(const struct List* args, const struct Expr* param, struct Env* env) {
	double result = eval(args->expr, env)->number;
	args = args->next;
	while (args != NULL) {
		result += eval(args->expr, env)->number;
		args = args->next;
	};
	return makeExprNumber(result);
}

const struct Expr* callableSub(const struct List* args, const struct Expr* param, struct Env* env) {
	double result = eval(args->expr, env)->number;
	args = args->next;
	while (args != NULL) {
		result -= eval(args->expr, env)->number;
		args = args->next;
	};
	return makeExprNumber(result);
}

const struct Expr* callableMul(const struct List* args, const struct Expr* param, struct Env* env) {
	double result = eval(args->expr, env)->number;
	args = args->next;
	while (args != NULL) {
		result *= eval(args->expr, env)->number;
		args = args->next;
	};
	return makeExprNumber(result);
}

const struct Expr* callableDiv(const struct List* args, const struct Expr* param, struct Env* env) {
	double result = eval(args->expr, env)->number;
	args = args->next;
	while (args != NULL) {
		result /= eval(args->expr, env)->number;
		args = args->next;
	};
	return makeExprNumber(result);
}

const struct Expr* callableLowerThanEqual(const struct List* args, const struct Expr* param, struct Env* env) {
	double first = eval(args->expr, env)->number;
	double second = eval(args->next->expr, env)->number;
	return makeExprBoolean(first <= second);
}

const struct Expr* callableSin(const struct List* args, const struct Expr* param, struct Env* env) {
	double first = eval(args->expr, env)->number;
	return makeExprNumber(sin(first));
}

const struct Expr* callableCos(const struct List* args, const struct Expr* param, struct Env* env) {
	double first = eval(args->expr, env)->number;
	return makeExprNumber(cos(first));
}

const struct Expr* callableQuote(const struct List* args, const struct Expr* param, struct Env* env) {
	return makeExprList(args->expr->list);
}

const struct Expr* callableLength(const struct List* args, const struct Expr* param, struct Env* env) {
	const struct Expr* result = eval(args->expr, env);
	const struct List* list = result->list;
	int length = 0;
	while (list != NULL) {
		length += 1;
		list = list->next;
	}
	return makeExprNumber(length);
}

const struct Expr* callableDo(const struct List* args, const struct Expr* param, struct Env* env) {
	const struct Expr* result;
	while (args != NULL) {
		result = eval(args->expr, env);
		args = args->next;
	}
	return result;
}

const struct Expr* callableIf(const struct List* args, const struct Expr* param, struct Env* env) {
	bool condition = eval(args->expr, env)->boolean;
	const struct Expr* result = condition ? args->next->expr : args->next->next->expr;
	return eval(result, env);
}

const struct Expr* callablePrint(const struct List* args, const struct Expr* param, struct Env* env) {
	printf("Not Implemented");
	exit(1);
}

const struct Expr* callableLambda(const struct List* args, const struct Expr* param, struct Env* env) {
	return makeExprLambda(args->expr->list, args->next->expr, env);
}

const struct Expr* callableParam(const struct List* args, const struct Expr* param, struct Env* env) {
	if (param->kind == LIST) {
		// TODO test case?
		printf("Unexpected\n");
		exit(1);
	} else if (param->kind == LAMBDA) {
		return evalLambda(param->lambda, args, env);
	} else {
		return param;
	}
}

const struct Expr* callableDef(const struct List* args, const struct Expr* param, struct Env* env) {
	czstring name = args->expr->symbol;
	const struct Expr* result = eval(args->next->expr, env);
	env->map = makeMap(name, callableParam, result, env->map);
	return result;
}

struct Env* envDefault() {
	struct Env* env = gcAlloc(sizeof(struct Env), ENV_N_POINTERS);
	env->map = NULL;
	env->parent = NULL;
	env->map = makeMap("zero", callableParam, makeExprNumber(0.0), env->map);
	env->map = makeMap("one", callableParam, makeExprNumber(1.0), env->map);
	env->map = makeMap("+", callableAdd, NULL, env->map);
	env->map = makeMap("*", callableMul, NULL, env->map);
	env->map = makeMap("-", callableSub, NULL, env->map);
	env->map = makeMap("/", callableDiv, NULL, env->map);
	env->map = makeMap("<=", callableLowerThanEqual, NULL, env->map);
	env->map = makeMap("sin", callableSin, NULL, env->map);
	env->map = makeMap("cos", callableCos, NULL, env->map);
	env->map = makeMap("pi", callableParam, makeExprNumber(3.141592653589793), env->map);
	env->map = makeMap("quote", callableQuote, NULL, env->map);
	env->map = makeMap("length", callableLength, NULL, env->map);
	env->map = makeMap("do", callableDo, NULL, env->map);
	env->map = makeMap("if", callableIf, NULL, env->map);
	env->map = makeMap("def", callableDef, NULL, env->map);
	env->map = makeMap("lambda", callableLambda, NULL, env->map);
	return env;
}

// Main

int main(int argc, czstring const argv[]) {
	const char* command = argc >= 2 ? argv[1] : "";
	const char* parameter = argc >= 3 ? argv[2] : "";

	ASSERT(sizeof(struct Lambda) == (3 * 8)); // only pointers
	ASSERT(sizeof(struct List) == (2 * 8)); // only pointers
	ASSERT(sizeof(struct Expr) == (8 + 8 + 8)); // pointers + values + enum
	ASSERT(sizeof(struct Map) == (4 * 8)); // only pointers
	ASSERT(sizeof(struct Env) == (2 * 8)); // only pointers

	gcInit();

	if (strEqual(command, "status") && strEqual(parameter, "who")) {
		printf("Mimicry C\n");
	} else if (strEqual(command, "selftest")) {
		ASSERT(strMatch("123.4", IS_NUMBER, LENGTH(IS_NUMBER)) == 5);
		ASSERT(strMatch("hello", IS_SYMBOL, LENGTH(IS_SYMBOL)) == 5);
		ASSERT(strMatch("+*-/<>=", IS_SYMBOL, LENGTH(IS_SYMBOL)) == 7);
		ASSERT(strMatch("", IS_SYMBOL, LENGTH(IS_SYMBOL)) == 0);

		gcStat();

		struct Env* env = envDefault();

		gcStat();

		const struct Expr* t1 = makeExprNumber(1.0); // 1 alloc
		const struct Expr* t2 = makeExprNumber(2.0);
		const struct Expr* t3 = read("(1 2)"); // 5 alloc = 2 expr number + 1 expr list + 2 list
		const struct Expr* t4 = read("(3 4)");
		const struct Expr* t5 = read("(do (def add (lambda (a b) (+ a b))) (add 3 4))");
		const struct Expr* t6 = eval(t5, env);

		(void) t1;
		(void) t2;
		(void) t3;
		(void) t4;

		//gcMark(env);
		//gcMark(t1);
		//gcMark(t2);
		//gcMark(t3);
		//gcMark(t4);
		//gcMark(t5);
		gcMark(t6);

		gcStat();

		gcSweep();

		printf("%g\n", t6->number);

		gcStat();

		// symbol

		ASSERT(makeExprSymbol("s")->symbol[0] == 's');

		const struct Expr* readSymbol = read("tru");
		ASSERT(readSymbol->kind == SYMBOL);
		ASSERT(strEqual(readSymbol->symbol, "tru"));

		// number

		ASSERT(makeExprNumber(132)->number == 132);

		const struct Expr* readNumber = read("123");
		ASSERT(readNumber->kind == NUMBER);
		const double epsilon = 1e-6;
		ASSERT(fabs(readNumber->number - 123.0) < epsilon);

		// boolean

		ASSERT(makeExprBoolean(true)->boolean == true);
		ASSERT(makeExprBoolean(false)->boolean == false);

		const struct Expr* readTrue = read("true ");
		ASSERT(readTrue->kind == BOOLEAN);
		ASSERT(readTrue->boolean == true);

		const struct Expr* readFalse = read("false ");
		ASSERT(readFalse->kind == BOOLEAN);
		ASSERT(readFalse->boolean == false);

		// list

		const struct Expr* readList = read("(1 true pi)");
		ASSERT(readList->kind == LIST);
		ASSERT(readList->list->expr->kind == NUMBER);
		ASSERT(readList->list->expr->number == 1);
		ASSERT(readList->list->next->expr->kind == BOOLEAN);
		ASSERT(readList->list->next->expr->boolean == true);
		ASSERT(readList->list->next->next->expr->kind == SYMBOL);
		ASSERT(strEqual(readList->list->next->next->expr->symbol, "pi"));
		ASSERT(readList->list->next->next->next == NULL);
	} else if (strEqual(command, "run")) {
		struct Env* env = envDefault();
		print(eval(read(parameter), env));
	} else if (strEqual(command, "measure")) {
		const struct Expr* exprRead = read(parameter);
		clock_t start = clock();
		double sum = 0.0;
		for (int i = 0; i < 1000; i += 1) {
			struct Env* env = envDefault();
			const struct Expr* exprEval = eval(exprRead, env);
			sum += exprEval->number;
		}
		clock_t stop = clock();
		double ms = 1000.0 * (float)(stop - start) / CLOCKS_PER_SEC;
		printf("(%g %g)", ms, sum);
	}

	return 0;
}
