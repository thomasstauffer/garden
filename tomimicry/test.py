#!/usr/bin/env python3

import sys
import subprocess

EDITIONS = {
	'JS': ['node', 'mimicry-node.js'],
	'C++': ['./mimicry-cpp'],
	'Nim': ['./mimicry-nim'],
	'Go': ['./mimicry-go'],
#	'Rust': ['./mimicry-rust-src/target/debug/mimicry-rust'],
#	'Haskell': ['./mimicry-haskell'], # Purescript?
#	'Clojure': ['clojure', 'mimicry.clj'],
#	'Racket': ['racket', 'mimicry.rkt'],
#	'Forth/Factor': '...',
#	'Julia': '... mimicry.jl',
#	'F#/Ocaml': '...',
#	'Io': '...',
#	'Smalltalk/Squeak/RSqueak': '...'
#	'LLVM': '...'
#	'RPython/PyPy': '...'
#	'Erlang': '... mimicry.erl' # ['erl', '-noshell', '-s', 'mimicry', 'main', '-s', 'init', 'stop']
#	'Scala': '...'
#	'Prolog/SWIProlog': '...'
#	'Clean/Idris': '...' (uniqueness types?)
}

TEST_CASES = [
	# number
	['1', 1],
	# boolean
	['true', True],
	['false', False],
	# math
	['(+ 1)', 1],
	['(+ 1.1)', 1.1],
	['(+ 1.1 2.2)', 1.1 + 2.2],
	['(+ 1.1 2.2 3.3)', 1.1 + 2.2 + 3.3],
	[' (  +   1    2     3)      ', 6],
	['(+ 1 (+ 2 3) (+ 4 5))', 1 + 2 + 3 + 4 + 5],
	['(* 3 4)', 3 * 4],
	['(- 5 3)', 5 - 3],
	['(/ 24.0 4.0 3.0)', 24.0 / 4.0 / 3.0],
	# comparison
	['(<= 1 0)', False],
	['(<= 1 1)', True],
	['(<= 1 2)', True],
	# internal variables
	['zero', 0],
	['one', 1],
	['(zero)', 0],
	['(one)', 1], # (1) # gosh -> invalid application
	['(+ zero one one one)', 0 + 1 + 1 + 1],
	# internals functions
	['(sin 3.14159265359)', 0.0],
	['(sin pi)', 0.0],
	['(cos 3.14159265359)', -1.0],
	# quote
	['(length (quote ()))', 0],
	['(length (quote (1)))', 1],
	['(length (quote (1 2)))', 2],
	['(length (quote (1 2 3 4 5 6)))', 6],
	# multiple expressions
	['(do (+ 1 1) (+ 2 2) (+ 3 3))', 3 + 3],
	['(do (+ 1 1))', 1 + 1],
	# if
	['(if (<= 1 2) (+ 3 3) (+ 4 4))', 3 + 3],
	['(if (<= 3 2) (+ 3 3) (+ 4 4))', 4 + 4],
	# define variables
	['(def x (+ 1 2))', 1 + 2],
	['(do (def e 2.718) (+ e))', 2.718],
	['(do (def e 2.718) (e))', 2.718], # gosh -> invalid application
	['(do (def e 2.718) e)', 2.718],
	['(do (def x (+ 1 2)) (+ x))', 3],
	# define and execute lambdas
	['((lambda (a b) (+ a b)) 1 2)', 3],
	['((lambda () 1))', 1],
	# following examples may likely leak memory if not properly garbage collected
	['(do (def add (lambda (a b) (+ a b))) (add 3 4))', 7],
	['(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 5))', 5 * 4 * 3 * 2 * 1],
	['(do (def x 1) (def f (lambda () (do (def x 2) x))) (f))', 2],
	# lexical scoping
	['(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) (f)))))', 8],
	['(do (def x 8) (def f (lambda () x)) ((lambda () (do (def x 16) x))))', 16],
]

# ((if (sum_else_product) + *) x 1)
# invalid ones? test return code?

def execute(*args):
	return subprocess.check_output(list(args)).decode().strip()

def str_to_bool(value):
	if (value != 'true') and (value != 'false'):
		raise ValueError('Invalid Boolean', value)
	return value == 'true'

def who():
	for (edition, command) in EDITIONS.items():
		print(execute(*command, 'status', 'who').strip('"'))

MEASURE_CASES = [
	'(+ 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3 1.1 2.2 3.3)',
	'(do (def fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))) (fact 100))', # reliable until 169
]

def measure():
	for case in MEASURE_CASES:
		print(case)
	for (edition, command) in EDITIONS.items():
			result = execute(*command, 'measure', case)
		print(edition, result)

def test():
	for (edition, command) in EDITIONS.items():
		for case in TEST_CASES:
			expr, expected = case
			result = execute(*command, 'run', expr)
			failed = True
			if (type(expected) == float) or (type(expected) == int):
				try:
					if abs(float(result) - expected) < 1.0e-6:
						failed = False
				except ValueError as e:
					print(e)
			elif type(expected) == bool:
				if str_to_bool(result) == expected:
					failed = False
			else:
				if result == str(expected):
					failed = False
			if failed:
				print('Failed', expr, '->', result, ' Expected ', expected)

if __name__ == '__main__':
	command = 'who'
	if len(sys.argv) > 1:
		command = sys.argv[1]
	globals()[command]()
