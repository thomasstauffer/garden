
/*
clang -emit-llvm -S mimicry-llvm.c -o mimicry-llvm.ll

TODO

clang -cc1 mimicry-llvm.c -emit-llvm # -ast-print

llvm-as mimicry-llvm.ll –o mimicry-llvm.bc

// https://aransentin.github.io/cwasm/

clang --target=wasm32-unknown-wasi --sysroot /tmp/wasi-libc -O2 -s -o mimicry.wasm mimicry.c

clang-8 --target=wasm32 -O1 -S mimicry-llvm.wasm mimicry-llvm.c

clang-8 -emit-llvm --target=wasm32 -O1 -S mimicry-llvm.c
llc-8 -O1 -filetype=obj mimicry-llvm.ll -o mimicry-llvm.o
wasm-ld-8 --no-entry mimicry-llvm.o -o output.wasm


https://packages.debian.org/sid/amd64/wabt/download

wasm2c
wasm2wat
wat2wasm


*/

double add(double a, double b) {
	return a + b;
}
