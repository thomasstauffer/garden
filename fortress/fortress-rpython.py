#!/usr/bin/env python3

import sys

'''
https://pypy.org/download.html
https://doc.pypy.org/en/release-2.4.x/getting-started-dev.html
https://tratt.net/laurie/blog/entries/fast_enough_vms_in_fast_enough_time.html
https://morepypy.blogspot.com/2011/04/tutorial-writing-interpreter-with-pypy.html
https://morepypy.blogspot.com/2011/04/tutorial-part-2-adding-jit.html
'''

#SOURCE = '11 5 + dup * 3 3 = 2 1 ? 4 3 % 4 3 - + + /'
#SOURCE = '111 10 1 do I . loop'
#SOURCE = '0 1000000 1 do 4 I 2 % 0 = -1 1 ? 1 I 2 * 1 - / * * + loop'

def debug(message):
	print(message)

def fortress(source):
	stack = []
	# [index, loop index, loop counter]
	loopstack = []
	tokens = source.split(' ')
	index = 0
	while index < len(tokens):
		token = tokens[index]
		if token == 'do':
			loopstack.append([index + 1, stack.pop(), stack.pop()])
			index += 1
		elif token == 'loop':
			if loopstack[-1][1] < loopstack[-1][2]:
				loopstack[-1][1] += 1
				index = int(loopstack[-1][0])
			else:
				loopstack.pop()
				index += 1
		elif token == 'dup':
			stack.extend(2 * [stack.pop()])
			index += 1
		elif token == '.':
			debug(stack[-1])
			index += 1
		elif token == '?':
			when_false, when_true, condition = stack.pop(), stack.pop(), stack.pop()
			stack.append(when_true if condition else when_false)
			index += 1
		elif token == '+':
			b, a = stack.pop(), stack.pop()
			stack.append(a + b)
			index += 1
		elif token == '-':
			b, a = stack.pop(), stack.pop()
			stack.append(a - b)
			index += 1
		elif token == '*':
			b, a = stack.pop(), stack.pop()
			stack.append(a * b)
			index += 1
		elif token == '/':
			b, a = stack.pop(), stack.pop()
			stack.append(a / b)
			index += 1
		elif token == '%':
			b, a = stack.pop(), stack.pop()
			x = int(a / b) * b
			stack.append(a - x)
			index += 1
		elif token == '=':
			b, a = stack.pop(), stack.pop()
			stack.append(a == b)
			index += 1
		elif token == 'I':
			stack.append(loopstack[-1][1])
			index += 1
		elif token == 'true':
			stack.append(True)
			index += 1
		elif token == 'false':
			stack.append(False)
			index += 1
		else:
			stack.append(float(token))
			index += 1
	return stack[-1]

def entry_point(argv):
	debug(fortress(argv[1]))
	return 0

def target(*args):
	return entry_point

if __name__ == '__main__':
	entry_point(sys.argv)

