
import * as fs from 'fs';

function main() {
	const wasm = fs.readFileSync('mimicry-wasm.wasm');

	console.log(typeof wasm);

	WebAssembly.instantiate(wasm).then(obj =>
		console.log(obj.instance.exports.run(42))
	);
}

main();
