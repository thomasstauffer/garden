
const AST = ['+', 5, ['*', 2, 3]];

function evaluate(ast) {
	if(typeof ast === 'number') {
		return ast;
	}
	const params = ast.slice(1).map($ => evaluate($));
	if(ast[0] === '+') {
		return params.reduce((acc, $) => acc + $, 0);
	} else if(ast[0] === '*') {
		return params.reduce((acc, $) => acc * $, 1);
	}
}

function toWAT(ast) {
	if(typeof ast === 'number') {
		return 'i32.const ' + ast + '\n';
	}
	const params = ast.slice(1).map($ => toWAT($)).join('');
	if(ast[0] === '+') {
		return params + 'call $add\n';
	} else if(ast[0] === '*') {
		return params + 'call $mul\n';
	}
}

function main() {
	evaluate(AST);

	const code = toWAT(AST);
	const wat = `
	(module
		(func $add (param $lhs i32) (param $rhs i32) (result i32)
			local.get $lhs
			local.get $rhs
			i32.add)
		(func $mul (param $lhs i32) (param $rhs i32) (result i32)
			local.get $lhs
			local.get $rhs
			i32.mul)
		(func $runMy (param $lhs i32) (result i32)
			i32.const 42
			i32.const 42
			call $mul)
		(func $run (param $lhs i32) (result i32)
			${code}
		)
		(export "run" (func $run))
	  )
	`;
	console.log(wat);
}

main();
