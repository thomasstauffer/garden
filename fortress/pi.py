#!/usr/bin/env python3

import math
import time
import sys

print(math.pi)
sys.setrecursionlimit(10000)

# converges absurdly slow
def piiter(n):
	result = 0.0
	for i in range(n, 0, -1):
		part = 4.0 * (-1 if i % 2 == 0 else 1) * (1.0 / (i * 2.0 - 1.0))
		result += part
	return result

def pirec(n):
	part = 4.0 * (-1 if n % 2 == 0 else 1) * (1.0 / (n * 2.0 - 1.0))
	if n > 1:
		return part + pirec(n - 1)
	else:
		return part

t1 = time.time()
print(piiter(1000))
print(pirec(1000))
t2 = time.time()
print(t2 - t1)
