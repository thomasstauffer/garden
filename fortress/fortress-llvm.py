"""

Targets

IR -> JIT
IR -> x86
IR -> wasm

pip3 install llvmlite

LLVM-Lite

- http://llvmlite.pydata.org/en/latest/index.html
- https://subscription.packtpub.com/book/application_development/9781785285981

LLVM

- https://llvm.org/docs/GarbageCollection.html

Examples:

- https://llvm.org/docs/tutorial/
- https://github.com/eliben/pykaleidoscope

"""

import re
import ctypes

from llvmlite import ir
import llvmlite.binding as llvm

def compile(expr):
	type_number = ir.DoubleType()
	type_fnnn = ir.FunctionType(type_number, (type_number, type_number))
	module = ir.Module(name='mimicry')

	fadd = ir.Function(module, type_fnnn, name='add')

	block = fadd.append_basic_block(name='entry')
	builder = ir.IRBuilder(block)
	a, b = fadd.args
	result = builder.fadd(a, b, name='res')
	builder.ret(result)

	assembly = str(module)

	print(assembly)

	return assembly

def make(assembly):
	llvm.initialize()
	llvm.initialize_native_target()
	llvm.initialize_native_asmprinter()

	module = llvm.parse_assembly(assembly)
	module.verify()

	target = llvm.Target.from_default_triple()
	target_machine = target.create_target_machine()
	module_empty = llvm.parse_assembly('')

	engine = llvm.create_mcjit_compiler(module_empty, target_machine)
	engine.add_module(module)
	engine.finalize_object()
	engine.run_static_constructors()

	return engine

def run(engine, function):
	function_pointer = engine.get_function_address('add')
	assert function_pointer != 0
	function = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double, ctypes.c_double)(function_pointer)
	res = function(1.0, 3.5)
	print('=', res)

assembly = compile(read('+ 1 2'))
engine = make(assembly)
run(engine, 'add')
