// TODO why does function(){ CODE }()); not work?

//////// ? ////////

"use strict";

//////// Test ////////

var Test = {};

Test.equal = function (left, right) {
	if ((typeof left !== typeof right) || (left !== right)) {
		console.log('Test Failed ' + left + ' <> ' + right);
		console.log(new Error().stack);
	}
};

Test.equal(1, 1);
Test.equal('a', 'a');

Test.test = function (condition) {
	if ((typeof condition !== 'boolean') || (!condition)) {
		console.log('Test Failed');
		console.log(new Error().stack);
	}
};

Test.test(true);

//////// Generic Functions ////////

/*
function debug(value) {
	var element = $('#Debug');
	element.html(value + '<br>' + element.html());
	console.log(value);
}

$(function () {
	var html = '<p id="Debug" style="background-color: #ccccff; border: 1px solid #9999ff;"></p>';
	$('body').prepend(html);
});
*/

Math.roundTo = function (value, factor) {
	return Math.round(value / factor) * factor;
};

Test.equal(Math.roundTo(0.06, 0.05), 0.05);
Test.equal(Math.roundTo(0.04, 0.05), 0.05);
Test.equal(Math.roundTo(0.02, 0.05), 0.00);

Math.randomFromTo = function (from, to) {
	return Math.floor(Math.random() * (to - from + 1) + from);
};

for(var i = 0; i < 10; i++) {
	var number = Math.randomFromTo(2, 5);
	Test.test((number >= 2) && (number <= 5));
}

String.prototype.format = function () {
	var args = arguments;
	return this.replace(/\{(\d+)\}/g, function (match, number) {
		return args[number] === undefined ? match : args[number];
	});
};

Test.equal('{0}{0}{0}-{1}{1}{1}'.format(6, 3 * 3), '666-999');

String.prototype.count = function (what) {
	var result = this.match(new RegExp(what, 'g'));
	if (result === null) {
		return 0;
	} else {
		return result.length;
	}
};

Test.equal('1-2-3-4-5'.count('-'), 4);

String.prototype.repeat = function (times) {
	var array = [];
	array.length = times + 1;
	return array.join(this);
};

Test.equal('X'.repeat(0), '');
Test.equal('X'.repeat(1), 'X');
Test.equal('X'.repeat(5), 'XXXXX');

//////// Generic Classes ////////

var Tom = {};

//////// Audio ////////

Tom.Audio = function (fileName) {
	this.audio = new Audio(fileName);
	this.audio.load();
};

Tom.Audio.prototype.play = function () {
	this.audio.pause();
	this.audio.currentTime = 0;
	this.audio.play();
};

//////// Sinus ////////

Tom.Sinus = function () {
	this.audio = new Audio();
	this.audio.mozSetup(1, 44100);
};

Tom.Sinus.prototype.play = function (frequency) {
	var samples = [];
	var sampleRate = 44100.0;
	for (var i = 0; i < (sampleRate / 4.0); i += 1) {
		var t = Math.PI * 2.0 * (i / sampleRate);
		var sample = Math.sin(frequency * t);
		samples.push(sample * 0.1);
	}
	this.audio.mozWriteAudio(samples);
};

//////// Instrument ////////

//////// Metronome ////////

Tom.Metronome = function () {
	this.lo = new Tom.Audio('MetronomeLo.wav');
	this.hi = new Tom.Audio('MetronomeHi.wav');
	this.counter = 0;
	this.timer = null;
};

Tom.Metronome.prototype.timeout = function () {
	var ts = this.signature;
	if ((this.counter % ts) === 0) {
		this.hi.play();
	} else {
		this.lo.play();
	}
	this.counter += 1;
};

Tom.Metronome.prototype.setTempo = function (tempo) {
	this.tempo = tempo;
};

Tom.Metronome.prototype.setSignature = function (signature) {
	this.signature = signature;
};

Tom.Metronome.prototype.off = function () {
	window.clearInterval(this.timer);
	this.timer = null;
	this.counter = 0;
};

Tom.Metronome.prototype.on = function () {
	this.off();
	var intervalMilliseconds = 1000.0 * 60.0 / this.tempo;
	if ((intervalMilliseconds >= 100.0) && (intervalMilliseconds <= 10000.0)) {
		this.timer = window.setInterval(this.timeout.bind(this), intervalMilliseconds);
	}
};

Tom.Metronome.prototype.toggle = function () {
	if (this.timer === null) {
		this.on();
	} else {
		this.off();
	}
};

//////// Tap ////////

Tom.Tap = function () {
	this.lastMs = 0.0;
	this.tapAverage = 0.0;
};

Tom.Tap.prototype.measure = function () {
	var ms = Date.now();
	var dms = ms - this.lastMs;
	this.lastMs = ms;
	if ((dms > 20.0) && (dms < 2000.0)) {
		//this.tapAverage = dms
		var quotient = dms / this.tapAverage;
		if ((quotient < 0.5) || (quotient > 2.0)) {
			this.tapAverage = dms;
		} else {
			this.tapAverage += (dms - this.tapAverage) / 10.0;
		}
		var bpm = 1000.0 * 60.0 / this.tapAverage;
		return Math.round(bpm);
	}
	return undefined;
};

//////// Scales ////////

/*
 http://pianoencyclopedia.com/scales/
 http://www.pluck-n-play.com/en/scale-list_2.html
 http://academic.udayton.edu/PhillipMagnuson/soundpatterns/scales/
 http://www.chubey.com/exotic-scal.html
 http://txstateprojects.googlecode.com/svn/trunk/MusicAnalysis/scaleslib.py
 */

Tom.Scales = {
	// Pentatonic

	'Pentatonic Major': [2, 2, 3, 2, 3],
	'Pentatonic Minor': [3, 2, 2, 3, 2],
	'Pentatonic Scottish': [2, 3, 2, 2, 3],
	'Pentatonic Rock': [3, 2, 3, 2, 2],
	'Pentatonic Neutral': [2, 3, 2, 3, 2],

	// Hexatonic

	'Whole': [2, 2, 2, 2, 2, 2],

	'Blues Major': [2, 1, 1, 3, 2, 3],
	'Blues Minor': [3, 2, 1, 1, 3, 2],

	//'Tritone 1': [1, 3, 2, 1, 3, 2],
	// c c# e f# g a#
	//'Tritone 2': [1, 1, 4, 1, 1, 4],
	// c c# d f# g g#

	// Heptaonic

	'Ionian / Major': [2, 2, 1, 2, 2, 2, 1],
	'Dorian': [2, 1, 2, 2, 2, 1, 2],
	'Phrygian': [1, 2, 2, 2, 1, 2, 2],
	'Lydian': [2, 2, 2, 1, 2, 2, 1],
	'Mixolydian': [2, 2, 1, 2, 2, 1, 2],
	'Aeolian / Natural Minor': [2, 1, 2, 2, 1, 2, 2],
	'Harmonic Minor': [2, 1, 2, 2, 1, 3, 1],
	'Melodic Minor Ascending': [2, 1, 2, 2, 2, 2, 1],
	'Melodic Minor Descending': [2, 1, 2, 2, 1, 2, 2],
	'Locrian': [1, 2, 2, 1, 2, 2, 2],

	// Octatonic

	'Whole Half': [2, 1, 2, 1, 2, 1, 2, 1],
	'Half Whole': [1, 2, 1, 2, 1, 2, 1, 2],

	// Chromatic

	'Chromatic': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
};

Tom.Intervals = {
	0: 'Unison',
	1: 'Minor Second',
	2: 'Major Second',
	3: 'Minor Third',
	4: 'Major Third',
	5: 'Fourth',
	6: 'Augmented Fourth / Diminished Fifth',
	7: 'Fifth',
	8: 'Minor Sixth',
	9: 'Major Sixth',
	10: 'Minor Seventh',
	11: 'Major Seventh',
	12: 'Octave',
	13: 'Minor Ninth',
	14: 'Major Ninth',
	15: 'Minor Tenth',
	16: 'Major Tenth',
	17: 'Eleventh',
	18: 'Augmented Eleventh / Diminished Twelfth',
	19: 'Twelfth',
	20: 'Minor Thirteenth',
	21: 'Major Thirteenth',
	22: 'Minor Fourteenth',
	23: 'Major Fourteenth',
	24: 'Major Fifteenth'
};

//////// Notes ////////

Tom.Notes = {};

Tom.Notes.EnglishNames = ['C', 'C#/Db', 'D', 'D#/Eb', 'E', 'F', 'F#/Gb', 'G', 'G#/Ab', 'A', 'A#/Bb', 'B'];
Tom.Notes.Names = ['c', 'cis', 'd', 'dis', 'e', 'f', 'fis', 'g', 'gis', 'a', 'ais', 'b'];
Tom.Notes.PrettyNames = ['c', 'c♯', 'd', 'd♯', 'e', 'f', 'f♯', 'g', 'g♯', 'a', 'a♯', 'b'];

//Tom.Notes.Names1 = ['c', 'cis', 'd', 'dis', 'e', 'f', 'fis', 'g', 'gis', 'a', 'ais', 'b'];
//Tom.Notes.Names2 = ['c', 'des', 'd', 'ees', 'e', 'f', 'ges', 'g', 'aes', 'a', 'bes', 'b'];
//Tom.Notes.Full = [true, false, true, false, true, true, false, true, false, true, false, true];

Tom.Notes.AllNames = [['c'], ['cis', 'des'], ['d'], ['dis', 'ees'], ['e'], ['f'], ['fis', 'ges'], ['g'], ['gis', 'aes'], ['a'], ['ais', 'bes'], ['b']];

//Tom.Notes.Names = ['a', 'ais/bes', 'b', 'c', 'cis/des', 'd', 'dis/ees', 'e', 'f', 'fis/ges', 'g', 'gis/aes'];

Tom.Notes.fromMidiToOctave = function (midi) {
	var n = Math.floor(midi / 12);
	var octave = '';
	if (n < 5) {
		octave = ','.repeat(4 - n);
	} else {
		octave = '\''.repeat(n - 4);
	}
	return octave;
};


Tom.Notes.fromMidiToName = function (midi) {
	var octave = Tom.Notes.fromMidiToOctave(midi);
	return Tom.Notes.Names[(midi % 12)] + octave;
};

Test.equal(Tom.Notes.fromMidiToName(0), 'c,,,,');
Test.equal(Tom.Notes.fromMidiToName(45), 'a,');
Test.equal(Tom.Notes.fromMidiToName(57), 'a');
Test.equal(Tom.Notes.fromMidiToName(69), 'a\''); // 440 Hz

Tom.Notes.fromNameToMidi = function (name) {
	var result = name.match(/([a-z]+)([,']*)/);

	if (result === null) {
		return 69;
	}

	// TODO 'cisis' or 'des' does not work yet
	var midi = 48 + Tom.Notes.Names.indexOf(result[1]);

	var up = result[2].count('\'');
	var down = result[2].count(',');

	midi += 12 * (up - down);

	return midi;
};

Test.equal(Tom.Notes.fromNameToMidi('a,'), 45);
Test.equal(Tom.Notes.fromNameToMidi('a'), 57);
Test.equal(Tom.Notes.fromNameToMidi('a\''), 69);
for (var i = 0; i < 255; i++) {
	Test.equal(Tom.Notes.fromNameToMidi(Tom.Notes.fromMidiToName(i)), i);
}

Tom.Notes.fromMidiToEnglishName = function (midi) {
	return Tom.Notes.EnglishNames[(midi % 12)] + (Math.floor(midi / 12) - 1);
};

Test.equal(Tom.Notes.fromMidiToEnglishName(45), 'A2');
Test.equal(Tom.Notes.fromMidiToEnglishName(57), 'A3');
Test.equal(Tom.Notes.fromMidiToEnglishName(69), 'A4');

Tom.Notes.fromMidiToFrequency = function (midi) {
	var factor = Math.pow(2.0, 1.0 / 12.0);
	var base = 440.0 / Math.pow(factor, 69);
	return base * Math.pow(factor, midi);
};

Test.equal(Tom.Notes.fromMidiToFrequency(69), 440.0);

Tom.Notes.paint = function (canvas, notes) {
	var canvasContext = canvas.getContext('2d');

	canvasContext.clearRect(0, 0, canvas.width, canvas.height);

	var staveNotes = [];
	var beamNotes = [
		[]
	];

	notes.split(' ').forEach(function (item) {
		var result = item.match(/([abcdefgr])([a-z]*)([,']*)([0-9]*)(\.?)/);

		if (result === null) {
			return;
		}

		var noteName = result[1];
		if (noteName === '') {
			return;
		}

		var noteAcc = '';
		if (result[2] === 'is') {
			noteAcc = '#';
		} else if (result[2] === 'isis') {
			noteAcc = '##';
		} else if (result[2] === 'es') {
			noteAcc = 'b';
		} else if (result[2] === 'eses') {
			noteAcc = 'bb';
		}

		var noteOctDown = result[3].count(',');
		var noteOctUp = result[3].count('\'');
		var noteOct = 3 + noteOctUp - noteOctDown;

		var noteLen = parseInt(result[4], 10);
		if (isNaN(noteLen)) {
			noteLen = 1;
		}

		var noteDot = result[5] === '.';

		if (noteName === 'r') {
			noteLen += 'r';
		}
		var n = new Vex.Flow.StaveNote({ keys: [noteName + '/' + noteOct], duration: noteLen + 'd' });
		if (noteAcc !== '') {
			n.addAccidental(0, new Vex.Flow.Accidental(noteAcc));
		}
		if (noteDot) {
			n.addDotToAll();
		}

		if (noteLen >= 8) {
			beamNotes[beamNotes.length - 1].push(n);
		} else {
			beamNotes.push([]);
		}

		staveNotes.push(n);
	});

	var beams = [];
	beamNotes.forEach(function (item) {
		if (item.length > 1) {
			beams.push(new Vex.Flow.Beam(item));
		}
	});

	var voice = new Vex.Flow.Voice(Vex.Flow.TIME4_4);
	voice.setMode(Vex.Flow.Voice.Mode.SOFT);
	voice.addTickables(staveNotes);

	var formatter = new Vex.Flow.Formatter();
	formatter.joinVoices([voice]);
	formatter.format([voice]);

	var width = formatter.getMinTotalWidth();
	canvas.width = width + 50 + 20;

	var renderer = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
	var context = renderer.getContext();
	var stave = new Vex.Flow.Stave(10, 0, width + 50);
	stave.addClef('treble');
	stave.setContext(context).draw();



	voice.draw(context, stave);
	beams.forEach(function (item) {
		item.setContext(context).draw();
	});
};

Tom.Notes.paintKeyboard = function (canvas, notes) {
	var keys = 12 * 2;

	var context = canvas.getContext('2d');
	canvas.width = 1.0 + (Math.ceil(keys / 12.0) * 7 * 15.0);
	canvas.height = 1.0 + (50.0);
	//context.clearRect(0, 0, canvas.width, canvas.height);

	var isEbony = [true, false, true, false, true, true, false, true, false, true, false, true];
	var distance = [0.0, 0.5, 1.0, 1.5, 2.0, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0];

	context.lineWidth = 1.0;
	context.strokeStyle = '#000';
	context.fillStyle = '#000';
	for (var i = 0; i < keys; i += 1) {
		var f = Math.floor(i / 12.0) * 7.0;
		if (isEbony[i % 12]) {
			context.strokeRect(0.5 + ((distance[i % 12] + f) * 15.0), 0.5, 15.0, 50.0);
		} else {
			context.fillRect(((distance[i % 12] + f) * 15.0) + 2.5, 0.0, 10.0, 30.0);
		}
	}

	context.fillStyle = '#f00';
	for (var i = 0; i < keys; i += 1) {
		if (notes.indexOf(i) >= 0) {
			context.beginPath();
			var f = Math.floor(i / 12.0) * 7.0;
			if (isEbony[i % 12]) {
				context.arc(7.5 + ((distance[i % 12] + f) * 15.0), 40.0, 4.0, 0.0, Math.PI * 2.0, true);
			} else {
				context.arc(7.5 + ((distance[i % 12] + f) * 15.0), 20.0, 4.0, 0.0, Math.PI * 2.0, true);
			}
			context.closePath();
			context.fill();
		}
	}
};

Tom.Notes.midisToChord = function (midis) {
	var base = midis[0];

	var result = '';
	result += Tom.Notes.PrettyNames[base % 12].toUpperCase();

	// create relative array

	var relatives = [];
	for (var i = 1; i < midis.length; i++) {
		relatives.push(midis[i] - midis[0]);
	}

	// create interval array

	var intervals = [];
	for (var i = 0; i < (midis.length - 1); i++) {
		intervals.push((midis[i + 1] - midis[i]));
	}

	var relativesToChord = {
		'3-6': '°',
		'3-7': 'm',
		//'3-8': '♯5',
		//'4-6': '♭5',
		'4-7': '',
		'4-8': '+',

		//'3-6-9': '°7',
		//'3-6-10': 'Ø7',
		//'3-6-11': 'mM7',
		'3-6-9': '°7',
		'3-6-10': 'Ø7♭5',
		'3-7-9': 'm6',
		'3-7-10': 'm7',
		'3-7-11': 'mM7',
		'4-7-9': '6',
		'4-7-10': '7',
		'4-7-11': 'M7',
		//'4-8-9': '6♯5',
		//'4-8-10': '+7',
		//'4-8-11': '+M7',

		'3-7-10-14': 'm9',
		'4-7-10-14': '9',

		'3-7-10-14-17': 'm11',
		'4-7-10-14-17': '11',

		'3-7-10-14-21': 'm13',
		'4-7-10-14-21': '13'
	};

	result += relativesToChord[relatives.join('-')] + ' ';


	return result; //+ ' ' + intervals + ' ' + relatives;
};
