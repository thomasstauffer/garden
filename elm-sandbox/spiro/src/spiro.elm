import Browser
import Html
import Html.Attributes
import Html.Events
import Svg
import Svg.Attributes exposing (..)
import Svg.Events
import Random
import Time
import Color

main = Browser.element { init = init, update = update, subscriptions = subscriptions, view = view }

type alias Model = {counter : Int, number : Int, time : Int, frameRadius : Float, wheelRadius : Float, penRadius : Float}

init : () -> (Model, Cmd Msg)
init _ = ({ counter = 0, number = 0, time = 1, frameRadius = 400.0, wheelRadius = 250.0, penRadius = 200.0 }, Cmd.none)

type Msg = CounterInc | CreateRandom | RandomGenerated Int | Tick Time.Posix | FrameRadiusSet String | WheelRadiusSet String | WheelRadiusAdd Float | PenRadiusAdd Float

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    CounterInc ->
      ({ model | counter = model.counter + 1 }, Cmd.none)
    CreateRandom ->
      (model, Random.generate RandomGenerated (Random.int 0 100))
    RandomGenerated number ->
      ({ model | number = number }, Cmd.none)
    FrameRadiusSet value ->
      ({ model | frameRadius = (String.toFloat value) |> Maybe.withDefault 0 }, Cmd.none)
    WheelRadiusSet value ->
      ({ model | wheelRadius = (String.toFloat value) |> Maybe.withDefault 0 }, Cmd.none)
    WheelRadiusAdd delta ->
      ({ model | wheelRadius = model.wheelRadius + delta }, Cmd.none)
    PenRadiusAdd delta ->
      ({ model | penRadius = model.penRadius + delta }, Cmd.none)
    Tick time ->
      ({ model | time = model.time + 1 }, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model = Time.every 20 Tick

width = 1600.0
height = 800.0

svgLine : Float -> Float -> Float -> Float -> String -> Svg.Svg Msg
svgLine x1 y1 x2 y2 c = Svg.line [
  Svg.Attributes.x1 (String.fromFloat (x1 + (width / 2))), Svg.Attributes.y1 (String.fromFloat (y1 + (height / 2))),
  Svg.Attributes.x2 (String.fromFloat (x2 + (width / 2))), Svg.Attributes.y2 (String.fromFloat (y2 + (height / 2))),
  strokeWidth "2", stroke c] []

svgCircle : Float -> Float -> Float -> String -> Svg.Svg Msg
svgCircle x y r c = Svg.circle [Svg.Attributes.cx (String.fromFloat (x + (width / 2))), Svg.Attributes.cy (String.fromFloat (y + (height / 2))),
  Svg.Attributes.r (String.fromFloat r), fill c, strokeWidth "5", color "#f0f"] []

svgPoint : Float -> Float -> String -> Svg.Svg Msg
svgPoint x y c = svgCircle x y 3 c

tFactor = 0.05

wheelAlpha : Int -> Float
wheelAlpha time = (toFloat time) * tFactor

wheelPos : Int -> Float -> Float -> (Float, Float)
wheelPos time frameRadius wheelRadius =
  let
    alpha = wheelAlpha time
    x = (cos alpha) * (frameRadius - wheelRadius)
    y = (sin alpha) * (frameRadius - wheelRadius)
  in
    (x, y)

penPos : Int -> Float -> Float -> Float -> (Float, Float)
penPos time frameRadius wheelRadius penRadius =
  let
    (wheelX, wheelY) = wheelPos time frameRadius wheelRadius
    distance = (wheelAlpha time) * frameRadius
    penAlpha = -distance / wheelRadius
    penX = (cos penAlpha) * penRadius
    penY = (sin penAlpha) * penRadius
  in
    (wheelX + penX, wheelY + penY)

penLine : Int -> Float -> Float -> Float -> String -> Svg.Svg Msg
penLine time frameRadius wheelRadius penRadius color =
  let
    (x1, y1) = penPos time frameRadius wheelRadius penRadius
    (x2, y2) = penPos (time - 1) frameRadius wheelRadius penRadius
  in
    svgLine x1 y1 x2 y2 color

rainbowColor : Int -> String
rainbowColor value =
  let
    hue = (toFloat (modBy 100 (value+10000))) / 100.0
  in
    Color.toCssString (Color.hsla 0.7 1.0 0.5 0.5)

penTrail : Int -> Float -> Float -> Float -> String -> List (Svg.Svg Msg)
penTrail time frameRadius wheelRadius penRadius color =
  let
    times = List.range (time - 500) time
  in
    List.map (\t -> penLine t frameRadius wheelRadius penRadius (rainbowColor t)) times

view : Model -> Html.Html Msg
view model =
  let
    (sx, sy) = wheelPos model.time model.frameRadius model.wheelRadius
    (px, py) = penPos model.time model.frameRadius model.wheelRadius model.penRadius
    lines = penTrail model.time model.frameRadius model.wheelRadius model.penRadius "#00f3"
  in
    Html.div [] [
      Html.p [] [
        Html.text "Frame Radius: ",
        Html.input [ Html.Attributes.value (String.fromFloat model.frameRadius), Html.Events.onInput FrameRadiusSet ] [],
        Html.text " Wheel Radius: ",
        Html.input [ Html.Attributes.value (String.fromFloat model.wheelRadius), Html.Events.onInput WheelRadiusSet ] [],
        Html.button [ Html.Events.onClick (WheelRadiusAdd -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (WheelRadiusAdd 1) ] [ Html.text "+" ],
        Html.text " Pen Radius: ",
        Html.text ((String.fromFloat model.penRadius) ++ " "),
        Html.button [ Html.Events.onClick (PenRadiusAdd -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (PenRadiusAdd 1) ] [ Html.text "+" ],
        Html.text " Time: ",
        Html.text ("Time: " ++ (String.fromInt model.time))
      ],
      Svg.svg [ Svg.Attributes.width (String.fromFloat width), Svg.Attributes.height (String.fromFloat height), Svg.Attributes.style "background-color: #eee" ] (
        [ svgCircle 0.0 0.0 model.frameRadius "#00f1" ] ++ [ svgCircle sx sy model.wheelRadius "#00f1" ] ++ lines ++ [ svgPoint px py "#00f" ]
      )
    ]
