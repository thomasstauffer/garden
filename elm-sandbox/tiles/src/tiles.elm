import Browser
import Html
import Html.Attributes
import Html.Events
import Svg
import Svg.Attributes exposing (..)
import Svg.Events
import Random
import Time
import Basics

main = Browser.element { init = init, update = update, subscriptions = subscriptions, view = view }

type alias Model = {time : Int, parameterA : Float, parameterB : Float, parameterC : Float}

init : () -> (Model, Cmd Msg)
init _ = ({ time = 1, parameterA = 5.0, parameterB = 4.0, parameterC = 3.0 }, Cmd.none)

type Msg = RandomGenerated Int | Tick Time.Posix | ParameterAChange Float | ParameterBChange Float | ParameterCChange Float

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    RandomGenerated number ->
      (model, Cmd.none)
    Tick time ->
      ({ model | time = model.time + 1 }, Cmd.none)
    ParameterAChange delta ->
      ({ model | parameterA = model.parameterA + delta }, Cmd.none)
    ParameterBChange delta ->
      ({ model | parameterB = model.parameterB + delta }, Cmd.none)
    ParameterCChange delta ->
      ({ model | parameterC = model.parameterC + delta }, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model = Time.every 20 Tick

width = 800.0
height = 800.0

svgLine : (Float, Float) -> (Float, Float) -> Svg.Svg Msg
svgLine (x1, y1) (x2, y2) = Svg.line [
    Svg.Attributes.x1 (String.fromFloat x1), Svg.Attributes.y1 (String.fromFloat y1),
    Svg.Attributes.x2 (String.fromFloat x2), Svg.Attributes.y2 (String.fromFloat y2),
    strokeWidth "2", stroke "#f0f"
  ] []

svgCircle : Float -> (Float, Float) -> Svg.Svg Msg
svgCircle r (x, y) = Svg.circle [Svg.Attributes.cx (String.fromFloat x), Svg.Attributes.cy (String.fromFloat y),
  Svg.Attributes.r (String.fromFloat r), fill "#f0f"] []

svgPoint : (Float, Float) -> Svg.Svg Msg
svgPoint = svgCircle 1

distanceConstant : Float -> Float
distanceConstant angle = 1

distanceSin : Float -> Float
distanceSin angle = (sin (angle * 20)) * 0.1 + 0.9

distanceSuper : Float -> Float -> Float ->Float
distanceSuper time param angle  =
  let
    m = time / 10
    m1 = m
    m2 = m
    n1 = 2
    n2 = 13
    n3 = 13
    a = 0.7
    b = 0.7
    c = abs ((cos ((m1 * angle) / 4)) / a)
    s = abs ((sin ((m2 * angle) / 4)) / b)
  in
    ((c ^ n2) + (s ^ n3)) ^ (-1 / n1)

distanceToPoint : (Float -> Float) -> Float -> (Float, Float)
distanceToPoint distanceFunction angle = ((cos angle) * (distanceFunction angle), (sin angle) * (distanceFunction angle))

pointLissajous : Float -> Float -> (Float, Float)
pointLissajous time angle = ((cos (3 * angle)) * 1, (sin (2 * angle)) * 1)

pointHypotrochoid : Float -> Float -> (Float, Float)
pointHypotrochoid time angle =
  let
    a = angle * 20.0
    rbig = time / 1000
    r = 4.0
    d = 5.0
    a2 = a * ((rbig - r) / r)
    x = (rbig - r) * (cos a) + (d * (cos a2))
    y = (rbig - r) * (sin a) - (d * (sin a2))
  in
    (x, y)

anglesUnitCircle : List Float
anglesUnitCircle = List.map (toFloat >> (*) 0.2 >> Basics.degrees) (List.range 0 (5 * 360))

scale : Float -> (Float, Float) -> (Float, Float)
scale factor (x, y) = (x * factor, y * factor)

translate : Float -> (Float, Float) -> (Float, Float)
translate offset (x, y) = (x + offset, y + offset)

squarify : (Float, Float) -> (Float, Float)
squarify (x, y) =
  let
    d = sqrt ((x ^ 2) + (y ^ 2))
    a = atan2 y x
    r1 = abs (1 / (cos a))
    r2 = abs (1 / (sin a))
    r = d * (Basics.min r1 r2)
  in
    (cos(a) * r, sin(a) * r)

autoScale : List (Float, Float) -> List (Float, Float)
autoScale points =
  let
    distance (x, y) = (sqrt ((x ^ 2) + (y ^ 2)))
    maxDistance = List.foldl (\point acc -> Basics.max (distance point) acc) 0.0 points
  in
    List.map (scale (1.0 / maxDistance)) points

--makePoints distanceFunction = List.map (\angle -> (svgPoint (translate 400 (scale 400 (toPoint distanceFunction angle))))) anglesUnitCircle

makePoints : (Float -> (Float, Float)) -> List (Svg.Svg Msg)
makePoints pointFunction = List.map (pointFunction >> scale 300 >> translate 400 >> svgPoint) anglesUnitCircle

makePointsAutoScale : (Float -> (Float, Float)) -> List (Svg.Svg Msg)
makePointsAutoScale pointFunction =
  let
    points = List.map pointFunction anglesUnitCircle |> autoScale
  in
    List.map (squarify >> scale 300 >> translate 400 >> svgPoint) points

makeLines : (Float -> (Float, Float)) -> List (Svg.Svg Msg)
makeLines pointFunction =
  let
    points = (List.map pointFunction anglesUnitCircle) |> autoScale
    points1 = List.map (scale 300 >> translate 400) points
    points2 = List.tail points1 |> Maybe.withDefault []
  in
    List.map2 svgLine points1 points2

view : Model -> Html.Html Msg
view model =
  let
    time = (toFloat model.time)
    --svgElements = makePoints (distanceToPoint distanceConstant time)
    --svgElements = makePointsAutoScale (distanceToPoint distanceSuper time)
    --svgElements = makeLines (distanceToPoint (distanceConstant))
    --svgElements = makeLines (distanceToPoint (distanceSin))
    --svgElements = makeLines (distanceToPoint (distanceSuper time model.parameterA))
    --svgElements = makeLines (pointLissajous time)
    svgElements = makeLines (pointHypotrochoid time)
  in
    Html.div [] [
      Html.p [] [
        Html.text (" A: " ++ String.fromFloat model.parameterA ++ " "),
        Html.button [ Html.Events.onClick (ParameterAChange -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (ParameterAChange 1) ] [ Html.text "+" ],
        Html.text (" B: " ++ String.fromFloat model.parameterB ++ " "),
        Html.button [ Html.Events.onClick (ParameterBChange -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (ParameterBChange 1) ] [ Html.text "+" ],
        Html.text (" C: " ++ String.fromFloat model.parameterC ++ " "),
        Html.button [ Html.Events.onClick (ParameterCChange -1) ] [ Html.text "-" ],
        Html.button [ Html.Events.onClick (ParameterCChange 1) ] [ Html.text "+" ],
        Html.text (" Time: " ++ (String.fromInt model.time))
      ],
      Svg.svg [
        Svg.Attributes.width (String.fromFloat width),
        Svg.Attributes.height (String.fromFloat height),
        Svg.Attributes.style "background-color: #eee"
      ] svgElements
    ]
