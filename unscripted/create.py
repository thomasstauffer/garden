#!/usr/bin/env python3

import collections
import csv
import os
import re
import subprocess
import sys

OUTPUT_PATH = './generated'
ICON_PATH = './icons'

ARROW = '→'

FRONT_INPUT_FILE_NAME = 'card-front.svg'
BACK_INPUT_FILE_NAME = 'card-back.svg'

FRONT_OUTPUT_FILE_NAME = os.path.join(OUTPUT_PATH, 'card-{number:03}-front.png')
BACK_OUTPUT_FILE_NAME = os.path.join(OUTPUT_PATH, 'card-{number:03}-back.png')

FRONT_TMP_FILE_NAME = os.path.join(OUTPUT_PATH, 'card-front.svg')
BACK_TMP_FILE_NAME = os.path.join(OUTPUT_PATH, 'card-back.svg')

DATA_SINGLE_FILE_NAME = 'cards-single.csv'
DATA_CONCEPTS_FILE_NAME = 'cards-concepts.csv'

RE_COMMENT = re.compile('#.+')

DPI = 600


def light_or_dark(color: str) -> str:
	# '#rrggbb'
	r = int(color[1:3], 16)
	g = int(color[3:5], 16)
	b = int(color[5:7], 16)

	rgb_max = max(r, g, b)
	rgb_min = min(r, g, b)
	l = (rgb_min + rgb_max) / 2.0

	if l < 100:
		return '#ffffff'
	else:
		return '#000000'


def black_or_red(suit: str) -> str:
	if (suit == '♥') or (suit == '♦'):
		return '#ff0000'
	else:
		return '#000000'


def linkify(s: str) -> str:
	s = s.replace('&', '&amp;')
	s = s.replace('<', '&lt;')
	s = s.replace('>', '&bg;')
	s = RE_COMMENT.sub('', s)
	s = s.replace('[', '<tspan text-decoration="underline" stroke="none">')
	s = s.replace(']', '</tspan> →')
	return s


def read_concepts() -> dict:
	data: dict = {}
	with open(DATA_CONCEPTS_FILE_NAME, newline='') as f:
		csv_reader = csv.reader(f, delimiter=',', quotechar='"')

		next(csv_reader)
		categories = next(csv_reader)
		topics = next(csv_reader)
		next(csv_reader)

		for (i, topic) in enumerate(topics):
			category = categories[i]
			if len(topic) == 0:
				continue
			if (category == '#N/A') or (category == ''):
				continue
			data.setdefault(category, collections.OrderedDict())
			data[category].setdefault(topic, [])

		for row in csv_reader:
			for (i, concept) in enumerate(row):
				topic, category = topics[i], categories[i]
				if '+' not in concept:
					continue
				concept = concept.replace('+', '').strip()
				data[category][topic] += [concept]

	return data


def print_concepts(concepts):
	print('Categories', len(concepts.keys()))
	for category in concepts:
		print('Topics', len(concepts[category].keys()))
		for topic in concepts[category]:
			print(category, '/', topic)
			#print(category, topic, concepts[category][topic])


def locate_icons():
	icons: dict = {}
	for (dirpath, _dirnames, filenames) in os.walk(ICON_PATH):
		for filename in filenames:
			if filename.endswith('.svg'):
				icons[os.path.splitext(filename)[0]] = os.path.join(
					dirpath, filename)
	return icons


def make(concepts: dict, icons: dict, convert_to_png: bool = False):
	if not os.path.exists(OUTPUT_PATH):
		os.mkdir(OUTPUT_PATH)

	with open(DATA_SINGLE_FILE_NAME, newline='') as f:
		csv_reader = csv.reader(f, delimiter=',', quotechar='"')

		next(csv_reader) # skip header

		front_original = open(FRONT_INPUT_FILE_NAME, 'r').read()
		back_original = open(BACK_INPUT_FILE_NAME, 'r').read()

		category_nth = 0
		category_previous = ''

		for row in csv_reader:
			print('Card', row[0])

			number_string, category, category_color, d100, d20, d10, d12, d8, d6, d4, d2, suit, letter, color_name_en, color_name_ch, color_hex, icon = row
			number = int(number_string)

			front, back = front_original, back_original

			front_output_file_name = FRONT_OUTPUT_FILE_NAME.format(number=number)
			back_output_file_name = BACK_OUTPUT_FILE_NAME.format(number=number)

			front = front.replace('>@L<', '>{0}<'.format(letter))
			front = front.replace('>@HEADER<', '>{0}<'.format(linkify(category)))
			front = front.replace('id="header" fill="#ff00ff"', 'id="header" fill="{0}"'.format(category_color))
			front = front.replace('>@W2<', '>{0}<'.format(d2))
			front = front.replace('>@W4<', '>{0}<'.format(d4))
			front = front.replace('>@W6<', '>{0}<'.format(d6))
			front = front.replace('>@W8<', '>{0}<'.format(d8))
			front = front.replace('>@W10<', '>{0}<'.format(d10))
			front = front.replace('>@W12<', '>{0}<'.format(d12))
			front = front.replace('>@W20<', '>{0}<'.format(d20))
			front = front.replace('>@W100<', '>{0}<'.format(d100))
			front = front.replace('>@C<', '>{0}<'.format(suit))
			front = front.replace('id="card" fill="#ff00ff"', 'id="card" fill="{0}"'.format(black_or_red(suit)))
			front = front.replace('id="color" fill="#ff00ff"', 'id="color" fill="{0}"'.format(color_hex))
			front = front.replace('>@COLOR<', '>{0}<'.format(linkify(color_name_ch)))
			front = front.replace('id="color-name" fill="#ff00ff"', 'id="color-name" fill="{0}"'.format(light_or_dark(color_hex)))
			icon = open(icons[icon], 'r').read()
			icon = icon.replace('fill="#fff"', 'fill="#000"')
			icon = icon.replace('viewBox="0 0 512 512"><path d="M0', 'viewBox="0 0 512 512"><path fill="#fff" d="M0')
			front = front.replace('@ICON', icon)

			back = back.replace('>@HEADER<', '>{0}<'.format(linkify(category)))
			back = back.replace('id="header" fill="#ff00ff"', 'id="header" fill="{0}"'.format(category_color))

			if category != category_previous:
				category_nth = 0
			category_previous = category

			topics = list(concepts[category].keys())
			for topic_index in range(20):
				left = '@L{0:02}'.format(topic_index + 1)
				right = '@R{0:02}'.format(topic_index + 1)

				if topic_index < len(topics):
					topic = topics[topic_index]
					back = back.replace(left, linkify(topic))
					if category_nth < len(concepts[category][topic]):
						back = back.replace(right, linkify(concepts[category][topic][category_nth]))
					else:
						back = back.replace(right, '-')
				else:
					back = back.replace(left, '')
					back = back.replace(right, '')

			category_nth += 1

			open(FRONT_TMP_FILE_NAME.format(number), 'w').write(front)
			open(BACK_TMP_FILE_NAME.format(number), 'w').write(back)

			if convert_to_png:
				subprocess.run('inkscape -d {dpi} -e {output} {input}'.format(dpi=DPI, input=FRONT_TMP_FILE_NAME, output=front_output_file_name), shell=True, check=True, stdout=subprocess.PIPE)
				subprocess.run('inkscape -d {dpi} -e {output} {input}'.format(dpi=DPI, input=BACK_TMP_FILE_NAME, output=back_output_file_name), shell=True, check=True, stdout=subprocess.PIPE)

			if number >= 2:
				pass
				#break

	os.remove(FRONT_TMP_FILE_NAME)
	os.remove(BACK_TMP_FILE_NAME)


concepts = read_concepts()
#print_concepts(concepts)
icons = locate_icons()
make(concepts, icons, convert_to_png=True)
locate_icons()
