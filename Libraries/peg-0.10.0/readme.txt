
Plugin

https://github.com/pegjs/pegjs/blob/master/docs/guides/javascript-api.md#new-pegcompilersessionoptions
https://raw.githubusercontent.com/dignifiedquire/pegjs-coffee-plugin/master/dist/pegjs-coffee-plugin-0.3.0.js

class MyPlugin {
	use(session, options) {
		session.passes.transform.push(this.pass);
	}

	pass(ast) {
		// ...
	}
}

const parser = peg.generate(GRAMMAR, { plugins: [new MyPlugin()] });
