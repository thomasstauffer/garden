#pragma once

#include <stdbool.h>
#include <stdint.h>

/*
This queue is not safe by to be used in interrupts by itself. Interrupts need
to be disabled manually in non interrupt context and it needs to be assured
that the same queue is not used in low/high priority interrupts. This
functionality is by design not within the queue.

Locking is often better done by the caller. E.g. if there are two queues, which
both needs to be atomically synchronized, interrupts needs to be disabled until
both operations are finished, to avoid inconsistencies between the two queues.

e.g.

DISABLED_INTERRUPTS()
queue_put(...)
ENABLE_INTERRUPTS()

e.g. on Cortex-M from CMSIS
__disable_irq() and __enable_irq()

*/

typedef struct {
	uint32_t index_put;
	uint32_t index_get;
	bool empty;

	uint32_t watermark;
	uint32_t max_item_count;
	uint8_t* storage;
	uint32_t storage_length;
	uint32_t item_stride;
	uint32_t max_item_data_length;
} queue;

void queue_init(queue* q, uint32_t max_item_count, uint8_t* storage, uint32_t storage_length);
bool queue_put(queue* q, const uint8_t* from, uint32_t length);
bool queue_get(queue* q, uint8_t* to, uint32_t* length);
uint32_t queue_count(const queue* q);
uint32_t queue_watermark(const queue* q);
