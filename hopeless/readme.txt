
C functions for embedded systems without any operating system.

- no heap, only stack
- lots of ISRs
- no sempaphores/mutexes
