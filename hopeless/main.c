#include <stdio.h>
#include <string.h>

#include "queue.h"

void assert(bool condition, const char* fileName, uint32_t line) {
	if (!condition) {
		printf("Assertion %s:%i\n", fileName, line);
	}
}

#define ASSERT(condition) assert(condition, __FILE__, __LINE__)

int main() {
	uint8_t mq_storage[64];

	queue q;

	queue_init(&q, 4, mq_storage, sizeof(mq_storage));

	// fill up

	ASSERT(queue_watermark(&q) == 0);
	ASSERT(queue_put(&q, (uint8_t*)"A", 2));
	ASSERT(queue_count(&q) == 1);
	ASSERT(queue_watermark(&q) == 1);
	ASSERT(queue_put(&q, (uint8_t*)"BC", 3));
	ASSERT(queue_count(&q) == 2);
	ASSERT(queue_watermark(&q) == 2);
	ASSERT(queue_put(&q, (uint8_t*)"DEF", 4));
	ASSERT(queue_count(&q) == 3);
	ASSERT(queue_watermark(&q) == 3);
	ASSERT(queue_put(&q, (uint8_t*)"GHIJ", 5));
	ASSERT(queue_count(&q) == 4);
	ASSERT(queue_watermark(&q) == 4);

	// try to overflow

	ASSERT(!queue_put(&q, (uint8_t*)"@", 1));
	ASSERT(queue_count(&q) == 4);
	ASSERT(queue_watermark(&q) == 4);

	for (uint32_t i = 0; i < sizeof(mq_storage); i += 1) {
		printf(" %02x", mq_storage[i]);
	}
	printf("\n");

	// remove all

	char buffer[16];
	uint32_t length;

	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(queue_count(&q) == 3);
	ASSERT(length == 2);
	ASSERT(strcmp(buffer, "A") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(queue_count(&q) == 2);
	ASSERT(length == 3);
	ASSERT(strcmp(buffer, "BC") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(queue_count(&q) == 1);
	ASSERT(length == 4);
	ASSERT(strcmp(buffer, "DEF") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(queue_count(&q) == 0);
	ASSERT(length == 5);
	ASSERT(strcmp(buffer, "GHIJ") == 0);

	// try to underflow the queue

	ASSERT(!queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(length == 0);
	ASSERT(queue_watermark(&q) == 4);

	for (uint32_t i = 0; i < sizeof(mq_storage); i += 1) {
		printf(" %02x", mq_storage[i]);
	}
	printf("\n");

	queue_init(&q, 4, mq_storage, sizeof(mq_storage));

	ASSERT(queue_count(&q) == 0);
	ASSERT(queue_watermark(&q) == 0);

	ASSERT(queue_put(&q, (uint8_t*)"A", 2));
	ASSERT(queue_put(&q, (uint8_t*)"BC", 3));
	ASSERT(queue_put(&q, (uint8_t*)"DEF", 4));
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "A") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "BC") == 0);
	ASSERT(queue_put(&q, (uint8_t*)"GHIJ", 5));
	ASSERT(queue_put(&q, (uint8_t*)"KLMNO", 6));
	ASSERT(queue_put(&q, (uint8_t*)"PQRSTU", 7));
	ASSERT(!queue_put(&q, (uint8_t*)"@", 1));
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "DEF") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "GHIJ") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "KLMNO") == 0);
	ASSERT(queue_get(&q, (uint8_t*)buffer, &length));
	ASSERT(strcmp(buffer, "PQRSTU") == 0);
	ASSERT(!queue_get(&q, (uint8_t*)buffer, &length));

	for (uint32_t i = 0; i < sizeof(mq_storage); i += 1) {
		printf(" %02x", mq_storage[i]);
	}
	printf("\n");

	return 0;
}
