#include <stdlib.h>
#include <string.h>

#include "queue.h"

typedef struct {
	uint32_t length;
	uint8_t data[];
} entry;

#include <stdio.h>

void queue_init(queue* q, uint32_t max_item_count, uint8_t* storage, uint32_t storage_length) {
	q->index_put = 0;
	q->index_get = 0;
	q->empty = true;

	q->watermark = 0;
	q->max_item_count = max_item_count;
	q->storage = storage;
	q->storage_length = storage_length;

	// could be computed during runtime (memory access vs cpu)
	q->item_stride = storage_length / q->max_item_count;
	q->max_item_data_length = q->item_stride - sizeof(entry);

	// initializing not really neceassary, only for debugging purposes
	memset(q->storage, 0xee, q->storage_length);
}

// returns true if added to the queue
bool queue_put(queue* q, const uint8_t* from, uint32_t length) {
	const bool can_add = q->empty || (q->index_put != q->index_get);
	if (can_add) {
		entry* e = (entry*)(&q->storage[q->index_put * q->item_stride]);

		e->length = length;
		memcpy(e->data, from, length);

		q->index_put = (q->index_put + 1) % q->max_item_count;
		q->empty = false;
	}

	const uint32_t count = queue_count(q);
	if (count > q->watermark) {
		q->watermark = count;
	}

	return can_add;
}

// returns true if removed from the queue
bool queue_get(queue* q, uint8_t* to, uint32_t* length) {
	const bool can_get = !q->empty;
	uint32_t l = 0;

	if (can_get) {
		entry* e = (entry*)(&q->storage[q->index_get * q->item_stride]);
		l = e->length;

		memcpy(to, e->data, l);

		q->index_get = (q->index_get + 1) % q->max_item_count;
		q->empty = q->index_get == q->index_put;
	}

	if (length != NULL) {
		*length = l;
	}

	return can_get;
}

uint32_t queue_count(const queue* q) {
	const uint32_t put = q->index_put;
	const uint32_t get = q->index_get;
	if (put == get) {
		return q->empty ? 0 : q->max_item_count;
	} else if (put > get) {
		return put - get;
	} else {
		return q->max_item_count - (get - put);
	}
}

uint32_t queue_watermark(const queue* q) {
	return q->watermark;
}
