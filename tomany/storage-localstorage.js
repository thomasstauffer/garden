import * as q from './../crow/q.js';

const KEY = 'tomany';

export class StorageLocalStorage {
	constructor() {
		this.localStorage = window.localStorage;
		if (this.localStorage === undefined) {
			throw 'LocalStorage Not Available';
		}
	}

	async set(data) {
		q.assertType(data, String);
		this.localStorage.setItem(KEY, data);
	}

	async get() {
		const data = this.localStorage.getItem(KEY);
		return data;
	}
}