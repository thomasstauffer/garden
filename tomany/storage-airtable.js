import * as q from './../crow/q.js';

export class StorageAirtable {
	constructor(url) {
		this.url = url;
	}

	async set(data) {
		q.assertType(data, String);
		const response = await fetch(this.url, {
			method: 'PATCH',
			body: JSON.stringify({ fields: { Data: data } }),
			headers: { 'Content-Type': 'application/json' },
		});
		q.assertEqual(response.status, 200);
		await response.text();
	}

	async get() {
		const response = await fetch(this.url);
		const text = await response.text();
		q.assertEqual(response.status, 200);
		const result = JSON.parse(text).fields.Data;
		q.assertType(result, String);
		return result;
	}
}