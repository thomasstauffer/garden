import * as q from './../crow/q.js';

/*

https://tomany-test.firebaseio.com/

curl -i https://tomany-test.firebaseio.com/storage.json

curl -i https://tomany-test.firebaseio.com/storage/test.json

curl -i https://tomany-test.firebaseio.com/storage/test.json --request PATCH --data '{"a": 1, "b": 3}'

https://firebase.google.com/docs/firestore/quotas

*/
export class StorageFirebase {
	constructor(url) {
		this.url = url;
	}

	async set(data) {
		q.assertType(data, String);
		const response = await fetch(this.url, {
			method: 'PATCH',
			body: JSON.stringify({ data: data }),
			headers: { 'Content-Type': 'application/json' },
		});
		q.assertEqual(response.status, 200);
		await response.text();
	}

	async get() {
		const response = await fetch(this.url);
		const text = await response.text();
		q.assertEqual(response.status, 200);
		const result = JSON.parse(text).data;
		q.assertType(result, String);
		return result;
	}
}