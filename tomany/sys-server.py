
# sudo apt install python3-tornado

import asyncio
import datetime
import io
import json
import os
import re
import time
import tornado.ioloop
import tornado.platform.asyncio
import tornado.web
import tornado.websocket

PATH = '../../Idle/Garden/'

def append(s):
	date = datetime.datetime.now().isoformat()
	message = date + ' ' + s + '\n'
	print(message, end='')
	with open('sys.log', 'a') as f:
		f.write(message)

def debug(*args):
	append(' '.join(map(str, args)))

def info(*args):
	append(' '.join(args))

class SysWebSocketHandler(tornado.websocket.WebSocketHandler):
	def __init__(self, application, request, **kwargs):
		assert SysWebSocketHandler._isValidFileName('abc.def')
		assert not SysWebSocketHandler._isValidFileName('x\x00')
		assert not SysWebSocketHandler._isValidFileName('a/b')
		assert not SysWebSocketHandler._isValidFileName('a\\b')
		#debug(application, request, kwargs)
		if (request.remote_ip != '127.0.0.1') or (request.headers['Origin'] not in ['null', 'file://', 'http://127.0.0.1:8888']):
			print(request)
			raise Exception('Access Denied')
		super(SysWebSocketHandler, self).__init__(application, request, **kwargs)

	def check_origin(self, origin):
		return True

	def open(self):
		debug('WebSocket Opened')

	@staticmethod
	def _isValidFileName(fileName):
		# because the slash and back-slash is checked, something like ../ should be impossible
		return re.search(r'[\x00-\x1f\'/\\:*?|<>]', fileName) is None

	@staticmethod
	def _checkFileName(fileName):
		if not SysWebSocketHandler._isValidFileName(fileName):
			raise ValueError('Invalid File Name')

	@staticmethod
	def _read(fileName):
		SysWebSocketHandler._checkFileName(fileName)
		try:
			with io.open(PATH + fileName, 'r', encoding='utf8', newline='') as f:
				data = f.read()
			return data
		except Exception:
			raise Exception('Read Error')

	@staticmethod
	def _write(fileName, data):
		SysWebSocketHandler._checkFileName(fileName)
		try:
			with io.open(PATH + fileName, 'w', encoding='utf8', newline='') as f:
				f.write(data)
		except Exception:
			raise Exception('Write Error')

	# TODO read/write binary messages instead of UTF-8?
	# TODO read all possible filenames on the beginning and only return the valid read/written names?
	# TODO rename readdir? better idea? something more generic? a pluggable filesystem?
	def on_message(self, message):
		message = json.loads(message)
		command = message['command']
		data = None
		error = None
		if command == 'writefile':
			try:
				SysWebSocketHandler._write(message['fileName'], message['data'])
			except Exception as e:
				error = str(e)
		elif command == 'readfile':
			try:
				data = SysWebSocketHandler._read(message['fileName'])
			except Exception as e:
				error = str(e)
		elif command == 'readdir':
			try:
				data = os.listdir(PATH)
			except Exception as e:
				error = 'Directory Error'
		else:
			info('Unkown Message')
			info(message)
			error = 'Unknown Message'
		answer = {'id': message['id'], 'command': command, 'data': data, 'error': error}
		self.write_message(json.dumps(answer))

	def on_close(self):
		debug('WebSocket Closed')
		pass

def main():
	PORT = 9999
	tornado.platform.asyncio.AsyncIOMainLoop().install()
	app = tornado.web.Application([
		(r'/sys', SysWebSocketHandler),
	])
	app.listen(PORT)
	loop = asyncio.get_event_loop()
	info('Ready To Run (127.0.0.1:' + str(PORT) + ')')
	if os.name == 'nt':
		async def wait():
			while True: # Windows Bugfix (Allow Ctrl+C)
				await asyncio.sleep(1.0)
		loop.run_until_complete(wait())
	else:
		loop.run_forever()
		pass


if __name__ == '__main__':
	main()
