import * as q from './../crow/q.js';
import { StorageAirtable } from './storage-airtable.js';
import { StorageLocalStorage } from './storage-localstorage.js';
import { StorageSessionStorage } from './storage-sessionstorage.js';
import { StorageFirebase } from './storage-firebase.js';
import { StorageIndexedDB } from './storage-indexeddb.js';
import { StorageSys } from './storage-sys.js';
import { StorageMemory } from './storage-memory.js';

const STORAGE_HEADER = 'tomany';

export class Storage {
	constructor(options) {
		const url = options.url;
		this.password = options.password;
		this.encrypt = true;
		if (url === 'memory') {
			this.encrypt = false;
			this.backend = new StorageMemory();
		} else if (url === 'sys') {
			this.backend = new StorageSys();
		} else if (url === 'sessionstorage') {
			this.backend = new StorageSessionStorage();
		} else if (url === 'localstorage') {
			this.backend = new StorageLocalStorage();
		} else if (url === 'indexeddb') {
			this.backend = new StorageIndexedDB();
		} else if (url === 'firebase') {
			const url = 'https://tomany-test.firebaseio.com/storage/test.json';
			this.backend = new StorageFirebase(url);
		} else if (url === 'airtable') {
			const apiKey = 'keysbmNlxxZxWXa8j'; // this is here by design
			const url = 'https://api.airtable.com/v0/appcHp35qQCOPXuqE/Table/recWYXubOXPZyfMOv?api_key=' + apiKey;
			this.backend = new StorageAirtable(url);
		} else {
			throw 'Unknown URL';
		}
	}

	static encode(value) {
		q.assertType(value, String);
		const encoder = new TextEncoder();
		const result = encoder.encode(value);
		q.assertType(result, Uint8Array);
		return result;
	}

	static decode(value) {
		q.assertType(value, Uint8Array);
		const decoder = new TextDecoder();
		const result = decoder.decode(value);
		q.assertType(result, String);
		return result;
	}

	static async deriveKey(password) {
		q.assertType(password, String);
		// https://developer.mozilla.org/en-US/docs/Web/API/CryptoKey
		const importedKey = await window.crypto.subtle.importKey('raw', Storage.encode(password), { name: 'PBKDF2' }, false, ['deriveKey']);
		const key = await window.crypto.subtle.deriveKey(
			{ name: 'PBKDF2', salt: Storage.encode('Hello Salt'), iterations: 1000, hash: 'SHA-256' },
			importedKey, { name: 'AES-GCM', length: 128 }, false, ['encrypt', 'decrypt']);
		q.assertType(key, CryptoKey);
		return key;
	}

	static async encrypt(data, password) {
		q.assertType(data, Object);
		q.assertType(password, String);
		const iv = crypto.getRandomValues(new Uint8Array(12));
		const key = await Storage.deriveKey(password);
		const encrypted = await window.crypto.subtle.encrypt(
			{ name: 'AES-GCM', iv: iv }, key, Storage.encode(JSON.stringify(data)));
		const toStorage = { iv: Array.from(iv), data: Array.from(new Uint8Array(encrypted)) };
		const result = JSON.stringify(toStorage);
		q.assertType(result, String);
		return result;
	}

	static async decrypt(fromStorage, password) {
		q.assertType(fromStorage, String);
		q.assertType(password, String);
		const fromStorageObject = JSON.parse(fromStorage);
		const iv = new Uint8Array(fromStorageObject.iv);
		const key = await Storage.deriveKey(password);
		const decrypted = Storage.decode(
			new Uint8Array(await window.crypto.subtle.decrypt(
				{ name: 'AES-GCM', iv: iv }, key, new Uint8Array(fromStorageObject.data))));
		const result = JSON.parse(decrypted);
		q.assertType(result, Object);
		return result;
	}

	// TODO second header in encrypted data?

	async set(data) {
		q.assertType(data, Object);
		const dataPayload = this.encrypt ? await Storage.encrypt(data, this.password) : JSON.stringify(data);
		const dataWithHeader = STORAGE_HEADER + dataPayload;
		await this.backend.set(dataWithHeader);
	}

	async get() {
		try {
			const dataWithHeader = await this.backend.get();
			const header = dataWithHeader.slice(0, STORAGE_HEADER.length);
			if (header !== STORAGE_HEADER) {
				throw 'Wrong Header';
			}
			const dataPayload = dataWithHeader.slice(STORAGE_HEADER.length);
			const data = this.encrypt ? await Storage.decrypt(dataPayload, this.password) : JSON.parse(dataPayload);
			q.assertType(data, Object);
			return data;
		} catch (error) {
			await this.set({});
			console.log(error);
			const result = {};
			q.assertType(result, Object);
			return result;
		}
	}
}