// TODO non utf8 hex-file? hex-editor? TextDecoder/TextEncoder but big question first it how to transfer that with JSON (has no uint8buffer)
// TODO if onclose is called try to reconnect with an exponentially increasing timer (500ms, 1s, 2, 4, 8, ...) what about messages which were already sent, use if more properly? at least add an event handler for errors, onclose on the other hand we are only on localhost, failure is unlikelier
// TODO handle on error
// TODO possibility to save in localstore and upload/download it as a zip/blob? do this as extension? synchronisation?

const ws = new WebSocket('ws://127.0.0.1:9999/sys');
//ws.binaryType = 'ArrayBuffer';
const opened = new Promise((resolve, _reject) => {
	ws.onopen = resolve;
});
const messageQueue = [];
let id = 0;

ws.onmessage = function (event) {
	const [resolve, reject, messageRequest] = messageQueue.shift();
	const message = JSON.parse(event.data);
	console.assert(message.id === messageRequest.id);
	console.assert(message.command === messageRequest.command);
	if (message.error === null) {
		resolve(message.data);
	} else {
		reject(message.error);
	}
};

async function makeRequest(message) {
	await opened;
	message.id = id;
	id += 1;
	console.assert(ws.readyState === WebSocket.OPEN, 'Socket Not Open');
	ws.send(JSON.stringify(message));
	return await new Promise((resolve, reject) => {
		messageQueue.push([resolve, reject, message]);
	});
}

export async function writeFile(fileName, data) {
	const message = { id: null, command: 'writefile', fileName: fileName, data: data };
	await makeRequest(message);
}

export async function readFile(fileName) {
	const message = { id: null, command: 'readfile', fileName: fileName };
	return await makeRequest(message);
}

export function readFiles(fileNames) {
	return Promise.all(fileNames.map(readFile));
}

export async function readDir() {
	const message = { id: null, command: 'readdir' };
	return await makeRequest(message);
}
