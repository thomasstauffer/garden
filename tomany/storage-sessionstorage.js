import * as q from '../crow/q.js';

const KEY = 'tomany';

export class StorageSessionStorage {
	constructor() {
		this.sessionStorage = window.sessionStorage;
		if (this.sessionStorage === undefined) {
			throw 'SessionStorage Not Available';
		}
	}

	async set(data) {
		q.assertType(data, String);
		this.sessionStorage.setItem(KEY, data);
	}

	async get() {
		const data = this.sessionStorage.getItem(KEY);
		return data;
	}
}