export class StorageMemory {
	constructor() {
		this.storage = '';
	}

	async set(data) {
		this.storage = data;
	}

	async get() {
		return this.storage;
	}
}