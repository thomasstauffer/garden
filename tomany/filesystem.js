//import * as q from './../crow/q.js';
import { Storage } from './storage.js';

/*
The API is in a state which will break easily.
Read/write quite likely will be async in the future e.g.
*/

export class FileSystem {
	constructor(options) {
		this.storage = new Storage(options);
		// TODO store files in list and create several indices for various combinations of tags e.g. indexName, indexNameType, ...
		this.files = {};
	}

	async open() {
		this.files = await this.storage.get();
	}

	async format() {
		this.files = {};
	}

	async sync() {
		await this.storage.set(this.files);
	}

	static isValidFile(file) {
		return (typeof (file) === 'object') && ('name' in file) && ('type' in file) && ('content' in file);
	}

	search(searchTags, resultTagNames) {
		const resultFiles = [];
		Object.values(this.files).forEach(file => {
			if (!FileSystem.isValidFile(file)) {
				return;
			}
			const resultFile = {};
			resultTagNames.forEach(name => {
				resultFile[name] = file[name];
			});
			resultFiles.push(resultFile);
		});
		return resultFiles;
	}

	// TODO add checksum
	write(file) {
		if (!FileSystem.isValidFile(file)) {
			throw 'Invalid File';
		}
		this.files[file.name] = file;
	}

	// TODO verify checksum
	read(file) {
		const result = this.files[file.name];
		if (!FileSystem.isValidFile(result)) {
			throw 'Invalid File';
		}
		return result;
	}
}