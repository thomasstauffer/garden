
/* global React, ReactDOM remarkable, chance */

/*
https://reactjs.org/docs/add-react-to-a-website.html
*/
const e = React.createElement;

/*
Most mobile browser in 2021 do not support Drag & Drop.

https://developer.mozilla.org/en-US/docs/Web/API/Document/dragstart_event

TODO

- use touch API to simulate it? but no drag and drop, that is quite likely awful to use, 
- no drag and drop at all, only a "Click Source", "Click Destination" thing?
- add last updated in GMT time to show a user a hint which element recently has changed
- cards can/may be/notbe shared betwwen lists?

*/
const DRAG_AND_DROP_AVAILABLE = 'draggable' in document.createElement('span');

class Markdown extends React.Component {
	constructor(props) {
		super(props);
		this.md = new remarkable.Remarkable(); // TODO share one instance with everyone?
	}

	render() {
		// this.props.useMarkdown === true???
		//e('span', { style: { color: card.color ?? '#333' } }, card.content)

		return e('div', { dangerouslySetInnerHTML: { __html: this.md.render(this.props.content) } });
	}
}

// TODO remove the following 3 functions with a more generic one? lodash? first think a bit more about the state, maybe an array is not a good idea anyway

function arrayInsert(base, index, element) {
	return [...base.slice(0, index), element, ...base.slice(index)];
}

function arrayReplace(base, index, element) {
	return [...base.slice(0, index), element, ...base.slice(index + 1)];
}

function arrayRemove(base, index) {
	return [...base.slice(0, index), ...base.slice(index + 1)];
}

function findCard(lists, cardId) {
	// TODO why are here strings?
	const cardLookup = {};
	for (const listIndex in lists) {
		for (const cardIndex in lists[listIndex].cardIds) {
			//console.log(typeof listIndex, typeof cardIndex);
			cardLookup[lists[listIndex].cardIds[cardIndex]] = { listIndex: parseInt(listIndex), cardIndex: parseInt(cardIndex) };
		}
	}
	return cardLookup[cardId];
}

function moveCard(lists, cardId, listIndexTo, cardIndexTo) {
	// should this information be stored in the card itself when dragged?
	const dragCardInfo = findCard(lists, cardId);
	// listIndexFrom, cardIndexFrom
	const l1 = lists;
	const l2 = arrayReplace(l1, dragCardInfo.listIndex, {
		...l1[dragCardInfo.listIndex], cardIds:
			arrayRemove(l1[dragCardInfo.listIndex].cardIds, dragCardInfo.cardIndex)
	});
	const l3 = arrayReplace(l2, listIndexTo, {
		...l2[listIndexTo], cardIds:
			arrayInsert(l2[listIndexTo].cardIds, cardIndexTo, cardId)
	});
	return l3;
}

function moveList(lists, listIndexFrom, listIndexTo) {
	const list = lists[listIndexFrom];
	const listRemove = arrayRemove(lists, listIndexFrom);
	const listInsert = arrayInsert(listRemove, listIndexTo - ((listIndexFrom < listIndexTo) ? 1 : 0), list);
	return listInsert;
}

function makeList(title, cardIds = []) {
	return { title: title, cardIds: cardIds };
}

function makeParagraph() {
	return chance.paragraph({ sentences: 3 });
}

class Board extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			version: 0, // relevant if stored in a DB
			title: 'Title ' + chance.word({ syllables: 5 }),
			cards: {
				0: { color: 'red', content: makeParagraph() },
				1: { color: 'green', content: makeParagraph() },
				2: { color: 'black', content: 'Hello **Tiny** Blueish `Livable` _World_\n- Yes\n- No' },
				3: { color: 'black', content: '[pixabay.com](https://pixabay.com)\n\n![pixabay.com](https://cdn.pixabay.com/photo/2015/09/09/16/05/forest-931706_960_720.jpg)' },
				4: { color: 'black', content: makeParagraph() },
				5: { color: 'black', content: makeParagraph() },
				6: { color: 'black', content: makeParagraph() },
				7: { color: 'black', content: makeParagraph() },
				8: { color: 'black', content: makeParagraph() },
				9: { color: 'black', content: makeParagraph() },
				10: { color: 'black', content: makeParagraph() },
				11: { color: 'black', content: makeParagraph() },
				12: { color: 'black', content: makeParagraph() },
			},
			lists: [
				makeList('List Alpha', [0, 1, 2]),
				makeList('List Beta', [3, 4]),
				makeList('List Gamma', [5, 6, 7]),
				makeList('List Delta', [8]),
				makeList('List Epsilon', [9, 10, 11, 12]),
				makeList('List Omega', []),
			],
			dragging: false,
			selected: {
				what: 'none', // 'none', 'list', 'card'
				cardId: null,
				listIndex: null,
			},
		};
	}

	update(event) {
		const state = this.state;
		const selected = state.selected;
		const cards = state.cards;
		const lists = state.lists;

		if (event.name === 'debug') {
			console.log(state);
		} else if (event.name === 'board-select') {
			this.setState({ selected: { what: 'board' } });
		} else if (event.name === 'board-title') {
			this.setState({ title: event.title });
		} else if (event.name === 'list-select') {
			this.setState({ selected: { what: 'list', listIndex: event.listIndex } });
		} else if (event.name === 'list-title') {
			this.setState({
				lists: [...arrayReplace(lists, selected.listIndex, { ...lists[selected.listIndex], title: event.title })]
			});
		} else if (event.name === 'list-add') {
			this.setState({ lists: [...lists, makeList('Empty')] });
		} else if (event.name === 'list-move') {
			this.setState({
				lists: moveList(lists, selected.listIndex, event.listIndex),
				dragging: false,
			});
		} else if (event.name === 'card-select') {
			this.setState({ selected: { what: 'card', cardId: event.cardId } });
		} else if (event.name === 'card-content') {
			this.setState({
				cards: {
					...cards,
					[selected.cardId]: { ...cards[selected.cardId], content: event.content }
				}
			});
		} else if (event.name === 'card-color') {
			this.setState({
				cards: {
					...cards,
					[selected.cardId]: { ...cards[selected.cardId], color: event.color }
				}
			});
		} else if (event.name === 'card-add') {
			const cardIdNew = 1 + (Math.max(...Object.keys(cards).map($ => parseInt($))));
			this.setState({
				cards: { ...cards, [cardIdNew]: { content: '???' } },
				lists: arrayReplace(lists, event.listIndex,
					{
						...lists[event.listIndex],
						cardIds: lists[event.listIndex].cardIds.concat(cardIdNew)
					})
			});
		} else if (event.name === 'card-move') {
			if (this.state.selected.what === 'card') {
				this.setState({
					lists: moveCard(lists, state.selected.cardId, event.listIndex, event.cardIndex),
					dragging: false,
				});
				// TODO selection is always wrong now, because list are not stored like cards
			}
		} else if (event.name === 'card-drag-start') {
			this.setState({
				dragging: true,
				selected: { what: 'card', cardId: event.cardId }
			});
		} else if (event.name === 'list-drag-start') {
			this.setState({
				dragging: true,
				selected: { what: 'list', listIndex: event.listIndex }
			});
		} else if (event.name === 'drag-end') {
			this.setState({
				dragging: false,
			});
		} else {
			console.debug('Unknown Event ' + event.name);
		}
	}

	renderCardDrop(listIndex, cardIndex) {
		const move = () => this.update({ name: 'card-move', listIndex: listIndex, cardIndex });
		return e('div', {
			className: 'carddrop' + ((this.state.dragging && (this.state.selected.what === 'card')) ? 'highlight' : ''),
			onDragOver: (event) => event.preventDefault(),
			onDrop: move,
			onClick: move,
		});
	}

	renderCard(cardId, card) {
		return e('div', {
			className: 'card',
			draggable: true,
			onDragStart: () => this.update({ name: 'card-drag-start', cardId: cardId }),
			onDragEnd: () => this.update({ name: 'drag-end' }),
			onTouchStart: () => console.log('touch start'),
			onTouchMove: () => console.log('touch move'),
			onTouchEnd: () => console.log('touch end'),
			onClick: () => this.update({ name: 'card-select', cardId: cardId }),
			style: {
				color: card.color,
				opacity: this.state.dragging && (this.state.selected.cardId === cardId) ? 0.5 : 1.0,
			}
		}, e(Markdown, { content: card.content }));
	}

	renderListTitle(list, listIndex) {
		return e('div', {
			className: 'listtitle',
			draggable: true,
			onDragStart: () => this.update({ name: 'list-drag-start', listIndex: listIndex }),
			onDragEnd: () => this.update({ name: 'drag-end' }),
			onClick: () => this.update({ name: 'list-select', listIndex: listIndex }),
		}, list.title);
	}

	renderList(cards, listIndex, list) {
		return e('div', { className: 'list' },
			this.renderListTitle(list, listIndex),
			this.renderCardDrop(listIndex, 0),
			...list.cardIds.map((cardId, cardIndex) =>
				[this.renderCard(cardId, cards[cardId]), this.renderCardDrop(listIndex, cardIndex + 1)]
			).flat(),
			e('button', {
				onClick: () => this.update({ name: 'card-add', listIndex: listIndex }),
			}, 'Add Empty Card'),
		);
	}

	renderListDrop(listIndex) {
		const move = () => this.update({ name: 'list-move', listIndex: listIndex });
		return e('div', {
			className: 'listdrop' + ((this.state.dragging && (this.state.selected.what === 'list')) ? 'highlight' : ''),
			onDragOver: (event) => event.preventDefault(),
			onDrop: move,
			onClick: move,
		});
	}

	renderBoard() {
		return e('div', { className: 'board' },
			this.renderListDrop(0),
			...this.state.lists.map((list, listIndex) =>
				[this.renderList(this.state.cards, listIndex, list), this.renderListDrop(listIndex + 1)]
			).flat()
		);
	}

	render() {
		const selected = this.state.selected;

		const buttonListAdd = e('button', {
			onClick: () => this.update({ name: 'list-add' }),
		}, 'Add Empty List');
		// TODO better to create several areas an show hide them?
		const text = e('textarea', {
			onChange: (event) => {
				if (selected.what === 'card') {
					this.update({ name: 'card-content', content: event.target.value });
				} else if (selected.what === 'list') {
					this.update({ name: 'list-title', title: event.target.value });
				} else if (selected.what === 'board') {
					this.update({ name: 'board-title', title: event.target.value });
				}
			},
			disabled: selected.what === 'none',
			value: (() => {
				if (selected.what === 'card') {
					return this.state.cards[selected.cardId].content;
				} else if (selected.what === 'list') {
					return this.state.lists[selected.listIndex].title;
				} else if (selected.what === 'board') {
					return this.state.title;
				} else {
					return '';
				}
			})(),
		});
		return e('div', { className: 'main' },
			e('div', { className: 'edit' }, ''),
			e('div', {
				className: 'boardtitle',
				onClick: () => this.update({ name: 'board-select' })
			}, this.state.title),
			e('div', { className: 'edit' },
				text,
				['list', 'card'].includes(selected.what) ? e('button', {}, '⬅️ Left') : null,
				['list', 'card'].includes(selected.what) ? e('button', {}, '➡️ Right') : null,
				selected.what === 'card' ? e('button', {}, '⬆️ Up') : null,
				selected.what === 'card' ? e('button', {}, '⬇️ Down') : null,
				selected.what === 'card' ? e('input', {
					onChange: (event) => this.update({ name: 'card-color', color: event.target.value }),
					value: this.state.cards[selected.cardId].color
				}) : null,
				buttonListAdd,
				e('h3', {}, 'Debug'),
				e('div', {}, 'Drag & Drop Available: ' + DRAG_AND_DROP_AVAILABLE.toString()),
				e('div', {}, 'Lists:'),
				e('div', { className: 'json' }, JSON.stringify(this.state.lists)),
				e('div', {}, 'Selection:'),
				e('div', { className: 'json' }, JSON.stringify(this.state.selected)),
			),
			this.renderBoard()
		);
	}
}

function main() {
	const domMain = document.getElementById('main');
	ReactDOM.render(React.createElement(Board), domMain);
}

window.addEventListener('load', main);
