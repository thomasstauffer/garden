/*

https://developer.mozilla.org/en-US/docs/Web/MathML
https://www.w3.org/TR/MathML3/mathml.html
https://www.tutorialspoint.com/mathml/index.htm
https://en.wikipedia.org/wiki/List_of_mathematical_symbols_by_subject
http://asciimath.org

TI-92/98

https://math.oregonstate.edu/files/math/home/programs/undergrad/TI_Manuals/ti92Guidebook.pdf
https://www.ocf.berkeley.edu/~pad/emu/v10.html

Ideas:

- [ ]
- { }
- imaginary/complex i
- vec
- matrix []
- Indices: log_10, a_1, a_xy, a[x,y], a_[x,y]? ...
- lim(Delta x, Delta x, 0)
- alternative derivatives: f', f^'
*/

import * as scanner from './../crow/scanner.js';
import * as parser from './../crow/parser.js';
import * as html from './../crow/html.js';
import * as error from './../crow/error.js';
import * as assert from './../crow/assert.js';

export function tokenize(string) {
	const rules = [
		{ name: 'name', re: '^[a-zA-Z][a-zA-Z0-9_\']*', skip: false },
		{ name: 'number', re: '^([0-9]+(\\.[0-9]+)?)', skip: false },
		{ name: '(', re: '^\\(', skip: false },
		{ name: ')', re: '^\\)', skip: false },
		{ name: '[', re: '^\\[', skip: false },
		{ name: ']', re: '^\\]', skip: false },
		{ name: ',', re: '^,', skip: false },
		{ name: ':=', re: '^:=', skip: false },
		{ name: '=', re: '^=', skip: false },
		{ name: '+', re: '^\\+', skip: false },
		{ name: '-', re: '^-', skip: false },
		{ name: '*', re: '^\\*', skip: false },
		{ name: '/', re: '^/', skip: false },
		{ name: '^', re: '^\\^', skip: false },
		{ name: '_', re: '^[ \r\t\n]+', skip: true },
	];

	return scanner.scanner(rules, string);
}

export function parse(tokens) {
	const p = new parser.Parser(tokens);

	function flat(array) {
		return [].concat.apply([], array);
	}

	function simplify(children, name) {
		if (children[1].length === 0) {
			return children[0];
		} else {
			return { name: name, value: flat(children) };
		}
	}

	const start = () => ({ name: 'start', value: listop() });

	// eqop = addop ( '=' addop )*
	const listop = () => p.and([eqop, p.repeat(p.and([p.token(','), eqop], $ => $[1]))], $ => simplify($, 'listop'))();
	const eqop = () => p.and([addop, p.repeat(p.and([p.token('='), addop], $ => $[1]))], $ => simplify($, 'eqop'))();
	const addop = () => p.and([subop, p.repeat(p.and([p.token('+'), subop], $ => $[1]))], $ => simplify($, 'addop'))();
	const subop = () => p.and([mulop, p.repeat(p.and([p.token('-'), mulop], $ => $[1]))], $ => simplify($, 'subop'))();
	const mulop = () => p.and([divop, p.repeat(p.and([p.token('*'), divop], $ => $[1]))], $ => simplify($, 'mulop'))();
	const divop = () => p.and([powop, p.repeat(p.and([p.token('/'), powop], $ => $[1]))], $ => simplify($, 'divop'))();
	const powop = () => p.and([negop, p.repeat(p.and([p.token('^'), negop], $ => $[1]))], $ => simplify($, 'powop'))();
	// negop = '-' atom | atom
	const negop = () => p.or([
		p.and([p.token('-'), atom], $ => ({ name: 'negop', value: $[1] })),
		atom,
	])();
	// atom = '(' listop ')' | atomCallOrName | 'number'
	const atom = () => p.or([
		p.and([p.token('('), listop, p.token(')')], $ => ({ name: 'paren', value: $[1] })),
		atomCallOrName,
		p.and([p.token('number')], $ => ({ name: 'number', value: $[0].value })),
	])();
	// name ( '(' listop ')' | () )
	const atomCallOrName = () => p.and([p.token('name'), p.or([
		p.and([p.token('('), listop, p.token(')')], $ => ({ name: 'func', expr: $[1] })),
		p.and([() => []], _$ => ({ name: 'name', expr: null })),
	])], $ => ({ name: $[1].name, value: $[1].expr === null ? $[0].value : [$[0].value, $[1].expr] }))();

	return start();
}

function toIntermediate(ast) {
	const mathml = html.mathml;

	// intersperse([1, 2, 3], 666) -> [1, 666, 2, 666, 3]
	function intersperse(array, item) {
		return array.reduce((acc, $) => acc.concat([item, $]), []).slice(1);
	}

	function removeParen(ast) {
		return ast.name === 'paren' ? ast.value : ast;
	}

	const reNameIndex = new RegExp('([a-zA-Z0-9]+)(_[a-zA-Z0-9]*)?');
	const greekLettersBig = ['Alpha', 'Beta', 'Gamma', 'Delta', 'Epsilon', 'Zeta', 'Eta', 'Theta', 'Iota', 'Kappa', 'Lambda', 'Mu', 'Nu', 'Xi', 'Omicron', 'Pi', 'Rho', '', 'Sigma', 'Tau', 'Upsilon', 'Phi', 'Chi', 'Psi', 'Omega'];
	const greekLettersSmall = ['alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta', 'eta', 'theta', 'iota', 'kappa', 'lambda', 'mu', 'nu', 'xi', 'omicron', 'pi', 'rho', 'varsigma', 'sigma', 'tau', 'upsilon', 'phi', 'chi', 'psi', 'omega'];

	function convertName(name) {
		const match = name.match(reNameIndex);
		const indexSmall = greekLettersSmall.indexOf(name);
		const indexBig = greekLettersBig.indexOf(name);
		if (name === 'inf') {
			return inf;
		} else if (indexBig >= 0) {
			return mathml('mi', String.fromCodePoint(0x0391 + indexBig));
		} else if (indexSmall >= 0) {
			return mathml('mi', String.fromCodePoint(0x03b1 + indexSmall));
		} else if (match[2] === undefined) {
			return mathml('mi', name);
		} else {
			return mathml('msub', mathml('mi', match[1]), mathml('mi', match[2].slice(1)));
		}
	}

	// https://en.wikipedia.org/wiki/List_of_mathematical_symbols_by_subject
	const middot = '\u22c5';
	const re = '\u211c';
	const im = '\u2111';
	const lfloor = '\u230a';
	const rfloor = '\u230b';
	const lceil = '\u2308';
	const rceil = '\u2309';
	const inf = '\u221e';
	const part = '\u2202';
	const int = '\u222b';
	const sum = '\u2211';
	const prod = '\u220f';
	const rarr = '\u2192';

	function convertFunc(ast) {
		if (ast.value[0] === 'sqrt') {
			return mathml('msqrt', convert(ast.value[1]));
		} else if (ast.value[0] === 'root') {
			const params = ast.value[1].value;
			return mathml('mroot', convert(params[0]), convert(params[1]));
		} else if (ast.value[0] === 'lim') {
			const params = ast.value[1].value;
			return mathml('mrow', mathml('munder', mathml('mo', 'lim'), mathml('mrow', convert(params[1]), mathml('mo', rarr), convert(params[2]))), convert(params[0]));
		} else if (ast.value[0] === 'int') {
			const params = ast.value[1].value;
			const ddx = mathml('mfrac', mathml('mn', part), mathml('mrow', mathml('mn', part), convert(params[1])));
			if (params.length == 2) {
				return mathml('mrow', mathml('mo', int), mathml('mo', '('), convert(params[0]), mathml('mo', ')'), ddx);
			}
			return mathml('mrow', mathml('munderover', mathml('mo', int), convert(params[1]), convert(params[2])), mathml('mo', '('), convert(params[0]), mathml('mo', ')'), ddx);
		} else if (ast.value[0] === 'diff') {
			const params = ast.value[1].value;
			const ddx = mathml('mfrac', mathml('mn', part), mathml('mrow', mathml('mn', part), convert(params[1])));
			return mathml('mrow', ddx, mathml('mo', '('), convert(params[0]), mathml('mo', ')'));
		} else if (ast.value[0] === 'sum') {
			const params = ast.value[1].value;
			return mathml('mrow', mathml('munderover', mathml('mo', sum), mathml('mrow', convert(params[1]), mathml('mo', '='), convert(params[2])), convert(params[3])), convert(params[0]));
		} else if (ast.value[0] === 'product') {
			const params = ast.value[1].value;
			return mathml('mrow', mathml('munderover', mathml('mo', prod), mathml('mrow', convert(params[1]), mathml('mo', '='), convert(params[2])), convert(params[3])), convert(params[0]));
		} else if (ast.value[0] === 'floor') {
			return mathml('mrow', mathml('mo', lfloor), convert(ast.value[1]), mathml('mo', rfloor));
		} else if (ast.value[0] === 'ceil') {
			return mathml('mrow', mathml('mo', lceil), convert(ast.value[1]), mathml('mo', rceil));
		} else if (ast.value[0] === 'abs') {
			return mathml('mrow', mathml('mo', '|'), convert(ast.value[1]), mathml('mo', '|'));
		} else if (ast.value[0] === 'vec') {
			return mathml('mi', 'TODO');
		} else if (ast.value[0] === 'angle') {
			return mathml('mi', 'TODO');
		} else if (ast.value[0] === 're') {
			return mathml('mrow', mathml('mo', re), convert(ast.value[1]));
		} else if (ast.value[0] === 'im') {
			return mathml('mrow', mathml('mo', im), convert(ast.value[1]));
		} else if (ast.value[0] === 'conj') {
			return mathml('mover', convert(ast.value[1]), mathml('mo', '_'));
		} else {
			return mathml('mrow', convertName(ast.value[0]), mathml('mrow', mathml('mo', '('), convert(ast.value[1]), mathml('mo', ')')));
		}
	}

	function convert(ast) {
		if (ast.name === 'start') {
			return mathml('math', { display: 'block' }, convert(ast.value));
		} else if (ast.name === 'name') {
			return convertName(ast.value);
		} else if (ast.name === 'number') {
			return mathml('mn', ast.value);
		} else if (ast.name === 'negop') {
			return mathml('mrow', mathml('mo', '-'), convert(ast.value));
		} else if (ast.name === 'listop') {
			return mathml('mrow', ...intersperse(ast.value.map(x => convert(x)), mathml('mo', ',')));
		} else if (ast.name === 'eqop') {
			return mathml('mrow', ...intersperse(ast.value.map(x => convert(x)), mathml('mo', '=')));
		} else if (ast.name === 'addop') {
			return mathml('mrow', ...intersperse(ast.value.map(x => convert(x)), mathml('mo', '+')));
		} else if (ast.name === 'subop') {
			return mathml('mrow', ...intersperse(ast.value.map(x => convert(x)), mathml('mo', '-')));
		} else if (ast.name === 'mulop') {
			return mathml('mrow', ...intersperse(ast.value.map(x => convert(x)), mathml('mo', middot)));
		} else if (ast.name === 'divop') {
			return mathml('mfrac', ...ast.value.map(x => convert(removeParen(x))));
		} else if (ast.name === 'powop') {
			return mathml('msup', ...ast.value.map(x => convert(x)));
		} else if (ast.name === 'paren') {
			return mathml('mrow', mathml('mo', '('), convert(ast.value), mathml('mo', ')'));
		} else if (ast.name === 'func') {
			return convertFunc(ast);
		} else {
			error.NotImplemented(ast.name);
		}
	}

	return convert(ast);
}

export function toDOM(ast) {
	return html.toDOM(toIntermediate(ast));
}

export function toString(ast) {
	return html.toHTML(toIntermediate(ast));
}

export function evaluate(ast, env) {
	const FUNCTIONS = {
		'sqrt': Math.sqrt,
		'sin': Math.sin,
		'cos': Math.cos,
		'tan': Math.tan,
		'asin': Math.asin,
		'acos': Math.acos,
		'atan': Math.atan,
		'ln': Math.log,
		'exp': Math.exp,
		'root': (a, b) => Math.pow(a, 1.0 / b),
	};

	function func(name, param) {
		if (param.name === 'listop') {
			if ((name === 'sum') || (name === 'product')) {
				const variable = param.value[0].value;
				const from = visit(param.value[1]);
				const to = visit(param.value[2]);
				const expr = param.value[3];
				const isSum = name === 'sum';
				let result = isSum ? 0 : 1;
				for (let i = from; i <= to; i += 1) {
					env[variable] = i;
					const part = visit(expr);
					result = isSum ? result + part : result * part;
				}
				return result;
			} else {
				const evaluated = param.value.map($ => visit($));
				return FUNCTIONS[name](...evaluated);
			}
		} else {
			return FUNCTIONS[name](visit(param));
		}
	}

	function visit(ast) {
		if (ast.name === 'start') {
			return visit(ast.value);
		} else if (ast.name === 'name') {
			return env[ast.value];
		} else if (ast.name === 'number') {
			return parseFloat(ast.value);
		} else if (ast.name === 'negop') {
			return -visit(ast.value);
		} else if (ast.name === 'eqop') {
			// TODO use := !!! what did I think here?
			const result = visit(ast.value[1]);
			env[ast.value[0].value] = result;
			return result;
		} else if (ast.name === 'addop') {
			return ast.value.map(x => visit(x)).reduce((a, x) => a + x);
		} else if (ast.name === 'subop') {
			return ast.value.map(x => visit(x)).reduce((a, x) => a - x);
		} else if (ast.name === 'mulop') {
			return ast.value.map(x => visit(x)).reduce((a, x) => a * x);
		} else if (ast.name === 'divop') {
			return ast.value.map(x => visit(x)).reduce((a, x) => a / x);
		} else if (ast.name === 'powop') {
			return ast.value.map(x => visit(x)).reduce((a, x) => Math.pow(a, x)); // TODO right associative?
		} else if (ast.name === 'func') {
			return func(ast.value[0], ast.value[1]);
		} else if (ast.name === 'paren') {
			return visit(ast.value);
		} else {
			error.NotImplemented(ast.name);
		}
	}
	const result = visit(ast);
	return result;
}

export function toDOT(ast) {
	let number = 0;
	function visit(ast) {
		const id = 'n' + number;
		number += 1;
		if (ast.name === 'start') {
			return 'digraph {\n' + visit(ast.value)[1] + '}\n';
		} else if (ast.name === 'name') {
			return [id, id + ' [label="' + ast.value + '"]\n'];
		} else if (ast.name === 'number') {
			return [id, id + ' [label="' + ast.value + '"]\n'];
		} else if (ast.name === 'negop') {
			return [id, id + ' [label="-"]\n'];
		} else if (ast.name === 'listop') {
			return [id, id + ' [label=","]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'eqop') {
			return [id, id + ' [label="="]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'addop') {
			return [id, id + ' [label="+"]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'subop') {
			return [id, id + ' [label="-"]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'mulop') {
			return [id, id + ' [label="*"]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'divop') {
			return [id, id + ' [label="/"]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'powop') {
			return [id, id + ' [label="^"]\n' + ast.value.map(x => { const [a, b] = visit(x); return id + ' -> ' + a + '\n' + b; }).join('')];
		} else if (ast.name === 'func') {
			const [a, b] = visit(ast.value[1]);
			return [id, id + ' [label="' + ast.value[0] + '"]\n' + id + ' -> ' + a + '\n' + b];
		} else if (ast.name === 'paren') {
			return visit(ast.value);
		} else {
			error.NotImplemented(ast.name);
		}
	}
	return visit(ast);
}

export function test() {
	function assertEval(string, expected) {
		const tokens = tokenize(string);
		const ast = parse(tokens);
		const result = evaluate(ast, { 'pi': Math.PI });
		assert.isTrue(toDOT(ast).length > 0);
		assert.isTrue(toString(ast).length > 0);
		assert.almostEqual(result, expected, 1.0e-6);
	}

	assertEval('0', 0);
	assertEval('123.456', 123.456);
	assertEval('1+2', 1 + 2);
	assertEval('1+2*3', 1 + 2 * 3);
	assertEval('(1+2)*3', (1 + 2) * 3);
	assertEval('1*(2)', 2.0);
	assertEval('1*(-2)', -2.0);
	assertEval('\r\t1\n + \r\n   2 +\r3\r \n', 1 + 2 + 3);
	assertEval('sqrt(2)^2', 2.0);
	assertEval('root(8, 3)', 2.0);
	assertEval('asin(1)*2', Math.PI);
	assertEval('sin(pi)', 0.0);
	assertEval('cos(pi)', -1.0);
	assertEval('ln(exp(1))', 1.0);
	assertEval('1 + 4/(2+2) + (2^3/8) + (5-sqrt(4^2))', 4.0);
	assertEval('sum(i, 1, 5, i)', 1 + 2 + 3 + 4 + 5);
	assertEval('product(i, 1, 5, i)', 1 * 2 * 3 * 4 * 5);
}
