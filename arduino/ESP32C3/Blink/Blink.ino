
/*
ESP32 C3 Super Mini

Board: "ESP32C3 Dev Module"
USB CDC on Boot: "Enabled"

Sucessfully Tested 2024-09-08
*/

#define LED 8

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
}

void loop() {
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
  Serial.println("ESP32C3");
}
