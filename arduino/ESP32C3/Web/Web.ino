
/*
ESP32 C3 Super Mini

Board: "ESP32C3 Dev Module"
USB CDC on Boot: "Enabled"

Sucessfully Tested 2024-09-08
*/

#include <WebServer.h>
#include <WiFi.h>

const char* WIFI_SSID = "Web";
const char* WIFI_PASSWORD = "";

#define BLINK_STEP_MS 2000
uint32_t blink_count = 0;

uint32_t blink_t = 0;

WebServer server(80);
IPAddress ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

void handle_root() {
  Serial.println("handle_root");
  server.send(200, "text/html", "<h1>ESP32C3</h1>");
}

void setup() {
  Serial.begin(115200);
  Serial.println("setup");

  WiFi.softAP(WIFI_SSID, WIFI_PASSWORD);
  WiFi.softAPConfig(ip, gateway, subnet);

  server.on("/", handle_root);
  server.begin();

  Serial.println("setup server.begin");
}

void loop() {
  server.handleClient();
  delay(5);
}
