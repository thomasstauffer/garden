#include <WebServer.h>
#include <WiFi.h>
#include <Wire.h>

#define BLINK_LED_GPIO 2

const char* WIFI_SSID = "Temperature Sensor";
const char* WIFI_PASSWORD = "";

#define BLINK_STEP_MS 2000
uint32_t blink_count = 0;

uint32_t blink_t = 0;

WebServer server(80);
IPAddress ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

int32_t tmperature_per_minute = 0;

#define RECORD_5S_N 3000
#define RECORD_5S_STEP_MS 5000
uint32_t record_5s_t = 0;
int32_t record_5s_data[RECORD_5S_N];
uint32_t record_5s_count = 0;

uint32_t ads_error_count = 0;

#define ADS1x15_I2C_ADDRESS 0x48

#define ADS1x15_CONVERSION_REG 0x00
#define ADS1x15_CONFIG_REG 0x01
#define ADS1x15_LO_THRESH_REG 0x02
#define ADS1x15_HI_THRESH_REG 0x03

#define ADS1x15_CONFIG_OS (0b1 << 15)
#define ADS1x15_CONFIG_MUX_AIN0_AIN1 (0b000 << 12)
#define ADS1x15_CONFIG_MUX_AIN0_GND (0b100 << 12)
#define ADS1x15_CONFIG_PGA_2048 (0b010 << 9)
#define ADS1x15_CONFIG_PGA_1024 (0b011 << 9)
#define ADS1x15_CONFIG_PGA_512 (0b100 << 9)
#define ADS1x15_CONFIG_PGA_256 (0b101 << 9)

byte ads1x15_write(uint8_t reg, uint16_t value) {
  Wire.beginTransmission(ADS1x15_I2C_ADDRESS);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xff);
  Wire.write((value >> 0) & 0xff);
  Wire.endTransmission();
}

uint16_t ads1x15_read(uint8_t reg) {
  Wire.beginTransmission(ADS1x15_I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(ADS1x15_I2C_ADDRESS, 2);
  uint32_t hi = Wire.read();
  uint32_t lo = Wire.read();

  return (hi << 8) | lo;
}

double ads1x15_voltage() {
  ads1x15_write(ADS1x15_CONFIG_REG, ADS1x15_CONFIG_OS | ADS1x15_CONFIG_MUX_AIN0_GND | ADS1x15_CONFIG_PGA_256);
  delay(200);
  uint16_t adc = 0;
  for (int i = 0; i < 3; i += 1) {
    adc = ads1x15_read(ADS1x15_CONVERSION_REG);
    if (adc > 0) {
      break;
    }
  }
  if (adc == 0) {
    ads_error_count += 1;
  }

  double v = 0.256 * adc / 32766.0;
  return v;
}

extern "C" {
uint8_t temprature_sens_read();
}

double cpu_temperature() {
  uint8_t t_fahrenheit = temprature_sens_read();
  double t_celsius = (t_fahrenheit - 32.0) / 1.8;
  return t_celsius;
}

// returns micro degree celcius
int32_t temperature_read() {
  // TODO access ads1x15

  //return 20000000 + (millis() % 1000000);

  //double t_celsius = cpu_temperature();
  //return t_celsius * 1000000.0;

  double v = ads1x15_voltage();
  double t_celsius = v / 0.04e-3; // 1 °C = 0.04 mV = 0.04e-3 V
  return t_celsius * 1000000.0;
}

void setup() {
  Serial.begin(115200);
  Serial.println("Init");

  for (int i = 0; i < RECORD_5S_N; i += 1) {
    record_5s_data[i] = 0;
  }

  pinMode(BLINK_LED_GPIO, OUTPUT);

  Wire.begin();

  WiFi.softAP(WIFI_SSID, WIFI_PASSWORD);
  WiFi.softAPConfig(ip, gateway, subnet);

  server.on("/", handle_root);
  server.on("/temperature/perminute", handle_temperature_per_minute);
  server.on("/temperature/current", handle_temperature_current);
  server.on("/temperature/record/5s", handle_temperature_record_5s);
  server.on("/adserror", handle_ads_error);
  server.begin();

  Serial.println("Run");
}

void loop() {
  server.handleClient();

  uint32_t t = millis();

  if ((t - blink_t) > BLINK_STEP_MS) {
    blink_t += BLINK_STEP_MS;
    blink();
  }

  if ((t - record_5s_t) > RECORD_5S_STEP_MS) {
    record_5s_t += RECORD_5S_STEP_MS;
    record_5s();
  }
}

void blink() {
  digitalWrite(BLINK_LED_GPIO, ((blink_count & 1) == 0) ? LOW : HIGH);
  blink_count += 1;
}

void record_5s() {
  if (record_5s_count >= RECORD_5S_N) {
    return;
  }

  const uint32_t delta_minute = 60000 / RECORD_5S_STEP_MS;

  record_5s_data[record_5s_count] = temperature_read();
  if (record_5s_count >= delta_minute) {
    tmperature_per_minute = record_5s_data[record_5s_count] - record_5s_data[record_5s_count - delta_minute];
  }
  record_5s_count += 1;
}

#include "index.html.h"

void handle_root() {
  server.send(200, "text/html", INDEX_HTML);
}

void handle_temperature_per_minute() {
  server.send(200, "text/plain", String(tmperature_per_minute));
}

void handle_temperature_current() {
  server.send(200, "text/plain", String(temperature_read()));
}

void handle_ads_error() {
  server.send(200, "text/plain", String(ads_error_count));
}

String join(int32_t array[], uint32_t length) {
  String result;
  String comma(",");
  String nl("\n");
  String dot(".");
  for (uint32_t i = 0; i < length; i += 1) {
    result += String(i);
    result += comma;
    result += String(array[i]);
    result += nl;
  }
  return result;
}

void handle_temperature_record_5s() {
  String html = join(record_5s_data, record_5s_count);
  // TODO possible to continuously send data?
  // TODO http 200 code in a header available?
  server.send(200, "text/plain", html);
}
