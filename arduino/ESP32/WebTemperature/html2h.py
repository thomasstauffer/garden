#!/usr/bin/env python3

FILENAMES = ['index.html']

for filename in FILENAMES:
	html = open(filename, 'r', encoding='utf-8').read()

	html_escaped = html.replace('"', '\\"').replace('\n', '\\n\\\n')
	s = '#define ' + filename.upper().replace('.', '_') + ' "' + html_escaped + '"'

	open(filename + '.h', 'w', encoding='ascii').write(s)
