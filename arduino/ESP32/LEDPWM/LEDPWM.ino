const int32_t LED_PIN = 2;
const int32_t FREQ = 1000;
const int32_t CHANNEL = 0;
const int32_t RESOLUTION = 8;

void setup() {
  ledcSetup(CHANNEL, FREQ, RESOLUTION);
  ledcAttachPin(LED_PIN, CHANNEL);
}

void loop() {
  for(int32_t dutyCycle = 0; dutyCycle <= 255; dutyCycle += 16){   
    ledcWrite(CHANNEL, dutyCycle);
    delay(200);
  }
}
