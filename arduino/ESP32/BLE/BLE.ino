#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

// https://www.uuidgenerator.net/
#define SERVICE_UUID        "6cfb4aac-53ab-4606-9065-f212a4aeeae1"
#define CHARACTERISTIC_UUID "441a32be-82fc-4be7-9460-98c21f29541a"

#define LED 2

class PrintCallback: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic* characteristic) {
    std::string value = characteristic->getValue();
    Serial.print("Value ");
    Serial.print(value.length());
    Serial.print(' ');
    for (int32_t i = 0; i < value.length(); i++) {
      Serial.print((int) value[i]);
      Serial.print(' ');
    }
    Serial.println();

    if (value.length() > 0) {
      value.pop_back(); // TODO from where is the trailing zero?
      if (value == "on") {
        digitalWrite(LED, HIGH);
      }
      if (value == "off") {
        digitalWrite(LED, LOW);
      }
    }
  }
};

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(115200);

  BLEDevice::init("TomBLE");
  BLEServer* server = BLEDevice::createServer();
  BLEService* service = server->createService(SERVICE_UUID);
  BLECharacteristic* characteristic = service->createCharacteristic(CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  characteristic->setCallbacks(new PrintCallback());
  characteristic->setValue("Hello TomBLE");
  service->start();
  BLEAdvertising* advertising = server->getAdvertising();
  advertising->start();

  delay(1000);

  Serial.println("Hello TomBLE");
}

void loop() {
  delay(10000);
  Serial.println("TomBLE Heartbeat");
  static uint32_t state = 0;
  state ^= 1;
  digitalWrite(LED, state == 0 ? LOW : HIGH);
}
