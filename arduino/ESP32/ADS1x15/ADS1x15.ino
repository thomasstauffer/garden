
#define BLINK_LED_GPIO 2
uint32_t blink_count = 0;

/*

ESP32
SCL GPIO22
SDA GPIO21
*/

#include <Wire.h>

#define ADS1x15_I2C_ADDRESS 0x48

#define ADS1x15_CONVERSION_REG 0x00
#define ADS1x15_CONFIG_REG 0x01
#define ADS1x15_LO_THRESH_REG 0x02
#define ADS1x15_HI_THRESH_REG 0x03

#define ADS1x15_CONFIG_OS (0b1 << 15)
#define ADS1x15_CONFIG_MUX_AIN0_AIN1 (0b000 << 12)
#define ADS1x15_CONFIG_MUX_AIN0_GND (0b100 << 12)
#define ADS1x15_CONFIG_PGA_2048 (0b010 << 9)
#define ADS1x15_CONFIG_PGA_1024 (0b011 << 9)
#define ADS1x15_CONFIG_PGA_512 (0b100 << 9)
#define ADS1x15_CONFIG_PGA_256 (0b101 << 9)

byte ads1x15_write(uint8_t reg, uint16_t value) {
  Wire.beginTransmission(ADS1x15_I2C_ADDRESS);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xff);
  Wire.write((value >> 0) & 0xff);
  Wire.endTransmission();
}

uint16_t ads1x15_read(uint8_t reg) {
  Wire.beginTransmission(ADS1x15_I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(ADS1x15_I2C_ADDRESS, 2);
  uint32_t hi = Wire.read();
  uint32_t lo = Wire.read();

  return (hi << 8) | lo;
}

double ads1x15_voltage() {
  ads1x15_write(ADS1x15_CONFIG_REG, ADS1x15_CONFIG_OS | ADS1x15_CONFIG_MUX_AIN0_GND | ADS1x15_CONFIG_PGA_256);
  delay(200);
  uint16_t adc = ads1x15_read(ADS1x15_CONVERSION_REG);
  double v = 0.256 * adc / 32766.0;
  return v;
}

void setup() {
  Serial.begin(115200);
  Serial.println("init");

  pinMode(BLINK_LED_GPIO, OUTPUT);

  Wire.begin();

  delay(1000);

  Serial.println("Conversion Register");
  Serial.println(ads1x15_read(0), HEX);
  Serial.println("Config Register");
  Serial.println(ads1x15_read(1), HEX);
  Serial.println("Lo_thresh Register");
  Serial.println(ads1x15_read(2), HEX);
  Serial.println("Hi_thresh Register");
  Serial.println(ads1x15_read(3), HEX);
}

void loop() {
  digitalWrite(BLINK_LED_GPIO, ((blink_count & 1) == 0) ? LOW : HIGH);
  blink_count += 1;

  Serial.print("Voltage: ");

  double v = ads1x15_voltage();

  char buffer[32];
  sprintf(buffer, "%f", v);
  Serial.println(buffer);

  delay(2000);
}
