#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define BLINK_LED_GPIO 2

#define BLE_APPEARANCE_GENERIC_THERMOMETER 768

#define SERVICE_TEMPERATURE_UUID "181a"
#define CHARACTERISTIC_TEMPERATURE_UUID "2a6e"

uint32_t count = 0;
bool isAdvertising = false;

BLECharacteristic* characteristic = NULL;
BLEServer* server = NULL;
BLEAdvertising* advertising = NULL;

void startAdvertising() {
	Serial.println("TomTS Start Adverstising");
	advertising->start();
	isAdvertising = true;
}

class ServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
        isAdvertising = false;
    }

    void onDisconnect(BLEServer* pServer) {
        startAdvertising();
    }
};

void setup() {
	Serial.begin(115200);

	Serial.println("TomTS Init");

	pinMode(BLINK_LED_GPIO, OUTPUT);

	BLEDevice::init("TomTS");
	server = BLEDevice::createServer();
	server->setCallbacks(new ServerCallbacks());
	BLEService* service = server->createService(SERVICE_TEMPERATURE_UUID);

	characteristic = service->createCharacteristic(CHARACTERISTIC_TEMPERATURE_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
	characteristic->addDescriptor(new BLE2902());

	service->start();

	advertising = server->getAdvertising();
	advertising->addServiceUUID(SERVICE_TEMPERATURE_UUID);
	advertising->setAppearance(BLE_APPEARANCE_GENERIC_THERMOMETER);

	startAdvertising();
}

void sendTemperature() {
	uint16_t t = (count % 1000) + 2000; // 3456 -> 34.56°C

	characteristic->setValue((uint8_t*) &t, sizeof(t));
	characteristic->notify();
}

void loop() {
	Serial.print("TomTS Heartbeat ");
	Serial.print(count);
	Serial.println();

	digitalWrite(BLINK_LED_GPIO, ((count & 1) == 0) ? LOW : HIGH);

	if (!isAdvertising) {
		sendTemperature();
	}

	count += 1;
	delay(isAdvertising ? 100 : 1000);
}
