
= Install

# adduser $USER dialout

# pip3 install pyserial

pyserial is used by esptool (which works with python3) inside the Arduino IDE

# ln -s /usr/bin/python3 ~/.local/bin/python

= Arduino IDE

File -> Preferences -> Additional Board Manager URLs

https://dl.espressif.com/dl/package_esp32_index.json

Tools -> Board -> Board Manager ...

ESP32

Boards

DOIT ESP32 DEVKIT V1

= Erase Flash

pip3 install esptool
erase_flash
