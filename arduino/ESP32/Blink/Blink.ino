#define LED 8

// ESP32 DEVKIT V1 = 2
// ESP32C3 SuperMini = 8

void setup() {
  Serial.begin(115200);
  
  pinMode(LED, OUTPUT);
}

void loop() {
  digitalWrite(LED, HIGH);
  Serial.println("HIGH");
  delay(200);
  digitalWrite(LED, LOW);
  Serial.println("LOW");
  delay(200);
}
