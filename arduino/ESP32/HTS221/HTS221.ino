
#define BLINK_LED_GPIO 2
uint32_t blink_count = 0;

/*
ST

x-nucleo-iks01a1.pdf
hts221.pdf

ESP32
SCL GPIO22
SDA GPIO21

*/

#include <Wire.h>

#define HTS221_I2C_ADDRESS 0x5f

#define HTS221_WHO_AM_I_REG 0x0f
#define HTS221_CTRL_REG1 0x20
#define HTS221_CTRL_REG2 0x21
#define HTS221_STATUS_REG 0x27
#define HTS221_TEMP_OUT_L 0x2a
#define HTS221_TEMP_OUT_H 0x2b

byte hts221_write_reg(uint8_t reg, uint8_t value) {
  Wire.beginTransmission(HTS221_I2C_ADDRESS);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

uint8_t hts221_read_reg8(uint8_t reg) {
  Wire.beginTransmission(HTS221_I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(HTS221_I2C_ADDRESS, 1);
  return Wire.read();
}

int16_t hts221_read_reg16(uint8_t reg) {
  // TODO continuous reading not possible?
  uint8_t l = hts221_read_reg8(reg);
  uint8_t h = hts221_read_reg8(reg + 1);
  return (h << 8) | l;
}

double hts221_temperature() {

  hts221_write_reg(HTS221_CTRL_REG1, 0x84);
  hts221_write_reg(HTS221_CTRL_REG2, 0x01);

  delay(10);

  //Serial.print("STATUS = 0x");
  //Serial.println(hts221_read_reg8(HTS221_STATUS_REG), HEX);

  int16_t t_out = hts221_read_reg16(HTS221_TEMP_OUT_L);

  uint32_t t0_degc_x8 = hts221_read_reg8(0x32);
  uint32_t t1_degc_x8 = hts221_read_reg8(0x33);
  uint32_t t1_t0_msb = hts221_read_reg8(0x35);
  int16_t t0_out = hts221_read_reg16(0x3c);
  int16_t t1_out = hts221_read_reg16(0x3e);

  double t0_degc = (t0_degc_x8 + ((t1_t0_msb & 0x03) << 8)) / 8.0;
  double t1_degc = (t1_degc_x8 + ((t1_t0_msb & 0x0c) << 6)) / 8.0;
  double t_degc = t0_degc + (t_out - t0_out) * (t1_degc - t0_degc) / (t1_out - t0_out);

  return t_degc;
}

void setup() {
  Serial.begin(115200);
  Serial.println("init");

  pinMode(BLINK_LED_GPIO, OUTPUT);

  Wire.begin();

  Serial.print("WHO_AM_I = Ox");
  Serial.println(hts221_read_reg8(HTS221_WHO_AM_I_REG), HEX);
}

void loop() {
	digitalWrite(BLINK_LED_GPIO, ((blink_count & 1) == 0) ? LOW : HIGH);
	blink_count += 1;

  Serial.println(hts221_temperature());

	delay(5000);
}
