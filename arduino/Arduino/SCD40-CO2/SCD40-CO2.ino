// https://sensirion.com/media/documents/E0F04247/631EF271/CD_DS_SCD40_SCD41_Datasheet_D1.pdf

#include <Wire.h>

#define I2C_ADDRESS 0x62

void setup() {
  Serial.begin(115200);
  Wire.begin();

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x21);
  Wire.write(0xB1);
  Wire.endTransmission();
}

uint32_t counter = 0;

void loop() {
  Serial.print("SDT40,");

  uint32_t h = counter / 360;
  uint32_t m = (counter / 6) % 60;
  uint32_t s = 10 * (counter % 6);

  Serial.print(h);
  Serial.print(":");
  Serial.print(m);
  Serial.print(":");
  Serial.print(s);
  Serial.print(",");

  Serial.print(counter * 10);
  Serial.print(",");

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0xEC);
  Wire.write(0x05);
  Wire.endTransmission();

  uint8_t data[9];
  Wire.requestFrom(I2C_ADDRESS, 9);
  for(uint32_t i = 0; i < sizeof(data); i += 1) {
    data[i] = Wire.read();
  }

  uint16_t co2raw = (data[0] << 8) | (data[1] << 0);
  uint16_t traw = (data[3] << 8) | (data[4] << 0);
  uint16_t rhraw = (data[6] << 8) | (data[7] << 0);

  double co2 = co2raw;
  double t = -45.0 + ((175.0 * traw) / 65535.0);
  double rh = 100.0 * (rhraw / 65535.0);

  Serial.print(co2);
  Serial.print(",");
  Serial.print(t);
  Serial.print(",");
  Serial.print(rh);
  Serial.println();

  counter += 1;

  delay(10000);
}
