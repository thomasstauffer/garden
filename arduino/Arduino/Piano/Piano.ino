void setup() {
  pinMode(3, OUTPUT);
  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);
}

void loop() {
  // Eingabe

  int taste1 = digitalRead(6);
  int taste2 = digitalRead(7);
  int taste3 = digitalRead(8);

  // Verarbeitung

  int frequenz = (taste1 * 400) + (taste2 * 200) + (taste3 * 100);

  // Ausgabe

  tone(3, frequenz);
}
