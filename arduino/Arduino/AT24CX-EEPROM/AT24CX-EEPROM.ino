// https://ww1.microchip.com/downloads/en/devicedoc/doc0670.pdf

#include <Wire.h>

void setup() {
  Serial.begin(115200);
  Wire.begin();
}

uint32_t counter = 0;

#define I2C_ADDRESS 0x50

void loop() {
  Serial.print("I2C ");
  Serial.print(counter);
  Serial.print(' ');

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x00);
  Wire.write(0x00);
  Wire.write(0xBE);
  Wire.write(0xEF);
  Wire.write(0x00);
  Wire.write(counter);
  Wire.endTransmission();
  
  delay(20);

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x00);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.requestFrom(I2C_ADDRESS, 4);
  while (Wire.available()) {
    uint8_t c = Wire.read();
    Serial.print(c, HEX);
    Serial.print(' ');
  }
  Serial.println();

  counter += 1;

  delay(1000);
}
