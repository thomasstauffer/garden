#include <mcp2515.h>

/*
Board: Arduino Nano
Processor: ATMega328P (Old Bootloader)
Programmer: ArduinoISP
*/

/*
Arduino IDE/Libraries: autowp-mcp2515

SCK D13
SI  D11
SO  D12
CS  D10
GND
VCC

https://github.com/autowp/arduino-mcp2515

CAN Controller: MCP2515
CAN Transceiver: TJA1050/C
*/

int n = 0;
MCP2515 mcp2515(10);
struct can_frame frame;

void setup() {
  Serial.begin(115200);
  
  MCP2515::ERROR error;

  error = mcp2515.reset();
  Serial.println(error);
  error = mcp2515.setBitrate(CAN_1000KBPS, MCP_8MHZ);
  Serial.println(error);
  //error = mcp2515.setLoopbackMode();
  error = mcp2515.setNormalMode();
  Serial.println(error);

  frame.can_id = 0x123456 | CAN_EFF_FLAG;
  frame.can_dlc = 4;
  frame.data[0] = 0xAA;
  frame.data[1] = 0xBB;
  frame.data[2] = 0xCC;
  frame.data[3] = 0xDD;
}

void loop() {
  if ((n % 10) == 0) {
    Serial.println("Send");
    mcp2515.sendMessage(&frame);
    frame.can_id += 1;
  }

  MCP2515::ERROR error = mcp2515.readMessage(&frame);

  if (error == MCP2515::ERROR_OK) {
    Serial.print("Read ");
    Serial.print(frame.can_id, HEX);
    Serial.print(" ");
    Serial.print(frame.can_dlc, HEX);
    for (int i = 0; i < frame.can_dlc; i += 1) {
      Serial.print(" ");
      Serial.print(frame.data[i], HEX);
    }
    Serial.println();
  } else if (error = MCP2515::ERROR_NOMSG) {
    //
  } else {
    Serial.print("Read Error ");
    Serial.print(error);
    Serial.println();
  }

  delay(100);

  n++;
}
