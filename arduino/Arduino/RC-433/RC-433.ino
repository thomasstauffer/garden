
#include <RCSwitch.h>

RCSwitch rc_switch = RCSwitch();

#define INPUT_PIN 12

int pinPrevious = 0;

void setup() {
  pinMode(INPUT_PIN, INPUT);
  digitalWrite(INPUT_PIN, HIGH);
  rc_switch.enableTransmit(10);
  Serial.begin(115200);
  pinPrevious = digitalRead(INPUT_PIN);
}

void loop() {
  int pin = digitalRead(INPUT_PIN);
  if ((pinPrevious == LOW) && (pin == HIGH))
  {
    Serial.println("Off");
    rc_switch.switchOff(1, 1);
    delay(100);
  }
  else if ((pinPrevious == HIGH) && (pin == LOW))
  {
    Serial.println("On");
    rc_switch.switchOn(1, 1);
    delay(100);
  }
  pinPrevious = pin;
}
