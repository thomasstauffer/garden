#define FASTLED_ALLOW_INTERRUPTS 0

#include <Bounce2.h>

// http://fastled.io/
// https://github.com/FastLED/FastLED/releases
#include <FastLED.h>

// https://github.com/NorthernWidget/DS3231
// https://forum.arduino.cc/index.php?topic=497808.0 A4 SDA & A5 SCL on Nano???
#include <DS3231.h>
#include <Wire.h>

#define ADJUST_HOUR_PIN 9
#define ADJUST_MINUTE_PIN 10

#define LED_PIN 5
#define NUM_LEDS 110
#define BRIGHTNESS_DAY 20
#define BRIGHTNESS_NIGHT 10
#define LED_TYPE WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
DS3231 Clock;

#define ENABLE_SERIAL

Bounce debouncerHours = Bounce();
Bounce debouncerMinutes = Bounce();

void setup() {
  pinMode(ADJUST_HOUR_PIN, INPUT_PULLUP);
  pinMode(ADJUST_MINUTE_PIN, INPUT_PULLUP);

  Wire.begin();
#ifdef ENABLE_SERIAL
  Serial.begin(9600);
  Serial.println("Hallo Wortuhr");
  Serial.println(Clock.oscillatorCheck());
#endif

  delay(500);

  Clock.setClockMode(false); // 24 Hour Clock

  pinMode(LED_BUILTIN, OUTPUT); 
  pinMode(LED_PIN, OUTPUT); 
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(UncorrectedColor /*TypicalSMD5050*/ );
  FastLED.setDither(0);
  FastLED.setBrightness(BRIGHTNESS_DAY);

  debouncerHours.attach(ADJUST_HOUR_PIN, INPUT_PULLUP);
  debouncerHours.interval(10);

  debouncerMinutes.attach(ADJUST_MINUTE_PIN, INPUT_PULLUP);
  debouncerMinutes.interval(10);
}

void enable_leds(bool condition, const uint8_t ls[], uint32_t length, CRGB color) {
  if(condition) {
    for(uint32_t i = 0; i < length; i += 1) {
      leds[ls[i]] = color;
    }
  }
}

const CRGB ON = 0xffffff;
const CRGB OFF = 0x000000;

void loop() {
  uint32_t time_seconds = Clock.getSecond();
  uint32_t time_minutes = Clock.getMinute();
  bool h12;
  bool pm;
  uint32_t time_hours = Clock.getHour(h12, pm);

  debouncerHours.update();
  if(debouncerHours.fell()) {
    Clock.setHour((time_hours + 1) % 24);
  }

  debouncerMinutes.update();
  if(debouncerMinutes.fell()) {
    Clock.setMinute((time_minutes + 1) % 60);
  }
  

  if((time_hours >= 6) && (time_hours <= 17)) {
    FastLED.setBrightness(BRIGHTNESS_DAY);
  } else {
    FastLED.setBrightness(BRIGHTNESS_NIGHT);
  }

#if 1
  // clear all
  for(uint32_t i = 0; i < NUM_LEDS; i++) {
    leds[i] = OFF;
  }

  uint32_t minutes_5 = time_minutes / 5; // 0 .. 11
  uint32_t minutes_modulo_5 = time_minutes % 5; // 0 .. 4

  const uint8_t es_esch_leds[] = { 0, 1, 3, 4, 5, 6 };
  const uint8_t fuef_leds[] = { 8, 9, 10 };
  const uint8_t zaeh_leds[] = { 21, 20, 19 };
  const uint8_t zwaenzg_leds[] = { 17, 16, 15, 14, 13, 12 };
  const uint8_t viertel_leds[] = { 22, 23, 24, 25, 26, 27, 28 };
  const uint8_t vor_leds[] = { 30, 31, 32 };
  const uint8_t ab_leds[] = { 42, 41 };
  const uint8_t halbi_leds[] = { 38, 37, 36, 35, 34 };

  const bool es_esch_lookup[] = { false, true, true, true, true, true, true, true, true, true, true, true };
  const bool fuef_lookup[] = { false, true, false, false, false, true, false, true, false, false, false, true };
  const bool zaeh_lookup[] = { false, false, true, false, false, false, false, false, false, false, true, false };
  const bool zwaenzg_lookup[] = { false, false, false, false, true, false, false, false, true, false, false, false };
  const bool viertel_lookup[] = { false, false, false, true, false, false, false, false, false, true, false, false };
  const bool vor_lookup[] = { false, false, false, false, false, true, false, false, true, true, true, true };
  const bool ab_lookup[] = { false, true, true, true, true, false, false, true, false, false, false, false };
  const bool halbi_lookup[] = { false, false, false, false, false, true, true, true, false, false, false, false };

  const bool stunde_korrektur[] = { false, false, false, false, false, true, true, true, true, true, true, true };

  bool es_esch = es_esch_lookup[minutes_5];
  bool fuef = fuef_lookup[minutes_5];
  bool zaeh = zaeh_lookup[minutes_5];
  bool zwaenzg = zwaenzg_lookup[minutes_5];
  bool viertel = viertel_lookup[minutes_5];
  bool vor = vor_lookup[minutes_5];
  bool ab = ab_lookup[minutes_5];
  bool halbi = halbi_lookup[minutes_5];

  enable_leds(es_esch, es_esch_leds, sizeof(es_esch_leds), ON);
  enable_leds(fuef, fuef_leds, sizeof(fuef_leds), ON);
  enable_leds(zaeh, zaeh_leds, sizeof(zaeh_leds), ON);
  enable_leds(zwaenzg, zwaenzg_leds, sizeof(zwaenzg_leds), ON);
  enable_leds(viertel, viertel_leds, sizeof(viertel_leds), ON);
  enable_leds(vor, vor_leds, sizeof(vor_leds), ON);
  enable_leds(ab, ab_leds, sizeof(ab_leds), ON);
  enable_leds(halbi, halbi_leds, sizeof(halbi_leds), ON);

  const uint8_t zwoelfi_leds[] = { 44, 45, 46, 47, 48, 49 };
  const uint8_t elfi_leds[] = { 51, 52, 53, 54 };
  const uint8_t fuefi_leds[] = { 65, 64, 63, 62 };
  const uint8_t saechsi_leds[] = { 60, 59, 58, 57, 57, 56, 55 };
  const uint8_t nueni_leds[] = { 66, 67, 68, 69 };
  const uint8_t achti_leds[] = { 70, 71, 72, 73, 74 };
  const uint8_t sebni_leds[] = { 87, 86, 85, 84, 83 };
  const uint8_t zweoei_leds[] = { 82, 81, 80, 79 };
  const uint8_t vieri_leds[] = { 88, 89, 90, 91, 92 };
  const uint8_t drue_leds[] = { 94, 95, 96 };
  const uint8_t eis_leds[] = { 109, 108, 107 };
  const uint8_t zaehni_leds[] = { 105, 104, 103, 102, 101 };

  uint32_t hours = time_hours; // 0 .. 11
  if(stunde_korrektur[minutes_5]) {
    hours += 1;
  }
  hours = hours % 12;

  
  bool zwoelfi = hours == 0;
  bool elfi = hours == 11;
  bool fuefi = hours == 5;
  bool saechsi = hours == 6;
  bool nueni = hours == 9;
  bool achti = hours == 8;
  bool sebni = hours == 7;
  bool zweoei = hours == 2;
  bool vieri = hours == 4;
  bool drue = hours == 3;
  bool eis = hours == 1;
  bool zaehni = hours == 10;

  enable_leds(zwoelfi, zwoelfi_leds, sizeof(zwoelfi_leds), ON);
  enable_leds(elfi, elfi_leds, sizeof(elfi_leds), ON);
  enable_leds(fuefi, fuefi_leds, sizeof(fuefi_leds), ON);
  enable_leds(saechsi, saechsi_leds, sizeof(saechsi_leds), ON);
  enable_leds(nueni, nueni_leds, sizeof(nueni_leds), ON);
  enable_leds(achti, achti_leds, sizeof(achti_leds), ON);
  enable_leds(sebni, sebni_leds, sizeof(sebni_leds), ON);
  enable_leds(zweoei, zweoei_leds, sizeof(zweoei_leds), ON);
  enable_leds(vieri, vieri_leds, sizeof(vieri_leds), ON);
  enable_leds(drue, drue_leds, sizeof(drue_leds), ON);
  enable_leds(eis, eis_leds, sizeof(eis_leds), ON);
  enable_leds(zaehni, zaehni_leds, sizeof(zaehni_leds), ON);

  const uint8_t minute_1_leds[] = { 76 };
  const uint8_t minute_2_leds[] = { 77 };
  const uint8_t minute_3_leds[] = { 98 };
  const uint8_t minute_4_leds[] = { 99 };

  bool minute_1 = minutes_modulo_5 >= 1;
  bool minute_2 = minutes_modulo_5 >= 2;
  bool minute_3 = minutes_modulo_5 >= 3;
  bool minute_4 = minutes_modulo_5 >= 4;

  enable_leds(minute_1, minute_1_leds, sizeof(minute_1_leds), ON);
  enable_leds(minute_2, minute_2_leds, sizeof(minute_2_leds), ON);
  enable_leds(minute_3, minute_3_leds, sizeof(minute_3_leds), ON);
  enable_leds(minute_4, minute_4_leds, sizeof(minute_4_leds), ON);
#endif

#ifdef ENABLE_SERIAL
  Serial.print(time_hours);
  Serial.print(":");
  Serial.print(time_minutes);
  Serial.print(":");
  Serial.print(time_seconds);
  Serial.println();
#endif

#if 0
  // binary clock
  uint32_t t = millis() / 1000;
  for(uint32_t i = 0; i < NUM_LEDS; i++) {
    uint32_t digit = 1 << i;
    leds[i] = ((t & digit) != 0) ? ON : OFF;
  }
#endif  

#if 0
  // every led
  uint32_t led = (millis() / 100) % NUM_LEDS;
  for(uint32_t i = 0; i < NUM_LEDS; i++) {
    leds[i] = (i == led) ? ON : OFF;
  }
#endif

  FastLED.show();
  delay(50);
}

