#include <IRremote.h>

// C:\Program Files (x86)\Arduino\libraries
// rename RobotIRremote

// Pin Definition

const int PIN_R = 9;
const int PIN_G = 5;
const int PIN_B = 10;
const int PIN_MOTOR = 6;
const int PIN_IR = 11;

float clamp(float value, float minimum, float maximum) {
  if (value > maximum) {
    return maximum;
  }
  if (value < minimum) {
    return minimum;
  }
  return value;
}

// LED

void ledRGB(float r, float g, float b) {
  r = clamp(r, 0.0, 1.0);
  g = clamp(g, 0.0, 1.0);
  b = clamp(b, 0.0, 1.0);
  analogWrite(PIN_R, 255 - (r * 130)); // min = 255, max = 125
  analogWrite(PIN_G, 255 - (g * 200)); // min = 255, max = 55
  analogWrite(PIN_B, 255 - (b * 254)); // min = 255, max = 1
}

void testLedRGB() {
  // black
  ledRGB(0.0, 0.0, 0.0);
  delay(1000);

  // red
  ledRGB(1.0, 0.0, 0.0);
  delay(1000);

  // green
  ledRGB(0.0, 1.0, 0.0);
  delay(1000);

  // blue
  ledRGB(0.0, 0.0, 1.0);
  delay(1000);

  // magenta
  ledRGB(1.0, 0.0, 1.0);
  delay(1000);

  // yellow
  ledRGB(1.0, 1.0, 0.0);
  delay(1000);

  // cyan
  ledRGB(0.0, 1.0, 1.0);
  delay(1000);

  // white
  ledRGB(1.0, 1.0, 1.0);
  delay(1000);
}

// hue = 0.0 - 6.0, 0.0 = red, 2.0 = green, 4.0 = blue
void ledHue(float hue) {
  hue = clamp(hue, 0.0, 6.0); // TODO float module on Arduino?

  float r = 0.0;
  float g = 0.0;
  float b = 0.0;
  float up = hue - int(hue);
  float down = 1.0 - up;

  if (hue < 1.0) {
    r = 1.0;
    g = up;
  } else if (hue < 2.0) {
    r = down;
    g = 1.0;
  } else if (hue < 3.0) {
    g = 1.0;
    b = up;
  } else if (hue < 4.0) {
    g = down;
    b = 1.0;
  } else if (hue < 5.0) {
    r = up;
    b = 1.0;
  } else {
    r = 1.0;
    b = down;
  }

  ledRGB(r, g, b);
}

void testLedHue() {
  for (float color = 0.0; color < 6.0; color += 0.01) {
    ledHue(color);
    delay(50);
  }
}

// Motor

// rpm 0.0 - 5.0
void motorRPM(float rpm) {
  rpm = clamp(rpm, 0.0, 5.0);
  int value = rpm * 255.0 / 5.0;
  analogWrite(PIN_MOTOR, value);
}

void testMotor() {
  const float epsilon = 1e-3;
  for(float rpm = 0.0; rpm <= (5.0 + epsilon); rpm += 1.0) {
    Serial.println(rpm);
    motorRPM(rpm);
    delay(10000);
  }
}

// IR

const unsigned long IR_CHM = 0xffa25d;
const unsigned long IR_CH = 0xff629d;
const unsigned long IR_CHP = 0xffe21d;
const unsigned long IR_PREV = 0xff22dd;
const unsigned long IR_NEXT = 0xff02fd;
const unsigned long IR_PLAYPAUSE = 0xffc23d;
const unsigned long IR_VOLM = 0xffe01f;
const unsigned long IR_VOLP = 0xffa857;
const unsigned long IR_EQ = 0xff906f;
const unsigned long IR_0 = 0xff6897;
const unsigned long IR_100P = 0xff9867;
const unsigned long IR_200P = 0xffb04f;
const unsigned long IR_1 = 0xff30cf;
const unsigned long IR_2 = 0xff18e7;
const unsigned long IR_3 = 0xff7a85;
const unsigned long IR_4 = 0xff10ef;
const unsigned long IR_5 = 0xff38c7;
const unsigned long IR_6 = 0xff5aa5;
const unsigned long IR_7 = 0xff42bd;
const unsigned long IR_8 = 0xff4ab5;
const unsigned long IR_9 = 0xff52ad;

struct {
  unsigned long code;
  const char* name;
} IR_CODE_NAME[] = {
  {IR_CHM, "CH-"},
  {IR_CH, "CH"},
  {IR_CHP, "CH+"},
  {IR_PREV, "PREV"},
  {IR_NEXT, "NEXT"},
  {IR_PLAYPAUSE, "PLAY/PAUSE"},
  {IR_VOLM, "VOL-"},
  {IR_VOLP, "VOL+"},
  {IR_EQ, "EQ"},
  {IR_0, "0"},
  {IR_100P, "100+"},
  {IR_200P, "200+"},
  {IR_1, "1"},
  {IR_2, "2"},
  {IR_3, "3"},
  {IR_4, "4"},
  {IR_5, "5"},
  {IR_6, "6"},
  {IR_7, "7"},
  {IR_8, "8"},
  {IR_9, "9"},
};

const int IR_CODE_NAME_COUNT = sizeof(IR_CODE_NAME) / sizeof(IR_CODE_NAME[0]);

bool irCodeValid(unsigned long code) {
  for (int i = 0; i < IR_CODE_NAME_COUNT; i++) {
    if (IR_CODE_NAME[i].code == code) {
      return true;
    }
  }
  return false;
}

const char* irCodeToName(unsigned long code) {
  for (int i = 0; i < IR_CODE_NAME_COUNT; i++) {
    if (IR_CODE_NAME[i].code == code) {
      return IR_CODE_NAME[i].name;
    }
  }
  return "";
}

// Main

IRrecv g_irrecv(PIN_IR);

void setup() {
  Serial.begin(9600);

  // Additional 5V
  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);

  // Additional GND
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);

  // R, G, B
  pinMode(PIN_R, OUTPUT);
  pinMode(PIN_G, OUTPUT);
  pinMode(PIN_B, OUTPUT);

  // Motor
  pinMode(PIN_MOTOR, OUTPUT);

  // IR

  g_irrecv.enableIRIn();

  // Welcome

  Serial.println("Manuela & Nicolas");
}

float g_hue = 0.0;
float g_rpm = 0.0;

// only called if irCode is valid
void irHandle(unsigned long irCode) {
  Serial.println(irCodeToName(irCode));

  // read

  if (irCode == IR_PREV) {
    g_hue -= 0.2;
  } else if (irCode == IR_NEXT) {
    g_hue += 0.2;
  } else if (irCode == IR_VOLP) {
    g_rpm += 0.2;
  } else if (irCode == IR_VOLM) {
    g_rpm -= 0.2;
  }

  // limit

  if (g_hue < 0.0) {
    g_hue = 6.0;
  }
  if (g_hue > 6.0) {
    g_hue = 0.0;
  }

  g_rpm = clamp(g_rpm, 0.0, 5.0);

  // output

  Serial.println(g_hue);
  Serial.println(g_rpm);

  ledHue(g_hue);
  motorRPM(g_rpm);
}

void loop() {
  //dtestLedRGB();
  //testLedHue();
  //testMotor();

  decode_results results;
  if (g_irrecv.decode(&results)) {
    unsigned long irCode = results.value;
    if (irCodeValid(irCode)) {
      irHandle(irCode);
    }
    g_irrecv.resume();
  }
}

