#define FASTLED_ALLOW_INTERRUPTS 0

#include <Bounce2.h>

// http://fastled.io/
// https://github.com/FastLED/FastLED/releases
#include <FastLED.h>

#define ADJUST_HOUR_PIN 9
#define ADJUST_MINUTE_PIN 10

#define LED_PIN 5
#define NUM_LEDS 30
#define BRIGHTNESS_DAY 40
#define BRIGHTNESS_NIGHT 20
#define LED_TYPE WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define ENABLE_SERIAL_DEBUG

const CRGB COLOR_ON = 0xffffff;
const CRGB COLOR_OFF = 0x000000;

uint32_t time_hour_correction = 0;
uint32_t time_minute_correction = 0;

Bounce debouncerHours = Bounce();
Bounce debouncerMinutes = Bounce();

void setup() {
  pinMode(ADJUST_HOUR_PIN, INPUT_PULLUP);
  pinMode(ADJUST_MINUTE_PIN, INPUT_PULLUP);

#ifdef ENABLE_SERIAL_DEBUG
  Serial.begin(9600);
  Serial.println("Hallo Wortuhr");
#endif

  delay(500);

  pinMode(LED_BUILTIN, OUTPUT); 
  pinMode(LED_PIN, OUTPUT); 
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(UncorrectedColor /*TypicalSMD5050*/ );
  FastLED.setDither(0);
  FastLED.setBrightness(BRIGHTNESS_DAY);

  debouncerHours.attach(ADJUST_HOUR_PIN, INPUT_PULLUP);
  debouncerHours.interval(10);

  debouncerMinutes.attach(ADJUST_MINUTE_PIN, INPUT_PULLUP);
  debouncerMinutes.interval(10);
}

void enable_leds(bool condition, const uint8_t ls[], uint32_t length, CRGB color) {
  if(condition) {
    for(uint32_t i = 0; i < length; i += 1) {
      leds[ls[i]] = color;
    }
  }
}

void loop() {
  debouncerHours.update();
  if(debouncerHours.fell()) {
    time_hour_correction += 1;
#ifdef ENABLE_SERIAL_DEBUG
  Serial.print("Hour Correction: ");
  Serial.print(time_hour_correction);
  Serial.println();
#endif
  }
  time_hour_correction = time_hour_correction % 24;

  debouncerMinutes.update();
  if(debouncerMinutes.fell()) {
    time_minute_correction += 1;
#ifdef ENABLE_SERIAL_DEBUG
  Serial.print("Minute Correction");
  Serial.print(time_minute_correction);
  Serial.println();
#endif
  }
  time_minute_correction = time_minute_correction % 60;

  const uint32_t seconds = (millis() / 3) + (time_hour_correction * 3600) + (time_minute_correction * 60);

  const uint32_t time_seconds = (seconds) % 60; // 0 .. 59
  const uint32_t time_minutes = (seconds / 60) % 60; // 0 .. 59
  const uint32_t time_hours = (seconds / (60 * 60)) % 12; // 0 .. 11

  if((time_hours >= 6) && (time_hours <= 17)) {
    FastLED.setBrightness(BRIGHTNESS_DAY);
  } else {
    FastLED.setBrightness(BRIGHTNESS_NIGHT);
  }

#if 1
  // clear all
  for(uint32_t i = 0; i < NUM_LEDS; i++) {
    leds[i] = COLOR_OFF;
  }

  const uint32_t minutes_5 = time_minutes / 5; // 0 .. 11

  const uint8_t es_esch_bald_leds[] = { 0, 1, 3, 4, 5, 6 /*, 13, 14, 15, 16 */ };

  const bool es_esch_bald_lookup[] = { true, true, true, false, false, false, false, false, false, false, false, false };

  const bool es_esch_bald = es_esch_bald_lookup[minutes_5];

  enable_leds(es_esch_bald, es_esch_bald_leds, sizeof(es_esch_bald_leds), COLOR_ON);

  const uint8_t eis_leds[] = { 24 };
  const uint8_t zweoei_leds[] = { 11 };
  const uint8_t drue_leds[] = { 10 };
  const uint8_t vieri_leds[] = { 9 };
  const uint8_t fuefi_leds[] = { 8 };
  const uint8_t saechsi_leds[] = { 17 };
  const uint8_t sebni_leds[] = { 18 };
  const uint8_t achti_leds[] = { 19 };
  const uint8_t nueni_leds[] = { 20 };
  const uint8_t zaehni_leds[] = { 21, 22 };
  const uint8_t elfi_leds[] = { 23, 24 };
  const uint8_t zwoelfi_leds[] = { 24, 25 };

  const bool stunde_korrektur[] = { false, false, false, false, false, true, true, true, true, true, true, true };
  uint32_t hours = time_hours; // 0 .. 11
  if(stunde_korrektur[minutes_5]) {
    //hours += 1;
  }
  hours = hours % 12;

  const bool eis = hours == 1;
  const bool zweoei = hours == 2;
  const bool drue = hours == 3;
  const bool vieri = hours == 4;
  const bool fuefi = hours == 5;
  const bool saechsi = hours == 6;
  const bool sebni = hours == 7;
  const bool achti = hours == 8;
  const bool nueni = hours == 9;
  const bool elfi = hours == 11;
  const bool zaehni = hours == 10;
  const bool zwoelfi = hours == 0;

  enable_leds(eis, eis_leds, sizeof(eis_leds), COLOR_ON);
  enable_leds(zweoei, zweoei_leds, sizeof(zweoei_leds), COLOR_ON);
  enable_leds(drue, drue_leds, sizeof(drue_leds), COLOR_ON);
  enable_leds(vieri, vieri_leds, sizeof(vieri_leds), COLOR_ON);
  enable_leds(fuefi, fuefi_leds, sizeof(fuefi_leds), COLOR_ON);
  enable_leds(saechsi, saechsi_leds, sizeof(saechsi_leds), COLOR_ON);
  enable_leds(sebni, sebni_leds, sizeof(sebni_leds), COLOR_ON);
  enable_leds(achti, achti_leds, sizeof(achti_leds), COLOR_ON);
  enable_leds(nueni, nueni_leds, sizeof(nueni_leds), COLOR_ON);
  enable_leds(zaehni, zaehni_leds, sizeof(zaehni_leds), COLOR_ON);
  enable_leds(elfi, elfi_leds, sizeof(elfi_leds), COLOR_ON);
  enable_leds(zwoelfi, zwoelfi_leds, sizeof(zwoelfi_leds), COLOR_ON);

  const uint8_t viertel_1_leds[] = { 29 };
  const uint8_t viertel_2_leds[] = { 28 };
  const uint8_t viertel_3_leds[] = { 27 };

  const bool viertel_1 = time_minutes >= 15;
  const bool viertel_2 = time_minutes >= 30;
  const bool viertel_3 = time_minutes >= 45;

  enable_leds(viertel_1, viertel_1_leds, sizeof(viertel_1_leds), COLOR_ON);
  enable_leds(viertel_2, viertel_2_leds, sizeof(viertel_2_leds), COLOR_ON);
  enable_leds(viertel_3, viertel_3_leds, sizeof(viertel_3_leds), COLOR_ON);
#endif

#ifdef ENABLE_SERIAL_DEBUG
  Serial.print(time_hours);
  Serial.print(":");
  Serial.print(time_minutes);
  Serial.print(":");
  Serial.print(time_seconds);
  Serial.println();
#endif

#if 0
  // every led
  uint32_t led = (millis() / 300) % NUM_LEDS;
  for(uint32_t i = 0; i < NUM_LEDS; i++) {
    leds[i] = (i == led) ? COLOR_ON : COLOR_OFF;
  }
#endif

  FastLED.show();
  delay(100);
}
