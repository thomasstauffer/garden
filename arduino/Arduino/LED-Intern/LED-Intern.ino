// setup() wird einmal ausgeführt
void setup() {
  // Pin 13 wird als Ausgang definiert
  pinMode(13, OUTPUT);
}

// loop() wird immer und immer wieder, unendlich lange ausgeführt
void loop() {
  // Pin wird eingeschalten
  digitalWrite(13, HIGH);
  // warte die angegebene Anzahl von Millisekunden (1000 Millisekunde = 1 Sekunde)
  delay(2000);
  // Pin wird ausgeschalten
  digitalWrite(13, LOW);
  // Wiederum warten
  delay(2000);   
}
