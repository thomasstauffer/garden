// https://www.nxp.com/docs/en/data-sheet/MPL3115A2S.pdf

#include <Wire.h>

#define I2C_ADDRESS 0x60

bool measure_altitude = false;
double altitude_pressure_min = 1.0e6;
double altitude_pressure_max = -1.0e6;
double altitude_pressure_filtered = 400;
int count = 0;

void setup() {
  Serial.begin(115200);
  Wire.begin();

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x26);
  Wire.write(0x38 | (measure_altitude << 7));
  Wire.endTransmission();
  delay(10);

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x13);
  Wire.write(0x07);
  Wire.endTransmission();
  delay(10);

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x26);
  Wire.write(0x39 | (measure_altitude << 7));
  Wire.endTransmission();
  delay(10);

}

uint8_t read(uint8_t reg) {
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission(false);
  Wire.requestFrom(I2C_ADDRESS, 1);
  delay(5);
  uint8_t result = 0xFF;
  while (Wire.available()) {
    result = Wire.read();
  }
  return result;
}

void loop() {
  const uint8_t status = read(0x00);
  const uint32_t out_p_msb = read(0x01);
  const uint32_t out_p_csb = read(0x02);
  const uint32_t out_p_lsb = read(0x03);
  const uint32_t out_t_msb = read(0x04);
  const uint32_t out_t_lsb = read(0x05);

  const uint32_t out = (out_p_msb << 12) + (out_p_csb << 4) + (out_p_lsb >> 4);
  const double altitude_pressure = measure_altitude ? (out / 16) : (out / 4);
  const double temperature = out_t_msb + ((out_t_lsb >> 4) / 16.0);

  if(count > 5) {
    altitude_pressure_min = min(altitude_pressure_min, altitude_pressure);
    altitude_pressure_max = max(altitude_pressure_max, altitude_pressure);
  }

  altitude_pressure_filtered += (altitude_pressure - altitude_pressure_filtered) * 0.05;

  if (count % 10 == 0) {
    Serial.print("MPL3115A2 ");
 
    /*
    Serial.print(status, HEX);
    Serial.print(' ');
    Serial.print(out_p_msb, HEX);
    Serial.print(' ');
    Serial.print(out_p_csb, HEX);
    Serial.print(' ');
    Serial.print(out_p_lsb, HEX);
    Serial.print(' ');
    Serial.print(out_t_msb, HEX);
    Serial.print(' ');
    Serial.print(out_t_lsb, HEX);
    Serial.print(' ');
    */
    
    Serial.print(altitude_pressure);
    Serial.print(' ');
    Serial.print(altitude_pressure_min);
    Serial.print(' ');
    Serial.print(altitude_pressure_max);
    Serial.print(' ');
    Serial.print(altitude_pressure_filtered);
    Serial.print(' ');
    Serial.print(temperature);
  
    Serial.println();
  }

  count += 1;

  delay(100);
}
