
/*
https://circuitcellar.com/research-design-hub/design-solutions/putting-1-wire-protocol-into-action/
https://www.maximintegrated.com/en/design/technical-documents/tutorials/2/214.html
https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf
https://de.wikipedia.org/wiki/1-Wire
*/

#define OW_GPIO_PIN 13
#define OW_2GPIO_RX_PIN 12
#define OW_2GPIO_TX_PIN 11

//#define OW_GPIO
#define OW_2GPIO
//#define OW_UART

void ow_pulse(uint32_t microseconds) {
#if defined(OW_GPIO)
  pinMode(OW_GPIO_PIN, OUTPUT);
  digitalWrite(OW_GPIO_PIN, LOW);
  delayMicroseconds(microseconds);
  pinMode(OW_GPIO_PIN, INPUT_PULLUP);
  delayMicroseconds(1);
#elif defined(OW_2GPIO)
  pinMode(OW_2GPIO_TX_PIN, OUTPUT);
  digitalWrite(OW_2GPIO_TX_PIN, LOW);
  delayMicroseconds(microseconds);
  digitalWrite(OW_2GPIO_TX_PIN, HIGH);
  delayMicroseconds(1);
#elif defined(OW_UART)
  if (microseconds == 1) {
    Serial.flush();
    Serial.print((char) 0xFF);
  } else if (microseconds == 60) {
    Serial.flush();
    Serial.print((char) 0xC0);
  } else if (microseconds == 480) {
    Serial.begin(9600);
    Serial.flush();
    Serial.print((char) 0xF0);
    Serial.begin(115200);
  }
#else
#error "#define OW_GPIO or OW_2GPIO"
#endif
}

bool ow_read_state() {
#if defined(OW_GPIO)
  bool result = true;
  for (uint32_t i = 0; i < 100; i += 1) {
    if (digitalRead(OW_GPIO_PIN) == LOW) {
      result = false;
    }
    delayMicroseconds(1);
  }
  return result;
#elif defined(OW_2GPIO)
  bool result = true;
  for (uint32_t i = 0; i < 100; i += 1) {
    if (digitalRead(OW_2GPIO_RX_PIN) == LOW) {
      result = false;
    }
    delayMicroseconds(1);
  }
  return result;
#elif defined(OW_UART)
  delayMicroseconds(100);
  Serial.read();
  return Serial.read() == 0xFF;
#endif
}

bool ow_reset() {
  ow_pulse(480);
  bool presence = !ow_read_state();
  return presence;
}

void ow_write_bit(bool value) {
  ow_pulse(value ? 1 : 60);
}

void ow_write_bytes(const uint8_t* buffer, uint16_t numberOfBytes) {
  for (uint16_t byteIndex = 0; byteIndex < numberOfBytes; byteIndex += 1) {
    for (uint8_t bitIndex = 0; bitIndex < 8; bitIndex += 1) {
      uint8_t bit_value = (buffer[byteIndex] >> bitIndex) & 0b1;
      ow_write_bit(bit_value);
      delayMicroseconds(100);
    }
  }
}

bool ow_read_bit() {
  ow_pulse(1);
  bool state = ow_read_state();
  return state;
}

uint8_t ow_read_bytes(uint8_t* buffer, uint16_t numberOfBytes) {
  for(uint16_t byteIndex = 0; byteIndex < numberOfBytes; byteIndex += 1) {
    uint8_t result = 0;
    for(uint8_t bitIndex = 0; bitIndex < 8; bitIndex += 1) {
      result |= (ow_read_bit() & 0b1) << bitIndex;
      delayMicroseconds(100);
    }
    buffer[byteIndex] = result;
  }
}

/* 0x28 for DS18B20 */
#define READ_ROM 0x33
#define SKIP_ROM 0xCC
#define CONVERT_T 0x44
#define WRITE_SCRATCHPAD 0x4E
#define READ_SCRATCHPAD 0xBE
#define COPY_SCRATCHPAD 0x48

void ow_readrom(uint8_t* buf)
{
  uint8_t cmd[1];
  cmd[0] = READ_ROM;
  ow_reset();
  ow_write_bytes(cmd, 1);
  ow_read_bytes(buf, 8);
}

void ow_command(uint8_t command, uint8_t* buf, uint16_t numberOfBytes) {
  uint8_t cmd[2];
  cmd[0] = SKIP_ROM;
  cmd[1] = command;
  ow_reset();
  ow_write_bytes(cmd, 2);
  ow_read_bytes(buf, numberOfBytes);
  delay(100);
}

void SerialPrintBuf(uint8_t* buf, uint32_t numberOfBytes) {
  char output[32];
  for (uint32_t i = 0; i < numberOfBytes; i += 1) {
    snprintf(output, sizeof(output), "%.2X", buf[i]);
    Serial.print(output);
  }
}

void setup() {
  pinMode(OW_GPIO_PIN, INPUT_PULLUP);
  pinMode(OW_2GPIO_RX_PIN, INPUT);
  pinMode(OW_2GPIO_TX_PIN, OUTPUT);
  digitalWrite(OW_2GPIO_TX_PIN, HIGH);

  Serial.begin(115200);
  Serial.println("========================================");

  uint8_t buf[32];

  bool presence = ow_reset();
  if (!presence) {
    Serial.println("No Presence After OneWire Reset");
  }

  ow_readrom(buf);
  Serial.print("ROMID 0x");
  SerialPrintBuf(buf, 8);
  Serial.println();

  ow_command(READ_SCRATCHPAD, buf, 9);
  Serial.print("SCRATCHPAD 0x");
  SerialPrintBuf(buf, 9);
  Serial.println();

  delay(3000);
}

void loop() {
  uint8_t buf[32];

  ow_command(CONVERT_T, buf, 0);
  delay(500);

  ow_command(READ_SCRATCHPAD, buf, 9);
  Serial.print("SCRATCHPAD 0x");
  SerialPrintBuf(buf, 9);
  Serial.println();
  uint32_t temperature_int = (buf[0] << 0) + (buf[1] << 8);
  float temperature_float = temperature_int * 0.0625f;
  Serial.println(temperature_float);
  delay(500);
}
