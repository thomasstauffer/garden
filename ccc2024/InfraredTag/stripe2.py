
# adafruit-circuitpython-neopixel
# adafruit-circuitpython-colorsys

import time
import board
import digitalio
import neopixel
import colorsys

#pin = digitalio.DigitalInOut(board.D13)
#pin.direction = digitalio.Direction.OUTPUT
#pin.drive_mode = digitalio.DriveMode.OPEN_DRAIN
#pin.deinit()

pixels = neopixel.NeoPixel(board.D13, 5, auto_write=False)
pixels.fill((0, 0, 0))
pixels.show()

def cycle():
    h = 0
    while True:
        h = (h + 1) % 500
        c = colorsys.hsv_to_rgb(h / 500.0, 1.0, 0.5)
        pixels.fill((c[0] * 255, c[1] * 255, c[2] * 255))
        pixels.show()
        time.sleep(0.01)

cycle()

# h=0-1, s=0-1, v=0-1
def hsv_to_rgb(h, s, v):
    h = int(h * 360) % 360
    c = v * s
    x = c * (1 - abs(((h/60.0) % 2) - 1))
    m = v - c
    rgb_map = [(c, x, 0), (x, c, 0), (0, c, x), (0, x, c), (x, 0, c), (c, 0, x)]
    rgb = rgb_map[h // 60]
    return list(map(lambda n: (n + m) * 1, rgb))
