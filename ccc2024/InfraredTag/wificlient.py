import wifi
import time
import socketpool

wifi.radio.connect(ssid='38c3-ir-tag', password='123456789')
pool = socketpool.SocketPool(wifi.radio)
sock = pool.socket(pool.AF_INET, pool.SOCK_DGRAM)

print('AP Address', wifi.radio.ipv4_gateway)
print('Our Address', wifi.radio.ipv4_address)

i = 0
while True:
    tx = bytearray(8)
    tx[0] = i % 100
    i += 1
    sock.sendto(tx, (str(wifi.radio.ipv4_gateway), 44444))
    time.sleep(1.0)
