import board
import pulseio
import time

def pulseio_read(pulses):
    l = []
    for i in range(len(pulses)):
        l += [ pulses[i] ]
    pulses.clear()
    return l

def rx():
    pulses = pulseio.PulseIn(board.D15, maxlen=32, idle_state=True)
    while True:
        if len(pulses) > 0:
            rx = pulseio_read(pulses)
            print('Rx: ' + str(rx))
        time.sleep(0.1)

rx()
