import wifi
import time
import socketpool

wifi.radio.start_ap(ssid='38c3-ir-tag', password='123456789')
pool = socketpool.SocketPool(wifi.radio)
sock = pool.socket(pool.AF_INET, pool.SOCK_DGRAM)

sock.bind(('0.0.0.0', 44444))
sock.settimeout(1.0)

buffer = bytearray(16)

while True:
    try:
        size, addr = sock.recvfrom_into(buffer)
        print(size, addr, buffer)
    except OSError:
        print('No Packet')
