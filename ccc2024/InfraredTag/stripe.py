
# adafruit-circuitpython-neopixel

import board
import neopixel

pixels = neopixel.NeoPixel(board.D13, 5, auto_write=False)
pixels.fill((0, 255, 0))
pixels.show()
