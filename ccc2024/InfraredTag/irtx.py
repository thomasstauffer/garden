import array
import board
import pulseio
import time

def tx():
    pulses = pulseio.PulseOut(board.D15, frequency=38000, duty_cycle=32768)
    while True:
        tx = [2000, 4000, 6000, 8000, 10000]
        print('Tx: ' + str(tx))
        pulses.send(array.array('H', tx)) 
        time.sleep(2)

tx()
