import time
import board
import digitalio

led = digitalio.DigitalInOut(board.D14)
led.direction = digitalio.Direction.OUTPUT

switch = digitalio.DigitalInOut(board.D12) 
switch.direction = digitalio.Direction.INPUT
switch.pull = digitalio.Pull.UP

while True:
    led.value = switch.value