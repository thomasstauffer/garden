import time
import board
import digitalio

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
while True:
    time.sleep(0.2)
    led.value = 1
    time.sleep(0.2)
    led.value = 0
