extends RigidBody3D

func _integrate_forces(state):
	const directionLeftRight = Vector3(0.0, 0.0, 1.0)
	const directionForwardBackward = Vector3(1.0, 0.0, 0.0)
	const torqueMultiplier = 10.0

	var torque = Vector3.ZERO
	if Input.is_action_pressed("Left"):
		torque += directionLeftRight * torqueMultiplier
	if Input.is_action_pressed("Right"):
		torque -= directionLeftRight * torqueMultiplier
	if Input.is_action_pressed("Backward"):
		torque += directionForwardBackward * torqueMultiplier
	if Input.is_action_pressed("Forward"):
		torque -= directionForwardBackward * torqueMultiplier

	state.apply_torque(torque)

func _process(delta):
	if self.position.y < 0.0:
		get_tree().reload_current_scene()

func _on_body_entered(body: Node) -> void:
	if body.name == "Finish":
		var nextLevel = body.get_meta("NextLevel")
		if nextLevel == null:
			get_tree().quit()
		else:
			get_tree().change_scene_to_file("res://" + nextLevel + ".tscn")
