extends RigidBody3D

func _integrate_forces(state):
	var torque = Vector3.ZERO
	if Input.is_action_pressed("Left"):
		torque += Vector3(0.0, 0.0, 5.0)
	if Input.is_action_pressed("Right"):
		torque -= Vector3(0.0, 0.0, 5.0)
	if Input.is_action_pressed("Forward"):
		torque -= Vector3(5.0, 0.0, 0.0)
	if Input.is_action_pressed("Backward"):
		torque += Vector3(5.0, 0.0, 0.0)

	state.apply_torque(torque)

func _process(delta):
	if self.position.y < 0.0:
		get_tree().reload_current_scene()

func _on_body_entered(body: Node) -> void:
	if body.name == "Finish":
		get_tree().quit()
