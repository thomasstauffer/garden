
# Features

- Move Ball with Keyboard
- Camera follows Player
- Collide with Objects

# Scene Tree

Node3D - Level1
    CSGBox3D "Floor"
        Size x=50 y=1 z=50
        Use Collision - On
        Material - StandardMaterial3D
            Albedo r=0 g=64 b=0
    CSGBox3D "Collider"
        Size x=10 y=10 z=10
        Use Collision - On
        Material - StandardMaterial3D
            Albedo r=0 g=0 b=255
        Transform
            Position x=-10 y=-1 z=10
            Rotation x=30 y=0 z=0
    CSGCylinder3D "Finish"
        Radius 1
        Height 1
        Use Collision - On
        Material - StandardMaterial3D
            Albedo r=255 g=0 b=0
        Transform
            Position x=-10 y=5 z=2
    DirectionalLight3D "Light"
        Shadow - Enabled - On
    RigidBody3D "Player"
        Contact Monitor - On
        Max Contacts Reported 1
        Transform
            Position x=0 y=10 z=0
        MeshInstance3D
            SphereMesh
                Radius 0.5
                Height 1
        CollisionShape3D
            SphereSphape3D
                Radius 0.5
        RemoteTransform3D
            Update
                Position - On
    Node3D "Camera"
        Camera3D
            Transform
                Position x=0 y=8 z=8
                Rotation x=-45 y=0 z=0

# Project Settings

Project - Project Settings - Input Map
    Add New Action
        Left
            Left
            A
        Right
            Right
            D
        Forward
            Up
            W
        Backward
            Down
            S
