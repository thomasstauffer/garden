
export const RULES_TEST = `

# comment

TEST = TEST_YESNO TEST_YESNO_CSV TEST_CAPITALIZE TEST_MATH TEST_PARENTHESES TEST_REPEAT
TEST_YESNO = "y" | "n"
TEST_YESNO_CSV = ,"y, n"
TEST_CAPITALIZE = capitalize( TEST_YESNO )
TEST_MATH = $2+2+clamp(1,2,10) $1+2*3 $gauss(20,5) $gausslimit(20,5,10) $(1+2)*3 $1+2*3
TEST_PARENTHESES = "a" ( "b" | "c" )
TEST_REPEAT = { "A" }2*2

ABCDE = ABC DE
ABC = "a" "b" "c"
DE = D E
D = "d"
E = "e"

`;
