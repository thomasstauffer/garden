
export const RULES_DE = `

START = TIER | BLUME

TIER = "Tier:" TEIL "Tier:" TEIL "Spezial:" SPEZIAL "Spezial:" SPEZIAL "Farbe:" FARBE "Grösse:" GROESSE_REL "Sinn:" SINN

SPEZIAL = ,"Mehr Augen, Mehr Beine, Mehr Arme, Mehr Organe, Krallen, Federn, Flügel, Beine, Pelz, Haare, Flosse, Tentakel, Stachel, Spezieller Sinn, Farbe Gefühl, Farbe Umgebung, Langlebigkeit, Hübsch, Hässlich"

SINN = ,"Infraschall, Schall, Ultraschall, UV-Lich, Infrarot-Licht, Licht, Elektromagnetische Wellen, Druck, Wärme, Feuchtigkeit, PH-Wert, Säure"

TEIL = LUFT | WASSER | LAND | AMPHIBIEN

LAND = ,"Eidechse, Katze, Hund, Löwe, Giraffe, Elefant, Affe, Fuchs, Bär, Pferd, Rind, Schwein, Ziege, Kamel, Mensch" "Beine:" $dice(2,4)-2
LUFT = ,"Adler, Biene, Ente, Eule, Falter, Flamingo, Gans, Geier, Kolibri, Libelle, Papagei, Storch" "Flügel:" $dice(2,4)-2
WASSER = ,"Wal, Tintenfisch, Delphin, Hai, Orca, Qualle, Pinguin, Krebs, Egel, Aal, Seeigel, Seestern, Rochen" "Flossen:" $dice(2,4)-2
AMPHIBIEN = ,"Otter, Schildkröte, Schlange"

GROESSE_REL = ,"winzig, klein, mittel, gross, riesig, gigantisch"

PFLANZE = BLUME | BAUM | STRAUCH

EIGENSCHAFT = "giftig" "klebrig" "fleischfressend" "leuchtend" "normal" "stinkend" "nahrhaft"

BLUME_PREFIX = "woll" | "wald" | "holz" | "stein" | "öl" | "essig" | "salz" | "zucker" | "gift" | "stink" | "erd" | "feuer" | "wasser" | "luft" | "haar" | "kahl" | "korallen" | "pfeffer" | "scharf" | "sand" | "kalk" | "aschen" | "tränen" | "sonnen" | "mond" | "tag" | "nacht" | "zwerg" | "kugel" | "zylinder"
BLUME_SUFFIX = "dorn" | "beere" | "salat" | "kraut" | "kirsche" | "apfel" | "birne" | "orange" | "rose" | "fingerhut" | "distel" | "rebe" | "moos"

BLUME_NAME = BLUME_PREFIX BLUME_SUFFIX FARBE

BLUME = "Blume:" BLUME_NAME "Grösse:" $dice(1,5) "cm" BLUETE

BLUETE = "Blüte:" "Farbe:" FARBE "Anzahl" $dice(1,20)+1

BAUM = "Baum:" "Grösse" $dice(2,10)+1 "m"

STRAUCH = "Strauch" "Grösse" $dice(2,2)+1 "m"

FARBE = "rot" | "orange" | "gelb" | "grün" | "blau" | "violett" | "weiss" | "schwarz"

AUGENFARBE = ,"blau, grün, grau, blaugrün, bernstein, grünbraun, braun, hellblau"
HAARFARBE = ,"schwarz, dunkelbraun, braun, hellbraun, rotbraun, rot, dunkelblond, blond, grau, weiss"

FRISUR = ,"Dauerwelle, Glatze, Halbglatze, Igelfrisur, Irokese/Mohawk, Mähne, Pferdeschwanz, Pilz, Pony, Scheitel, Schmalzlocke, Steckfrisur, ungekämmt, verfiltzt, Zopf, Zwei Zöpfe"

STIMME = ,"hoch, tief, leise, laut, heiser, lispeln, monoton, piepsen, schrill, singen, stottern"

`;
