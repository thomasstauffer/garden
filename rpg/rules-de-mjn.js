
export const RULES_MJN = `

# Mutant Jahr Null

MJN = "Name" MJN_NAME "Alter" $20+dice(1,10) "Augen" AUGENFARBE "Haare" HAARFARBE FRISUR "Stimme" STIMME "Rolle" MJN_ROLLE "Charakter" CHARAKTER

MJN_NAME = ,"Hugust, Lenny, Marl, Pontis, Otiak, Ronan, Ingrit, Mubba, Nelma, Rebeth, Quark, Oktan, Plonk, Zingo, Zippo, Delta, Iridia, Loranga, Nafta, Zanova, Danko, Endel, Franton, Hammed, Max, Felin, Jena, Katin, Krin, Tula, Abed, Denrik, Fillix, Jonar, Leodor, Jolisa, Lula, Marlian, Monja, Novia, Finn, Jony, Mohan, Montiak, Rasper, Anny, Brie, Krinnel, Linna, Sofin, Erister, Olias, Maxim, Silas, Victon, Amara, Danova, Johalin, Hanneth, Miri, Augustian, Kristor, Maximon, Mohamin, Oskartian, Briktoria, Elona, Gunitt, Natara, Bristin, Dink, Fils, Hent, Mart, Wilo, Alia, Eria, Henny, Kim, Lin"

MJN_ROLLE = ,"Vollstrecker, Schrauber, Pirscher, Hehler, Hundeführer, Chronist, Boss, Sklave"

`;
