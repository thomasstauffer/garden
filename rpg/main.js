
import peg from '../Libraries/peg-0.10.0/peg-0.10.0.js';
import * as q from '../crow/q.js';
import { RULES_DE } from './rules-de.js';
import { RULES_TEST } from './rules-test.js';
import { RULES_MJN } from './rules-de-mjn.js';
import { RULES_CHARACTER } from './rules-de-character.js';

/*
Ideas:

- better output formating
- functions for gender?
- choice element with probability e.g. "yes:9" "no:1" means yes if selected in 9 out of 10 cases

*/

const GRAMMAR = `

start = nl* lines:line* { return lines }

line = comment / rule

comment = "#" (!nl .)* nl+ { return {type: 'comment'} }
rule = name:name _ "=" _ item:choice nl+ { return {type: 'rule', name: name.value, value: item } }

choice = first:seq rest:(_ "|" _ item:seq { return item})* { return { type: 'choice', value: [first].concat(rest) } }
seq = first:item rest:(_ item:item { return item })* { return { type: 'seq', value: [first].concat(rest) } }

item = ("(" _ choice:choice _ ")" { return choice }) / string / csv / math / external / repeat / name_or_call
name_or_call = name:name call:("(" _ item:choice _ ")" { return item })? { return call === null ? name : { type: 'callrule', name: name.value, value: call } }

string = quote chars:(!quote char:. { return char })* quote { return { type: 'string', value: chars.join('') } }
csv = "," quote chars:(!quote char:. { return char })* quote { return { type: 'csv', value: chars.join('').split(',').map($ => $.trim()) } }
math = "$" expr:expr { return { type: 'expr', expr: expr } }
external = "@" chars:[a-zA-Z0-9_]+ { return { type: 'external', value: chars.join('') } }
repeat = "{" _ item:item _ "}" expr:expr { return { type: 'repeat', value: item, expr: expr } }
name = first:[a-zA-Z] rest:[a-zA-Z0-9_]* { return { type: 'name', value: first + rest.join('') } }

expr = add
add = left:mul op:[+-] right:add { return { type: 'op', op: op, left:left, right:right } } / mul
mul = left:primary op:[*/] right:mul { return { type: 'op', op: op, left:left, right:right } } / primary
primary = number / name:[a-z]+ "(" params:params ")" { return { type: 'callexpr', name: name.join(''), params: params } }
params = first:expr rest:("," expr:expr { return expr })* { return [first].concat(rest) }
number = ("(" expr:expr ")" { return expr }) / chars:[0-9]+ { return { type: 'number', value: parseInt(chars.join('')) } }

quote = "\\""
_ = " "

nl = [\\n\\r]
eof = !.
eol = nl / eof
`;

function parse(rules, external) {
	function visit(ast) {
		if ((ast.type === 'seq') || (ast.type === 'choice')) {
			return { type: ast.type, value: ast.value.map($ => visit($)) };
		} else if (ast.type === 'external') {
			return external(ast.value);
		} else {
			return ast;
		}
	}

	const parser = peg.generate(GRAMMAR);
	const ast = parser.parse(rules);
	const rulesMap = {};
	for (const item of ast) {
		if (item.type === 'rule') {
			rulesMap[item.name] = visit(item.value);
		}
	}
	return rulesMap;
}

// randomInt(5) -> 0 - 4
function randomInt(number) {
	return Math.floor(Math.random() * number);
}

// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
// https://en.wikipedia.org/wiki/Marsaglia_polar_method
function randomGaussInt(mean, stddev) {
	let x1 = 0;
	let x2 = 0;
	let r = 0;
	do {
		x1 = (2.0 * Math.random()) - 1.0;
		x2 = (2.0 * Math.random()) - 1.0;
		r = (x1 * x1) + (x2 * x2);
	} while ((r >= 1.0) || (r <= Number.EPSILON));
	const scale = Math.sqrt((-2.0 * Math.log(r)) / r);
	const rnd = x1 * scale;
	return Math.round((rnd * stddev) + mean);
}

function randomGaussIntLimit(mean, stddev, limit) {
	const lower = mean - limit;
	const upper = mean + limit;
	let r = 0;
	do {
		r = randomGaussInt(mean, stddev);
	} while ((r < lower) || (r > upper));
	return r;
}

function generate(rulesMap, start) {
	const functionsMath = {
		dice: (repeat, side) => q.sum(q.times(repeat, _ => randomInt(side) + 1)),
		clamp: q.clamp,
		log: (...args) => console.log(args),
		gauss: (mean, stddev) => randomGaussInt(mean, stddev),
		gausslimit: (mean, stddev, limit) => randomGaussIntLimit(mean, stddev, limit),
		'+': (a, b) => a + b,
		'-': (a, b) => a - b,
		'*': (a, b) => a * b,
		'/': (a, b) => a / b,
	};
	const functionsRule = {
		'capitalize': $ => $[0].toUpperCase() + $.slice(1),
		'uppercase': $ => $.toUpperCase(),
		'lowercase': $ => $.toLowerCase(),
	};

	function visit(ast) {
		if (ast.type === 'seq') {
			return ast.value.map($ => visit($)).join(' ');
		} else if (ast.type === 'choice') {
			const index = randomInt(ast.value.length);
			return visit(ast.value[index]);
		} else if (ast.type === 'string') {
			return ast.value;
		} else if (ast.type === 'math') {
			const rnd = ast.offset + q.sum(q.times(ast.repeat, _ => randomInt(ast.side) + 1));
			return rnd.toString();
		} else if (ast.type === 'name') {
			return visit(rulesMap[ast.value]);
		} else if (ast.type === 'csv') {
			const index = randomInt(ast.value.length);
			return ast.value[index];
		} else if (ast.type === 'repeat') {
			const repeat = visit(ast.expr);
			return q.times(repeat, _ => visit(ast.value)).join(' ');
		} else if (ast.type === 'callrule') {
			return functionsRule[ast.name](visit(ast.value));
		} else if (ast.type === 'op') {
			return functionsMath[ast.op](visit(ast.left), visit(ast.right));
		} else if (ast.type === 'number') {
			return ast.value;
		} else if (ast.type === 'expr') {
			return visit(ast.expr).toString();
		} else if (ast.type === 'callexpr') {
			const params = ast.params.map($ => visit($));
			return functionsMath[ast.name](...params);
		} else {
			console.error('Unknown Rule', ast);
		}
	}
	return visit(rulesMap[start]);
}

function main() {
	const nodeRules = document.getElementById('rules');
	const nodeStart = document.getElementById('start');
	const nodeOutput = document.getElementById('output');
	const nodeDebug = document.getElementById('debug');

	const ADDITIONAL_RULES = [RULES_TEST, RULES_MJN, RULES_CHARACTER, RULES_CHARACTER];
	nodeRules.value = RULES_DE.trim();

	function external(what) {
		return { type: 'string', value: what };
	}

	function click() {
		const rules = [nodeRules.value].concat(ADDITIONAL_RULES).join('\n');
		//console.log(rules);
		let rulesMap = null;
		try {
			rulesMap = parse(rules, external);
		} catch (e) {
			console.error(e);
			nodeDebug.innerHTML = e.message + '<br>Start ' + e.location.start.line + ':' + e.location.start.column + ' End ' + e.location.end.line + ':' + e.location.end.column;
			return;
		}
		//console.log(ast);

		const generated = generate(rulesMap, 'ABCDE');
		//console.log(generated);
		console.assert(generated.replaceAll(' ', '') === 'abcde');

		const start = nodeStart.value;
		const output = generate(rulesMap, start);
		//console.log(output);
		nodeOutput.innerText = output;
	}

	document.getElementById('generate').addEventListener('click', click);
	click();
}

window.addEventListener('load', main);
