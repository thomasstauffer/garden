import time
import board
import digitalio

# ESP32C3 SuperMini
# makergo_esp32c3_supermini
print(board.board_id)

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

while True:
    time.sleep(0.2)
    led.value = 1
    time.sleep(0.2)
    led.value = 0
