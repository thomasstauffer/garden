
pip3 install --user --break-system-packages esptool

# ESP32 DEVKIT V1

esptool.py --chip esp32 --port /dev/ttyACM0 erase_flash

download firmware

https://circuitpython.org/board/doit_esp32_devkit_v1/

esptool.py --port /dev/ttyACM0 write_flash -z 0x0 adafruit-circuitpython-doit_esp32_devkit_v1-en_US-9.2.4.bin

picocom /dev/ttyACM0

# ESP 32 C3 Super Mini

https://circuitpython.org/board/makergo_esp32c3_supermini/

adafruit-circuitpython-makergo_esp32c3_supermini-en_US-9.2.4.bin

esptool.py --port /dev/ttyACM0 erase_flash

esptool.py --port /dev/ttyACM0 write_flash 0x0 adafruit-circuitpython-makergo_esp32c3_supermini-en_US-9.2.4.bin

picocom /dev/ttyACM0

# ESP 32 C6 Seeed Studio

https://files.seeedstudio.com/wiki/SeeedStudio-XIAO-ESP32C6/XIAO-ESP32-C6_v1.0_SCH_PDF_24028.pdf

https://circuitpython.org/board/seeed_xiao_esp32c6/

adafruit-circuitpython-seeed_xiao_esp32c6-en_US-9.2.4.bin

esptool.py --port /dev/ttyACM0 write_flash 0x0 adafruit-circuitpython-seeed_xiao_esp32c6-en_US-9.2.4.bin

picocom /dev/ttyACM0

# Pico (Original & Chinese)

https://circuitpython.org/board/raspberry_pi_pico/

adafruit-circuitpython-raspberry_pi_pico-en_US-9.2.4.uf2

Flash via BOOTSEL Button / Mass Storage Device

picocom /dev/ttyACM0

# Pico W

https://circuitpython.org/board/raspberry_pi_pico_w/

adafruit-circuitpython-raspberry_pi_pico_w-en_US-9.2.3.uf2

Flash via BOOTSEL Button / Mass Storage Device

picocom /dev/ttyACM0
