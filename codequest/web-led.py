import wifi
import time
import socketpool
import board
import digitalio
import mdns

# Tested 2024-02-16
#
# - Pico 1 W
# - ESP32C3 Super Mini
# - ESP32C6 Seeed Studio (board.LED not mapped yet, would be GPIO15, use e.g. D10 just for toggling another IO)
#
# Libraries via Thonny 4.1.7

# adafruit-circuitpython-httpserver
from adafruit_httpserver import Server, Request, Response, PUT

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

WIFI_SSID = os.getenv()['WIFI_SSID']
WIFI_PASSWORD = os.getenv()['WIFI_PASSWORD']

wifi.radio.hostname = 'tomcq'
wifi.radio.connect(ssid=WIFI_SSID, password=WIFI_PASSWORD)
pool = socketpool.SocketPool(wifi.radio)

print('AP Address', wifi.radio.ipv4_gateway)
print('Our Address', wifi.radio.ipv4_address)

mdns_server = mdns.Server(wifi.radio)
mdns_server.hostname = wifi.radio.hostname
mdns_server.advertise_service(service_type='_http', protocol='_tcp', port=80)

server = Server(pool, debug=True)

counter = 0

@server.route("/")
def on_root(request):
    global counter
    counter += 1
    return Response(request, 'Server Running (' + str(counter) + ')')

@server.route("/led", PUT)
def on_led(request):
    print(request.body)
    if request.body == b'off':
        led.value = False
    elif request.body == b'on':
        led.value = True
    return Response(request, "Ok")

server.serve_forever(host=str(wifi.radio.ipv4_address), port=80)
