import time
import board
import colorsys  # adafruit-circuitpython-colorsys
import neopixel  # adafruit-circuitpython-neopixel

def just_neopixel_rainbow():
    pixels = neopixel.NeoPixel(board.IO10, 100, auto_write=False)
    for led in range(100):
        rgb = colorsys.hsv_to_rgb(led / 100.0, 1.0, 0.05)
        m = 255
        pixels[led] = (int(rgb[0] * m), int(rgb[1] * m), int(rgb[2] * m))
    pixels.show()

def just_neopixel_glow():
    n = 80
    pixels = neopixel.NeoPixel(board.IO10, n, auto_write=False, bpp=3)
    for led in range(n):
        pixels[led] = (255, 0, 0)
    while True:
        b = math.sin(time.monotonic()) * 0.2 + 0.22
        pixels.brightness = b
        pixels.show()

#just_neopixel_rainbow()
#just_neopixel_glow()
