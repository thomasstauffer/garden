import time
import board
import digitalio
import adafruit_si7021  # adafruit-circuitpython-si7021
import adafruit_scd4x  # adafruit-circuitpython-scd4x
import adafruit_htu21d  # adafruit-circuitpython-HTU21D, GY-21
import adafruit_mpu6050  # dafruit-circuitpython-mpu6050
import adafruit_vl53l0x  # adafruit-circuitpython-vl53l0x

i2c = board.I2C()
i2c.try_lock()
devices = i2c.scan()
print('I2C Devices:', [hex(device) for device in devices])
i2c.unlock()

def just_blink():
    led = digitalio.DigitalInOut(board.LED)
    led.direction = digitalio.Direction.OUTPUT
    while True:
        time.sleep(0.2)
        led.value = 1
        time.sleep(0.2)
        led.value = 0

def just_htu21d():
    sensor = adafruit_htu21d.HTU21D(i2c)
    while True:
        print(f'Temperature: {sensor.temperature:.2f} °C')
        print(f'Humidity: {sensor.relative_humidity:.2f} %')
        time.sleep(5.0)

def just_si7021():
    sensor = adafruit_si7021.SI7021(i2c)
    while True:
        print(f'Temperature: {sensor.temperature:.2f} °C')
        print(f'Humidity: {sensor.relative_humidity:.2f} %')
        time.sleep(5.0)

def just_scd4x():
    scd4x = adafruit_scd4x.SCD4X(i2c)
    scd4x.start_periodic_measurement()
    while True:
        if scd4x.data_ready:
            print(f'CO2: {scd4x.CO2} ppm')
            print(f'Temperature: {scd4x.temperature:.2f} °C')
            print(f'Humidity: {scd4x.relative_humidity:.2f} %')
        time.sleep(5.0)

def just_mpu6050():
    mpu = adafruit_mpu6050.MPU6050(i2c)
    while True:
        a = mpu.acceleration
        g = mpu.gyro
        print(a)
        print(f'Acceleration X:{a[0]:.2f} Y:{a[1]:.2f}, Z:{a[2]:.2f} m/s^2')
        print(f'Gyro X:{g[0]:.2f} Y:{g[1]:.2f}, Z:{g[2]:.2f} degrees/s')
        print(f'Temperature: {mpu.temperature:.2f} C')
        time.sleep(1.0)

def just_vl53l0x():
    sensor = adafruit_vl53l0x.VL53L0X(i2c)
    while True:
        print(f'Range: {sensor.range:.2f} mm')
        time.sleep(1.0)

#just_blink()
#just_si7021()
#just_htu21d()
#just_scd4x()
#just_mpu6050()
#just_vl53l0x()
