all:
	$(MAKE) -C elm-sandbox all
	$(MAKE) -C heapless all
	$(MAKE) -C tomimicry all
	$(MAKE) -C tomnemonic all

clean:
	$(MAKE) -C elm-sandbox clean
	$(MAKE) -C heapless clean
	$(MAKE) -C tomimicry clean
	$(MAKE) -C tomnemonic clean

test:
	$(MAKE) -C crow test
	$(MAKE) -C heapless test
	$(MAKE) -C tomany test
	$(MAKE) -C tomark test
	$(MAKE) -C tomath test
	$(MAKE) -C tomimicry test
	$(MAKE) -C tomnemonic test

lint:
	$(MAKE) -C anderswelt lint
	$(MAKE) -C crow lint
	$(MAKE) -C notes lint
	$(MAKE) -C nota lint
	$(MAKE) -C randomstory lint
	$(MAKE) -C template lint
	$(MAKE) -C tomany lint
	$(MAKE) -C tomark-editor lint
	$(MAKE) -C tomark lint
	$(MAKE) -C tomascara lint
	$(MAKE) -C tomath lint
	$(MAKE) -C tomicrocosm lint
	$(MAKE) -C tomimicry lint
	$(MAKE) -C tomomentum lint
	$(MAKE) -C rpg lint

format:
	$(MAKE) -C heapless format

setup:
	sudo apt install curl
	# programming
	sudp apt install julia ghc clojure clang-9 g++ gcc make erlang-base golang-go python3-pip virtualenv
	# hardware programming
	sudo apt install iverlog gtkwave
	# /lib/modules/node_modules
	sudo npm -g install eslint
	# llvm
	pip3 install llvmlite
	# webassembly
	sudo apt install wabt
	#wget http://ftp.ch.debian.org/debian/pool/main/w/wabt/wabt_1.0.11-1_amd64.deb
	#dpkg -i wabt_1.0.11-1_amd64.deb
	#rm -f wabt_1.0.11-1_amd64.deb

node-check: NODE_VERSION=$(shell node --version | cut -c 1-4)
node-check:
	test "$(NODE_VERSION)" = "v13."

setup-node:
	# TODO check if already installed
	# https://github.com/nodesource/distributions
	curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
	sudo apt install nodejs

setup-nim:
	# https://nim-lang.org/install_unix.html
	curl https://nim-lang.org/choosenim/init.sh -sSf | sh

setup-dev: setup-vscode setup-tortoisehg
	sudo apt install mc mercurial git

setup-tortoisehg:
	# TODO check if already installed
	wget https://mirrors.kernel.org/ubuntu/pool/universe/t/tortoisehg/tortoisehg_4.8.1-0.1_all.deb
	sudo apt install ./tortoisehg_4.8.1-0.1_all.deb

setup-vscode:
	# https://code.visualstudio.com/docs/setup/linux
	sudo snap install --classic code

gitlab:
	# cp return 1 when omitting recursive directories
	cp -a . public || true
	# ln -s . public
	make -C public/notes
	make -C public/randomstory
	date > public/build.txt
